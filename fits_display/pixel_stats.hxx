/*
  "@(#) $Id$"
*/

#ifndef PIXEL_STATS_HXX
#define PIXEL_STATS_HXX

class pixel_stats
{
public:
  pixel_stats():min_x(0), min_y(0), max_x(0), max_y(0), minimum(0.),
    maximum(0.), mean(0.),stddev(0.),median(0)
  {
  };
  pixel_stats(const pixel_stats & old_pixel_stats)
  {
    min_x = old_pixel_stats.min_x;
    min_y = old_pixel_stats.min_y;
    max_x = old_pixel_stats.max_x;
    max_y = old_pixel_stats.max_y;
    minimum = old_pixel_stats.minimum;
    maximum = old_pixel_stats.maximum;
    mean = old_pixel_stats.mean;
    stddev = old_pixel_stats.stddev;
    median = old_pixel_stats.median;
  };
  ~pixel_stats()
  {
  };
  bool operator==(const pixel_stats & old_pixel_stats) const
  {
    if(min_x != old_pixel_stats.min_x)
      return false;
    if(min_y != old_pixel_stats.min_y)
      return false;
    if(max_x != old_pixel_stats.max_x)
      return false;
    if(max_y != old_pixel_stats.max_y)
      return false;
    if(minimum != old_pixel_stats.minimum)
      return false;
    if(maximum != old_pixel_stats.maximum)
      return false;
    if(mean != old_pixel_stats.mean)
      return false;
    if(stddev != old_pixel_stats.stddev)
      return false;
    if(median != old_pixel_stats.median)
      return false;
    return true;
  };
  pixel_stats & operator=(const pixel_stats & old_pixel_stats)
  {
    if(static_cast < const pixel_stats & >(*this) == old_pixel_stats)
      {
	  return (static_cast < pixel_stats & >(*this));
      }
    else
      {
	  min_x = old_pixel_stats.min_x;
	  min_y = old_pixel_stats.min_y;
	  max_x = old_pixel_stats.max_x;
	  max_y = old_pixel_stats.max_y;
	  minimum = old_pixel_stats.minimum;
	  maximum = old_pixel_stats.maximum;
	  mean = old_pixel_stats.mean;
	  stddev = old_pixel_stats.stddev;
	  median = old_pixel_stats.median;
	  return (static_cast < pixel_stats & >(*this));
      }
  };
  void copy(const pixel_stats & old_pixel_stats)
  {
    min_x = old_pixel_stats.min_x;
    min_y = old_pixel_stats.min_y;
    max_x = old_pixel_stats.max_x;
    max_y = old_pixel_stats.max_y;
    minimum = old_pixel_stats.minimum;
    maximum = old_pixel_stats.maximum;
    mean = old_pixel_stats.mean;
    stddev = old_pixel_stats.stddev;
    median = old_pixel_stats.median;
  };

  int min_x;
  int min_y;
  int max_x;
  int max_y;
  float minimum;
  float maximum;
  float mean;
  float stddev;
  int median;
};
#endif
