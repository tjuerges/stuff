/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Feb 18, 2007  created
*/

#include <qapplication.h>
#include <qguardedptr.h>
#include <qcombobox.h>
#include <logging.h>
#include <maciSimpleClient.h>
#include "EventLoop.h"
#include "fits_display.h"

maci::SimpleClient* sc(0);
EventLoop* myEventLoop(0);

int main(int argc, char* argv[])
{
    QApplication app(argc, argv);

    // Standard Pixmap for every icon:
    app.setFont(QFont("Helvetica", 10), TRUE);

    sc = new maci::SimpleClient;
    try
    {
        /**
         * Create the instance of Client and Init() it.
         */
        if(sc->init(qApp->argc(), qApp->argv()) == 0)
        {
            ACS_SHORT_LOG((LM_ERROR,"CCDGUI::main: Cannot init client."));
            delete sc;
            return -1;
        }

        if(sc->login()==0)
        {
            ACS_SHORT_LOG((LM_ERROR,"CCDGUI::main: Cannot login."));
            delete sc;
            return -1;
        }
    }
    catch(...)
    {
        ACS_SHORT_LOG((LM_ERROR,"CCDGUI::main: Exception in init!"));
        delete sc;
        return -1;
    }

    myEventLoop = new EventLoop;
    myEventLoop->setSimpleClient(sc);
    myEventLoop->start();

    QGuardedPtr< FitsDisplay > display(new FitsDisplay);

    maci::HandleSeq seq;
    maci::ComponentInfoSeq_var components = sc->manager()->get_component_info(
        sc->handle(), seq, "*", "*FrameGrabber*", false);

    for(CORBA::ULong i(0UL); i < components->length(); ++i)
    {
        display->FrameGrabberComponents->insertItem(
            QString(components[i].name.in()));
    }

    display->setSimpleClient(sc);

    app.setMainWidget(display);
    display->show();
    int ret(app.exec());

    sc->logout();
    sc->destroy();

    return ret;
}
