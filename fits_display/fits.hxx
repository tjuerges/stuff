/*
  "@(#) $Id$"
*/

#ifndef FITS_HXX
#define FITS_HXX

#include <qstring.h>

class fits_c
{
    public:
    fits_c::fits_c(void):ra(-9999.9),dec(-9999.9),epoch(-9999.9),temperat(-9999.9),airtempe(-9999.9),exptime(-9999.9),gain(-9999.9),xpixsz(-9999.9),ypixsz(-9999.9),crot1(-9999.9),crot2(-9999.9),cdelt1(-9999.9),cdelt2(-9999.9),focallen(-9999),width(-9999),height(-9999)
    {};
    fits_c::~fits_c(void)
    {};
	    
    QString origfile;
    QString filename;
    QString type;
    QString imagetyp;
    QString observer;
    QString origin;
    QString telescop;
    QString instrume;
    QString date;
    QString date_obs;
    QString datestop;
    QString filter;
    QString object;
    QString source;
    QString timesys;
    double ra;
    double dec;
    double epoch;
    double temperat;
    double airtempe;
    float exptime;
    float gain;
    float xpixsz;
    float ypixsz;
    float crot1;
    float crot2;
    float cdelt1;
    float cdelt2;
    int focallen;
    int width;
    int height;
};
#endif
