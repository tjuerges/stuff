/*
  "@(#) $Id$"
*/

#include "Image.hxx"
#include <memory>
#include <iostream>
#include <vector>
#include <complex>
#include <algorithm>
#include <qguardedptr.h>
#include <qapplication.h>
#include <qimage.h>
#include <qpixmap.h>
#include "my_types.hxx"
#include "pixel_stats.hxx"
#include "iso_time.hxx"
#include "image_type.hxx"
#include "guide_star.hxx"
#include "fits.hxx"
#include "box.hxx"

/*
Image::Image(const unsigned short width,const unsigned short height,const int displayed_width,const int displayed_height,const std::vector<USHORT>& values,const int off_x,const int off_y,const int lum,const int cont):original_Image(0),downscaled_Image(0),original_Pixmap(0),data(0),img_width(width),img_height(height),display_height(0),display_width(0),img_size(static_cast<unsigned int>(img_width)*static_cast<unsigned int>(img_height)),offset_x(off_x),offset_y(off_y),luminosity(lum),contrast(cont),Valid(false)

displayed_height is given above just for convenience. Thus below it is removed because it is never used.
*/
Image::Image(
	const unsigned short width,
	const unsigned short height,
	const int displayed_width,
	const int,
	const std::vector< USHORT >& values,
	const QPoint& offset,
	const int lum,
	const int cont):
	original_Image(0),
	downscaled_Image(0),
	original_Pixmap(0),
	data(0),
	img_width(width),
	img_height(height),
	display_height(0),
	display_width(0),
	img_size(static_cast< unsigned int >(img_width) * static_cast< unsigned int >(img_height)),
	offset_x(offset.x()),
	offset_y(offset.y()),
	luminosity(lum),
	contrast(cont),
	Valid(false)
{
	if(width > displayed_width)
	{
		display_width = static_cast< unsigned short >(displayed_width);
		display_height = static_cast< unsigned short >(
			static_cast< float >(displayed_width * img_height) / static_cast< float >(img_width));
	}
	else
	{
		display_width = img_width;
		display_height = img_height;
	}
	//Create null image.
	data = new std::vector< USHORT >(values);
	if(static_cast< unsigned int >(data->size()) != img_size)
	{
		std::cerr << "ERROR in Image::Image: Cannot allocate enough memory for data array." << std::endl;
		return;
	}

	original_Image=new QImage(img_width,img_height,BIT_PER_PIXEL,NUMBER_OF_COLORS);
	if((static_cast<unsigned int>(original_Image->width())*static_cast<unsigned int>(original_Image->height()))!=img_size)
	{
	std::cerr<<"ERROR in Image::Image: Cannot allocate enough memory for QImage."<<std::endl;
	delete[] data;
	data=0;
	return;
	}

	original_Pixmap=new QPixmap(img_width,img_height);
	if((static_cast<unsigned int>(original_Pixmap->width())*static_cast<unsigned int>(original_Pixmap->height()))!=img_size)
	{
	std::cerr<<"ERROR in Image::Image: Cannot allocate enough memory for QPixmap."<<std::endl;
	delete[] data;
	data=0;
	delete original_Image;
	original_Image=0;
	return;
	}
	Valid=true;
	calc_stats();
	create_colortable();
	make_image_pixels();
	set_colortable();
}

void Image::calc_stats(void)
{
	if(Valid)
	{
	unsigned long pix_sum=0L;
	unsigned int i_min=0,i_max=0;
	unsigned short min=MAX_DATA_VAL,max=MIN_DATA_VAL,pixel=0;

	// Calculate minimum and maximum intensity.
	for(int i=img_size-1;i>=0;--i)
	{
		pixel=data->at(i);
		pix_sum+=static_cast<unsigned long>(pixel);
		if(pixel<min)
		{
		i_min=i;
		min=pixel;
		}
		else if(pixel>max)
		{
		i_max=i;
		max=pixel;
		}
	}

	stats.minimum=static_cast<float>(min);
	stats.maximum=static_cast<float>(max);
	stats.mean=static_cast<float>(pix_sum)/static_cast<float>(img_size);
	stats.max_x=i_max%img_width;
	stats.max_y=i_max/img_width;
	stats.min_x=i_min%img_width;
	stats.min_y=i_min/img_width;

	// Calculate the standard deviation.
	const float mean=stats.mean;
	float stddev=static_cast<float>(0);
	for(int i=img_size-1;i>=0;--i)
	{
		pixel=data->at(i);
		stddev+=std::pow(mean-static_cast<float>(pixel),static_cast<float>(2));
	}
	stddev/=static_cast<float>(img_size); // Use 1/N because we calculate over the complete sample.
	stddev=std::sqrt(stddev);
	stats.stddev=stddev;

	//Calculate the median
	std::vector<unsigned short>* dummy=new std::vector<unsigned short>(img_size);
	if(dummy->size()!=img_size)
	{
		std::cerr<<"ERROR in Image::calc_stats: Cannot allocate enough memory for temporary data array."<<std::endl;
		stats.median=-1;
	}
	else
	{
		for(int i=img_size-1;i>=0;--i)
		{
		dummy->at(i)=data->at(i);
		}

		std::sort(dummy->begin(),dummy->end());

		if((img_size%2)==1) // img_size is odd: median=data(img_size/2+1)
		{
		stats.median=static_cast<unsigned short>(dummy->at((img_size+1)/2));
		}
		else // img_size is even: median=(data(img_size/2)+data(img_size/2+1))/2
		{
		stats.median=static_cast<unsigned short>((dummy->at(img_size/2)+dummy->at((img_size/2)+1))/2);
		}
	}
	delete(std::vector<unsigned short>*) dummy;
	}
	else
	{
	std::cerr<<"ERROR in Image::calc_stats: Data is not valid."<<std::endl;
	}
}

void Image::set_colortable(void)
{
	// Thanks to Daniel Brown for helping me out.
	float tan_alpha=std::tan(static_cast<float>(contrast)*static_cast<float>(M_PI)/static_cast<float>(180));
	const float num_colors=static_cast<float>(NUMBER_OF_COLORS);

	qApp->lock();
	for(int table_index=NUMBER_OF_COLORS-1;table_index>=0;--table_index)
	{
	float color_value=tan_alpha*static_cast<float>(table_index)+(num_colors/static_cast<float>(2))-tan_alpha*static_cast<float>(luminosity);
	unsigned int value=0;
	if(color_value<static_cast<float>(0))
	{
		color_value=static_cast<float>(0);
	}
	else if(color_value>=num_colors)
	{
		color_value=num_colors-static_cast<float>(1);
	}
	value=static_cast<unsigned int>(color_value);
	downscaled_Image->setColor(table_index,qRgb(value,value,value));
	original_Image->setColor(table_index,qRgb(value,value,value));
	}
}

void Image::make_image_pixels(void)
{
  const float min=stats.mean-static_cast<float>(1)*stats.stddev;
  const float max=stats.mean+static_cast<float>(4)*stats.stddev;  
  const float maxmin=max-min;
  const float maxminbroken=maxmin/static_cast<float>(NUMBER_OF_COLORS);
  float colortable_index=0.;  

  qApp->lock();
  unsigned int pixel_number,pixel_value;
  for(int y=img_height-1;y>=0;--y)
  {
	  for(int x=img_width-1;x>=0;--x)
	  {
	  pixel_number=x+y*img_width;
	  pixel_value=data->at(pixel_number);
	  if(pixel_value>max)
	  {
		  colortable_index=static_cast<float>(NUMBER_OF_COLORS-1);
	  }
	  else if(pixel_value<min)
	  {
		  colortable_index=static_cast<float>(0);
	  }
	  else
	  {
		  colortable_index=(static_cast<float>(pixel_value)-min)/maxminbroken;
	  }
	  original_Image->setPixel(x,y,static_cast<unsigned int>(colortable_index));
	  }
  }
  if(downscaled_Image!=0)
  {
	  delete downscaled_Image;
  }
  downscaled_Image=new QImage(original_Image->scale(display_width,display_height));
  downscaled_Image->setNumColors(NUMBER_OF_COLORS);  
  qApp->unlock();
}

void Image::create_colortable(void)
{
	if(original_Image!=0)
	{
	qApp->lock();
	//reset color table to NUMBER_OF_COLORS grey levels
	for(int i=NUMBER_OF_COLORS-1;i>=0;--i)
	{
		original_Image->setColor(i,qRgb(i,i,i));
	}
	qApp->unlock();
	}
}

void Image::Logarithmic(void)
{
}

void Image::Linear(void)
{
}

void Image::set_Luminosity(int value)
{
	luminosity=value;
	set_colortable();
}

void Image::set_Contrast(int value)
{
	contrast=value;
	set_colortable();
}
