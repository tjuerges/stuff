# $Id$

TEMPLATE = app

CONFIG += warn_on release rtti stl qt thread

DEPENDPATH += stuff

INCLUDEPATH += . \
    stuff sla/include \
    /usr/include/cfitsio \
    /alma/ACS-current/ACSSW/include \
    /alma/ACS-current/TAO/ACE_wrappers/build/linux \
    /alma/ACS-current/TAO/ACE_wrappers/build/linux/ace \
    /alma/ACS-current/TAO/ACE_wrappers/build/linux/TAO \
    /alma/ACS-current/TAO/ACE_wrappers/build/linux/TAO/orbsvcs \
    /alma/ACS-current/TAO/ACE_wrappers/build/linux/TAO/orbsvcs/orbsvcs \
    /export/home/delphinus/tjuerges/workspace/introot.ICD/include \
    /export/home/delphinus/tjuerges/workspace/introot.CONTROL/include

LIBS += -lloki \
    -lacsutil \
    -lmaciClient \
    -lbaselogging \
    -llogging \
    -lFrameGrabberStubs \
    -lFrameGrabberExceptions \
    -lcfitsio \
    -lfftw3 \
    -lm \
    sla/lib/libsla.a

PROG_VERSION = $$system(./compilation_run)

COMP_DATE = $$system(date --iso-8601=seconds)

#DEFINES += COMPILATION_DATE=\"$$COMP_DATE\" VERSION=\"$$PROG_VERSION\"

QMAKE_CXXFLAGS_RELEASE = -g \
	-O2 \
	-march=pentium4m \
	-mfpmath=sse,387 \
	-mmmx \
	-m3dnow \
    -msse \
    -msse2 \
    -DCOMPILATION_DATE=\"`date --iso-8601=seconds`\" \
    -DVERSION=\"`./compilation_run`\"

# Input
FORMS += fits_display.ui

HEADERS += EventLoop.h \
        fits.hxx \
        fits_display.ui.h \
        Image.hxx \
        my_types.hxx \
        pixel_stats.hxx \
        stuff/box.hxx \
        stuff/fits.hxx \
        stuff/gauss.hxx \
        stuff/guide_star.hxx \
        stuff/image_type.hxx \
        stuff/iso_time.hxx \
        stuff/read_fits.hxx \
        stuff/write_fits.hxx

SOURCES += EventLoop.cpp \
        Image.cpp \
        main.cpp \
        stuff/gauss.cpp \
        stuff/iso_time.cpp \
        stuff/read_fits.cpp \
        stuff/write_fits.cpp
