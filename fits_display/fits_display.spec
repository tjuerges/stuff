%define name fits_display
%define version 0.3
%define release `cat compilation_run`

Summary: Software for displaying FITS data files.
Name: %{name}
Version: %{version}
Release: %{release}
Copyright: GPL
Group: Sciences/Astronomy
Source: https://www.sokrates.homelinux.net/~thomas/Private/Soft/Astro/%{name}-%{version}.tar.bz2
Url: https://www.sokrates.homelinux.net/~thomas/
Buildroot: %{_tmppath}/%{name}-buildroot

%description
Displaying program for astronomical CCD image data files in FITS format.

%prep
rm -rf $RPM_BUILD_ROOT

%setup

%configure

%build
qmake -o Makefile %{name}.pro "QMAKE_CXXFLAGS=$RPM_OPT_FLAGS"

%make

%install
install -d $RPM_BUILD_ROOT%{_bindir}
install -s -m 0755 ./%{name} $RPM_BUILD_ROOT%{_bindir}/%{name}

install -d $RPM_BUILD_ROOT%{_miconsdir}
install -d $RPM_BUILD_ROOT%{_iconsdir}
install -d $RPM_BUILD_ROOT%{_liconsdir}
install -m 0644 ./%{name}-16.xpm $RPM_BUILD_ROOT%{_miconsdir}/%{name}.xpm
install -m 0644 ./%{name}-32.xpm $RPM_BUILD_ROOT%{_iconsdir}/%{name}.xpm
install -m 0644 ./%{name}-48.xpm $RPM_BUILD_ROOT%{_liconsdir}/%{name}.xpm

install -d $RPM_BUILD_ROOT%{_menudir}

cat << EOF > $RPM_BUILD_ROOT/%{_menudir}/%{name}
?package(fits_display):command="%{name}" icon="%{name}.xpm" needs="X11" section="Applications/Sciences" title="FITS display" longtitle="FITS display software"
EOF

%clean
rm -rf $RPM_BUILD_ROOT

%post
%{update_menus}

%postun
%{clean_menus}

%files
%defattr(0644,root,root,0755)
%doc AUTHORS CHANGELOG COPYING INSTALL LICENSE README TODO
%defattr(0755,root,root,0755)
%{_bindir}/%{name}
%defattr(0644,root,root,0644)
%{_miconsdir}/%{name}.xpm
%{_iconsdir}/%{name}.xpm
%{_liconsdir}/%{name}.xpm
%{_menudir}/%{name}

%changelog
* Tue Nov 28 2006 Thomas Juerges <thomas@sokrates.homelinux.net> 0.3-0
- Updated to compile with latest fxt and fftw3 packages.
* Sat Jan 24 2004 Thomas Juerges <thomas.juerges@astro.rub.de> 0.2-1
- Fixed incompabilities with QT update to version 3.1.2.
* Sat Jun 21 2003 Thomas Juerges <thomas.juerges@astro.rub.de> 0.1-5
- Fixed bug for images with width!=height and width>display_width.
- Fixed bug when reading FITS images in read_fits.cpp.
* Mon Jun 09 2003 Thomas Juerges <thomas.juerges@astro.rub.de> 0.1-4
- Added second type of FFT calculation, using FXT package (c) J�rg
Arndt and FFTW package.
- Moved image stuff to own class.
* Sun Jun 08 2003 Thomas Juerges <thomas.juerges@astro.rub.de> 0.1-3
- Added much more stuff. Now debugging.
* Fri Dec 13 2002 Thomas Juerges <thomas.juerges@astro.ruhr-uni-bochum.de> 0.1-2
- Added Fast Fourier Transformation (FFT) (pre beta!)
* Fri Oct 18 2002 Thomas Juerges <thomas.juerges@astro.ruhr-uni-bochum.de> 0.1-1
- Initial work has been done.

#"@(#) $Id$"
