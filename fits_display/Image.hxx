/*
  "@(#) $Id$"
*/

#ifndef IMAGE_HXX
#define IMAGE_HXX

#include <memory>
#include <iostream>
#include <vector>
#include <qguardedptr.h>
#include <qimage.h>
#include <qpixmap.h>
#include <qpoint.h>
#include "my_types.hxx"
#include "pixel_stats.hxx"
#include "iso_time.hxx"
#include "image_type.hxx"
#include "guide_star.hxx"
#include "fits.hxx"
#include "box.hxx"

class Image
{
	public:
		Image::Image(
			const unsigned short,
			const unsigned short,
			const int,
			const int,
			const std::vector< unsigned short >&,
			const QPoint&,
			const int,
			const int);

		~Image()
		{
			delete original_Image;
			delete downscaled_Image;
			delete original_Pixmap;
			delete data;
		};

		const pixel_stats& get_pixel_stats(void) const
		{
			return stats;
		};

		template< typename T > const unsigned short get_data(const T index) const
		{
			if(data != 0)
			{
				return data->at(static_cast< unsigned int >(index));
			}
			else
			{
				std::cerr << "ERROR in Image::get_data: Cannot return value. "
					"Array is not defined."
					<< std::endl;
				return 0;
			}
		};

		template< typename T > const unsigned short get_data(
			const T x, const T y) const
		{
			if(data != 0)
			{
				return data->at(static_cast< unsigned int >(x)
					+ static_cast< unsigned int >(y) * img_width);
			}
			else
			{
				std::cerr << "ERROR in Image::get_data: Cannot return value. "
					"Array is not defined."
					<< std::endl;
				return 0;
			}
		};

		std::vector< unsigned short >& get_data_array(void) const
		{
			return *data;
		};

		const QImage& get_original_Image(void) const
		{
			return *original_Image;
		};

		const QPixmap* get_original_Pixmap(void) const
		{
			original_Pixmap->convertFromImage(*original_Image);
			return original_Pixmap;
		};

		const QImage& get_downscaled_Image(void) const
		{
			return *downscaled_Image;
		};

		const unsigned short get_img_width(void) const
		{
			return img_width;
		};

		const unsigned short get_img_height(void) const
		{
			return img_height;
		};

		const unsigned int get_img_size(void) const
		{
			return img_size;
		};

		template< typename T > void remap_coordinates(T& x, T& y,
			const int direction)
		{
			if(direction == TO_IMAGE)
			{
				x -= offset_x;
				y -= offset_y;

				x = static_cast< T >(
					static_cast< float >(x * img_width) / static_cast< float >(display_width));
				y = static_cast< T >(
					static_cast< float >(y * img_height) / static_cast< float >(display_height));
			}
			else
			{
				x = static_cast< T >(
					static_cast< float >(x * display_width) / static_cast< float >(img_width));
				y = static_cast< T >(
					static_cast< float >(y * display_height) / static_cast< float >(img_height));
			}
		};

		template<typename T> void remap_coordinates(T& x, T& y, T& w, T& h,
			const int direction)
		{
			remap_coordinates(x,y,direction);
			remap_coordinates(w,h,direction);
		};

		void Image::remap_coordinates(box& cb, const int direction)
		{
			remap_coordinates(cb.x,cb.y,direction);
			remap_coordinates(cb.w,cb.h,direction);
		};

		void Invert(void)
		{
			if(Valid == true)
			{
				downscaled_Image->invertPixels();
			}
		};

		void set_Luminosity(int);
		void set_Contrast(int);
		void Logarithmic(void);
		void Linear(void);

	private:
		void create_colortable(void);
		void set_colortable(void);
		void make_image_pixels(void);
		void calc_stats(void);
		void make_rgb(const unsigned short,const unsigned short);

		QImage* original_Image;
		QImage* downscaled_Image;
		QPixmap* original_Pixmap;
		std::vector<unsigned short>* data;

		unsigned short img_width, img_height;
		unsigned short display_height, display_width;
		unsigned int img_size;
		int offset_x,offset_y;
		int luminosity,contrast;

		pixel_stats stats;
		bool Valid;
};

#endif // IMAGE_HXX
