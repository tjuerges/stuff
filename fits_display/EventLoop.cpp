/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Feb 18, 2007  created
*/

#include <EventLoop.h>
#include <maciSimpleClient.h>
#include <ace/Time_Value.h>

void EventLoop::setSimpleClient(maci::SimpleClient* theClient)
{
    sc = theClient;
}

void EventLoop::run()
{
    eventEnd = false;
    ACE_Time_Value t;

    while(eventEnd == false)
    {
        t.set(5L, 0L);
        sc->run(t);
    }
}

void EventLoop::stop()
{
    eventEnd = true;
}
