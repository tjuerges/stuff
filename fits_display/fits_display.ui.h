/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename slots use Qt Designer which will
** update this file, preserving your code. Create an init() slot in place of
** a constructor, and a destroy() slot in place of a destructor.
*****************************************************************************/

/*
 "@(#) $Id$"
*/

#include <iostream>
#include <cerrno>
#include <complex>
#include <vector>
#include <string>
#include <algorithm>

#include <qslider.h>
#include <qcheckbox.h>
#include <qpushbutton.h>
#include <qtooltip.h>
#include <qpainter.h>
#include <qevent.h>
#include <qpixmap.h>
#include <qspinbox.h>
#include <qimage.h>
#include <qcombobox.h>
#include <qtextview.h>
#include <qpoint.h>
#include <qguardedptr.h>
#include <qobject.h>
#include <qfont.h>
#include <qpixmap.h>
#include <qguardedptr.h>
#include <qfiledialog.h>
#include <qradiobutton.h>
#include <qstring.h>
#include <qmessagebox.h>
#include <qtextview.h>
#include <qregexp.h>

#include "Image.hxx"
#include "fits_display-48.xpm"

#include <complex>
#include <fftw3.h>

#include <acserr.h>
#include <FrameGrabberC.h>


void FitsDisplay::init()
{
    imageValid = fft_done = button_pressed = false;
    fits_Image = real_Image = imaginary_Image = amplitude_Image = phase_Image
        = 0;
    displayed_Image = 0;
    //File saving class:
    writefitsfile = new write_fits;
    //File reading class:
    read_fits_file = new read_fits;

    zoom_factor = ZoomFactor->value();
    zoom_width = Zoom->width();
    zoom_height = Zoom->height();
    zoom_box_size = 4;

    setCaption(QString("FITS display (compiled on " COMPILATION_DATE ", build " VERSION ")"));
    IconPixmap = new QPixmap(reinterpret_cast< const char* >(fits_display_48_xpm));
    setIcon(*IconPixmap);

    Pixmap = 0;
    adjustSize();

    set_FWHM_box(FWHM->value());
    set_pixel_width(PixelWidth->value());
    set_pixel_height(PixelHeight->value());
    set_focal_length(FocalLength->value());

    connect_signals_to_slots();
    //    this->setUpdatesEnabled(true);
}

void FitsDisplay::connect_signals_to_slots(void)
{
    connect(this, SIGNAL(need_write_on_log_window(const char*)),
        this, SLOT(write_on_log_window(const char*)));
    connect(this, SIGNAL(need_write_on_log_window(const QString&)),
        this, SLOT(write_on_log_window(const QString&)));
    connect(read_fits_file, SIGNAL(need_write_on_log_window(const std::string&)),
        this, SLOT(write_on_log_window(const std::string&)));
    connect(read_fits_file, SIGNAL(need_write_on_log_window(const char*)),
        this, SLOT(write_on_log_window(const char*)));
    connect(writefitsfile, SIGNAL(need_write_on_log_window(const std::string&)),
        this, SLOT(write_on_log_window(const std::string&)));
    connect(writefitsfile, SIGNAL(need_write_on_log_window(const char*)),
        this, SLOT(write_on_log_window(const char*)));
    connect(this, SIGNAL(need_get_header(fits_c&)),
        read_fits_file, SLOT(get_header(fits_c&)));
}

void FitsDisplay::destroy()
{
    std::cout << "yo!" << std::endl;

    this->disconnect();
    reset();
    delete IconPixmap;
    delete Pixmap;
    delete read_fits_file;
    delete writefitsfile;
    selectFrameGrabber(FrameGrabberComponents->text(0));
}

void FitsDisplay::autofind_guide(void)
{
    if(imageValid == true)
    {
        pixel_stats stats = displayed_Image->get_pixel_stats();
        my_guide.x = stats.max_x;
        my_guide.y = stats.max_y;
        cursor_box.x = my_guide.x-BOX_MIN / 2;
        cursor_box.y = my_guide.y-BOX_MIN / 2;
        if(cursor_box.x + BOX_MIN >= displayed_Image->get_img_width())
        {
            cursor_box.x = displayed_Image->get_img_width() - BOX_MIN;
        }
        if(cursor_box.y + BOX_MIN >= displayed_Image->get_img_height())
        {
            cursor_box.y = displayed_Image->get_img_height() - BOX_MIN;
        }

        cursor_box.w = BOX_MIN;
        cursor_box.h = BOX_MIN;

        box cb(cursor_box);
        int x(stats.max_x), y(stats.max_y);
        QPainter p;
        p.begin(Image_Frame);
        p.setPen(Qt::red);
        displayed_Image->remap_coordinates(x, y, FROM_IMAGE);
        p.drawPoint(x, y);
        displayed_Image->remap_coordinates(cb, FROM_IMAGE);
        p.drawRect(cb.x, cb.y, cb.w, cb.h);
        p.end();
        set_guiders();
    }
}

void FitsDisplay::guide_star_off(void)
{
    my_guide.valid = false;
    update();
}

/*
    Set the colortable according to the user's wanted setup (luminosity
    and contrast sliders) and the Fits image pixel values.
    The image can additionally be inverted.
*/
void FitsDisplay::SetLuminosity(int value)
{
    if(imageValid == true)
    {
        displayed_Image->set_Luminosity(value);
        update();
    }
}

void FitsDisplay::SetContrast(int value)
{
    if(imageValid == true)
    {
        displayed_Image->set_Contrast(value);
        update();
    }
}

void FitsDisplay::set_FWHM_box(int diameter)
{
    if(diameter > 0)
    {
        FWHM_diameter = diameter;
        BOX_MIN = diameter;
    }
}

void FitsDisplay::set_pixel_width(int width)
{
    if(width > 0)
    {
        float fwidth(static_cast< float >(width) / 100.0);
        pixel_width = fwidth;
        if(pixel_height > 0)
        {
            pixel_square = std::sqrt(pixel_width * pixel_height);
        }
    }
}

void FitsDisplay::set_pixel_height(int height)
{
    if(height > 0)
    {
        float fheight(static_cast< float >(height) / 100.0);
        pixel_height = fheight;
        if(pixel_width > 0)
        {
            pixel_square = std::sqrt(pixel_width * pixel_height);
        }
    }
}

void FitsDisplay::set_focal_length( int length)
{
    if(length > 0)
    {
        focal_length = length;
    }
}

void FitsDisplay::DisplayFITSHeader(void)
{
    if(imageValid == true)
    {
        const QString header(read_fits_file->get_header().c_str());
        StatsBox->append(header);
    }
}

void FitsDisplay::fft_switch(void)
{
    if(Real->isChecked() == true)
    {
        displayed_Image = real_Image;
    }
    else if(Imag->isChecked() == true)
    {
        displayed_Image = imaginary_Image;
    }
    else if(Amplitude->isChecked() == true)
    {
        displayed_Image = amplitude_Image;
    }
    else if(Phase->isChecked() == true)
    {
        displayed_Image = phase_Image;
    }

    update();
}

void FitsDisplay::fft_on_off(void)
{
    if(imageValid == true)
    {
        if(FFT->isChecked() == true) // Do the FFT on FITS data and display it
        {
            if(fft_done == false)
            {
                calc_fft();
            }

            fft_switch();
            set_fft(true);
        }
        else // Display FITS data
        {
            displayed_Image = fits_Image;
            update();
            set_fft(false);
        }
    }
}

void FitsDisplay::read_fitsfile(void)
{
    bool ret(false);
    QString filename(QFileDialog::getOpenFileName(
        QString::null,"FITS (*.fits *.fit)", this));

    if(filename.isEmpty() == false)
    {
        if(filename.contains("//") > 0)
        {
            filename.replace(QRegExp("//"), "");
        }

        unsigned short w(0), h(0);
        set_wait_cursor();
        std::vector< unsigned short >* data(new std::vector< unsigned short >);
        std::string fname;
        fname = filename.latin1();

        ret = read_fits_file->read_file(fname, w, h, *data);
        if((ret == true) && (w > 0) && (h > 0))
        {
            imageValid = false;
            setCaption(QString("FITS display (compiled on " COMPILATION_DATE ", build " VERSION ") ").append(filename));
            make_rgb(w, h, *data);
        }

        delete data;
        unset_wait_cursor();
    }
}

//Display exposure statistics in statusbar
void FitsDisplay::print_stats(void)
{
    if(imageValid == true)
    {
        QString inf;
        pixel_stats stats = displayed_Image->get_pixel_stats();
        inf.sprintf("Statistics for image\n");
        emit(need_write_on_log_window(inf));
        inf = this->caption();
        inf.append(":\n");
        emit(need_write_on_log_window(inf));
        inf.sprintf("Min = %.0f (%d, %d)\n"
            "Max = %.0f (%d, %d)\n"
            "Mean = %.2f\n"
            "Median = %d\n"
            "Standard deviation = %.2f\n\n",
            stats.minimum, stats.min_x, stats.min_y, stats.maximum, stats.max_x,
            stats.max_y, stats.mean, stats.median, stats.stddev);
        emit(need_write_on_log_window(inf));
    }
}

//Search the brightest pixel
void FitsDisplay::find_maximum(const int x, const int y, const int w, const int h)
{
    if(imageValid == true)
    {
        int width(static_cast<int>(displayed_Image->get_img_width()));
        int max_x(0), max_y(0), counts(0), max_counts(-1);
        for(int row(y + h - 1); row >= y; --row)
        {
            for(int column(x + w - 1); column >= x; --column)
            {
                counts = displayed_Image->get_data(column + row * width);
                if(counts >= max_counts)
                {
                    max_counts = counts;
                    max_x = column;
                    max_y = row;
                }
            }
        }
        my_guide.x = max_x;
        my_guide.y = max_y;
        cursor_box.x = x;
        cursor_box.y = y;
        cursor_box.w = w;
        cursor_box.h = h;
        set_guiders();
    }
}

//Setup the area around the brightest pixel and calculate a Gaussian fit
void FitsDisplay::set_guiders(void)
{
    bool gauss(false);

    GaussFit* fit(new GaussFit(displayed_Image->get_data_array()));

    switch(fit->do_it(GuessSkyCheckBox->isChecked(),
        displayed_Image->get_img_width(), cursor_box, my_guide))
    {
    case GAUSS_NO_ERROR:
    {
        if((focal_length > 0) && (pixel_width > 0) && (pixel_height > 0))
        {
            // pixel_width, pixel_height and pixel_square are in �m, focal_length is in mm.
            my_guide.fwhm_arcsec = std::atan2(
                my_guide.fwhm * pixel_square * 10E-6, focal_length * 10E-3);
            // The FWHM in pixels is for sure less than an arc degree (FWHM<1�), therefore calculate the arc seconds as follows.
            my_guide.fwhm_arcsec *= (180.0 * 3600.0 / M_PI);
        }
        else
        {
            my_guide.fwhm_arcsec = -1.;
        }
        my_guide.valid = true;
        my_guide.inf.sprintf("Position = [%d, %d]\n"
            "Box: X = %d, Y = %d, W = %d, H = %d\n"
            "Fitdiameter = %d pixel\n"
            "Sky = %g\n"
            "Peak = %.2f\n"
            "Pixel width = %.2f�m\n"
            "Pixel height = %.2f�m\n"
            "Focal length = %dmm\n"
            "FWHM = %.2f pixel\n"
            "FWHM = %.2f\"\n"
            "X-Cent. = %.2f, Width = %.2f\n"
            "Y-Cent. = %.2f, Width = %.2f\n"
            "Eccentricity = %.2f\n"
            "Major axis = %.2f\n"
            "Minor axis = %.2f\n\n",
            my_guide.x, my_guide.y, cursor_box.x, cursor_box.y, cursor_box.w,
            cursor_box.h, my_guide.fitrad, my_guide.sky, my_guide.peak, pixel_width,
            pixel_height, focal_length, my_guide.fwhm, my_guide.fwhm_arcsec,
            my_guide.col_cen, my_guide.col_width, my_guide.row_cen,
            my_guide.row_width, my_guide.eccen, my_guide.major_axis,
            my_guide.minor_axis);
        gauss = true;
    }
        break;

    default:
    {
        emit(need_write_on_log_window("ERROR: Class GaussFit returns with error, description follows.\n"));
    }

    case GAUSS_NO_MEM_X:
    {
        emit(need_write_on_log_window("ERROR do_axes: can't allocate for x vector\n"));
    }
        break;

    case GAUSS_NO_MEM_Y:
    {
        emit(need_write_on_log_window("ERROR do_axes: can't allocate for y vector\n"));
    }
        break;

    case GAUSS_NO_MEM_Z:
    {
        emit(need_write_on_log_window("ERROR do_axes: can't allocate for z vector\n"));
    }
        break;

    case GAUSS_ROW_OUT_OF_BOUNDS:
    {
        emit(need_write_on_log_window("ERROR: computed row centroid is out of bounds\n"));
    }
        break;

    case GAUSS_COL_OUT_OF_BOUNDS:
    {
        emit(need_write_on_log_window("ERROR: computed column centroid is out of bounds\n"));
    }
        break;

    case GAUSS_CIRC_GAUSS_ERROR:
    {
        emit(need_write_on_log_window("ERROR: circular_gaussian returns with error\n"));
    }
        break;

    case GAUSS_CIRC_GAUSS_NO_MEM_X:
    {
        emit(need_write_on_log_window("ERROR: circular_gaussian: can't allocate for x vector\n"));
    }
        break;

    case GAUSS_CIRC_GAUSS_NO_MEM_Y:
    {
        emit(need_write_on_log_window("ERROR: circular_gaussian: can't allocate for y vector\n"));
    }
        break;

    case GAUSS_CIRC_GAUSS_NO_MEM_SIG:
    {
        emit(need_write_on_log_window("ERROR: circular_gaussian: can't allocate for sig vector\n"));
    }
        break;

    case GAUSS_NOT_ENOUGH_POINTS:
    {
        emit(need_write_on_log_window("ERROR: gauss_fit: passed fewer than three data points\n"));
    }
        break;
    }

    if(gauss == false)
    {
        my_guide.valid = false;
        my_guide.inf = "ERROR:\n Cannot determine Gauss!";
    }

    delete fit;

    StatsBox->append(my_guide.inf);
    draw_guide();
    zoom(my_guide.x, my_guide.y);
}

void FitsDisplay::setZoomFactor(int factor)
{
    zoom_factor = factor;
}

void FitsDisplay::write_on_log_window(const QString& log)
{
    StatsBox->append(log);
}

void FitsDisplay::write_on_log_window(const std::string& log)
{
    emit(need_write_on_log_window(QString(log.c_str())));
}

void FitsDisplay::write_on_log_window(const char* log)
{
    emit(need_write_on_log_window(QString(log)));
}

void FitsDisplay::write_fitsfile(void)
{
    QString filename(QFileDialog::getSaveFileName(QString::null,"FITS (*.fits *.fit)",this));
    if(filename.isEmpty() == false)
    {
        QString dummy_s;
        if(filename.contains("//") > 0)
        {
            filename.replace(QRegExp("//"), "");
        }
        fits_c header;
        emit(need_get_header(header));
        writefitsfile->write_fits_file(filename.latin1(),
            header, &(displayed_Image->get_data_array().front()));
    }
}

/***********************************************************
    FFT etc.
    **********************************************************/
void FitsDisplay::calc_fft(void)
{
    if(FFT->isChecked() == false)
    {
        return;
    }

    set_wait_cursor();

    const unsigned int img_size(displayed_Image->get_img_size());
    const unsigned short img_width(displayed_Image->get_img_width());
    const unsigned short img_height(displayed_Image->get_img_height());
    float min_real(MAX_DATA_VAL),
        min_imag(MAX_DATA_VAL),
        min_ampl(MAX_DATA_VAL),
        min_phase(MAX_DATA_VAL),
        max_real(MIN_DATA_VAL),
        max_imag(MIN_DATA_VAL),
        max_ampl(MIN_DATA_VAL),
        max_phase(MIN_DATA_VAL);

    std::vector<double>* real_data(new std::vector<double>(img_size));
    std::vector<double>* imaginary_data(new std::vector<double>(img_size));
    std::vector<double>* amplitude_data(new std::vector<double>(img_size));
    std::vector<double>* phase_data(new std::vector<double>(img_size));
    std::vector<unsigned short>* data(new std::vector<unsigned short>(img_size));

    if((real_data->size() + imaginary_data->size() + amplitude_data->size()
        + phase_data->size() + data->size()) != 5 * img_size)
    {
        std::cerr << "ERROR calc_fft: not enough memory for arrays."
            << std::endl;
        delete real_data;
        delete imaginary_data;
        delete amplitude_data;
        delete phase_data;
        delete data;
        return;
    }

    std::vector< std::complex< double > >* in(
        new std::vector< std::complex< double > >(img_size));

    std::vector< std::complex< double > >* out(
        new std::vector< std::complex< double > >(img_size));

    if((in->size() + out->size()) != (2 * img_size))
    {
        std::cerr<<"Not enough memory for FFT arrays."<<std::endl;
        delete in;
        delete out;
        delete real_data;
        delete imaginary_data;
        delete amplitude_data;
        delete phase_data;
        delete data;
        return;
    }

    const bool center(CenterCheckBox->isChecked());
    if(center == true)
    {
        int coordinate;
        for(int y(img_height - 1); y >= 0; --y)
        {
            for(int x(img_width - 1); x >= 0; --x)
            {
                coordinate = y * img_width + x;
                in->at(coordinate) = std::pow(-1.0, x + y)
                    * (displayed_Image->get_data(coordinate));
            }
        }
    }
    else
    {
        for(int i(img_size - 1); i >= 0; --i)
        {
            in->at(i) = displayed_Image->get_data(i);
        }
    }

    fftw_plan plan(fftw_plan_dft_2d(
        static_cast< int >(img_width),
        static_cast< int >(img_height),
        reinterpret_cast< fftw_complex* >(&((*in)[0])),
        reinterpret_cast< fftw_complex* >(&((*out)[0])),
        FFTW_FORWARD, FFTW_MEASURE));

    fftw_execute(plan);
    fftw_destroy_plan(plan);

    float val(0.0);
    for(int i(img_size - 1); i >= 0; --i)
    {
        real_data->at(i) = (out->at(i)).real();
        imaginary_data->at(i) = (out->at(i)).imag();
        amplitude_data->at(i) = std::abs(out->at(i));
        phase_data->at(i) = std::arg(out->at(i));

        val = static_cast< float >(real_data->at(i));
        if(val < min_real)
        {
            min_real = val;
        }
        else if(val > max_real)
        {
            max_real = val;
        }

        val = static_cast< float >(imaginary_data->at(i));
        if(val < min_imag)
        {
            min_imag = val;
        }
        else if(val > max_imag)
        {
            max_imag = val;
        }

        val = static_cast< float >(amplitude_data->at(i));
        if(val < min_ampl)
        {
            min_ampl = val;
        }
        else if(val > max_ampl)
        {
            max_ampl = val;
        }

        val = static_cast< float >(phase_data->at(i));
        if(val < min_phase)
        {
            min_phase = val;
        }
        else if(val > max_phase)
        {
            max_phase = val;
        }
    }

    delete in;
    delete out;

    fft_done = true;
    // End of FFT calculation

    val = 0.0;
    for(int i(img_size - 1); i >= 0; --i)
    {
        val = real_data->at(i);
        if(min_real < 0) // Prescale the values and replace pixel coordinate 0,0 to center of image.
        {
            data->at(i) = static_cast< unsigned short >(
                (std::abs(min_real) + val) / max_real * static_cast< float >(MAX_DATA_VAL));
        }
        else
        {
            data->at(i) = static_cast< unsigned short >(
                (val - min_real) / max_real * static_cast< float >(MAX_DATA_VAL));
        }
    }
    real_Image = new Image(
        img_width,
        img_height,
        Image_Frame->width(),
        Image_Frame->height(),
        *data,
        Image_Frame->mapToParent(Image_Frame->pos()),
        LuminositySlider->value(),
        ContrastSlider->value());
    delete real_data;

    for(int i(img_size - 1); i >= 0; --i)
    {
        val = imaginary_data->at(i);
        if(min_imag < 0)
        {
            // Prescale the values and replace pixel coordinate 0,0 to center of image.
            data->at(i) = static_cast< unsigned short >(
                (std::abs(min_imag) + val) / max_imag
                    * static_cast< float >(MAX_DATA_VAL));
        }
        else
        {
            data->at(i) = static_cast< unsigned short >(
                (val-min_imag) / max_imag
                    * static_cast< float >(MAX_DATA_VAL));
        }
    }

    imaginary_Image = new Image(
        img_width,
        img_height,
        Image_Frame->width(),
        Image_Frame->height(),
        *data,
        Image_Frame->mapToParent(Image_Frame->pos()),
        LuminositySlider->value(),
        ContrastSlider->value());
    delete imaginary_data;

    for(int i(img_size - 1); i >= 0; --i)
    {
        val=amplitude_data->at(i);
        if(min_ampl<0)
        {
            // Prescale the values and replace pixel coordinate 0,0 to center of image.
            data->at(i) = static_cast< unsigned short >(
                (std::abs(min_ampl) + val) / max_ampl
                    * static_cast< float >(MAX_DATA_VAL));
        }
        else
        {
            data->at(i) = static_cast< unsigned short >(
                (val - min_ampl) / max_ampl
                    * static_cast< float >(MAX_DATA_VAL));
        }
    }
    amplitude_Image = new Image(
    img_width,
    img_height,
    Image_Frame->width(),
    Image_Frame->height(),
    *data,
    Image_Frame->mapToParent(Image_Frame->pos()),
    LuminositySlider->value(),
    ContrastSlider->value());
    delete amplitude_data;

    for(int i(img_size - 1); i >= 0; --i)
    {
        if(min_phase<0)
        {
            // Prescale the values and replace pixel coordinate 0,0 to center of image.
            data->at(i) = static_cast< unsigned short >(
                (std::abs(min_phase) + val) / max_phase
                    * static_cast< float >(MAX_DATA_VAL));
        }
        else
        {
            data->at(i) = static_cast< unsigned short >(
                (val - min_phase) / max_phase
                    * static_cast< float >(MAX_DATA_VAL));
        }
    }
    phase_Image = new Image(
    img_width,
    img_height,
    Image_Frame->width(),
    Image_Frame->height(),
    *data,
    Image_Frame->mapToParent(Image_Frame->pos()),
    LuminositySlider->value(),
    ContrastSlider->value());
    delete phase_data;
    delete data;

    unset_wait_cursor();
}

void FitsDisplay::delete_fft(void)
{
    fft_done = false;

    delete real_Image;
    real_Image=0;

    delete imaginary_Image;
    imaginary_Image=0;

    delete amplitude_Image;
    amplitude_Image=0;

    delete phase_Image;
    phase_Image=0;

    FFT->setChecked(false);
    set_fft(false);
}

/***********************************************************
    Displaying etc.
    **********************************************************/
void FitsDisplay::paintEvent( QPaintEvent * )
{
    if(imageValid == true)
    {
        show_data_pixels();
        draw_guide();
    }
}

void FitsDisplay::mousePressEvent(QMouseEvent* e)
{
    if(imageValid == true)
    {
        int x(e->x()), y(e->y()), button(e->button());
        display_pixel_coordinates(x,y);
        repaint();
        displayed_Image->remap_coordinates(x, y, TO_IMAGE);
        if((button == Qt::LeftButton)
        && (x < displayed_Image->get_img_width())
        &&(y < displayed_Image->get_img_height())
        &&(x >= 0) && (y >= 0))
        {
            p1 = QPoint(x,y);
            p2 = p1;
            p3 = p1;
            button_pressed = true;
            Image_Frame->setCursor(Qt::SizeFDiagCursor);
        }
    }
}

void FitsDisplay::mouseMoveEvent(QMouseEvent* e)
{
    if(imageValid == true)
    {
        int _x(e->x()), _y(e->y());
        display_pixel_coordinates(_x, _y);
        if((button_pressed == true)
        && (_x < displayed_Image->get_img_width())
        && (_y < displayed_Image->get_img_height())
        && (_x >= 0) && (_y >= 0))
        {
            repaint();
            displayed_Image->remap_coordinates(_x, _y, TO_IMAGE);
            p2 = QPoint(_x, _y);
            QPainter p;
            p.begin(Image_Frame);
            p.setPen(Qt::red);
            QRect rect(QRect(p1,p2).normalize());
            int x(rect.x()), y(rect.y()), w(rect.width()), h(rect.height());
            displayed_Image->remap_coordinates(x, y, w, h, FROM_IMAGE);
            p.drawRect(x, y, w, h);
            p3 = p2;
            p.end();
        }
    }
}

void FitsDisplay::mouseReleaseEvent(QMouseEvent* e)
{
    if(imageValid == true)
    {
        int _x(e->x()), _y(e->y()), button(e->button());
        show_data_pixels();
        //    repaint();
        if((button_pressed == true) && (button==Qt::LeftButton))
        {
            int x(0), y(0), w(0), h(0);
            display_pixel_coordinates(_x, _y);
            QRect(p1, p2).normalize().rect(&x, &y, &w, &h);
            if((x >= BOX_MIN / 2)
            &&((x + BOX_MIN / 2) < displayed_Image->get_img_width())
            &&(y >= BOX_MIN / 2)
            &&((y + BOX_MIN / 2) < displayed_Image->get_img_height())
            &&(x > w / 2)
            &&((x + w / 2) <= displayed_Image->get_img_width())
            &&(y > h / 2)
            &&((y + h / 2) <= displayed_Image->get_img_height()))
            {
                find_maximum(x, y, w, h);
            }
            button_pressed = false;
            Image_Frame->setCursor(Qt::CrossCursor);
        }
        draw_guide();
    }
}

void FitsDisplay::reset(void)
{
    imageValid = false;

    delete fits_Image;
    fits_Image = 0;

    displayed_Image=0;

    delete Pixmap;
    Pixmap = 0;

    my_guide.x = my_guide.y =
        cursor_box.x = cursor_box.y = cursor_box.w = cursor_box.h = 0;
    button_pressed = my_guide.valid = false;

    LogImageCheckBox->setChecked(false);
    InvertImageCheckBox->setChecked(false);
    delete_fft();
}

/*
    New image arrived or we had to reset the whole stuff: Resize the displayed
    image and reset the images's colortable.
*/
void FitsDisplay::create(const USHORT w,
    const USHORT h,
    std::vector< USHORT >& data)
{
    delete fits_Image;

    fits_Image = new Image(
        w,
        h,
        Image_Frame->width(),
        Image_Frame->height(),
        data,
        Image_Frame->mapToParent(Image_Frame->pos()),
        LuminositySlider->value(),
        ContrastSlider->value());
    Pixmap = new QPixmap(Image_Frame->width(), Image_Frame->height());
    QPainter p;
    p.begin(Image_Frame);
    p.eraseRect(Image_Frame->frameRect());
    p.end();
}

void FitsDisplay::make_rgb(const USHORT w, const USHORT h,
    std::vector< USHORT >& data)
{
    delete_fft();
    create(w, h, data);
    displayed_Image = fits_Image;
    imageValid = true;
    update();
    print_stats();
}

void FitsDisplay::draw_guide(void)
{
    if(my_guide.valid == true)
    {
        box cb(cursor_box);
        QPainter p(Image_Frame);
        p.setPen(Qt::red);
        displayed_Image->remap_coordinates(cb, FROM_IMAGE);
        p.drawRect(cb.x, cb.y, cb.w, cb.h);
        cb.x = static_cast< unsigned int >(my_guide.x);
        cb.y = static_cast< unsigned int >(my_guide.y);
        cb.w = static_cast< unsigned int >(my_guide.col_width);
        cb.h = static_cast< unsigned int >(my_guide.row_width);
        if(cb.w < 5)
        {
            cb.w = 5;
        }

        if(cb.h < 5)
        {
            cb.h = 5;
        }

        displayed_Image->remap_coordinates(cb, FROM_IMAGE);
        p.drawEllipse(cb.x - cb.w / 2, cb.y - cb.h / 2, cb.w, cb.h);
        p.end();
    }
}

void FitsDisplay::show_data_pixels(void)
{
    if(imageValid == true)
    {
        Pixmap->convertFromImage(displayed_Image->get_downscaled_Image());
        QPainter p;
        p.begin(Image_Frame);
        p.drawPixmap(0, 0, *Pixmap);
        p.end();
    }
}

void FitsDisplay::display_pixel_coordinates(int& _x,int& _y)
{
    if(imageValid == true)
    {
        int x(_x), y(_y);

        displayed_Image->remap_coordinates(x, y, TO_IMAGE);
        if((x >= 0)
        &&(x < displayed_Image->get_img_width())
        &&(y > 0)
        &&(y < displayed_Image->get_img_height()))
        {
            X->display(x);
            Y->display(y);
            Value->display(displayed_Image->get_data(x,y));
            zoom(x, y);
        }
    }
}

void FitsDisplay::zoom(const int _x, const int _y)
{
    if(imageValid == true)
    {
        int x(_x), y(_y);
        int w_half(zoom_width / zoom_factor / 2); //w_half=half width of image section which is to be displayed
        int h_half(zoom_width / zoom_factor / 2); //h_half=half height of image section which is to be displayed

        x -= w_half;
        y -= h_half;

        if(x < 0)
        {
            x = 0;
        }
        else if(x > (displayed_Image->get_img_width() - 2 * w_half))
        {
            x = displayed_Image->get_img_width() - 2 * w_half;
        }

        if(y < 0)
        {
            y = 0;
        }
        else if(y > (displayed_Image->get_img_height() - 2 * h_half))
        {
            y = displayed_Image->get_img_height() - 2 * h_half;
        }

        QPainter p;
        p.begin(Zoom);
        p.scale(zoom_factor, zoom_factor);
        p.drawPixmap(0, 0, *(displayed_Image->get_original_Pixmap()),
            x, y, w_half * 2, h_half * 2);
        p.end();
        p.begin(Zoom);
        p.setPen(Qt::red);
        p.drawRect(
            QRect(
                zoom_width / 2 - zoom_box_size / 2 * zoom_factor / 3,
                zoom_height / 2 - zoom_box_size / 2 * zoom_factor / 3,
                zoom_box_size * zoom_factor / 3,
                zoom_box_size * zoom_factor / 3));
        p.end();
    }
}


void FitsDisplay::InvertImage(void)
{
    if(imageValid == true)
    {
        displayed_Image->Invert();
        update();
    }
}

void FitsDisplay::LogImage(void)
{
    if(imageValid == true)
    {
        if(LogImageCheckBox->isChecked() == true)
        {
            displayed_Image->Logarithmic();
        }
        else
        {
            displayed_Image->Linear();
        }
    }
}

void FitsDisplay::set_wait_cursor(void)
{
    setCursor(Qt::WaitCursor);
    Image_Frame->setCursor(Qt::WaitCursor);
}

void FitsDisplay::unset_wait_cursor(void)
{
    setCursor(Qt::ArrowCursor);
    Image_Frame->setCursor(Qt::CrossCursor);
}

void FitsDisplay::set_fft(const bool on_off)
{
    Real->setEnabled(on_off);
    Imag->setEnabled(on_off);
    Phase->setEnabled(on_off);
    Amplitude->setEnabled(on_off);
}

void FitsDisplay::clear_log(void)
{
    StatsBox->setText("");
}

void FitsDisplay::SetFWHMBoxWidth(const int)
{
}

void FitsDisplay::acquireFromOptTel(void)
{
    std::cout << "1" << std::endl;

    if(CORBA::is_nil(frameGrabber.in()) == false)
    {
        std::cout << "2" << std::endl;

        ACSErr::Completion_var completion(new ACSErr::Completion);
        if(frameGrabber->currentStatus()->get_sync(completion.out())
        != Control::READY)
        {
            std::cout << "Camera is not in READY status: "
                << frameGrabber->currentStatus()->get_sync(completion.out())
                << std::endl;

            return;
        }

        Control::Exposure_var exposureStruct(new Control::Exposure);
        std::cout << "3" << std::endl;

        try
        {
            exposureStruct = frameGrabber->getExposure();
        }
        catch(const ControlExceptions::DeviceBusyEx& ex)
        {
            std::cout << "ControlExceptions::DeviceBusyEx" << std::endl;
            return;
        }
        catch(const FrameGrabberExceptions::InvalidExposureEx& ex)
        {
            std::cout << "FrameGrabberExceptions::InvalidExposureEx" << std::endl;
            return;
        }
        catch(const ControlDeviceExceptions::HwLifecycleEx& ex)
        {
            std::cout << "ControlDeviceExceptions::HwLifecycleEx" << std::endl;
            return;
        }
        catch(...)
        {
            std::cout << "General exception!" << std::endl;
            return;
        }

        set_wait_cursor();
        unsigned short w(static_cast< unsigned short >(exposureStruct->width)),
            h(static_cast< unsigned short >(exposureStruct->height));

        if(static_cast< unsigned long >(exposureStruct->data.length())
        != static_cast< unsigned long >((w * h)))
        {
            std::cout << "ERROR! width = "
                << w
                << ", height = "
                << h
                << ", array size = "
                << exposureStruct->data.length()
                << std::endl;
            unset_wait_cursor();
            return;
        }

        if((w > 0U) && (h > 0U))
        {
            imageValid = false;
            setCaption(QString("FITS display (compiled on " COMPILATION_DATE ", build " VERSION ") ").append("OPTTEL"));
            const unsigned long size(w * h - 1UL);
            std::vector< unsigned short >* data(
                new std::vector< unsigned short >);
            data->resize(size + 1UL);
            for(unsigned long i(0); i <= size; ++i)
            {
                (*data)[i] = static_cast< unsigned short >(
                    std::floor(exposureStruct->data[i] + 0.5));
            }

            make_rgb(w, h, *data);
            delete data;
        }

        unset_wait_cursor();
    }
}

void FitsDisplay::selectFrameGrabber(const QString& chosenFrameGrabber)
{
    const std::string selectedName(chosenFrameGrabber.latin1());

    if(CORBA::is_nil(frameGrabber.in()) == false)
    {
        CORBA::String_var tmpName(frameGrabber->name());
        const std::string currentName(tmpName);

        if(currentName != selectedName)
        {
            sc->releaseComponent(currentName.c_str());
            frameGrabber = Control::FrameGrabber::_nil();
        }
    }

    if(selectedName == "None selected")
    {
        return;
    }
    else
    {
        try
        {
            frameGrabber = sc->getComponent< Control::FrameGrabber >(
                selectedName.c_str(), 0, true);
        }
        catch(const maciErrType::CannotGetComponentExImpl& ex)
        {
            std::cout << "Could not activate "
            << selectedName
            << std::endl;

            frameGrabber = Control::FrameGrabber::_nil();
            FrameGrabberComponents->setCurrentItem(0);
            return;
        }
    }
}

void FitsDisplay::setSimpleClient(maci::SimpleClient* simpleClient)
{
    sc = simpleClient;
}
