/*##*************************************
##
## E.S.O. - VLT project
##
## "@(#) $Id$" 
##
## bitmap.h"
## 
## who      when     what
##-------- -------- ------------------
##mnastvog 06/27/95 created for cmm
##*/
/* file: bitmap.h
** SCCSid: @(#)bitmap.h 1.4 11/11/91
** Author: Jeffrey W Percival (jwp@sal.wisc.edu)
** Space Astronomy Laboratory, University of Wisconsin
** Do not remove or alter the lines above.
**********************************************************************
** header file for bitmap
**********************************************************************
*/

#ifndef BITMAP_INCLUDE
#define BITMAP_INCLUDE

/* the quantum of allocation for the bitmap */
typedef long map_t;

/* the type of bit numbers */
typedef long bit_t;

#endif
