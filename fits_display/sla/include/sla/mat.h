/*##*************************************
##
## E.S.O. - VLT project
##
## "@(#) $Id$" 
##
## mat.h"
## 
## who      when     what
##-------- -------- ------------------
##mnastvog 06/27/95 created for cmm
##*/
/* file: mat.h
** sccs: @(#)mat.h UW-SAL 1.1 (10/7/93)
** Copyright 1992,1993 University of Wisconsin
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/

#ifndef MAT_INCLUDE
#define MAT_INCLUDE

/*
**********************************************************************
** these are the old matrix types,
** preserved for backward compatibility.
** use m[36]*.c for the new stuff.
**********************************************************************
*/

struct s_matrix {
	double m[3][3];
};

/* some matrix macros */

#define matDecXX(f,x)		(f.m[0][0] -= (x))
#define matDecXY(f,x)		(f.m[0][1] -= (x))
#define matDecXZ(f,x)		(f.m[0][2] -= (x))
#define matDecYX(f,x)		(f.m[1][0] -= (x))
#define matDecYY(f,x)		(f.m[1][1] -= (x))
#define matDecYZ(f,x)		(f.m[1][2] -= (x))
#define matDecZX(f,x)		(f.m[2][0] -= (x))
#define matDecZY(f,x)		(f.m[2][1] -= (x))
#define matDecZZ(f,x)		(f.m[2][2] -= (x))

#define matDivXX(f,x)		(f.m[0][0] /= (x))
#define matDivXY(f,x)		(f.m[0][1] /= (x))
#define matDivXZ(f,x)		(f.m[0][2] /= (x))
#define matDivYX(f,x)		(f.m[1][0] /= (x))
#define matDivYY(f,x)		(f.m[1][1] /= (x))
#define matDivYZ(f,x)		(f.m[1][2] /= (x))
#define matDivZX(f,x)		(f.m[2][0] /= (x))
#define matDivZY(f,x)		(f.m[2][1] /= (x))
#define matDivZZ(f,x)		(f.m[2][2] /= (x))

#define matGetXX(f)		(f.m[0][0])
#define matGetXY(f)		(f.m[0][1])
#define matGetXZ(f)		(f.m[0][2])
#define matGetYX(f)		(f.m[1][0])
#define matGetYY(f)		(f.m[1][1])
#define matGetYZ(f)		(f.m[1][2])
#define matGetZX(f)		(f.m[2][0])
#define matGetZY(f)		(f.m[2][1])
#define matGetZZ(f)		(f.m[2][2])

#define matIncXX(f,x)		(f.m[0][0] += (x))
#define matIncXY(f,x)		(f.m[0][1] += (x))
#define matIncXZ(f,x)		(f.m[0][2] += (x))
#define matIncYX(f,x)		(f.m[1][0] += (x))
#define matIncYY(f,x)		(f.m[1][1] += (x))
#define matIncYZ(f,x)		(f.m[1][2] += (x))
#define matIncZX(f,x)		(f.m[2][0] += (x))
#define matIncZY(f,x)		(f.m[2][1] += (x))
#define matIncZZ(f,x)		(f.m[2][2] += (x))

#define matMulXX(f,x)		(f.m[0][0] *= (x))
#define matMulXY(f,x)		(f.m[0][1] *= (x))
#define matMulXZ(f,x)		(f.m[0][2] *= (x))
#define matMulYX(f,x)		(f.m[1][0] *= (x))
#define matMulYY(f,x)		(f.m[1][1] *= (x))
#define matMulYZ(f,x)		(f.m[1][2] *= (x))
#define matMulZX(f,x)		(f.m[2][0] *= (x))
#define matMulZY(f,x)		(f.m[2][1] *= (x))
#define matMulZZ(f,x)		(f.m[2][2] *= (x))

#define matSetXX(f,x)		(f.m[0][0] = (x))
#define matSetXY(f,x)		(f.m[0][1] = (x))
#define matSetXZ(f,x)		(f.m[0][2] = (x))
#define matSetYX(f,x)		(f.m[1][0] = (x))
#define matSetYY(f,x)		(f.m[1][1] = (x))
#define matSetYZ(f,x)		(f.m[1][2] = (x))
#define matSetZX(f,x)		(f.m[2][0] = (x))
#define matSetZY(f,x)		(f.m[2][1] = (x))
#define matSetZZ(f,x)		(f.m[2][2] = (x))

/* EXTERN_START */
/* EXTERN_STOP */

#endif
