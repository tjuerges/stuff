/*##*************************************
##
## E.S.O. - VLT project
##
## "@(#) $Id$" 
##
## contour.h"
## 
## who      when     what
##-------- -------- ------------------
##mnastvog 06/27/95 created for cmm
##*/
/* file: contour.h
** SCCSid: @(#)contour.h 1.10 11/11/91
** Author: Jeffrey W Percival (jwp@sal.wisc.edu)
** Space Astronomy Laboratory, University of Wisconsin
** Do not remove or alter the lines above.
**********************************************************************
** header file for contour programs
**********************************************************************
*/

#ifndef CONTOUR_INCLUDE
#define CONTOUR_INCLUDE

#define NROWS (200)
#define NCOLS (200)

#define NORTH	(0)
#define EAST	(1)
#define SOUTH	(2)
#define WEST	(3)

/* number of labels allowed */
#define NLABELS (32)
/* max length of each label */
#define LABLEN	(64)

#endif
