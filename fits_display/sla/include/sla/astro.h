/*##*************************************
##
## E.S.O. - VLT project
##
## "@(#) $Id$" 
##
## astro.h"
## 
## who      when     what
##-------- -------- ------------------
##mnastvog 06/27/95 created for cmm
##
*/
/* file: astro.h
** SCCSid: @(#)astro.h 1.2 3/8/94
** Author: Jeffrey W Percival (jwp@sal.wisc.edu)
** Space Astronomy Laboratory, University of Wisconsin
** Do not remove or alter the lines above.
**********************************************************************
** header file for celestial mechanics software
**********************************************************************
*/

#ifndef ASTRO_INCLUDE
#define ASTRO_INCLUDE

#include "misc.h"
#include "vec.h"
#include "times.h"

#include "pmachine.h"

/*
**********************************************************************
** IAU (1976) System of Astronomical Constants
** from the Astronomical Almanac, 1984 p. K6
** (SI units, MKS)
**********************************************************************
*/

/* Gaussian gravitational constant */
#define IAU_K	(0.01720209895)

/* distance of moon */
#define IAU_DM	(384400.0e3)

/* astronomical unit */
#define IAU_AU	(1.49597870e11)

/* speed of light */
#define IAU_C	(299792458.0)

/* radius of earth */
#define IAU_RE	(6378137.0)

/* radius of moon */
#define IAU_RM	(1738000.0)

/* flattening factor of earth */
#define IAU_F	(0.00335281)

/* constant of aberration */
#define IAU_KAPPA	(20.49552)

/* rotational velocity of the earth in radians/s */
#define IAU_W	(7.2921151467e-5)

/* this structure defines a star datum */
struct s_star {
	double a;	/* right ascension */
	double d;	/* declination */
	double m;	/* magnitude */
};
/* this structure defines a line segment between two stars */
struct s_cons {
	double a1;	/* right ascension */
	double d1;	/* declination */
	double a2;	/* right ascension */
	double d2;	/* declination */
};

/* some simple transformations implemented as macros */

#define precess(t1,t2,v)	(m3v6(precess_m((t1),(t2)), (v)))
#define nutate(t,v)		(m3v6(nutate_m(t), (v)))
#define denutate(t,v)		(m3v6(m3inv(nutate_m(t)), (v)))

/* special 3-vector versions */
#define v3precess(t1,t2,v)	(m3v3(precess_m((t1),(t2)), (v)))
#define v3nutate(t,v)		(m3v3(nutate_m(t), (v)))
#define v3denutate(t,v)		(m3v3(m3inv(nutate_m(t)), (v)))

#define ecl2equ(v,obl)		(m3v6(m3Rx(-(obl)), (v)))
#define equ2ecl(v,obl)		(m3v6(m3Rx((obl)), (v)))

/* definitive time transformations */
#define et2tdt(et)	(et)
#define tai2tdt(tai)	((tai)+(32.184/86400))
#define tdt2et(tdt)	(tdt)
#define ut12et(ut1)	((ut1)+(delta_T(ut1)/86400))
#define utc2et(utc)	((utc)+(delta_ET(utc)/86400))
#define utc2tai(utc)	((utc)+(delta_AT(utc)/86400))
#define utc2tdt(utc)	((utc)+(delta_TT(utc)/86400))
#define utc2ut1(utc)	((utc)+(delta_UT(utc)/86400))

/* approximate time transformations */
#define et2ut1(et)	((et)-(delta_T(et)/86400))
#define et2utc(et)	((et)-(delta_ET(et)/86400))
#define tai2utc(tai)	((tai)-(delta_AT(tai)/86400))
#define tdt2tai(tdt)	((tdt)-(32.184/86400))
#define tdt2utc(tdt)	((tdt)-(delta_TT(tdt)/86400))
#define ut12utc(ut1)	((ut1)-(delta_UT(ut1)/86400))

/* derived time transformations */
#define et2tai(et)	(tdt2tai(et2tdt(et)))
#define et2tdb(et)	(tdt2tdb(et2tdt(et)))
#define tai2et(tai)	(tdt2et(tai2tdt(tai)))
#define tai2tdb(tai)	(tdt2tdb(tai2tdt(tai)))
#define tai2ut1(tai)	(et2ut1(tdt2et((tai2tdt(tai)))))
#define tdb2et(tdb)	(tdt2et(tdb2tdt(tdb)))
#define tdb2tai(tdb)	(tdt2tai(tdb2tdt(tdb)))
#define tdb2ut1(tdb)	(et2ut1(tdt2et(tdb2tdt(tdb))))
#define tdb2utc(tdb)	(tai2utc(tdt2tai(tdb2tdt(tdb))))
#define tdt2ut1(tdt)	(et2ut1(tdt2et(tdt)))
#define ut12tai(ut1)	(tdt2tai(et2tdt(ut12et(ut1))))
#define ut12tdb(ut1)	(tdt2tdb(et2tdt(ut12et(ut1))))
#define ut12tdt(ut1)	(et2tdt(ut12et(ut1)))
#define utc2tdb(utc)	(tdt2tdb(tai2tdt(utc2tai(utc))))

/* convenience time transformations */
#define et2ut(ut)	(et2ut1(et))
#define ut2et(ut)	(ut12et(ut))
#define ut2gmst(ut)	(ut12gmst(ut))

/* EXTERN_START */
extern char *pstate();
extern double E2v();
extern double M2E();
extern double ae2ha();
extern double ae2pa();
extern double delta_AT();
extern double delta_ET();
extern double delta_T();
extern double delta_TT();
extern double delta_UT();
extern double eccentricity();
extern double eq_equinox();
extern double eq_time();
extern double func();
extern double nut_longitude();
extern double nut_obliquity();
extern double obj2lha();
extern double obliquity();
extern double refract();
extern double refraction();
extern double solar_perigee();
extern double tdb2tdt();
extern double tdt2tdb();
extern double theta4();
extern double theta4dot();
extern double theta5();
extern double theta5dot();
extern double ut12gmst();
extern double zd2airmass();
extern double zee4();
extern double zee4dot();
extern double zee5();
extern double zee5dot();
extern double zeta4();
extern double zeta4dot();
extern double zeta5();
extern double zeta5dot();
extern int n_cons_data();
extern int n_star_data();
extern int pmachine();
extern struct s_m3 eplane();
extern struct s_m3 fplane();
extern struct s_m3 nutate_m();
extern struct s_m3 precess_m();
extern struct s_m6 Pn();
extern struct s_m6 Po();
extern struct s_v6 aberrate();
extern struct s_v6 altaz();
extern struct s_v6 azel2hadec();
extern struct s_v6 barvel();
extern struct s_v6 com2cof();
extern struct s_v6 ellab();
extern struct s_v6 equ2gal();
extern struct s_v6 fk425();
extern struct s_v6 fk524();
extern struct s_v6 gal2equ();
extern struct s_v6 geod2geoc();
extern struct s_v6 geoid();
extern struct s_v6 hadec2azel();
extern struct s_v6 hadec2radec();
extern struct s_v6 ldeflect();
extern struct s_v6 lonlat();
extern struct s_v6 observer();
extern struct s_v6 radec2hadec();
extern struct s_v6 v2r();
extern void atm();
extern void draw_saturn();
extern void geoc2geod();
extern void nutations();
extern void refco();
/* EXTERN_STOP */

#endif
