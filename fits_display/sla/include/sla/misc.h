/*##*************************************
##
## E.S.O. - VLT project
##
## "@(#) $Id$" 
##
## misc.h"
## 
## who      when     what
##-------- -------- ------------------
##mnastvog 06/27/95 created for cmm
##*/
/* file: misc.h
** sccs: @(#)misc.h UW-SAL 1.18 (11/30/93)
** Copyright 1992,1993 University of Wisconsin
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/

/*
**********************************************************************
** header file for miscellaneous routines
**********************************************************************
*/

#ifndef MISC_H
#define MISC_H

#include "std.h"
#include "amoeba.h"
#include "bitmap.h"
#include "contour.h"

#define REAL	(0)
#define IMAG	(1)

/* EXTERN_START */
extern bit_t bit_near();
extern double brent();
extern double c_distance();
extern double comb();
extern double fact();
extern double gaussian();
extern double golden();
extern double mag2size();
extern double polint();
extern double qromb();
extern double qsimp();
extern double qtrap();
extern double ratint();
extern double rtbis();
extern double rtflsp();
extern double rtsec();
extern double spline();
extern double trapzd();
extern double uniform();
extern double zbrent();
extern int amoeba();
extern int bit_clear();
extern int bit_init();
extern int bit_query();
extern int bit_set();
extern int c_contour();
extern int c_countedges();
extern int c_findedge();
extern int c_testedge();
extern int c_within();
extern int ctoi();
extern int makeargw();
extern int spline_prep();
extern int zbrac();
extern int zbrak();
extern void bit_status();
extern void c_clearedge();
extern void c_getxy();
extern void c_markedges();
extern void c_ps_epilog();
extern void c_ps_prolog();
extern void c_setedge();
extern void c_zapedges();
extern void convlv();
extern void cosft();
extern void dHeapDown();
extern void dHeapSort();
extern void dHeapUp();
extern void dft();
extern void fft();
extern void fresnel();
extern void heapsort();
extern void lHeapDown();
extern void lHeapSort();
extern void lHeapUp();
extern void lsnp();
extern void meanvar();
extern void mnbrak();
extern void realft();
extern void sinft();
extern void twofft();
/* EXTERN_STOP */

#endif
