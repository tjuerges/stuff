/*##*************************************
##
## E.S.O. - VLT project
##
## "@(#) $Id$" 
##
## std_vxworks.h"
## 
## who      when     what
##-------- -------- ------------------
##mnastvog 06/27/95 created for cmm
##*/
/* file: %M%
** sccs: %Z%%M% UW-SAL %I% (%G%)
** Copyright 1992,1993 University of Wisconsin
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/

/*
**********************************************************************
** this file does a standardized include of system header files.
**********************************************************************
*/

#ifndef STD_VXWORKS_H
#define STD_VXWORKS_H

/* These things make vxworks happy. */

#define CPU_FAMILY	MC680X0

#include "types.h"
#include "math.h"
#include "string.h"
#include "ctype.h"
#include "errno.h"
#include "stdlib.h"
#include "systime.h"
#include "vxWorks.h"
#include "sysLib.h"
#include "sigLib.h"
#include "stdioLib.h"
#include "ioLib.h"
#include "net/inet.h"
#include "taskLib.h"
#include "in.h"
#include "socket.h"
#include "iv.h"

/* from <time.h> */
struct	tm {
	int	tm_sec;
	int	tm_min;
	int	tm_hour;
	int	tm_mday;
	int	tm_mon;
	int	tm_year;
	int	tm_wday;
	int	tm_yday;
	int	tm_isdst;
	char	*tm_zone;
	long	tm_gmtoff;
};

/* from <netdb.h> */

struct	hostent {
	char	*h_name;	/* official name of host */
	char	**h_aliases;	/* alias list */
	int	h_addrtype;	/* host address type */
	int	h_length;	/* length of address */
	char	**h_addr_list;	/* list of addresses from name server */
#define	h_addr	h_addr_list[0]	/* address, for backward compatiblity */
};

/* now get rid of vx's use of OFFSET, which we want to use ourselves */
#undef OFFSET

#endif
