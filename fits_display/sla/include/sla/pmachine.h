/*##*************************************
##
## E.S.O. - VLT project
##
## "@(#) $Id$" 
##
## pmachine.h"
## 
## who      when     what
##-------- -------- ------------------
##mnastvog 06/27/95 created for cmm
##*/
/* file: pmachine.h
** sccs: @(#)pmachine.h UW-SAL 1.6 (1/2/94)
** Copyright 1992,1993 University of Wisconsin
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/

/*
**********************************************************************
** pointing machine header file
**********************************************************************
*/

#ifndef PM_H
#define PM_H

/* the state names */
#define PM_S00	(0)
#define PM_S01	(1)
#define PM_S02	(2)
#define PM_S03	(3)
#define PM_S04	(4)
#define PM_S05	(5)
#define PM_S06	(6)
#define PM_S07	(7)
#define PM_S08	(8)
#define PM_S09	(9)
#define PM_S10	(10)
#define PM_S11	(11)
#define PM_S12	(12)
#define PM_S13	(13)
#define PM_S14	(14)
#define PM_S15	(15)
#define PM_S16	(16)
#define PM_S17	(17)
#define PM_S18	(18)
#define PM_S19	(19)
#define PM_S20	(20)

#define N_PM_STATES	(21)

/* the transition names */
#define PM_T00	(0)
#define PM_T01	(1)
#define PM_T02	(2)
#define PM_T03	(3)
#define PM_T04	(4)
#define PM_T05	(5)
#define PM_T06	(6)
#define PM_T07	(7)
#define PM_T08	(8)
#define PM_T09	(9)
#define PM_T10	(10)
#define PM_T11	(11)
#define PM_T12	(12)
#define PM_T13	(13)
#define PM_T14	(14)

#define N_PM_TRANS	(15)

/* telescope state */
struct s_tstate {
	/*************************/
	/* independent variables */
	/*************************/
	double utc;		/* coordinated universal time, in JD */
	int delta_at;		/* utc + delta_at = tai */
	double delta_ut;	/* utc + delta_ut = ut1 */
	double lon;		/* east longitude in radians */
	double lat;		/* latitude in radians */
	double alt;		/* altitude above geoid in meters */
	double xpole;		/* polar motion in radians */
	double ypole;		/* polar motion in radians */
	double T;		/* ambient temperature in Kelvins */
	double P;		/* ambient pressure in millibars */
	double H;		/* ambient humidity (0-1) */
	double wavelength;	/* observing wavelength in microns */

	/*****************************/
	/* dependent dynamical times */
	/*****************************/
	double tai;		/* international atomic time */
	double tdt;		/* terrestrial dynamical time */
	double tdb;		/* barycentric dynamical time */

	/************************************/
	/* dependent geometrical quantities */
	/************************************/
	double obliquity;	/* the obliquity of the ecliptic */
	double nut_lon;		/* the nutation in longitude */
	double nut_obl;		/* the nutation in the obliquity */
	struct s_m3 nm;		/* the nutation matrix for now */
	struct s_m3 pm;		/* the precession matrix from J2000 to now */

	/******************************/
	/* dependent rotational times */
	/******************************/
	double ut1;		/* universal time */
	double gmst;		/* greenwich mean sidereal time */
	double gast;		/* greenwich apparent sidereal time */
	double last;		/* local apparent sidereal time */

	/************************/
	/* observer ephemerides */
	/************************/
	struct s_v6 earth;	/* barycentric state vector */
	struct s_v6 obs1;	/* geocentric space-fixed state vector */
	struct s_v6 obs2;	/* geocentric state vector of date */
	struct s_v6 obs3;	/* geocentric state vector of equinox */

	/*********************************/
	/* dependent physical quantities */
	/*********************************/
	double refa;		/* refraction coefficient */
	double refb;		/* refraction coefficient */
};

/* a pointing machine cell */
struct s_pmcell {
	int ptrans;	/* the next transition */
	int pstate;	/* the resulting state */
};

/* EXTERN_START */
/* EXTERN_STOP */

#endif
