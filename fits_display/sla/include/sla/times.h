/*##*************************************
##
## E.S.O. - VLT project
##
## "@(#) $Id$" 
##
## times.h"
## 
## who      when     what
##-------- -------- ------------------
##mnastvog 06/27/95 created for cmm
##*/
/* file: times.h
** sccs: @(#)times.h UW-SAL 1.26 (1/27/94)
** Copyright 1992,1993 University of Wisconsin
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/

/*
**********************************************************************
** declarations for the times routines
**
** "rdb" time is defined to be a scalar of the form yymmdd.ff
**********************************************************************
*/

#ifndef TIMES_H
#define TIMES_H

#include "std.h"

/* the JD of B1950.0 */
#define B1950	(2433282.42345905)

/* the tropical century at 1900.0 */
#define CB	(36524.21987817305)

/* the beginning of any besselian year */
#define BYEAR(x)	(B1950 + (x-1950)*(CB/100))

/* the JD of J2000.0 */
#define J2000	(2451545.0)

/* the julian century */
#define CJ	(36525.0)

/* the beginning of any julian year */
#define JYEAR(x)	(J2000 + (x-2000)*(CJ/100))

/* the JD of the modified JD system */
#define MJD_0	(2400000.5)

#define SUNDAY		(0)
#define MONDAY		(1)
#define TUESDAY		(2)
#define WEDNESDAY	(3)
#define THURSDAY	(4)
#define FRIDAY		(5)
#define SATURDAY	(6)

/* degree, minute, second */
struct s_dms {
	double dd;
	double mm;
	double ss;
};

/* hour, minute, second */
struct s_hms {
	double hh;
	double mm;
	double ss;
};

/* year, month, day*/
struct s_ymd {
	int y;
	int m;
	double dd;
	struct s_hms hms;
};

/* julian day */
struct s_jd {
	double dd;		/* day part */
	struct s_hms hms;	/* fractional part */
};

/* define some macros to access the structures */

#define dmsDecDegrees(s, x)	(s.dd -= (x))
#define dmsDecMinutes(s, x)	(s.mm -= (x))
#define dmsDecSeconds(s, x)	(s.ss -= (x))
#define dmsDivDegrees(s, x)	(s.dd /= (x))
#define dmsDivMinutes(s, x)	(s.mm /= (x))
#define dmsDivSeconds(s, x)	(s.ss /= (x))
#define dmsGetDegrees(s)	(s.dd)
#define dmsGetMinutes(s)	(s.mm)
#define dmsGetSeconds(s)	(s.ss)
#define dmsIncDegrees(s, x)	(s.dd += (x))
#define dmsIncMinutes(s, x)	(s.mm += (x))
#define dmsIncSeconds(s, x)	(s.ss += (x))
#define dmsMulDegrees(s, x)	(s.dd *= (x))
#define dmsMulMinutes(s, x)	(s.mm *= (x))
#define dmsMulSeconds(s, x)	(s.ss *= (x))
#define dmsSetDegrees(s, x)	(s.dd = (x))
#define dmsSetMinutes(s, x)	(s.mm = (x))
#define dmsSetSeconds(s, x)	(s.ss = (x))

#define hmsDecHours(s, x)	(s.hh -= (x))
#define hmsDecMinutes(s, x)	(s.mm -= (x))
#define hmsDecSeconds(s, x)	(s.ss -= (x))
#define hmsDivHours(s, x)	(s.hh /= (x))
#define hmsDivMinutes(s, x)	(s.mm /= (x))
#define hmsDivSeconds(s, x)	(s.ss /= (x))
#define hmsGetHours(s)		(s.hh)
#define hmsGetMinutes(s)	(s.mm)
#define hmsGetSeconds(s)	(s.ss)
#define hmsIncHours(s, x)	(s.hh += (x))
#define hmsIncMinutes(s, x)	(s.mm += (x))
#define hmsIncSeconds(s, x)	(s.ss += (x))
#define hmsMulHours(s, x)	(s.hh *= (x))
#define hmsMulMinutes(s, x)	(s.mm *= (x))
#define hmsMulSeconds(s, x)	(s.ss *= (x))
#define hmsSetHours(s, x)	(s.hh = (x))
#define hmsSetMinutes(s, x)	(s.mm = (x))
#define hmsSetSeconds(s, x)	(s.ss = (x))

#define jdDecDay(s, x)		(s.dd -= (x))
#define jdDecHours(s, x)	(s.hms.hh -= (x))
#define jdDecMinutes(s, x)	(s.hms.mm -= (x))
#define jdDecSeconds(s, x)	(s.hms.ss -= (x))
#define jdDivDay(s, x)		(s.dd /= (x))
#define jdDivHours(s, x)	(s.hms.hh /= (x))
#define jdDivMinutes(s, x)	(s.hms.mm /= (x))
#define jdDivSeconds(s, x)	(s.hms.ss /= (x))
#define jdGetDay(s)		(s.dd)
#define jdGetHours(s)		(s.hms.hh)
#define jdGetMinutes(s)		(s.hms.mm)
#define jdGetSeconds(s)		(s.hms.ss)
#define jdIncDay(s, x)		(s.dd += (x))
#define jdIncHours(s, x)	(s.hms.hh += (x))
#define jdIncMinutes(s, x)	(s.hms.mm += (x))
#define jdIncSeconds(s, x)	(s.hms.ss += (x))
#define jdMulDay(s, x)		(s.dd *= (x))
#define jdMulHours(s, x)	(s.hms.hh *= (x))
#define jdMulMinutes(s, x)	(s.hms.mm *= (x))
#define jdMulSeconds(s, x)	(s.hms.ss *= (x))
#define jdSetDay(s, x)		(s.dd = (x))
#define jdSetHours(s, x)	(s.hms.hh = (x))
#define jdSetMinutes(s, x)	(s.hms.mm = (x))
#define jdSetSeconds(s, x)	(s.hms.ss = (x))

#define ymdDecDay(s, x)		(s.dd -= (x))
#define ymdDecHours(s, x)	(s.hms.hh -= (x))
#define ymdDecMinutes(s, x)	(s.hms.mm -= (x))
#define ymdDecMonth(s, x)	(s.m -= (x))
#define ymdDecSeconds(s, x)	(s.hms.ss -= (x))
#define ymdDecYear(s, x)	(s.y -= (x))
#define ymdDivDay(s, x)		(s.dd /= (x))
#define ymdDivHours(s, x)	(s.hms.hh /= (x))
#define ymdDivMinutes(s, x)	(s.hms.mm /= (x))
#define ymdDivMonth(s, x)	(s.m /= (x))
#define ymdDivSeconds(s, x)	(s.hms.ss /= (x))
#define ymdDivYear(s, x)	(s.y /= (x))
#define ymdGetDay(s)		(s.dd)
#define ymdGetHours(s)		(s.hms.hh)
#define ymdGetMinutes(s)	(s.hms.mm)
#define ymdGetMonth(s)		(s.m)
#define ymdGetSeconds(s)	(s.hms.ss)
#define ymdGetYear(s)		(s.y)
#define ymdIncDay(s, x)		(s.dd += (x))
#define ymdIncHours(s, x)	(s.hms.hh += (x))
#define ymdIncMinutes(s, x)	(s.hms.mm += (x))
#define ymdIncMonth(s, x)	(s.m += (x))
#define ymdIncSeconds(s, x)	(s.hms.ss += (x))
#define ymdIncYear(s, x)	(s.y += (x))
#define ymdMulDay(s, x)		(s.dd *= (x))
#define ymdMulHours(s, x)	(s.hms.hh *= (x))
#define ymdMulMinutes(s, x)	(s.hms.mm *= (x))
#define ymdMulMonth(s, x)	(s.m *= (x))
#define ymdMulSeconds(s, x)	(s.hms.ss *= (x))
#define ymdMulYear(s, x)	(s.y *= (x))
#define ymdSetDay(s, x)		(s.dd = (x))
#define ymdSetHours(s, x)	(s.hms.hh = (x))
#define ymdSetMinutes(s, x)	(s.hms.mm = (x))
#define ymdSetMonth(s, x)	(s.m = (x))
#define ymdSetSeconds(s, x)	(s.hms.ss = (x))
#define ymdSetYear(s, x)	(s.y = (x))

/* define some short-cut macros */
#define d2h(d)			((d)/15.0)
#define d2hms(d)		(h2hms(d2h(d)))
#define d2r(d)			((d)*M_PI/180)
#define dms2h(dms)		(d2h(dms2d(dms)))
#define dms2r(dms)		(d2r(dms2d(dms)))
#define fmt_dms(dms)		(fmt_d(dms2d(dms)))
#define fmt_hms(hms)		(fmt_h(hms2h(hms)))
#define fmt_jd(jd)		(fmt_j(jd2j(jd)))
#define fmt_r(r)		(fmt_d(r2d(r)))
#define fmt_y(y)		(fmt_ymd(y2ymd(y)))
#define h2d(h)			((h)*15.0)
#define h2dms(h)		(d2dms(h2d(h)))
#define h2r(h)			((h)*M_PI/12)
#define hms2d(hms)		(h2d(hms2h(hms)))
#define hms2r(hms)		(h2r(hms2h(hms)))
#define j2j(j)			(j)
#define j2rdb(j)		(jd2rdb(j2jd(j)))
#define j2y(j)			(jd2y(j2jd(j)))
#define j2ymd(j)		(jd2ymd(j2jd(j)))
#define jd2rdb(jd)		(ymd2rdb(jd2ymd(jd)))
#define jd2y(jd)		(ymd2y(jd2ymd(jd)))
#define r2d(r)			((r)*180/M_PI)
#define r2dms(r)		(d2dms(r2d(r)))
#define r2h(r)			((r)*12/M_PI)
#define r2hms(r)		(h2hms(r2h(r)))
#define rdb2j(rdb)		(jd2j(rdb2jd(rdb)))
#define rdb2jd(rdb)		(ymd2jd(rdb2ymd(rdb)))
#define rdb2rdb(rdb)		(ymd2rdb(rdb2ymd(rdb)))
#define rdb2y(rdb)		(ymd2y(rdb2ymd(rdb)))
#define rdb_diff(rdb1,rdb2)	(jd_diff(rdb2jd(rdb1),rdb2jd(rdb2)))
#define y2j(y)			(jd2j(y2jd(y)))
#define y2jd(y)			(ymd2jd(y2ymd(y)))
#define y2rdb(y)		(ymd2rdb(y2ymd(y)))
#define y2y(y)			(y)
#define ymd2j(ymd)		(jd2j(ymd2jd(ymd)))
#define ymd_diff(ymd1,ymd2)	(jd_diff(ymd2jd(ymd1),ymd2jd(ymd2)))

/* EXTERN_START */
extern char *fmt_alpha();
extern char *fmt_d();
extern char *fmt_delta();
extern char *fmt_h();
extern char *fmt_j();
extern char *fmt_rdb();
extern char *fmt_ymd();
extern char *fmt_ymd_raw();
extern double d2d();
extern double dms2d();
extern double gcal2j();
extern double h2h();
extern double hms2h();
extern double jcal2j();
extern double jd2j();
extern double r2r();
extern double ymd2dd();
extern double ymd2rdb();
extern double ymd2y();
extern int j2dow();
extern int y2doy();
extern struct s_dms d2dms();
extern struct s_dms dms2dms();
extern struct s_dms dms_diff();
extern struct s_dms dms_sum();
extern struct s_dms hms2dms();
extern struct s_hms dms2hms();
extern struct s_hms h2hms();
extern struct s_hms hms2hms();
extern struct s_hms hms_diff();
extern struct s_hms hms_sum();
extern struct s_jd j2jd();
extern struct s_jd jd2jd();
extern struct s_jd jd_diff();
extern struct s_jd jd_now();
extern struct s_jd jd_sum();
extern struct s_jd ymd2jd();
extern struct s_ymd jd2ymd();
extern struct s_ymd rdb2ymd();
extern struct s_ymd y2ymd();
extern struct s_ymd ydd2ymd();
extern struct s_ymd ymd2ymd();
extern void j2gcal();
extern void j2jcal();
/* EXTERN_STOP */

#endif
