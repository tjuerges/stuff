/*##*************************************
##
## E.S.O. - VLT project
##
## "@(#) $Id$" 
##
## std.h"
## 
## who      when     what
##-------- -------- ------------------
##mnastvog 06/27/95 created for cmm
##*/
/* file: %M%
** sccs: %Z%%M% UW-SAL %I% (%G%)
** Copyright 1992,1993 University of Wisconsin
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/

/*
**********************************************************************
** this file does a standardized include of system header files.
**********************************************************************
*/

#ifndef STD_H
#define STD_H

#ifdef VMS

#include "std_vms.h"

#else

/* These things make Unix happy. */

#	ifdef MAKE_VXWORKS

#		include "std_vxworks.h"

#	else

#		include "std_unix.h"

#	endif

#endif

/* These things make everybody happy */

#if !defined(M_PI)
#	define M_PI	(3.14159265358979323846)
#endif

/* this is the type that arg 1 in fread() and fwrite() points at */
#if !defined(FIO_T)
#	if defined(_SIZE_T_)
		typedef void *fio_t;
#	else
		typedef char *fio_t;
#	endif
#	define FIO_T
#endif

#endif
