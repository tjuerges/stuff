/*##*************************************
##
## E.S.O. - VLT project
##
## "@(#) $Id$" 
##
## m6.h"
## 
## who      when     what
##-------- -------- ------------------
##mnastvog 06/27/95 created for cmm
##*/
/* file: m6.h
** sccs: @(#)m6.h UW-SAL 1.1 (10/7/93)
** Copyright 1992,1993 University of Wisconsin
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/

#ifndef M6_INCLUDE
#define M6_INCLUDE

struct s_m6 {
	struct s_m3 m[2][2];
};

#define m6GetPP(m6)	(m6.m[0][0])
#define m6GetPV(m6)	(m6.m[0][1])
#define m6GetVP(m6)	(m6.m[1][0])
#define m6GetVV(m6)	(m6.m[1][1])

#define m6SetPP(m6,m3)	(m6.m[0][0] = (m3))
#define m6SetPV(m6,m3)	(m6.m[0][1] = (m3))
#define m6SetVP(m6,m3)	(m6.m[1][0] = (m3))
#define m6SetVV(m6,m3)	(m6.m[1][1] = (m3))

/* EXTERN_START */
/* EXTERN_STOP */

#endif
