/*##*************************************
##
## E.S.O. - VLT project
##
## "@(#) $Id$" 
##
## std_vms.h"
## 
## who      when     what
##-------- -------- ------------------
##mnastvog 06/27/95 created for cmm
##*/
/* file: %M%
** sccs: %Z%%M% UW-SAL %I% (%G%)
** Copyright 1992,1993 University of Wisconsin
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/

/*
**********************************************************************
** this file does a standardized include of system header files.
**********************************************************************
*/

#ifndef STD_VMS_H
#define STD_VMS_H

/* These things make VMS happy. */

#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#endif
