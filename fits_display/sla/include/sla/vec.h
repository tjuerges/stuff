/*##*************************************
##
## E.S.O. - VLT project
##
## "@(#) $Id$" 
##
## vec.h"
## 
## who      when     what
##-------- -------- ------------------
##mnastvog 06/27/95 created for cmm
##*/
/* file: vec.h
** sccs: @(#)vec.h UW-SAL 1.16 (10/7/93)
** Copyright 1992,1993 University of Wisconsin
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/

#ifndef VEC_INCLUDE
#define VEC_INCLUDE

#include "std.h"

#define CARTESIAN	(0)
#define SPHERICAL	(1)
#define POLAR		(SPHERICAL)

/* define 3-vectors */
#include "v3.h"

/* define 6-vectors */
#include "v6.h"

/* define 3-matrices */
#include "m3.h"

/* define 6-matrices */
#include "m6.h"

/* EXTERN_START */
extern char *m3fmt();
extern char *m6fmt();
extern char *v3fmt();
extern char *v6fmt();
extern double v3alpha();
extern double v3delta();
extern double v3dot();
extern double v3mod();
extern double v6alpha();
extern double v6delta();
extern double v6dot();
extern double v6mod();
extern struct s_m3 m3I();
extern struct s_m3 m3O();
extern struct s_m3 m3Rx();
extern struct s_m3 m3RxDot();
extern struct s_m3 m3Ry();
extern struct s_m3 m3RyDot();
extern struct s_m3 m3Rz();
extern struct s_m3 m3RzDot();
extern struct s_m3 m3diff();
extern struct s_m3 m3inv();
extern struct s_m3 m3m3();
extern struct s_m3 m3scale();
extern struct s_m3 m3sum();
extern struct s_m6 m6I();
extern struct s_m6 m6O();
extern struct s_m6 m6Qx();
extern struct s_m6 m6Qy();
extern struct s_m6 m6Qz();
extern struct s_m6 m6diff();
extern struct s_m6 m6inv();
extern struct s_m6 m6m6();
extern struct s_m6 m6scale();
extern struct s_m6 m6sum();
extern struct s_v3 m3v3();
extern struct s_v3 m6v3();
extern struct s_v3 v3c2s();
extern struct s_v3 v3cross();
extern struct s_v3 v3diff();
extern struct s_v3 v3init();
extern struct s_v3 v3s2c();
extern struct s_v3 v3scale();
extern struct s_v3 v3sum();
extern struct s_v3 v3unit();
extern struct s_v3 v62v3();
extern struct s_v6 m3v6();
extern struct s_v6 m6v6();
extern struct s_v6 v32v6();
extern struct s_v6 v6c2s();
extern struct s_v6 v6cross();
extern struct s_v6 v6diff();
extern struct s_v6 v6init();
extern struct s_v6 v6s2c();
extern struct s_v6 v6scale();
extern struct s_v6 v6sum();
extern struct s_v6 v6unit();
/* EXTERN_STOP */

#endif
