/*##*************************************
##
## E.S.O. - VLT project
##
## "@(#) $Id$" 
##
## amoeba.h"
## 
## who      when     what
##-------- -------- ------------------
##mnastvog 06/27/95 created for cmm
##*/
/* file: amoeba.h
** SCCSid: @(#)amoeba.h 1.4 11/11/91
** Author: Jeffrey W Percival (jwp@sal.wisc.edu)
** Space Astronomy Laboratory, University of Wisconsin
** Do not remove or alter the lines above.
**********************************************************************
** header file for the "amoeba" downhill simplex minimizer
**********************************************************************
*/

#ifndef AMOEBA_INCLUDE
#define AMOEBA_INCLUDE

/* maximum number of dimensions */
#define AMOEBA_ND (20)

/* maximum number of points */
#define AMOEBA_NP (21)

/* these are the characteristic scale factors
** for the simplex reflections
*/
#define AMOEBA_ALPHA	(1.0)
#define AMOEBA_BETA	(0.5)
#define AMOEBA_GAMMA	(2.0)

#endif
