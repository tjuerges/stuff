/*##*************************************
##
## E.S.O. - VLT project
##
## "@(#) $Id$" 
##
## std_unix.h"
## 
## who      when     what
##-------- -------- ------------------
##mnastvog 06/27/95 created for cmm
##*/
/* file: %M%
** sccs: %Z%%M% UW-SAL %I% (%G%)
** Copyright 1992,1993 University of Wisconsin
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/

/*
**********************************************************************
** this file does a standardized include of system header files.
**********************************************************************
*/

#ifndef STD_UNIX_H
#define STD_UNIX_H

#ifdef __sgi
#define _BSD_SIGNALS
#endif

/* These things make Unix happy. */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>
#include <strings.h>

/*#include <arpa/inet.h> */
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <math.h>
#include <netdb.h>
/*#include <netinet/in.h> */
#include <signal.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/uio.h>
#include <time.h>
#include <unistd.h>

extern char *initstate();
extern char *setstate();
extern double copysign();
extern long random();
extern unsigned alarm();
extern unsigned sleep();
extern void bcopy();
extern void bzero();
extern void ftime();
extern void perror();
#ifndef __sgi
extern void srandom();
#endif

#endif
