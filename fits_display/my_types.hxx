/*
  "@(#) $Id$"
*/

#ifndef MY_TYPES_HXX
#define MY_TYPES_HXX

#ifdef USHORT
#undef USHORT
#endif
#define USHORT unsigned short
#ifdef UINT
#undef UINT
#endif
#define UINT unsigned int

#define TO_IMAGE TRUE
#define FROM_IMAGE FALSE

const int NUMBER_OF_COLORS=256;
const int BIT_PER_PIXEL=8;

/* Maximum and minimum values one
gets with 16 bit AD resolution
*/
const int MIN_DATA_VAL=0;
const int MAX_DATA_VAL=(1<<16)-1;

/* For tracking purposes one needs a minimum value
which tells the tracking unit that the offet from
the image before to the actual image is too small
to be corrected.
*/
const float MINIMUM_FRACTIONAL_PIXEL=1.0e-4;

// Constants used in FFT et al.
const int REAL_FFT=1;
const int IMAGINARY_FFT=2;
const int AMPLITUDE_FFT=4;
const int PHASE_FFT=8;

#endif // MY_TYPES_H
