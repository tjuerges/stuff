/*
  "@(#) $Id$"
*/

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <unistd.h>
#include <sys/time.h>

#ifdef __linux__
#include <sys/stat.h>
#endif

#include "write_fits.hxx"
#include "iso_time.hxx"
#include <fitsio.h>
#include <sla/slalib.h>
#include <sla/slamac.h>

void write_fits::set_header(const fits_c& header)
{
    fits=header;
}

void write_fits::set_filename_root(const std::string& name)
{
  filename_root = name;
}

void write_fits::set_width(const int& w)
{
    fits.width=w;
}

void write_fits::set_height(const int& h)
{
    fits.height=h;
}

void write_fits::set_filename(const std::string& name)
{
  filename = name;
}

void write_fits::set_telescope_name(const std::string& name)
{
  fits.telescop = name;
}

void write_fits::set_instrument_name(const std::string& name)
{
  fits.instrume = name;
}

void write_fits::set_exposure_type(const int& exposure_type)
{
  image_type = exposure_type;
  switch (exposure_type)
    {
    case BIAS:
      fits.type = "zero";
      break;
    case DARK:
      fits.type = "dark";
      break;
    case FLAT:
      fits.type = "flat";
      break;
    case OBJECT:
    default:
      fits.type = "object";
      break;
    }
  fits.imagetyp = fits.type;
}

void write_fits::set_observer_name(const std::string& name)
{
  fits.observer = name;
}

void write_fits::set_object_name(const std::string& name)
{
  fits.origin = fits.object = fits.source = name;
}

void write_fits::set_air_temperature(const double& airtemp)
{
  fits.airtempe = airtemp + 273.15;     // Temeprature in Kelvin
}

void write_fits::set_ccd_temperature(const double& ccdtemp)
{
  fits.temperat = ccdtemp + 273.15;     // Temeprature in Kelvin
}

void write_fits::set_exp_time(const unsigned long& t)
{
  exp_time = t;
}

void write_fits::set_field_rotation(const double& rot)
{
  field_rotation_angle = rot;
}

void write_fits::set_gain(const float& g)
{
  gain = g;
}

void write_fits::set_pixel_width(const float& width)
{
  pixel_width = width;
}

void write_fits::set_pixel_height(const float& height)
{
  pixel_height = height;
}

void write_fits::set_filter_name(const std::string& filtername)
{
  fits.filter = filtername;
}

void write_fits::set_RA_coord(const double& RA)
{
  fits.ra = RA;
}

void write_fits::set_DEC_coord(const double& DEC)
{
  fits.dec = DEC;
}

void write_fits::set_focal_length(const int& length)
{
  fits.focallen = length;
}

void write_fits::write_fits_file(const std::string& tmp_filename,
                                 const unsigned int& width,
                                 const unsigned int& height,
                                 const unsigned short *data)
{
  const iso_time now;

  std::string dummy = filename;
  set_filename(tmp_filename);
  save_image(now, now, width, height, data);
  set_filename(dummy);
}

void write_fits::write_fits_file(const std::string& filename,const fits_c& header,const unsigned short int* data)
{
  fits=header;
  long naxes[2] = {fits.width,fits.height};
  fitsfile *file = 0;
  const long naxis = 2;
  int status=0;
  const unsigned int frame_size_short=fits.width * fits.height * sizeof(unsigned short);
  std::ostringstream inf;
  status = 0;
  fits_create_file(&file,filename.c_str(), &status);
  if(status)
  {
    inf << "ERROR: Cannot open file \"" << filename << "\" for writing data to disc." << std::ends;
      emit(need_write_on_log_window(inf.str()));
      fits_report_error(stderr, status);
      return;
    }

  fits_create_img(file, USHORT_IMG, naxis, naxes, &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  char dummy[FLEN_VALUE];
  std::sprintf(dummy, "UTC");
  fits_update_key(file, TSTRING, "TIMESYS", dummy, "Time zone used", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "%s", fits.origfile.c_str());
  fits_update_key(file, TSTRING, "ORIGFILE", dummy, "Original image filename",
                  &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "%s", fits.filename.c_str());
  fits_update_key(file, TSTRING, "FILENAME", dummy, "Original image filename",
                  &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "%s", fits.date.c_str());
  fits_update_key(file, TSTRING, "DATE", dummy,
                  "Date and time of exposure start", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "%s", fits.date_obs.c_str());
  fits_update_key(file, TSTRING, "DATE-OBS", dummy,
                  "Date and time of exposure start", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "%s", fits.datestop.c_str());
  fits_update_key(file, TSTRING, "OBS-STOP", dummy,
                  "Date and time of exposure stop", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  fits_update_key(file, TDOUBLE, "EPOCH", &fits.epoch,
                  "Epoch of the observation", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  fits_update_key(file, TFLOAT, "EXPTIME", &fits.exptime,
                  "Exposure time in seconds", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "s");
  fits_write_key_unit(file, "EXPTIME", dummy, &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  if(fits.gain != -9999.9)
    {
      fits_update_key(file, TFLOAT, "GAIN", &fits.gain, "Gain in e-/ADU",
                      &status);
      if(status)
        {
          fits_report_error(stderr, status);
          status = 0;
        }
    }

  if(fits.xpixsz != -9999.9)
    {
      fits_update_key(file, TFLOAT, "XPIXSZ", &fits.xpixsz, "Pixel width",
                      &status);
      if(status)
        {
          fits_report_error(stderr, status);
          status = 0;
        }

      sprintf(dummy, "10E-6 m");
      fits_write_key_unit(file, "XPIXSZ", dummy, &status);
      if(status)
        {
          fits_report_error(stderr, status);
          status = 0;
        }
    }

  if(fits.ypixsz != -9999.9)
    {
      fits_update_key(file, TFLOAT, "YPIXSZ", &fits.ypixsz, "Pixel height",
                      &status);
      if(status)
        {
          fits_report_error(stderr, status);
          status = 0;
        }

      sprintf(dummy, "10E-6 m");
      fits_write_key_unit(file, "YPIXSZ", dummy, &status);
      if(status)
        {
          fits_report_error(stderr, status);
          status = 0;
        }
    }

  if(fits.xpixsz != -9999.9)
    {
      fits_update_key(file, TFLOAT, "CDELT1", &fits.cdelt1, "Pixel width",
                      &status);
      if(status)
        {
          fits_report_error(stderr, status);
          status = 0;
        }

      sprintf(dummy, "deg");
      fits_write_key_unit(file, "CDELT1", dummy, &status);
      if(status)
        {
          fits_report_error(stderr, status);
          status = 0;
        }
    }

  if(fits.ypixsz != -9999.9)
    {
      fits_update_key(file, TFLOAT, "CDELT2", &fits.cdelt2, "Pixel height",
                      &status);
      if(status)
        {
          fits_report_error(stderr, status);
          status = 0;
        }

      sprintf(dummy, "deg");
      fits_write_key_unit(file, "CDELT2", dummy, &status);
      if(status)
        {
          fits_report_error(stderr, status);
          status = 0;
        }
    }

  if(fits.temperat != -9999.9)
    {
      fits_update_key(file, TDOUBLE, "TEMPERAT", &fits.temperat,
                      "CCD temperature", &status);
      if(status)
        {
          fits_report_error(stderr, status);
          status = 0;
        }

      sprintf(dummy, "K");
      fits_write_key_unit(file, "TEMPERAT", dummy, &status);
      if(status)
        {
          fits_report_error(stderr, status);
          status = 0;
        }
    }

  if(fits.airtempe != -9999.9)
    {
      fits_update_key(file, TDOUBLE, "AIRTEMPE", &fits.airtempe,
                      "Air temperature", &status);
      if(status)
        {
          fits_report_error(stderr, status);
          status = 0;
        }

      sprintf(dummy, "K");
      fits_write_key_unit(file, "AIRTEMPE", dummy, &status);
      if(status)
        {
          fits_report_error(stderr, status);
          status = 0;
        }
    }

  sprintf(dummy, "%s", fits.source.c_str());
  fits_update_key(file, TSTRING, "SOURCE", dummy, "Observed object", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "%s", fits.object.c_str());
  fits_update_key(file, TSTRING, "OBJECT", dummy, "Observed object", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "%s", fits.origin.c_str());
  fits_update_key(file, TSTRING, "ORIGIN", dummy, "Observed object", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  fits_update_key(file, TDOUBLE, "RA", &fits.ra, "Object rightascension",
                  &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "hrs");
  fits_write_key_unit(file, "RA", dummy, &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  fits_update_key(file, TDOUBLE, "DEC", &fits.dec, "Object declination",
                  &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "deg");
  fits_write_key_unit(file, "DEC", dummy, &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  if(field_rotation_angle != -9999.9)
  {
      fits_update_key(file, TFLOAT, "CROT1", &(fits.crot1),
                  "Rotation angle of the image", &status);
      if(status)
      {
	  fits_report_error(stderr, status);
	  status = 0;
      }
  }

  sprintf(dummy, "deg");
  fits_write_key_unit(file, "CROT1", dummy, &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  fits_update_key(file, TFLOAT, "CROT2", &(fits.crot2),
                  "Rotation angle of the image", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "deg");
  fits_write_key_unit(file, "CROT2", dummy, &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "%s", fits.telescop.c_str());
  fits_update_key(file, TSTRING, "TELESCOP", dummy, "Telescope", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  fits_update_key(file, TINT, "FOCALLEN", &fits.focallen,
                  "Telescope focal length", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "10E-3 m");
  fits_write_key_unit(file, "FOCALLEN", dummy, &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "%s", fits.instrume.c_str());
  fits_update_key(file, TSTRING, "INSTRUME", dummy, "Instrument being used",
                  &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "%s", fits.filter.c_str());
  fits_update_key(file, TSTRING, "FILTER", dummy, "Filtername", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "%s", fits.observer.c_str());
  fits_update_key(file, TSTRING, "OBSERVER", dummy, "Observer", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "%s", fits.type.c_str());
  fits_update_key(file, TSTRING, "TYPE", dummy, "Imagetype for IRAF or MIDAS",
                  &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "%s", fits.imagetyp.c_str());
  fits_update_key(file, TSTRING, "IMAGETYP", dummy,
                  "Imagetype for IRAF or MIDAS", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy,"Image created or modified by fits_display (c) by Thomas J�rges");
  fits_write_comment(file,dummy,&status);
  const iso_time now;
  sprintf("Modified on: %s",now.get_time_string_iso().c_str());
  fits_write_history(file,dummy,&status);
  fits_write_img(file, TUSHORT, 1, frame_size_short, const_cast<unsigned short int*>(data), &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  fits_close_file(file, &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

#ifdef __linux__
  /*
     Because of sudo/chmod a+s, we have written the file with root:root
     ownership. Change the rights of the written for the user to be
     writeable/deleteable. 
   */
  chmod(filename.c_str(),S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
#endif

  inf << "Data saved to disc as file: " << std::ends;
  emit(need_write_on_log_window(inf.str()));
}

void write_fits::save_image(const iso_time& start, const iso_time& stop,
                            const unsigned short& width,
                            const unsigned short& height,
                            const unsigned short *data)
{
  /*
     while dealing with the time, we set up some FITS header data. the files
     will be written to the directory where the software has been run from. 
   */
  fitsfile *file = 0;
  const long naxis = 2;
  long naxes[2] = { width, height };
  int status;
  const unsigned int frame_size_short =  width * height * sizeof(unsigned short);
  std::string tmp_filename;
  const std::string filename_postfix = ".fits";

  // filename: "YYYYMMDD-HHMMSS.fits"
  fits.epoch = start.get_epoch();
  tmp_filename = start.get_time_string_no_iso();
  tmp_filename += filename_postfix;

  // Set FITS header filename
  fits.origfile = fits.filename = tmp_filename;

  // Prepend user defined path to filename
  tmp_filename = filename_root + tmp_filename;
  tmp_filename += '\0';
  status = 0;
  fits_create_file(&file, tmp_filename.c_str(), &status);
  if(status)
    {
      std::ostringstream dummy;
      char milliseconds[16];
      sprintf(milliseconds,"%ld",start.get_microseconds() / 1000L);

      dummy << "ERROR: Cannot open file \"" << tmp_filename.c_str() << "\" for writing data to disc." << std::ends;
      emit(need_write_on_log_window(dummy.str()));
      fits_report_error(stderr, status);

      tmp_filename = start.get_time_string_no_iso();
      // Add the milliseconds of time to filename:
      tmp_filename += '.';
      tmp_filename += milliseconds;

      tmp_filename += filename_postfix;

      // Set FITS header filename
      fits.origfile = fits.filename = tmp_filename;

      // Prepend user defined path to filename
      tmp_filename = filename_root + tmp_filename;
      tmp_filename += '\0';
      dummy<<"Trying to save the data to file \"" << tmp_filename.c_str() << "\" instead." << std::ends;
      emit(need_write_on_log_window(dummy.str()));
      fits_create_file(&file, tmp_filename.c_str(), &status);
      if(status)
        {
          dummy << "ERROR: Cannot open file \"" << tmp_filename.c_str() << "\" for writing data to disc." << std::ends;
          emit(need_write_on_log_window(dummy.str()));
          dummy << "The file \"" << tmp_filename.c_str() << "\" was NOT saved!" << std::ends;
          emit(need_write_on_log_window(dummy.str()));
          fits_report_error(stderr, status);
          return;
        }
    }

  fits_create_img(file, USHORT_IMG, naxis, naxes, &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  char dummy[FLEN_VALUE];
  sprintf(dummy, "UTC");
  fits_update_key(file, TSTRING, "TIMESYS", dummy, "Time zone used", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "%s", fits.origfile.c_str());
  fits_update_key(file, TSTRING, "ORIGFILE", dummy, "Original image filename",
                  &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "%s", fits.filename.c_str());
  fits_update_key(file, TSTRING, "FILENAME", dummy, "Original image filename",
                  &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  fits.date_obs = fits.date = start.get_time_string_iso();
  sprintf(dummy, "%s", fits.date.c_str());
  fits_update_key(file, TSTRING, "DATE", dummy,
                  "Date and time of exposure start", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "%s", fits.date_obs.c_str());
  fits_update_key(file, TSTRING, "DATE-OBS", dummy,
                  "Date and time of exposure start", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  fits.datestop = stop.get_time_string_iso();
  sprintf(dummy, "%s", fits.datestop.c_str());
  fits_update_key(file, TSTRING, "OBS-STOP", dummy,
                  "Date and time of exposure stop", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  fits_update_key(file, TDOUBLE, "EPOCH", &fits.epoch,
                  "Epoch of the observation", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  fits.exptime = (float) exp_time;
  fits.gain = gain;
  fits.xpixsz = pixel_width;
  fits.ypixsz = pixel_height;
  fits.cdelt1 =
    atan((float) pixel_width / 1000. / (float) fits.focallen) * 180 / M_PI_2;
  fits.cdelt2 =
    atan((float) pixel_height / 1000. / (float) fits.focallen) * 180 / M_PI_2;
  if(image_type == BIAS)
    fits.exptime = 0.;
  else
    fits.exptime /= 100.;

  fits_update_key(file, TFLOAT, "EXPTIME", &fits.exptime,
                  "Exposure time in seconds", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "s");
  fits_write_key_unit(file, "EXPTIME", dummy, &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  if(fits.gain >= 0.)
    {
      fits_update_key(file, TFLOAT, "GAIN", &fits.gain, "Gain in e-/ADU",
                      &status);
      if(status)
        {
          fits_report_error(stderr, status);
          status = 0;
        }
    }

  if(fits.xpixsz > 0.)
    {
      fits_update_key(file, TFLOAT, "XPIXSZ", &fits.xpixsz, "Pixel width",
                      &status);
      if(status)
        {
          fits_report_error(stderr, status);
          status = 0;
        }

      sprintf(dummy, "10E-6 m");
      fits_write_key_unit(file, "XPIXSZ", dummy, &status);
      if(status)
        {
          fits_report_error(stderr, status);
          status = 0;
        }
    }

  if(fits.ypixsz > 0.)
    {
      fits_update_key(file, TFLOAT, "YPIXSZ", &fits.ypixsz, "Pixel height",
                      &status);
      if(status)
        {
          fits_report_error(stderr, status);
          status = 0;
        }

      sprintf(dummy, "10E-6 m");
      fits_write_key_unit(file, "YPIXSZ", dummy, &status);
      if(status)
        {
          fits_report_error(stderr, status);
          status = 0;
        }
    }

  if(fits.xpixsz > 0.)
    {
      fits_update_key(file, TFLOAT, "CDELT1", &fits.cdelt1, "Pixel width",
                      &status);
      if(status)
        {
          fits_report_error(stderr, status);
          status = 0;
        }

      sprintf(dummy, "deg");
      fits_write_key_unit(file, "CDELT1", dummy, &status);
      if(status)
        {
          fits_report_error(stderr, status);
          status = 0;
        }
    }

  if(fits.ypixsz > 0.)
    {
      fits_update_key(file, TFLOAT, "CDELT2", &fits.cdelt2, "Pixel height",
                      &status);
      if(status)
        {
          fits_report_error(stderr, status);
          status = 0;
        }

      sprintf(dummy, "deg");
      fits_write_key_unit(file, "CDELT2", dummy, &status);
      if(status)
        {
          fits_report_error(stderr, status);
          status = 0;
        }
    }

  if(fits.temperat >= 0.)
    {
      fits_update_key(file, TDOUBLE, "TEMPERAT", &fits.temperat,
                      "CCD temperature", &status);
      if(status)
        {
          fits_report_error(stderr, status);
          status = 0;
        }

      sprintf(dummy, "K");
      fits_write_key_unit(file, "TEMPERAT", dummy, &status);
      if(status)
        {
          fits_report_error(stderr, status);
          status = 0;
        }
    }

  if(fits.airtempe >= 0.)
    {
      fits_update_key(file, TDOUBLE, "AIRTEMPE", &fits.airtempe,
                      "Air temperature", &status);
      if(status)
        {
          fits_report_error(stderr, status);
          status = 0;
        }

      sprintf(dummy, "K");
      fits_write_key_unit(file, "AIRTEMPE", dummy, &status);
      if(status)
        {
          fits_report_error(stderr, status);
          status = 0;
        }
    }

  sprintf(dummy, "%s", fits.source.c_str());
  fits_update_key(file, TSTRING, "SOURCE", dummy, "Observed object", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "%s", fits.object.c_str());
  fits_update_key(file, TSTRING, "OBJECT", dummy, "Observed object", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "%s", fits.origin.c_str());
  fits_update_key(file, TSTRING, "ORIGIN", dummy, "Observed object", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  fits_update_key(file, TDOUBLE, "RA", &fits.ra, "Object rightascension",
                  &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "hrs");
  fits_write_key_unit(file, "RA", dummy, &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  fits_update_key(file, TDOUBLE, "DEC", &fits.dec, "Object declination",
                  &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "deg");
  fits_write_key_unit(file, "DEC", dummy, &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  if(field_rotation_angle > 0.)
    {
      fits.crot1 = field_rotation_angle * 180 / M_PI_2;
      fits.crot2 = field_rotation_angle * 180 / M_PI_2;
    }
  else
    fits.crot1 = fits.crot2 = 0.;

  fits_update_key(file, TFLOAT, "CROT1", &(fits.crot1),
                  "Rotation angle of the image", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "deg");
  fits_write_key_unit(file, "CROT1", dummy, &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  fits_update_key(file, TFLOAT, "CROT2", &(fits.crot2),
                  "Rotation angle of the image", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "deg");
  fits_write_key_unit(file, "CROT2", dummy, &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "%s", fits.telescop.c_str());
  fits_update_key(file, TSTRING, "TELESCOP", dummy, "Telescope", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  fits_update_key(file, TINT, "FOCALLEN", &fits.focallen,
                  "Telescope focal length", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "10E-3 m");
  fits_write_key_unit(file, "FOCALLEN", dummy, &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "%s", fits.instrume.c_str());
  fits_update_key(file, TSTRING, "INSTRUME", dummy, "Instrument being used",
                  &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "%s", fits.filter.c_str());
  fits_update_key(file, TSTRING, "FILTER", dummy, "Filtername", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "%s", fits.observer.c_str());
  fits_update_key(file, TSTRING, "OBSERVER", dummy, "Observer", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "%s", fits.type.c_str());
  fits_update_key(file, TSTRING, "TYPE", dummy, "Imagetype for IRAF or MIDAS",
                  &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  sprintf(dummy, "%s", fits.imagetyp.c_str());
  fits_update_key(file, TSTRING, "IMAGETYP", dummy,
                  "Imagetype for IRAF or MIDAS", &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  fits_write_img(file, TUSHORT, 1, frame_size_short, const_cast<unsigned short int*>(data), &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

  fits_close_file(file, &status);
  if(status)
    {
      fits_report_error(stderr, status);
      status = 0;
    }

#ifdef __linux__
  /*
     Because of sudo/chmod a+s, we have written the file with root:root
     ownership. Change the rights of the written for the user to be
     writeable/deleteable. 
   */
  chmod(tmp_filename.c_str(),
        S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
#endif

  std::ostringstream inf;
  inf << "Data saved to disc as file: " << tmp_filename;
  emit(need_write_on_log_window(inf.str()));
}
