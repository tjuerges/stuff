/*
  "@(#) $Id$"
*/

#ifndef ISO_TIME_HXX
#define ISO_TIME_HXX

#include <string>
#include <ctime>
#include <unistd.h>
#include <sys/time.h>

#include <sla/slalib.h>
#include <sla/slamac.h>

class iso_time
{
public:
  iso_time():time_now(0), epoch(-1.), modifiedJulianDate(0.), slalibError(0)
  {
    // TO BE DONE!!!!!!!!!!!
    // Should be reworked as soon as QT can handle timezones!!!!!
    // Check out the system's time for FITS header and filename
    timerclear(&tv);
    time_now = time(NULL);
    gettimeofday(&tv, &tz);
    tm_time = gmtime(&time_now);
    slaCldj(tm_time->tm_year + 1900, tm_time->tm_mon + 1, tm_time->tm_mday,
            &modifiedJulianDate, &slalibError);
    if(!slalibError)
      epoch = slaEpj(modifiedJulianDate);
  };
  ~iso_time()
  {
  };
  std::string get_time_string_iso(void) const;
  std::string get_time_string_no_iso(void) const;
  std::string get_time_string_no_iso_with_spaces(void) const;
  long get_microseconds(void) const;
  double get_epoch(void) const;

private:
  time_t time_now;
  tm *tm_time;
  struct timeval tv;
  struct timezone tz;
  double epoch;
  double modifiedJulianDate;
  int slalibError;
};
#endif
