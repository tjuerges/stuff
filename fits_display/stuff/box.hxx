#ifndef BOX_HXX
#define BOX_HXX

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Feb 19, 2007  created
*/

class box
{
    public:
    box():
        x(0),
        y(0),
        w(0),
        h(0)
    {
    };

    box(const box & old_box):
        x(old_box.x),
        y(old_box.y),
        w(old_box.w),
        h(old_box.h)
    {
    };

    box(const unsigned int a, const unsigned int b, const unsigned int c,
    const unsigned int d):
        x(a),
        y(b),
        w(c),
        h(d)
    {
    };

    box(const unsigned int set):
        x(set),
        y(set),
        w(set),
        h(set)
    {
    };

    ~box()
    {
    };

    bool operator==(const box & box1) const
    {
        if(x != box1.x)
        {
           return false;
        }

        if(y != box1.y)
        {
           return false;
        }

        if(w != box1.w)
        {
           return false;
        }

        if(h != box1.h)
        {
           return false;
        }

        return true;
    };

    box & operator=(const box & old_box)
    {
        if(static_cast < const box & >(*this) == old_box)
        {
           return (static_cast < box & >(*this));
        }
        else
        {
            x = old_box.x;
            y = old_box.y;
            w = old_box.w;
            h = old_box.h;
            return (static_cast < box & >(*this));
        }
    };

    void copy(const box & old_box)
    {
        x = old_box.x;
        y = old_box.y;
        w = old_box.w;
        h = old_box.h;
    };

    unsigned int x;
    unsigned int y;
    unsigned int w;
    unsigned int h;
};
#endif
