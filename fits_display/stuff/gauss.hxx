/*
  "@(#) $Id$"
*/

#ifndef GAUSS_HXX
#define GAUSS_HXX

#include <vector>
#include "guide_star.hxx"
#include "box.hxx"

//Errors, the gaussian return:
enum GAUSS_ERRORS{
    GAUSS_NO_ERROR,
	    GAUSS_NO_MEM_X,
	    GAUSS_NO_MEM_Y,
	    GAUSS_NO_MEM_Z,
	    GAUSS_ROW_OUT_OF_BOUNDS,
	    GAUSS_COL_OUT_OF_BOUNDS,
	    GAUSS_CIRC_GAUSS_ERROR,
	    GAUSS_CIRC_GAUSS_NO_MEM_X,
	    GAUSS_CIRC_GAUSS_NO_MEM_Y,
	    GAUSS_CIRC_GAUSS_NO_MEM_SIG,
	    GAUSS_NOT_ENOUGH_POINTS
};

// max number of iterations allowed before quit
const unsigned int MAXITER=20;

/*amount to which chisq should converge
  and also min. fractional amount by which
  gaussian parameters allowed to change
*/
const float TINY=1.0e-4;

/*fraction of parameters used as a step size
  to determine derivative empirically
*/
const float FACTOR=0.1;

//largest argument for exp function
const float MAX_ARG=10.;

class GaussFit
{
    public:
    GaussFit();
    GaussFit(const std::vector<unsigned short>&);
    ~GaussFit();

    int do_it(bool, unsigned short,box&, guide_star &);
 
    protected:
    void guess_sky(unsigned short,box&, guide_star &);
    int do_axes(unsigned short, box&, guide_star &);
    int circular_gaussian(unsigned short, box&, guide_star&, const double,
                      const double, double &, double &);
    int gauss_fit(const std::vector < float >&, const std::vector < float >&,
              const std::vector < float >&, const unsigned int, float &,
              float &, float &, float &);
    void guess_params(const std::vector < float >&, const std::vector < float >&,
                  const unsigned int, float &, float &, float &);
    float find_chisq(const std::vector < float >&, const std::vector < float >&,
                 const std::vector < float >&, const std::vector < float >&,
                 const int, const unsigned int, const float, const float,
                 float &);
    
    std::vector<unsigned short>* data;
};
#endif
