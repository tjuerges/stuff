/*
  "@(#) $Id$"
*/

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <cerrno>
#include <unistd.h>
#include <sys/time.h>

#include <qstring.h>
#include <qfile.h>

#include <fitsio.h>
#include <sla/slalib.h>
#include <sla/slamac.h>

#ifdef __linux__
#include <sys/stat.h>
#endif

#include "read_fits.hxx"
//#include "my_types.hxx"

void read_fits::get_header(fits_c& fits_header) const
{
    fits_header=fits;
}

std::string read_fits::get_header(void) const
{
  return FITSHeader;
}

void read_fits::get_filename(std::string& name) const
{
  name = fits.filename;
}

void read_fits::get_telescope_name(std::string& name) const
{
  name = fits.telescop;
}

void read_fits::get_instrument_name(std::string& name) const
{
  name = fits.instrume;
}

void read_fits::get_exposure_type(std::string& type) const
{
  type = fits.imagetyp;
}

void read_fits::get_observer_name(std::string& obs) const
{
  obs = fits.observer;
}

void read_fits::get_object_name(std::string& source) const
{
  source = fits.source;
}

void read_fits::get_air_temperature(double& temp) const
{
  temp = fits.airtempe;         // Temeprature in Kelvin
}

void read_fits::get_ccd_temperature(double& temp) const
{
  temp = fits.temperat;         // Temeprature in Kelvin
}

void read_fits::get_exp_time(float& time) const
{
  time = fits.exptime;
}

void read_fits::get_field_rotation(float& rot) const
{
  rot = fits.crot1;
}

void read_fits::get_gain(float& gain) const
{
  gain = fits.gain;
}

void read_fits::get_pixel_width(float& w) const
{
  w = fits.xpixsz;
}

void read_fits::get_pixel_height(float& h) const
{
  h = fits.ypixsz;
}

void read_fits::get_filter_name(std::string& filter) const
{
  filter = fits.filter;
}

void read_fits::get_RA_coord(double& ra) const
{
  ra = fits.ra;
}

void read_fits::get_DEC_coord(double& dec) const
{
  dec = fits.dec;
}

void read_fits::get_focal_length(int& l) const
{
  l = fits.focallen;
}

int read_fits::read_header_keywords(fitsfile& file)
{
  int bitpix = 0;
  int status = 0, nkeys = 0, keypos = 0, hdutype = 0;
  char card[FLEN_CARD];
  std::ostringstream inHeader;

  fits_read_record(&file,0,card,&status);
  if(fits_get_img_type(&file,&bitpix,&status))
  {
      emit(need_write_on_log_window("Cannot read BITPIX entry!\n"));
      fits_report_error(stderr, status);
      return -1;
  }
  else
  {
      std::string data_type;
      switch (bitpix)
      {
		case BYTE_IMG:
	  data_type = "Byte (8 Bits)";
	  break;
		case SHORT_IMG:
	  data_type = "Short (16 Bits)";
	  break;
		case USHORT_IMG:
	  data_type = "Unsigned Short (16 Bits)";
	  break;
		case LONG_IMG:
	  data_type = "Long (32 Bits)";
	  break;
		case ULONG_IMG:
	  data_type = "Unsigned long (32 Bits)";
	  break;
		case LONGLONG_IMG:
	  data_type = "Long Long (64 Bits)";
	  break;
		case FLOAT_IMG:
	  data_type = "Float";
	  break;
		case DOUBLE_IMG:
	  data_type = "Double";
	  break;
	  default:
	  data_type = "No type";
	  emit(need_write_on_log_window("Cannot determine data type!\n"));
	  return -1;
	  break;
      };
      inHeader << "Data in this image is of type " << data_type << std::endl;
  }
  
  status = 0;
  char dummy[FLEN_VALUE];
  fits_read_record(&file,0,card,&status);
  if(!fits_read_key(&file,TSTRING,"TIMESYS",dummy,NULL,&status))
  {
      fits.timesys=dummy;
  }
  fits_read_record(&file,0,card,&status);
  if(!fits_read_key(&file,TSTRING,"ORIGFILE",dummy,NULL,&status))
  {
      fits.origfile=dummy;
  }
  fits_read_record(&file,0,card,&status);
  if(!fits_read_key(&file,TSTRING,"FILENAME",dummy,NULL,&status))
  {
      fits.filename=dummy;
  }
  fits_read_record(&file,0,card,&status);
  if(!fits_read_key(&file,TSTRING,"DATE",dummy,NULL,&status))
  {
      fits.date=dummy;
  }
  fits_read_record(&file,0,card,&status);
  if(!fits_read_key(&file,TSTRING,"DATE-OBS",dummy,NULL,&status))
  {
      fits.date_obs=dummy;
  }
  fits_read_record(&file,0,card,&status);
  if(!fits_read_key(&file,TSTRING,"OBS-STOP",dummy,NULL,&status))
  {
      fits.datestop=dummy;
  }
  fits_read_record(&file,0,card,&status);
  fits_read_key(&file,TFLOAT,"GAIN",&fits.gain,NULL,&status);

  int axes=0;
  fits_read_record(&file,0,card,&status);
  if(!fits_get_img_dim(&file,&axes,&status))
  {
      if(axes==2)
      {
	  long naxis[]={0,0};
	  fits_read_record(&file,0,card,&status);
	  if(!fits_get_img_size(&file,axes,naxis,&status))
	  {
	      fits.width=naxis[0];
	      fits.height=naxis[1];
	  }
	  else
	  {
	      fits_report_error(stderr, status);
	      return -1;
	  }
      }
      else
      {
	  fits_report_error(stderr, status);
	  return -1;
      }
}
  
  fits_read_record(&file,0,card,&status);
  fits_read_key(&file,TINT,"NAXIS2",&fits.height,NULL,&status);
  fits_read_record(&file,0,card,&status);
  fits_read_key(&file,TDOUBLE,"EPOCH",&fits.epoch,NULL,&status);
  fits_read_record(&file,0,card,&status);
  fits_read_key(&file,TFLOAT,"EXPTIME",&fits.exptime,NULL,&status);
  fits_read_record(&file,0,card,&status);
  fits_read_key(&file,TFLOAT,"GAIN",&fits.gain,NULL,&status);
  fits_read_record(&file,0,card,&status);
  fits_read_key(&file,TFLOAT,"XPIXSZ",&fits.xpixsz,NULL,&status);
  fits_read_record(&file,0,card,&status);
  fits_read_key(&file,TFLOAT,"YPIXSZ",&fits.ypixsz,NULL,&status);
  fits_read_record(&file,0,card,&status);
  fits_read_key(&file,TFLOAT,"CDELT1",&fits.cdelt1,NULL,&status);
  fits_read_record(&file,0,card,&status);
  fits_read_key(&file,TFLOAT,"CDELT2",&fits.cdelt2,NULL,&status);
  fits_read_record(&file,0,card,&status);
  fits_read_key(&file,TDOUBLE,"TEMPERAT",&fits.temperat,NULL,&status);
  fits_read_record(&file,0,card,&status);
  fits_read_key(&file,TDOUBLE,"AIRTEMPE",&fits.airtempe,NULL,&status);
  fits_read_record(&file,0,card,&status);
  if(!fits_read_key(&file,TSTRING,"SOURCE",dummy,NULL,&status))
  {
      fits.source=dummy;
  }
  fits_read_record(&file,0,card,&status);
  if(!fits_read_key(&file,TSTRING,"OBJECT",dummy,NULL,&status))
  {
      fits.object=dummy;
  }
  fits_read_record(&file,0,card,&status);
  if(!fits_read_key(&file,TSTRING,"ORIGIN",dummy,NULL,&status))
  {
      fits.origin=dummy;
  }
  fits_read_record(&file,0,card,&status);
  fits_read_key(&file,TDOUBLE,"RA",&fits.ra,NULL,&status);
  fits_read_record(&file,0,card,&status);
  fits_read_key(&file,TDOUBLE,"DEC",&fits.dec,NULL,&status);
  fits_read_record(&file,0,card,&status);
  fits_read_key(&file,TFLOAT,"CROT1",&(fits.crot1),NULL,&status);
  fits_read_record(&file,0,card,&status);
  fits_read_key(&file,TFLOAT,"CROT2",&(fits.crot2),NULL,&status);
  fits_read_record(&file,0,card,&status);
  if(!fits_read_key(&file,TSTRING,"TELESCOP",dummy,NULL,&status))
  {
      fits.telescop=dummy;
  }
  fits_read_record(&file,0,card,&status);
  fits_read_key(&file,TINT,"FOCALLEN",&fits.focallen,NULL,&status);
  fits_read_record(&file,0,card,&status);
  if(!fits_read_key(&file,TSTRING,"INSTRUME",dummy,NULL,&status))
  {
      fits.instrume=dummy;
  }
  fits_read_record(&file,0,card,&status);
  if(!fits_read_key(&file,TSTRING,"FILTER",dummy,NULL,&status))
  {
      fits.filter=dummy;
  }
  fits_read_record(&file,0,card,&status);
  if(!fits_read_key(&file,TSTRING,"OBSERVER",dummy,NULL,&status))
  {
      fits.observer=dummy;
  }
  fits_read_record(&file,0,card,&status);
  if(!fits_read_key(&file,TSTRING,"TYPE",dummy,NULL,&status))
  {
      fits.type=dummy;
  }
  fits_read_record(&file,0,card,&status);
  if(!fits_read_key(&file,TSTRING,"IMAGETYP",dummy,NULL,&status))
  {
      fits.imagetyp=dummy;
  }

  /*
     attempt to move to next HDU, until we get an EOF error 
   */
  fits_read_record(&file,0,card,&status); //reset the internal CFITSIO pointer to the beginning of the header
  for(int i = 1; !(fits_movabs_hdu(&file, i, &hdutype, &status)); i++)
    {
      if(fits_get_hdrpos(&file, &nkeys, &keypos, &status))
        {
          fits_report_error(stderr, status);
          return -1;
        }

      inHeader << "Header listing for HDU #" << i << ":" << std::endl;
      for(int j = 1; j <= nkeys; j++)
        {
          if(fits_read_record(&file, j, card, &status))
            {
              fits_report_error(stderr, status);
              return -1;
            }
          inHeader << card << std::endl;
        }
      inHeader << "END" << std::endl << std::endl;
  }
  inHeader << std::ends;
  FITSHeader = inHeader.str();

  return bitpix;
}

bool read_fits::read_file(const std::string& filename, unsigned short& width, unsigned short& height,std::vector<unsigned short>& data)
{
  std::ostringstream inf;
  width = height = 0;
  fitsfile *file = 0;
  long naxes[2] = { 0, 0 };
  int status = 0, nfound = 0, anynull = 0;

  status = 0;
  if(fits_open_file(&file, filename.c_str(), READONLY, &status))
    {
      fits_report_error(stderr, status);
      inf << "Cannot open FITS file \"" << filename << "\"." << std::endl << std::ends;
      emit(need_write_on_log_window(inf.str()));
      return false;
    }
  int bitpix = read_header_keywords(*file);
  if(bitpix == -1)
    {
      emit(need_write_on_log_window("Data type in image is unknown.\n"));
      return false;
    }
  // read the NAXIS1 and NAXIS2 keyword to get image size
  if(fits_read_keys_lng(file, "NAXIS", 1, 2, naxes, &nfound, &status))
    {
      fits_report_error(stderr, status);
      emit(need_write_on_log_window("Cannot determine the image dimensions.\n"));
      return false;
    }
  width = static_cast<unsigned short>(naxes[0]);
  height = static_cast<unsigned short>(naxes[1]);
  const unsigned int image_size = width * height;

  if(image_size <= 0)
    {
      emit(need_write_on_log_window("Either image dimensions must be greater than 0!"));
      return false;
    }
  
  unsigned short *us = 0;
  std::vector<float>* tmp_buffer=new std::vector<float>(image_size);
  if(tmp_buffer->size() != image_size)
  {
      delete (std::vector<float>*) tmp_buffer;
      fits_report_error(stderr, status);
      emit(need_write_on_log_window("Not enough memory for temporary image data."));
      return false;
  }
  if(fits_read_img(file,TFLOAT,1L,static_cast<long>(image_size),us,&(tmp_buffer->front()),&anynull,&status))
  {
      delete (std::vector<float>*) tmp_buffer;
      fits_report_error(stderr, status);
      emit(need_write_on_log_window("Reading of image data failed."));
      return false;
  }
  
  data.resize(image_size);
  if(data.size() != image_size)
    {
      delete (std::vector<float>*) tmp_buffer;
      emit(need_write_on_log_window("Not enough memory for image data."));
      return false;
  }

  for(unsigned int x=0;x<width;x++)
  {
      for(unsigned int y=0;y<height;y++)
      {
	  data.at(image_size-(width-x+y*width))=static_cast<unsigned short>(tmp_buffer->at(x+y*width));
      }
  }
  
  delete (std::vector<float>*) tmp_buffer;
  fits_close_file(file, &status);

  inf << "Data read from file: " << filename << std::ends;
  emit(need_write_on_log_window(inf.str()));
  
  return true;
}
