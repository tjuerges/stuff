#ifndef READ_FITS_HXX
#define READ_FITS_HXX
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Feb 19, 2007  created
*/

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

#include <vector>
#include <string>
#include <ctime>
#include <qobject.h>
#include <fitsio.h>

#include "fits.hxx"
#include "image_type.hxx"
#include "iso_time.hxx"

class read_fits:public QObject
{
    Q_OBJECT
    public:
    read_fits(void)
    {
    };
    ~read_fits(void)
    {
    };
    std::string get_header(void) const;

    signals:
    void need_write_on_log_window(const std::string &);
    void need_write_on_log_window(const char *);
    void need_get_header(fits_c &);
    void need_read_fits_file(const std::string &, unsigned short &,
                 unsigned short &, std::vector < unsigned short >&);
    void need_get_filename(std::string &);
    void need_get_telescope_name(std::string &);
    void need_get_instrument_name(std::string &);
    void need_get_exp_time(float &);
    void need_get_object_name(std::string &);
    void need_get_observer_name(std::string &);
    void need_get_field_rotation(double &);
    void need_get_gain(float &);
    void need_get_pixel_width(float &);
    void need_get_pixel_height(float &);
    void need_get_filter_name(std::string &);
    void need_get_RA_coord(double &);
    void need_get_DEC_coord(double &);
    void need_get_focal_length(int &);
    void need_get_ccd_temperature(double &);
    void need_get_air_temperature(double &);
    void need_get_exposure_type(std::string &);

    public slots:
    void get_header(fits_c &) const;
    void get_focal_length(int &) const;
    void get_instrument_name(std::string &) const;
    void get_telescope_name(std::string &) const;
    bool read_file(const std::string &, unsigned short &, unsigned short &,
           std::vector < unsigned short >&);
    void get_filename(std::string &) const;
    void get_exposure_type(std::string &) const;
    void get_observer_name(std::string &) const;
    void get_object_name(std::string &) const;
    void get_ccd_temperature(double &) const;
    void get_air_temperature(double &) const;
    void get_exp_time(float &) const;
    void get_field_rotation(float &) const;
    void get_gain(float &) const;
    void get_pixel_width(float &) const;
    void get_pixel_height(float &) const;
    void get_filter_name(std::string &) const;
    void get_RA_coord(double &) const;
    void get_DEC_coord(double &) const;

    private:
    int read_header_keywords(fitsfile &);

    std::string FITSHeader;
    fits_c fits;
};
#endif
