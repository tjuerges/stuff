/*
  "@(#) $Id$"
*/

#ifndef IMAGE_TYPE_HXX
#define IMAGE_TYPE_HXX

//Define some exposure types
enum ImageType
{ BIAS, DARK, FLAT, OBJECT };
#endif
