/*
  "@(#) $Id$"
*/

#ifndef WRITE_FITS_HXX
#define WRITE_FITS_HXX

#include <string>
#include <ctime>
#include <qobject.h>

#include "fits.hxx"
#include "image_type.hxx"
#include "iso_time.hxx"

class write_fits:public QObject
{
  Q_OBJECT
public:
    write_fits(void):exp_time(0L), field_rotation_angle(0.), gain(0.),
    pixel_width(0.), pixel_height(0.), image_type(BIAS)
  {
  };
  ~write_fits(void)
  {
  };

signals:
  void need_write_on_log_window(const std::string&);
  void need_write_on_log_window(const char*);
  void need_get_header(fits_c&);
  void need_set_header(const fits_c&);
  void need_set_width(const int&);
  void need_set_height(const int&);
  void need_save_image(const iso_time&, const iso_time&,const unsigned short&, const unsigned short&,const unsigned short *);
  void need_write_fits_file(const std::string&, const unsigned int&,const unsigned int&, const unsigned short *);
  void need_write_fits_file(const std::string&,const fits_c&,const unsigned short *);
  void need_set_filename_root(const std::string &);
  void need_set_filename(const std::string &);
  void need_set_telescope_name(const std::string &);
  void need_set_instrument_name(const std::string &);
  void need_set_exp_time(unsigned long&);
  void need_set_object_name(const std::string &);
  void need_set_observer_name(const std::string &);
  void need_set_field_rotation(const double&);
  void need_set_gain(const float&);
  void need_set_pixel_width(const float&);
  void need_set_pixel_height(const float&);
  void need_set_filter_name(const std::string &);
  void need_set_RA_coord(const double&);
  void need_set_DEC_coord(const double&);
  void need_set_focal_length(const int&);
  void need_set_ccd_temperature(const double&);
  void need_set_air_temperature(const double&);

public slots:
  void set_header(const fits_c&);
  void set_width(const int&);
  void set_height(const int&);
  void set_focal_length(const int&);
  void set_instrument_name(const std::string &);
  void set_telescope_name(const std::string &);
  void write_fits_file(const std::string&,const unsigned int&, const unsigned int&,const unsigned short *);
  void write_fits_file(const std::string&,const fits_c& header,const unsigned short*);
  void set_filename_root(const std::string &);
  void set_filename(const std::string &);
  void set_exposure_type(const int&);
  void set_observer_name(const std::string &);
  void set_object_name(const std::string &);
  void set_ccd_temperature(const double&);
  void set_air_temperature(const double&);
  void set_exp_time(const unsigned long&);
  void set_field_rotation(const double&);
  void set_gain(const float&);
  void set_pixel_width(const float&);
  void set_pixel_height(const float&);
  void set_filter_name(const std::string &);
  void set_RA_coord(const double&);
  void set_DEC_coord(const double&);

protected:
  void save_image(const iso_time&, const iso_time&, const unsigned short&,const unsigned short&, const unsigned short *);
  std::string filename, filename_root;
  unsigned long exp_time;
  double field_rotation_angle;
  float gain;
  float pixel_width, pixel_height;
  int image_type;
  fits_c fits;
};
#endif
