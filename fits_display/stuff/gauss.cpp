/*
  "@(#) $Id$"
*/

/* Adapted from XVista package.
   Thomas Juerges
*/
#include <cmath>
#include <vector>
#include <algorithm>

#include "gauss.hxx"
#include "guide_star.hxx"
#include "box.hxx"

//If you want some informational output...
#undef DEBUG
#undef DEBUG2
//#define DEBUG 1
//#define DEBUG2 1
#ifdef DEBUG
#include <iostream>
#endif

//this is the smallest total number of pixels above sky to accept
/*Not necessary anymore. We check in do_fwhm:
  (pixel count<0.?pixel count=0:do nothing)
  #define SMALL_SUM 1.0
*/

GaussFit::GaussFit()
{
}

GaussFit::~GaussFit()
{
    delete(std::vector<unsigned short>*) data;
};

GaussFit::GaussFit(const std::vector<unsigned short>&d)
{
    data=new std::vector<unsigned short>(d);
}

int GaussFit::do_it(bool do_guess_sky, unsigned short width, box& cursor_box, guide_star & my_guide)
{
    if(data->size()==0)
	return GAUSS_NOT_ENOUGH_POINTS;

  my_guide.peak = my_guide.fwhm = my_guide.fwhm_arcsec = my_guide.col_cen =
    my_guide.col_width = my_guide.row_cen = my_guide.row_width =
    my_guide.eccen = my_guide.major_axis = my_guide.minor_axis = 0.;
  // make the fit out as far as the smaller of the box dimensions
  my_guide.fitrad =
    (cursor_box.h > cursor_box.w ? cursor_box.w : cursor_box.h);

  if(do_guess_sky)
    guess_sky(width, cursor_box, my_guide);
  else
    my_guide.sky = 0;

  return (do_axes(width, cursor_box, my_guide));
}

/*
  Guess the "sky" value by averging the pixel values around
  the box of data.
*/
/*FIXME: Has to be fixed for cursor boxes on the image border.
  We run into trouble when we do not check for pixels coordinates outside
  the image bounds or wrapped pixels. Wrapped pixels: think of a cursor box
  at the left image border. The border, we calculate the sky from, is taken
  one pixel to the left of the border, i.e. the right border of the image one
  pixel shifted up.
*/
void GaussFit::guess_sky(unsigned short width, box& cursor_box,guide_star & my_guide)
{
  unsigned int counter = 0;
  double sky = 0.;

  for(unsigned int i=cursor_box.x;i<(cursor_box.x+cursor_box.w);i++)
    {
      sky += (*data)[i + cursor_box.y * width];
      sky += (*data)[i + (cursor_box.y + cursor_box.h - 1) * width];
      counter += 2;
    }
  for(unsigned int i=cursor_box.y;i<(cursor_box.y+cursor_box.h);i++)
    {
      sky += (*data)[i * width + cursor_box.x];
      sky += (*data)[i * width + cursor_box.x + cursor_box.w - 1];
      counter += 2;
    }
  sky /= (double) counter;
  my_guide.sky = sky;
#ifdef DEBUG
  std::cerr << "sky=" << my_guide.sky << std::endl;
#endif
}

/*
 *  do the actual computations on the data-find centroid, FWHM,
 *  and so forth... some of the algorithms herein are based upon 
 *  those found in the VISTA code of Lauer and Stover.
 *
 *  modified 10/3/92 by MWR to use a gaussian fitted to the marginal
 *  light distributions in each direction for the light-center,
 *  rather than the first moment of the light, AND to fit a 1-d
 *  gaussian to the radial profile of the star as well. 
 *
 *  We use only pixels within "fitrad" of the center of the box
 *  to measure the centroid.  However, we use all pixels in the
 *  box to measure second moments and orientation ....
 *
 *  This routine assumes that sky has NOT been subtracted from the
 *  data yet, and subtracts the given "sky" value as it goes.
 *
 *  The output arguments are:
 *
 *     row_cen           centroid in row direction, from gaussian fitted
 *                            to marginal sums
 *     col_cen           centroid in col direction, from gaussian fitted
 *                            to marginal sums
 *     row_width         FWHM of a gaussian fitted to marginal sums along
 *                            the row direction
 *     col_width         FWHM of a gaussian fitted to marginal sums along
 *                            the col direction
 *     fwhm              FWHM of a circular gaussian fitted to radial profile
 *     peak              peak of the fitted gaussian
 *     eccen             eccentricity of object, based on second moments
 *     major_axis        position angle of major axis, in degrees
 *     minor_axis        position angle of minor axis, in degrees
 * 
 * The function returns 0 if all goes well, or
 *              returns 1 if there's a problem (and prints error messages)
 */
int GaussFit::do_axes(unsigned short width, box& cursor_box, guide_star & my_guide)
{
  unsigned int row, col, bigger;
  unsigned int srow, scol, erow, ecol;
  int px, py;
  float chisq, amp, center, x_width, y_width;
  double pix, sum, sumx, sumy, max, xc_ax, yc_ax;
  double sumxx, sumxy, sumyy, a, b, c, temp;
  double eigen1, eigen2, xmaj, xminor, ymaj, yminor;
  double angmaj_x, angmin_x, ecc_ax;
  double circ_fwhm, peak_value;

  /*
     compute the centroid, using only pixels within 'fitrad' of the center of 
     the box 
   */
  sum = 0.;
  sumx = 0.;
  sumy = 0.;
  px = 0;
  py = 0;
  max = 0.;

  if((srow =
      static_cast<unsigned int> (cursor_box.y + cursor_box.h / 2 + 0.5 -
                      my_guide.fitrad)) < cursor_box.y)
    srow = cursor_box.y;
  if((scol =
      static_cast<unsigned int> (cursor_box.x + cursor_box.w / 2 + 0.5 -
                      my_guide.fitrad)) < cursor_box.x)
    scol = cursor_box.x;
  if((erow =
      static_cast<unsigned int> (cursor_box.y + cursor_box.h / 2 + 0.5 +
                      my_guide.fitrad)) >= cursor_box.y + cursor_box.h)
    erow = cursor_box.y + (cursor_box.h - 1);
  if((ecol =
      static_cast<unsigned int> (cursor_box.x + cursor_box.w / 2 + 0.5 +
                      my_guide.fitrad)) >= cursor_box.x + cursor_box.w)
    ecol = cursor_box.x + (cursor_box.w - 1);

  for(row = srow; row <= erow; row++)
    for(col = scol; col <= ecol; col++)
      {
        pix = static_cast<double>((*data)[row * width + col]) - my_guide.sky;
        if(pix < 0.)            // Added by TJ -> No need for SMALL_SUM
          // anymore.
          pix = 0.;
        sum += pix;
        sumx += (pix * row);
        sumy += (pix * col);
        if(pix > max)
          {
            px = row;
            py = col;
            max = pix;
          }
      }

  /*
     TJ: replaced by some lines above: if(pix<0.)... this test will fail if
     the user has supplied a "sky" value much higher than appropriate.  Too
     bad for him. if(sum<SMALL_SUM) { QString inf; inf.sprintf("ERROR: not
     enough signal to compute axes! sky=%g, ret=%g\n",my_guide.sky,sum);
     emit(need_write_on_log_window(inf)); return(-1); } 
   */

  xc_ax = sumx / sum;           /* centroid in x */
  yc_ax = sumy / sum;           /* centroid in y */
#ifdef DEBUG
  std::cout.precision(10);
  std::cerr << "done with first moments, sum is " << sum << " center is (" <<xc_ax << ", " << yc_ax << ")" << std::endl;
#endif

  /*
     allocate the std::vectors we'll need 
   */
  if(cursor_box.h > cursor_box.w)
    bigger = cursor_box.h;
  else
    bigger = cursor_box.w;

  std::vector < float >x(bigger), y(bigger), sig(bigger);
  if(x.empty())
    {
      return (GAUSS_NO_MEM_X);
    }
  if(y.empty())
    {
      return (GAUSS_NO_MEM_Y);
    }
  if(sig.empty())
    {
      return (GAUSS_NO_MEM_Z);
    }

  x.assign(x.size(),0);
  y.assign(y.size(),0);

  for(unsigned int i = 0; i < bigger; i++)
    sig[i] = 1.0;

  // first do along the row (X) direction 
  for(row = srow; row <= erow; row++)
    {
      x[row - srow] = row;
      y[row - srow] = 0;
      for(col = scol; col <= ecol; col++)
        y[row - srow] += ((*data)[row * width + col] - my_guide.sky);
    }
  if(gauss_fit(x, y, sig, (1 + erow - srow), amp, center, x_width, chisq) !=
     0)
    {
#ifdef DEBUG
      std::cerr << "ERROR: do_axes: gauss_fit has problem in X-direction" <<std::endl;
#endif
      x_width = -1;             // signal to use 2nd moment later on
    }
  else
    {
      my_guide.row_width = x_width;
      xc_ax = center;
    }

#ifdef DEBUG
  std::cout.precision(10);
  std::cerr << "done with fitting X center, is " << xc_ax << std::endl;
#endif

  // now do along the col (Y) direction
  for(col = scol; col <= ecol; col++)
    {
      x[col - scol] = col;
      y[col - scol] = 0;
      for(row = srow; row <= erow; row++)
        y[col - scol] += ((*data)[row * width + col] - my_guide.sky);
    }
  if(gauss_fit(x, y, sig, (1 + ecol - scol), amp, center, y_width, chisq) !=
     0)
    {
#ifdef DEBUG
      std::
        cerr << "ERROR: do_axes: gauss_fit has problem in Y-direction" <<
        std::endl;
#endif
      y_width = -1;             // signal to use 2nd moment later on
    }
  else
    {
      my_guide.col_width = y_width;
      yc_ax = center;
    }

#ifdef DEBUG
  std::cout.precision(10);
  std::cerr << "done with fitting Y center, is " << yc_ax << std::endl;
#endif

  if((xc_ax < srow) || (xc_ax > erow))
    {
      return (GAUSS_ROW_OUT_OF_BOUNDS);
    }
  if((yc_ax < scol) || (yc_ax > ecol))
    {
      return (GAUSS_COL_OUT_OF_BOUNDS);
    }

  // find some value for the FWHM of the object
  int circ_gauss_return =
    circular_gaussian(width, cursor_box, my_guide, xc_ax, yc_ax,
                      peak_value, circ_fwhm);
  if(circ_gauss_return != GAUSS_NO_ERROR)
    return (GAUSS_CIRC_GAUSS_ERROR);
#ifdef DEBUG
  std::cout.precision(10);
  std::cerr << "done with fitting FWHM: peak " << peak_value << ", fwhm=";
  std::cout.precision(8);
  std::cerr << circ_fwhm << std::endl;
#endif

  /*
     now calculate the quadrupole moment-we need it to find the object's
     principal axes AND the object's "width", if either the X or Y
     gaussian-fitting failed. 
   */
  sumxx = 0.0;
  sumyy = 0.0;
  sumxy = 0.0;
  for(row = srow; row < erow; row++)
    {
      for(col = scol; col < ecol; col++)
        {
          pix = (*data)[row * width + col] - my_guide.sky;
          sumxx += ((row - xc_ax) * (row - xc_ax) * pix);
          sumxy += ((row - xc_ax) * (col - yc_ax) * pix);
          sumyy += ((col - yc_ax) * (col - yc_ax) * pix);
        }
    }
  a = sumxx / sum;
  b = sumxy / sum;
  c = sumyy / sum;
  if(x_width == -1.0)
    x_width = a;
  if(y_width == -1.0)
    y_width = c;

  // find the eigenvalues
  temp = sqrt((a - c) * (a - c) + 4 * b * b);
  eigen1 = (a + c + temp) / 2.0;
  eigen2 = (a + c - temp) / 2.0;
  if(eigen1 == 0.0)
    eigen1 = 1.0;
  // if the eigenvalues imply imaginary eccentricity, set it to 0.0
  ecc_ax = 1.0 - (eigen2 / eigen1);
  if(ecc_ax <= 0.0)
    ecc_ax = 0.0;
  else
    ecc_ax = sqrt(ecc_ax);

  // major axes
  if(b == 0.0)
    {
      if(a >= c)
        {
          xmaj = 1.0;
          ymaj = 0.0;
        }
      else
        {
          xmaj = 0.0;
          ymaj = 1.0;
        }
    }
  else
    {
      xmaj = 1.0;
      ymaj = (eigen1 - a) / b;
      temp = sqrt(xmaj * xmaj + ymaj * ymaj);
      xmaj = xmaj / temp;
      ymaj = ymaj / temp;
    }

  // minor axes
  if(b == 0.0)
    {
      if(a < c)
        {
          xminor = 1.0;
          yminor = 0.0;
        }
      else
        {
          xminor = 0.0;
          yminor = 1.0;
        }
    }
  else
    {
      xminor = 1.0;
      yminor = (eigen2 - a) / b;
      temp = sqrt(xminor * xminor + yminor * yminor);
      xminor = xminor / temp;
      yminor = yminor / temp;
    }

  angmaj_x = 90.0 + atan2(-ymaj, xmaj) * 57.3;
  angmin_x = 90.0 + atan2(-yminor, xminor) * 57.3;

#ifdef DEBUG
  std::cerr << "done with second-moment calculations" << std::endl;
#endif

  /*
   * if we got here, all is well, and we can place results into
   * the output arguments.
   */
  my_guide.row_cen = xc_ax;
  my_guide.col_cen = yc_ax;
  my_guide.fwhm = circ_fwhm;
  my_guide.peak = peak_value;
  my_guide.eccen = ecc_ax;
  my_guide.major_axis = angmaj_x;
  my_guide.minor_axis = angmin_x;

  return (GAUSS_NO_ERROR);
}

/*
 * Calculate the FWHM of the object at the given center of the
 * given small area of data.  We assume the object is
 * axisymmetric, and fit a single 1-D gaussian to the radial profile.
 *
 * Subtract the "sky" value from each pixel as we go.
 *
 * Ignore all points more than 'fitrad' pixels from the center.
 *
 * We double the actual number of points, placing a copy of each
 * point at its negative radius.  Thus, we create a perfectly
 * symmetric profile, in which each point appears once in the
 * positive and once in the negative direction.  8/2/96 MWR
 *
 * Return 
 *     0    if we could calculate amplitude and FWHM
 *    -1    to indicate an error.
 */

int GaussFit::circular_gaussian(unsigned short width, box& cursor_box,
                      guide_star& my_guide, const double cr, const double cc,
                      double &peak, double &fwhm)
{
  int ret, sign;
  float amp, center, _org_width, chisq;
  double dx, dy, dr, pix;

  unsigned int ndata = 2 * cursor_box.h * cursor_box.w;
  std::vector < float >x(ndata), y(ndata), sig(ndata);
  if(x.empty())
    {
      return (GAUSS_CIRC_GAUSS_NO_MEM_X);
    }
  if(y.empty())
    {
      return (GAUSS_CIRC_GAUSS_NO_MEM_Y);
    }
  if(sig.empty())
    {
      return (GAUSS_CIRC_GAUSS_NO_MEM_SIG);
    }
  x.assign(x.size(),0);
  y.assign(y.size(),0);
  sig.assign(sig.size(),0);

  unsigned int i = 0;
  sign = 1;
  for(unsigned int row = cursor_box.y; row < cursor_box.y + cursor_box.h;
      row++)
    {
      dx = row - cr;
      for(unsigned int col = cursor_box.x; col < cursor_box.x + cursor_box.w;
          col++)
        {
          pix = (*data)[row * width + col] - my_guide.sky;
          dy = col - cc;
          dr = sqrt(dx * dx + dy * dy);
          if(dr < my_guide.fitrad)
            {
              x[i] = dr;
              y[i] = pix;
              sig[i++] = 1.0;
              x[i] = -dr;
              y[i] = pix;
              sig[i++] = 1.0;
            }
        }
    }
#ifdef DEBUG
  std::
    cerr << "circular_gaussian: done with filling arrays for gauss_fit" <<
    std::endl;
#endif
  ret = gauss_fit(x, y, sig, i, amp, center, _org_width, chisq);
#ifdef DEBUG
  std::cerr << "circular_gaussian: done with call to gauss_fit" << std::endl;
  std::cout.precision(10);
  std::cerr << "peak before=" << amp << std::endl;
#endif
  if(ret == 0)
    {
      fwhm = 2.35 * _org_width;
      peak = amp;
    }

  return (ret);
}

/* a routine that fits a gaussian to some data, without using ANY
   Numerical Recipes routines at all.  It makes an initial guess as
   the parameters, then iterates, trying to minimize chi-square for
   each parameter independently in each iteration.  Seems to work
   pretty well, even given data only on one side of the gaussian peak.

   if it the fit doesn't converge, it returns SDSS_ERROR and the values
   of the parameters are left untouched.  If it does converge, the
   function returns SDSS_OK and the values are placed into the 
   passed parameters.   MWR 12/7/92. */

/* 12/8/92
 *          -changed name of function from 'gauss_fit' to 'gauss_fit'
 *          -now include "utils.h"
 *               ... MWR
 */

/* 12/11/92
 *          -modified version for non-SDSS use in XVista.
 *               ... MWR
 8/24/93-check for maximum value of exp function -rrt
*/

/* find the gaussian that best fits the given x[] and y[] data;
   return the amplitude (A), center (m) and width (s) of the gaussian,
   where
   (x-m)^2
   gaussian=A exp(---------)
   2*s^2

   and the formal chisq uncertainty in the fit.  Note that if the
   user doesn't know (or care) what the sigma in each
   y[] point is, he can pass values of 1.0 in each sig[] and
   then ignore the chisq.  Only "real" values for sig[] produce
   a meaningful chisq, but this function always returns it. */

/* return with -1 if the user passes fewer than 3 points, or if
   the parameters don't converge within the maximum number of
   iterations.  If there is convergence, return 0 */
int GaussFit::gauss_fit(const std::vector < float >&x, const std::vector < float >&y,
              const std::vector < float >&sig, const unsigned int ndata,
              float &amp, float &center, float &_org_width, float &chisq)
{
  int sigflag;
  std::vector < int >sign(3);
  std::vector < float >a(3), newa(3), alpha(3), deriv(3);
  float oldchisq, x1, x2, chi;

  if(ndata < 3)
    {
      return (GAUSS_NOT_ENOUGH_POINTS);
    }

  /*
     make some initial guesses to the parameters here 
   */
  guess_params(x, y, ndata, a[0], a[1], a[2]);

  for(unsigned int i = 0; i < 3; i++)
    alpha[i] = fabs(a[i]) * FACTOR;

  std::vector < float >sig2(ndata);
  if(sig2.empty())
    {
#ifdef DEBUG
      std::
        cerr << "gauss_fit: couldn't malloc for sig2 - will be slow" << std::
        endl;
#endif
      sigflag = 0;
    }
  else
    {
      sigflag = 1;
      for(unsigned int i = 0; i < ndata; i++)
        sig2[i] = sig[i] * sig[i];
    }

  // first, we find chisq given the guessed parameters
  chi = find_chisq(x, y, sig, sig2, sigflag, ndata, a[0], a[1], a[2]);
  oldchisq = chi;

  unsigned int iter = 0;
#ifdef DEBUG
  std::cout.precision(13);
  std::
    cerr << "iter " << iter << ": a[0]=" << a[0] << ", a[1]=" << a[1] <<
    ", a[2]=" << a[2] << "  chi=" << chi << std::endl;
#endif

  do
    {
      /*
         now, find the change in chisq w.r.t. change in each of the three
         parameters.  use alpha[i] as the change in parameter[i] used to find 
         the derivative empirically 
       */
      for(unsigned int i = 0; i < 3; i++)
        {
          newa[i] = a[i];
#ifdef DEBUG2
          std::cerr << "  param " << i << ": ";
#endif
          if(alpha[i] < TINY)
            {
#ifdef DEBUG2
              std::cerr << "  skipping" << std::endl;
#endif
              continue;
            }
          a[i] -= alpha[i];
          x1 = find_chisq(x, y, sig, sig2, sigflag, ndata, a[0], a[1], a[2]);
#ifdef DEBUG2
          std::cout.precision(13);
          std::cerr << a[i] << " " << x1 << "  ";
#endif
          a[i] += 2.0 * alpha[i];
          x2 = find_chisq(x, y, sig, sig2, sigflag, ndata, a[0], a[1], a[2]);
#ifdef DEBUG2
          std::cout.precision(13);
          std::cerr << a[i] << " " << x2 << "  ";
#endif
          a[i] -= alpha[i];     /* put a[i] back to where it started */
          deriv[i] = (x2 - x1) / (2.0 * alpha[i]);
#ifdef DEBUG2
          std::cout.precision(13);
          std::cerr << deriv[i] << " ";
#endif
          sign[i] = ((deriv[i] > 0) ? 1 : -1);
          /*
             now, we want to make the derivative zero by making (x2-x1) equal 
             to zero.  So, we want to move a[i] in the direction OPPOSITE the 
             deriv, by some amount-let's say, alpha[i]. If the deriv doesn't
             change sign on either side of the current a[i], then leave the
             amount alpha[i] alone and use a new value for a[i]. If it does
             change, we're close to a minimum; therefore, then cut alpha[i]
             by a factor of 4 and don't change a[i] in this iteration. 
           */
          if(fabs(deriv[i]) > TINY)
            {
              if(((deriv[i] < 0) && (x2 < chi))
                 || ((deriv[i] > 0) && (x1 < chi)))
                {
                  newa[i] = a[i] - sign[i] * alpha[i];
#ifdef DEBUG2
                  std::cout.precision(13);
                  std::cerr << "new " << newa[i] << std::endl;
#endif
                  alpha[i] = fabs(newa[i]) * FACTOR;
                }
              else
                {
                  newa[i] = a[i];       // leave it as is...
#ifdef DEBUG2
                  std::cout.precision(13);
                  std::cerr << "div " << newa[i] << std::endl;
#endif
                  alpha[i] *= 0.25;
                }
            }
#ifdef DEBUG2
          else
            {
              std::cout.precision(13);
              std::cerr << "sam " << a[i] << std::endl;
            }
#endif
        }
      for(unsigned int i = 0; i < 3; i++)
        a[i] = newa[i];

      chi = find_chisq(x, y, sig, sig2, sigflag, ndata, a[0], a[1], a[2]);
      iter++;
#ifdef DEBUG
      std::cout.precision(13);
      std::
        cerr << "iter " << iter << ": a[0]=" << a[0] << ", a[1]=" << a[1] <<
        ", a[2]=" << a[2] << "  chi=" << chi << std::endl;
#endif

      if(fabs(chi - oldchisq) < TINY)
        break;
      oldchisq = chi;
    }
  while (iter < MAXITER);

  // If we didn't converge, just return with best numbers so far
  // Maybe we want to print out a warning message here?
  if(iter == MAXITER)
    a[0] = a[0];                // to placate compiler

  amp = a[0];
  center = a[1];
  /*
     note that my "gauss" function doesn't use the "2*sigma^2", so we have to 
     divide here by sqrt(2) 
   */
  _org_width = a[2] / sqrt(2.0);
  chisq = chi;
  return (GAUSS_NO_ERROR);
}

/* guess the values of the best-fitting gaussian.  I hope we don't
   have to be too close */
void GaussFit::guess_params(const std::vector < float >&x, const std::vector < float >&y,
                  const unsigned int ndata, float &amp, float &center,
                  float &_org_width)
{
  float max, min, sumy, sumxy, sumxyy;

  // guess that the amplitude is (min-max)
  min = y[0];
  max = y[0];
  for(unsigned int i = 0; i < ndata; i++)
    {
      if(y[i] < min)
        min = y[i];
      if(y[i] > max)
        max = y[i];
    }
  amp = max - min;

  // guess that the center is given by the first moment
  sumy = 0.0;
  sumxy = 0.0;
  for(unsigned int i = 0; i < ndata; i++)
    {
      sumy += y[i];
      sumxy += x[i] * y[i];
    }
  if(sumy == 0.0)
    center = 0.0;
  else
    center = sumxy / sumy;

  // guess that the width is sqrt(the second moment)
  sumxyy = 0.0;
  for(unsigned int i = 0; i < ndata; i++)
    sumxyy += (x[i] - center) * (x[i] - center) * y[i];
  if(sumy == 0.0)
    _org_width = 1.0;
  else
    {
      _org_width = sumxyy / sumy;
      if(_org_width < 0.0)
        _org_width = 1.0;
      else
        _org_width = sqrt(static_cast<double>(_org_width));
    }
}

/* calculate the value of chisq given the data and the values of
   the gaussian parameters. return the chisq.  If the argument
   "sigflag" is equal to 1, then the "sig2" argument contains
   sig[i] squared-use it, to speed up calculations.  If the 
   "sigflag" argument is zero, though, actually square each element
   of the "sig" array.  Slower, but necessary if we couldn't allocate
   space for the "sig2" array */
float GaussFit::find_chisq(const std::vector < float >&x, const std::vector < float >&y,
                 const std::vector < float >&sig,
                 const std::vector < float >&sig2, const int sigflag,
                 const unsigned int ndata, const float amp,
                 const float center, float &_org_width)
{
  float yy, sum, arg, ex;

#ifdef DEBUG3
  std::cout.precision(13);
  std::
    cerr << "find_chisq: given a[0]=" amp << ", a[1]=" << center << ", a[2]="
    << _org_width << std::endl;
#endif

  sum = 0;

  /*
     this is a hack, but if someone passes "width=0", we have to do
     _something_. 
   */
  if(fabs(_org_width) < TINY)
    _org_width = TINY;

  if(sigflag == 1)
    {
      for(unsigned int i = 0; i < ndata; i++)
        {
          arg = (x[i] - center) / _org_width;
          if(fabs(arg) < MAX_ARG)
            ex = exp(-arg * arg);
          else
            ex = 0.;
          yy = amp * ex;
          sum += ((yy - y[i]) * (yy - y[i]) / sig2[i]);
#ifdef DEBUG3
          std::cout.precision(13);
          std::
            cerr << "   x=" << x[i] << "  y=" << y[i] << " yy=" << yy <<
            "  sum=" << sum << std::endl;
#endif
        }
    }
  else
    {
      for(unsigned int i = 0; i < ndata; i++)
        {
          arg = (x[i] - center) / _org_width;
          if(fabs(arg) < MAX_ARG)
            ex = exp(-arg * arg);
          else
            ex = 0.;
          yy = amp * ex;
          sum += ((yy - y[i]) * (yy - y[i]) / (sig[i] * sig[i]));
#ifdef DEBUG3
          std::cout.precision(13);
          std::
            cerr << "   x=" << x[i] << "  y=" << y[i] << " yy=" << yy <<
            "  sum=" << sum << std::endl;
#endif
        }
    }

  return (sum);
}
