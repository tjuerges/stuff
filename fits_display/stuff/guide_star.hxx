/*
  "@(#) $Id$"
*/

#ifndef GUIDE_STAR_HXX
#define GUIDE_STAR_HXX

#include <qstring.h>

class guide_star
{
public:
  guide_star():valid(FALSE), inf(""), x(0), y(0), fitrad(0), sky(0.),
    peak(0.), fwhm(0.), fwhm_arcsec(0.), row_cen(0.), row_width(0.),
    col_cen(0.), col_width(0.), eccen(0.), major_axis(0.), minor_axis(0.)
  {
  };
  guide_star(const guide_star & old_guide)
  {
    valid = old_guide.valid;
    inf = old_guide.inf;
    x = old_guide.x;
    y = old_guide.y;
    fitrad = old_guide.fitrad;
    sky = old_guide.sky;
    peak = old_guide.peak;
    fwhm = old_guide.fwhm;
    fwhm_arcsec = old_guide.fwhm_arcsec;
    row_cen = old_guide.row_cen;
    row_width = old_guide.row_width;
    col_cen = old_guide.col_cen;
    col_width = old_guide.col_width;
    eccen = old_guide.eccen;
    major_axis = old_guide.major_axis;
    minor_axis = old_guide.minor_axis;
  };
  ~guide_star()
  {
  };
  bool operator==(const guide_star & old_guide) const
  {
    if(valid != old_guide.valid)
      return false;
    if(inf != old_guide.inf)
      return false;
    if(x != old_guide.x)
      return false;
    if(y != old_guide.y)
      return false;
    if(fitrad != old_guide.fitrad)
      return false;
    if(sky != old_guide.sky)
      return false;
    if(peak != old_guide.peak)
      return false;
    if(fwhm != old_guide.fwhm)
      return false;
    if(fwhm_arcsec != old_guide.fwhm_arcsec)
      return false;
    if(row_cen != old_guide.row_cen)
      return false;
    if(row_width != old_guide.row_width)
      return false;
    if(col_cen != old_guide.col_cen)
      return false;
    if(col_width != old_guide.col_width)
      return false;
    if(eccen != old_guide.eccen)
      return false;
    if(major_axis != old_guide.major_axis)
      return false;
    if(minor_axis != old_guide.minor_axis)
      return false;
    return true;
  };
  guide_star & operator=(const guide_star & old_guide)
  {
    if(static_cast < const guide_star & >(*this) == old_guide)
      return (static_cast < guide_star & >(*this));
    else
      {
        valid = old_guide.valid;
        inf = old_guide.inf;
        x = old_guide.x;
        y = old_guide.y;
        fitrad = old_guide.fitrad;
        sky = old_guide.sky;
        peak = old_guide.peak;
        fwhm = old_guide.fwhm;
        fwhm_arcsec = old_guide.fwhm_arcsec;
        row_cen = old_guide.row_cen;
        row_width = old_guide.row_width;
        col_cen = old_guide.col_cen;
        col_width = old_guide.col_width;
        eccen = old_guide.eccen;
        major_axis = old_guide.major_axis;
        minor_axis = old_guide.minor_axis;

        return (static_cast < guide_star & >(*this));
      }
  };
  void copy(const guide_star & old_guide)
  {
    valid = old_guide.valid;
    inf = old_guide.inf;
    x = old_guide.x;
    y = old_guide.y;
    fitrad = old_guide.fitrad;
    sky = old_guide.sky;
    peak = old_guide.peak;
    fwhm = old_guide.fwhm;
    fwhm_arcsec = old_guide.fwhm_arcsec;
    row_cen = old_guide.row_cen;
    row_width = old_guide.row_width;
    col_cen = old_guide.col_cen;
    col_width = old_guide.col_width;
    eccen = old_guide.eccen;
    major_axis = old_guide.major_axis;
    minor_axis = old_guide.minor_axis;
  };

  bool valid;
  QString inf;
  int x;
  int y;
  int fitrad;
  double sky;
  double peak;
  double fwhm;
  double fwhm_arcsec;
  double row_cen;
  double row_width;
  double col_cen;
  double col_width;
  double eccen;
  double major_axis;
  double minor_axis;
};
#endif
