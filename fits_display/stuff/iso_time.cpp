/*
  "@(#) $Id$"
*/

#include <string>

#include "iso_time.hxx"

std::string iso_time::get_time_string_iso(void) const
{
  char time_string[64];
  strftime(time_string, 64, "%Y-%m-%dT%H:%M:%S", tm_time);
  std::string time_date(time_string);
  return (time_date);
}

std::string iso_time::get_time_string_no_iso(void) const
{
  char time_string[64];
  strftime(time_string, 64, "%Y%m%d-%H%M%S", tm_time);
  std::string time_date(time_string);
  return (time_date);
}

std::string iso_time::get_time_string_no_iso_with_spaces(void) const
{
  char time_string[64];
  strftime(time_string, 64, "%Y/%m/%d %H:%M:%S", tm_time);
  std::string time_date(time_string);
  return (time_date);
}

long iso_time::get_microseconds(void) const
{
  return (tv.tv_usec);
}

double iso_time::get_epoch(void) const
{
  return (epoch);
}
