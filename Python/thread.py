#! /usr/bin/env python
# $Id$

import time
import signal
import thread

stop = False

def worker(lock, a, b):
	global stop

	while True:
		lock.acquire()
		if stop == True:
			lock.release()
			break
		else:
			lock.release()

		print "Yo, I am still working... %d, %d" % (a, b)
		time.sleep(1.0)

print "Starting thread..."
lock = thread.allocate_lock()
t = thread.start_new_thread(worker, (lock, 1, 2))
print "Let the thread run for a while..."
time.sleep(4)
print "Stop the thread..."
lock.acquire()
stop = True
lock.release()
print "Good bye!"
