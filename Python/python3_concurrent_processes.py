#! /usr/bin/env python3

import concurrent.futures
import time
import random
import math

# Demonstrate the use of multiprocessing in Python3 by
# using the concurrent.futures.ProcessPoolExecutor function.

MAX_JOBS = 20
SLEEP_TIME = 0.01
ITERATIONS = 100

# Worker process
def job(job_identifier, sleep_time = 0.0, max_iterations = 0):
    iteration = max_iterations
    while iteration > 0:
        iteration -= 1
        number = math.sin(random.random() / math.pi)
        print("Job\t#%d:\titeration #%d\tnumber = %f" % (job_identifier, max_iterations - iteration, number))
        if sleep_time is not None:
            time.sleep(sleep_time)
    return job_identifier

def main():
    with concurrent.futures.ProcessPoolExecutor(max_workers = MAX_JOBS) as executor:
        future_to_job_identifier = {executor.submit(job, id, SLEEP_TIME, ITERATIONS): id for id in range(MAX_JOBS)}
        for i in range(100):
            print()
        for future in concurrent.futures.as_completed(future_to_job_identifier):
            id = future_to_job_identifier[future]
            try:
                data = future.result()
            except Exception as e:
                print('Job\t#%d:\texception %s' % (id, e))
            else:
                print('Job\t#%d:\treturned %s' % (id, data))

if __name__ == '__main__':
    main()
