import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.path as path


numBins = 10


fig = plt.figure()
ax = fig.add_subplot(111)

f = file("dataFile")
input = f.readlines()
f.close()

data = []
for i in input:
    data.append(float(i.replace("\n", "")))
n, bins = np.histogram(data, numBins)

left = np.array(bins[: - 1])
right = np.array(bins[1:])
bottom = np.zeros(len(left))
top = bottom + n

XY = np.array([[left, left, right, right], [bottom, top, top, bottom]]).T
barpath = path.Path.make_compound_path_from_polys(XY)
patch = patches.PathPatch(barpath, facecolor = "blue, edgecolor = "gray", alpha = 0.8)
ax.add_patch(patch)
ax.set_xlim(left[0], right[-1])
ax.set_ylim(bottom.min(), top.max())

plt.show()
