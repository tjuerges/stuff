#! /usr/bin/env python

def bit(value = 0, bitNum = 0):
	bit = 1 << bitNum
	bitValue = value & bit
	bitValue >>= bitNum

	return bitValue

def bin(number = 0):
	prefix = "0%"
	string = ""

	if number == 0:
		string += "0"
	else:
		while number > 0:
			string = chr(48 + (number & 1)) + string
			number >>= 1

	return (prefix + string)


#for i in range(1, 15):
#	for j in range(0, i):
#		print "i = %d, j = %d -> %d" % (i, j, bit(i, j))

print bin(0x3f)
