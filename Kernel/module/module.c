/*
 * @(#) $Id$
 */

#include <linux/module.h>
#include <linux/kernel.h>

int init_module(void)
{
         printk("Hello world %d\n", request_module("rtai_hal"));
         return 0;
}

void cleanup_module(void)
{
        printk("Good Bye\n");
}
