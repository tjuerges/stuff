#include <stdio.h>

int main( int argc, char **argv )
{
    int uid;

    if( argc != 2 ) {
        printf("usage: %s uid\n", argv[0] );
        return -1;
    }
    uid = atoi( argv[1] );
    printf("setting to uid %d\n", uid );
    if( setuid( uid )!=0 )
	    perror("setuid");
    return 0;
}
