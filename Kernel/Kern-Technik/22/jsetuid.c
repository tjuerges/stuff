#include <linux/module.h>
#include <linux/fs.h>
#include <linux/kprobes.h>
#include <linux/kallsyms.h>

static void jsetuid( uid_t uid )
{
	printk("jsetuid( %d )\n", uid );
	jprobe_return();
}

static struct jprobe jp = {
	.entry   = (kprobe_opcode_t *) jsetuid,
};

static int __init probe_init(void)
{
	jp.kp.addr = (kprobe_opcode_t *) kallsyms_lookup_name("sys_setuid");
	if( jp.kp.addr == NULL ) {
		printk("kallsyms_lookup_name could not find address"
			"for the specified symbol name\n");
		return 1;
	}
	register_jprobe(&jp);
	printk("jprobe registered\n");
	return 0;
}

static void __exit probe_exit(void)
{
	unregister_jprobe(&jp);
	printk("jprobe unregistered\n");
}

module_init( probe_init );
module_exit( probe_exit );
MODULE_LICENSE("GPL");
