#include <linux/module.h>
#include <linux/kprobes.h>
#include <linux/kallsyms.h>

extern unsigned long kallsyms_lookup_name(const char *name);

static int call_count = 0;

static int pre_probe(struct kprobe *p, struct pt_regs *regs)
{
	++call_count;
	printk("pre_probe ...\n");
	return 0;
}

static struct kprobe kp = {
	.pre_handler = pre_probe,
	.post_handler = NULL,
	.fault_handler = NULL,
	.addr = (kprobe_opcode_t *) NULL,
};

static int __init probe_init(void)
{
	kp.addr = (kprobe_opcode_t *) kallsyms_lookup_name("sys_setuid");

	if (kp.addr == NULL) {
		printk("kallsyms_lookup_name could not find address"
			"for the specified symbol name\n");
		return 1;
	}
	register_kprobe(&kp);
	printk("kprobe registered address %p\n", kp.addr);
	return 0;
}

static void __exit probe_exit(void)
{
  unregister_kprobe(&kp);
  printk("probe-breakpoint called %d times.\n", call_count);
}

module_init( probe_init );
module_exit( probe_exit );
MODULE_LICENSE("GPL");
