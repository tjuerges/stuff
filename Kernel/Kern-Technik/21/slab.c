#include <linux/module.h>
#include <linux/completion.h>
#include <linux/slab.h>

static int thread_id=0;
static DECLARE_COMPLETION( on_exit );
static kmem_cache_t *cache;

struct linobj {
	int dummy_i, dummy_j, dummy_k;
	char dummy_feld[250];
};

static void linobj_destructor(void *objp, kmem_cache_t *cache,
	unsigned long flags)
{
	printk("linobj_destructor( %p )\n", objp);
	return;
}

static void linobj_constructor(void *objp, kmem_cache_t *cache,
       unsigned long flags)
{
	if( flags&SLAB_CTOR_VERIFY )
		return;
	printk("linobj_constructor( %p )\n", objp);
	return;
}

static int thread_code( void *data )
{
	unsigned long timeout;
	int i;
	struct linobj *obj;

	daemonize("linobj-test");
	allow_signal( SIGTERM );
	for( i=0; i<5; i++ ) {
		obj = (struct linobj *)kmem_cache_alloc( cache, GFP_KERNEL );
		printk("objadr=%p\n", obj );
		timeout=HZ;
		set_current_state( TASK_INTERRUPTIBLE );
		timeout=schedule_timeout(timeout);
		kmem_cache_free( cache, obj );
		if( timeout )
			break;
	}
	complete_and_exit( &on_exit, 0 );
}

static int __init slab_init(void)
{
	cache = kmem_cache_create( "linobj", sizeof(struct linobj),
		0, 0, linobj_constructor, linobj_destructor );
	if( !cache )
		return -EFAULT;
	thread_id=kernel_thread( thread_code, NULL, CLONE_KERNEL );
	if( thread_id==0 )
		return -EIO;
	return 0;
}

static void __exit slab_exit(void)
{
	kill_proc( thread_id, SIGTERM, 1 );
	wait_for_completion( &on_exit );
	if( cache )
		kmem_cache_destroy( cache );
}

module_init( slab_init );
module_exit( slab_exit );
MODULE_LICENSE("GPL");
