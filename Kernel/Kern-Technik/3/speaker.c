#include <linux/module.h>
#include <linux/version.h>
#include <linux/init.h>
#include <linux/fs.h>      /* struct inode   */
#include <asm/uaccess.h>   /* copy_to_user() */
#include <asm/io.h>

MODULE_LICENSE("GPL");

static ssize_t DriverWrite( struct file *File, __user const char *User,
	size_t count, loff_t *Offs)
{
	u16 tonwert;
	s8 Save;
	char buffer[6];

	if( count > sizeof(buffer) ) {
		count = sizeof(buffer);
	}
	copy_from_user( buffer, User, count );
	buffer[sizeof(buffer)-1] = '\0';
	tonwert = (u16) simple_strtoul( buffer, NULL, 0 );
	printk("tonwert=%d\n", tonwert );
	if( tonwert ) {
		tonwert = CLOCK_TICK_RATE/tonwert;
		printk("tonwert=%x\n", tonwert );
		outb( 0xb6, 0x43 );
		outb_p(tonwert & 0xff, 0x42);
		outb((tonwert>>8) & 0xff, 0x42);
		Save = inb( 0x61 );
		outb( Save | 0x03, 0x61 );
	} else {
		outb(inb_p(0x61) & 0xFC, 0x61);
	}
	return count;
}

static struct file_operations Fops = {
	.owner=THIS_MODULE,
	.write=DriverWrite,    /* write   */
};

static int __init BufInit(void)
{
	if(register_chrdev(240, "PC-Speaker", &Fops) == 0) {
		return 0;
	};
	return -EIO;
}

static void __exit BufExit(void)
{
	outb(inb_p(0x61) & 0xFC, 0x61);
	unregister_chrdev(240,"PC-Speaker");
}

module_init( BufInit );
module_exit( BufExit );
