#include <linux/fs.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/pci.h>
#include <linux/interrupt.h>

MODULE_LICENSE("GPL");

// TODO: Hier m�ssen die eigenen ID's eingetragen werden.
#define MY_VENDOR_ID PCI_ANY_ID
#define MY_DEVICE_ID PCI_ANY_ID

static unsigned long ioport=0L, iolen=0L, memstart=0L, memlen=0L;

static irqreturn_t pci_isr( int irq, void *dev_id, struct pt_regs *regs )
{
	return IRQ_HANDLED;
}

static int DeviceInit(struct pci_dev *dev, const struct pci_device_id *id)
{
	printk("0x%4.4x|0x%4.4x: \"%s\"\n", dev->vendor, dev->device,
				dev->dev.name );
	return -EIO;
}

static void DeviceDeInit( struct pci_dev *pdev )
{
	return;
}

static struct file_operations PCIFops;

static struct pci_device_id pci_drv_tbl[] __devinitdata = {
	{ MY_VENDOR_ID, MY_DEVICE_ID, PCI_ANY_ID, PCI_ANY_ID, 0, 0, 0 },
	{ 0, }
};

static struct pci_driver pci_drv = {
	.name= "pci_drv",
	.id_table= pci_drv_tbl,
	.probe= DeviceInit,
	.remove= DeviceDeInit,
};

static int __init pci_drv_init(void)
{
	if(register_chrdev(240, "PCI-Driver", &PCIFops)==0) {
		if( pci_module_init(&pci_drv) == 0 )
			return 0;
		unregister_chrdev(240,"PCI-Driver");
	}
	return -EIO;
}

static void __exit pci_drv_exit(void)
{
	pci_unregister_driver( &pci_drv );
	unregister_chrdev(240,"PCI-Driver");
}

module_init(pci_drv_init);
module_exit(pci_drv_exit);
