#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>

MODULE_LICENSE("GPL");

static int __init ModInit(void)
{
	return 0;
}

static void __exit ModExit(void)
{
	return;
}

module_init( ModInit );
module_exit( ModExit );
