#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>

MODULE_LICENSE("GPL");

static int DriverOpen( struct inode *GeraeteDatei, struct file *Instanz )
{
	return 0;
}

static struct file_operations Fops = {
	.open=    DriverOpen,
};

static int __init ModInit(void)
{
	if( register_chrdev(240,"Treiber-Test",&Fops)==0) {
		return 0; // Treiber ist angemeldet
	};
	return( -EIO ); // Fehler: Major-Nummer ist bereits belegt
}

static void __exit ModExit(void)
{
	unregister_chrdev(240, "Treiber-Test");
	return;
}

module_init( ModInit );
module_exit( ModExit );

