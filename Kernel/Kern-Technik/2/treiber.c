#include <linux/module.h>
#include <linux/version.h>
#include <linux/init.h>
#include <linux/fs.h>      /* struct inode   */
#include <linux/wait.h>    /* wait_event_interruptible   */
#include <asm/uaccess.h>   /* copy_to_user() */

MODULE_LICENSE("GPL");

static wait_queue_head_t ReadWq;
static int FillIndex=0;
static char MemBuf[256];

static ssize_t DriverRead( struct file *File, char *User, size_t count,
	loff_t *Offset )
{
	if( FillIndex == 0 ) { // Keine Daten vorhanden
		if( File->f_flags & O_NONBLOCK ) // non blocking mode - return
			return -EAGAIN;
		if( wait_event_interruptible(ReadWq,(FillIndex>0)) ) // Schleife
			return -ERESTARTSYS;
	}
	if( count>FillIndex )
		count=FillIndex;
	if( copy_to_user(User,MemBuf,count) ) // Daten konnten nich komplett
		return -EFAULT;                   // kopiert werden.
	FillIndex -= count;
	return count;
}

static ssize_t DriverWrite( struct file *File, const char *User, size_t count,
	loff_t *Offs)
{
	if( count > sizeof(MemBuf) )
		count = sizeof(MemBuf);
	if( copy_from_user( MemBuf, User, count ) )
		return -EFAULT;
	FillIndex=count;
	wake_up_interruptible( &ReadWq );
	return count;
}

static struct file_operations Fops = {
	.owner=THIS_MODULE,
	.read=DriverRead,      /* read    */
	.write=DriverWrite,    /* write   */
};

static int __init BufInit(void)
{
	if(register_chrdev(240, "MemoryTreiber", &Fops) == 0) {
		init_waitqueue_head( &ReadWq );
		return 0;
	};
	return -EIO;
}

static void __exit BufExit(void)
{
	unregister_chrdev(240,"MemoryTreiber");
}

module_init( BufInit );
module_exit( BufExit );
