#include <linux/module.h>
#include <linux/init.h>
#include <linux/in.h>
#include <linux/inet.h>
#include <net/sock.h>

MODULE_LICENSE("GPL");

static const unsigned short serverport = 5555;
static struct socket *clientsocket=NULL;

static int __init server_init( void )
{
	int len, clienterror;
	char buf[64];
	struct msghdr msg;
	struct iovec iov;
	mm_segment_t oldfs;
	struct sockaddr_in client;

	if( sock_create( PF_INET,SOCK_STREAM,IPPROTO_TCP,&clientsocket)<0 ) {
		printk( KERN_ERR "server: Error creating serversocket.\n" );
		return -EIO;
	}
	client.sin_family = AF_INET;
	client.sin_addr.s_addr = in_aton( "127.0.0.1" ); /* destination address */
	client.sin_port = htons( (unsigned short)serverport );
	clienterror = clientsocket->ops->connect( clientsocket,
			(struct sockaddr *) &client, sizeof( client ), 0 );
	if( clienterror < 0 ) {
		printk( KERN_ERR "Carrera: Connect error = %d on clientsocket",
				clienterror );
		return -EIO;
	}
	memcpy( buf, "hallo", 6 );
	iov.iov_base = buf;
	iov.iov_len  = 6;
	msg.msg_control = NULL;
	msg.msg_controllen = 0;
	msg.msg_iov	 = &iov;
	msg.msg_iovlen = 1;
	msg.msg_flags = 0;

	oldfs = get_fs(); 
	set_fs( KERNEL_DS );
	len = sock_sendmsg( clientsocket, &msg, 6 );
	if( len > 0 ) {
		iov.iov_len  = sizeof(buf);
		len = sock_recvmsg( clientsocket, &msg, sizeof(buf), 0 );
		if( len > 0 )
			printk( KERN_INFO "returned: \"%s\"\n", buf);
		else
			printk( KERN_ERR "recvmsg: %d\n", len );
	}
	set_fs( oldfs );
	
	//return 0;
	if( clientsocket ) {
		sock_release( clientsocket );
		clientsocket = NULL;
	}
	return -EIO;
}

static void __exit server_exit( void )
{
	if( clientsocket )
		sock_release( clientsocket );
}

module_init( server_init );
module_exit( server_exit );
