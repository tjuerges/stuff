#include <linux/module.h>
#include <linux/init.h>
#include <linux/in.h>
#include <linux/inet.h>
#include <net/sock.h>

static const unsigned short serverport = 5555;
static struct socket *clientsocket=NULL;

static int __init server_init( void )
{
	int len;
	char buf[64];
	struct msghdr msg;
	struct iovec iov;
	mm_segment_t oldfs;
	struct sockaddr_in to;

	if( sock_create( PF_INET,SOCK_DGRAM,IPPROTO_UDP,&clientsocket)<0 ) {
		printk( KERN_ERR "server: Error creating serversocket.\n" );
		return -EIO;
	}
	to.sin_family = AF_INET;
	to.sin_addr.s_addr = in_aton( "127.0.0.1" ); /* destination address */
	to.sin_port = htons( (unsigned short)serverport );

	memcpy( buf, "hallo", 6 );
	msg.msg_control = NULL;
	msg.msg_controllen = 0;
	msg.msg_iov	 = &iov;
	msg.msg_iovlen = 1;
	msg.msg_name = &to;
	msg.msg_namelen = sizeof(to);
	iov.iov_base = buf;
	iov.iov_len  = 6;

	oldfs = get_fs(); 
	set_fs( KERNEL_DS );
	len = sock_sendmsg( clientsocket, &msg, 6 );
	if( len < 0 )
		printk( KERN_ERR "sock_sendmsg returned: %d\n", len);
	set_fs( oldfs );
	return 0;
}

static void __exit server_exit( void )
{
	if( clientsocket )
		sock_release( clientsocket );
}

module_init( server_init );
module_exit( server_exit );
MODULE_LICENSE("GPL");
/* vim: set tw=80 aw sw=4 ts=4 ic: */
