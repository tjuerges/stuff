#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int main( int argc, char **argv )
{
	struct sockaddr_in to;
	char *buf;
	int sd, ret;
	
	sd = socket( AF_INET, SOCK_DGRAM, 0 );
	if( sd <= 0 ) {
		perror( "socket" );
		exit( -1 );
	}
	bzero( &to, sizeof(to) );
	to.sin_family = AF_INET;
	inet_aton( "192.168.69.25", &to.sin_addr );
	to.sin_port = htons( (unsigned short)5555 );
	buf = "hallo";
	ret = sendto( sd, buf, strlen(buf)+1, 0, (struct sockaddr *)&to,
		sizeof(to) );

	if( ret>0 ) {
		printf("ok\n" );
	} else {
		printf("sendto returned %d\n", ret );
	}

	close( sd );
	return 0;
}
