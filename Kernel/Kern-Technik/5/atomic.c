#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>

MODULE_LICENSE("GPL");

static atomic_t Status = ATOMIC_INIT( 2 );

static int __init ModInit(void)
{
	int i, s;

	atomic_set( &Status, 4 );
	s = atomic_read( &Status );
	atomic_add( 2, &Status );
	atomic_sub( 2, &Status );
	atomic_inc( &Status );
	atomic_dec( &Status );
	for( i=0; i<6; i++ ) {
		s=atomic_sub_and_test( 1, &Status );
		printk("sub_and_test: %d, read: %d\n", s, atomic_read(&Status) );
	}
#if 0
atomic_sub_and_test( int i, atomic_t *v )
atomic_add_negative( int i, atomic_t *v )
atomic_inc_and_test( atomic_t *v )
atomic_dec_and_test( atomic_t *v )
#endif

	return 0;
}

static void __exit ModExit(void)
{
	return;
}

module_init( ModInit );
module_exit( ModExit );
