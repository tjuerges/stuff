/* vim: set ts=4: */
#include <linux/module.h>
#include <linux/version.h>
#include <linux/init.h>
#include <linux/completion.h>
#include <linux/spinlock.h>
//#include <linux/delay.h>

MODULE_LICENSE("GPL");

static int ThreadID1=0, ThreadID2=0;
DECLARE_COMPLETION( cmpltn );
static spinlock_t sl = SPIN_LOCK_UNLOCKED;
static wait_queue_head_t wq;

static int ThreadCode( void *data )
{
	int i;
	unsigned long iflags;
	unsigned long timeout;

	daemonize( (char *)data );
	allow_signal( SIGTERM );
	for( i=0; i<15; i++ ) {
		spin_lock_irqsave( &sl, iflags );
		// Kritischer Abschnitt ...
		spin_unlock_irqrestore( &sl, iflags );
		printk("Thread %d: nach dem kritischen Abschnitt ...\n",
						current->pid);
		timeout = HZ;
		wait_event_interruptible_timeout( wq, (timeout==0), timeout);
		if( signal_pending( current ) ) {
			printk("Signal ...\n");
			complete( &cmpltn );
			return -EIO;
		}
	}
	printk("Normales Ende ...\n");
	complete( &cmpltn );
	return 0;
}

static int __init SpinlockInit(void)
{
	init_waitqueue_head( &wq );
	ThreadID1=kernel_thread(ThreadCode, "SLTest1", CLONE_KERNEL);
	if( !ThreadID1 ) goto out;
	ThreadID2=kernel_thread(ThreadCode, "SLTest2", CLONE_KERNEL);
	if( !ThreadID2 ) goto cleanup;
	return 0;
cleanup:
	kill_proc( ThreadID1, SIGTERM, 1 );
	wait_for_completion( &cmpltn );
out:
	return -EIO;
}

static void __exit SpinlockExit(void)
{
	kill_proc( ThreadID1, SIGTERM, 1 );
	kill_proc( ThreadID2, SIGTERM, 1 );
	wait_for_completion( &cmpltn );
	wait_for_completion( &cmpltn );
}

module_init( SpinlockInit );
module_exit( SpinlockExit );
