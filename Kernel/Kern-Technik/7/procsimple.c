/* vim: set ts=4 sw=4 aw ic: */
#include <linux/module.h>
#include <linux/version.h>
#include <linux/proc_fs.h>
#include <linux/init.h>
#include <linux/stat.h>

MODULE_LICENSE("GPL");

static struct proc_dir_entry *ProcDir, *ProcFile;

static int ProcRead( char *buf, char **start, off_t offset,
			    int size, int *eof, void *data)
{
	int BytesWritten=0;
	printk("ProcRead: %d\n", size );
	BytesWritten+=snprintf( buf, size, "ProcRead wurde\n");
	BytesWritten+=snprintf( buf+BytesWritten,
			size-BytesWritten, "aufgerufen.\n");
	*eof = 1;
	return BytesWritten;
}

static int __init ProcInit(void)
{
	ProcDir = proc_mkdir( "ExampleDir", NULL );
	ProcFile=create_proc_entry( "ProcExample", S_IRUGO, ProcDir );
	if( ProcFile ) {
		ProcFile->read_proc = ProcRead;
		ProcFile->data = NULL;
	}
	return 0;
}

static void __exit ProcExit(void)
{
	if( ProcFile ) remove_proc_entry( "ProcExample", ProcDir );
	if( ProcDir )  remove_proc_entry( "ExampleDir", NULL );
}

module_init( ProcInit );
module_exit( ProcExit );
