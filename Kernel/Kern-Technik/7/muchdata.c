/* vim: set ts=4 sw=4 aw ic: */
#include <linux/module.h>
#include <linux/version.h>
#include <linux/proc_fs.h>
#include <linux/init.h>
#include <linux/stat.h>

MODULE_LICENSE("GPL");

static struct proc_dir_entry *ProcDir, *ProcFile;

static int ProcRead( char *buf, char **start, off_t offset,
			    int size, int *eof, void *data)
{
	int ActualWritten=0, BytesWritten=0, NewByteCount;

	while( BytesWritten<(long)offset ) {
		ActualWritten=snprintf( buf, size, "Hallo Welt\n" );
		BytesWritten+=ActualWritten;
	}
	// In buf befinden sich jetzt alte _und_ neue Daten.
	// Start wird auf die neuen Daten gesetzt.
	NewByteCount=BytesWritten - offset;
	*start = buf + ActualWritten - NewByteCount;
	size -= ActualWritten - NewByteCount; // Auf neue Startadresse anpassen.

	NewByteCount+=snprintf( *start+NewByteCount,
			size-NewByteCount, "Hallo Welt\n");
	if( offset>4000 ) {
		*eof = 1;
		return 0;
	}
	return NewByteCount;
}

static int __init ProcInit(void)
{
	ProcDir = proc_mkdir( "ExampleDir", NULL );
	ProcFile=create_proc_entry( "ProcExample", S_IRUGO, ProcDir );
	if( ProcFile ) {
		ProcFile->read_proc = ProcRead;
		ProcFile->data = NULL;
	}
	return 0;
}

static void __exit ProcExit(void)
{
	if( ProcFile ) remove_proc_entry( "ProcExample", ProcDir );
	if( ProcDir )  remove_proc_entry( "ExampleDir", NULL );
}

module_init( ProcInit );
module_exit( ProcExit );
