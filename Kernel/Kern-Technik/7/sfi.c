/* vim: set ts=4 sw=4 aw ic: */
#include <linux/module.h>
#include <linux/version.h>
#include <linux/proc_fs.h>
#include <linux/init.h>
#include <linux/seq_file.h>

MODULE_LICENSE("GPL");

static void *IteratorStart(struct seq_file *m, loff_t *pos)
{
	int i;
	struct task_struct *tptr = current;

	printk("IteratorStart( %d )\n", (int)*pos );
	for( i=(int)*pos; i && tptr!=tptr->parent; i-- )
		tptr = tptr->parent;
	if( tptr==tptr->parent )
		return NULL;
	return (void *)tptr;
}

static void IteratorStop(struct seq_file *m, void *v)
{
	printk("IteratorStop(%p)\n", v);
	return;
}

static void *IteratorNext(struct seq_file *m, void *v, loff_t *pos)
{
	struct task_struct *tptr = (struct task_struct *)v;

	printk("IteratorNext(%p,parent=%p,%d)\n", v, tptr->parent,(int)*pos);
	if( tptr == tptr->parent ) {
		return NULL;
	}
	(*pos)++;
	return (void *)tptr->parent;
}

static int DataShow(struct seq_file *m, void *v)
{
	struct task_struct *tptr = (struct task_struct *)v;

	printk("DataShow(%p)\n", v);
	seq_printf(m, "Prozess PID: %d\n", tptr->pid );
	seq_printf(m, "     Eltern: %p\n", tptr->parent );
	return 0;
}

static struct seq_operations sops = {
	.start = IteratorStart,
	.next  = IteratorNext,
	.stop  = IteratorStop,
	.show  = DataShow,
};

static int call_seq_open( struct inode *GeraeteDatei, struct file *Instanz )
{
	return seq_open( Instanz, &sops );
}

static struct file_operations fops = {
	.owner   = THIS_MODULE,
	.open    = call_seq_open,
	.read    = seq_read,
	.llseek  = seq_lseek,
	.release = seq_release,
};

static int __init ProcInit(void)
{
	static struct proc_dir_entry *procdirentry;

	printk("\n\nProcInit()\n");
	procdirentry=create_proc_entry( "SequenceFileTest", 0, NULL );
	if( procdirentry ) {
		procdirentry->proc_fops = &fops;
	}
	return 0;
}

static void __exit ProcExit(void)
{
	remove_proc_entry( "SequenceFileTest", NULL );
}

module_init( ProcInit );
module_exit( ProcExit );
