#include <linux/module.h>
#include <linux/init.h>
#include <linux/moduleparam.h>

MODULE_LICENSE("GPL");

#define param_check_even( name, p )

int param_set_even( const char *pbuf, struct kernel_param *kp )
{
	int number;

	number = simple_strtoul( pbuf, NULL, 0 );
	if( number%2 ) // ungerade
		return -EINVAL;
	*(int *)kp->arg = number;
	return 0;
}

int param_get_even( char *pbuf, struct kernel_param *kp )
{
	return sprintf((char *)pbuf, "%d", *(int *)kp->arg );
}

static int para;
module_param( para, even, 0666 );

static int __init mod_init(void)
{
	printk("para = %d\n", para );
	return 0;
}

static void __exit mod_exit(void)
{
}
module_init( mod_init );
module_exit( mod_exit );
