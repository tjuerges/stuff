#include <linux/module.h>
#include <linux/moduleparam.h>

MODULE_LICENSE("GPL");

static char string[10];
module_param_string( optionname, string, sizeof(string), 666 );

static int __init mod_init(void)
{
	if( string[0] )
		printk("string: %s\n", string);
	return 0;
}

static void __exit mod_exit(void)
{
}
module_init( mod_init );
module_exit( mod_exit );
