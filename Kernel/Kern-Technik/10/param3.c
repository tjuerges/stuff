#include <linux/module.h>
#include <linux/moduleparam.h>

MODULE_LICENSE("GPL");

static int intarray[4];
static int intarraycount=4;
module_param_array( intarray, int, intarraycount, 666 );
MODULE_PARM_DESC(intarray, "Ein Feld mit bis zu 4 Int-Werten.");

static int __init mod_init(void)
{
	printk("intarraycount=%d\n", intarraycount);
	for( ; intarraycount; intarraycount-- ) {
		printk("%d: %d\n", intarraycount, intarray[intarraycount-1] );
	}
	return 0;
}

static void __exit mod_exit(void)
{
	return;
}

module_init( mod_init );
module_exit( mod_exit );
