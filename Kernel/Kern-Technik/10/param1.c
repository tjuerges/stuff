#include <linux/module.h>
#include <linux/moduleparam.h>

MODULE_LICENSE("GPL");
MODULE_PARM_DESC(myint, "Testparameter");

static int myint;
module_param( myint, int, 666 );

static int __init mod_init(void)
{
	printk("myint = %d\n", myint );
	return 0;
}

static void __exit mod_exit(void)
{
}

module_init( mod_init );
module_exit( mod_exit );
