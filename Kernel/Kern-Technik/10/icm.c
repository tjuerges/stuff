#include <linux/module.h>
#include <linux/init.h>
#include <asm/io.h>

MODULE_LICENSE("GPL");

static char *textbuf = "Hallo Treiber";

void ton_an( u16 tonwert )
{
	s8 save;

	if( tonwert ) {
		tonwert = CLOCK_TICK_RATE/tonwert;
		printk("ton_an( 0x%x )\n", tonwert );
		outb( 0xb6, 0x43 );
		outb_p(tonwert & 0xff, 0x42);
		outb((tonwert>>8) & 0xff, 0x42);
		save = inb( 0x61 );
		outb( save | 0x03, 0x61 );
	} else {
		outb(inb_p(0x61) & 0xFC, 0x61);
	}
}

static int __init buf_init(void)
{
	printk(textbuf);
	return 0;
}

static void __exit buf_exit(void)
{
	outb(inb_p(0x61) & 0xFC, 0x61);
}

module_init( buf_init );
module_exit( buf_exit );
EXPORT_SYMBOL_GPL( ton_an );
EXPORT_SYMBOL_GPL( textbuf );
