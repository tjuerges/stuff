#include <linux/module.h>
#include <linux/init.h>

MODULE_LICENSE("GPL");

extern void ton_an( u16 tonwert );
extern char *textbuf;

static void (*ton_an_function)(u16 var );
static char **textbufptr;

static int __init buf_init(void)
{
	ton_an_function = symbol_get( ton_an );
	if( ton_an_function )
		ton_an_function( 330 );
	else
		printk("can't find address of symbol \"ton_an\"\n");
	textbufptr = symbol_get( textbuf );
	if( textbufptr )
		printk("content of 0x%p: \"%s\"\n", textbufptr, *textbufptr );
	else
		printk("can't find address of symbol \"textbufptr\"\n");
	return 0;
}

static void __exit buf_exit(void)
{
	if( ton_an_function ) {
		ton_an_function( 0 );
		symbol_put( ton_an );
		//symbol_put_addr(ton_an_function); // alternativ zu symbol_put
	}
	if( textbufptr )
		symbol_put( textbuf );
}

module_init( buf_init );
module_exit( buf_exit );
