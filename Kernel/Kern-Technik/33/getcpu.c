#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#define __USE_GNU
#include <sched.h>
#include <errno.h>

static unsigned int getcpu()
{
    unsigned int i, cpuid;
	FILE *fp;

	fp=fopen( "/proc/self/stat", "r" );
	if( fp==NULL ) {
		perror( "/proc/self/stat" );
		exit( -1 );
	}
	fscanf( fp, "%*d %*s %*s" );
	for( i=3; i<39; i++ ) // Das 39. Feld enthaelt die CPU-Id.
		fscanf( fp, "%d", &cpuid );
	return cpuid;
}

static unsigned int getaffinity( cpu_set_t *mask )
{
	CPU_ZERO( mask );
	if( sched_getaffinity( getpid(), sizeof(*mask), mask) ) {
		perror("sched_setaffinity");
		exit( -2 );
	}
	return mask->__bits[0]; // XXX - max. 32 Prozessoren
}

int main( int argc, char **argv )
{
	cpu_set_t mask;
	int cpuid;

	cpuid = getcpu();
    printf("Aktive CPU: %d\n", cpuid );
	printf("Affinity-Maske: 0x%x\n", getaffinity( &mask ) );
	CPU_CLR( cpuid, &mask ); // Aktuelle CPU verbieten.
	printf("Setzen der Affinität auf: 0x%lx\n", mask.__bits[0] );
	if( sched_setaffinity( getpid(), sizeof(mask), &mask ) ) {
		perror("sched_setaffinity"); // UP-System???
	}
	printf("Affinity-Maske: 0x%x\n", getaffinity( &mask ) );
    printf("Aktive CPU %d\n", getcpu() );

    return 0;
}
// vim: aw ic ts=4:
