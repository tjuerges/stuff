/* vim: set ts=4 ic aw sw=4: */
#include <linux/fs.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/blkdev.h>

#define BD_MAJOR 242
#define SIZE_IN_MBYTES 64

MODULE_LICENSE("GPL");

static struct gendisk *disk;
static struct request_queue *bdqueue;
static char *mempool;
static spinlock_t bdlock = SPIN_LOCK_UNLOCKED;

static void bdRequest( request_queue_t *q )
{
	struct request *req;
	unsigned long start, ToCopy;

	while((req=elv_next_request(q))!=NULL ) {
		if( !blk_fs_request(req) ) {
			end_request( req, 0 );
			continue;
		}
		ToCopy  = req->current_nr_sectors<<9;
		start = req->sector<<9;
		spin_unlock_irq(q->queue_lock);
		if( (start+ToCopy)<=(SIZE_IN_MBYTES*1024*1024) ) {
			if (rq_data_dir(req) == READ) {
				memcpy(req->buffer, mempool+start, ToCopy);
			} else {
				memcpy(mempool+start, req->buffer, ToCopy);
			}
		} else {
			printk("%ld not in range...\n", start+ToCopy);
		}
		spin_lock_irq(q->queue_lock);
		end_request(req,1);
	}
}

static struct block_device_operations bdops = {
    .owner           = THIS_MODULE,
};

static int __init ModInit(void)
{
	if( register_blkdev(BD_MAJOR, "bdsample" )) {
		printk("blockdevice: Majornummer %d not free.", BD_MAJOR);
		return -EIO;
	}
	if( !(mempool=vmalloc(SIZE_IN_MBYTES*1024*1024)) ) {
		printk("vmalloc failed ...\n");
		goto out_no_mem;
	}

	if( !(disk=alloc_disk(1)) ) {
		printk("alloc_disk failed ...\n");
		goto out;
	}
	disk->major = BD_MAJOR;
	disk->first_minor = 0;
    disk->fops = &bdops;
	sprintf(disk->disk_name, "bd0");
	set_capacity( disk, (SIZE_IN_MBYTES*1024*1024)>>9 ); // in 512 Byte Bloecke
	if( (bdqueue=blk_init_queue(&bdRequest,&bdlock))==NULL )
		goto out;
	blk_queue_hardsect_size( bdqueue, 512 );
	disk->queue = bdqueue;
	add_disk( disk );
	return 0;
out:
	vfree( mempool );
out_no_mem:
	unregister_blkdev(BD_MAJOR, "bdsample" );
	return -EIO;
}

static void __exit ModExit(void)
{
	del_gendisk(disk);
	put_disk( disk );
	blk_cleanup_queue( bdqueue );
	vfree( mempool );
	unregister_blkdev(BD_MAJOR, "bdsample" );
}

module_init( ModInit );
module_exit( ModExit );
