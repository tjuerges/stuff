#include <linux/module.h>
#include <linux/init.h>
#include <linux/debugfs.h>

static struct dentry *vardir, *varfile;
static u32 index=99;

static int __init mod_init(void)
{
	vardir = debugfs_create_dir("vardir", NULL);
	if( vardir==NULL ) {
		printk("can't create debugfs-dir\n" );
		return -EIO;
	}
	varfile = debugfs_create_u32("index", S_IRUGO|S_IWUGO, vardir, &index);
	if( varfile==NULL ) {
		printk("can't create debugfs-file\n");
		debugfs_remove( vardir );
		return -EIO;
	}
	printk("Laden: index hat den Wert: %d\n", index );
	return 0;
}

static void __exit mod_exit(void)
{
	printk("Entladen: index hat den Wert: %d\n", index );
	debugfs_remove( varfile );
	debugfs_remove( vardir );
}

module_init( mod_init );
module_exit( mod_exit );

MODULE_LICENSE("GPL");
