#include <stdio.h>
#include <fcntl.h>
#include <sys/mman.h>

int main( int argc, char **argv )
{
	int fd;
	char *buf=NULL;

	fd=open("/debugfs/relay/logfile0", O_RDONLY );
	if( fd < 0 ) {
		return -1;
	}
	buf = (char *) mmap( (void *)buf, 30*4, PROT_READ, MAP_SHARED, fd, 0 );
	if( buf == (void*)(-1) ) {
		return -2;
	}
	// Ab hier erfolgen die direkten Zugriffe auf den Speicher
	...
	return 0;
}
