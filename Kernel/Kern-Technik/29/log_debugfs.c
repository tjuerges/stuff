/* vim: set ts=4 ai ic aw sw=4: */
#include <linux/module.h>
#include <linux/init.h>
#include <linux/relay.h>
#include <linux/debugfs.h>

static char *TEXT="hello world";
static struct rchan *channel_id;
static struct dentry *logdir;

static int lremove_buf_file(struct dentry *dentry)
{
	debugfs_remove(dentry);
	return 0;
}

static struct dentry *lcreate_buf_file(const char *filename,
	struct dentry *parent, int mode, struct rchan_buf *buf, int *is_global)
{
	return debugfs_create_file(filename, mode, parent, buf,
					&relay_file_operations);
}

static int lsubbuf_start(struct rchan_buf *buf, void *subbuf, void *prev_subbuf,
	unsigned int prev_padding)
{
	printk("subbuf_start padding=%d\n", prev_padding);
	return 1; // bufferchange can occur
}

struct rchan_callbacks cb = {
	subbuf_start:  lsubbuf_start,
	create_buf_file: lcreate_buf_file,
	remove_buf_file: lremove_buf_file,
};

static int __init mod_init(void)
{
	int i;

	logdir = debugfs_create_dir("logdir", NULL);
	if( logdir==NULL ) {
		printk("logdir= %p\n", logdir );
		return -EIO;
	}

	channel_id = relay_open( "logfile", logdir, 30, 4, &cb );
	for( i=0; i<20; i++ ) {
		relay_write( channel_id, TEXT, strlen(TEXT)+1 );
		schedule_timeout_interruptible(HZ);
	}
	return 0;
}

static void __exit mod_exit(void)
{
	relay_close( channel_id );
	debugfs_remove( logdir );
}

module_init( mod_init );
module_exit( mod_exit );

MODULE_LICENSE("GPL");
