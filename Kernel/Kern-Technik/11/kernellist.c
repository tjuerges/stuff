#include <linux/module.h>
#include <linux/init.h>

MODULE_LICENSE("GPL");

static spinlock_t sl;

struct my_list {
	int counter;
	struct list_head link;
};
struct list_head rootptr;

static int show_listfunction_usage( void )
{
	int i;
	struct my_list *newelement;
	struct list_head *loopvar, *tmp;

	for( i=0; i<5; i++ ) { // Liste anlegen
		// kmalloc darf nicht innerhalb der critical section verwendet werden!
		newelement = kmalloc( sizeof(struct my_list), GFP_USER );
		spin_lock( &sl );
		newelement->counter = i;
		printk( "adding: %p- %d\n", newelement, i );
		list_add( &newelement->link, &rootptr );
		spin_unlock( &sl );
	}
	spin_lock( &sl );
	list_for_each_safe( loopvar, tmp, &rootptr ) { // Liste freigeben
		i = list_entry( loopvar, struct my_list, link )->counter;
		printk( "delete: %p - %d\n", loopvar, i );
		list_del( loopvar );
		kfree( list_entry( loopvar,struct my_list, link ) );
	}
	spin_unlock( &sl );
	return 0;
}

static int __init modinit(void)
{
	INIT_LIST_HEAD( &rootptr );
	show_listfunction_usage();
	return -EIO;
}

static void __exit modexit(void)
{
	return;
}

module_init( modinit );
module_exit( modexit );
