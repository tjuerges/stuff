#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

int main( int argc, char **argv )
{
	int fd;
	void *pageaddr;
	int *ptr;

	fd = open( "/dev/mmap", O_RDWR );
	if( fd<0 ) {
		perror( "/dev/mmap" );
		return -1;
	}
	pageaddr = mmap( NULL, 4096, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0 );

	printf("pageaddr mmap: %p\n", pageaddr );
	if( pageaddr ) {
		ptr = (int *)pageaddr;
		printf("*pageaddr: %d\n", *ptr );
		*ptr = 55;
	}
	munmap( pageaddr, 4096 );

	return 0;
}
