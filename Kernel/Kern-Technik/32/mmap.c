#include <linux/module.h>
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/mm.h>

static int major;
static struct class *demo_class;
static struct page *common_page;

struct page *driver_vma_nopage( struct vm_area_struct *vma,
	unsigned long address, int *type )
{
	printk("driver_vma_nopage( %p, %lx, %p )\n", vma, address, type );
	get_page( common_page );
	if( type )
		*type = VM_FAULT_MINOR;
	return common_page;
	//return NOPAGE_SIGBUS;
}

static struct vm_operations_struct driver_vma_ops = {
	.nopage = driver_vma_nopage,
};

static int driver_mmap( struct file *instance, struct vm_area_struct *vma )
{
	printk("driver_mmap(%p, %p)\n", instance, vma);
	vma->vm_ops = &driver_vma_ops;
	vma->vm_flags |= VM_RESERVED;
	return 0;
}

static struct file_operations fops = {
	.mmap = driver_mmap,
};

static int __init driver_init(void)
{
	int *ptr;

	printk("driver_init called\n");
	if( (major=register_chrdev(0,"treiber",&fops))==0 ) {
		printk("no major number available\n");
		return -EIO;
	}
	printk("major: %d\n", major );
	demo_class = class_create(THIS_MODULE, "demo");
	if( IS_ERR(demo_class) ) {
		printk("no udev support\n");
	} else {
		printk("class_device_create...\n");
		class_device_create(demo_class,NULL,MKDEV(major,0),NULL,"mmap");
	}
	common_page = alloc_page( GFP_USER );
	if( !common_page ) {
		if( !IS_ERR(demo_class) ) {
			class_device_destroy( demo_class, MKDEV(major,0) );
			class_destroy( demo_class );
		}
		unregister_chrdev( major, "treiber" );
		return -ENOMEM;
	}
	ptr = (int *)page_address( common_page );
	*ptr = 99;
	return 0;
}

static void __exit driver_exit(void)
{
	unregister_chrdev( major, "treiber" );
	if( !IS_ERR(demo_class) ) {
		printk("removing demo_class...\n");
		class_device_destroy( demo_class, MKDEV(major,0) );
		class_destroy( demo_class );
	}
	printk("driver_exit called\n");
}

module_init( driver_init );
module_exit( driver_exit );
MODULE_LICENSE("GPL");
// vim: se aw tw=80 ic ts=4 sw=4:
