#include <linux/syscalls.h>
#include <linux/jiffies.h>
#include <asm/delay.h>

asmlinkage long sys_udelay( int museconds )
{
	pr_debug("sys_udelay( %d )\n", museconds );
	if( (museconds < 0) || (museconds > 10000) )
		return -EINVAL;
	udelay( museconds );
	return 0;
}
