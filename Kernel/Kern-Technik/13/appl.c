#include <unistd.h>

int main( int argc, char **argv )
{
	int ret;

	ret = syscall( __NR_udelay, 9000 );
	printf("udelay(9000)=%d\n", ret);
	ret = syscall( __NR_udelay, 10001 );
	perror("udelay");
	printf("udelay(10001)=%d\n", ret);
	return 0;
}
