#include <asm/unistd.h>

extern int errno;

_syscall3( int, write, int, fd, char *, buffer, int, size );

int main( int argc, char **argv )
{
	write( 1, "hello world\n", 13 );
	return 0;
}
