#include <unistd.h>
#include <asm/unistd.h>

int main( int argc, char **argv )
{
	syscall( __NR_write, 1, "Hello World\n", 13 );
	return 0;
}
