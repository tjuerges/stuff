#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/interrupt.h>

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Tasklet-Beispieltreiber");

static void TaskletFunction( unsigned long data )
{
	printk("Tasklet called...\n");
	return;
}

DECLARE_TASKLET( TlDescr, TaskletFunction, 0L );

static int __init ModInit(void)
{
	printk("ModInit called\n");
	tasklet_schedule( &TlDescr ); // Tasklet sobald als m�glich ausf�hren
	return 0;
}

static void __exit ModExit(void)
{
	printk("ModExit called\n");
	tasklet_kill( &TlDescr );
}

module_init( ModInit );
module_exit( ModExit );
