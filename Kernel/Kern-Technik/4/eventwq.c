#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/workqueue.h>

MODULE_LICENSE("GPL");

static void WorkQueueFunction( void *data )
{
	printk("end of WorkQueueFunction\n");
}

static DECLARE_WORK( work, WorkQueueFunction, NULL );

static int __init ModInit(void)
{
	if( schedule_work( &work )==0 )
		printk( "schedule_work not successful ...\n");
	else
		printk( "schedule_work successful ...\n");
	return 0;
}

static void __exit ModExit(void)
{
	flush_scheduled_work();
}

module_init( ModInit );
module_exit( ModExit );
