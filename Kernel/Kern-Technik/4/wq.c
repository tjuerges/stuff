#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/workqueue.h>

MODULE_LICENSE("GPL");

static struct workqueue_struct *wq;

static void WorkQueueFunction( void *data )
{
	printk("WorkQueueFunction\n");
}

static DECLARE_WORK( work, WorkQueueFunction, NULL );

static int __init ModInit(void)
{
	wq = create_workqueue( "DrvrSmpl" );
	if( queue_work( wq, &work ) )
		printk( "work has been queued ...\n");
	else
		printk( "queue_work failed ...\n");
	return 0;
}

static void __exit ModExit(void)
{
	printk("ModExit called\n");
	if( wq ) {
		destroy_workqueue( wq );
		printk("workqueue destroyed\n");
	}
}

module_init( ModInit );
module_exit( ModExit );
