#include <linux/module.h>
#include <linux/version.h>
#include <linux/timer.h>
#include <linux/sched.h>
#include <linux/init.h>

MODULE_LICENSE("GPL");

static struct timer_list MyTimer;

static void IncCount(unsigned long arg)
{
	printk("IncCount called (%ld)...\n",
		MyTimer.expires );
	MyTimer.expires = jiffies + (2*HZ); // 2 seconds
	add_timer( &MyTimer );
}

static int __init ktimerInit(void)
{
	init_timer( &MyTimer );
	MyTimer.function = IncCount;
	MyTimer.data = 0;
	MyTimer.expires = jiffies + (2*HZ); // 2 seconds
	add_timer( &MyTimer );
	return 0;
}

static void __exit ktimerExit(void)
{
	if( del_timer_sync( &MyTimer ) )
		printk("Activ timer deactivated.\n");
	else
		printk("No timer activ.\n");
}

module_init( ktimerInit );
module_exit( ktimerExit );
