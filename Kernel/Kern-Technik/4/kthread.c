#include <linux/module.h>
#include <linux/version.h>
#include <linux/init.h>
#include <linux/completion.h>

MODULE_LICENSE("GPL");

static int ThreadID=0;
static wait_queue_head_t wq;
static DECLARE_COMPLETION( OnExit );

static int ThreadCode( void *data )
{
	unsigned long timeout;
	int i;

	daemonize("MyKThread");
	allow_signal( SIGTERM );
	printk("ThreadCode startet ...\n");
	for( i=0; i<500; i++ ) {
		timeout=HZ;
		timeout=wait_event_interruptible_timeout(wq, (timeout==0), timeout);
		printk("ThreadCode: woke up ...\n");
		if( timeout==-ERESTARTSYS ) {
			printk("got signal, break\n");
			ThreadID = 0;
			break;
		}
	}
	printk("Normal end.\n");
	complete_and_exit( &OnExit, 0 );
}

static int __init kthreadInit(void)
{
	init_waitqueue_head(&wq);
	ThreadID=kernel_thread(ThreadCode, NULL, CLONE_KERNEL );
	if( ThreadID==0 )
		return -EIO;
	return 0;
}

static void __exit kthreadExit(void)
{
	kill_proc( ThreadID, SIGTERM, 1 );
	wait_for_completion( &OnExit );
}

module_init( kthreadInit );
module_exit( kthreadExit );
