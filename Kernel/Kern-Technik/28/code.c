#include <stdio.h>

void print_para_adr( unsigned int neutraler_parameter )
{
	printf("Adresse 2: %p\n",
		(char *)neutraler_parameter);
}

int main( int argc, char **argv )
{
	int var;

	printf("Adresse 1: %p\n", &var );
	print_para_adr( (int)&var );
	return 0;
}
