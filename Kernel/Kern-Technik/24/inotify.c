#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <linux/inotify.h>
#include <asm/unistd.h>

// Funktions-Wrapper fuer die Systemcalls
static inline int inotify_init (void)
{
	return syscall (__NR_inotify_init);
}

static inline int inotify_add_watch (int fd, const char *name, __u32 mask)
{
	return syscall (__NR_inotify_add_watch, fd, name, mask);
}

static inline int inotify_rm_watch (int fd, __u32 wd)
{
	return syscall (__NR_inotify_rm_watch, fd, wd);
}

// Eigentlicher Programmcode
static char *watch_event( int event )
{
	switch( event ) {
	case IN_ACCESS:
		return "IN_ACCESS";
	case IN_MODIFY:
		return "IN_MODIFY";
	case IN_ATTRIB:
		return "IN_ATTRIB";
	case IN_CLOSE_WRITE:
		return "IN_CLOSE_WRITE";
	case IN_CLOSE_NOWRITE:
		return "IN_CLOSE_NOWRITE";
	case IN_OPEN:
		return "IN_OPEN";
	case IN_MOVED_FROM:
		return "IN_MOVED_FROM";
	case IN_MOVED_TO:
		return "IN_MOVED_TO";
	case IN_CREATE:
		return "IN_CREATE";
	case IN_DELETE:
		return "IN_DELETE";
	case IN_DELETE_SELF:
		return "IN_DELETE_SELF";
	case IN_MOVE_SELF:
		return "IN_MOVE_SELF";
	case IN_UNMOUNT:
		return "IN_UNMOUNT";
	case IN_Q_OVERFLOW:
		return "IN_Q_OVERFLOW";
	case IN_IGNORED:
		return "IN_IGNORED";
	}
	return "unknown";
}

int main( int argc, char **argv )
{
	int fd, watch_descriptor, len, ret;
	struct inotify_event *ie;
	char buf[1000];
       
	if( argc != 2 ) {
		printf("usage: %s path\n", argv[0] );
		exit( -1 );
	}
	fd = inotify_init();
	if( fd<0 ) {
		exit( -2 );
	}
	watch_descriptor = inotify_add_watch(fd, argv[1],
		IN_ALL_EVENTS|IN_UMOUNT);
	while( 1 ) {
		len = read( fd, buf, sizeof(buf) );
		ie = (struct inotify_event *)&buf;
		while( len>0 ) {
			printf("wd=%04x, %16.16s (0x%08x), "
				"cookie=%04x, len=%04x ",
				ie->wd, watch_event(ie->mask),
				ie->mask, ie->cookie, ie->len);
			if( ie->len )
				printf("name=\"%s\"", ie->name );	
			putchar( '\n' );
			len -= sizeof(struct inotify_event)+ie->len;
			ie = (struct inotify_event *) ((char*)ie 
				+ sizeof(struct inotify_event)+ie->len);
		}
	}
	ret = inotify_rm_watch (fd, watch_descriptor);
	return 0;
}
