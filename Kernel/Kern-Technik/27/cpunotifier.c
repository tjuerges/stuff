#include <linux/fs.h>
#include <linux/notifier.h>
#include <linux/cpufreq.h>

static int my_cpufreq_notifier( struct notifier_block *nb, unsigned long val,
	void *data )
{
	struct cpufreq_freqs *freq = data;

	switch( val ) {
	case CPUFREQ_PRECHANGE:
		printk("CPUFREQ_PRECHANGE ");
		break;
	case CPUFREQ_POSTCHANGE:
		printk("CPUFREQ_POSTCHANGE ");
		break;
	case CPUFREQ_RESUMECHANGE:
		printk("CPUFREQ_RESUMECHANGE ");
		break;
	case CPUFREQ_SUSPENDCHANGE:
		printk("CPUFREQ_RESUMECHANGE ");
		break;
	default:
		printk("unknown val\n");
		break;
	}
	printk("cpu: %d, old: %d, new: %d\n", freq->cpu, freq->old, freq->new );
	return 0;
}

static struct notifier_block nb={
	.notifier_call = my_cpufreq_notifier,
};

static int __init mod_init(void)
{
	if( cpufreq_register_notifier(&nb,CPUFREQ_TRANSITION_NOTIFIER) ) {
		printk("cpufreq_register_notifer failed\n");
		return -EIO;
	}
	printk("mot_init: cpufreq_get: %d\n", cpufreq_get(0));// CPU 0
	return 0;
}

static void __exit mod_exit(void)
{
	cpufreq_unregister_notifier( &nb, CPUFREQ_TRANSITION_NOTIFIER );
}

module_init( mod_init );
module_exit( mod_exit );
MODULE_LICENSE("GPL");
