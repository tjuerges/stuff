#include <linux/fs.h>
#include <linux/delay.h>
#include <linux/cpufreq.h>

static int __init delaytest_init(void)
{
	long old_jiffies;
	cycles_t t1, t2;
	int freq=cpufreq_get(0)/1000;

	t1=get_cycles();
	old_jiffies = jiffies;
	udelay(1000);
	t2=get_cycles();
	printk(" 1000ms:  diff-jiffies: %2.2ld - (%ld)\n",
		jiffies-old_jiffies, (unsigned long)(t2-t1)/freq);
	old_jiffies = jiffies;
	t1 = t2;
	udelay(20000);
	t2=get_cycles();
	printk("20000ms:  diff-jiffies: %2.2ld - (%ld)\n",
		jiffies-old_jiffies, (unsigned long)(t2-t1)/freq);
	return -EIO; // Nur ein Test: Das erspart uns das Entladen.
}

module_init( delaytest_init );
MODULE_LICENSE("GPL");
