/* vim: set ts=4: */
#include <linux/fs.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/device.h>

#define DRIVER_MAJOR 240

// Metainformation
MODULE_LICENSE("GPL");

static struct file_operations Fops;

struct platform_device MyDev = {
    .name  = "MyDevice",
    .id    = 0,   // devid = "Speaker0"
};

static int __init DrvInit(void)
{
    if(register_chrdev(DRIVER_MAJOR, "MyDevice", &Fops) == 0) {
		platform_device_register( &MyDev );
		return 0;
	}
	return -EIO;
}

static void __exit DrvExit(void)
{
	platform_device_unregister( &MyDev );
	unregister_chrdev(DRIVER_MAJOR,"MyDevice");
}

module_init( DrvInit );
module_exit( DrvExit );
