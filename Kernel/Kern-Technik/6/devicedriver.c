#include <linux/fs.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/device.h>
#include <linux/completion.h>

#define DRIVER_MAJOR 240

MODULE_LICENSE("GPL");

static struct file_operations Fops;
static DECLARE_COMPLETION( DevObjectIsFree );
static int Frequenz; // Zustandsvariable des Ger�tes

static void mydevice_release( struct device *dev )
{
	complete( &DevObjectIsFree );
}

struct platform_device mydevice = {
    .name  = "MyDevice",
    .id    = 0,
	.dev = {
		.release = mydevice_release,
	}
};

static struct device_driver mydriver = {
	.name = "MyDevDrv",
	.bus = &platform_bus_type,
};

static ssize_t ReadFreq( struct device *dev, char *buf )
{
	snprintf(buf, 256, "Frequenz: %d", Frequenz ); 
    return strlen(buf)+1;
}

static ssize_t WriteFreq( struct device *dev, const char *buf, size_t count )
{
    Frequenz = simple_strtoul( buf, NULL, 0 );
    return strlen(buf)+1;
}

static DEVICE_ATTR( freq ,S_IRUGO|S_IWUGO, ReadFreq, WriteFreq );


static int __init DrvInit(void)
{
    if(register_chrdev(DRIVER_MAJOR, "MyDevice", &Fops) == 0) {
		driver_register(&mydriver);           // register the driver
		platform_device_register( &mydevice );// register the device
		mydevice.dev.driver = &mydriver;      // now tie them together
		device_bind_driver( &mydevice.dev );  // links the driver to the device
		device_create_file( &mydevice.dev, &dev_attr_freq );
		return 0;
	}
	return -EIO;
}

static void __exit DrvExit(void)
{
	device_remove_file( &mydevice.dev, &dev_attr_freq );
	device_release_driver( &mydevice.dev );
	platform_device_unregister( &mydevice );
	driver_unregister(&mydriver);
	unregister_chrdev(DRIVER_MAJOR,"MyDevice");
	wait_for_completion( &DevObjectIsFree );
}

module_init( DrvInit );
module_exit( DrvExit );
