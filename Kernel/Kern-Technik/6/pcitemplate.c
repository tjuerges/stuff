/* vim: set ts=4 aw ic sw=4: */
#include <linux/fs.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/pci.h>
#include <linux/interrupt.h>
#include <linux/device.h>

MODULE_LICENSE("GPL");

static void GameDeviceRelease( struct device *dev );
int GameClassHotplug( struct class_device *dev, char **envp,
		               int num_envp, char *buffer, int buffer_size);
void GameClassRelease( struct class_device *dev );

static struct device_driver GameDriver = {
	.name = "GameDriver",
	.bus = &platform_bus_type,
};
struct platform_device GameDevice = {
    .name  = "GameDevice",
    .id    = 0,
	.dev = {
		.release = GameDeviceRelease,
	}
};
static struct class GameClass = {
	.name = "GameClass",
	.hotplug = GameClassHotplug,
	.release = GameClassRelease,
};

static struct class_device GameClassDevice = {
	.class = &GameClass,
};
static struct file_operations Fops;
static DECLARE_COMPLETION( DevObjectIsFree );


static void GameDeviceRelease( struct device *dev )
{
	complete( &DevObjectIsFree );
}

int GameClassHotplug( struct class_device *dev, char **envp,
	int num_envp, char *buffer, int buffer_size)
{
	printk("GameClassHotplug( %p )\n", dev );
	return 0;
}
void GameClassRelease( struct class_device *dev )
{
	printk("GameClassRelease( %p )\n", dev );
}

static int __init ModInit(void)
{
    if(register_chrdev(240, "gamedevice", &Fops) == 0) {
		driver_register(&GameDriver);
		platform_device_register( &GameDevice );
		// now tie them together
		GameDevice.dev.driver = &GameDriver;
		device_bind_driver( &GameDevice.dev ); // links the driver to the dev
		class_register( &GameClass );
		GameClassDevice.dev = &GameDevice.dev;
		strlcpy( (void *)&GameClassDevice.class_id, "GameClassDevice", 16 );
		class_device_register( &GameClassDevice );
		return 0;
	}
	return -EIO;
}

static void __exit ModExit(void)
{
	printk("ModExit: %p\n", GameDevice.dev.driver );
	class_device_unregister( &GameClassDevice );
	class_unregister( &GameClass );
	device_release_driver( &GameDevice.dev );
	platform_device_unregister( &GameDevice );
	driver_unregister(&GameDriver);
	unregister_chrdev(240,"gamedevice");
	wait_for_completion( &DevObjectIsFree );
}

module_init( ModInit );
module_exit( ModExit );
