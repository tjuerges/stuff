/* vim: set ts=4: */
#include <linux/fs.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/device.h>

#define DRIVER_MAJOR 240

// Metainformation
MODULE_LICENSE("GPL");

static struct file_operations Fops;

static struct device_driver hellodrv = {
	.name = "MyDevDrv",
	.bus = &platform_bus_type,
};

static int __init DrvInit(void)
{
    if(register_chrdev(DRIVER_MAJOR, "MyDevice", &Fops) == 0) {
		driver_register(&hellodrv);
		return 0;
	}
	return -EIO;
}

static void __exit DrvExit(void)
{
	driver_unregister(&hellodrv);
	unregister_chrdev(DRIVER_MAJOR,"MyDevice");
}

module_init( DrvInit );
module_exit( DrvExit );
