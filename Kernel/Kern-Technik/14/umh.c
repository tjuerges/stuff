#include <linux/module.h>
#include <linux/init.h>

MODULE_AUTHOR("J�rgen Quade");
MODULE_LICENSE("GPL");

static int __init mod_init(void)
{
	int ret;
	char *argv[4] = {"/bin/cp", "/tmp/foo", "/tmp/bar", NULL};
	char *envp[3] = {"HOME=/", "PATH=/sbin:/bin:/usr/sbin:/usr/bin", NULL};

	printk("mod_init called\n");
	ret=call_usermodehelper(argv[0],argv,envp,1);
	if( ret<0 ) {
		printk("call_usermodehelper failed ...\n");
		return -EIO;
	}
	return -EIO;
//	return 0;
}

static void __exit mod_exit(void)
{
	printk("mod_exit called\n");
}

module_init( mod_init );
module_exit( mod_exit );
