#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/fs.h>

MODULE_LICENSE("GPL");

static char filename[255];
module_param_string( filename, filename, sizeof(filename), 666 );
static char buf[512];

static int __init mod_init(void)
{
	struct file *config_file;

	if( filename[0]=='\0' )
		strncpy( filename, "/etc/motd", sizeof(filename) );
	printk("opening filename: %s\n", filename);
	config_file = filp_open( filename, O_RDWR, 0 );
	printk("config_file: %p\n", config_file );
	if( IS_ERR( config_file ) )
		return -EIO;

	kernel_read( config_file, 0, buf, sizeof(buf) );
	printk( buf );
	filp_close( config_file, NULL );
	return 0;
}

static void __exit mod_exit(void)
{
}
module_init( mod_init );
module_exit( mod_exit );
/* vim:set ts=4 sw=4 ic aw: */
