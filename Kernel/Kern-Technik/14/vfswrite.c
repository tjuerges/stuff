#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/fs.h>
#include <asm/uaccess.h>

MODULE_LICENSE("GPL");

static char filename[255];
module_param_string( filename, filename, sizeof(filename), 666 );
struct file *log_file;

static int __init mod_init(void)
{
	mm_segment_t oldfs;

	if( filename[0]=='\0' )
		strncpy( filename, "/tmp/kernel_file", sizeof(filename) );
	printk("opening filename: %s\n", filename);
	log_file = filp_open( filename, O_WRONLY | O_CREAT, 0644 );
	printk("log_file: %p\n", log_file );
	if( IS_ERR( log_file ) )
		return -EIO;

	oldfs = get_fs();
	set_fs( KERNEL_DS );
	vfs_write( log_file, "hallo\n", 6, &log_file->f_pos );
	set_fs( oldfs );
	filp_close( log_file, NULL );
	return 0;
}

static void __exit mod_exit(void)
{
}
module_init( mod_init );
module_exit( mod_exit );
/* vim:set ts=4 sw=4 ic aw: */
