#include <stdio.h>
#include <sys/time.h>

int main( int argc, char **argv )
{
	struct timeval tv;
	unsigned long ttw=10;

	if( argc==2 )
		ttw = strtoul( argv[1], NULL, 0 );
	printf("%d second warp...\n", ttw);
	gettimeofday( &tv, NULL );
	tv.tv_sec += ttw;
	settimeofday( &tv, NULL );
	perror( "settimeofday" );
	return 0;
}
