/* vim: set ts=4 sw=4 aw ic: */
#include <linux/fs.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/usb.h>
#include <asm/uaccess.h>

MODULE_LICENSE( "GPL" );

#define USB_VENDOR_ID 0x05a9
#define USB_DEVICE_ID 0xa511

struct usb_device *dev;

static ssize_t usbtest_read( struct file *Instanz, char *buffer, size_t count,
		loff_t *ofs )
{
	__u16 *status;
	char pbuf[20];

	status = kmalloc( sizeof(__u16), GFP_KERNEL );
	if( status==NULL )
		return -ENOMEM;
	if( usb_control_msg(dev, usb_rcvctrlpipe(dev, 0), USB_REQ_GET_STATUS,
				 USB_DIR_IN | USB_TYPE_STANDARD | USB_RECIP_INTERFACE,
				0, 0, status, sizeof(status), 5*HZ) < 0 ) {
		kfree( status );
		return -EIO;
	}
	snprintf( pbuf, sizeof(pbuf), "status=%d\n", *status );
	kfree( status );
	if( strlen(pbuf) < count )
		count = strlen(pbuf);
	count -= copy_to_user(buffer,pbuf,count);
	return count;
}

static int usbtest_open( struct inode *devicefile, struct file *Instanz )
{
	return 0;
}

static struct file_operations USBFops = {
	.owner = THIS_MODULE,
	.open  = usbtest_open,
	.read  = usbtest_read,
};

static struct usb_device_id usbid [] = {
	{ USB_DEVICE(0x05a9, 0xa511), },
	{ }                 /* Terminating entry */
};

struct usb_class_driver ClassDescr = {
	.name = "usbtest",
	.fops = &USBFops,
	.mode = S_IFCHR | S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH,
	.minor_base = 16,
};

static int usbtest_probe(struct usb_interface *interface,
	const struct usb_device_id *id)
{
	dev = interface_to_usbdev(interface);
	printk("USBTEST: 0x%4.4x|0x%4.4x, if=%p\n", dev->descriptor.idVendor,
		dev->descriptor.idProduct, interface );
	if(dev->descriptor.idVendor==USB_VENDOR_ID
		&& dev->descriptor.idProduct==USB_DEVICE_ID) {
		if( usb_register_dev( interface, &ClassDescr ) ) {
			return -EIO;
		}
		printk("got minor= %d\n", interface->minor );
		return 0;
	}
	return -ENODEV;
}

static void usbtest_disconnect( struct usb_interface *iface )
{
	usb_deregister_dev( iface, &ClassDescr );
}

static struct usb_driver usbtest = {
	.name= "usbtest",
	.id_table= usbid,
	.probe= usbtest_probe,
	.disconnect= usbtest_disconnect,
};

static int __init usbtest_init(void)
{
	if( usb_register(&usbtest) ) {
		printk("usbtest: unable to register usb driver\n");
		return -EIO;
	}
	return 0;
}

static void __exit usbtest_exit(void)
{
	usb_deregister(&usbtest);	
}

module_init(usbtest_init);
module_exit(usbtest_exit);
