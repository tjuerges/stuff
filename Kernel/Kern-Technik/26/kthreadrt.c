#include <linux/module.h>
#include <asm/delay.h>
#include <asm/atomic.h>

static int thread_id_nort=0, thread_id_rt=0;
static DECLARE_COMPLETION( on_exit );
static atomic_t active = ATOMIC_INIT(0);
static int interrupted = 0, not_interrupted = 0;
static int disable=0;
module_param( disable, int, 0666 );

static int thread_nort( void *data )
{
	unsigned long remaining;

	daemonize("normal-kthread");
	allow_signal( SIGTERM );
	while( 1 ) {
		if( disable ) preempt_disable();
		atomic_set( &active, 1 );
		udelay( 1000 );
		set_task_state( current, TASK_INTERRUPTIBLE );
		atomic_set( &active, 0 );
		if( disable ) preempt_enable();
		remaining = schedule_timeout( 10 );
		if( signal_pending(current) ) {
			thread_id_nort = 0;
			break;
		}
	}
	complete_and_exit( &on_exit, 0 );
}

static int thread_rt( void *data )
{
	unsigned long remaining;
	struct sched_param sched_params;

	daemonize("rt-kthread");
	allow_signal( SIGTERM );

	sched_params.sched_priority = 50;
	if( sched_setscheduler(current,SCHED_RR,&sched_params)!=0 ) {
		printk("can't set realtime priority ...\n");
		complete_and_exit( &on_exit, -1 );
	}

	while( 1 ) {
		set_task_state( current, TASK_INTERRUPTIBLE );
		remaining = schedule_timeout( 7 );
		if( atomic_add_return(0,&active) )
			interrupted++;
		else
			not_interrupted++;
		if( signal_pending(current) ) {
			thread_id_rt = 0;
			break;
		}
	}
	printk("    interrupted: %d\nnot interrupted: %d\n",
		interrupted, not_interrupted );
	complete_and_exit( &on_exit, 0 );
}

static int __init kthread_init(void)
{
#ifndef CONFIG_PREEMPT
	printk("KERNEL DOES NOT SUPPORT KERNEL-PREEMPTION!\n");
#endif
	thread_id_nort=kernel_thread(thread_nort,NULL,CLONE_KERNEL);
	if( thread_id_nort==0 )
		return -EIO;
	thread_id_rt=kernel_thread(thread_rt,NULL,CLONE_KERNEL);
	if( thread_id_rt==0 ) {
		kill_proc( thread_id_nort, SIGTERM, 1 );
		wait_for_completion( &on_exit );
		return -EIO;
	}
	if( disable )
		printk("Kernel-Preemption in kernel-thread disabled...\n");
	else
		printk("Kernel-Preemption in kernel-thread activated...\n");
	return 0;
}

static void __exit kthread_exit(void)
{
	kill_proc( thread_id_nort, SIGTERM, 1 );
	kill_proc( thread_id_rt, SIGTERM, 1 );
	wait_for_completion( &on_exit );
	wait_for_completion( &on_exit );
}

module_init( kthread_init );
module_exit( kthread_exit );
MODULE_LICENSE("GPL");
