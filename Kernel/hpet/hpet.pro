#
# $Id$
#

TEMPLATE = app
TARGET = hpet

LANGUAGE = C

#DEFINES +=

#CONFIG += qt rtti stl warn_on release
CONFIG -= qt

QMAKE_LINK = gcc

#QMAKE_LFLAGS += -g

QMAKE_CFLAGS_RELEASE += $$system(cflags.sh QMAKE_RELEASE_C)
QMAKE_CFLAGS_DEBUG += $$system(cflags.sh QMAKE_DEBUG_C)

#TEMPLATE = subdirs
#SUBDIRS += foo bar

SOURCES += \
	hpet.c

