/*
 * $Id$
 */


#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/vmalloc.h>


static void* foo = 0;
static unsigned long size = 1024UL * 1024UL;
module_param(size, ulong, S_IRUGO);


int init_module(void)
{
    printk("Hello world\nvmalloc %lu bytes...\n", size);
    foo = vmalloc(size);
    if(foo != 0)
    {
        printk("Allocation successful. Freeing it.\n");
        vfree(foo);
    }

    foo = NULL;

    printk("Hello world\nkmalloc %lu bytes...\n", size);
    foo = kmalloc(size, GFP_ATOMIC);
    if(foo != 0)
    {
        printk("kmalloc successful. Freeing it.\n");
        kfree(foo);
        return 0;
    }

    return -ENOMEM;
}

void cleanup_module(void)
{
}
