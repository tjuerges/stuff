# $Id$

TEMPLATE = app
TARGET = kernel_version

LANGUAGE=C
QMAKE_LINK=gcc

QMAKE_CFLAGS_RELEASE = -g \
	-O2 \
	-DCOMPILATION_DATE=\"`date --iso-8601=seconds`\" \
	-DVERSION=\"`./compilation_run`\"
;;	-march=pentium4m \
;;	-mfpmath=sse,387 \
;;	-mmmx \
;;	-m3dnow \
;;	-msse \
;;	-msse2

;;TEMPLATE = subdirs
;;SUBDIRS += foo bar

HEADERS += 

SOURCES += kernel_version.c
