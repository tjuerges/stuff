#include <stdio.h>
#include <linux/version.h>

int main(int n, char* args[])
{
	#if KERNEL_VERSION(2,4,20) == LINUX_VERSION_CODE
		printf("2.4.20");
	#elif LINUX_VERSION_CODE >= KERNEL_VERSION(2,60,0)
		printf(">= 2.60.0");
	#elif LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,0)
		printf(">= 2.6.0");
	#else
		printf("No idea: %d", LINUX_VERSION_CODE);
	#endif

	printf("\n");

	return 0;
}
