/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2007 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id$"
 *
 * who       when      what
 * --------  --------  ----------------------------------------------
 * tjuerges  Sep 10, 2007  created 
 */


#include <linux/module.h>
#include <linux/init.h>
#include <linux/moduleparam.h>
#include <linux/kprobes.h>
#include <linux/kallsyms.h>
#include <rtLog.h>


/*
 * The kernel module name.
 */
#define modName "oopsWatchdog"

MODULE_AUTHOR ("Thomas Juerges, <tjuerges@nrao.edu>");
MODULE_DESCRIPTION ("Kernel Oops watch dog for ALMA");
MODULE_LICENSE("GPL");

/*
 * The name of the kernel function to be intercepted.
 */
#define DEBUGTHIS "do_invalid_op"
/*
 * Make the method name available as parameter.
 */
static char* debugThisName = DEBUGTHIS;
module_param(debugThisName, charp, S_IRUGO);
MODULE_PARM_DESC(debugThisName, "Provide the name of a kernel "
                "function which shall be intercepted before executed. The default "
                "value is " DEBUGTHIS " in order to report kernel Oopses.");

/*
 * Structure which contains the necessary probes, i.e. handler addresses. I use
 * only the pre-execution probe.
 */
static struct kprobe kernelProbe;

static int beforeExecution(struct kprobe* kpr, struct pt_regs* p)
{
	printk( "\nBefore the watchdog gets food.\n");
    return 0;
}

/*
 * If ever necessary. activate this probe, too. Don't forget to activate it in
 * kernelProbe, too!
static int afterExecution(struct kprobe* kpr, struct pt_regs* p, unsigned long flags)
{
	printk( "\nAfter the watchdog got food.\n");
	return 0;
}
 */ 

static void __exit exit_kprobe(void)
{
	printk("\nModule exiting \n");
	unregister_kprobe(&kernelProbe);
}

static int __init init_kprobe(void)
{
	unsigned long debugThis = 0UL;
	int ret = -1;

	printk("\nTrying to find the symbol address for %s", debugThisName);
	debugThis = kallsyms_lookup_name(debugThisName);
	if(debugThis != 0)
	{
	    /*
	     * Just to be on the safe side, clear the kprobe structure.
	     */
	    memset(&kernelProbe, 0, sizeof(struct kprobe));

	    /* Registering a kprobe */
		printk("\nInserting the kprobe for symbol %s\n", debugThisName);

		kernelProbe.pre_handler = (kprobe_pre_handler_t)(beforeExecution);
		/*
		 * Do not register a post-probe. In ALMA we do not need it.
		kernelProbe.post_handler = (kprobe_post_handler_t)(afterExecution);
         */
		kernelProbe.addr = (kprobe_opcode_t*)(debugThis);

		printk("\nAddress where the kprobe is\ngoing to be inserted = 0x%p\n",
		    kernelProbe.addr);

		register_kprobe(&kernelProbe);
		ret = 0;
	}
	else
	{
		printk("\nCould not find the address for the symbol %s", debugThisName);
	}

	return ret;
}

module_init(init_kprobe);
module_exit(exit_kprobe);
