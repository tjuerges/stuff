#
# $Id: fft.pro 261 2010-06-11 22:36:19Z thomas $
#

TEMPLATE = app
TARGET = fft2

LANGUAGE = C++

CONFIG += rtti stl exceptions debug warn_on
CONFIG -= qt

QMAKE_CXXFLAGS_RELEASE += $$system(cflags.sh QMAKE_RELEASE_CXX)
QMAKE_CXXFLAGS_DEBUG += $$system(cflags.sh QMAKE_DEBUG_CXX)

INCLUDEPATH += /export/home/delphinus/tjuerges/soft/installed/include

LIBS += -L/export/home/delphinus/tjuerges/soft/installed/lib
LIBS += -lgsl
LIBS += -lgslcblas
LIBS += -lm

SOURCES += fft2.cpp

