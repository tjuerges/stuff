#! /usr/bin/env python
#
# $Id: genData.py 261 2010-06-11 22:36:19Z thomas $
#

import math

sigma = 0.5
factor = 1.0 / (sigma * math.sqrt(2.0 * math.pi))
exponentFactor = - 1.0 / (2.0 * sigma**2)
samplerate = 50.0
frequency = 200.0
min = -10.0
max = 10.0
step = (max - min) / (frequency * samplerate)

print "sample rate = %f, frequency = %f, min = %f, max = %f, step = %f" % (samplerate, frequency, min, max, step)

f = file("data", "w+")

i = min
while i < max:
    f.write("%.12f\n" % (math.sin(frequency * i)))
    i += step

f.close()
