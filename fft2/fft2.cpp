//
// $Id$
//


#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <cmath>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_fft_real.h>
#include <gsl/gsl_fft_halfcomplex.h>


int main(int argc, char* argv[])
{
    if(argc != 3)
    {
        std::cout << "Usage: "
            << argv[0]
            << " sampleRate [double] file name [string, no spaces or quoted]"
                "\n\n";
        return -1;
    }


    // Convert argv[1] to a sample rate.
    double sampleRate(0.0);
    try
    {
        std::istringstream in(argv[1]);
        in >> sampleRate;
    }
    catch(...)
    {
        std::cout << "Cannot convert the sample rate to a double.\n\n";
        return -1;
    }

    if(sampleRate <= 0.0)
    {
        std::cout << "Sample rate has to be > 0.0!\n\n";
        return -1;
    }
    else
    {
        std::cout << "Sample rate = "
            << sampleRate
            << "\n";
    }

    // Convert argv[3] to an input file.
    std::ifstream input(argv[2], std::ios_base::in);
    if(!input)
    {
        std::cout << "No data file!\n\n";
        return -1;
    }
    else
    {
        std::cout << "Reading file "
            << argv[2]
            << ".\n";
    }


    // Read the data.
    std::vector< double > data;
    double dummy(0UL);

    while(true)
    {
        input >> dummy;
        if(input.good() == true)
        {
            data.push_back(dummy);
        }
        else
        {
            input.close();
            break;
        }
    };

    std::cout << "Read "
        << data.size()
        << " elements.\n";

    // Pad data if the size is odd.
    if((data.size() % 2) == 1)
    {
        data.push_back(0.0);
    }

    const unsigned long long size(data.size());

    // Vector for the FFT values.
    gsl_fft_real_wavetable* real = gsl_fft_real_wavetable_alloc(size);
    std::cout << "real = "
        << real
        << "\n";
    gsl_fft_real_workspace* work = gsl_fft_real_workspace_alloc(size);
    std::cout << "work = "
            << work
            << "\n";
    std::cout << "gsl_fft_real_transform = "
        << gsl_fft_real_transform(&(data[0]), 1, size, real, work)
        << "\n";
    gsl_fft_real_wavetable_free(real);
    gsl_fft_real_workspace_free(work);

    // Write the FFT data to a file.
    const std::string fileName(std::string(argv[2]) + std::string(".fft"));
    std::ofstream output(fileName.c_str(), std::ios_base::out);
    if(!output)
    {
        std::cout << "Cannot create result file!\n\n";
        return -1;
    }
    else
    {
        // 15 digits precision.
        output.setf(std::ios::fixed, std::ios::floatfield);
        output.precision(10);

        unsigned long long index(0U);
        double amplitude(0.0);
        double phase(0.0);
        std::vector< double >::const_iterator iter(data.begin());
        for(; index < size; ++index)
        {
            phase = std::atan2(*iter, *(iter + 1));
            amplitude =
                std::sqrt(((*iter) * (*iter)) +  ((*(iter + 1)) * (*(iter + 1))));

            output << index
                << " "
                << *iter
                << " "
                << *(iter + 1)
                << " "
                << amplitude
                << " "
                << phase
                << "\n";

            iter += 2;
        }

        output.close();
    }

    return 0;
}
