#!/usr/bin/perl

# Generate postscript and png plot with GNUplot
# (C) 2005 www.captain.at

# set custom font path
$ENV{GDFONTPATH} = "/usr/share/fonts/truetype/ttf-bitstream-vera/";

# GNUPLOT POSTSCRIPT
open (GNUPLOT, "|gnuplot");
print GNUPLOT <<EOPLOT;
set term post color "Courier" 12
set output "data.ps"
set size 1 ,1
set nokey
set data style line
set xlabel "frequency [Hz]" font "Courier,14"
set xrange [0:2000]
set yrange [0:4000000]
set title "FFT" font "Courier,14"
set grid xtics ytics
set xtics 100
plot "data.dat" using 1:2 w lines 1
EOPLOT
close(GNUPLOT);

# GNUPLOT PNG
open (GNUPLOT, "|gnuplot");
print GNUPLOT <<EOPLOT;
set term png small xFFFFFF font "VeraMono" 8
set output "data.png"
set size 1 ,1
set nokey
set data style line
set xlabel "frequency [Hz]" font "VeraMono,10"
set xrange [0:2000]
set yrange [0:4000000]
set title "FFT" font "VeraMono,10"
set grid xtics ytics
set xtics 100
plot "data.dat" using 1:2 w lines 1
EOPLOT
close(GNUPLOT);
