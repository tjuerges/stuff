#include <iostream>
#include <sstream>
#include <iomanip>
#include <cstring>
#include <cerrno>
#include <unistd.h>

#include <boost/format.hpp>

#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <netinet/in.h>


static void printError(int line)
{
        std::cout << "Error in line #"
            << line
            << ": "
            << std::strerror(errno)
            << "\n";
}


int main(int _z, char* _y[])
{
    int socketFd(socket(AF_INET, SOCK_DGRAM, IPPROTO_IP));
    if(socketFd == -1)
    {
        printError(__LINE__);
        return -1;
    }

    struct if_nameindex* ifNameIndex(if_nameindex());
    if(ifNameIndex == NULL)
    {
        close(socketFd);
        printError(__LINE__);
        return -1;
    }

    std::size_t numberOfInterfaces(0U);
    for(struct if_nameindex* i(ifNameIndex);
        !(i->if_index == 0 && i->if_name == NULL); ++i, ++numberOfInterfaces)
    {
        struct ifreq interface;
        memset(&interface, 0, sizeof(ifreq));
        strcpy(interface.ifr_name, i->if_name);
        std::ostringstream s;
        if(0 == ioctl(socketFd, SIOCGIFHWADDR, &interface))
        {
            unsigned char b[6];
            for(std::size_t i(0); i <= 5; ++i)
            {
                b[i] = interface.ifr_addr.sa_data[i];
            }

            s << std::hex
                << std::setw(2)
                << std::setfill('0')
                << static_cast< unsigned int >(b[0])
                << ":"
                << static_cast< unsigned int >(b[1])
                << ":"
                << static_cast< unsigned int >(b[2])
                << ":"
                << static_cast< unsigned int >(b[3])
                << ":"
                << static_cast< unsigned int >(b[4])
                << ":"
                << static_cast< unsigned int >(b[5]);
        }

        std::cout << "Interface index = "
        << i->if_index
        << ", interface name = \""
        << i->if_name
        << "\", hardware-address = "
        << s.str()
        << "\n";
    }

    if_freenameindex(ifNameIndex);

    std::cout << "Number of interfaces = "
    << numberOfInterfaces
    << "\n";

    close(socketFd);

    return 0;
}
