#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/socket.h>
#include <netdb.h>
#include <sys/ioctl.h>


static void error(const char *msg)
{
    perror(msg);
    exit(-1);
}


static ssize_t recvTimeout(int s, char* buf, int len, double timeout)
{
    // Check preconditions
    if(s < 0)
    {
        printf("No valid socket!\n");
        return -1;
    }

    if((buf == NULL) || (timeout < 0) || (len <= 0))
    {
        printf("Invalid arguments !!\n");
        return -2;
    }

    // We will sleep for 0.1s.  Calculate how many times we got to go
    // through the loop.
    //configure timeout value
    const double minimumTimeout = 0.1;
    if(timeout < minimumTimeout)
    {
        return -2;
    }

    const struct timespec ts = {0, (long int)(minimumTimeout * 1e9)};
    unsigned int numberOfCycles = (unsigned int)(timeout / minimumTimeout);
    int ioctlResult = -1;
    size_t availableBytes = 0U;
    ssize_t n = 0;

    do
    {
        availableBytes = 0U;
        ioctlResult = ioctl(s, FIONREAD, &availableBytes);
        printf("ioctl = %d, bytes available = %lu\n",
            ioctlResult, availableBytes);

        if((ioctlResult == 0) && (availableBytes > 0U))
        {
            n = recv(s, buf, availableBytes, 0);
            break;
        }

        printf("Timeout = %fs, cycle = %u, sleep for %fs\n\n",
            timeout, numberOfCycles,
            (double)ts.tv_sec + (double)ts.tv_nsec / 1e9);

        nanosleep(&ts, NULL);
        --numberOfCycles;
    }
    while(numberOfCycles > 0U);

    if(n == 0)
    {
        printf("Network timeout!\n");
        return -3;
    }
    else if(n == -1)
    {
        printf("Network error!\n");
        return -4;
    }

    return n;
}


int main(int _y, char* _z[])
{
    if((_y != 4))
    {
        printf("Usage: %s HOST PORT Message\n\n", _z[0]);
        return -1;
    }

    char* ip = _z[1];
    char* port = _z[2];

    struct addrinfo hints;
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_canonname = NULL;
    hints.ai_addr = NULL;
    hints.ai_next = NULL;
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_NUMERICSERV;

    struct addrinfo* result;
    if(getaddrinfo(ip, port, &hints, &result) != 0)
    {
        error("ERROR resolving the name");
    }

    struct addrinfo* rp;
    int connection = -1;
    for(rp = result; rp != NULL; rp = rp->ai_next)
    {
        connection = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if(connection == -1)
        {
            continue;
        }

        if(connect(connection, rp->ai_addr, rp->ai_addrlen) != -1)
        {
            break;
        }

        close(connection);
    }

    if(rp == NULL)
    {
        error("ERROR connecting");
    }

    freeaddrinfo(result);

    const struct timespec t = {1, 0};
    char message[1024];
    memset(message, 0, sizeof(message));
    strncpy(message, _z[3], sizeof(message));
    char buffer[1024] = {};
    const int len = sizeof(buffer);
    ssize_t n = -1;
    do
    {
        if((n = write(connection, &message, strlen(message))) < 0)
        {
            error("ERROR writing message to socket");
        }
        else if((n = recvTimeout(connection, buffer, len, 5.0)) > 0)
        {
            printf("Received %ld bytes:  \"%s\".\n", n, buffer);
        }

        nanosleep(&t, NULL);
    }
    while(1 == 1);

    close(connection);
    return 0;
}
