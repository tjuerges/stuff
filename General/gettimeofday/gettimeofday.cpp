/**
 * $Id$
 */


#include <cerrno>
#include <sstream>
#include <sys/time.h>
#include <iostream>
#include <cstdlib>


int main(int argc, char* argv[])
{
    if(argc != 2)
    {
        std::cout << "\n\nPlease provide the number of executions of "
            "gettimeofday!\n\n";
        return -EINVAL;
    }

    std::stringstream value;
    value << argv[1];
    unsigned long long LOOP(0ULL);
    value >> LOOP;

    timeval startTime;
    timeval endTime;
    timeval* endTime_p(&endTime);
 
    gettimeofday(&startTime, 0);
 
    unsigned long long int i(LOOP);
    for (; i > 0ULL; --i)
    {
        gettimeofday(endTime_p, 0);
    }
 
    double t(1e6 * (endTime.tv_sec - startTime.tv_sec)
        + endTime.tv_usec - startTime.tv_usec);
 
    std::cout << "Number of calls: " << i << ".\n"
        << "Total elapsed time: " << t / 1e6 << "s.\n"
        << "Time per call: " << t / LOOP << "us.\n";
 
    return EXIT_SUCCESS;
};
