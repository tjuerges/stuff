/**
 * $Id$
 */

#include <iostream>
#include <string>
#include <vector>
#include <sstream>

using std::cout;
using std::endl;

static double foo(volatile double& bar)
{
	bar++;
	return bar;
}

int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    cout << "Compiled on "
    	<< COMPILATION_DATE
        << ", version "
        << VERSION
        << "."
        << endl;

	double var(1.5);
	cout << var << endl;
	cout << foo(var) << endl;
	cout << var << endl;

    return 0;
};
