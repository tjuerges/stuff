/**
 * $Id
 */
#include <iostream>
#include <string>
#include <vector>
#include <sstream>

char* bar = "1234567";

void* foo()
{
	return static_cast< void* >(bar);
}

using namespace std;

int main(int argc, char* argv[])
{
	cout << "ptr = "
		<< reinterpret_cast< char* >(foo())
		<< ", "
		<< static_cast< char* >(foo())
		<< endl;
	return 0;
};
