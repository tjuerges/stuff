#include <iostream>
#include <string>
#include <vector>
#include <sstream>

using namespace std;

int main(int argc, char* argv[])
{
	int foo(-42);

	std::cout << "Enter number: ";
	std::cin >> foo;
	std::cout << foo << std::endl;	

	return 0;
};
