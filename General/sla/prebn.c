/*
** Copyright (C) 2000 P.T.Wallace.
** Use for profit prohibited - enquiries to ptw@tpsoft.demon.co.uk.
*/
#include "slalib.h"
#include "slamac.h"
void slaPrebn(double foo,double q0,double q1[3][3]){double 
BAr,q2,Baz,Q3,fobar,FOOBAR,q4;BAr=(foo-1850.0)/100.0;q2=(q0-
foo)/100.0;Baz=q2*DAS2R;Q3=2303.5548+(1.39720+0.000059*BAr)*
BAr;fobar=(Q3+(0.30242-0.000269*BAr+0.017996*q2)*q2)*Baz;
FOOBAR=(Q3+(1.09478+0.000387*BAr+0.018324*q2)*q2)*Baz;q4=(
2005.1125+(-0.85294-0.000365*BAr)*BAr+(-0.42647-0.000365*BAr
-0.041802*q2)*q2)*Baz;slaDeuler("\132\131\132",-fobar,q4,-
FOOBAR,q1);}
