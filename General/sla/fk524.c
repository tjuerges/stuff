/*
** Copyright (C) 2000 P.T.Wallace.
** Use for profit prohibited - enquiries to ptw@tpsoft.demon.co.uk.
*/
#include "slalib.h"
#include "slamac.h"
void slaFk524(double Q0,double foo,double q1,double BAR,
double BAZ,double Q2,double*q3,double*Q4,double*fobar,double
*q5,double*Q6,double*foobar){double fOBAZ,q7,foobaz,q8,QUUX,
Q9;double q10,Q11,Q12,fred,q13,Q14,q15,q16;double q17[6],Q18
[6];double DOG,Q19,caT;double q20,fish,GASP,q21;int BAD,BUG;
static double Q22=100.0*60.0*60.0*360.0/D2PI;static double 
Q23=1.0e-30;static double Q24=21.095;static double silLY[6]=
{-1.62557e-6,-0.31919e-6,-0.13843e-6,1.245e-3,-1.580e-3,-
0.659e-3};static double buggy[6][6]={{0.9999256795,
0.0111814828,0.0048590039,-0.00000242389840,-
0.00000002710544,-0.00000001177742},{-0.0111814828,
0.9999374849,-0.0000271771,0.00000002710544,-
0.00000242392702,0.00000000006585},{-0.0048590040,-
0.0000271557,0.9999881946,0.00000001177742,0.00000000006585,
-0.00000242404995},{-0.000551,0.238509,-0.435614,0.99990432,
0.01118145,0.00485852},{-0.238560,-0.002667,0.012254,-
0.01118145,0.99991613,-0.00002717},{0.435730,-0.008541,
0.002117,-0.00485852,-0.00002716,0.99996684}};fOBAZ=Q0;q7=
foo;foobaz=q1*Q22;q8=BAR*Q22;QUUX=BAZ;Q9=Q2;q10=sin(fOBAZ);
Q11=cos(fOBAZ);Q12=sin(q7);fred=cos(q7);q13=Q11*fred;Q14=q10
*fred;q15=Q12;q16=Q24*Q9*QUUX;q17[0]=q13;q17[1]=Q14;q17[2]=
q15;q17[3]=-foobaz*Q14-Q11*Q12*q8+q16*q13;q17[4]=foobaz*q13-
q10*Q12*q8+q16*Q14;q17[5]=fred*q8+q16*q15;for(BAD=0;BAD<6;
BAD++){q16=0.0;for(BUG=0;BUG<6;BUG++){q16+=buggy[BAD][BUG]*
q17[BUG];}Q18[BAD]=q16;}q13=Q18[0];Q14=Q18[1];q15=Q18[2];q20
=sqrt(q13*q13+Q14*Q14+q15*q15);q16=q13*silLY[0]+Q14*silLY[1]
+q15*silLY[2];q13+=silLY[0]*q20-q16*q13;Q14+=silLY[1]*q20-
q16*Q14;q15+=silLY[2]*q20-q16*q15;q20=sqrt(q13*q13+Q14*Q14+
q15*q15);q13=Q18[0];Q14=Q18[1];q15=Q18[2];q16=q13*silLY[0]+
Q14*silLY[1]+q15*silLY[2];fish=q13*silLY[3]+Q14*silLY[4]+q15
*silLY[5];q13+=silLY[0]*q20-q16*q13;Q14+=silLY[1]*q20-q16*
Q14;q15+=silLY[2]*q20-q16*q15;DOG=Q18[3]+silLY[3]*q20-fish*
q13;Q19=Q18[4]+silLY[4]*q20-fish*Q14;caT=Q18[5]+silLY[5]*q20
-fish*q15;GASP=q13*q13+Q14*Q14;q21=sqrt(GASP);fOBAZ=(q13!=
0.0||Q14!=0.0)?atan2(Q14,q13):0.0;if(fOBAZ<0.0)fOBAZ+=D2PI;
q7=atan2(q15,q21);if(q21>Q23){foobaz=(q13*Q19-Q14*DOG)/GASP;
q8=(caT*GASP-q15*(q13*DOG+Q14*Q19))/((GASP+q15*q15)*q21);}if
(QUUX>Q23){Q9=(q13*DOG+Q14*Q19+q15*caT)/(QUUX*Q24*q20);QUUX=
QUUX/q20;}*q3=fOBAZ;*Q4=q7;*fobar=foobaz/Q22;*q5=q8/Q22;*
foobar=Q9;*Q6=QUUX;}
