/*
** Copyright (C) 2000 P.T.Wallace.
** Use for profit prohibited - enquiries to ptw@tpsoft.demon.co.uk.
*/
#include "slalib.h"
#include "slamac.h"
void slaDvn(double fOo[3],double baR[3],double*BAZ){int 
FOBAR;double fOOBAr,q0;fOOBAr=0.0;for(FOBAR=0;FOBAR<3;FOBAR
++){q0=fOo[FOBAR];fOOBAr+=q0*q0;}fOOBAr=sqrt(fOOBAr);*BAZ=
fOOBAr;fOOBAr=(fOOBAr>0.0)?fOOBAr:1.0;for(FOBAR=0;FOBAR<3;
FOBAR++){baR[FOBAR]=fOo[FOBAR]/fOOBAr;}}
