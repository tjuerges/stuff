/*
** Copyright (C) 2000 P.T.Wallace.
** Use for profit prohibited - enquiries to ptw@tpsoft.demon.co.uk.
*/
#include "slalib.h"
#include "slamac.h"
void slaDav2m(double q0[3],double q1[3][3]){double FoO,bar,
Q2,baz,FOBAR,FOOBAR,FobAZ;FoO=q0[0];bar=q0[1];Q2=q0[2];baz=
sqrt(FoO*FoO+bar*bar+Q2*Q2);FOBAR=sin(baz);FOOBAR=cos(baz);
FobAZ=1.0-FOOBAR;if(baz!=0.0){FoO=FoO/baz;bar=bar/baz;Q2=Q2/
baz;}q1[0][0]=FoO*FoO*FobAZ+FOOBAR;q1[0][1]=FoO*bar*FobAZ+Q2
*FOBAR;q1[0][2]=FoO*Q2*FobAZ-bar*FOBAR;q1[1][0]=FoO*bar*
FobAZ-Q2*FOBAR;q1[1][1]=bar*bar*FobAZ+FOOBAR;q1[1][2]=bar*Q2
*FobAZ+FoO*FOBAR;q1[2][0]=FoO*Q2*FobAZ+bar*FOBAR;q1[2][1]=
bar*Q2*FobAZ-FoO*FOBAR;q1[2][2]=Q2*Q2*FobAZ+FOOBAR;}
