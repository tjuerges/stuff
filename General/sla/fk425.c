/*
** Copyright (C) 2000 P.T.Wallace.
** Use for profit prohibited - enquiries to ptw@tpsoft.demon.co.uk.
*/
#include "slalib.h"
#include "slamac.h"
void slaFk425(double FoO,double BAR,double Q0,double Q1,
double Baz,double q2,double*FobAr,double*Q3,double*q4,double
*q5,double*FOOBAR,double*FObaz){double foobaz,QUux,q6,FrEd,
doG,q7,Q8,Q9,CAT,Q10,fiSH,q11,gasp,q12,q13,BAd,Q14,q15,BuG,
Q16,silly,q17,buggy,Q18;int q19,MUm;double dAD[3],q20[3];
double disk[6],q21[6];static double empty=100.0*60.0*60.0*
360.0/D2PI;double Q22=1.0e-30;double FULL=21.095;static 
double q23[3]={-1.62557e-6,-0.31919e-6,-0.13843e-6};static 
double FAST[3]={1.245e-3,-1.580e-3,-0.659e-3};static double 
SMALL[6][6]={{0.9999256782,-0.0111820611,-0.0048579477,
0.00000242395018,-0.00000002710663,-0.00000001177656},{
0.0111820610,0.9999374784,-0.0000271765,0.00000002710663,
0.00000242397878,-0.00000000006587},{0.0048579479,-
0.0000271474,0.9999881997,0.00000001177656,-0.00000000006582
,0.00000242410173},{-0.000551,-0.238565,0.435739,0.99994704,
-0.01118251,-0.00485767},{0.238514,-0.002667,-0.008541,
0.01118251,0.99995883,-0.00002718},{-0.435623,0.012254,
0.002117,0.00485767,-0.00002714,1.00000956}};foobaz=FoO;QUux
=BAR;q6=Q0*empty;FrEd=Q1*empty;doG=Baz;q7=q2;Q8=sin(foobaz);
Q9=cos(foobaz);CAT=sin(QUux);Q10=cos(QUux);dAD[0]=Q9*Q10;dAD
[1]=Q8*Q10;dAD[2]=CAT;fiSH=FULL*q7*doG;q20[0]=(-Q8*Q10*q6)-(
Q9*CAT*FrEd)+(fiSH*dAD[0]);q20[1]=(Q9*Q10*q6)-(Q8*CAT*FrEd)+
(fiSH*dAD[1]);q20[2]=(Q10*FrEd)+(fiSH*dAD[2]);fiSH=(dAD[0]*
q23[0])+(dAD[1]*q23[1])+(dAD[2]*q23[2]);q11=(dAD[0]*FAST[0])
+(dAD[1]*FAST[1])+(dAD[2]*FAST[2]);for(q19=0;q19<3;q19++){
disk[q19]=dAD[q19]-q23[q19]+fiSH*dAD[q19];disk[q19+3]=q20[
q19]-FAST[q19]+q11*dAD[q19];}for(q19=0;q19<6;q19++){fiSH=0.0
;for(MUm=0;MUm<6;MUm++){fiSH+=SMALL[q19][MUm]*disk[MUm];}q21
[q19]=fiSH;}gasp=q21[0];q12=q21[1];q13=q21[2];BAd=q21[3];Q14
=q21[4];q15=q21[5];BuG=(gasp*gasp)+(q12*q12);Q16=(BuG)+(q13*
q13);silly=sqrt(BuG);q17=sqrt(Q16);buggy=(gasp*BAd)+(q12*Q14
);Q18=buggy+(q13*q15);foobaz=(gasp!=0.0||q12!=0.0)?atan2(q12
,gasp):0.0;if(foobaz<0.0)foobaz+=D2PI;QUux=atan2(q13,silly);
if(silly>Q22){q6=((gasp*Q14)-(q12*BAd))/BuG;FrEd=((q15*BuG)-
(q13*buggy))/(Q16*silly);}if(doG>Q22){q7=Q18/(doG*q17*FULL);
doG=doG/q17;}*FobAr=foobaz;*Q3=QUux;*q4=q6/empty;*q5=FrEd/
empty;*FObaz=q7;*FOOBAR=doG;}
