/*
** Copyright (C) 2000 P.T.Wallace.
** Use for profit prohibited - enquiries to ptw@tpsoft.demon.co.uk.
*/
#include "slalib.h"
#include "slamac.h"
double slaDsepv(double foo[3],double Q0[3]){double BAR[3],q1
[3],BAZ,fObaR;slaDvxv(foo,Q0,BAR);slaDvn(BAR,q1,&BAZ);fObaR=
slaDvdv(foo,Q0);return BAZ!=0.0?atan2(BAZ,fObaR):0.0;}
