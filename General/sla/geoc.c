/*
** Copyright (C) 2000 P.T.Wallace.
** Use for profit prohibited - enquiries to ptw@tpsoft.demon.co.uk.
*/
#include "slalib.h"
#include "slamac.h"
void slaGeoc(double Q0,double Q1,double*FOO,double*Q2){
double q3,q4,q5,q6;static double q7=6378140.0;static double 
bar=1.0/298.257;double q8=(1.0-bar)*(1.0-bar);static double 
BAZ=1.49597870e11;q3=sin(Q0);q4=cos(Q0);q5=1.0/sqrt(q4*q4+q8
*q3*q3);q6=q8*q5;*FOO=(q7*q5+Q1)*q4/BAZ;*Q2=(q7*q6+Q1)*q3/
BAZ;}
