/*
** Copyright (C) 2000 P.T.Wallace.
** Use for profit prohibited - enquiries to ptw@tpsoft.demon.co.uk.
*/
#include "slalib.h"
#include "slamac.h"
void slaDc62s(double FoO[6],double*bar,double*Q0,double*q1,
double*q2,double*baz,double*q3){double q4,FoBAR,foobar,foBaz
,q5,FOOBAZ,Q6,q7,quux,q8;q4=FoO[0];FoBAR=FoO[1];foobar=FoO[2
];foBaz=FoO[3];q5=FoO[4];FOOBAZ=FoO[5];Q6=q4*q4+FoBAR*FoBAR;
if((quux=Q6+foobar*foobar)==0.0){q4=foBaz;FoBAR=q5;foobar=
FOOBAZ;Q6=q4*q4+FoBAR*FoBAR;quux=Q6+foobar*foobar;}q7=sqrt(
Q6);q8=q4*foBaz+FoBAR*q5;if(Q6!=0.0){*bar=atan2(FoBAR,q4);*
Q0=atan2(foobar,q7);*q2=(q4*q5-FoBAR*foBaz)/Q6;*baz=(FOOBAZ*
Q6-foobar*q8)/(quux*q7);}else{*bar=0.0;*Q0=(foobar!=0.0)?
atan2(foobar,q7):0.0;*q2=0.0;*baz=0.0;}*q3=((*q1=sqrt(quux))
!=0.0)?(q8+foobar*FOOBAZ)/(*q1):0.0;}
