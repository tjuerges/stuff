/*
** Copyright (C) 2000 P.T.Wallace.
** Use for profit prohibited - enquiries to ptw@tpsoft.demon.co.uk.
*/
#include "slalib.h"
#include "slamac.h"
void slaFk45z(double Q0,double FOO,double q1,double*q2,
double*Q3){double q4;int q5,bar;double Q6[3],baz[3],Q7[3],Q8
[6];static double fobar=100.0*60.0*60.0*360.0/D2PI;static 
double foobar[3]={-1.62557e-6,-0.31919e-6,-0.13843e-6};
static double FOBAZ[3]={1.245e-3,-1.580e-3,-0.659e-3};static
 double Q9[6][3]={{0.9999256782,-0.0111820611,-0.0048579477}
,{0.0111820610,0.9999374784,-0.0000271765},{0.0048579479,-
0.0000271474,0.9999881997},{-0.000551,-0.238565,0.435739},{
0.238514,-0.002667,-0.008541},{-0.435623,0.012254,0.002117}}
;slaDcs2c(Q0,FOO,Q6);q4=(q1-1950.0)/fobar;for(q5=0;q5<3;q5++
){baz[q5]=foobar[q5]+q4*FOBAZ[q5];}q4=Q6[0]*baz[0]+Q6[1]*baz
[1]+Q6[2]*baz[2];for(q5=0;q5<3;q5++){Q7[q5]=Q6[q5]-baz[q5]+
q4*Q6[q5];}for(q5=0;q5<6;q5++){q4=0.0;for(bar=0;bar<3;bar++)
{q4+=Q9[q5][bar]*Q7[bar];}Q8[q5]=q4;}q4=(slaEpj(slaEpb2d(q1)
)-2000.0)/fobar;for(q5=0;q5<3;q5++){Q8[q5]+=q4*Q8[q5+3];}
slaDcc2s(Q8,&q4,Q3);*q2=slaDranrm(q4);}
