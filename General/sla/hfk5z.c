/*
** Copyright (C) 2000 P.T.Wallace.
** Use for profit prohibited - enquiries to ptw@tpsoft.demon.co.uk.
*/
#include "slalib.h"
#include "slamac.h"
void slaHfk5z(double q0,double q1,double FoO,double*BAR,
double*q2,double*Q3,double*baZ)
#define Q4 0.484813681109535994e-5
{static double FOBAR[3]={-19.9e-3*Q4,-9.1e-3*Q4,22.9e-3*Q4},
fOOBAR[3]={-0.30e-3*Q4,0.60e-3*Q4,0.70e-3*Q4};double q5[3],
FoBAZ[3][3],Q6[3],q7,FOOBAZ[3],Q8[3][3],qUUx[3][3],Q9[6],q10
[3],Q11,Q12,q13;int freD;slaDcs2c(q0,q1,q5);slaDav2m(FOBAR,
FoBAZ);slaDmxv(FoBAZ,fOOBAR,Q6);q7=FoO-2000.0;for(freD=0;
freD<3;freD++){FOOBAZ[freD]=fOOBAR[freD]*q7;}slaDav2m(FOOBAZ
,Q8);slaDmxm(FoBAZ,Q8,qUUx);slaDimxv(qUUx,q5,Q9);slaDvxv(Q6,
q5,q10);slaDimxv(qUUx,q10,Q9+3);slaDc62s(Q9,&Q11,q2,&Q12,Q3,
baZ,&q13);*BAR=slaDranrm(Q11);}
