/*
** Copyright (C) 2000 P.T.Wallace.
** Use for profit prohibited - enquiries to ptw@tpsoft.demon.co.uk.
*/
#include "slalib.h"
#include "slamac.h"
void slaPm(double Q0,double foo,double Q1,double BAR,double 
BAZ,double Q2,double Q3,double q4,double*fobar,double*q5){
static double q6=(365.25*86400.0/149597870.0)*DAS2R;int q7;
double FOOBAR,FOBAZ[3],foobaz,QUuX[3];slaDcs2c(Q0,foo,QUuX);
FOOBAR=q6*Q2*BAZ;FOBAZ[0]=-Q1*QUuX[1]-BAR*cos(Q0)*sin(foo)+
FOOBAR*QUuX[0];FOBAZ[1]=Q1*QUuX[0]-BAR*sin(Q0)*sin(foo)+
FOOBAR*QUuX[1];FOBAZ[2]=BAR*cos(foo)+FOOBAR*QUuX[2];foobaz=
q4-Q3;for(q7=0;q7<3;q7++)QUuX[q7]=QUuX[q7]+(foobaz*FOBAZ[q7]
);slaDcc2s(QUuX,fobar,q5);*fobar=slaDranrm(*fobar);}
