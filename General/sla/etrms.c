/*
** Copyright (C) 2000 P.T.Wallace.
** Use for profit prohibited - enquiries to ptw@tpsoft.demon.co.uk.
*/
#include "slalib.h"
#include "slamac.h"
void slaEtrms(double FOO,double Q0[3]){double Q1,bar,q2,baz,
FOBAR,FOobaR;Q1=(FOO-1950.0)*1.00002135903e-2;bar=0.01673011
-(0.00004193+0.000000126*Q1)*Q1;q2=(84404.836-(46.8495+(
0.00319+0.00181*Q1)*Q1)*Q1)*DAS2R;baz=(1015489.951+(6190.67+
(1.65+0.012*Q1)*Q1)*Q1)*DAS2R;FOBAR=bar*20.49552*DAS2R;
FOobaR=cos(baz);Q0[0]=FOBAR*sin(baz);Q0[1]=-FOBAR*FOobaR*cos
(q2);Q0[2]=-FOBAR*FOobaR*sin(q2);}
