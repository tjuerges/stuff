/*
** Copyright (C) 2000 P.T.Wallace.
** Use for profit prohibited - enquiries to ptw@tpsoft.demon.co.uk.
*/
#include "slalib.h"
#include "slamac.h"
void slaDcc2s(double Q0[3],double*Q1,double*FoO){double bar,
Q2,BAZ,fobar;bar=Q0[0];Q2=Q0[1];BAZ=Q0[2];fobar=sqrt(bar*bar
+Q2*Q2);*Q1=(fobar!=0.0)?atan2(Q2,bar):0.0;*FoO=(BAZ!=0.0)?
atan2(BAZ,fobar):0.0;}
