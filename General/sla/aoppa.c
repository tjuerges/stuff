/*
** Copyright (C) 2000 P.T.Wallace.
** Use for profit prohibited - enquiries to ptw@tpsoft.demon.co.uk.
*/
#include "slalib.h"
#include "slamac.h"
void slaAoppa(double Q0,double q1,double q2,double Q3,double
 Q4,double Q5,double Q6,double foo,double BAR,double Q7,
double BAZ,double FoBar,double Q8[14])
#define q9 173.14463331
#define Q10 1.00273790935
{double q11,Q12,foobar,FOBAZ,q13,FOOBAZ,quUX,q14,q15,fReD,
DOG;q11=cos(Q3);Q12=cos(q2)*q11;foobar=sin(q2)*q11;FOBAZ=sin
(Q3);q13=Q12-Q5*FOBAZ;FOOBAZ=foobar+Q6*FOBAZ;quUX=Q5*Q12-Q6*
foobar+FOBAZ;q14=(q13!=0.0||FOOBAZ!=0.0)?atan2(FOOBAZ,q13):
0.0;q15=atan2(quUX,sqrt(q13*q13+FOOBAZ*FOOBAZ));Q8[0]=q15;Q8
[1]=sin(q15);Q8[2]=cos(q15);slaGeoc(q15,Q4,&fReD,&DOG);Q8[3]
=D2PI*fReD*Q10/q9;Q8[4]=Q4;Q8[5]=foo;Q8[6]=BAR;Q8[7]=Q7;Q8[8
]=BAZ;Q8[9]=FoBar;slaRefco(Q4,foo,BAR,Q7,BAZ,q15,FoBar,1e-10
,&Q8[10],&Q8[11]);Q8[12]=q14+slaEqeqx(Q0)+q1*Q10*DS2R;
slaAoppat(Q0,Q8);}
