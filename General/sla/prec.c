/*
** Copyright (C) 2000 P.T.Wallace.
** Use for profit prohibited - enquiries to ptw@tpsoft.demon.co.uk.
*/
#include "slalib.h"
#include "slamac.h"
void slaPrec(double q0,double q1,double q2[3][3]){double Q3,
Q4,Q5,FOO,BAR,Q6,BAZ;Q3=(q0-2000.0)/100.0;Q4=(q1-q0)/100.0;
Q5=Q4*DAS2R;FOO=2306.2181+((1.39656-(0.000139*Q3))*Q3);BAR=(
FOO+((0.30188-0.000344*Q3)+0.017998*Q4)*Q4)*Q5;Q6=(FOO+((
1.09468+0.000066*Q3)+0.018203*Q4)*Q4)*Q5;BAZ=((2004.3109+(-
0.85330-0.000217*Q3)*Q3)+((-0.42665-0.000217*Q3)-0.041833*Q4
)*Q4)*Q5;slaDeuler("\132\131\132",-BAR,BAZ,-Q6,q2);}
