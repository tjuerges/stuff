/*
** Copyright (C) 2000 P.T.Wallace.
** Use for profit prohibited - enquiries to ptw@tpsoft.demon.co.uk.
*/
#include "slalib.h"
#include "slamac.h"
void slaSupgal(double foo,double baR,double*BAZ,double*q0){
double Q1[3],FOBAR[3];static double Q2[3][3]={{-
0.735742574804,0.677261296414,0.0},{-0.074553778365,-
0.080991471307,0.993922590400},{0.673145302109,
0.731271165817,0.110081262225}};slaDcs2c(foo,baR,Q1);
slaDimxv(Q2,Q1,FOBAR);slaDcc2s(FOBAR,BAZ,q0);*BAZ=slaDranrm(
*BAZ);*q0=slaDrange(*q0);}
