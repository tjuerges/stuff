/*
** Copyright (C) 2000 P.T.Wallace.
** Use for profit prohibited - enquiries to ptw@tpsoft.demon.co.uk.
*/
#include "slalib.h"
#include "slamac.h"
void slaDjcal(int Q0,double Q1,int FOO[4],int*Q2){double q3,
Q4,Q5,Q6;long bar,baz,q7;if((Q1<=-2395520.0)||(Q1>=1.0e9)){*
Q2=-1;return;}else{q3=pow(10.0,(double)gmax(Q0,0));q3=dnint(
q3);Q4=Q1*q3;Q4=dnint(Q4);Q5=dmod(Q4,q3);if(Q5<0.0)Q5+=q3;Q6
=(Q4-Q5)/q3;bar=(long)dnint(Q6)+2400001L;baz=4L*(bar+((2L*((
4L*bar-17918L)/146097L)*3L)/4L+1L)/2L-37L);q7=10L*(((baz-
237L)%1461L)/4L)+5L;FOO[0]=(int)((baz/1461L)-4712L);FOO[1]=(
int)(((q7/306L+2L)%12L)+1L);FOO[2]=(int)((q7%306L)/10L+1L);
FOO[3]=(int)dnint(Q5);*Q2=0;}}
