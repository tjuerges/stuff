/*
** Copyright (C) 2000 P.T.Wallace.
** Use for profit prohibited - enquiries to ptw@tpsoft.demon.co.uk.
*/
#include "slalib.h"
#include "slamac.h"
void slaSubet(double Q0,double FOO,double q1,double*BAR,
double*q2){double Q3[3],Q4[3],Baz;int q5;slaEtrms(q1,Q3);
slaDcs2c(Q0,FOO,Q4);Baz=1.0+slaDvdv(Q4,Q3);for(q5=0;q5<3;q5
++){Q4[q5]=Baz*Q4[q5]-Q3[q5];}slaDcc2s(Q4,BAR,q2);*BAR=
slaDranrm(*BAR);}
