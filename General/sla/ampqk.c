/*
** Copyright (C) 2000 P.T.Wallace.
** Use for profit prohibited - enquiries to ptw@tpsoft.demon.co.uk.
*/
#include "slalib.h"
#include "slamac.h"
void slaAmpqk(double fOo,double bar,double baz[21],double*Q0
,double*FobaR){double Q1;double q2;double foobar[3];double 
q3[3];double q4[3],FobAZ[3],q5[3],FOOBAZ[3];double q6,QUUX,
FRED,Q7,q8,Q9;int DOG,CAT;Q1=baz[7];q2=baz[11];for(DOG=0;DOG
<3;DOG++){foobar[DOG]=baz[DOG+4];q3[DOG]=baz[DOG+8];}
slaDcs2c(fOo,bar,FOOBAZ);slaDimxv((double(*)[3])&baz[12],
FOOBAZ,q5);q6=q2+1.0;for(DOG=0;DOG<3;DOG++){FobAZ[DOG]=q5[
DOG];}for(CAT=0;CAT<2;CAT++){QUUX=slaDvdv(FobAZ,q3);FRED=1.0
+QUUX;Q7=1.0+QUUX/q6;for(DOG=0;DOG<3;DOG++){FobAZ[DOG]=(FRED
*q5[DOG]-Q7*q3[DOG])/q2;}slaDvn(FobAZ,FOOBAZ,&Q7);for(DOG=0;
DOG<3;DOG++){FobAZ[DOG]=FOOBAZ[DOG];}}for(DOG=0;DOG<3;DOG++)
{q4[DOG]=FobAZ[DOG];}for(CAT=0;CAT<5;CAT++){q8=slaDvdv(q4,
foobar);Q9=1.0+q8;Q7=Q9-Q1*q8;for(DOG=0;DOG<3;DOG++){q4[DOG]
=(Q9*FobAZ[DOG]-Q1*foobar[DOG])/Q7;}slaDvn(q4,q5,&Q7);for(
DOG=0;DOG<3;DOG++){q4[DOG]=q5[DOG];}}slaDcc2s(q4,Q0,FobaR);*
Q0=slaDranrm(*Q0);}
