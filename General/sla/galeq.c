/*
** Copyright (C) 2000 P.T.Wallace.
** Use for profit prohibited - enquiries to ptw@tpsoft.demon.co.uk.
*/
#include "slalib.h"
#include "slamac.h"
void slaGaleq(double q0,double q1,double*foO,double*bAR){
double Q2[3],baz[3];static double Q3[3][3]={{-0.054875539726
,-0.873437108010,-0.483834985808},{0.494109453312,-
0.444829589425,0.746982251810},{-0.867666135858,-
0.198076386122,0.455983795705}};slaDcs2c(q0,q1,Q2);slaDimxv(
Q3,Q2,baz);slaDcc2s(baz,foO,bAR);*foO=slaDranrm(*foO);*bAR=
slaDrange(*bAR);}
