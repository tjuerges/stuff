/*
** Copyright (C) 2000 P.T.Wallace.
** Use for profit prohibited - enquiries to ptw@tpsoft.demon.co.uk.
*/
#include "slalib.h"
#include "slamac.h"
double slaDbear(double q0,double FOo,double q1,double BAR){
double BAZ,q2,q3;BAZ=q1-q0;q3=sin(BAZ)*cos(BAR);q2=sin(BAR)*
cos(FOo)-cos(BAR)*sin(FOo)*cos(BAZ);return(q2!=0.0||q3!=0.0)
?atan2(q3,q2):0.0;}
