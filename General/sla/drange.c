/*
** Copyright (C) 2000 P.T.Wallace.
** Use for profit prohibited - enquiries to ptw@tpsoft.demon.co.uk.
*/
#include "slalib.h"
#include "slamac.h"
double slaDrange(double fOo){double bar;bar=dmod(fOo,D2PI);
return(fabs(bar)<DPI)?bar:bar-dsign(D2PI,fOo);}
