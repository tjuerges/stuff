/*
** Copyright (C) 2000 P.T.Wallace.
** Use for profit prohibited - enquiries to ptw@tpsoft.demon.co.uk.
*/
#include "slalib.h"
#include "slamac.h"
void slaDmat(int FoO,double*bar,double*q0,double*Q1,int*q2,
int*Q3)
#define baz 1e-20
{int Q4,fobar,Q5,FoOBAr,q6;double q7,fobaz,Q8;double*FOOBAZ,
*QuUX,*q9;*q2=0;*Q1=1.0;for(Q4=0,FOOBAZ=bar;Q4<FoO;Q4++,
FOOBAZ+=FoO){q7=fabs(FOOBAZ[Q4]);fobar=Q4;q9=FOOBAZ;if(Q4!=
FoO){for(Q5=Q4+1,QuUX=FOOBAZ+FoO;Q5<FoO;Q5++,QuUX+=FoO){
fobaz=fabs(QuUX[Q4]);if(fobaz>q7){q7=fobaz;fobar=Q5;q9=QuUX;
}}}if(q7<baz){*q2=-1;}else{if(fobar!=Q4){for(FoOBAr=0;FoOBAr
<FoO;FoOBAr++){fobaz=FOOBAZ[FoOBAr];FOOBAZ[FoOBAr]=q9[FoOBAr
];q9[FoOBAr]=fobaz;}fobaz=q0[Q4];q0[Q4]=q0[fobar];q0[fobar]=
fobaz;*Q1=-*Q1;}Q3[Q4]=fobar;*Q1*=FOOBAZ[Q4];if(fabs(*Q1)<
baz){*q2=-1;}else{FOOBAZ[Q4]=1.0/FOOBAZ[Q4];for(FoOBAr=0;
FoOBAr<FoO;FoOBAr++){if(FoOBAr!=Q4){FOOBAZ[FoOBAr]*=FOOBAZ[
Q4];}}Q8=q0[Q4]*FOOBAZ[Q4];q0[Q4]=Q8;for(Q5=0,QuUX=bar;Q5<
FoO;Q5++,QuUX+=FoO){if(Q5!=Q4){for(FoOBAr=0;FoOBAr<FoO;
FoOBAr++){if(FoOBAr!=Q4){QuUX[FoOBAr]-=QuUX[Q4]*FOOBAZ[
FoOBAr];}}q0[Q5]-=QuUX[Q4]*Q8;}}for(Q5=0,QuUX=bar;Q5<FoO;Q5
++,QuUX+=FoO){if(Q5!=Q4){QuUX[Q4]*=-FOOBAZ[Q4];}}}}}if(*q2!=
0){*Q1=0.0;}else{for(Q4=FoO;Q4-->0;){q6=Q3[Q4];if(Q4!=q6){
for(Q5=0,QuUX=bar;Q5<FoO;Q5++,QuUX+=FoO){fobaz=QuUX[Q4];QuUX
[Q4]=QuUX[q6];QuUX[q6]=fobaz;}}}}}
