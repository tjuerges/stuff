/*
** Copyright (C) 2000 P.T.Wallace.
** Use for profit prohibited - enquiries to ptw@tpsoft.demon.co.uk.
*/
#include "slalib.h"
#include "slamac.h"
static void q0(double,double,double,double,double,double,
double,double,double,double,double,double,double*,double*,
double*);static void FOO(double,double,double,double,double,
double*,double*);void slaRefro(double q1,double BAR,double 
Q2,double Q3,double q4,double q5,double q6,double q7,double 
baz,double*FOBAR)
#define foobar 16384
{static double fobaz=1.623156204;static double Q8=8314.32;
static double FOObAz=28.9644;static double q9=18.0152;static
 double quux=6378120.0;static double q10=18.36;static double
 q11=11000.0;static double FREd=80000.0;double dog;double 
Q12;double q13;double q14;double Q15;double Q16,caT,q17,FISH
,GASP,bad;double Q18;double q19;double Q20;double bug;int 
SILLY,Q21,Q22,buggy,mum,dad;double q23,q24,q25,DISK,EMPty,
q26,FuLl,q27,FAST,SMALL,big,OK,HeLLo,bYE,q28,MaGIC,oBscuRe,
speed,iNdEX,bAR_FOO,Bar_BAr,Q29,Q30,q31,q32,bar_baz,Q33,q34,
bar_fOBAR,Q35,Q36,BAR_FOOBAR,Q37,q38,bAr_FOBaZ,q39,Q40,q41,
baR_fOoBaz,q42,BaR_qUux,Q43,BAR_FRED,BaR_DoG,Q44,BAr_CAt,
bar_fish,Q45,q46,Q47,Q48;
#define q49(Q50,q51) ((q51)/(Q50+q51));
q23=slaDrange(q1);q24=fabs(q23);q24=gmin(q24,fobaz);q25=gmax
(BAR,-1000.0);q25=gmin(q25,FREd);Q12=gmax(Q2,100.0);Q12=gmin
(Q12,500.0);DISK=gmax(Q3,0.0);DISK=gmin(DISK,10000.0);EMPty=
gmax(q4,0.0);EMPty=gmin(EMPty,1.0);q26=gmax(q5,0.1);q13=fabs
(q7);q13=gmax(q13,0.001);q13=gmin(q13,0.01);q28=fabs(baz);
q28=gmax(q28,1e-12);FuLl=gmin(q28,0.1)/2.0;dad=(q26<=100.0);
q27=q26*q26;FAST=9.784*(1.0-0.0026*cos(2.0*q6)-2.8e-7*q25);
SMALL=(dad)?((287.604+1.6288/q27+0.0136/(q27*q27))*273.15/
1013.25)*1e-6:77.6890e-6;bug=FAST*FOObAz/Q8;big=bug/q13;q14=
big-2.0;Q15=q10-2.0;OK=Q12-273.15;HeLLo=pow(10.0,(0.7859+
0.03477*OK)/(1.0+0.00412*OK))*(1.0+DISK*(4.5e-6+6e-10*OK*OK)
);bYE=(DISK>0.0)?EMPty*HeLLo/(1.0-(1.0-EMPty)*HeLLo/DISK):
0.0;q28=bYE*(1.0-q9/FOObAz)*big/(q10-big);Q16=SMALL*(DISK+
q28)/Q12;caT=(SMALL*q28+(dad?11.2684e-6:6.3938e-6)*bYE)/Q12;
q17=(big-1.0)*q13*Q16/Q12;FISH=(q10-1.0)*q13*caT/Q12;GASP=
dad?0.0:375463e-6*bYE/Q12;bad=GASP*Q15*q13/(Q12*Q12);dog=
quux+q25;q0(dog,Q12,q13,q14,Q15,Q16,caT,q17,FISH,GASP,bad,
dog,&MaGIC,&oBscuRe,&speed);iNdEX=oBscuRe*dog*sin(q24);
bAR_FOO=q49(oBscuRe,speed);Q18=quux+gmax(q11,q25);q0(dog,Q12
,q13,q14,Q15,Q16,caT,q17,FISH,GASP,bad,Q18,&q19,&Q20,&
Bar_BAr);Q29=asin(iNdEX/(Q18*Q20));Q30=q49(Q20,Bar_BAr);FOO(
Q18,q19,Q20,bug,Q18,&q31,&q32);bar_baz=asin(iNdEX/(Q18*q31))
;Q33=q49(q31,q32);q34=quux+FREd;FOO(Q18,q19,Q20,bug,q34,&
bar_fOBAR,&Q35);Q36=asin(iNdEX/(q34*bar_fOBAR));BAR_FOOBAR=
q49(bar_fOBAR,Q35);for(Q21=1;Q21<=2;Q21++){Q37=1.0;SILLY=8;
if(Q21==1){q38=q24;bAr_FOBaZ=Q29-q38;q39=bAR_FOO;Q40=Q30;}
else{q38=bar_baz;bAr_FOBaZ=Q36-q38;q39=Q33;Q40=BAR_FOOBAR;}
q41=0.0;baR_fOoBaz=0.0;Q22=1;for(;;){q42=bAr_FOBaZ/(double)
SILLY;BaR_qUux=(Q21==1)?dog:Q18;for(buggy=1;buggy<SILLY;
buggy+=Q22){Q43=sin(q38+q42*(double)buggy);if(Q43>1e-20){q28
=iNdEX/Q43;BAR_FRED=BaR_qUux;mum=0;do{if(Q21==1){q0(dog,Q12,
q13,q14,Q15,Q16,caT,q17,FISH,GASP,bad,BAR_FRED,&Q44,&BAr_CAt
,&bar_fish);}else{FOO(Q18,q19,Q20,bug,BAR_FRED,&BAr_CAt,&
bar_fish);}BaR_DoG=(BAR_FRED*BAr_CAt-q28)/(BAr_CAt+bar_fish)
;BAR_FRED-=BaR_DoG;}while(fabs(BaR_DoG)>1.0&&mum++<=4);
BaR_qUux=BAR_FRED;}if(Q21==1){q0(dog,Q12,q13,q14,Q15,Q16,caT
,q17,FISH,GASP,bad,BaR_qUux,&Q45,&BAr_CAt,&bar_fish);}else{
FOO(Q18,q19,Q20,bug,BaR_qUux,&BAr_CAt,&bar_fish);}q46=q49(
BAr_CAt,bar_fish);if(Q22==1&&buggy%2==0){baR_fOoBaz+=q46;}
else{q41+=q46;}}Q47=q42*(q39+4.0*q41+2.0*baR_fOoBaz+Q40)/3.0
;if(Q21==1)Q48=Q47;if(fabs(Q47-Q37)<=FuLl||SILLY>=foobar)
break;Q37=Q47;SILLY+=SILLY;baR_fOoBaz+=q41;q41=0.0;Q22=2;}}*
FOBAR=Q48+Q47;if(q23<0.0)*FOBAR=-(*FOBAR);}static void q0(
double dog,double Q12,double q13,double q14,double Q15,
double Q16,double caT,double q17,double FISH,double GASP,
double bad,double BaR_qUux,double*Q45,double*BAr_CAt,double*
bar_fish){double q28,Q52,Q53,BAR_GASp;q28=Q12-q13*(BaR_qUux-
dog);q28=gmin(q28,320.0);q28=gmax(q28,100.0);Q52=q28/Q12;Q53
=pow(Q52,q14);BAR_GASp=pow(Q52,Q15);*Q45=q28;*BAr_CAt=1.0+(
Q16*Q53-(caT-GASP/q28)*BAR_GASp)*Q52;*bar_fish=BaR_qUux*(-
q17*Q53+(FISH-bad/Q52)*BAR_GASp);}static void FOO(double Q18
,double q19,double Q20,double bug,double BaR_qUux,double*
BAr_CAt,double*bar_fish){double BAR_BAD,q28;BAR_BAD=bug/q19;
q28=(Q20-1.0)*exp(-BAR_BAD*(BaR_qUux-Q18));*BAr_CAt=1.0+q28;
*bar_fish=-BaR_qUux*BAR_BAD*q28;}
