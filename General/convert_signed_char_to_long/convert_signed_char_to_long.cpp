/**
 * "@(#) $Id$"
 */

#include <iostream>
#include <string>
#include <vector>
#include <sstream>

#include "convert_signed_char_to_long.h"


int main(int argc, char* argv[])
{
    std::cout << "Compiled on "
        << COMPILATION_DATE
        << ", version "
        << VERSION
        << "."
        << std::endl;

	unsigned char a(255);
	char b(255);
	long c(a);
	long d(b);

	std::cout << "a = " << a << std::endl
		<< "b = " << b << std::endl
		<< "c = " << c << std::endl
		<< "d = " << d << std::endl;
	c = static_cast< long >(a);
	d = static_cast< long >(b);
	std::cout << "static_cast< long >: c = " << c << std::endl
		<< "static_cast< long >: d = " << d << std::endl;
	
    return 0;
};
