#
# $Id$
#

TEMPLATE = app
TARGET = Monte_Carlo_PI_estimation

LANGUAGE = C++

#DEFINES +=

CONFIG += rtti stl exceptions debug warn_on
CONFIG -= qt

QMAKE_CXXFLAGS_RELEASE += -g $$system(cflags.sh QMAKE_RELEASE_CXX)
QMAKE_CXXFLAGS_DEBUG += -g $$system(cflags.sh QMAKE_DEBUG_CXX)

SOURCES += Monte_Carlo_PI_estimation.cpp

LIBRARIES += m
