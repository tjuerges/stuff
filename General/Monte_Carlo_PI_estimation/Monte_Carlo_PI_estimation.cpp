/**
 * $Id$
 */


#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <limits>
#include <stdint.h>
#include <cmath>
#include <cstdlib>


//#define DEBUG
#define USE_DEV


class Random
{
    public:
    Random(double);
    ~Random();

    void getRandom(double&);


    private:
    Random(const Random&);
    Random& operator=(const Random&);

    std::ifstream input;
    const double radius;
};


Random::Random(double _radius):
    input(),
    radius(_radius)
{
    input.open("/dev/urandom", std::ios::binary | std::ios::in);
    if(!input)
    {
        throw(std::string("No random file available.\n"));
    };
}

Random::~Random()
{
    input.close();
}

void Random::getRandom(double& number)
{
    #ifdef USE_DEV
    input.read(reinterpret_cast< char*>(&number), sizeof(double));
    number = std::fmod(number, radius);
    #else
    number = ::random();
    number /= RAND_MAX;
    number *= radius;
    #endif
}


int main(int argc, char* argv[])
{
    if(argc != 3)
    {
        std::cout << "Parameter 1 = radius, Parameter 2 = iterations\n\n";
        return -1;
    }

    const double radius(static_cast< double >(std::atof(argv[1])));
    const std::size_t iterations(std::atoi(argv[2]));

    try
    {
        Random randomGenerator(radius);

        std::size_t counterInsideCircle(0UL);
        double x(0.0), y(0.0);
        double sqrtTemp(0.0);

        for(std::size_t i(0); i < iterations; ++i)
        {
            randomGenerator.getRandom(x);
            randomGenerator.getRandom(y);
            sqrtTemp = std::sqrt((x * x) + (y * y));

            #ifdef DEBUG
            std::cout << "(x, y) = ("
                << x
                << ", "
                << y
                << "), "
                << "length = "
                << sqrtTemp
                << "\n";
            #endif

            if(sqrtTemp < radius)
            {
                ++counterInsideCircle;
            }
        }

        const double insideCircle(static_cast< double >(counterInsideCircle));

        std::cout.precision(20);
        std::cout << "\n"
            << "\nRadius = "
            << radius
            << "\nAll hits = "
            << iterations
            << "\nHits inside circle = "
            << insideCircle
            << "\nHits inside square = "
            << iterations - counterInsideCircle
            << "\nPi estimation = "
            << 4.0 * insideCircle / static_cast< double >(iterations)
            << ".\n";
    }
    catch(const std::string& ex)
    {
        std::cout << ex;
        return -1;
    }

    return 0;
};
