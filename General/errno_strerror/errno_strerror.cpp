/**
 * $Id$
 */


#include <iostream>
#include <cerrno>
#include <cstring>


void printError(int error)
{
    std::cout << "Error = "
        << error
        << ", \""
        << std::strerror(error)
        << "\"\n";
}


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    printError(EBADF);
    printError(ENOTSOCK);
    printError(EFAULT);
    printError(EMSGSIZE);
    printError(EAGAIN);
    printError(ENOBUFS);
    printError(EINTR);
    printError(ENOMEM);
    printError(EINVAL);
    printError(EPIPE);

    return 0;
};
