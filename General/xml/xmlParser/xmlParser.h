#ifndef xml_h
#define xml_h
/**
 * $Id$
 */
#include <string>
#include <map>
#include <vector>

/**
 * Internal class for XML tree handling.
 */
class CXMLTreeNode
{
    public:
    /**
     * Constrcutor.
     * @param pParent parent node.
     */
    CXMLTreeNode( CXMLTreeNode *pParent );

    /**
     * Destructor.
     */
    ~CXMLTreeNode();

    /**
     * Get node attributes names (also subnodes names are added).
     * @param names out parameter.
     */
    void getAttributeNames(std::string &names);

    /// XML node name.
    std::string m_name;

    /// Node parent in the tree.
    CXMLTreeNode* m_parent;

    typedef std::map<std::string, CXMLTreeNode*> MapStringToNode;
    /// Children nodes std::map.
    MapStringToNode	m_subNodesMap;

    typedef std::map<std::string, std::string> MapStringToString;
    /// Node XML fields (attributes) std::map.
    MapStringToString m_fieldMap;

    private:
    CXMLTreeNode(const CXMLTreeNode&);
    CXMLTreeNode& operator=(const CXMLTreeNode&);
};

/**
 * Internal class for XML tree handling.
 */
class DAOProxy
{
    public:

    /**
    * Constrcutor (local mode).
    * @param xml XML represendation (to be parsed).
    */
    DAOProxy(const char * nodeName, const char* xml);

    /**
    * Destructor.
    */
    virtual ~DAOProxy();


    /**
    * Internal helper method.
    * @param name attribute name.
    * @param value out parameter.
    * @throw cdbErrType::CDBFieldDoesNotExistExImpl
    */
    void get_field(const char* name, std::string &value);

    typedef std::vector<std::string> VectorString;

    /**
    * Split std::string str into several substrings, which are separated with
    * commas. If quotes are used, then substrings are considered to be 
    * enclosed in them. Quotes can be escaped so that they can be treated
    * verbatim.
    */
    static bool split(const std::string& str, VectorString& array);

    /// Node name.
    std::string m_nodeName;

    /// Error message.
    std::string m_errorMessage;


    // expat (XML parser) internal operations
    protected:
    void	Start(const char *el, const char **attr);
    void	End(const char *el);

    static void start(void *data, const char *el, const char **attr);
    static void end(void *data, const char *el);

    // destruction status flag.
    bool m_destroyed;

    // temporary variables for array handling
    bool m_inArray;
    std::string m_arrayName;
    std::string m_arrayContent;

    // XML tree handling
    CXMLTreeNode* m_rootNode;
    CXMLTreeNode* m_currNode;

    private:
    DAOProxy(const DAOProxy&);
    DAOProxy& operator=(const DAOProxy&);
};
#endif
