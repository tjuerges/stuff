#
# $Id$
#

TEMPLATE = app
TARGET = xmlParser

LANGUAGE = C++

#DEFINES +=

#CONFIG += qt rtti stl exceptions debug warn_on 
#CONFIG += qt rtti stl exceptions release warn_on 
CONFIG += rtti stl exceptions debug warn_on
CONFIG -= qt

INCLUDEPATH += /export/home/delphinus/tjuerges/workspace/alma/ACS-current/TAO/ACE_wrappers/build/linux \
/export/home/delphinus/tjuerges/workspace/alma/ACS-current/TAO/ACE_wrappers/build/linux/ace \
/export/home/delphinus/tjuerges/workspace/alma/ACS-current/TAO/ACE_wrappers/build/linux/TAO \
/export/home/delphinus/tjuerges/workspace/alma/ACS-current/TAO/ACE_wrappers/build/linux/TAO/orbsvcs \
/export/home/delphinus/tjuerges/workspace/alma/ACS-current/TAO/ACE_wrappers/build/linux/TAO/orbsvcs/orbsvcs \
/export/home/delphinus/tjuerges/workspace/alma/ACS-current/ACSSW/include \
/export/home/delphinus/tjuerges/workspace/kernel/rtai-3.7.1-installed/include \
/export/home/delphinus/tjuerges/workspace/introot/include

LIBPATH += /export/home/delphinus/tjuerges/workspace/alma/ACS-current/TAO/ACE_wrappers/build/linux/ace \
/export/home/delphinus/tjuerges/workspace/alma/ACS-current/TAO/ACE_wrappers/build/linux/TAO/tao \
/export/home/delphinus/tjuerges/workspace/alma/ACS-current/ACSSW/lib \
/export/home/delphinus/tjuerges/workspace/kernel/rtai-3.7.1-installed/lib \
/export/home/delphinus/tjuerges/workspace/introot/lib

LIBS += -lACE \
    -lloki \
    -lacsutil \
    -lacscommonStubs \
    -lbaselogging \
    -llogging \
    -lACSErrTypeCommon \
    -lcdbErrType \
    -lexpat

HEADERS += xmlParser.h

SOURCES += xmlParser.cpp
