#include <iostream>
#include <string>
#include <vector>
#include <sstream>

using namespace std;

int main(int argc, char* argv[])
{

    int to(10), count(0);

    while(++count < to )
    {
	std::cout << "Waiting... " << count << std::endl;;
    }

    /*
     * on timeout return with error
     */
    if(count == to)
    {
	std::cout << "1";
    }
    else
    {
	std::cout << "0";
    }
    std::cout << std::endl;

	return 0;
};

