#include <iostream>

int main(int, char**)
{
	int a(10);
	for(int b(a-1); b >= 0; --b)
	{
		std::cout << "Running, b = " << b << std::endl;
	}

	return 0;
}
