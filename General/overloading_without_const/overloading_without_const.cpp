/**
 * $Id$
 */


#include "overloading_without_const.h"


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{

    parent* a(new parent);
    a->printItNonConst();
    a->printItConst();
    delete a;

    a = new kid;
    a->printItNonConst();
    a->printItConst();
    delete a;

    a = new kidInv;
    a->printItNonConst();
    a->printItConst();
    delete a;
    return 0;
};
