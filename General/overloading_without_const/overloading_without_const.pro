#
# $Id$
#

TEMPLATE = app
TARGET = overloading_without_const

LANGUAGE = C++

#DEFINES +=

#CONFIG += qt rtti stl exceptions debug warn_on 
#CONFIG += qt rtti stl exceptions release warn_on 
CONFIG += rtti stl exceptions debug warn_on
CONFIG -= qt

#QMAKE_LFLAGS += -g

QMAKE_CXXFLAGS_RELEASE += -g $$system(cflags.sh QMAKE_RELEASE_CXX) \
	-DCOMPILATION_DATE=\\\"`date --iso-8601=seconds`\\\" \
    -DVERSION=\\\"`./compilation_run`\\\"
QMAKE_CXXFLAGS_DEBUG += -g $$system(cflags.sh QMAKE_DEBUG_CXX) \
	-DCOMPILATION_DATE=\\\"`date --iso-8601=seconds`\\\" \
    -DVERSION=\\\"`./compilation_run`\\\"

QMAKE_CFLAGS_RELEASE += -g $$system(cflags.sh QMAKE_RELEASE_C) \
	-DCOMPILATION_DATE=\\\"`date --iso-8601=seconds`\\\" \
    -DVERSION=\\\"`./compilation_run`\\\"
QMAKE_CFLAGS_DEBUG += -g $$system(cflags.sh QMAKE_DEBUG_C) \
	-DCOMPILATION_DATE=\\\"`date --iso-8601=seconds`\\\" \
    -DVERSION=\\\"`./compilation_run`\\\"

#INCLUDEPATH += /export/home/delphinus/tjuerges/workspace/alma/ACS-current/TAO/ACE_wrappers/build/linux \
#    /export/home/delphinus/tjuerges/workspace/alma/ACS-current/TAO/ACE_wrappers/build/linux/ace \
#    /export/home/delphinus/tjuerges/workspace/alma/ACS-current/TAO/ACE_wrappers/build/linux/TAO \
#    /export/home/delphinus/tjuerges/workspace/alma/ACS-current/TAO/ACE_wrappers/build/linux/TAO/orbsvcs \
#    /export/home/delphinus/tjuerges/workspace/alma/ACS-current/TAO/ACE_wrappers/build/linux/TAO/orbsvcs/orbsvcs \
#    /export/home/delphinus/tjuerges/workspace/alma/ACS-current/ACSSW/include \
#    /export/home/delphinus/tjuerges/workspace/kernel/rtai-3.7.1-installed/include \
#    /export/home/delphinus/tjuerges/workspace/introot/include

#LIBPATH += /export/home/delphinus/tjuerges/workspace/alma/ACS-current/TAO/ACE_wrappers/build/linux/ace \
#    /export/home/delphinus/tjuerges/workspace/alma/ACS-current/TAO/ACE_wrappers/build/linux/TAO/tao \
#    /export/home/delphinus/tjuerges/workspace/alma/ACS-current/ACSSW/lib \
#    /export/home/delphinus/tjuerges/workspace/kernel/rtai-3.7.1-installed/lib \
#    /export/home/delphinus/tjuerges/workspace/introot/lib

#LIBS += -lACE \
#    -lloki \
#    -lacsutil \
#    -lacscommonStubs \
#    -lbaselogging \
#    -llogging

#TEMPLATE = subdirs
#SUBDIRS += foo bar

#FORMS += overloading_without_const.ui

HEADERS += overloading_without_const.h

SOURCES += overloading_without_const.cpp

