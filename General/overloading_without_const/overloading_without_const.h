#ifndef overloading_without_const_h
#define overloading_without_const_h
/**
 * $Id$
 */


#include <iostream>


class parent
{
    public:
    virtual ~parent()
    {
        std::cout << "parent::~parent\n";
    };

    virtual void printItNonConst()
    {
        std::cout << "parent::printItNonConst\n";
    };

    virtual void printItConst() const
    {
        std::cout << "parent::printItConst\n";
    };
};

class kid: public parent
{
    public:
    virtual ~kid()
    {
        std::cout << "kid::~kid\n";
    };

    virtual void printItNonConst()
    {
        std::cout << "kid::printItNonConst\n";
    };

    virtual void printItConst() const
    {
        std::cout << "kid::printItConst\n";
    };
};

class kidInv: public parent
{
    public:
    virtual ~kidInv()
    {
        std::cout << "kidInv::~kidInv\n";
    };

    virtual void printItNonConst() const
    {
        std::cout << "kidInv::printItNonConst\n";
    };

    virtual void printItConst()
    {
        std::cout << "kidInv::printItConst\n";
    };
};
#endif
