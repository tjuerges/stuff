/**
 * "@(#) $Id$"
 */

#include <iostream>
#include <string>
#include <netdb.h>
#include <arpa/inet.h>
#include <cerrno>

int main(int argc, char* argv[])
{
    std::cout << "Compiled on "
        << COMPILATION_DATE
        << ", version "
        << VERSION
        << "."
        << std::endl;

	if(argc != 2)
	{
		std::cout << "Provide a hostname!" << std::endl;
		return 0;
	}
	else
	{
		std::cout << "Resolving " << argv[1] << std::endl;
	}

	struct hostent* host = gethostbyname(argv[1]);
	if(host == 0)
	{
		herror("No host entries found.");
		return errno;
	}

	in_addr_t foo = (*(reinterpret_cast< in_addr_t* >(host->h_addr_list[0])));
	struct in_addr a;
	a.s_addr = foo;
	char* addr = inet_ntoa(a);
	std::cout << "hostent.h_addr_list[0] = "
		<< std::hex << ntohl(foo) << std::dec
		<< ", IP = "
		<< addr
		<< std::endl << std::endl;

    return 0;
};
