#
# $Id$
#

TEMPLATE = app
TARGET = tan

LANGUAGE = C

#DEFINES +=

CONFIG -= qt

LIBS = -Wl,--as-needed $${LIBS}

QMAKE_LINK = gcc

QMAKE_CFLAGS_RELEASE += $$system(cflags.sh QMAKE_RELEASE_C)
QMAKE_CFLAGS_DEBUG += $$system(cflags.sh QMAKE_DEBUG_C)


SOURCES += tan.c
