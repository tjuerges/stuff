#include <stdio.h>
#include <math.h>

int main(int a, char*b[])
{
    double foo = M_PI;
    printf("tan(M_PI) = %f.\n", tan(foo));
    foo = tan(90.0);
    printf("tan(90.0) = %f.\n", tan(90.0 / 180.0 * M_PI));

    return 0;
}

