/**
 * $Id$
 */


#include <iostream>
#include <sstream>
#include <ctime>
#include <cerrno>
#include <cstring>


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    struct timespec foo = {2, 0};

    nanosleep(&foo, 0);

    std::ostringstream out;
    out << "Error = "
        << errno
        << " \""
        << strerror(errno)
        << "\"\n";
    std::cout << out.str();

    return 0;
};
