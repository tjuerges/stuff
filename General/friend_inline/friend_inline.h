#ifndef friend_inline_h
#define friend_inline_h
/**
 * $Id$
 */


#include <iostream>


inline void bar1_print()
{
    std::cout << "Yo!  I am bar1_print.\n";
};


inline void bar2_print()
{
    std::cout << "Yo!  I am bar2_print.\n";
};


namespace foo
{
/*
    inline void bar1_print()
    {
        std::cout << "Yo!  I am foo::bar1_print.\n";
    };


    inline void bar2_print()
    {
        std::cout << "Yo!  I am foo::bar2_print.\n";
    };
*/

    class bar1
    {
        friend inline void bar1_print()
        {
            std::cout << "Yo!  I am foo::bar1::bar1_print.\n";
        };
    };

    class bar2
    {
        friend inline void bar2_print()
        {
            std::cout << "Yo!  I am foo::bar2::bar2_print.\n";
        };
    };
};
#endif
