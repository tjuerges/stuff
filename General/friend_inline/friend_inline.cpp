/**
 * $Id$
 */


#include "friend_inline.h"


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    std::cout << "Compiled on "
        << COMPILATION_DATE
        << ", version "
        << VERSION
        << "."
        << std::endl;

    foo::bar1 b1;
    foo::bar2 b2;

/*
    b1.bar1_print();
    b2.bar2_print();
*/
    foo::bar1_print();
    foo::bar2_print();

    bar1_print();
    bar2_print();

    ::bar1_print();
    ::bar2_print();

    return 0;
};
