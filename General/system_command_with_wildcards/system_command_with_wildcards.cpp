/**
 * "@(#) $Id$"
 */

#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <cstdlib>

#include "system_command_with_wildcards.h"


int main(int argc, char* argv[])
{
	std::cout << "Compiled on "
		<< COMPILATION_DATE
		<< ", version "
		<< VERSION
		<< "."
		<< std::endl;

	system("ls -al /bin/ch*");
	return 0;
};
