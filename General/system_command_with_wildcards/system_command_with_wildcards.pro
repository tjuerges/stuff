# $Id$

TEMPLATE = app
TARGET = system_command_with_wildcards

LANGUAGE = C++

CONFIG += rtti stl warn_on release

QMAKE_CXXFLAGS_RELEASE = -g \
    -O2 \
    -DCOMPILATION_DATE=\"`date --iso-8601=seconds`\" \
    -DVERSION=\"`./compilation_run`\"
    -march=pentium4m \
    -mfpmath=sse,387 \
    -mmmx \
    -m3dnow \
    -msse \
    -msse2

SOURCES += system_command_with_wildcards.cpp

