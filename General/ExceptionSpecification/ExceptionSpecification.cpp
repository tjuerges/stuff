/**
 * $Id$
 */

#include <iostream>
#include <exception>
#include <stdexcept>


class MyException: public std::runtime_error
{
    public:
    MyException(): std::runtime_error("MyException")
    {
    };
};


class Thrower
{
    public:
    /**
     * Simply throw a MyException.
     */
    static void bar() throw(MyException)
    {
        std::cout << "Thrower::bar() start.\n";
        throw(MyException());
        std::cout << "Thrower::bar() exit.\n";
    };
};


class Caller
{
    public:
    /**
     * This method promises not to throw any other exception than std::logic_error.
     * It calls foo2::bar() which throws MyException. Thus the caller cannot
     * catch the MyException because this method does not specify it.
     */
    void bar() throw(std::logic_error)
    {
        std::cout << "Calling Thrower::bar().\n";
        Thrower::bar();
        std::cout << "Thrower::bar() was called.\n";
    };
};


class BadThrower
{
    public:
    static void bar() throw()
    {
        std::cout << "BadThrower::bar() start.\n";
        throw(MyException());
        std::cout << "BadThrower::bar() exit.\n";
    };
};


/**
 * Example for a method which is wrapped in a try/catch block.
 */
void willy() throw(std::logic_error)
try
{
    std::cout << "Throwing an exception...\n";
    throw(std::logic_error("void willy()"));
}
catch(...)
{
    std::cout << "Caught an exception!\n";
}


int main(int c __attribute__((unused)), char* v[] __attribute__((unused)))
{
    willy();

    Caller instance;

    try
    {
        std::cout << "Calling BadThrower::bar()\n";
        BadThrower::bar();
        std::cout << "BadThrower::bar() was called.\n";

        std::cout << "Calling Caller::bar()...\n";
        instance.bar();
        std::cout << "Caller::bar() was called.\n";
    }
    catch(const std::logic_error& ex)
    {
        std::cout << "Caught a std::logic_error exception.\n";
    }
    catch(const MyException& ex)
    {
        std::cout << "Caught a MyException.\n";
    }
    catch(...)
    {
        std::cout << "Caught an unexpected exception.\n";
    }

    return 0;
}
