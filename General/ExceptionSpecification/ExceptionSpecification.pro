# $Id$

TEMPLATE = app
TARGET = ExceptionSpecification

LANGUAGE = C++

CONFIG += rtti stl warn_on release debug
CONFIG -= qt

QMAKE_CXXFLAGS_DEBUG = -O2

QMAKE_LFLAGS = -g

SOURCES += ExceptionSpecification.cpp
