/**
 * $Id$
 */


#include <iostream>
#include <cstring>
#include <sys/utsname.h>
#include <sys/sysinfo.h>


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    struct utsname machineInfo;
    std::memset(&machineInfo, 0, sizeof(struct utsname));
    if(uname(&machineInfo) == 0)
    {
        std::cout << "Implementation name = "
            << machineInfo.sysname
            << "\nNode name = "
            << machineInfo.nodename
            << "\nRelease = "
            << machineInfo.release
            << "\nVersion = "
            << machineInfo.version
            << "\nHardware type = "
            << machineInfo.machine
#if _UTSNAME_DOMAIN_LENGTH - 0
            << "\nDomain name = "
#ifdef __USE_GNU
            << machineInfo.domainname
#else
            << machineInfo.__domainname
#endif
#endif
            << "\n\n";
    }

    std::cout << "Number of configured processors = "
        << get_nprocs_conf()
        << "\nNumber of available processors = "
        << get_nprocs()
        << "\n\n";
    
    return 0;
};
