#include <stdio.h>
#include <stdint.h>

#define __rdtsc(var) \
do \
{ \
    __asm__ volatile( \
        "rdtsc\n\t" \
        "movl %%eax,0(%0)\n\t" \
        "movl %%edx,4(%0)" \
        : : "r" (var) : "eax", "edx" \
    ); \
} \
while(0)


int main(int a __attribute__((unused)), char* b[] __attribute__((unused)))
{
	uint64_t tsc_start, tsc_end, junk, difference, average;
	uint32_t outer, inner;
	const uint64_t loops = 1;
	const uint64_t all = loops * loops;

	average = 0ULL;
	for(outer = 0; outer < loops; ++outer)
	{
		__rdtsc(&tsc_start);
		for(inner = 0; inner < loops; ++inner)
		{
			__rdtsc(&junk);
		}
		__rdtsc(&tsc_end);

        difference = tsc_end - tsc_start;
		average += (difference / all);
	}

	printf("tsc_start = %llu, tsc_end = %llu, difference = %llu, "
	    "average = %llu\n",
	    tsc_start, tsc_end, difference, average);

	return 0;
}
