	.file	"exampleIF-4.c"
	.section	.rodata.str1.4,"aMS",@progbits,1
	.align 4
.LC0:
	.string	"Current value of x: %d.  Current value of y %d\n"
	.text
	.p2align 4,,15
.globl main
	.type	main, @function
main:
	pushl	%ebp
	movl	$200, %edx
	movl	%esp, %ebp
	subl	$24, %esp
	movl	$100, %eax
	andl	$-16, %esp
	subl	$16, %esp
	movl	%edx, 8(%esp)
	movl	%eax, 4(%esp)
	movl	$.LC0, (%esp)
	call	printf
	leave
	movl	$199, %eax
	ret
	.size	main, .-main
	.section	.note.GNU-stack,"",@progbits
	.ident	"GCC: (GNU) 3.4.6 20060404 (Red Hat 3.4.6-3)"
