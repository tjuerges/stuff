	.file	"exampleIF-4.c"
	.section	.rodata.str1.4,"aMS",@progbits,1
	.align 4
.LC0:
	.string	"Current value of x: %d.  Current value of y %d\n"
	.text
.globl main
	.type	main, @function
main:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$24, %esp
	andl	$-16, %esp
	subl	$16, %esp
	movl	$200, 8(%esp)
	movl	$100, 4(%esp)
	movl	$.LC0, (%esp)
	call	printf
	movl	$199, %eax
	leave
	ret
	.size	main, .-main
	.section	.note.GNU-stack,"",@progbits
	.ident	"GCC: (GNU) 3.4.6 20060404 (Red Hat 3.4.6-3)"
