#
# $Id$
#


TEMPLATE = app
TARGET = lazy_malloc

CONFIG -= qt
QMAKE_CFLAGS_RELEASE += -std=c17
QMAKE_CFLAGS_DEBUG += -std=c17

LANGUAGE = C

SOURCES += lazy_malloc.c
