#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

int main(int a __attribute__((unused)), char* v[] __attribute__((unused)))
{
    const size_t oneGB = (size_t)1024 * (size_t)1024 * (size_t)1024;
    const size_t factor = 1024;

    printf("Trying to allocate %luGB memory...\n", (factor * oneGB));

    void* mem = NULL;
    mem = malloc(factor * oneGB);
    if(mem == NULL)
    {
        printf("Failed to allocate %luGB of mem. %s\n", (factor * oneGB), strerror(errno));
        return errno;
    }
    else
    {
        printf("Could allocate %luGB of memory at address 0x%0llx!\n", (factor * oneGB), (uint64_t)mem);
        free(mem);
    }

    return 0;
}

