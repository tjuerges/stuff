#include <cerrno>
#include <iostream>


int main(int argc, char* argv[])
{
    typedef size_t SizeType;

    int factor{1}; // Number of gigabytes to try (default is 1)

    if(argc == 2)
    {
    	factor = ::atoi(argv[1]);
    }

    std::cout << "Factor: "
        << factor
        << "\n";

    const SizeType oneGB{static_cast< SizeType >(1024) * static_cast< SizeType >(1024) * static_cast< SizeType >(1024)};
    const SizeType value{static_cast< SizeType >(factor) * oneGB};

    std::cout << "Trying to allocate memory:" << value << " GB\n";

    void* mem{nullptr};
    mem = malloc(value);
    if((mem == 0)
    || (errno != 0))
    {
        std::cout << "Failed to allocate memory: "
            << value
            << " GB\n"
            << "Error = "
            << std::strerror(errno);
        return errno;
    }
    else
    {
        std::cout << "Could allocate memory: "
            << value
            << " GB\n";
        free(mem);
    }

    return 0;
}
