#
# $Id$
#


TEMPLATE = app
TARGET = lazy_malloc

CONFIG -= qt
QMAKE_CXXFLAGS_RELEASE += -std=c++17
QMAKE_CXXFLAGS_DEBUG += -std=c++17

SOURCES += lazy_malloc.cpp
