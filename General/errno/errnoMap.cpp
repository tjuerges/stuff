#include <iostream>
#include <map>
#include <string>
#include <cstring>

int main(int a __attribute__((unused)), char* b[] __attribute__((unused)))
{
    typedef std::map< const std::string, std::pair< int, std::string > >
        MapType;
    MapType errnoMap;


    #include "errnoMap.ii"

    for(auto i(errnoMap.begin()); i!= errnoMap.end(); ++i)
    {
        std::cout << (*i).first
            << " = "
            << (*i).second.first
            << " = "
            << (*i).second.second
            << "\n";
    }

    return 0;
}
