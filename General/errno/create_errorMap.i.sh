#! /usr/bin/env bash
#
# $Id: create_errnoMap.i.sh 282 2010-12-08 17:32:38Z thomas $
#

function createMap()
{
    sort=${1}
    key=${2}
    ${CPP} ${CPP_FLAGS} ${ERRNO} | egrep '^#define[[:space:]]+E' | sort --numeric-sort --key=${key} > errno.${sort}
    # Feed awk with the just created list in order to create an include file for
    # C++.  The include file will contain statements to populate a
    # [[http://www.sgi.com/tech/stl/Map.html | std::map]].
    awk '{print "errnoMap[\""$2"\"] = std::make_pair("$2", std::string(std::strerror("$2")));"}' < errno.${sort} > errnoMap.ii

    ${CXX} ${CXX_FLAGS} -o errno errnoMap.cpp && ./errno | sort --numeric-sort --key=${key-1} > errnoMap.${sort}
    rm -f errnoMap.i errno
}


case $(uname) in
    Darwin)
        CPP="clang"
        CXX="clang++"
        C_STD="-std=c17"
        CXX_STD="-std=c++17"
        CPP_FLAGS="${C_STD} -E -dM"
        CXX_FLAGS="${CXX_FLAGS} ${CXX_STD}"
        ERRNO="/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/errno.h
"
    ;;
    Linux)
        CPP="cpp"
        CXX="g++"
        C_STD="-std=gnu17"
        CXX_STD="-std=gnu++17"
        CPP_FLAGS="${C_STD} -dM"
        CXX_FLAGS="${CXX_FLAGS} ${CXX_STD} -static"
        ERRNO="/usr/include/errno.h"

        if [ "x${WIND_BASE}" != "x" ]; then
            CPP="cpppentium"
            CXX="g++pentium"
            STD="-std=gnu++98"
            CPP_FLAGS="${DEFINE} ${INCLUDE}"
            CXX_FLAGS="${STD} -static -march=core2 -nostdlib -fno-builtin -fno-defer-pop -fno-implicit-fp -fno-zero-initialized-in-bss -Wall -MD -MP -fexceptions -frtti -Wunused -Wextra -Werror ${DEFINE} ${INCLUDE}"
            ERRNO="${WIND_BASE}/target/h/errno.h"
            DEFINE="-D_WRS_KERNEL -DVX_CPU_FAMILY=pentium -DCPU=_VX_CORE -DTOOL_FAMILY=gnu"
            INCLUDE="-I${WIND_BASE}/target/h -I${WIND_BASE}/target/h/wrn/coreip"
        fi
    ;;
    *)
        echo "System is unknown."
        exit -1
    ;;
esac

# Create a list containing all the defined errno numbers.  It will be sorted by
# the errno value.
createMap "sorted_by_number" 3

# Sorted by name instead of number:
createMap "sorted_by_name" 2
