#include <stdio.h>


int main(int a, char* b[])
{
    int i = 0;
    while(i < 5)
    {
        printf("i = %d.\n", i);
        ++i;
    }

    do
    {
        --i;
        printf("i = %d.\n", i);
    }
    while(i > 0);

    return 0;
}
