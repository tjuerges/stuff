#include "foo.h"
#include "bar.h"

int main(int a,char* b[])
{
   foo::p(); // prints 17
   bar::q();
   foo::p(); // prints 10

   return 0;
}
