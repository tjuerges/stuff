#ifndef preprocessorMacros_h
#define preprocessorMacros_h
/**
 * "@(#) $Id$"
 */

class Foo
{
	public:
	static void staticPrint();
	void print() const;
};


void Foo::staticPrint()
{
	std::cout
		<< "Foo::staticPrint(): __FILE__=" << __FILE__ << std::endl
		<< "Foo::staticPrint(): __LINE__=" << __LINE__ << std::endl
		<< "Foo::staticPrint(): __FUNCTION__=" << __FUNCTION__
		<< std::endl;
}

void Foo::print() const
{
	std::cout
		<< "Foo::print(): __FILE__=" << __FILE__ << std::endl
		<< "Foo::print(): __LINE__=" << __LINE__ << std::endl
		<< "Foo::print(): __FUNCTION__=" << __FUNCTION__
		<< std::endl;
}

#endif
