/**
 * "@(#) $Id$"
 */

#include <iostream>
#include <string>
#include <vector>
#include <sstream>

#include "preprocessorMacros.h"


int main(int argc, char* argv[])
{
    std::cout << "Compiled on "
        << COMPILATION_DATE
        << ", version "
        << VERSION
        << "."
        << std::endl;

	// Standard preprocessor expansion.
	std::cout
		<< "__FILE__=" << __FILE__ << std::endl
		<< "__LINE__=" << __LINE__ << std::endl
		<< "__FUNCTION__=" << __FUNCTION__ << std::endl
		<< "__cplusplus=" << __cplusplus
		<< std::endl;


	// Preprocessor expansion in static class member.
	Foo::staticPrint();

	Foo foo;
	// Preprocessor expansion in normal class member.
	foo.print();
    return 0;
};
