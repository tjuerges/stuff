#
# $Id$
#

TEMPLATE = lib
TARGET = lib2

LANGUAGE = C++

CONFIG += rtti stl exceptions debug warn_on
CONFIG -= qt

INCLUDEPATH += /export/home/delphinus/tjuerges/workspace/Tests/General/linking/link_dependency/lib1

#LIBPATH += 
LIBS += -Wl,-rpath,/export/home/delphinus/tjuerges/workspace/Tests/General/linking/link_dependency/lib1 -L/export/home/delphinus/tjuerges/workspace/Tests/General/linking/link_dependency/lib1 -llib1

HEADERS += lib2.h

SOURCES += lib2.cpp
