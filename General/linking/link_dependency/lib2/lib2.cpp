/**
 * $Id$
 */


#include <iostream>
#include <lib1.h>


void foo()
{
	std::cout << "Calling lib1::bar...\n";

	bar();

	std::cout << "lib1::bar called.\n";
}
