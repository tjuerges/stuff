#
# $Id$
#

TEMPLATE = lib
TARGET = lib1

LANGUAGE = C++

CONFIG += rtti stl exceptions debug warn_on
CONFIG -= qt

LIBS += -lm

HEADERS += lib1.h

SOURCES += lib1.cpp

