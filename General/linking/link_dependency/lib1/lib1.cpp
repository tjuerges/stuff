/**
 * $Id$
 */


#include <iostream>
#include <cmath>


void bar()
{
    std::cout << "I am bar.\n"
        << std::sin(M_PI_2)
        << "\n";
}
