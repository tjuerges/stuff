/**
 * $Id$
 */


#include <lib2.h>
#include <iostream>

namespace std
{
    extern float cos(float);
};

extern float cos(float);

int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
	std::cout << "Calling lib2::foo...\n";

	foo();

	std::cout << "lib2::foo called.\n"
        << "Calling std::cos...\n"
        << std::cos(1.2345678)
        << cos(1.2345678);

	std::cout << "\nstd::cos called.\n";

    return 0;
};
