#
# $Id$
#

TEMPLATE = app
TARGET = app

LANGUAGE = C++

CONFIG += rtti stl exceptions debug warn_on
CONFIG -= qt

INCLUDEPATH += /export/home/delphinus/tjuerges/workspace/Tests/General/linking/link_dependency/lib2

LIBS += -Wl,-rpath,/export/home/delphinus/tjuerges/workspace/Tests/General/linking/link_dependency/lib2 -L/export/home/delphinus/tjuerges/workspace/Tests/General/linking/link_dependency/lib2 -llib2

SOURCES += app.cpp
