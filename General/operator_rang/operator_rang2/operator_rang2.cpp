#include <iostream>

int test1(void);

int test1(void)
{
	std::cout<<"test1: returning true"<<std::endl;
	return 0;
}

int main(int argc,char* argv[])
{
	int result(~test1() & 0x07);
	std::cout << result << std::endl;
	result = (~test1()) & 0x07;
	std::cout << result << std::endl;
	return 0;
}
