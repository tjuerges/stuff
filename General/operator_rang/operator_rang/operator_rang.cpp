#include <iostream>

bool test1(void);
bool test2(void);

bool test1(void)
{
	std::cout<<"test1: returning true"<<std::endl;
	return true;
}

bool test2(void)
{
	std::cout<<"test2: returning false"<<std::endl;
	return false;
}
	
int main(int argc,char* argv[])
{
	if((test1() && test2()) == false)
	{
		std::cout<<"FALSE"<<std::endl;
	}
	else
	{
		std::cout<<"TRUE"<<std::endl;
	}

	if((test2() && test1()) == false)
	{
		std::cout<<"FALSE"<<std::endl;
	}
	else
	{
		std::cout<<"TRUE"<<std::endl;
	}

	return 0;
}
