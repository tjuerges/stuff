/**
 * $Id$
 */


#include <vector>
#include <cxxabi.h>
#include <typeinfo>
#include <iostream>


const std::string demangle(const char* mangled)
{
    std::size_t s(1024U);
    std::vector< char > buffer(s, 0);
    int status(0);

    const std::string demangled(
        __cxxabiv1::__cxa_demangle(mangled, &(buffer[0]), &s, &status));

    return demangled;
}

int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    std::cout << "Demangling static_cast< const char* >(\"foo\") = \""
        << demangle(typeid(static_cast< const char* >("foo")).name())
        << "\"\n";
    std::cout << "Demangling \"foo\" = \""
        << demangle(typeid("foo").name())
        << "\"\n";

    return 0;
};
