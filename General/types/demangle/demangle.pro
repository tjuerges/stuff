#
# $Id$
#

TEMPLATE = app
TARGET = demangle

LANGUAGE = C++

CONFIG += rtti stl exceptions debug warn_on
CONFIG -= qt

SOURCES += demangle.cpp
