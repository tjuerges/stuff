# $Id$

TEMPLATE = app
TARGET = boolean

LANGUAGE = C++

#DEFINES +=

#CONFIG += qt rtti stl warn_on release
CONFIG += rtti stl warn_on
CONFIG -= qt

#QMAKE_LFLAGS += -g

QMAKE_CXXFLAGS_RELEASE += -g $$system(cflags.sh QMAKE_RELEASE_CXX)
QMAKE_CXXFLAGS_DEBUG += -g $$system(cflags.sh QMAKE_DEBUG_CXX)

QMAKE_CFLAGS_RELEASE += -g $$system(cflags.sh QMAKE_RELEASE_C)
QMAKE_CFLAGS_DEBUG += -g $$system(cflags.sh QMAKE_DEBUG_C)

#INCLUDEPATH += /alma/ACS-7.0/TAO/ACE_wrappers/build/linux \
#    /alma/ACS-7.0/TAO/ACE_wrappers/build/linux/ace \
#    /alma/ACS-7.0/TAO/ACE_wrappers/build/linux/TAO \
#    /alma/ACS-7.0/TAO/ACE_wrappers/build/linux/TAO/orbsvcs \
#    /alma/ACS-7.0/TAO/ACE_wrappers/build/linux/TAO/orbsvcs/orbsvcs \
#    /alma/ACS-7.0/ACSSW/include \
#    /alma/ACS-7.0/ACSSW/../rtai/include \
#    /export/home/delphinus/tjuerges/workspace/introot.ICD/include \
#    /export/home/delphinus/tjuerges/workspace/introot.CONTROL/include\
#    /export/home/delphinus/tjuerges/workspace/introot/include

#LIBPATH += /alma/ACS-7.0/TAO/ACE_wrappers/build/linux/ace \
#    /alma/ACS-7.0/TAO/ACE_wrappers/build/linux/TAO/tao \
#    /alma/ACS-7.0/ACSSW/lib \
#    /alma/ACS-7.0/ACSSW/../rtai/lib \
#    /export/home/delphinus/tjuerges/workspace/introot.ICD/lib \
#    /export/home/delphinus/tjuerges/workspace/introot.CONTROL/lib \
#    /export/home/delphinus/tjuerges/workspace/introot/lib

#LIBS += -lACE \
#    -lloki \
#    -lacsutil \
#    -lacscommonStubs \
#    -lbaselogging \
#    -llogging

#TEMPLATE = subdirs
#SUBDIRS += foo bar

#FORMS += boolean.ui

#HEADERS += boolean.h

SOURCES += boolean.cpp

