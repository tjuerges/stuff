/**
 * $Id$
 */


#include <iostream>


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
	const bool foo(true), bar(false);
	const unsigned long foo2(0), bar2(~0);

	if(foo == static_cast< bool >(!foo2))
	{
		std::cout << "foo = foo2." << std::endl;
	}
	else
	{
		std::cout << "foo != foo2." << std::endl;
	}

	if(!foo2)
	{
		std::cout << "!foo2 = true." << std::endl;
	}
	else
	{
		std::cout << "!foo2 = false." << std::endl;
	}

	if(!bar2)
	{
		std::cout << "!bar2 = true." << std::endl;
	}
	else
	{
		std::cout << "!bar2 = false." << std::endl;
	}

    return 0;
};
