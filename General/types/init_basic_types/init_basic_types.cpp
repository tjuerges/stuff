/**
 * $Id$
 */


#include <iostream>


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
	unsigned char UChar;
	char Char;
	unsigned short UShort;
	short Short;
	unsigned long ULong;
	long Long;
	float Float;
	double Double;

	std::cout << "\nType initialisation\n"
		<< "\nunsigned char: "
		<< static_cast< unsigned short >(UChar)
		<< "\nchar: "
		<< static_cast< short >(Char)
		<< "\nunsigned short: "
		<< UShort
		<< "\nshort: "
		<< Short
		<< "\nunsigned long: "
		<< ULong
		<< "\nlong: "
		<< Long
		<< "\nfloat: "
		<< Float
		<< "\ndouble: "
		<< Double
		<< "\n";
	return 0;
};

