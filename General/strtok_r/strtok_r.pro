#
# $Id$
#

TEMPLATE = app
TARGET = strtok_r

LANGUAGE = C

#DEFINES +=

CONFIG -= qt

LIBS = -Wl,--as-needed $${LIBS}

QMAKE_LINK = gcc

QMAKE_CFLAGS_RELEASE += $$system(cflags.sh QMAKE_RELEASE_C)

QMAKE_CFLAGS_DEBUG += $$system(cflags.sh QMAKE_DEBUG_C)

#TEMPLATE = subdirs
#SUBDIRS += foo bar


SOURCES += strtok_r.c
