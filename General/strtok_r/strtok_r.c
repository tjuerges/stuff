#include <stdio.h>
#include <string.h>


int main(int _a, char* b[])
{
    char text[] = "MOV X12 Y34 Z56 U78 V10 W20\n";
    char* savePtr = NULL;
    const char* separator = " U36\n";

    printf("Text = %s\nSeparator = \"%s\"\n\n", text, separator);
    char* found = strtok_r(&(text[4]), separator, &savePtr);
    while(found != NULL)
    {
        printf("%p, \"%s\"\n", (void*)found, found);
        found = strtok_r(NULL, separator, &savePtr);
    };

    return 0;
}
