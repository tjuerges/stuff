/**
 * $Id$
 */

#include <iostream>
#include <cstdio>


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    std::cout << "Compiled on "
        << COMPILATION_DATE
        << ", version "
        << VERSION
        << "."
        << std::endl;

	std::cout << "Allocating 100 bytes." << std::endl;
	void* foo(malloc(100));
	void* bar(foo);

	std::cout << "Freeing first time." << std::endl;
	free(foo);

	std::cout << "Freeing second time." << std::endl;
	free(bar);

	std::cout << "Should have aborted." << std::endl;

    return 0;
};
