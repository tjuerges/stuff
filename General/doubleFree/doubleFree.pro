# $Id$

TEMPLATE = app
TARGET = doubleFree

LANGUAGE = C++

CONFIG -= qt
CONFIG += rtti stl warn_on debug

QMAKE_CXXFLAGS_RELEASE += $$system(cflags.sh QMAKE_RELEASE) \
	-DCOMPILATION_DATE=\\\"`date --iso-8601=seconds`\\\" \
    -DVERSION=\\\"`./compilation_run`\\\"
QMAKE_CXXFLAGS_DEBUG += $$system(cflags.sh QMAKE_DEBUG) \
	-DCOMPILATION_DATE=\\\"`date --iso-8601=seconds`\\\" \
    -DVERSION=\\\"`./compilation_run`\\\"

SOURCES += doubleFree.cpp

