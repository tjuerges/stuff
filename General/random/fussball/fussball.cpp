/**
 * $Id$
 */


#include <iostream>
#include <fstream>
#include <cmath>

unsigned int randomNumber(unsigned int limit)
{
    unsigned int dummy(0U);

    std::ifstream input("/dev/urandom",
        std::ios_base::binary | std::ios_base::in);
    if(input)
    {
        input.read(reinterpret_cast< char* >(&dummy), sizeof(dummy));
        input.close();

    }
    else
    {
        throw std::exception();
    }

    dummy %= limit;
    return dummy;
}

int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    const unsigned maxGoals(10U);
    unsigned int limit(randomNumber(maxGoals));

    while(limit == 0U)
    {
        limit = randomNumber(maxGoals);
    };
        
    std::cout << randomNumber(limit)
        << ":"
        << randomNumber(limit)
        << "\n";

    return 0;
}
