#
# $Id$
#

TEMPLATE = app
TARGET = fussball

LANGUAGE = C++

CONFIG += rtti stl exceptions debug warn_on
CONFIG -= qt

LIBS = -Wl,--as-needed $${LIBS}

QMAKE_CXXFLAGS_RELEASE += $$system(cflags.sh QMAKE_RELEASE_CXX)
QMAKE_CXXFLAGS_DEBUG += $$system(cflags.sh QMAKE_DEBUG_CXX)

LIBS += -lm

SOURCES += fussball.cpp
