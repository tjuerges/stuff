#
# $Id$
#

TEMPLATE = app
TARGET = random

LANGUAGE = C++

CONFIG += rtti stl exceptions debug warn_on
CONFIG -= qt

LIBS = -Wl,--as-needed $${LIBS}

QMAKE_CXXFLAGS_RELEASE += $$system(cflags.sh QMAKE_RELEASE_CXX)
QMAKE_CXXFLAGS_DEBUG += $$system(cflags.sh QMAKE_DEBUG_CXX)
#LIBS += -lACE
#LIBS += -lloki
#LIBS += -lacsutil
#LIBS += -lacscommonStubs
#LIBS += -lbaselogging
#LIBS += -llogging

#TEMPLATE = subdirs
#SUBDIRS += foo bar

#FORMS += random.ui

SOURCES += \
	random.cpp \ 

