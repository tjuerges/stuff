//
// * $Id$
//
#include <fstream>
#include <iostream>
#include <vector>
#include <cstdlib>


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    std::ifstream input("/dev/urandom", std::ios_base::binary
        | std::ios_base::in);

    if(input)
    {
        double dummy[2] = {0.0, 42.42};

        std::cout << "Random number1 = "
            << dummy[0]
            << "\nRandom number2 = "
            << dummy[1]
            << "\n";

        input.read(reinterpret_cast< char* > (&dummy), sizeof(dummy));
        input.close();

        std::cout << "Random number1 = "
            << dummy[0]
            << "\nRandom number2 = "
            << dummy[1]
            << "\n";
    }
    else
    {
        std::cout << "Problem.\n";
    }

    std::cout << "::random() = "
        << ::random()
        << ", RAND_MAX = "
        << RAND_MAX
        << ", ::random() / static_cast< double >(RAND_MAX) = "
        << (::random() / static_cast< double >(RAND_MAX))
        << ".\n";

    return 0;
}
