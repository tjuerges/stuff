/**
 * "@(#) $Id$"
 */
#include <stdio.h>


int main(int argc, char* argv[])
{
	typedef struct
	{
		const int a;
		const char* b;
	} foo_s;

    printf("Compiled on " COMPILATION_DATE " version " VERSION ".\n");

	foo_s foo = {1, "Yo!"};

	printf("foo.a = %d\nfoo.b = %s\n", foo.a, foo.b);
    return 0;
};
