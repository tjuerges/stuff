/**
 * $Id$
 */

#include <iostream>


using std::cout;
using std::endl;


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    cout << "Compiled on "
        << COMPILATION_DATE
        << ", version "
        << VERSION
        << "."
        << endl;

	std::string foo("foo");

	pid_t ppid = fork();
	switch(ppid)
	{
		case -1:
			cout << "error" << foo << endl;
			break;

		case 0:
			cout << "fork" << endl;
			break;

		default:
			cout << "parent" << endl;
			break;
	}

	return 0;
};
