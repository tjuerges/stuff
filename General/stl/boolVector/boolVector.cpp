/**
 * "@(#) $Id$"
 */

#include <iostream>
#include <vector>
#include <iterator>

int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    std::cout << "Compiled on "
        << COMPILATION_DATE
        << ", version "
        << VERSION
        << "."
        << std::endl;


	std::vector< bool > a(8);
	unsigned char raw(0x95); // = %1001 0101

    a[0] = raw & 0x01;
	a[1] = raw & 0x02;
	a[2] = raw & 0x04;
	a[3] = raw & 0x08;
	a[4] = raw & 0x10;
	a[5] = raw & 0x20;
	a[6] = raw & 0x40;
	a[7] = raw & 0x80;

	int j(0);
	for(std::vector< bool >::iterator i(a.begin()); i != a.end(); ++i, ++j)
	{
		std::cout << j << ": " << *i << std::endl;
	}

    return 0;
};
