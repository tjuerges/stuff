#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <map>

class Comparison
{
	public:
		bool operator()(const std::string&, const std::string&) const;
};

bool Comparison::operator()(const std::string& x, const std::string& y) const
{
	std::cout << "Comparing " << x << " with " << y << "." <<std::endl;

	if(x != y)
	{
		return false;
	}

	return true;
}


int main(int argc, char* argv[])
{
	std::map<std::string, std::string, Comparison> myMap;

	myMap.insert(std::make_pair("Foo", "Bar"));
	myMap.insert(std::make_pair("Willy", "Joe"));

	return 0;
};

