/**
 * $Id$
 */


#include <iostream>
#include <sstream>
#include <ios>
#include <iomanip>


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
	const long long number(0x0807060504030201LL);

	std::cout << "Output (hexadecimal base) of 0x0807060504030201LL:  "
        << std::hex
        << "0x" << number
        << "\n"
	    << "\\n Does not change the manipulator:\n"
        << "0x" << number
        << "\n"
	    << "Neither does std::endl:"
        << std::endl
        << "0x" << number
        << "\n";

	std::cout << "\n\nNow decimal base? "
        << "0x" << number
        << "\nNo.  Still not decimal base even when printed out in a new "
            "statement.\n\n\n";

    // Reset the base flags to 10-base.
    std::cout<< std::dec;

    // Now do it right.  Store the old format flags.
    std::ios::fmtflags oldFlags(std::cout.flags());
    // Set the new format flags, make sure that no colliding flags are possible
    // by using the flags mask.
    std::cout.setf(std::ios::hex, std::ios::basefield);
    std::cout.setf(std::ios::showbase);
    std::cout.setf(std::ios::internal, std::ios::adjustfield);
    const unsigned int number2(0x02040607);
    // Output.
    std::cout << "Output (hexadecimal base) of 0x0807060504030201LL:\n"
        << std::setw(30) << std::setfill('0') << number2
        << "\n\n";
    // Restore old flags.
    std::cout.setf(oldFlags);
    // Everything is okay.
    std::cout << "Output (decimal base) of 0x0807060504030201LL:\n"
        << std::setw(30) << std::setfill('0') << number2
        << "\n\n\n";

    // Set the new format flags, make sure that no colliding flags are possible
    // by using the flags mask.
    std::cout.setf(std::ios::showbase | std::ios::scientific, std::ios::floatfield);
    std::cout.precision(16);
    // Output.
    const double doubleNumber(11.12345678901234567890e9);
    std::cout << "Output (scientific) of 11.12345678901234567890e9:\n"
        << doubleNumber
        << "\n\n";
    std::cout.setf(oldFlags);
    std::cout << "Output (normal) of 11.12345678901234567890e9:\n"
        << doubleNumber
        << "\n\n\n";


    std::ostringstream foo("0x");
    foo << std::hex;

    for(unsigned int i(0U); i < 8U; ++i)
    {
        foo << std::setw(2)
            << std::setfill('0')
            << static_cast< unsigned short >(i);
    }

    std::cout << "foo = 0x"
        << foo.str()
        << "\n";

    std::ostringstream foo2("0x");
    foo2 << std::hex
        << std::setw(2)
        << std::setfill('0');

    for(unsigned int i(0U); i < 8U; ++i)
    {
        foo2 << static_cast< unsigned short >(i);
    }

    std::cout << "foo2 = 0x"
        << foo2.str()
        << "\n";

    char tmpSN[] = {1, 2, 3, 4, 5, 6, 7, 8};
    std::ostringstream uniqueId;
    uniqueId.width(2);
    uniqueId.fill('0');
    uniqueId.setf(std::ios::hex, std::ios::basefield);
    uniqueId.setf(std::ios::internal, std::ios::adjustfield);
    uniqueId << "0x";
    for(unsigned int idx(0); idx < 8U; ++idx)
    {
        uniqueId << static_cast< unsigned short >(tmpSN[idx]);
    }

    std::cout << uniqueId.str() << "\n";

    return 0;
};
