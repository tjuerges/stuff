#include <iostream>
#include <string>


namespace StringTools
{
    std::string removeWhitespaces(const std::string& inputString,
        const std::string whitespaces = " \t\n\r")
    {
        std::string str(inputString);
        std::string::size_type position(0), endPosition(0);
        while((position = str.find_first_of(whitespaces, position))
            != std::string::npos)
        {
            endPosition = str.find_first_not_of(whitespaces, position);
            str.erase(position, endPosition - position);
        };

        return str;
    }
}


int main(int a __attribute__((unused)), char* b[] __attribute__((unused)))
{
    std::string inputString("Venus=Venus;Mars    =    Mars;Jupiter=   Jupiter;Neptun     =Neptun;    Merkur    =    Merkur;    Uranus   =   Uranus   ;   Pluto   =    ;  Saturn    ;   =   Erde   ;    ");

    std::cout << "Input string = \""
        << inputString
        << "\"\nOutput sting with whitespaces removed = \""
        << StringTools::removeWhitespaces(inputString)
        << "\".\n\n";

    return 0;
}
