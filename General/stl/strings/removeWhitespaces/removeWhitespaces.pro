#
# $Id$
#

TEMPLATE = app
TARGET = removeWhiteSpaces

LANGUAGE = C++

#DEFINES +=

#CONFIG += qt rtti stl exceptions debug warn_on 
#CONFIG += qt rtti stl exceptions release warn_on 
CONFIG += rtti stl exceptions debug warn_on
CONFIG -= qt

#CONFIG += link_pkgconfig
#PKGCONFIG +=

LIBS = -Wl,--as-needed $${LIBS}

#QMAKE_CXXFLAGS_RELEASE += $$system(cflags.sh QMAKE_RELEASE_CXX)
#QMAKE_CXXFLAGS_DEBUG += $$system(cflags.sh QMAKE_DEBUG_CXX)

#INCLUDEPATH +=
#LIBPATH +=
#LIBS +=

#FORMS += removeWhiteSpaces.ui


SOURCES += removeWhiteSpaces.cpp
