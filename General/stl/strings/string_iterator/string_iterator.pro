#
# $Id$
#

TEMPLATE = app
TARGET = string_iterator

LANGUAGE = C++

CONFIG += rtti stl exceptions debug warn_on
CONFIG -= qt

SOURCES += string_iterator.cpp

