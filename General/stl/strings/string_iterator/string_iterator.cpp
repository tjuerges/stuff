/**
 * $Id$
 */


#include <iostream>
#include <string>


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
	std::string foo("123456789ABCDEF");
	for(std::string::iterator i(foo.begin()); i != foo.end(); ++i)
	{
		std::cout << *i << "\n";
	}

    return 0;
};
