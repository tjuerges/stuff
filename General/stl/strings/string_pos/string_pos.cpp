/**
 * "@(#) $Id$"
 */

#include <iostream>
#include <string>
#include <vector>
#include <sstream>

int main(int argc, char* argv[])
{
    std::cout << "Compiled on "
        << COMPILATION_DATE
        << ", version "
        << VERSION
        << "."
        << std::endl;


	std::string foo("123456789");
	std::string sub(foo.end() - 1, foo.end());
	std::cout << foo << std::endl
		<< sub << std::endl;
    return 0;
};
