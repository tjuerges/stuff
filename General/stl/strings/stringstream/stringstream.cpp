#include <iostream>
#include <string>
#include <vector>
#include <sstream>

int main(int argc, char* argv[])
{
	std::string foo1("3298749283429086029");
	const char foo2[] = "skdhfksjdfgkjsf";

	std::ostringstream bar1;
	bar1 << foo1 << std::ends;
	std::string bar2(bar1.str());
	std::cout << bar1.str() << std::endl << bar2 << std::endl;
	bar1.str(foo2);
	std::string bar3(bar1.str());
	std::cout << bar1.str() << std::endl << bar3 << std::endl;
	return 0;
};

