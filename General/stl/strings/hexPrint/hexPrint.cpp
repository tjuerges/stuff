/**
 * $Id$
 */


#include <iostream>
#include <string>
#include <sstream>


void hexPrint(const std::string& printThis)
{
    std::ostringstream buffer;

    for(std::string::const_iterator iter(printThis.begin());
        iter != printThis.end(); ++iter)
    {
        buffer << std::hex
            << "0x"
            << static_cast< int >(*iter)
            << std::dec
            << " ";
    }

	std::cout << "Hex print = "
		<< buffer.str()
		<< "\n";
};

int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
	std::cout << "\n\nPrinting \\n\\r" << "\n";
	hexPrint("\n\r");
	std::cout << "\nPrinting \\r\\n" << "\n";
	hexPrint("\r\n");
	std::cout << "\nPrinting \\nFoo\\r" << "\n";
	hexPrint("\nFoo\r");
	std::cout << "\nPrinting \\rFoo\\n" << "\n";
	hexPrint("\rFoo\n");
	std::cout << "\n\n";

    return 0;
};
