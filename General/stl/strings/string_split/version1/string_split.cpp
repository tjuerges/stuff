/**
 * $Id$
 */

#include <iostream>
#include <sstream>
#include <string>
#include <map>


namespace StringTools
{
    /**
    * @brief Return a substring value either as numeric or as string value.
    *
    * @param str: input string.
    * @param delimiter: string which contains delimiting characters. The first
    * occurence of any of them will match.
    * @param startPos: position in \a str from where the search starts. Will be
    * modified to point to the begin of the found substring.
    *
    * @return The value of the substring. The type depends on \a T,
    */
    template< typename T > T getSubstringValue(const std::string& str,
        const std::string& delimiter, std::string::size_type& startPos)
    {
        T returnValue(0);
        std::istringstream value;

        std::string::size_type pos(str.find_first_of(delimiter, startPos));
        if(pos == std::string::npos)
        {
            pos = str.size() - 1;
        }

        value.str(str.substr(startPos, pos - startPos));
        startPos = pos;
        value >> returnValue;

        return returnValue;
    }

    /**
    * @brief Split input std::string splitThis into key/value pairs.
    *
    * @param entrySplitter: each key/value pair is separated by
    * the other through this string.
    * @param keyValueSplitter: each key is separated from the value
    * through this string.
    *
    * @return Returns a map containing the key/value pairs as strings.
    */
    std::map< std::string, std::string > splitString(
        const std::string& splitThis,
        const std::string& entrySplitter, const std::string& keyValueSplitter)
    {
        std::map< std::string, std::string > result;

        const std::string::size_type keyValueSplitterLength(
            keyValueSplitter.size());
        const std::string::size_type entrySplitterLength(entrySplitter.size());

        /**
        * Test if any of the input strings has 0 size. Return an empty map if
        * true.
        * Test if the key/value separator and the pair separator are the same.
        * Return an ampty map if true.
        */
        if((keyValueSplitterLength == 0)
        || (entrySplitterLength == 0)
        || (entrySplitter == keyValueSplitter))
        {
            return result;
        }

        std::string::size_type entryPosition(0);
        std::string::size_type valuePosition(0);
        std::string::size_type pos(0);

        std::string key("No value");
        std::string value("No value");

        do
        {
            valuePosition = splitThis.find(keyValueSplitter, entryPosition);
            if(valuePosition == std::string::npos)
            {
                break;
            }

            entryPosition = splitThis.find(entrySplitter, valuePosition);

            if(entryPosition == std::string::npos)
            {
                entryPosition = splitThis.size();
            }

            pos = splitThis.rfind(entrySplitter, valuePosition);
            if(pos == std::string::npos)
            {
                pos = 0;
            }
            else
            {
                pos += entrySplitterLength;
            }

            key.assign(splitThis, pos, (valuePosition - pos));
            valuePosition += keyValueSplitterLength;
            value.assign(splitThis, valuePosition, (entryPosition - valuePosition));
            entryPosition += entrySplitterLength;

            result[key] = value;
        }
        while(true);

        return result;
    }
}


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
	// Split the following string into key/value pairs.
    const std::string splitter(" aaa ");
    const std::string keyvalSplitter("aaa: ");

    std::string splitThis(
        "Antenna" + keyvalSplitter + "FooOK" + splitter +
        "PLL" + keyvalSplitter + "OK" + splitter +
        "GPS" + keyvalSplitter + "Unlocked" + splitter +
        "Foo" + keyvalSplitter + "Very Bar!");

	std::cout << "String to be split into key/value pairs: \n"
	    << splitThis
	    << "\n\nEntry splitter = \""
	    << splitter
	    << "\"\nKey/Value splitter = \""
	    << keyvalSplitter
	    << "\"\n\n";

	std::map< std::string, std::string > result(
	    StringTools::splitString(splitThis, splitter, keyvalSplitter));

	for(std::map< std::string, std::string >::const_iterator iter(
	        result.begin()); iter != result.end(); ++iter)
	{
		std::cout << "Key = "
			<< (*iter).first
			<< ", Value = "
			<< (*iter).second
			<< "\n";
	}
    return 0;
};
