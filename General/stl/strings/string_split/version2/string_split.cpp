#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <utility>
#include <boost/foreach.hpp>
#include <boost/format.hpp>


namespace Tools
{
    typedef std::pair< std::string, std::string > StringPair;
    typedef std::map< std::string, std::string > StringMap;
    typedef std::vector< std::string > StringVector;


    static void sanitiseString(std::string& str,
        const std::string whiteSpace = " \t\n\r")
    {
        std::string::size_type position(0), endPosition(0);
        while((position = str.find_first_of(whiteSpace, position))
            != std::string::npos)
        {
            endPosition = str.find_first_not_of(whiteSpace, position);
            str.erase(position, endPosition - position);
        };
    }

    static StringPair splitStringToPair(const std::string& str,
        const std::string& delimiter, bool sanitiseWhitespace = true)
    {
        StringPair result;
        std::string firstString, secondString;

        std::string::size_type position(0);
        position = str.find(delimiter, position);
        if(position != std::string::npos)
        {
            firstString = str.substr(0, position);
            secondString = str.substr(position + 1, std::string::npos);
            if((firstString.empty() == false)
            && (secondString.empty() == false))
            {
                if(sanitiseWhitespace == true)
                {
                    sanitiseString(firstString);
                    sanitiseString(secondString);
                }

                result = std::make_pair(firstString, secondString);
            }
        }

        return result;
    }

    static StringVector splitString(const std::string& str,
        const std::string& delimiter, bool sanitiseWhitespace = true)
    {
        std::string::size_type previousPosition(0), position(0);
        StringVector result;
        std::string subString;

        // Find delimiter within string n times and copy n+1 substrings to result.
        do
        {
            position = str.find(delimiter, previousPosition);
            if(position != std::string::npos)
            {
                subString = str.substr(previousPosition,
                    position - previousPosition);
                if(sanitiseWhitespace == true)
                {
                    sanitiseString(subString);
                }

                result.push_back(subString);
                previousPosition = position + 1;
            }
        }
        while(position != std::string::npos);

        // Now copy the remaining part, too.
        result.push_back(str.substr(previousPosition,
            position - previousPosition));

        return result;
    }
}

int main(int a __attribute__((unused)), char* b[] __attribute__((unused)))
{
    const std::string strings("Venus=Venus;Mars    =    Mars;Jupiter=   Jupiter;Neptun     =Neptun;    Merkur    =    Merkur;    Uranus   =   Uranus   ;   Pluto   =    ;  Saturn    ;   =   Erde   ;    ");

    const Tools::StringVector stringVector(Tools::splitString(strings, ";"));
    Tools::StringMap stringMap;
    Tools::StringPair result;

    std::string s;
    std::cout << "Input string = \""
        << strings
        << "\"\n\n";

    BOOST_FOREACH(s, stringVector)
    {
        result = Tools::splitStringToPair(s, "=");

        std::cout << boost::str(boost::format("Sanitised input string = "
        "\"%s\"\nString pair = (\"%s\", \"%s\")\n\n")
            % s % result.first % result.second);

        if(result.first.empty() == false)
        {
            stringMap.insert(result);
        }
    }

    BOOST_FOREACH(result, stringMap)
    {
        std::cout << boost::str(boost::format("String map entry = ([\"%s\"], "
            "\"%s\")\n") % result.first % result.second);
    }

    return 0;
}
