# $Id$

TEMPLATE = app
TARGET = string_comparison

LANGUAGE = C++

#DEFINES +=

#CONFIG = qt rtti stl warn_on release
CONFIG = rtti stl warn_on

QMAKE_CXXFLAGS_RELEASE += -g $$system(cflags.sh QMAKE_RELEASE) \
	-DCOMPILATION_DATE=\\\"`date --iso-8601=seconds`\\\" \
    -DVERSION=\\\"`./compilation_run`\\\"
QMAKE_CXXFLAGS_DEBUG += -g $$system(cflags.sh QMAKE_DEBUG) \
	-DCOMPILATION_DATE=\\\"`date --iso-8601=seconds`\\\" \
    -DVERSION=\\\"`./compilation_run`\\\"

#INCLUDEPATH += /alma/ACS-6.0/TAO/ACE_wrappers/build/linux \
#    /alma/ACS-6.0/TAO/ACE_wrappers/build/linux/ace \
#    /alma/ACS-6.0/TAO/ACE_wrappers/build/linux/TAO \
#    /alma/ACS-6.0/TAO/ACE_wrappers/build/linux/TAO/orbsvcs \
#    /alma/ACS-6.0/TAO/ACE_wrappers/build/linux/TAO/orbsvcs/orbsvcs \
#    /alma/ACS-6.0/ACSSW/include \
#    /alma/ACS-6.0/ACSSW/../rtai/include \
#    /export/home/delphinus/tjuerges/workspace/introot.ICD/include \
#    /export/home/delphinus/tjuerges/workspace/introot.CONTROL/include\
#    /export/home/delphinus/tjuerges/workspace/introot/include

#LIBPATH += /alma/ACS-6.0/TAO/ACE_wrappers/build/linux/ace \
#    /alma/ACS-6.0/TAO/ACE_wrappers/build/linux/TAO/tao \
#    /alma/ACS-6.0/ACSSW/lib \
#    /alma/ACS-6.0/ACSSW/../rtai/lib \
#    /export/home/delphinus/tjuerges/workspace/introot.ICD/lib \
#    /export/home/delphinus/tjuerges/workspace/introot.CONTROL/lib \
#    /export/home/delphinus/tjuerges/workspace/introot/lib

#LIBS += -lACE \
#    -lloki \
#    -lacsutil \
#    -lacscommonStubs \
#    -lbaselogging \
#    -llogging

#TEMPLATE = subdirs
#SUBDIRS += foo bar

#FORMS += string.ui

SOURCES += \
	string_comparison.cpp

