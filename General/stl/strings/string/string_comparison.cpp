#include <string>
#include <iostream>

int main(int a,char* b[])
{
	std::string foo("Hello wolrd!");
	std::string bar("!Hola mundo!");

	std::cout << "Foo=" << foo << std::endl
	<< "Bar = " << bar << std::endl;

	if(foo == bar)
	{
		std::cout << "Foo == Bar (surprise)!" <<std::endl << std::endl;
	}
	else
	{
		std::cout << "Foo != Bar (as expected)." <<std::endl << std::endl;
	}

	foo = "!Hola mundo!";

	std::cout << "Foo=" << foo << std::endl
	<< "Bar = " << bar << std::endl;

	if(foo == bar)
	{
		std::cout << "Foo == Bar (as expected)." <<std::endl << std::endl;
	}
	else
	{
		std::cout << "Foo != Bar (surprise)!" <<std::endl << std::endl;
	}


	foo.reserve(1000);
	bar.reserve(100);

	std::cout << "Foo=" << foo << std::endl
	<< "Bar = " << bar << std::endl;

	if(foo == bar)
	{
		std::cout << "Foo == Bar (as expected)." <<std::endl << std::endl;
	}
	else
	{
		std::cout << "Foo != Bar (surprise)!" <<std::endl << std::endl;
	}

	return 0;
}
 
