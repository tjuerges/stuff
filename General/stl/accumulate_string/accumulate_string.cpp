/**
 * $Id$
 */


#include <iostream>
#include <sstream>
#include <string>
#include <list>
#include <map>
#include <vector>
#include <functional>
#include <numeric>


/*template< class A, class B > struct foo: public std::binary_function< A, B, A >
{
    A operator()(const A& key, const B& messages) const
    {
        strings += const_cast< B& >(messages)[key];
        return strings;
    };

    A value() const
    {
        return strings;
    };


    mutable A strings;
};
*/

template< class A, class B > class foo: public std::binary_function< A, B, A >
{
    public:
    A operator()(const A& key, const B& messages) const
    {
        strings += const_cast< B& >(messages)[key];
        return strings;
    };

    A value() const
    {
        return strings;
    };


    private:
    mutable A strings;
};


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    std::list< std::string > currentErrors;
    currentErrors.push_back("One");
    currentErrors.push_back("Two");

    std::map< std::string, std::string > errorMessages;
    errorMessages.insert(std::make_pair(
        "One", "This is error message one.\n"));
    errorMessages.insert(std::make_pair(
        "Two", "This is error message two.\n"));

    std::string errorString;

    for(std::list< std::string >::const_iterator iter(currentErrors.begin());
        iter != currentErrors.end(); ++iter)
    {
        errorString.append(errorMessages[(*iter)]);
    }

    std::cout << "Error string: "
        << errorString
        << "\n\n";

    std::string result = 
        std::for_each(currentErrors.begin(), currentErrors.end(),
            bind2nd(foo< std::string, std::map< std::string, std::string > >(),
            errorMessages));

    std::cout << "Error string: "
        << result
        << "\n";
    return 0;
};
