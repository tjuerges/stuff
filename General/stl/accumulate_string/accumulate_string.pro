#
# $Id$
#

TEMPLATE = app
TARGET = accumulate_string

LANGUAGE = C++

CONFIG += rtti stl exceptions debug warn_on
CONFIG -= qt

SOURCES += accumulate_string.cpp
