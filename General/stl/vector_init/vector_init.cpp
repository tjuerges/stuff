#include <iostream>
#include <vector>
#include <iterator>

using namespace std;

int main(int argc, char* argv[])
{
	unsigned char bar[] = {'\x0a', '\xfe'};
	std::vector<unsigned char> foo(*bar);

	for(std::vector< unsigned char >::iterator iter(foo.begin());
		iter != foo.end(); ++iter)
	{
		std::cout << "+++" << *iter << "+++" << std::endl;
	}

	return 0;
};

