#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <bitset>

int main(int argc, char* argv[])
{
	std::bitset< 16 > bits;

	bits.set(15);
	std::cout << "FEDCBA9876543210" << std::endl << bits << std::endl;

	try
	{
		bits.set(16);
		std::cout << "FEDCBA9876543210" << std::endl << bits << std::endl;
	}
	catch(...)
	{
		std::cout << "Cannot set bit# 16 because bitsets count from 0!" << std::endl;
	};
	
	return 0;
};

