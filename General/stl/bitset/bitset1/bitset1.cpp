#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <bitset>

int main(int argc, char* argv[])
{
	std::bitset<1> foo;
	foo.set(0);

	std::cout << foo.test(0) << std::endl;
	return 0;
};
