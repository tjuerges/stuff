#
# $Id$
#

TEMPLATE = app
TARGET = bitset1

LANGUAGE = C++

CONFIG += strict_c++ c++14 c11 strict_c rtti stl exceptions thread release warn_on
#CONFIG += qt strict_c++ c++14 c11 strict_c rtti stl exceptions thread debug warn_on
#CONFIG += qt strict_c++ c++14 c11 strict_c rtti stl exceptions thread release warn_on
CONFIG -= qt
CONFIG += cmdline

#CONFIG += link_pkgconfig
#PKGCONFIG +=

#DEFINES +=

LIBS = $${LIBS}

#INCLUDEPATH +=
#LIBPATH +=
#LIBS +=

#FORMS += bitset1.ui


SOURCES += bitset1.cpp
