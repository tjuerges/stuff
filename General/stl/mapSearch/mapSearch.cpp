/**
 * $Id$
 */


#include <iostream>
#include <string>
#include <map>
#include <memory>
#include <sstream>
#include <algorithm>


// Class which searches for substrings in std::map keys of type std::string.
class FindInMap
{
    public:
    FindInMap(const std::string& _searchForThis):
        searchForThis(_searchForThis)
    {};

    const std::string searchString() const
    {
        return searchForThis;
    };

    template< typename ValueType >bool operator()(
        const std::pair< const std::string, ValueType >& pair)
    {
        // std::string.find returns std::string::npos if a string could not be
        // found. Since it is the objective to find a substring from the
        // position 0 on, only return true if the found position == 0.
        const std::string::size_type pos(pair.first.find(searchForThis));
        if(pos == 0)
        {
            return true;
        }

        return false;
    };

    private:
    // Keeps a copy of the substring we are looking for.
    const std::string searchForThis;
};


template< typename ValueType >void printPair(
    const std::pair< const std::string, ValueType >& pair)
{
    std::cout << "Key = "
        << pair.first
        << ", Value = "
        << pair.second
        << std::endl;
}


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    std::map< std::string, std::string > foo;

    // The key and the value templates.
    const std::string key("Yo_"), value("World_");

    // File the map with key-value pairs.
    for(std::size_t i(0U); i < 10U; ++i)
    {
        // std::ostringstream behaves off (as far as my judgement is okay) if
        // k, v are defined outside this loop and just reset to
        // k.str(key); k << i.
        std::ostringstream k, v;
        k << key << i;
        v << value << i;

        foo.insert(std::make_pair(k.str(), v.str()));
    }

    // Add the key-value pair which matches perfectly.
    foo.insert(std::make_pair(key, value));

    // Print all key-value pairs.
    for(std::map< std::string, std::string >::const_iterator iter(foo.begin());
        iter != foo.end(); ++iter)
    {
        printPair(*iter);
    }

    // Set the substring which shall be found in the keys.
    FindInMap find("Yo");

    std::cout << std::endl
        << "Searching for "
        << find.searchString()
        << "..."
        << std::endl;

    // This is the iterator which will point at the current key-value
    // pair where the key matches the substring. To bein with the search,
    // it is set to std::map::begin().
    std::map< std::string, std::string >::iterator iter(foo.begin());

    // Now iterate through all occurences in the std::map where the search
    // string matches a substring in a key. Stop if std::map::end() is returned.
    while((iter = std::find_if(iter, foo.end(), find)) != foo.end())
    {
        // A key-value pair has been found. Print it.
        std::cout << "Found: ";
        printPair(*iter);

        // Now increment the iterator so that the search continues with the next
        // key-value pair in the std::map or std::map::end() is automatically
        // reached.
        ++iter;
    }

    return 0;
};
