# $Id$

TEMPLATE = app
TARGET = mapSearch

LANGUAGE = C++

CONFIG += rtti stl warn_on
CONFIG -= qt

QMAKE_CXXFLAGS_RELEASE += -g $$system(cflags.sh QMAKE_RELEASE)
QMAKE_CXXFLAGS_DEBUG += -g $$system(cflags.sh QMAKE_DEBUG)

QMAKE_LFLAGS = -s

SOURCES += mapSearch.cpp
