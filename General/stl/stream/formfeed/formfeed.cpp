#include <iostream>
#include <string>
#include <vector>
#include <sstream>

int main(int argc, char* argv[])
{
	std::string foo1("1234567890");
	std::string foo2("0987654321");

	std::ostringstream ostr;
	ostr << foo1 << "\naaa" << '\x1d' << "aaa" << foo2 << "\n" << std::ends;

	std::cout << ostr.str();
	return 0;
};

