#include <iostream>
#include <string>
#include <vector>
#include <iterator>
#include <sstream>

using namespace std;

int main(int argc, char* argv[])
{
	istringstream istr("1.234567 8.991", istringstream::in);
	istream_iterator<double> iter(istr), eof;
	double a;
	while(iter != eof)
	{
	    a = *iter;
	    ++iter;
	    cout << a << endl;
	}

	return 0;
};

