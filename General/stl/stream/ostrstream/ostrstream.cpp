/**
 * $Id$
 */


#include <iostream>
#include <sstream>


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    std::ostringstream foo;
    foo.str("Hello");
    foo << " world!";
    std::cout << foo.str()
        << "\n";

    std::ostringstream foo2("Hello");
    foo2 << " world!";
    std::cout << foo2.str()
        << "\n";

    std::ostringstream foo3;
    foo3 << "Hello"
        << " world!";
    std::cout << foo3.str()
        << "\n";

    std::cout << "Printing again: '"
        << foo3.str()
        << "'\n";

    foo3.str("");

    std::cout << "Printing again: '"
        << foo3.str()
        << "'\n";

    return 0;
};
