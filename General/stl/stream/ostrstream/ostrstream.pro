# $Id$

TEMPLATE = app
TARGET = ostrstream

LANGUAGE = C++

CONFIG += rtti stl warn_on debug
CONFIG -= qt

QMAKE_CXXFLAGS_RELEASE += $$system(cflags.sh QMAKE_RELEASE)
QMAKE_CXXFLAGS_DEBUG += $$system(cflags.sh QMAKE_DEBUG)

SOURCES += ostrstream.cpp
