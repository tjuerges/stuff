#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <iomanip>

using namespace std;

int main(int argc, char* argv[])
{
	long foo(12345);
	std::ostringstream bar;

	bar << std::setw(6) << std::setfill('0') << foo;
	std::cout << bar.str() << std::endl;

	return 0;
};
