#
# $Id$
#

TEMPLATE = app
TARGET = streamMustBeClearedForReuse

LANGUAGE = C++

CONFIG += stl debug warn_on
CONFIG -= qt

SOURCES += streamMustBeClearedForReuse.cpp
