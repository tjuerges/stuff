//
// $Id$
//


#include <iostream>
#include <sstream>


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    std::istringstream value;

    // Prepare the std::istringstream for hex input.
    value.setf(std::ios::hex, std::ios::basefield);
    value.str("0x30002");

    unsigned int val(0);
    value >> val;

    std::cout << "std::istringstream = "
    	<< value.str()
        << ", unsigned int = "
        << val
        << "\n";

    // What no book and not internet page tells you is that
    // you cannot simply reuse the std::istringstream object
    // for another input.  Any attempt to assign a new value fails:
    value.str("0x30001");
    value >> val;

    std::cout << "\nAssignment without clearing the eofbit...\n"
        "std::istringstream = "
    	<< value.str()
        << ", unsigned int = "
        << val
        << "\n";

    // The reason is that the previous output operator has set the
    // std::ios::eofbit.  Unfortunately - or is it just stupid
    // design? - one must clear the "error" bit first before any
    // new assignment can be successful.
    const std::ios::iostate foo(value.rdstate());
    std::cout << "\nstd::ios::failbit = "
        << (foo == std::ios::failbit)
        << "\nstd::ios::goodbit = "
        << (foo == std::ios::goodbit)
        << "\nstd::ios::badbit = "
        << (foo == std::ios::badbit)
        << "\nstd::ios::eofbit = "
        << (foo == std::ios::eofbit)
        << "\n\nClearing the eofbit...";

    value.clear();

    // Now the assignment works just fine.
    value.str("0x30000");
    value >> val;

    std::cout << "\n\nNow assignment with eofbit cleared...\n"
        "std::istringstream = "
    	<< value.str()
        << ", unsigned int = "
        << val
        << "\n";

    return 0;
}
