#include <iostream>


int main(int a, char* b[])
{
    #if (__cplusplus >= 201103L)
        std::cout << "C++11\n";
    #else
        std::cout << "C++98\n";
    #endif

    return 0;
}
