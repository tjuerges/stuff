#include <cstddef>
#include <iostream>


struct unpacked_struct
{
    uint32_t foo1;
    uint16_t foo2[17];
    uint8_t foo3[5];
    uint64_t foo4;
};

#if defined USE_ALIGNAS
#warning Using alignas
struct packed_struct
{
    alignas(uint8_t) uint32_t foo1;
    alignas(uint8_t) uint16_t foo2[17];
    alignas(uint8_t) uint8_t foo3[5];
    alignas(uint8_t) uint64_t foo4;
};
#elif defined USE_PRAGMA
#warning Using pragma
#pragma pack(push)
#pragma pack(1)
struct packed_struct
{
    uint32_t foo1;
    uint16_t foo2[17];
    uint8_t foo3[5];
    uint64_t foo4;
};
#pragma pack(pop)
#elif defined USE_ATTRIBUTE
#warning Using __attribute__
struct packed_struct
{
    uint32_t foo1;
    uint16_t foo2[17];
    uint8_t foo3[5];
    uint64_t foo4;
} __attribute__((packed));
#endif


int main(int a, char* b[])
{
    struct unpacked_struct willino;
    struct packed_struct willy;
    std::cout << "unpacked size = "
        << sizeof(willino) << "\n"
        << "packed size = "
        << sizeof(willy) << "\n";

    return 0;
}

