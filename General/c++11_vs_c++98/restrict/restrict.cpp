#include <boost/format.hpp>
#include <iostream>


void check(double* __restrict__ a, double* __restrict__ b)
{
    if(a != b)
    {
        std::cout << boost::str(
            boost::format("%p != %p\n") % a %b);
    }
    else
    {
        std::cout << boost::str(
            boost::format("%p == %p\n") % a %b);
    }
}

int main(int a, char* b[])
{
    double x{0.0};
    double y{0.0};

    check(&x, &y);

    union u
    {
        double a;
        char b[sizeof(double)];
    };

    u z;
    z.a = 0.1;
    z.b[0] = '1';
    check(&z.a, reinterpret_cast< double* >(&(z.b[0])));

    return 0;
}

