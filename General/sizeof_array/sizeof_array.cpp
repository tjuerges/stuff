/**
 * $Id$
 */


#include <iostream>


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    unsigned long foo[10];
    foo[0] = 12345;
    std::cout << std::hex
        << "foo = "
        << foo
        << ", &foo[0] = "
        << &foo[0]
		<< ", sizeof/expected = "
		<< sizeof(foo)
		<< "/"
		<< sizeof(unsigned long) * 10
        << "\n";

    long double foo2[10];
    foo2[0] = 12345.56789;
    std::cout << std::hex
        << "foo2 = "
        << foo2
        << ", &foo2[0] = "
        << &foo2[0]
		<< ", sizeof/expected = "
		<< sizeof(foo2)
		<< "/"
		<< sizeof(long double) * 10
        << "\n\n";

    return 0;
};
