# $Id$

TEMPLATE = app
TARGET = sizeof_array

LANGUAGE = C++

CONFIG += rtti stl debug warn_on
CONFIG -= qt

QMAKE_CXXFLAGS_RELEASE += -g $$system(cflags.sh QMAKE_RELEASE_CXX)
QMAKE_CXXFLAGS_DEBUG += -g $$system(cflags.sh QMAKE_DEBUG_CXX)

SOURCES += sizeof_array.cpp

