/**
 * $Id: constructorsAlmostSame.cpp 339 2014-09-11 09:35:45Z thomas $
 */


#include <iostream>
#include "constructorsAlmostSame2.h"


/*foo::foo()
{
    std::cout << "foo::foo()\n";
}
*/
foo::foo(int bar)
{
    std::cout << "foo::foo(int bar = 0), bar = \""
        << bar
        << "\"\n";
}


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    std::cout << "foo a0:\n";
    foo a0;

    std::cout << "\nfoo a1(3):\n";
    foo a1(3);

    std::cout << "\nfoo a2(true):\n";
    foo a2(true);

    std::cout << "\nfoo a4('a'):\n";
    foo a4('a');

    std::cout << "\nfoo a5(1.23):\n";
    foo a5(1.23);

    return 0;
};
