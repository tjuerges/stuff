#ifndef constructorCalledAsFunction_h
#define constructorCalledAsFunction_h
/**
 * $Id$
 */


#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>

class foo
{
    public:
    foo():
        name("")
    {
        const int randomValue(rand());
        std::ostringstream s;
        s << randomValue;
        name = s.str();

        std::cout << "I am the foo constructor for \""
            << name
            << "\".  Do I exist, i.e. this != 0?  ";
        if(this != 0)
        {
            std::cout << "yes\n";
        }
        else
        {
            std::cout << "no\n";
        }
    };

    virtual ~foo()
    {
        std::cout << "I am the foo destructor for \""
            << name
            << "\".\n\n";
    };

    private:
        std::string name;
};

class fooInherited: public foo
{
};
#endif
