/**
 * $Id$
 */


#include <iostream>
#include "constructorCalledAsFunction.h"


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{

    std::cout << "foor bar1a:\n";
    foo bar1a;

    std::cout << "\nfoo bar1b():\n";
    foo bar1b();

    std::cout << "\nfooInherited bar1c:\n";
    fooInherited bar1c;
    std::cout << "\n";

    std::cout << "\nfooInherited bar1d():\n";
    fooInherited bar1d();
    std::cout << "\n";

    std::cout << "\nfoo* bar2a(new foo):\n";
    foo* bar2a(new foo);
    delete bar2a;

    std::cout << "\nfoo* bar2b(new foo()):\n";
    foo* bar2b(new foo());
    delete bar2b;

    std::cout << "\nfoo* bar3a = new foo:\n";
    foo* bar3a = new foo;
    delete bar3a;

    std::cout << "\nfoo* bar3b = new foo():\n";
    foo* bar3b = new foo();
    delete bar3b;

    std::cout << "\nfoo* undeleted(new foo):\n";
    foo* undeleted(new foo);

    return 0;
};
