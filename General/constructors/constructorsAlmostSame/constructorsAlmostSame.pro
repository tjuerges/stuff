TEMPLATE = app
TARGET = constructorsAlmostSame
LANGUAGE = C++
CONFIG += c++14 rtti stl exceptions debug warn_on cmdline
CONFIG -= qt
HEADERS += constructorsAlmostSame.h
SOURCES += constructorsAlmostSame.cpp

