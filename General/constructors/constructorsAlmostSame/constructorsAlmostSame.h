#ifndef constructorsAlmostSame_h
#define constructorsAlmostSame_h
/**
 * $Id$
 */


class foo
{
    public:
    foo();
    foo(int bar = 0);
    foo(bool bar = false);
    foo(float bar = 0.0F);
    foo(char bar = 'a');
};
#endif
