/**
 * $Id$
 */


#include <iostream>
#include "constructorsAlmostSame.h"


foo::foo()
{
    std::cout << "foo::foo()\n";
}

foo::foo(int bar)
{
    std::cout << "foo::foo(int bar = 0), bar = \""
        << bar
        << "\".\n";
}

foo::foo(bool bar)
{
    std::cout << "foo::foo(bool bar = false), bar = \""
        << bar
        << "\".\n";
}

foo::foo(float bar)
{
    std::cout << "foo::foo(float bar = 0.0F), bar = \""
        << bar
        << "\".\n";
}

foo::foo(char bar)
{
    std::cout << "foo::foo(char bar = 'a'), bar = \""
        << bar
        << "\".\n";
}


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    foo a0{};
    foo a1(3);
    foo a2(true);
    foo a3(3.3F);
    foo a4('a');

    return 0;
};
