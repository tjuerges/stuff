TEMPLATE = app
TARGET = constructorCallOfBaseClass
LANGUAGE = C++
CONFIG += c++14 rtti stl warn_on debug cmdline
CONFIG -= qt
HEADERS += constructorCallOfBaseClass.h
SOURCES += constructorCallOfBaseClass.cpp
