#ifndef constructorCallOfBaseClass_h
#define constructorCallOfBaseClass_h
/**
 * "@(#) $Id$"
 */


#include <iostream>
#include <string>


class A
{
    public:
    A()
    {
        std::cout << "Constructor of A.\n";
    };
};


class B: public A
{
    public:
//    B(const std::string& a __attribute__((unused)))
    B()
    {
        std::cout << "Constructor of B.\n";
    };

    private:
//    B();
};
#endif
