#ifndef copyConstructor_h
#define copyConstructor_h
/**
 * $Id$
 */


#include <vector>
#include <map>


class FooWith
{
    public:
    FooWith();
    virtual ~FooWith();
    FooWith(const FooWith&);

    std::vector< int > intVec;
    std::map< int, int* > intMap;
};

class FooWithout
{
    public:
    FooWithout();
    virtual ~FooWithout();

    std::vector< int > intVec;
    std::map< int, int* > intMap;
};
#endif
