TEMPLATE = app
TARGET = copyConstructor
LANGUAGE = C++
CONFIG += c++14 rtti stl exceptions debug warn_on cmdline
CONFIG -= qt
HEADERS += copyConstructor.h
SOURCES += copyConstructor.cpp
