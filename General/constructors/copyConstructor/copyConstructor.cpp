/**
 * $Id$
 */


#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <sstream>

#include "copyConstructor.h"


FooWith::FooWith()
{
    std::cout << "FooWith::FooWith\n";
}

FooWith::FooWith(const FooWith& copyMe):
    intVec(copyMe.intVec),
    intMap(copyMe.intMap)
{
    std::cout << "FooWith::FooWith(const FooWith&)\n";
}

FooWith::~FooWith()
{
    std::cout << "FooWith::~FooWith\n";
}


FooWithout::FooWithout()
{
    std::cout << "FooWithout::FooWithout\n";
}

FooWithout::~FooWithout()
{
    std::cout << "FooWithout::~FooWithout\n";
}


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    int a[10] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

    FooWith fooW;
    FooWithout fooWo;

    for(int i(-5); i < 5; ++i)
    {
        fooW.intVec.push_back(i);
        fooW.intMap.insert(std::pair< int, int* >(i, &(a[i])));
        fooWo.intVec.push_back(i);
        fooWo.intMap.insert(std::pair< int, int* >(i, &(a[i])));
    }

    std::cout << "\n\nFooWithout\n";
    FooWithout bar1(fooWo);
    for(std::vector< int >::const_iterator iter(bar1.intVec.begin());
        iter != bar1.intVec.end(); ++iter)
    {
        std::cout << *iter
            << "\n";
    }

    for(std::map< int, int* >::const_iterator iter(bar1.intMap.begin());
        iter != bar1.intMap.end(); ++iter)
    {
        std::cout << (*iter).first
            << ", "
            << (*iter).second
            << "\n";
    }

    std::cout << "FooWith\n";
    FooWith bar2(fooW);
    for(std::vector< int >::const_iterator iter(bar2.intVec.begin());
        iter != bar2.intVec.end(); ++iter)
    {
        std::cout << *iter
            << "\n";
    }

    for(std::map< int, int* >::const_iterator iter(bar2.intMap.begin());
        iter != bar2.intMap.end(); ++iter)
    {
        std::cout << (*iter).first
            << ", "
            << (*iter).second
            << "\n";
    }

    return 0;
};
