#include <stdio.h>


int main(int a __attribute__((unused)), char* b[] __attribute__((unused)))
{
    printf("\n\n"
        "char = %lu\n"
        "short = %lu\n"
        "int = %lu\n"
        "long = %lu\n"
        "long long = %lu\n"
        "float = %lu\n"
        "double = %lu\n"
        "long double = %lu\n"
        "void* = %lu\n\n",
        sizeof(char),
        sizeof(short),
        sizeof(int),
        sizeof(long),
        sizeof(long long),
        sizeof(float),
        sizeof(double),
        sizeof(long double),
        sizeof(void*));

    return 0;
}
