TEMPLATE = app
TARGET = sizeof

LANGUAGE = C++

CONFIG += cmdline c++17 rtti stl exceptions debug warn_on
CONFIG -= qt

INCLUDEPATH = /usr/local/include

SOURCES += sizeof.cpp
