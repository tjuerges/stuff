#
# $Id$
#

TEMPLATE = app
TARGET = sigsegv_handler

LANGUAGE = C++

CONFIG += rtti stl exceptions debug warn_on
CONFIG -= qt

QMAKE_CXXFLAGS_RELEASE += $$system(cflags.sh QMAKE_RELEASE_CXX)
QMAKE_CXXFLAGS_DEBUG += $$system(cflags.sh QMAKE_DEBUG_CXX)

INCLUDEPATH += ${ACE_ROOT} \
    ${ACE_ROOT}/ace \
    ${ACE_ROOT}/TAO \
    ${ACE_ROOT}/TAO/orbsvcs \
    ${ACE_ROOT}/TAO/orbsvcs/orbsvcs \
    ${ACSROOT}/include \
    ${RTAI_HOME}/include \
    ${INTROOT}/include

LIBPATH += ${ALMASW_INSTDIR}/TAO/ACE_wrappers \
    ${ACE_ROOT}/lib \
    ${ACE_ROOT}/TAO/tao \
    ${ACSROOT}/lib \
    ${RTAI_HOME}/lib \
    ${INTROOT}/lib

LIBS += -lacsutil -lTAO_PortableServer

SOURCES += sigsegv_handler.cpp
