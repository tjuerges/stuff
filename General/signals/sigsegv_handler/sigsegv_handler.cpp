/**
 * $Id$
 */


#include <iostream>
#include <string>
#include <cstring>
#include <cerrno>
#include <signal.h>
#include <acsutilTimeStamp.h>


void sigSegvHandler(int signal, siginfo_t* info, void* data)
{
    const std::string myTimeStamp(::getStringifiedTimeStamp().c_str());

    std::cout << "Caught signal "
        << std::strerror(info->si_errno)
        << ", code = "
        << info->si_code
        << ".  Code interpretation = ";
    if(info->si_code == SEGV_MAPERR)
    {
        std::cout << "address not mapped to object";
    }
    else if(info->si_code == SEGV_ACCERR)
    {
        std::cout << "invalid permissions for mapped object";
    }
    else
    {
        std::cout << "unknown code";
    }

    std::cout << ".  Date/Time = "
        << myTimeStamp
        << "\n";
}


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    struct sigaction newAction;
    std::memset(&newAction, 0, sizeof(struct sigaction));

    newAction.sa_sigaction = sigSegvHandler;
    newAction.sa_flags = SA_SIGINFO | SA_RESETHAND;

    if(sigaction(SIGSEGV, &newAction, 0) != 0)
    {
        std::cout << "Error while installing the new signal handler: "
            << std::strerror(errno)
            << "\n";
    }

    std::cout << "Creating a SIGSEGV...\n";

    char* ptr = 0;
    std::cout << "This should not be printed but create a SIGSEGV."
        << *ptr
        << "\n";

    std::cout << "Signalhandler should have executed.\n";
    
    return 0;
}
