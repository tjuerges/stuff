#include <iostream>
#include <ctime>
#include <cstdlib>
#include <csignal>


class A
{
    public:
    A()
    {
        std::cout << __PRETTY_FUNCTION__
            << "\n";
    };

    ~A()
    {
        std::cout << __PRETTY_FUNCTION__
        << "\n";
    };
};


static void printIt()
{
    std::cout << __PRETTY_FUNCTION__
        << "\n";
}

static void signalHandler(int signal)
{
    std::cout << "Caught signal #"
        << signal
        << ".\n";
    exit(0);
}


int main(int a, char* b[])
{
    atexit(printIt);
    struct sigaction sa;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    sa.sa_handler = signalHandler;

    if(sigaction(SIGINT, &sa, NULL) == -1)
    {
        std::cout << "Could not install the CTRL-C handler.\n";
    }
    else if(sigaction(SIGTERM, &sa, NULL) == -1)
    {
        std::cout << "Could not install the SIGTERM handler.\n";
    }

    A foo;
    const struct timespec t = {1, 0};
    while(true)
    {
        std::cout << "Sleeping...\n";
        nanosleep(&t, NULL);
    };

    return 0;
}
