#
# $Id$
#

TEMPLATE = app
TARGET = testDefaultStackSize

LANGUAGE = C

DEFINES += _GNU_SOURCE

CONFIG -= qt

LIBS = -pthread -Wl,--as-needed $${LIBS}

QMAKE_LINK = gcc

QMAKE_CFLAGS_RELEASE += $$system(cflags.sh QMAKE_RELEASE_C)

QMAKE_CFLAGS_DEBUG += $$system(cflags.sh QMAKE_DEBUG_C)

#TEMPLATE = subdirs
#SUBDIRS += foo bar


SOURCES += testDefaultStackSize.c
