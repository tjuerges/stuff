/*
 * testDefaultStackSize.c
 *
 *  Created on: Oct 9, 2009
 *      Author: jschwarz
 */


#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <limits.h>


static const unsigned int MAXITEMS = 10U;


void printAttributes(pthread_t* thread)
{
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    size_t stackSize = 0U;
    if(thread == NULL)
    {
        const pthread_attr_t cattr = attr;
        pthread_attr_getstacksize(&cattr, &stackSize);
    }
    else
    {
        if(pthread_getattr_np(*thread, &attr) == 0)
        {
            const pthread_attr_t cattr = attr;
            void* stackAddr = NULL;
            pthread_attr_getstack(&cattr, &stackAddr, &stackSize);
        }
        else
        {
            return;
        }
    }

    printf("stack size: %fMB\n", ((double)stackSize) / 1e6);
}

static void* recursion(void* _i)
{
    if(_i == NULL)
    {
        return NULL;
    }

    unsigned int i = *((unsigned int*)(_i));
    if(i <= MAXITEMS)
    {
        unsigned int takeSpace[MAXITEMS];

        takeSpace[i] = i;
        ++i;
        size_t stackSize = 0U;
        pthread_t thread = pthread_self();
        printAttributes(&thread);
        recursion((void*)(&i));
    }

    return NULL;
}

int main(int y __attribute__((unused)), char* z[] __attribute__((unused)))
{
    printf("Default ");
    printAttributes(NULL);
    size_t stacksize = 100000U;
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setstacksize(&attr, stacksize);
    printf("Setting stack size to: %fMB\n", ((double)stacksize) / 1e6);

    pthread_t thread;
    unsigned int i = 0U;
    pthread_create(&thread, &attr, recursion, ((void*)(&i)));
    pthread_attr_destroy(&attr);

    pthread_join(thread, NULL);

    return 0;
}
