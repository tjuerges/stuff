#include <iostream>
#include <pthread.h>
#include <ctime>
#include <cerrno>
#include <cstring>
#include <sys/time.h>
// For ACE_Time_Value
#include <ace/Time_Value.h>
// For ACE_OS::select
#include <ace/OS_NS_sys_select.h>


void takeMutex(pthread_mutex_t& cmdFifoMtx_m)
{
    struct timeval currentTime, oldCurrentTime;
    struct timespec timeout;

    gettimeofday(&currentTime, NULL);
    timeout.tv_sec = currentTime.tv_sec + 5;
    timeout.tv_nsec = 0;

    int status(-1);
    while((status = pthread_mutex_timedlock(&cmdFifoMtx_m, &timeout)) != 0)
    {
        std::cout << timeout.tv_sec + timeout.tv_nsec / 1e9
            << ": "
            << status
            << ", "
            << std::strerror(errno)
            << ", ";

        oldCurrentTime = currentTime;
        gettimeofday(&currentTime, NULL);

        std::cout << (currentTime.tv_sec - oldCurrentTime.tv_sec) * 1000000 + (currentTime.tv_usec - oldCurrentTime.tv_usec)
            << "\n";

        timeout.tv_sec = currentTime.tv_sec + 5;
        timeout.tv_nsec = currentTime.tv_usec * 1000;        
    }

    std::cout << "Yo!\n";

    pthread_mutex_unlock(&cmdFifoMtx_m);

}

void takeMutex2(pthread_mutex_t& cmdFifoMtx_m)
{
    struct timeval currentTime, oldCurrentTime;

    gettimeofday(&currentTime, NULL);

    int status(-1);
    while((status = pthread_mutex_trylock(&cmdFifoMtx_m)) != 0)
    {
        std::cout << status
            << ", "
            << std::strerror(errno)
            << ", ";

        oldCurrentTime = currentTime;
        gettimeofday(&currentTime, NULL);

        std::cout << (currentTime.tv_sec - oldCurrentTime.tv_sec) * 1000000 + (currentTime.tv_usec - oldCurrentTime.tv_usec)
            << "\n";

        ACE_Time_Value timeout(5, 0);
        ACE_OS::select(0, 0, 0, 0, timeout);        
    }

    pthread_mutex_unlock(&cmdFifoMtx_m);

}

int main(int c __attribute__((unused)), char* v[] __attribute__((unused)))
{
    pthread_mutex_t cmdFifoMtx_m;

    pthread_mutex_init(&cmdFifoMtx_m, 0);
//			   reinterpret_cast<const pthread_mutexattr_t*>("fast"));    

    takeMutex(cmdFifoMtx_m);

    return 0;
}
