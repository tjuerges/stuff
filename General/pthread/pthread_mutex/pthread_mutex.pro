#
# $Id$
#

TEMPLATE = app
TARGET = pthread_mutex

LANGUAGE = C++

CONFIG += rtti stl exceptions debug warn_on
CONFIG -= qt

LIBS = -Wl,--as-needed $${LIBS}

INCLUDEPATH += $(ACE_ROOT)
INCLUDEPATH += $(ACE_ROOT)/ace
INCLUDEPATH += $(ACE_ROOT)/TAO
INCLUDEPATH += $(ACE_ROOT)/TAO/orbsvcs
INCLUDEPATH += $(ACE_ROOT)/TAO/orbsvcs/orbsvcs
INCLUDEPATH += $(ACSROOT)/include

LIBPATH += $(ALMASW_INSTDIR)/TAO/ACE_wrappers
LIBPATH += $(ACE_ROOT)/lib
LIBPATH += $(ACE_ROOT)/TAO/tao

LIBS += -lACE
LIBS += -lpthread
LIBS += -lrt

SOURCES += pthread_mutex.cpp
