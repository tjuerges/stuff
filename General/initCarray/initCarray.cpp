#include <iostream>

static char* foo()
{

}

int main(int, char**)
{
    char not_cleared[PATH_MAX];
    char cleared[PATH_MAX];
    std::memset(cleared, 0, PATH_MAX);
    char cleared_cpp[PATH_MAX]{};
    std::cout << "Not cleared: \""
        << not_cleared
        << "\"\ncleared: \""
        << cleared
        << "\"\ncleared_cpp: \""
        << cleared_cpp
        << "\"\n";

    return 0;
}
