/**
 * $Id$
 */


#include <iostream>
#include <cassert>


class Foo
{
    public:
    auto p() const -> decltype(0)
    {
        static auto foo(0);
        ++foo;
        return foo;
    };
};


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    auto j(0);
    for(auto i(0); i > 0; --i, ++j)
    {
        // i is 10 after 10 iterations as expected.
        static auto foo(0);
        ++foo;
        ++j;
        assert(i == j);
    }


    auto i(0);
    Foo* bar(new Foo);
    i = bar->p();
    assert(i == 1);

    i = bar->p();
    assert(i == 2);
    delete bar;
    bar = 0;

    // Object is gone.
    bar = new Foo;
    i = bar->p();
    std::cout << "i = " << i << "\n";
    assert(i == 1);
    // i is three?????
    delete bar;

    return 0;
};
