#
# $Id$
#

TEMPLATE = app

#TEMPLATE = subdirs
#SUBDIRS += foo bar

TARGET = static_inside_scope

LANGUAGE = C++

#DEFINES +=

#CONFIG += qt rtti stl exceptions debug warn_on 
#CONFIG += qt rtti stl exceptions release warn_on 
CONFIG += rtti stl exceptions debug warn_on
#CONFIG -= qt

#CONFIG += link_pkgconfig
#PKGCONFIG +=

LIBS = -Wl,--as-needed $${LIBS}

QMAKE_CXXFLAGS_RELEASE += -std=gnu++11
QMAKE_CXXFLAGS_DEBUG += -std=gnu++11

#INCLUDEPATH +=
#LIBPATH +=
#LIBS +=

#FORMS += static_inside_scope.ui
#HEADERS +=
SOURCES += static_inside_scope.cpp
