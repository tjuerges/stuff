#include <iostream>

int main(int, char**)
{
	std::cout << "Creating a new long*..." << std::endl;

	long* foo(new long);

	*foo = 42L;

	std::cout << "Value of the long = " << *foo << std::endl
	<< "Deleting foo and setting the pointer = 0..." << std::endl;

	delete foo;
	foo = 0;

	std::cout << "Deleting foo again..." << std::endl;

	delete foo;

	std::cout << "If the second deletion segfaulted, this should not be seen." << std::endl;

	return 0;
}
