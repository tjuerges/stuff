#include <iostream>

class foo
{
	public:
		foo():
			val(5)
		{
			std::cout << " I'm in the constructor, val (5?) = " << val << std::endl;
		};

		~foo()
		{
			val = 13;
			std::cout << " I'm in the destructor, val (13?) = " << val << std::endl;
		};

		int delete_me()
		{
			val = 42;
			std::cout << " I'm in delete_me, val (42?) = " << val << std::endl;

			delete this;

			val = 44;

			std::cout << " I'm in delete_me, after delete this. val (44?) = " << val << std::endl;

			return 42;
		};

	private:
		int val;
};

int main(int c, char* v[])
{
	int val(0);

	foo* bug_heap = new foo;
	val = bug_heap->delete_me();
	std::cout << "Return value of foo_heap = " << val << std::endl;

	val = 0;

	foo bug_stack;
	val = bug_stack.delete_me();
	std::cout << "Return value of foo_stack = " << val << std::endl;

	return 0;
}
