#
# $Id$
#

TEMPLATE = app
TARGET = atomicUserspace

LANGUAGE = C++

CONFIG += rtti stl exceptions warn_on debug
CONFIG -= qt

#QMAKE_LFLAGS += -g

QMAKE_CXXFLAGS_RELEASE += -g $$system(cflags.sh QMAKE_RELEASE_CXX) \
	-DCOMPILATION_DATE=\\\"`date --iso-8601=seconds`\\\" \
    -DVERSION=\\\"`./compilation_run`\\\"
QMAKE_CXXFLAGS_DEBUG += -g $$system(cflags.sh QMAKE_DEBUG_CXX) \
	-DCOMPILATION_DATE=\\\"`date --iso-8601=seconds`\\\" \
    -DVERSION=\\\"`./compilation_run`\\\"

QMAKE_CFLAGS_RELEASE += -g $$system(cflags.sh QMAKE_RELEASE_C) \
	-DCOMPILATION_DATE=\\\"`date --iso-8601=seconds`\\\" \
    -DVERSION=\\\"`./compilation_run`\\\"
QMAKE_CFLAGS_DEBUG += -g $$system(cflags.sh QMAKE_DEBUG_C) \
	-DCOMPILATION_DATE=\\\"`date --iso-8601=seconds`\\\" \
    -DVERSION=\\\"`./compilation_run`\\\"

SOURCES += \
	atomicUserspace.cpp \ 

