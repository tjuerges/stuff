/**
 * $Id$
 */


#include <iostream>
#include <asm/atomic.h>


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{

    /**
     * This uses the Linux Kernel types.
     * Usually results in a warning that kernel include files
     * should not be used in user space.
     */
    atomic_t foo = ATOMIC_INIT(0);
    std::cout << atomic_read(&foo) << "\n";
    atomic_set(&foo, 15);
    std::cout << atomic_read(&foo) << "\n";

    /**
     * This uses the GCC specific built-ins to accomplish the same.
     * See http://gcc.gnu.org/onlinedocs/gcc-4.1.2/gcc/Atomic-Builtins.html#Atomic-Builtins
     * a full list of atomic operations.
     */
    int bar(0);
    std::cout << __sync_fetch_and_add (&bar, 0) << "\n";
    __sync_add_and_fetch(&bar, 15);
    std::cout << __sync_fetch_and_add (&bar, 0) << "\n";

    return 0;
};
