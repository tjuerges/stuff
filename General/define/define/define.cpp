#include <iostream>

typedef int int1;
typedef int int2;

#define doof(a, b) std::cout << "Erstes Argument (#a) = " << #a << ", zweites Argument (#b) = " << #b << "\na=" << a << ", b=" << b << "\n";

#define doof2(a, b) std::cout << "Erstes Argument (#a) = " << #a << ", zweites Argument (#b) = " << #b << "\n(int##a)(10.5) = " << (int##a)(10.5) << ", (int##b)(10.5) = " << (int##b)(10.5) << "\n";

int main(int _a __attribute__((unused)), char* _b[] __attribute__((unused)))
{
    const std::string ich("Martin"), du("Thomas");
    const int i1(1), i2(2);

    std::cout << "\nVariablen: std::string ich, du\n\n"
        "Aufruf: doof(ich, du);\n";
    doof(ich,du);

    std::cout << "\ntypedef int int1;\n"
        "typedef int int2;\n"
        "Aufruf: doof2(1, 2)\n";
    doof2(1, 2);

    return 0;
}

