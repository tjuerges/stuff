#include <iostream>

#define ACS_MONITOR_C class TCORBA, class TCORBA_out, class TCB, class TPOA, BACIValue::Type TBACIValuetype

#define ACS_MONITOR_T TCORBA, TCORBA_out, TCB, TPOA, TBACIValuetype

#define ACS_MONITOR_SEQ(T,TCORBA)  TCORBA, TCORBA##_out, ACS::CB##T##Seq, POA_ACS::Monitor##T, BACIValue::type_##T##Seq

#define ACS_MONITOR(T,TCORBA)  TCORBA##_out, ACS::CB##T,POA_ACS::Monitor##T, BACIValue::type_##T

ACS_MONITOR(int,double)
int main(int,char**)
{
  return 0;
}
