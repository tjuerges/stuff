# $Id$

TEMPLATE = app
TARGET = define_undefined

LANGUAGE = C++

;;CONFIG += qt rtti stl warn_on release
CONFIG += rtti stl warn_on release

QMAKE_CXXFLAGS_RELEASE = -g \
	-O2 \
	-DCOMPILATION_DATE=\"`date --iso-8601=seconds`\" \
	-DVERSION=\"`./compilation_run`\"
;;	-march=pentium4m \
;;	-mfpmath=sse,387 \
;;	-mmmx \
;;	-m3dnow \
;;	-msse \
;;	-msse2

;;INCLUDEPATH += /alma/ACS-6.0/TAO/ACE_wrappers/build/linux \
;;	/alma/ACS-6.0/TAO/ACE_wrappers/build/linux/ace \
;;	/alma/ACS-6.0/TAO/ACE_wrappers/build/linux/TAO \
;;	/alma/ACS-6.0/TAO/ACE_wrappers/build/linux/TAO/orbsvcs \
;;	/alma/ACS-6.0/TAO/ACE_wrappers/build/linux/TAO/orbsvcs/orbsvcs \
;;	/alma/ACS-6.0/ACSSW/include \
;;	/export/home/delphinus/tjuerges/workspace/introot.ICD/include \
;;	/export/home/delphinus/tjuerges/workspace/introot.CONTROL/include\ 
;;	/export/home/delphinus/tjuerges/workspace/introot/include

;;LIBPATH += /alma/ACS-6.0/TAO/ACE_wrappers/build/linux/ace \
;;	/alma/ACS-6.0/TAO/ACE_wrappers/build/linux/TAO/tao \
;;	/alma/ACS-6.0/ACSSW/lib \
;;	/export/home/delphinus/tjuerges/workspace/introot.ICD/lib \
;;	/export/home/delphinus/tjuerges/workspace/introot.CONTROL/lib \
;;	/export/home/delphinus/tjuerges/workspace/introot/lib

;;LIBS += -lACE \
;;	-lloki \
;;	-lacsutil \
;;	-lacscommonStubs \
;;	-lbaselogging \
;;	-llogging

;;TEMPLATE = subdirs
;;SUBDIRS += foo bar

;;FORMS += define_undefined.ui
HEADERS += 

SOURCES += define_undefined.cpp
