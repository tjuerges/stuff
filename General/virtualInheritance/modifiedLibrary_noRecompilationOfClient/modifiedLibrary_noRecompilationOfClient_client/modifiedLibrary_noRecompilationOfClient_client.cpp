/**
 * $Id$
 */


#include <iostream>
#include <string>
#include <vector>
#include <sstream>

#include "modifiedLibrary_noRecompilationOfClient_client.h"


Bar::Bar()
{
    std::cout << __PRETTY_FUNCTION__
        << "\n";
}

Bar::~Bar()
{
    std::cout << __PRETTY_FUNCTION__
        << "\n";
}

void Bar::print1() const
{
    std::cout << __PRETTY_FUNCTION__
        << "\n";
}


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    Bar bar;
    bar.print1();

    return 0;
}
