#
# $Id$
#

TEMPLATE = app
TARGET = modifiedLibrary_noRecompilationOfClient_client

LANGUAGE = C++

CONFIG += rtti stl exceptions debug warn_on
CONFIG -= qt

LIBS = -Wl,--as-needed $${LIBS}

INCLUDEPATH += $$(PWD)/../modifiedLibrary_noRecompilationOfClient_lib

LIBPATH += $$(PWD)/../modifiedLibrary_noRecompilationOfClient_lib

LIBS += -Wl,-rpath,$$(PWD)/../modifiedLibrary_noRecompilationOfClient_lib -lmodifiedLibrary_noRecompilationOfClient_lib

HEADERS += modifiedLibrary_noRecompilationOfClient_client.h

SOURCES += modifiedLibrary_noRecompilationOfClient_client.cpp
