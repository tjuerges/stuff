#ifndef modifiedLibrary_noRecompilationOfClient_client_h
#define modifiedLibrary_noRecompilationOfClient_client_h
//
// $Id$
//


#include "modifiedLibrary_noRecompilationOfClient_lib.h"


class Bar: public Foo
{
    public:
    Bar();
    virtual ~Bar();
    virtual void print1() const;
};
#endif
