//
// $Id$
//


#include <iostream>
#include <string>
#include <vector>
#include <sstream>

#include "modifiedLibrary_noRecompilationOfClient_lib.h"


Foo::Foo()
{
    std::cout << __PRETTY_FUNCTION__
        << "\n";
}

Foo::~Foo()
{
    std::cout << __PRETTY_FUNCTION__
        << "\n";
}

void Foo::print1() const
{
    std::cout << __PRETTY_FUNCTION__
        << "\n";
}

#ifdef ADD_METHOD
void Foo::print2() const
{
    std::cout << __PRETTY_FUNCTION__
        << "\n";
}
#endif


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    Foo yo;
    yo.print1();

    return 0;
}
