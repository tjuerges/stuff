#ifndef modifiedLibrary_noRecompilationOfClient_lib_h
#define modifiedLibrary_noRecompilationOfClient_lib_h
//
// $Id$
//


class Foo
{
    public:
    Foo();
    virtual ~Foo();
    virtual void print1() const;
    #ifdef ADD_METHOD
    virtual void print2() const;
    #endif
};
#endif
