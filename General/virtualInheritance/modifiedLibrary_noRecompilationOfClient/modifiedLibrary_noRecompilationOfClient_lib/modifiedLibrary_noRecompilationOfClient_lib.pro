#
# $Id$
#

TEMPLATE = lib
TARGET = modifiedLibrary_noRecompilationOfClient_lib

LANGUAGE = C++

CONFIG += rtti stl exceptions debug warn_on
CONFIG -= qt

#DEFINES += ADD_METHOD

LIBS = -Wl,--as-needed $${LIBS}

HEADERS += modifiedLibrary_noRecompilationOfClient_lib.h

SOURCES += modifiedLibrary_noRecompilationOfClient_lib.cpp
