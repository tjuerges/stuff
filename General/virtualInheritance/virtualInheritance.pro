#
# $Id$
#


TEMPLATE = subdirs
SUBDIRS = multipleInheritance
SUBDIRS += virtualAndNonVirtualDestructor
SUBDIRS += publicInheritance
SUBDIRS += polymorph
SUBDIRS += inheritanceChainWithMissingImplementation
SUBDIRS += modifiedLibrary_noRecompilationOfClient
