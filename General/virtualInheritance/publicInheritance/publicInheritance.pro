# $Id$

TEMPLATE = app
TARGET = publicInheritance

LANGUAGE = C++

#CONFIG = qt rtti stl warn_on release
CONFIG = rtti stl warn_on

QMAKE_CXXFLAGS_RELEASE += -g $$system(cflags.sh QMAKE_RELEASE)
QMAKE_CXXFLAGS_DEBUG += -g $$system(cflags.sh QMAKE_DEBUG)

#INCLUDEPATH +=  \
#    /ace \
#    /TAO \
#    /TAO/orbsvcs \
#    /TAO/orbsvcs/orbsvcs \
#    /include \
#    /../rtai/include \
#    /home/thomas/workspace/introot.ICD/include \
#    /home/thomas/workspace/introot.CONTROL/include\
#    /include

#LIBPATH += /ace \
#    /TAO/tao \
#    /lib \
#    /../rtai/lib \
#    /home/thomas/workspace/introot.ICD/lib \
#    /home/thomas/workspace/introot.CONTROL/lib \
#    /lib

#LIBS += -lACE \
#    -lloki \
#    -lacsutil \
#    -lacscommonStubs \
#    -lbaselogging \
#    -llogging

#TEMPLATE = subdirs
#SUBDIRS += foo bar

#FORMS += publicInheritance.ui

HEADERS += \
	publicInheritance.h

SOURCES += \
	publicInheritance.cpp

