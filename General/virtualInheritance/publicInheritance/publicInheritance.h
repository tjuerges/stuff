/**
 * "@(#) $Id$"
 */

#ifndef publicInheritance_h
#define publicInheritance_h
#include <iostream>

class Foo;

class VirtualBase
{
	friend class Foo;

	public:
	VirtualBase():
		value(-1)
	{
		std::cout << "Constructor of VirtualBase" << std::endl;
		print();
	};

	virtual ~VirtualBase()
	{
		std::cout << "Destructor of VirtualBase" << std::endl;
	};

	private:
	void set(const int a)
	{
		value = a;
	};

	void print()
	{
		std::cout << "Value = " << value << std::endl;
	};

	int value;
};

class Foo: public VirtualBase
{
	public:
	Foo()
	{
		std::cout << "Constructor of Foo" << std::endl;
		set(15);
	};

	virtual ~Foo()
	{
		print();
		std::cout << "Destructor of Foo" << std::endl;
	};
};

#endif
