/**
 * $Id$
 *
 * Deriving from a class with a virtual destructor makes the derived classes'
 * destructor automatically virtual.
 * As soon as the base class has a virtual member method but no virtual
 * destructor, the destructor of the derived class won't be ever called.
 */

#include <iostream>
#include <string>


class Foo
{
	public:
	Foo()
	{
		std::cout << "Foo created." << std::endl;
	};

	virtual ~Foo()
	{
		std::cout << "Foo destroyed." << std::endl;
	};

	virtual void print(const std::string& text) const
	{
		std::cout << text << std::endl;
	};
};

class BarVirtual: public Foo
{
	public:
	BarVirtual()
	{
		print("BarVirtual created.");
	};

	virtual ~BarVirtual()
	{
		print("BarVirtual destroyed.");
	};
};

class BarNonVirtual: public Foo
{
	public:
	BarNonVirtual()
	{
		print("BarNonVirtual created.");
	};

	~BarNonVirtual()
	{
		print("BarNonVirtual destroyed.");
	};
};

class FooNoVirtualDestructor
{
	public:
	FooNoVirtualDestructor()
	{
		std::cout << "FooNoVirtualDestructor created." << std::endl;
	};

	~FooNoVirtualDestructor()
	{
		std::cout << "FooNoVirtualDestructor destroyed." << std::endl;
	};

	virtual void print(const std::string& text) const
	{
		std::cout << text << std::endl;
	};
};


class BarVirtualDerivedFromFooNoVirtualDestructor: public FooNoVirtualDestructor
{
	public:
	BarVirtualDerivedFromFooNoVirtualDestructor()
	{
		print("BarVirtualDerivedFromFooNoVirtualDestructor created.");
	};

	virtual ~BarVirtualDerivedFromFooNoVirtualDestructor()
	{
		print("BarVirtualDerivedFromFooNoVirtualDestructor destroyed.");
	};
};

class BarNonVirtualDerivedFromFooNoVirtualDestructor: public FooNoVirtualDestructor
{
	public:
	BarNonVirtualDerivedFromFooNoVirtualDestructor()
	{
		print("BarNonVirtualDerivedFromFooNoVirtualDestructor created.");
	};

	~BarNonVirtualDerivedFromFooNoVirtualDestructor()
	{
		print("BarNonVirtualDerivedFromFooNoVirtualDestructor destroyed.");
	};
};

int main(int argc, char* argv[])
{
	std::cout << "Creating a Foo containing a BarVirtual..." << std::endl;
	Foo* one(new BarVirtual);

	std::cout << "Creating a Foo containing a BarNonVirtual..." << std::endl;
	Foo* two(new BarNonVirtual);

	std::cout << "Destroying Foo containing a BarVirtual..." << std::endl;
	delete one;

	std::cout << "Destroying Foo containing a BarNonVirtual..." << std::endl;
	delete two;

	std::cout << std::endl << std::endl;

	std::cout << "Creating a FooNoVirtualDestructor containing a BarVirtualDerivedFromFooNoVirtualDestructor..." << std::endl;
	FooNoVirtualDestructor* three(new BarVirtualDerivedFromFooNoVirtualDestructor);

	std::cout << "Creating a FooNoVirtualDestructor containing a BarNonVirtualDerivedFromFooNoVirtualDestructor..." << std::endl;
	FooNoVirtualDestructor* four(new BarNonVirtualDerivedFromFooNoVirtualDestructor);

	std::cout << "Destroying FooNoVirtualDestructor containing a BarVirtualDerivedFromFooNoVirtualDestructor..." << std::endl;
	delete three;

	std::cout << "Destroying FooNoVirtualDestructor containing a BarNonVirtualDerivedFromFooNoVirtualDestructor..." << std::endl;
	delete four;

	return 0;
}
