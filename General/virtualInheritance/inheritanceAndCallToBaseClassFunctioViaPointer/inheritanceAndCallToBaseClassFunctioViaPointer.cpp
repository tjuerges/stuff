/**
 * $Id$
 */


#include <iostream>
#include <string>
#include <vector>
#include <sstream>

#include "inheritanceAndCallToBaseClassFunctioViaPointer.h"


One::One():
    external(new External(this, &One::getPtr))
{
    std::cout << "One ctor\n";
}

One::~One()
{
    std::cout << "One dtor\n";
    delete external;
}

void One::callMe()
{
    std::cout << "One callMe\n";
}

MemberFunction One::getPtr()
{
    std::cout << "One getPtr\n";
    return &One::callMe;
}

void One::execExternal()
{
    std::cout << "One execExternal\n";
    external->doIt();
}


Two::Two():
    One()
{
    std::cout << "Two ctor\n";
};

Two::~Two()
{
    std::cout << "Two dtor\n";
};

void Two::callMe()
{
    std::cout << "Two callMe\n";
};

MemberFunction Two::getPtr()
{
    std::cout << "Two getPtr\n";
    return reinterpret_cast< MemberFunction >(&Two::callMe);
}


External::External(One* _one, GetPointerFunction arg):
    one(_one),
    getPtr(arg),
    extDoIt((one->*getPtr)())
{
    std::cout << "External ctor\n";
};

External::~External()
{
    std::cout << "External dtor\n";
};

void External::doIt()
{
    std::cout << "External doIt\n";
    (one->*extDoIt)();
};


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    std::cout << "\n\n";

    One one;
    one.execExternal();

    std::cout << "\n\n";

    Two two;
    two.execExternal();

    std::cout << "\n\n";

    return 0;
};
