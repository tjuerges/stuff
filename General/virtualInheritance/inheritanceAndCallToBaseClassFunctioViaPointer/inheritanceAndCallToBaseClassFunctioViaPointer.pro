#
# $Id$
#

TEMPLATE = app
TARGET = inheritanceAndCallToBaseClassFunctioViaPointer

LANGUAGE = C++

CONFIG += rtti stl exceptions debug warn_on
CONFIG -= qt


HEADERS += inheritanceAndCallToBaseClassFunctioViaPointer.h

SOURCES += inheritanceAndCallToBaseClassFunctioViaPointer.cpp
