#ifndef inheritanceAndCallToBaseClassFunctioViaPointer_h
#define inheritanceAndCallToBaseClassFunctioViaPointer_h
/**
 * $Id$
 */


class One;
class External;


typedef void (One::* MemberFunction)();
typedef MemberFunction (One::* GetPointerFunction)();


class One
{
    public:

    One();

    virtual ~One();

    virtual void callMe();

    virtual MemberFunction getPtr();

    virtual void execExternal();


    protected:
    External* external;


    private:
    One(const One&);
    One& operator=(const One&);
};


class Two: public One
{
    public:
    Two();

    virtual ~Two();

    virtual void callMe();

    virtual MemberFunction getPtr();


    private:
    Two(const Two&);
    Two& operator=(const Two&);
};


class External
{
    public:

    External(One* _one, GetPointerFunction arg);

    virtual ~External();

    void doIt();


    private:
    External(const External&);
    External& operator=(const External&);


    One* one;
    GetPointerFunction getPtr;
    MemberFunction extDoIt;
};
#endif
