#include <iostream>
#include <typeinfo>

class Vater
{
	public:
	virtual ~Vater(void)
	{
		std::cout << "Destruktor Vater" << std::endl;
	};

	virtual void print(void) const
	{
  		std::cout << "print Vater" << std::endl;
	}
};

class Sohn: public Vater
{
	public:
	virtual ~Sohn(void)
	{
		std::cout<<"Destruktor Sohn"<<std::endl;
	};

	virtual void print(void) const
	{
  		std::cout << "print Sohn" << std::endl;
	}
};

int main(int argc,char* argv[])
{
	Vater v;
	Sohn s;
	Vater* d = &s;

	std::cout << "Vater v:" <<typeid(v).name() << std::endl;
	std::cout << "Sohn s:" <<typeid(s).name() << std::endl;
	std::cout << "Vater* d=&s:" <<typeid(*d).name() << std::endl;

	if(typeid(s) == typeid(Vater))
	{
		std::cout << "s ist Vater-Objekt" << std::endl;
	}
	else
	{
	std::cout << "s ist Sohn-Objekt" << std::endl;
	}

	if(typeid(d) == typeid(Vater))
	{
		std::cout << "d ist Vater-Objekt" << std::endl;
	}
	else
	{
		std::cout << "d ist Sohn-Objekt" << std::endl;
	}

	v.print();
	s.print();
	d->print();

	return 0;
}
