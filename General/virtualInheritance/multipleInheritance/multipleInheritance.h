/**
 * "@(#) $Id$"
 */

#ifndef multipleInheritance_h
#define multipleInheritance_h
#include <iostream>

class VirtualBase
{
	public:
	VirtualBase(double param):
		value(param)
	{
		std::cout << "Constructor of VirtualBase" << std::endl;
		print();
	};

	virtual ~VirtualBase()
	{
		std::cout << "Destructor of VirtualBase" << std::endl;
	};

	void set(const double a)
	{
		value = a;
	};

	void print()
	{
		std::cout << "Value = " << value << std::endl;
	};

	protected:
	double value;
    
    private:
    VirtualBase();
};

class Foo: public virtual VirtualBase
{
	public:
	Foo(double param):
        VirtualBase(param)
	{
		std::cout << "Constructor of Foo" << std::endl;
	};

	virtual ~Foo()
	{
		std::cout << "Destructor of Foo" << std::endl;
	};

    private:
    Foo();
};

class Bar: public virtual VirtualBase
{
	public:
	Bar(double param):
        VirtualBase(param)
	{
		std::cout << "Constructor of Bar" << std::endl;
	};

	virtual ~Bar()
	{
		std::cout << "Destructor of Bar" << std::endl;
	};

    private:
    Bar();
};

class Mixed: public Foo, public Bar
{
	public:
	Mixed():
        VirtualBase(-1.0),
        Foo(-1.0),
        Bar(-1.0)
	{
		std::cout << "Constructor of Mixed" << std::endl;
	};

    Mixed(double param):
        VirtualBase(param),
        Foo(param),
        Bar(param)
    {
        std::cout << "2nd constructor of Mixed" << std::endl;
    };

	virtual ~Mixed()
	{
		std::cout << "Destructor of Mixed" << std::endl;
	};
};

class seppi: public Mixed
{
    public:
    seppi():
        VirtualBase(1.0),
        Mixed(1.0)
    {
    };

    virtual ~seppi()
    {
		std::cout << "Destructor of seppi" << std::endl;
    };
};
#endif
