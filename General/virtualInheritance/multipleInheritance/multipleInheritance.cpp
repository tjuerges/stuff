/**
 * "@(#) $Id$"
 */

#include <iostream>
#include "multipleInheritance.h"


int main(int a __attribute__((unused)), char* ab[] __attribute__((unused)))
{
	VirtualBase vb(1.0);
	vb.set(10);
	std::cout << "VirtualBases' value:\n";
	vb.print();

	Foo foo(1.0);
	foo.set(20);
	std::cout << "\n\nFoo's value:\n";
	foo.print();

	Bar bar(1.0);
	bar.set(-20);
	std::cout << "\n\nBar's value:\n";
	bar.print();
	std::cout << "\n\nFoo's value:\n";
	foo.print();

	Mixed mixed(1.0);
	mixed.set(1000);
	std::cout << "Mixed's value:\n";
	mixed.print();
	std::cout << "Bar's value:\n";
	bar.print();
	std::cout << "Foo's value:\n";
	foo.print();

	seppi s;
	s.set(1000.0);
	std::cout << "seppi's value:\n";
	s.print();
	std::cout << "Bar's value:\n";
	bar.print();
	std::cout << "Foo's value:\n";
	foo.print();

    return 0;
};
