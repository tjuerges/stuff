TEMPLATE = app
TARGET = multipleInheritance

LANGUAGE = C++

CONFIG = rtti stl warn_on

HEADERS += multipleInheritance.h

SOURCES += multipleInheritance.cpp

