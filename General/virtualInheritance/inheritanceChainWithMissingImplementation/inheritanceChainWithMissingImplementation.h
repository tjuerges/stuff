#ifndef inheritanceChainWithMissingImplementation_h
#define inheritanceChainWithMissingImplementation_h
/**
 * $Id$
 */

#include <iostream>


class One
{
	public:
	virtual ~One()
	{
	};

	virtual void foo() const
	{
	};
};


class Two: public One
{
};


class Three: public Two
{
	public:
	virtual void foo() const
	{
		std::cout << "Yo, Three did it." << std::endl << std::endl;
	};
};

#endif
