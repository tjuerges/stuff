/**
 * $Id$
 */

#include <iostream>
#include <string>
#include <vector>
#include <sstream>

#include "inheritanceChainWithMissingImplementation.h"


int main(int argc, char* argv[])
{
    std::cout << "Compiled on "
        << COMPILATION_DATE
        << ", version "
        << VERSION
        << "."
        << std::endl;

	std::cout << "One* foo(new One);" << std::endl;
	One* foo(new One);
	foo->foo();
	delete foo;

	std::cout << "foo = new Two;" << std::endl;
	foo = new Two;
	foo->foo();
	delete foo;

	std::cout << "foo = new Three;" << std::endl;
	foo = new Three;
	foo->foo();
	delete foo;

    return 0;
};
