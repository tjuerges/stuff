#
# $Id$
#

TEMPLATE = app
TARGET = proxyTest

LANGUAGE = C++

CONFIG += rtti stl exceptions debug warn_on
CONFIG -= qt

QMAKE_CXXFLAGS_DEBUG += -O0
QMAKE_LFLAGS_DEBUG += -O0

LIBS += -lrt
LIBS += -lpthread

HEADERS += proxyTest.h
HEADERS += Utils.h
HEADERS += Utils.i

SOURCES += proxyTest.cpp
