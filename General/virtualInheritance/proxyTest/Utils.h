#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef Utils_h
#define Utils_h


#include <pthread.h>


namespace Foo
{
    template< typename Class, typename returnType >
    class Cache
    {
        /// Typedef for functor.
        typedef returnType (Class::*funcType)();

        public:
        /// Constructor.
        Cache(Class* base, funcType pm);

        /// Destructor.
        ~Cache();

        /// The function pointer will be called.
        void executeCacheFunction();


        private:
        /// Pointer for the AMB device.
        Class* device;

        /// Functor that reads values from the hardware.
        funcType method;

        /// Mutex for protection of \ref current and \ref last_id.
        pthread_mutex_t accessMutex;
    };
};

// Include the implementations of the template classes.
#include <Utils.i>
#endif
