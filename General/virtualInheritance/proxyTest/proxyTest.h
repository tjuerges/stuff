#ifndef proxyTest_h
#define proxyTest_h
/**
 * $Id$
 */


#include <Utils.h>
#include <boost/shared_ptr.hpp>


class Base
{
    public:
    Base();
    virtual ~Base();
    virtual void proxy();
    virtual void myFunction1();
    virtual void myFunction2();

    private:
    boost::shared_ptr< Foo::Cache< Base, void > > cacheFunction;
};

class Impl: public Base
{
    public:
    Impl();
    virtual ~Impl();
    virtual void myFunction1();
};
#endif
