#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef Utils_i
#define Utils_i

#include <sstream>
#include <exception>
#include <pthread.h>


template< typename Class, typename ParentType >
Foo::Cache< Class, ParentType >::Cache(
    Class* base, funcType pm):
    device(base),
    method(pm)
{
    std::cout << __PRETTY_FUNCTION__
        << "\n";

    if(pthread_mutex_init(&accessMutex, 0) != 0)
    {
        std::cout << __PRETTY_FUNCTION__
            << "Cannot create a mutex.\n";
        throw std::exception();
    }

    int foo(0);
    const char* realName(abi::__cxa_demangle(typeid(device).name(), 0, 0, &foo));
    std::cout << "\t"
        << __PRETTY_FUNCTION__
        << ": device class = "
        << realName
        << "\n";

    std::cout << "\n";
}

template< typename Class, typename ParentType >
Foo::Cache< Class, ParentType >::~Cache()
{
    pthread_mutex_destroy(&accessMutex);
}

template< typename Class, typename ParentType >
void Foo::Cache< Class, ParentType >::executeCacheFunction()
{
    std::cout << __PRETTY_FUNCTION__
        << "\n";

    // Safe guarding the access of the cached values.
    pthread_mutex_lock(&accessMutex);

    int foo(0);
    char* realName(abi::__cxa_demangle(typeid(device).name(), 0, 0, &foo));
    std::cout << "\t"
        << __PRETTY_FUNCTION__
        << ": device class = "
        << realName
        << "\n";

    realName = abi::__cxa_demangle(typeid(method).name(), 0, 0, &foo);
    std::cout << "\t"
        << __PRETTY_FUNCTION__
        << ": method = "
        << realName
        << "\n";

    std::cout << "\t"
        << __PRETTY_FUNCTION__
        << ": Address class = "
        << device
        << "\n";

    std::cout << "\t"
        << __PRETTY_FUNCTION__
        << ": Address method = "
        << method
        << "\n";

    std::cout << "\t"
        << __PRETTY_FUNCTION__
        << ": Calling (device->*method)()\n";

    (device->*method)();

    pthread_mutex_unlock(&accessMutex);

    std::cout << "\t"
        << __PRETTY_FUNCTION__
        << ": Done\n";

    std::cout << "\n";
}
#endif
