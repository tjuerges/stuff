/**
 * $Id$
 */


#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <cxxabi.h>
#include <boost/shared_ptr.hpp>

#include "proxyTest.h"


Base::Base()
{
    std::cout << __PRETTY_FUNCTION__
        << "\n";

    cacheFunction.reset(new Foo::Cache< Base, void >(
        this, &Base::proxy));

    int foo(0);
    const char* realName(abi::__cxa_demangle(typeid(this).name(), 0, 0, &foo));
    std::cout << "\t"
        << __PRETTY_FUNCTION__
        << ": I am a "
        << realName
        << "\n";

    std::cout << "\n";
}

Base::~Base()
{
    std::cout << __PRETTY_FUNCTION__
        << "\n";

    std::cout << "\n";
}

void Base::proxy()
{
    std::cout << __PRETTY_FUNCTION__
        << "\n";

    Base::myFunction1();

    std::cout << "\n";
}

void Base::myFunction1()
{
    std::cout << __PRETTY_FUNCTION__
        << "\n";

    typedef void (Base::* funcType)();
    const funcType foo(&Base::myFunction1);

    std::cout.setf(
        std::ios::showbase | std::ios::scientific, std::ios::floatfield);
    std::cout << "\t"
        << __PRETTY_FUNCTION__
        << ": My address = "
        << this
        << "\n\tAddress of Base::myFunction1 = "
        << foo
        << "\n";

    std::cout << "\n";
}

void Base::myFunction2()
{
    std::cout << __PRETTY_FUNCTION__
        << "\n";

    std::cout << "\t"
        << __PRETTY_FUNCTION__
        << ": Calling cacheFunction->executeCacheFunction()\n";
    cacheFunction->executeCacheFunction();

    std::cout << "\t"
        << __PRETTY_FUNCTION__
        << ": Done\n";

    std::cout << "\n";
}


Impl::Impl():
    Base()
{
    std::cout << __PRETTY_FUNCTION__
        << "\n";

    int foo(0);
    char* realName(abi::__cxa_demangle(typeid(this).name(), 0, 0, &foo));
    std::cout << "\t"
        << __PRETTY_FUNCTION__
        << ": I am a "
        << realName
        << "\n";

    std::cout << "\n";
}

Impl::~Impl()
{
    std::cout << __PRETTY_FUNCTION__
        << "\n";

    std::cout << "\n";
}

void Impl::myFunction1()
{
    std::cout << __PRETTY_FUNCTION__
        << "\n";

    typedef void (Impl::* funcType)();
    const funcType foo(&Impl::myFunction1);

    std::cout.setf(
        std::ios::showbase | std::ios::scientific, std::ios::floatfield);
    std::cout << "\t"
        << __PRETTY_FUNCTION__
        << ": My address = "
        << this
        << "\n\tAddress of Impl::myFunction1 = "
        << foo
        << "\n";

    std::cout << "\t"
        << __PRETTY_FUNCTION__
        << ": Calling Base::myFunction1()\n";
    Base::myFunction1();

    std::cout << "\t"
        << __PRETTY_FUNCTION__
        << ": Calling myFunction2()\n";
    myFunction2();

    std::cout << "\t"
        << __PRETTY_FUNCTION__
        << ": Done\n";

    std::cout << "\n";
}


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    std::cout << __PRETTY_FUNCTION__
        << "\n";

    Base* willy(new Impl);

    std::cout << "\t"
        << __PRETTY_FUNCTION__
        << ": Calling willy->myFunction1()\n";
    willy->myFunction1();

    delete willy;

    return 0;
}
