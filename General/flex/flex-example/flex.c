%{
/*
c
5
0
0
0000

{STATUS}  printf("got status: \"%s\"\n", yytext);
{CURRENT}  printf("got current: \"%s\"\n", yytext);
{POSITION}  printf("got position: \"%s\"\n", yytext);
{AXIS}  printf("got axis: \"%s\"\n", yytext);
*/
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>


void gotBegin(const char* text);
void gotStatus(const char* text);
void zeroPosReached(const char* text);
%}

%option noyywrap

CR  \r
LF  \n
NL  (({CR}|{LF})+({CR}|{LF})*)+
JUNK    ([[:alpha:]]*|[[:digit:]]*)*
JUNK_INIT  ([[:alnum:]]{-}[pt])*
BEGIN   c{NL}
AXIS   [[:digit:]]{1}{NL}
POSITION   [[:digit:]]+{NL}
CURRENT [[:digit:]]+{NL}
STATUS [[:digit:]]{4}{NL}
COMPLETE_STATUS {BEGIN}{AXIS}{POSITION}{CURRENT}{STATUS}
INIT_DONE   {JUNK_INIT}t{JUNK_INIT}{NL}{JUNK_INIT}p{JUNK_INIT}{NL}{JUNK_INIT}t{JUNK_INIT}{NL}
%%
{INIT_DONE} zeroPosReached(yytext);
{COMPLETE_STATUS}    gotStatus(yytext);
{BEGIN}{AXIS}{POSITION}{CURRENT}    gotStatus(yytext);
{BEGIN}{AXIS}{POSITION}    gotStatus(yytext);
{BEGIN}{AXIS}    gotStatus(yytext);
{BEGIN} gotBegin(yytext);
.   ;
%%


int main(int y, char* z[])
{
    ++z, --y;  /* skip over program name */
    int fd = -1, fdDuplicate = -1; 
    if(y > 0)
    {
        if((fd = open(z[0], O_RDONLY)) >= 0)
        {
            fdDuplicate = dup(fd);
            yyin = fdopen(fdDuplicate, "r");
        }
        else
        {
            printf("Cannot open the file %s.\n", z[0]);
            return -ENOENT;
        }
    }
    else
    {
        yyin = stdin;
    }

    yylex();

    close(fd);

    return 0;
}

void gotBegin(const char* text)
{
#ifdef DEBUG
    printf("Received \"c\": \"%s\"\n", text);
#endif
}

void gotStatus(const char* string)
{
    char* text = strdup(string + (strchr(string, 'c') - string) + 1);
    char* savePtr = NULL;
    const char* separator = "\r\n";
    char* index = strtok_r(text, separator, &savePtr);
    unsigned item = 0U;
    while(index != NULL)
    {
        if((*index == '\r') || (*index == '\n'))
        {
            index = strtok_r(NULL, separator, &savePtr);
            continue;
        }

#ifdef DEBUG
        printf("index = \"%s\"\n", index);
#endif
        switch(item)
        {
            case 0U:
            {
                printf("Axis = %d.\n", ((unsigned char)(*index) - '0'));
                ++item;
            }
            break;

            case 1U:
            {
                long position = strtol(index, NULL, 10);
                printf("Position = %ld.\n", position);
                ++item;
            }
            break;

            case 2U:
            {
                double current = (double)(strtol(index, NULL, 10)) / 1e3;
                printf("Current = %f.\n", current);
                ++item;
            }
            break;

            case 3U:
            {
                printf("Status = %s.\n", index);
                ++item;
            }
            break;

            default:
            break;
        }

        index = strtok_r(NULL, separator, &savePtr);
    }

    free(text);
}

void zeroPosReached(const char* text)
{
    printf("*** Home position reached ***\n");
}

