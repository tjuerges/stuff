#include <iostream>
#include <typeinfo>


class Generic
{
	public:
		virtual ~Generic()
	{
	};

    virtual void help() = 0;

	virtual void describe()
	{
		std::cout << __PRETTY_FUNCTION__
            << "\n";
	};
};


class Sensor: public Generic
{
	public:
		virtual ~Sensor()
	{
	};

	virtual void describe() = 0;
};


class ASensor: public Sensor
{
	public:
		virtual ~ASensor()
	{
	};

	virtual void help()
	{
		std::cout << __PRETTY_FUNCTION__
            << "\n";
	};
	virtual void describe()
	{
		std::cout << __PRETTY_FUNCTION__ << "\n";
	};
};


class AGeneric: public Generic
{
	public:
	virtual ~AGeneric()
	{
	};

	virtual void help()
	{
		std::cout << __PRETTY_FUNCTION__
            << "\n";
	};
};


template< typename T1, typename T2 > void isA(T1* a)
{
	std::cout << "\n"
        << typeid(*a).name()
        << " ";

	if(typeid(*a) != typeid(T2))
	{
		std::cout << "!";
	}

    std::cout << "= "
    << typeid(T2).name()
    << ".\n\n";
}


template< typename T1, typename T2 > void isADyn(T1* b)
{
    T2* a(dynamic_cast< T2* >(b));

	std::cout << "\n"
        << typeid(*a).name()
        << " ";

	if(typeid(*a) != typeid(T2))
	{
		std::cout << "!";
	}

    std::cout << "= "
    << typeid(T2).name()
    << ".\n\n";
}


int main(int _a __attribute__((unused)), char* _b[] __attribute__((unused)))
{
	ASensor* a1(new ASensor);
    Sensor* b1(static_cast< Sensor* >(a1));
	Sensor* c1(a1);
	std::cout << typeid(*a1).name()
        << "\n"
        << typeid(*b1).name()
		<< "\n"
		<< typeid(*c1).name()
		<< "\n\n";
/*
	isA< Foo >(a);
	isA< Bar >(a);
	isA< Baz >(a);
	isADyn< Foo >(a);
	isADyn< Bar >(a);
	isADyn< Baz >(a);
*/
	delete a1;

	AGeneric* a2(new AGeneric);
//    Sensor* b2(static_cast< Sensor* >(a2));
//	Sensor* c2(a2);
	std::cout << typeid(*a2).name()
//	<< "\n"
//	<< typeid(*b2).name()
//	<< "\n"
//	<< typeid(*c2).name()
	<< "\n\n";
	delete a2;
	return 0;
}
