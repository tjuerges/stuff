#include <stdio.h>
#include <asm/cache.h>

int main(int c __attribute__((unused)), char* v[] __attribute__((unused)))
{
	typedef struct rt_task_struct
	{
    	long* stack __attribute__ ((__aligned__ (L1_CACHE_BYTES)));
		unsigned long dummy;
	} RT_TASK __attribute__ ((__aligned__ (L1_CACHE_BYTES)));

	RT_TASK a;
	a.stack = (void *)0x12345677UL;
	a.dummy = 0x87654322UL;

	printf("Printed with &: 0x%p, printed without &: 0x%p.\n",
		&a,
		a);

	return -1;
}
