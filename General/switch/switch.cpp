/**
 * $Id$
 */

#include <iostream>
#include <string>


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    std::cout << "Compiled on "
        << COMPILATION_DATE
        << ", version "
        << VERSION
        << "."
        << std::endl;


    int foo = 3;
    switch(foo)
    {
        case 3:
            std::cout << "3" << std::endl;
        case 2:
            std::cout << "2" << std::endl;
        case 1:
            std::cout << "1" << std::endl;
        default:
            break;
    };

    return 0;
};
