/**
 * $Id$
 */


#include <iostream>
#include <sys/resource.h>
#include <cerrno>


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    struct rlimit limits;
    std::cout << "getrlimit call returned "
        << (getrlimit(RLIMIT_STACK, &limits) == 0 ? 0 : errno)
        << std::endl;

    std::cout << "Current process maximum stack size = "
        << (limits.rlim_cur / (1024.0 * 1024.0))
        << "M."
        << std::endl
        << "Hard limit for maximum stack size = "
        << (limits.rlim_max / (1024.0 * 1024.0))
        << "M."
        << std::endl;

    return 0;
};
