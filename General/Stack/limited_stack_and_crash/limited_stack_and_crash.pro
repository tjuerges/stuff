#
# $Id$
#


TEMPLATE = app
TARGET = limited_stack_and_crash

LANGUAGE = C
QMAKE_LINK = gcc

CONFIG += debug
CONFIG -= qt

LIBS += -lpthread

SOURCES += limited_stack_and_crash.c
 