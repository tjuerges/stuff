/*
 * testDefaultStackSize.c
 *
 *  Created on: Oct 9, 2009
 *      Author: jschwarz
 */

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <limits.h>


static void recursion(int i)
{
    int takeSpace[2000];
    size_t index;

    printf("Array size = %u, iteration %d\n", sizeof(takeSpace), i);

    for(index = 0; index < (sizeof(takeSpace) / sizeof(int)); index++)
    {
        takeSpace[index] = i;
    }

    i++;
    recursion(i);
};


int main(int n __attribute__((unused)), char* p[] __attribute__((unused)))
{
    pthread_t thread;
    pthread_attr_t attr, cattr;
    size_t stacksize;

    /* init to default values */
    pthread_attr_init(&attr);

    cattr = attr;
    pthread_attr_getstacksize(&cattr, &stacksize);
    printf("Default stack size = %d[bytes]\n", stacksize);

    stacksize = (size_t)100000;
    printf("Resetting stack size to %d[bytes]\n", stacksize);
    pthread_attr_setstacksize(&attr, stacksize);

    printf("Creating the recursive thread... ");
    pthread_create(&thread, &attr, (void *)recursion, 0);

    printf("Thread created.\nWaiting for thread to finish... ");
    pthread_join(thread, 0);

    printf("Thread finished.  Done.\n");

    return 0;
}
