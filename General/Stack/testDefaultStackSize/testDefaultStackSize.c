/*
 * testDefaultStackSize.c
 *
 *  Created on: Oct 9, 2009
 *      Author: jschwarz
 */

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <limits.h>

int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    pthread_t thread;
    pthread_attr_t attr, cattr;
    size_t stacksize;

    /* init to default values */
    pthread_attr_init(&attr);
    cattr = attr;
    pthread_attr_getstacksize( &cattr, &stacksize);
    printf("Default stack size: %d\n", stacksize);

    stacksize = (size_t)100000;
    printf("Resetting stack size to: %d\n", stacksize);

    void recursion(int i)
    {
        int takeSpace[2000];
        takeSpace[i] = i;
        printf("Iteration %d\n", i);
        i++;
        recursion(i);
    }

    pthread_attr_setstacksize( &attr, stacksize);
    pthread_create( &thread, &attr, (void *) recursion, 0);
    pthread_join(thread, NULL);
    return 0;
}

