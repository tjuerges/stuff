#
# $Id$
#


TEMPLATE = app
TARGET = testDefaultStackSize

LANGUAGE = C
QMAKE_LINK = gcc

CONFIG += debug
CONFIG -= qt

LIBS += -lpthread

SOURCES += testDefaultStackSize.c
 