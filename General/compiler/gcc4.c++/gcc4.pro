#
# $Id$
#

TEMPLATE = app
TARGET = gcc4

LANGUAGE = C++

QMAKE_CXXFLAGS_RELEASE += -march=pentium3
QMAKE_CXXFLAGS_DEBUG += -march=pentium3

CONFIG = rtti stl warn_on
CONFIG -= qt

SOURCES += gcc4.cpp
