/**
 * $Id$
 */


#include <iostream>


unsigned int getCounter()
{
    static volatile unsigned int errorCounter(0U);

    #if __GNUC__ >= 4
    const unsigned int counter(__sync_add_and_fetch(&errorCounter, 1U));
    #else
    #error __sync_fetch_and_add is not available with this compiler!
    #endif

    return counter;
}


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    std::cout << "Calling getCounter once:  "
        << ::getCounter()
        << ", calling getCounter second time:  "
        << ::getCounter()
        << "\n\n";

    return 0;
}
