/**
 * $Id$
 */


#include <stdio.h>
#include <math.h>


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    int foo = 0;

    ++foo;
    foo = round(tgamma((double)foo));
    ++foo;
    foo = round(tgamma((double)foo));
    printf("foo = %d.\n", foo);
    foo++;
    foo = round(tgamma((double)foo));
    printf("foo = %d.\n", foo);

    return 0;
}
