#
# $Id$
#

TEMPLATE = app
TARGET = pre-postincrement-decrement

LANGUAGE = C

#DEFINES +=

CONFIG += rtti stl warn_on debug
CONFIG -= qt

QMAKE_LINK = gcc

QMAKE_CFLAGS_RELEASE += $$system(cflags.sh QMAKE_RELEASE_C) -std=c99
QMAKE_CFLAGS_DEBUG += $$system(cflags.sh QMAKE_DEBUG_C) -std=c99

LIBS += -lm

SOURCES += pre-postincrement-decrement.c
