#! /usr/bin/env bash

cat >foo.$$.cpp <<EOF
#include <cstddef>
//#include <vxWorks.h>
EOF

#c++pentium -std=gnu++98 -D_GNU_SOURCE -E -dM foo.$$.cpp | sort > Defines
#g++ -m32 -std=gnu++98 -D_GNU_SOURCE -E -dM foo.$$.cpp | sort > Defines
#g++ -std=gnu++98 -D_GNU_SOURCE -E -dM foo.$$.cpp | sort > Defines
#g++ -std=gnu++98 -E -dM foo.$$.cpp | sort > Defines
#g++ -std=gnu++11 -D_GNU_SOURCE -E -dM foo.$$.cpp | sort > Defines
#${BREW_PATH}/bin/g++-12 -I${BREW_PATH}/opt/gcc/include/c++/12 -I${BREW_PATH}/opt/gcc/include/c++/12/aarch64-apple-darwin22 -std=c++14 -stdlib=libc++ -E -dM foo.$$.cpp | sort > Defines
clang++ -std=c++14 -stdlib=libc++ -E -dM foo.$$.cpp | sort > Defines

rm -f foo.$$.cpp

