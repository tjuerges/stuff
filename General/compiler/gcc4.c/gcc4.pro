#
# $Id$
#

TEMPLATE = app
TARGET = gcc4

LANGUAGE = C

QMAKE_CFLAGS_RELEASE += -march=pentium3
QMAKE_CFLAGS_DEBUG += -march=pentium3

CONFIG += warn_on
CONFIG -= qt

SOURCES += gcc4.c

