/**
 * $Id$
 */


#include <iostream>
#include <linux/compiler.h>


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{

    if(unlikely(1 == 1, 1))
    {
        std::cout << "1 == 1 is unlikely.\n";
    }
    else
    {
        std::cout << "1 == 1 is not unlikely.\n";
    }

    if(likely(1 == 1, 1))
    {
        std::cout << "1 == 1 is likely.\n";
    }
    else
    {
        std::cout << "1 == 1 is not likely.\n";
    }

    return 0;
};
