# $Id$

TEMPLATE = app
TARGET = unlikely

LANGUAGE=C

CONFIG-=qt

QMAKE_LINK=gcc

QMAKE_CFLAGS_RELEASE += $$system(cflags.sh QMAKE_RELEASE_C)
QMAKE_CFLAGS_DEBUG += $$system(cflags.sh QMAKE_DEBUG_C)
QMAKE_LFLAGS += -g

SOURCES += unlikely.c

