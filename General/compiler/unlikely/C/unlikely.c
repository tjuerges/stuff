/**
 * $Id$
 */


#include <stdio.h>
#include <stdlib.h>


#define likely(x)    __builtin_expect(!!(x), 1)
#define unlikely(x)  __builtin_expect(!!(x), 0)


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    int a;

    /* Get the value from somewhere GCC can't optimize */
    a = atoi(argv[1]);

    if(unlikely (a == 2))
    {
      a++;
    }
    else
    {
        a--;
    }

    printf ("%d\n", a);

    return 0;
};

