/**
 * $Id$
 */


#include <iostream>


class foo
{
	public:
	foo()
	{
		std::cout << "__func__ = " << __func__ << "\n";
		std::cout << "__PRETTY_FUNCTION__ = " << __PRETTY_FUNCTION__ << "\n";
	};
};


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
	std::cout << "__func__ = " << __func__ << "\n";
	std::cout << "__PRETTY_FUNCTION__ = " << __PRETTY_FUNCTION__ << "\n";

	foo bar;

    return 0;
};
