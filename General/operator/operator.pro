# $Id$

TEMPLATE = app
TARGET = operatortest

LANGUAGE = C++

CONFIG += rtti stl warn_on debug
CONFIG -= qt

QMAKE_CXXFLAGS_RELEASE += -g $$system(cflags.sh QMAKE_RELEASE)
QMAKE_CXXFLAGS_DEBUG += -g $$system(cflags.sh QMAKE_DEBUG)

SOURCES += operatortest.cpp
