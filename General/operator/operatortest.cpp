//
// $Id$
//


#include <iostream>


int main(int y __attribute__((unused)), char* z[] __attribute__((unused)))
{
    int a(10), b(20), c(0);
    c -= (a * b);
    std::cout << "c =" << c << "\n";
    c = 0;
    c -= (a * b);
    std::cout << "c =" << c << "\n";

    c = 0;
    std::cout << "c = " << c << "\n";
    std::cout << "c++ = " << c++ << "\n";
    std::cout << "c = " << c << "\n";
    std::cout << "++c = " << ++c << "\n";
    std::cout << "c = " << c << "\n";
}
