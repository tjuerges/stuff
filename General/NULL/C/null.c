/*
 * @(#) $Id$
 */

#include <stdio.h>

int main(int c, char* v[])
{
	const char* dummy = NULL;
	const void* dummy2 = 0;

	printf("dummy=NULL == 0?\n");
	if(dummy == 0)
	{
		printf("Yes.\n");
	}
	else
	{
		printf("No.\n");
	}

	printf("dummy=NULL == dummy2=0?\n");
	if(dummy == dummy2)
	{
		printf("Yes.\n");
	}
	else
	{
		printf("No.\n");
	}

	return 0;
}

