#include <iostream>

int main(int, char**)
{
	const char* dummy(NULL);

	std::cout<< "dummy=NULL == 0?" << std::endl;
	if(dummy == 0)
	{
		std::cout << "Yes.";
	}
	else
	{
		std::cout << "No.";
	}
	std::cout << std::endl;

	const void* dummy2(0);
	std::cout<< "dummy=NULL == dummy2=0?" << std::endl;
	if(dummy == dummy2)
	{
		std::cout << "Yes.";
	}
	else
	{
		std::cout << "No.";
	}
	std::cout << std::endl;

	return 0;
}

