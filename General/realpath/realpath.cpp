#include <iostream>
#include <cstdlib>
#include <cstring>

int main(int, char* argv[])
{
    char full_path[PATH_MAX];
    std::memset(full_path, 0, PATH_MAX);
    realpath(argv[0], full_path);

    char* shortened_exec = strrchr(argv[0], '/');
    if(shortened_exec != nullptr)
    {
        shortened_exec++;
    }

    std::cout << "\n\tExecutable = "
        << argv[0]
        << "\n\tFull path = "
        << full_path
        << "\n\tUtil::check_args = "
        << shortened_exec
        << "\n";
    return 0;
}

