TEMPLATE = app
TARGET = realpath

LANGUAGE = C++

CONFIG += strict_c++ c++14 c11 strict_c rtti stl exceptions thread release warn_on
CONFIG -= qt

CONFIG += cmdline

SOURCES = realpath.cpp
