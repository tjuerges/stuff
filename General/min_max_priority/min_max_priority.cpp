//
// $Id$
//


#include <iostream>
#include <sched.h>


void getPrio(int policy, int& min, int& max)
{
    min = sched_get_priority_min(policy);
    max = sched_get_priority_max(policy);
};


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    int min = 99, max = -1;

    getPrio(SCHED_OTHER, min, max);
    std::cout << "SCHED_OTHER:  min priority = "
        << min
        << ", max priority = "
        << max
        << ".\n";

    getPrio(SCHED_BATCH, min, max);
    std::cout << "SCHED_BATCH:  min priority = "
        << min
        << ", max priority = "
        << max
        << ".\n";

    getPrio(SCHED_IDLE, min, max);
    std::cout << "SCHED_IDLE:  min priority = "
        << min
        << ", max priority = "
        << max
        << ".\n";

    getPrio(SCHED_RR, min, max);
    std::cout << "SCHED_RR:  min priority = "
        << min
        << ", max priority = "
        << max
        << ".\n";

    getPrio(SCHED_FIFO, min, max);
    std::cout << "SCHED_FIFO:  min priority = "
        << min
        << ", max priority = "
        << max
        << ".\n";

    return 0;
}
