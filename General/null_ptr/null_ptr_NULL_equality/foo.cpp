/*
clang++ -Wall -Wextra -Wformat -Werror=format-security -pedantic -Og -g -Werror -Wdeprecated -Wdeprecated-implementations -Wextra-semi -fPIC -std=c++14 -c -o foo.cpp.o foo.cpp

clang++ -Wall -Wextra -Wformat -Werror=format-security -pedantic -Og -g -Werror -Wdeprecated -Wdeprecated-implementations -Wextra-semi -fPIC -o foo foo.cpp.o foo.c.o

*/

#include <iostream>

extern "C" void* return_NULL(void);

int main(int, char**)
{
    std::cout << "NULL == nullptr: "
        << (return_NULL() == nullptr ? "equal" : "not equal")
        << "\n";
    return 0;
}
