/*
clang -Wall -Wextra -Wformat -Werror=format-security -pedantic -Og -g -Werror -Wdeprecated -Wdeprecated-implementations -Wextra-semi -fPIC -std=c11 -c -o foo.c.o foo.c
*/

#include <stddef.h>

void* return_NULL(void)
{
    return NULL;
}
