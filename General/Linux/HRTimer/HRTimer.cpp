/**
 * $Id$
 */


#include <iostream>
#include <cstdlib>
#include <cmath>
#include <ctime>


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    if(argc != 2)
    {
        std::cout << "Provide a time interval!\n\n";
        return 0;
    }


    double parameter(atof(argv[1]));
    double int_part(0.0), fract_part(std::modf(parameter, &int_part));
    const timespec sleepTime =
    {
        static_cast< unsigned long >(int_part),
        static_cast< unsigned long >(fract_part * 1e9)
    };

    unsigned long long counter(0ULL);
    while(true)
    {
        timespec start = {0, 0}, stop = {0, 0};

        clock_gettime(CLOCK_REALTIME, &start); 
        nanosleep(&sleepTime, 0);
        clock_gettime(CLOCK_REALTIME, &stop); 

        double difference(stop.tv_nsec - start.tv_nsec);
        if(difference < 0.0)
        {
            difference *= -1.0;
            stop.tv_sec -= 1;
        }

        difference *= 1e-9;
        difference += (stop.tv_sec - start.tv_sec);

        std::cout << counter++
            << " "
            << difference
            << "\n";
    }

    return 0;
};
