#
# $Id$
#

TEMPLATE = app
TARGET = HRTimer

LANGUAGE = C++
CONFIG += rtti stl exceptions debug warn_on
CONFIG -= qt


QMAKE_CXXFLAGS_RELEASE += -g $$system(cflags.sh QMAKE_RELEASE_CXX)
QMAKE_CXXFLAGS_DEBUG += -g $$system(cflags.sh QMAKE_DEBUG_CXX)

QMAKE_CFLAGS_RELEASE += -g $$system(cflags.sh QMAKE_RELEASE_C)
QMAKE_CFLAGS_DEBUG += -g $$system(cflags.sh QMAKE_DEBUG_C)

LIBS += -lm -lrt

SOURCES += HRTimer.cpp
