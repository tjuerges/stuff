/**
 * $Id$
 */

#include <iostream>
#include <sys/utsname.h>
#include <sys/sysinfo.h>


using std::cout;
using std::endl;


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
	struct utsname name;
	if(uname(&name) == -1)
	{
		cout << "Cannot get system name!" << endl;
		return -1;
	}

	cout << "Kernel sysname: "
		<< name.sysname
		<< endl
		<< "Kernel nodename: "
		<< name.nodename
		<< endl
		<< "Kernel release: "
		<< name.release
		<< endl
		<< "Kernel version: "
		<< name.version
		<< endl
		<< "Kernal machine: "
		<< name.machine
		<< endl
#ifdef _GNU_SOURCE
		<< "Kernel domainname: "
		<< name.domainname
		<< endl
#endif
		<< "Number of configured processors: "
		<< get_nprocs_conf()
		<< endl
		<< "Number of available processors: "
		<< get_nprocs()
		<< endl;
    return 0;
};
