/**
 * $Id$
 */

#include <iostream>
#include <string>
#include <vector>
#include <typeinfo>
#include <cxxabi.h>


std::string demangle(const char* mangled)
{
    std::size_t s(1024);
    std::vector< char > buffer(s);
    int status(0);

    std::string demangled(__cxxabiv1::__cxa_demangle(
        mangled, &(buffer[0]), &s, &status));

    return demangled;
}

int main(int _a __attribute__((unused)), char* _b[] __attribute__((unused)))
{
    std::cout << "demangle(typeid(\"Hallo\").name()) = "
        << demangle(typeid("Hallo").name()) << "\n";

    const char* a2 = "Hello!";
    std::cout << "demangle(typeid(const char*).name()) = "
        << demangle(typeid(a2).name()) << "\n";

    char* a3 = "Hello!";
    std::cout << "demangle(typeid(char*).name()) = "
        << demangle(typeid(a3).name()) << "\n";

    const char a4[] = "Hello!";
    std::cout << "demangle(typeid(const char[]).name()) = "
        << demangle(typeid(a4).name()) << "\n";

    char a5[] = "Hello!";
    std::cout << "demangle(typeid(char[]).name()) = "
        << demangle(typeid(a5).name()) << "\n";

    std::cout << "demangle(typeid(char[]).name()) = "
        << demangle(typeid(char[]).name()) << "\n";

    const std::string b1("Foo!");
    std::cout << "demangle(typeid(const std::string).name()) = "
        << demangle(typeid(b1).name()) << "\n";

    std::string b2("Foo!");
    std::cout << "demangle(typeid(std::string).name()) = "
        << demangle(typeid(b2).name()) << "\n";

    return 0;
}
