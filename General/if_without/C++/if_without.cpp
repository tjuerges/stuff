/**
 * $Id$
 */


#include <iostream>


int main(int argc, char* argv[])
{
	const int a(0);

	std::cout << "a = "
		<< a
		<< "\n";

        if(a)
        {
            std::cout << "a is true.\n";
        }
        else
        {
            std::cout << "a is false.\n";
        }

        if(a == 0)
        {
            std::cout << "a is false.\n";
        }
        else
        {
            std::cout << "a is true.\n";
        }
	
	return 0;
};

