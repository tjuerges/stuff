//
// $Id$
//


#include <iostream>
#include <map>
#include <string>
#include <cstring>
#include <cerrno>


int main(int a __attribute__((unused)), char* b[] __attribute__((unused)))
{
    typedef std::map< const std::string, std::pair< int, std::string > >
        MapType;
    MapType errnoMap;


    #include "errnoMap.i"

    for(MapType::const_iterator i(errnoMap.begin()); i!= errnoMap.end(); ++i)
    {
        std::cout << (*i).first
            << " = "
            << (*i).second.first
            << " = "
            << (*i).second.second
            << "\n";
    }

    return 0;
}