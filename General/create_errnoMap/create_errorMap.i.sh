#! /usr/bin/env bash
#
# $Id$
#


function createMap()
{
    sort=${1}
    key=${2}
    [[ $(uname) = Darwin ]] && errno_path=/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk
    cpp -dM ${errno_path}/usr/include/errno.h | egrep '^#define[[:space:]]+E' | sort --numeric-sort --key=${key} > errno.${sort}

    # Feed awk with the just created list in order to create an include file for
    # C++.  The include file will contain statements to populate a
    # [[http://www.sgi.com/tech/stl/Map.html | std::map]].
    awk '{print "errnoMap[\""$2"\"] = std::make_pair("$2", std::string(std::strerror("$2")));"}' < errno.${sort} > errnoMap.i

    g++ -o errno errnoMap.cpp && ./errno | sort --numeric-sort --key=${key-1} > errnoMap.${sort}
    rm errnoMap.i errno
}


# Create a list containing all the defined errno numbers.  It will be sorted by
# the errno value.
createMap "sorted_by_number" 3

# Sorted by name instead of number:
createMap "sorted_by_name" 2
