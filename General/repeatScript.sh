#!/bin/sh
# This script starts from the current directory and does a "make clean" in all
# subdirectories that contain a Makefile

REPETITIONS=$1
shift

echo Repeating $REPETITIONS times the command: $@

for ((i=$REPETITIONS; i>0; i--)); do
    source $@;
done
# _oOo_
