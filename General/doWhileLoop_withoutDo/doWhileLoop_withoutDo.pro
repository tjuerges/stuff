#
# $Id$
#

TEMPLATE = app
TARGET = doWhileLoop_withoutDo

LANGUAGE = C++

CONFIG += rtti stl exceptions debug warn_on
CONFIG -= qt

SOURCES += doWhileLoop_withoutDo.cpp
