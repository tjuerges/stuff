/**
 * $Id$
 */


#include <iostream>


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    const unsigned int limit(5);

    std::cout << "Running the loops "
        << limit
        << " times...\n\n";

    unsigned counter(0U);

    do
    {
        std::cout << "Do/While with Do, counter = "
            << counter
            << ".\n";
    }
    while((++counter) < limit);

    std::cout << "\n\n";

    counter = 0U;
    {
        std::cout << "Do/While without Do, counter = "
            << counter
            << ".\n";
    }
    while((++counter) < limit);

    return 0;
}
