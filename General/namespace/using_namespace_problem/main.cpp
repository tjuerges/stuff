/**
 * $Id$
 */

#include <iostream>
#include "foo.h"
#include "bar.h"


void print()
{
	std::cout << "I am print()." << std::endl;
};
	

int main(int c, char* v[])
{
    A::print();
    B::print();
    print();

    return 0;
}
