#ifndef foo_h
#define foo_h
/**
 * $Id$
 */

#include <iostream>

namespace A
{
    void print()
    {
        std::cout << "I am A::print." << std::endl;
    };
};

namespace B
{
    void print()
    {
        std::cout << "I am B::print." << std::endl;
    };
};
#endif
