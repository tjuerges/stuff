#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <ace/OS.h>

using namespace std;

int main(int argc, char* argv[])
{
	const std::string foo("FOO");
	#ifdef USE_STRING
	std::string e(ACE_OS::getenv(foo.c_str()));
	std::cout << "Should be empty: e=" << e << std::endl;
	#else
	char* e(0);
	#endif

	if(true)
	{
		std::string env(foo + "=bar");
		std::cout << "env=" << env << " ." << std::endl;
		ACE_OS::putenv(env.c_str());
		e = ACE_OS::getenv(foo.c_str());
		std::cout << "e=" << e << " ." << std::endl;
	}

	e = ACE_OS::getenv(foo.c_str());
	std::cout << "e=" << e << " ." << std::endl;
	
	return 0;
};

