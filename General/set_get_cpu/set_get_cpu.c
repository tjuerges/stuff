#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sched.h>


int main(int a __attribute__((unused)), char* b[] __attribute__((unused)))
{
    cpu_set_t* set;
    size_t size;
    const int nrcpus = 1024;

    set = CPU_ALLOC(nrcpus);
    size = CPU_ALLOC_SIZE(nrcpus);
    CPU_ZERO(set);

    /* Assign process to CPU #1. */
    CPU_SET(1, set);
    if(sched_setaffinity(0, size, set) == -1)
    {
        printf("%s\n", strerror(errno));
    }

    if(sched_getaffinity(0, size, set) == -1)
    {
        printf("%s\n", strerror(errno));
    }

    printf("cpu = %d\n", (CPU_ISSET(1, set) == 1 ? 1 : -1));

    /* Assign process to CPU #0. */
    CPU_ZERO(set);
    CPU_SET(0, set);
    if(sched_setaffinity(0, size, set) == -1)
    {
        printf("%s\n", strerror(errno));
    }

    if(sched_getaffinity(0, size, set) == -1)
    {
        printf("%s\n", strerror(errno));
    }

    printf("cpu = %d\n", (CPU_ISSET(0, set) == 1 ? 0 : -1));

    CPU_FREE(set);

    return 0;
}
