#
# $Id$
#

TEMPLATE = app
TARGET = set_get_cpu

LANGUAGE = C

DEFINES += _GNU_SOURCE

CONFIG -= qt

QMAKE_LINK = gcc

QMAKE_CFLAGS_RELEASE += $$system(cflags.sh QMAKE_RELEASE_C)
QMAKE_CFLAGS_DEBUG += $$system(cflags.sh QMAKE_DEBUG_C)

SOURCES += set_get_cpu.c
