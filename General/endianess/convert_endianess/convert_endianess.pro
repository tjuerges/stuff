#
# $Id$
#

TEMPLATE = app
TARGET = convert_endianess

LANGUAGE = C++

CONFIG += rtti stl exceptions debug warn_on
CONFIG -= qt

HEADERS += convert_endianess.h

SOURCES += convert_endianess.cpp

