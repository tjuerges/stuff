/**
 * $Id$
 */


#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <linux/types.h>

#include "convert_endianess.h"


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    u_int64_t org_value(0xdeadbeef01020304ULL);
    u_int64_t value(0ULL);

    vec_u8 source;
    for(std::size_t i(sizeof(u_int64_t)); i > 0; --i)
    {
        source.push_back((reinterpret_cast< u_int8_t* >(&org_value))[i - 1]);
    }

    std::cout << std::hex;

    std::cout << "Raw bytes to be converted = 0x"
        << org_value
        << "\n";

    value = byteSwap(org_value);
    std::cout << "Raw bytes swapped = 0x"
        << value
        << "\n";


    while(source.empty() != true)
    {
        std::cout << "\n\n\n***** Size of raw data vector = "
            << source.size()
            << ".\n";
        std::cout << "Raw bytes to be converted:\n";
        print_vector(source);

        for(unsigned int i(1); i <= sizeof(short); ++i)
        {
            std::cout << "Converting "
                << source.size()
                << " bytes to "
                << "unsigned short"
                << " with number of raw bytes = "
                << i
                << ".\n";
            std::vector< unsigned short > destination_u;
            dataToValue< unsigned short >(source, destination_u, i);
            print_vector(destination_u);

            std::cout << "Converting "
                << source.size()
                << " bytes to "
                << "short"
                << " with number of raw bytes = "
                << i
                << ".\n";
            std::vector< short > destination;
            dataToValue< short >(source, destination, i);
            print_vector(destination);
        }

        for(unsigned int i(1); i <= sizeof(int); ++i)
        {
            std::cout << "Converting "
                << source.size()
                << " bytes to "
                << "unsigned int"
                << " with number of raw bytes = "
                << i
                << ".\n";
            std::vector< unsigned int > destination_u;
            dataToValue< unsigned int >(source, destination_u, i);
            print_vector(destination_u);

            std::cout << "Converting "
                << source.size()
                << " bytes to "
                << "int"
                << " with number of raw bytes = "
                << i
                << ".\n";
            std::vector< int > destination;
            dataToValue< int >(source, destination, i);
            print_vector(destination);
        }

        for(unsigned int i(1); i <= sizeof(long); ++i)
        {
            std::cout << "Converting "
                << source.size()
                << " bytes to "
                << "unsigned long"
                << " with number of raw bytes = "
                << i
                << ".\n";
            std::vector< unsigned long > destination_u;
            dataToValue< unsigned long >(source, destination_u, i);
            print_vector(destination_u);

            std::cout << "Converting "
                << source.size()
                << " bytes to "
                << "long"
                << " with number of raw bytes = "
                << i
                << ".\n";
            std::vector< long > destination;
            dataToValue< long >(source, destination, i);
            print_vector(destination);
        }


        for(unsigned int i(1); i <= sizeof(long long); ++i)
        {
            std::cout << "Converting "
                << source.size()
                << " bytes to "
                << "unsigned long long"
                << " with number of raw bytes = "
                << i
                << ".\n";
            std::vector< unsigned long long > destination_u;
            dataToValue< unsigned long long >(source, destination_u, i);
            print_vector(destination_u);

            std::cout << "Converting "
                << source.size()
                << " bytes to "
                << "long long"
                << " with number of raw bytes = "
                << i
                << ".\n";
            std::vector< long long > destination;
            dataToValue< long long >(source, destination, i);
            print_vector(destination);
        }

        source.resize(source.size() - 1);
    }

    return 0;
}
