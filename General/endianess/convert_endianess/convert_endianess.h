#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef convert_endianess_h
#define convert_endianess_h
/**
 * $Id$
 */


#include <algorithm>
#include <vector>
#include <stdexcept>
#include <typeinfo>
#include <cstring>
#include <unistd.h>
#include <linux/types.h>


typedef std::vector< u_int8_t > vec_u8;
typedef std::vector< u_int8_t >::iterator vec_u8_iter;


template< typename T > T byteSwap(const T& source)
{
    T destination(source);
    u_int8_t* ptr(reinterpret_cast< unsigned char* >(&destination));

    for (std::size_t i(0);  i < (sizeof(T) >> 1U);  ++i)
    {
        std::swap(ptr[i], ptr[sizeof(T) - 1U - i]);
    }

    return destination;
};

// Take numberOfRawBytes at a time from raw, then convert them to Type and
// push them in to output.
template< typename Type > void dataToValue(vec_u8 raw,
    std::vector< Type >& output, std::size_t numberOfRawBytes)
{
    output.clear();
    if(numberOfRawBytes > sizeof(Type))
    {
        std::cout << "Converted CAN data does not fit into output data "
            "type.  sizeof("
            << typeid(Type).name()
            << ") = "
            << sizeof(Type)
            << ", number of bytes to convert = "
            << numberOfRawBytes
            << ".\n";
        return;
    }
    else if((raw.size() % numberOfRawBytes) != 0)
    {
        std::cout << "Not all CAN data can be converted because slices are not "
            "compatible with the target data type.  Length of the raw data "
            "vector = "
            << raw.size()
            << ", number of raw data bytes per output data type = "
            << numberOfRawBytes
            << ".\n";
        return;
    }
    else if(raw.empty() == true)
    {
        std::cout << "CAN data vector is empty.\n\n";
    }

    std::cout << "Architecture is "
    #if __BYTE_ORDER == __BIG_ENDIAN
        << "big"
    #else
        << "little"
    #endif
        << " endian. I will "
    #if __BYTE_ORDER == __BIG_ENDIAN
        << "not "
    #endif
        << "swap the raw data bytes.\n";

    // Populate the byte array in the correct order and copy them then to the
    // output vector.
    output.clear();
    std::vector< u_int8_t > byteArray;

    for(std::size_t i(0); i < (raw.size() / numberOfRawBytes); ++i)
    {
        byteArray.clear();
        byteArray.resize(sizeof(Type));

        // Populate the byte array in the correct order
        for(std::size_t j(0); j < numberOfRawBytes; ++j)
        {
            #if __BYTE_ORDER == __LITTLE_ENDIAN
            byteArray.at(j) =
                raw.at(raw.size() - 1 - j - (numberOfRawBytes * i));
            #else
            byteArray.at(sizeof(Type) - numberOfRawBytes + j) =
                raw.at(j + (numberOfRawBytes * i));
            #endif
        }

      output.push_back(*(reinterpret_cast< Type* >(&byteArray[0])));
    }
};



template< typename T > void print_vector(const std::vector< T >& vec)
{
    unsigned int index(0U);

    for(typename std::vector< T >::const_iterator iter(vec.begin());
        iter != vec.end(); ++iter, ++index)
    {
        std::cout << "vector["
            << index
            << "] = 0x";

        if(sizeof(T) != 1)
        {
            std::cout << static_cast< T >(*iter);
        }
        else
        {
            std::cout << static_cast< short >(*iter);
        }

        std::cout << "\n";
    }

    std::cout << "\n";
};
#endif
