/*
 * @(#) $Id$
 */


#include <stdio.h>
#include "check_endianess.h"


#define LITTLE_ENDIAN   0
#define BIG_ENDIAN      1


int main(int a __attribute__((unused)), char* v[] __attribute__((unused)))
{
    int ret = LITTLE_ENDIAN;
    long int i = 1;

    const char* p = (const char*)&i;

    // Lowest address contains the least significant byte.
    if(p[0] == 1)
    {
        printf("LITTLE_ENDIAN\n");
    }
    else
    {
        printf("BIG_ENDIAN\n");
        ret = BIG_ENDIAN;
    }

    return ret;
}
