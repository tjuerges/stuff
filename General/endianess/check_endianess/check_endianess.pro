#
# $Id$
#

TEMPLATE = app
TARGET = check_endianess

LANGUAGE = C
QMAKE_LINK = gcc

CONFIG -= qt

QMAKE_CFLAGS_RELEASE += $$system(cflags.sh QMAKE_RELEASE)
QMAKE_CFLAGS_DEBUG += $$system(cflags.sh QMAKE_DEBUG)

HEADERS += check_endianess.h

SOURCES += check_endianess.c

