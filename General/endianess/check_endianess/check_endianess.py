#!/usr/bin/env python
#
# $Id$
#


import array
import sys


def little_endian():
    return ord(array.array("i",[1]).tostring()[0])


if little_endian():
    print "little-endian platform (amd, intel, alpha)"
else:
    print "big-endian platform (motorola, sparc)"

if sys.byteorder == "little":
    print "little-endian platform (amd, intel, alpha)"
else:
    print "big-endian platform (motorola, sparc)"
