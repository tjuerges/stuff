/**
 * @(#) $Id$
 */

#include <iostream>

int main(int argc, char* argv[])
{
	std::cout << (127UL + 16UL) / 32UL << std::endl;
	std::cout << (128UL + 16UL) / 32UL << std::endl;
	std::cout << (129UL + 16UL) / 32UL << std::endl;
	std::cout << (15UL + 16UL) / 32UL << std::endl;
	std::cout << (16UL + 16UL) / 32UL << std::endl;
	std::cout << (17UL + 16UL) / 32UL << std::endl;
	std::cout << (47UL + 16UL) / 32UL << std::endl;
	std::cout << (48UL + 16UL) / 32UL << std::endl;
	std::cout << (49UL + 16UL) / 32UL << std::endl;
	return 0;
};

