#include <cmath>
#include <iostream>

int main(int,char**)
{
	std::cout<<std::atan2(2.0,1.0)*180/M_PI<<std::endl;
	std::cout<<std::atan2(2.0,-1.0)*180/M_PI<<std::endl;
	std::cout<<std::atan2(-2.0,-1.0)*180/M_PI+360.0<<std::endl;
	std::cout<<std::atan2(-2.0,1.0)*180/M_PI+360.0<<std::endl;
	return 0;
}
