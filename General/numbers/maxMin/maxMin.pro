# $Id$

TEMPLATE = app
TARGET = maxMin

LANGUAGE = C++

#CONFIG += qt rtti stl warn_on release
CONFIG += rtti stl warn_on
CONFIG -= qt

QMAKE_LFLAGS = -g

QMAKE_CXXFLAGS_RELEASE += -g $$system(cflags.sh QMAKE_RELEASE)
QMAKE_CXXFLAGS_DEBUG += -g $$system(cflags.sh QMAKE_DEBUG)

QMAKE_CFLAGS_RELEASE += -g $$system(cflags.sh QMAKE_RELEASE)
QMAKE_CFLAGS_DEBUG += -g $$system(cflags.sh QMAKE_DEBUG)

INCLUDEPATH += /alma/ACS-current/TAO/ACE_wrappers/build/linux \
    /alma/ACS-current/TAO/ACE_wrappers/build/linux/ace \
    /alma/ACS-current/TAO/ACE_wrappers/build/linux/TAO \
    /alma/ACS-current/TAO/ACE_wrappers/build/linux/TAO/orbsvcs \
    /alma/ACS-current/TAO/ACE_wrappers/build/linux/TAO/orbsvcs/orbsvcs \
    /alma/ACS-current/ACSSW/include

LIBPATH += /alma/ACS-current/TAO/ACE_wrappers/build/linux/ace \
    /alma/ACS-current/TAO/ACE_wrappers/build/linux/TAO/tao \
    /alma/ACS-current/ACSSW/lib

LIBS += -lACE \
    -lTAO

SOURCES += maxMin.cpp
