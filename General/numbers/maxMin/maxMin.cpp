/**
 * $Id$
 */


#include <iostream>
#include <sstream>
#include <tao/Basic_Types.h>
#include <limits>


template< class T > void printMinMax(const std::string& name)
{
    std::ostringstream msg;
    msg << name
        << ": min = "
        << std::numeric_limits< T >::min()
        << ", max = "
        << std::numeric_limits< T >::max()
        << ".\n";

    std::cout << msg.str();
}


// Two template function specialisations because unsigned char and char
// are always printed as a character.  In this case I want the number to
// be printed instead.
template< > void printMinMax< unsigned char >(const std::string& name)
{
    std::ostringstream msg;
    msg << name
        << ": min = "
        << static_cast< unsigned int >(
            std::numeric_limits< unsigned char >::min())
        << ", max = "
        << static_cast< unsigned int >(
            std::numeric_limits< unsigned char >::max())
        << ".\n";

    std::cout << msg.str();
}

template< > void printMinMax< char >(const std::string& name)
{
    std::ostringstream msg;
    msg << name
        << ": min = "
        << static_cast< int >(std::numeric_limits< char >::min())
        << ", max = "
        << static_cast< int >(std::numeric_limits< char >::max())
        << ".\n";

    std::cout << msg.str();
}



int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
	printMinMax< bool >("bool");
    
	printMinMax< unsigned char >("unsigned char");
	printMinMax< char >("char");

	printMinMax< unsigned short >("unsigned short");
	printMinMax< short >("short");

	printMinMax< unsigned int >("unsigned int");
	printMinMax< int >("int");

	printMinMax< unsigned long >("unsigned long");
	printMinMax< long >("long");

	printMinMax< unsigned long long >("unsigned long long");
	printMinMax< long long >("long long");

	printMinMax< float >("float");
	printMinMax< double >("double");
	printMinMax< long double >("long double");

	printMinMax< CORBA::Boolean >("CORBA::Boolean");

	printMinMax< CORBA::Octet >("CORBA::Octet");
	printMinMax< CORBA::Char >("CORBA::Char");
	printMinMax< CORBA::WChar >("CORBA::WChar");

	printMinMax< CORBA::UShort >("CORBA::UShort");
	printMinMax< CORBA::Short >("CORBA::Short");

	printMinMax< CORBA::ULong >("CORBA::ULong");
	printMinMax< CORBA::Long >("CORBA::Long");

	printMinMax< CORBA::ULongLong >("CORBA::ULongLong");
	printMinMax< CORBA::LongLong >("CORBA::LongLong");

	printMinMax< CORBA::Float >("CORBA::Float");
	printMinMax< CORBA::Double >("CORBA::Double");
    // CORBA::LongDouble is not a native type, so std::numeric_limits< T >
    // does not work for it.
    // printMinMax< CORBA::LongDouble >("CORBA::LongDouble");

    return 0;
};
