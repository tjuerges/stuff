#
# $Id$
#

TEMPLATE = app
TARGET = checkForNan

LANGUAGE = C

CONFIG -= qt

LIBS = -Wl,--as-needed $${LIBS} -lm

QMAKE_LINK = gcc

QMAKE_CFLAGS_RELEASE += $$system(cflags.sh QMAKE_RELEASE_C) -std=gnu99

QMAKE_CFLAGS_DEBUG += $$system(cflags.sh QMAKE_DEBUG_C) -std=gnu99

SOURCES += checkForNan.c
