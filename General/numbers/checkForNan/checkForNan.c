#include <stdio.h>
#include <math.h>


int main(int _a, char* _b[])
{
    double foo = 0.0;
    unsigned long* pFoo = (unsigned long *)(&foo);
    printf("foo = 0.0, %lx, fpclassify = %d, isnan = %d, isfinite = %d\n", *pFoo, fpclassify(foo), isnan(foo), isfinite(foo));

    foo = 3.0;
    printf("foo = 3.0, %lx, fpclassify = %d, isnan = %d, isfinite = %d\n", *pFoo, fpclassify(foo), isnan(foo), isfinite(foo));

    foo = -3.0;
    printf("foo = -3.0, %lx, fpclassify = %d, isnan = %d, isfinite = %d\n", *pFoo, fpclassify(foo), isnan(foo), isfinite(foo));

    foo = 1.0 / 0.0;
    printf("foo = 1.0 / 0.0, %lx, fpclassify = %d, isnan = %d, isfinite = %d\n", *pFoo, fpclassify(foo), isnan(foo), isfinite(foo));

    foo = -1.0 / 0.0;
    printf("foo = -1.0 / 0.0, %lx, fpclassify = %d, isnan = %d, isfinite = %d\n", *pFoo, fpclassify(foo), isnan(foo), isfinite(foo));

    foo = 0.0 / 0.0;
    printf("foo = 0.0 / 0.0, %lx, fpclassify = %d, isnan = %d, isfinite = %d\n", *pFoo, fpclassify(foo), isnan(foo), isfinite(foo));

    return 0;
}

