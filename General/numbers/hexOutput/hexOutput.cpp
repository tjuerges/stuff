/**
 * $Id$
 */

#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <iomanip>


int main(int argc, char* argv[])
{
    std::cout << "Compiled on "
        << COMPILATION_DATE
        << ", version "
        << VERSION
        << "."
        << std::endl;

	const unsigned int v(12345U);
	const std::string str("Wallawalla");
	std::ostringstream s;
	s << "Wallawalla. Hex value = 0x"
		<< std::hex << v << std::dec
		<< ". Dec. value = " << v
		<< ". ";
	for(std::string::const_iterator iter(str.begin());
		iter != str.end(); ++iter)
	{
		s << std::hex << "0x" << static_cast< int >(*iter) << std::dec << " ";
	}

	std::cout << s.str() << std::endl;

    return 0;
};
