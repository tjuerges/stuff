#include <iostream>
#include <limits>

int main(int argc,char* argv[])
{
	std::cout<<std::endl<<"Minimum difference between two numbers of float type (8 bits): "<<std::numeric_limits<float>::epsilon()<<std::endl<<std::endl;

	float a(0.3);
	float b(a/2.0);

	std::cout<<"Setup: a=0.3, b=a/2.0. Test if  b equals 0.15: b == 0.15?"<<std::endl;
	if( b == 0.15 )
	{
		std::cout<<"Yes, b equals 0.15."<<std::endl;
	}
	else
	{
		std::cout<<"No, is different from 0.15!"<<std::endl;
	}

	std::cout<<std::endl<<"Setup: a=0.3, b=a/2.0. Test if  b - 0.15 is less than epsilon(): b - 0.15 < std::numeric_limits<long float>::epsilon()?"<<std::endl;
	if( b - 0.15 < std::numeric_limits<float>::epsilon() )
	{
		std::cout<<"Yes, b equals 0.15."<<std::endl;
	}
	else
	{
		std::cout<<"No, is different from 0.15!"<<std::endl;
	}

	return 0;
}
