#include <iostream>
#include <cmath>
#include <limits>
#include <boost/format.hpp>


int main(int a __attribute__((unused)), char* b[] __attribute__((unused)))
{
    int intNumber(0);
    double number(0.0);
    double scale(0.0);
    double scaledNumber(0.0);
    double fromInt(0.0);

    while(true)
    {
        std::cout << "\nNumber = ";
        std::cin >> number;
        if(std::abs(number) < std::numeric_limits< double >::epsilon())
        {
            break;
        }

        for(scale = 1e10; scale >= 1e3; scale /= 10.0)
        {
            scaledNumber = number * scale;
            intNumber = static_cast< int >(lround(scaledNumber));
            fromInt = intNumber / scale;
            std:: cout << boost::str(boost::format("scale = %e, number * "
                "scale = %0g, product = %0d, coefficient "
                "/ scale = %0g\n") %
                scale % scaledNumber % intNumber % fromInt);
        }
    };

    return 0;
}
