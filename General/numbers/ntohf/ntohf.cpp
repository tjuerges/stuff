#include <iostream>
#include <string>
#include <sstream>
#include <typeinfo>
#include <boost/format.hpp>
#include <cstring>
#include <ctime>
#if (__cplusplus > 201103L)
#include <cstdint>
#else
#include <stdint.h>
#endif

//#define DEBUG

union SwapFloatUnion
{
    uint32_t uint;
    uint8_t bytes[sizeof(float)];
};



template< typename T > std::string printAsHex(const T& f)
{
    std::cout << f << "\n";
    std::string ret;
    switch(sizeof(T))
    {
        case sizeof(uint8_t):
        {
            typedef uint8_t UInt;
            const UInt f_uint{static_cast< UInt >(f)};
            ret = boost::str(boost::format("%f = 0x%02x\n") % f % f_uint);
            break;
        }

        case sizeof(uint16_t):
        {
            typedef uint16_t UInt;
            const UInt f_uint{static_cast< UInt >(f)};
            ret = boost::str(boost::format("%f = 0x%04x\n") % f % f_uint);
            break;
        }

        case sizeof(float):
        {
            typedef uint32_t UInt;
            const UInt f_uint{static_cast< UInt >(f)};
            ret = boost::str(boost::format("%f = 0x%08x\n") % f % f_uint);
            break;
        }

        case sizeof(double):
        {
            typedef uint64_t UInt;
            const UInt f_uint{static_cast< UInt >(f)};
            ret = boost::str(boost::format("%f = 0x%016x\n") % f % f_uint);
            break;
        }

        /*case sizeof(long double):
        {
            typedef uint8_t UInt;
            UInt f_uint[sizeof(f)];
            std::memset(f_uint, 0, sizeof(f_uint));
            std::memcpy(f_uint, &f, sizeof(f));
            ret = boost::str(boost::format("%f = 0x%032x\n") % f % f_uint);
            break;
        }*/

        default:
        {
            std::ostringstream s;
            s << __PRETTY_FUNCTION__
                << "\n\nCannot convert type "
                << typeid(T).name()
                << " to an uintN_t type.\n\n";
            ret = s.str();
            break;
        }
    }

    return ret;
}


float turn_float(int Dataf)
{
#ifdef DEBUG
    std::ostringstream s;
    s << __PRETTY_FUNCTION__
        << "\nin:\t"
        << printAsHex(static_cast< float >(Dataf));
#endif
    char SwapBytes[4];
    std::memcpy(SwapBytes, &Dataf, 4);

    char Swap[4];
    Swap[0] = SwapBytes[3];
    Swap[1] = SwapBytes[2];
    Swap[2] = SwapBytes[1];
    Swap[3] = SwapBytes[0];

    std::memcpy(&Dataf, Swap, 4);

#ifdef DEBUG
    s << "out:\t"
    << printAsHex(Dataf)
    << "\n";
    std::cout << s.str();
#endif

    return static_cast< float>(Dataf);
}


float ntohf(float f)
{
    union SwapFloatUnion nonSwapped, swapped;
    nonSwapped.uint = static_cast< uint32_t >(f);

#ifdef DEBUG
    std::ostringstream s;
    s << __PRETTY_FUNCTION__
        << "\nin:\t"
        << printAsHex(nonSwapped.uint);
#endif

    for(size_t i(0), j(sizeof(nonSwapped) - 1); i < sizeof(nonSwapped); ++i, --j)
    {
        swapped.bytes[i] = nonSwapped.bytes[j];
    }

#ifdef DEBUG
    s << "out:\t"
        << printAsHex(swapped.uint)
        << "\n";
    std::cout << s.str();
#endif

    return static_cast< float >(swapped.uint);
}


int main(int a, char* b[])
{
    if(a != 2)
    {
        std::cout << "Please enter the number of iterations!\n\n";
        return -1;
    }

    struct timespec start = {0U, 0U}, stop = {0U, 0U};
    unsigned long runs{strtoul(b[1], nullptr, 10)};
    const float f(0x01020304);

    double result1{0.0}, result2{0.0};
    unsigned long counter(0UL);
    for(unsigned long i{runs}; i > 0UL; --i)
    {
        ++counter;
        start = {0U, 0U};
        stop = {0U, 0U};
        clock_gettime(CLOCK_REALTIME, &start);
        turn_float(static_cast< int >(f));
        clock_gettime(CLOCK_REALTIME, &stop);
        result1 += (static_cast< double >(stop.tv_sec)
            - static_cast< double >(start.tv_sec)) * 1e9;
        result1 += + (static_cast< double >(stop.tv_nsec)
            - static_cast< double >(start.tv_nsec));
    }

    if(counter != runs)
    {
        std::cout << counter << "\n";
        return -2;
    }

    for(unsigned long i{runs}; i > 0UL; --i)
    {
        start = {0U, 0U};
        stop = {0U, 0U};
        clock_gettime(CLOCK_REALTIME, &start);
        ntohf(f);
        clock_gettime(CLOCK_REALTIME, &stop);
        result2 += (static_cast< double >(stop.tv_sec)
            - static_cast< double >(start.tv_sec)) * 1e9;
        result2 += + (static_cast< double >(stop.tv_nsec)
            - static_cast< double >(start.tv_nsec));
    }

    result1 /= static_cast< double >(runs);
    result2 /= static_cast< double >(runs);

    std::cout << boost::format("result1(%u) = %02.9fns\nresult2(%u) = "
        "%02.9fns\nresult2/result1 = %f%%.\n\n")
        % runs
        % result1
        % runs
        % result2
        % ((result2 / result1) * 100.0);

    return 0;
}
