/**
 * "@(#) $Id$"
 */

#include <iostream>
#include <cmath>


int main(int argc, char* argv[])
{
    std::cout << "Compiled on "
        << COMPILATION_DATE
        << ", version "
        << VERSION
        << "."
        << std::endl;


	int foo(0U);
	double a(1.501), b(-1.501);

	std::cout
		<< "foo = " << foo << std::endl
		<< "a = " << a << std::endl
		<< "b = " << b << std::endl;

	foo = static_cast< unsigned int >(std::floor(a + 0.5));
	std::cout << "foo(a) = " << foo << std::endl;

	foo = static_cast< unsigned int >(std::floor(b - 0.5));
	std::cout << "Wrong: foo = static_cast< unsigned int >(std::floor(b - 0.5)) = " << foo << std::endl;

	foo = static_cast< unsigned int >(std::floor(b + 0.5));
	std::cout << "Correct: foo = static_cast< unsigned int >(std::floor(b + 0.5)) = " << foo << std::endl;

    return 0;
};
