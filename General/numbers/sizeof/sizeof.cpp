#include <iostream>
#include <sstream>
#include <omniORB4/CORBA.h>

template< typename T > void printSize(const std::string& name)
{
    std::cout << "sizeof("
        << name
        << ") = "
        << sizeof(T)
        << "\n";
}

int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    printSize< bool >("bool");

    printSize< unsigned char >("unsigned char");
    printSize< char >("char");

    printSize< unsigned short >("unsigned short");
    printSize< short >("short");

    printSize< unsigned int >("unsigned int");
    printSize< int >("int");

    printSize< unsigned long >("unsigned long");
    printSize< long >("long");

    printSize< unsigned long long >("unsigned long long");
    printSize< long long >("long long");

    printSize< float >("float");
    printSize< double >("double");
    printSize< long double >("long double");


    printSize< CORBA::Boolean >("CORBA::Boolean");

    printSize< CORBA::Octet >("CORBA::Octet");
    printSize< CORBA::Char >("CORBA::Char");
    printSize< CORBA::WChar >("CORBA::WChar");

    printSize< CORBA::UShort >("CORBA::UShort");
    printSize< CORBA::Short >("CORBA::Short");

    printSize< CORBA::ULong >("CORBA::ULong");
    printSize< CORBA::Long >("CORBA::Long");

    printSize< CORBA::ULongLong >("CORBA::ULongLong");
    printSize< CORBA::LongLong >("CORBA::LongLong");

    printSize< CORBA::Float >("CORBA::Float");
    printSize< CORBA::Double >("CORBA::Double");
#ifdef OMNI_HAS_LongDouble
    printSize< CORBA::LongDouble >("CORBA::LongDouble");
#endif

    return 0;
}
