#ifndef member_function_template_h
#define member_function_template_h
/**
 * $Id$
 */


#include <iostream>
#include <string>
#include <sstream>


class foo
{
    public:
    template< typename in_type > std::string toString(const in_type& in) const
    {
        std::cout << __PRETTY_FUNCTION__ << "\n";

        std::stringstream out;
        out << in; // first insert value to stream
        return out.str();
    };

    std::string toString(const char* in) const
    {
        std::cout << __PRETTY_FUNCTION__
            << "\t"
            << std::string(in)
            << "\n";
        return std::string(in);
    };

    std::string toString(char* in) const
    {
        std::cout << __PRETTY_FUNCTION__
            << "\t"
            << std::string(in)
            << "\n";
        return std::string(in);
    };

    std::string toString(const std::string& in) const
    {
        std::cout << __PRETTY_FUNCTION__
            << "\t"
            << std::string(in)
            << "\n";
        return std::string(in);
    };

    std::string toString(const std::string* in) const
    {
        std::cout << __PRETTY_FUNCTION__
            << "\t"
            << std::string(*in)
            << "\n";
        return std::string(*in);
    };

    std::string toString(std::string* in) const
    {
        std::cout << __PRETTY_FUNCTION__
            << "\t"
            << std::string(*in)
            << "\n";
        return std::string(*in);
    };
};

#endif
