/**
 * $Id$
 */


#include "member_function_template.h"
#include <string>
#include <memory>
#include <cmath>


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{

    foo test;
    const std::string s1("const std::string");
    std::string s2("std::string");
    std::auto_ptr< std::string > s3(new std::string("std::string*"));
    const char* s4 = "const char*";
    char* s5 = "char*";
    const char s6[] = "const char[]";
    char s7[] = "char[]";

    test.toString(s1);
    test.toString(s2);
    test.toString(s3.get());
    test.toString(s4);
    test.toString(s5);
    test.toString(s6);
    test.toString(s7);
    test.toString(M_PIl);

    return 0;
};
