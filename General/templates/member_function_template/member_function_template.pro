#
# $Id$
#

TEMPLATE = app
TARGET = member_function_template

LANGUAGE = C++

CONFIG += rtti stl exceptions debug warn_on
CONFIG -= qt

QMAKE_CXXFLAGS_RELEASE += -g $$system(cflags.sh QMAKE_RELEASE_CXX)
QMAKE_CXXFLAGS_DEBUG += -g $$system(cflags.sh QMAKE_DEBUG_CXX)

HEADERS += member_function_template.h

SOURCES += member_function_template.cpp
