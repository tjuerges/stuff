# $Id$

TEMPLATE = app
TARGET = problem2

LANGUAGE = C++

CONFIG += warn_on release
CONFIG -= qt

QMAKE_CXXFLAGS_RELEASE += -ftemplate-depth-4000000
QMAKE_LFLAGS = -s

SOURCES += problem2.cpp
