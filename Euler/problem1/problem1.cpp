/**
 * $Id$
 */

/**
 * Solution for Problem # 1 from Project Euler.
 * http://projecteuler.net/index.php?section=problems
 *
 * If we list all the natural numbers below 10 that are multiples of 3 or 5,
 * we get 3, 5, 6 and 9. The sum of these multiples is 23.
 *
 *Find the sum of all the multiples of 3 or 5 below 1000.
 */


#include <iostream>


template< int number, int divider1, int divider2 > struct isDividable
{
    enum
    {
        value = ((number % divider1 == 0) ? number :
         (number % divider2 == 0) ? number : 0)
    };
};


template< int start, int number1, int number2 > struct func
{
    enum
    {
        value = (start > 0 ?
             (isDividable< start, number1, number2 >::value
                + func< start - 1, number1, number2 >::value) : 0)
    };
};


template<int number1, int number2 > struct func< 0, number1, number2>
{
    enum
    {
        value = 0
    };
};



int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    std::cout
        << func< 999, 3, 5 >::value
        << std::endl;

    return 0;
};
