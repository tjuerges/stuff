# $Id$

TEMPLATE = app
TARGET = problem1

LANGUAGE = C++

CONFIG += warn_on release
CONFIG -= qt

QMAKE_CXXFLAGS_RELEASE += -ftemplate-depth-1000
QMAKE_LFLAGS = -s

SOURCES += problem1.cpp
