SET SERVEROUTPUT ON;

DECLARE
	lruid NUMBER;
	assid NUMBER;
	asstid NUMBER;
	propid NUMBER;

BEGIN

SELECT COUNT(lrutypeid) INTO lruid FROM lrutype;
SELECT COUNT(assemblyid) INTO assid FROM assembly;
SELECT COUNT(assemblytypeid) INTO asstid FROM assemblytype;
SELECT COUNT(propertytypeid) INTO propid FROM propertytype;

lruid := lruid + 1;
asstid := asstid + 1;
propid := propid + 1;
assid := assid + 1;

IF lruid = NULL THEN lruid := 1;
END IF;

IF asstid = NULL THEN asstid := 1;
END IF;

IF propid = NULL THEN propid := 1;
END IF;

IF assid = NULL THEN assid := 1;
END IF;

DBMS_OUTPUT.PUT_LINE('lruid=' || lruid);
DBMS_OUTPUT.PUT_LINE('assid=' || assid);
DBMS_OUTPUT.PUT_LINE('asstid=' || asstid);
DBMS_OUTPUT.PUT_LINE('propid=' || propid);

END;
/
