#! /bin/bash

export LD_LIBRARY_PATH=`pwd`/instantclient_10_2:$LD_LIBRARY_PATH

if [ $# -ne 1 ]; then
	echo "Please provide an SQL-script."
	exit
fi

./gqlplus 'monitorData/alma$dba@archive1' @$1
