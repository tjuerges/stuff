CREATE TABLE LRUType (
        LRUTypeId               Number (2)              NOT NULL,
	LRU_Name                VARCHAR (24)            NOT NULL,
	Full_Name                VARCHAR (80)            NOT NULL,
	ICD                     VARCHAR (50)            NOT NULL,
	ICD_Date                 Date                    NOT NULL,
	Description             VARCHAR (1024)          NOT NULL,
	Notes                   VARCHAR (1024)          NULL,
	CONSTRAINT LRUType_1 PRIMARY KEY (LRUTypeId)
);

CREATE TABLE AssemblyType (
	AssemblyTypeId		NUMBER (2)              NOT NULL,
        LRUTypeId               Number (2)              NOT NULL,
	Assembly_name           VARCHAR (24)            NOT NULL,
	Full_Name               VARCHAR (80)            NOT NULL,
	Node_Address            VARCHAR (16)            NOT NULL,
	Channel_Number          NUMBER (1)              NOT NULL,
	Description             VARCHAR (1024)          NOT NULL,
	Notes                   VARCHAR (1024)          NULL,
        CONSTRAINT AssemblyType_1 PRIMARY KEY (AssemblyTypeId),
        CONSTRAINT AssemblyType_2 FOREIGN KEY (LRUTypeId) REFERENCES LRUType
);

CREATE TABLE PropertyType (
	PropertyTypeId          NUMBER (4)              NOT NULL,
	AssemblyTypeId		NUMBER (2)              NOT NULL,
	Data_Type               VARCHAR (16)            NOT NULL,
        Table_Name	        VARCHAR (24)            NOT NULL,
	Property_Name           VARCHAR (24)            NOT NULL,
	RCA                     VARCHAR (16)            NOT NULL,
	teRelated               CHAR (1)                NOT NULL,
	Raw_Data_Type           VARCHAR (24)            NOT NULL,
	World_Data_Type         VARCHAR (24)            NOT NULL,
	Units                   VARCHAR (24)            NULL,
	Scale                   BINARY_DOUBLE           NULL,
	Offset                  BINARY_DOUBLE           NULL,
	Min_Range               BINARY_DOUBLE           NULL,
	Max_Range               BINARY_DOUBLE           NULL,
	SamplingInterval        BINARY_FLOAT            NOT NULL,
	Graph_Min               BINARY_FLOAT            NULL,
	Graph_Max               BINARY_FLOAT            NULL,
	Graph_Format            VARCHAR (16)            NULL,
	Graph_Title             VARCHAR (80)            NULL,
	Description             VARCHAR (1024)          NOT NULL,
	CONSTRAINT PropertyType_1 PRIMARY KEY (PropertyTypeId),
	CONSTRAINT PropertyType_2 FOREIGN KEY (AssemblyTypeId) REFERENCES AssemblyType,
        CONSTRAINT PropertyType_3 CHECK (teRelated IN ('N', 'Y'))
);

CREATE TABLE Assembly (
	AssemblyId		Number (4)		NOT NULL,
	AssemblyTypeId		NUMBER (2)              NOT NULL,	
	Serial_Number		VARCHAR (80)            NOT NULL,
	CONSTRAINT Assembly_1 PRIMARY KEY (AssemblyId),
	CONSTRAINT Assembly_2 FOREIGN KEY (AssemblyTypeId) REFERENCES AssemblyType
);

CREATE TABLE Antenna (
	Antenna_Name		VARCHAR (8)             NOT NULL,
	AssemblyId		Number (4)		NOT NULL,
	CONSTRAINT Antenna_1 PRIMARY KEY (Antenna_Name, AssemblyId),
	CONSTRAINT Antenna_2 FOREIGN KEY (AssemblyId) REFERENCES Assembly
);

CREATE TABLE FloatProperty (
	AssemblyId		Number (4)		NOT NULL,
	PropertyTypeId		Number (4)		NOT NULL,
	Sample_Time		TIMESTAMP		NOT NULL,
	Value			BINARY_FLOAT            NOT NULL,
	CONSTRAINT FloatProperty_1 PRIMARY KEY (AssemblyId, PropertyTypeId, Sample_Time),
	CONSTRAINT FloatProperty_2 FOREIGN KEY (AssemblyId) REFERENCES Assembly,
	CONSTRAINT FloatProperty_3 FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType
);

CREATE TABLE StringProperty (
	AssemblyId		Number (4)		NOT NULL,
	PropertyTypeId		Number (4)		NOT NULL,
	Sample_Time		TIMESTAMP		NOT NULL,
	Value			VARCHAR (24)            NOT NULL,
	CONSTRAINT StringProperty_1 PRIMARY KEY (AssemblyId, PropertyTypeId, Sample_Time),
	CONSTRAINT StringProperty_2 FOREIGN KEY (AssemblyId) REFERENCES Assembly,
	CONSTRAINT StringProperty_3 FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType
);

CREATE TABLE BooleanProperty (
	AssemblyId		Number (4)		NOT NULL,
	PropertyTypeId		Number (4)		NOT NULL,
	Sample_Time		TIMESTAMP		NOT NULL,
	Value			CHAR (1)            	NOT NULL,
	CONSTRAINT BooleanProperty_1 PRIMARY KEY (AssemblyId, PropertyTypeId, Sample_Time),
	CONSTRAINT BooleanProperty_2 FOREIGN KEY (AssemblyId) REFERENCES Assembly,
	CONSTRAINT BooleanProperty_3 FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType,
	CONSTRAINT BooleanProperty_4 CHECK (Value IN ('N', 'Y'))
);

CREATE TABLE DoubleProperty (
	AssemblyId		Number (4)		NOT NULL,
	PropertyTypeId		Number (4)		NOT NULL,
	Sample_Time		TIMESTAMP		NOT NULL,
	Value			BINARY_DOUBLE           NOT NULL,
	CONSTRAINT DoubleProperty_1 PRIMARY KEY (AssemblyId, PropertyTypeId, Sample_Time),
	CONSTRAINT DoubleProperty_2 FOREIGN KEY (AssemblyId) REFERENCES Assembly,
	CONSTRAINT DoubleProperty_3 FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType
);

CREATE TABLE IntegerProperty (
	AssemblyId		Number (4)		NOT NULL,
	PropertyTypeId		Number (4)		NOT NULL,
	Sample_Time		TIMESTAMP		NOT NULL,
	Value			Number(10)            	NOT NULL,
	CONSTRAINT IntegerProperty_1 PRIMARY KEY (AssemblyId, PropertyTypeId, Sample_Time),
	CONSTRAINT IntegerProperty_2 FOREIGN KEY (AssemblyId) REFERENCES Assembly,
	CONSTRAINT IntegerProperty_3 FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType
);

commit;
/
.
quit;
