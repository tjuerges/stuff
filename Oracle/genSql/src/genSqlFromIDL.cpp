#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <fstream>
#include <iomanip>


const std::string header("SET SERVEROUTPUT ON;\n\
\n\
DECLARE\n\
	lruid NUMBER;\n\
	assid NUMBER;\n\
	asstid NUMBER;\n\
	propid NUMBER;\n\
\n\
BEGIN\n\
\n\
SELECT COUNT(lrutypeid) INTO lruid FROM lrutype;\n\
SELECT COUNT(assemblyid) INTO assid FROM assembly;\n\
SELECT COUNT(assemblytypeid) INTO asstid FROM assemblytype;\n\
SELECT COUNT(propertytypeid) INTO propid FROM propertytype;\n\
\n\
lruid := lruid + 1;\n\
INSERT INTO LRUType VALUES (\n\
	lruid,\n\
	'SUBSTITUTE_ME',\n\
	'Long device name',\n\
	'ICD reference',\n\
	'ICD date',\n\
	'Long description, ICD abstract.',\n\
	''\n\
);\n\
\n\
asstid := asstid + 1;\n\
INSERT INTO AssemblyType VALUES (\n\
	asstid,\n\
	lruid,\n\
	'SUBSTITUTE_ME',\n\
	'Long device name',\n\
	'Node id',\n\
	'Channel number',\n\
	'Long description, ICD abstract.',\n\
	''\n\
);\n\
\n\
propid := propid + 1;\n\
INSERT INTO PropertyType VALUES (\n\
	propid,\n\
	asstid,\n\
	'integer',\n\
	'IntegerProperty',\n\
	'errors',\n\
	'0x30001',\n\
	'N',\n\
	'uint32',\n\
	'uint32',\n\
	'na',\n\
	'1',\n\
	'0',\n\
	'0',\n\
	'4294967295',\n\
	'60',\n\
	'0',\n\
	'4294967295',\n\
	'10d',\n\
	'Errors',\n\
	'This monitor point provides the number of communication errors between the AMBSI and the slave device.'\n\
);\n\
\n\
propid := propid + 1;\n\
INSERT INTO PropertyType VALUES (\n\
	propid,\n\
	asstid,\n\
	'integer',\n\
	'IntegerProperty',\n\
	'transactions',\n\
	'0x30002',\n\
	'N',\n\
	'uint32',\n\
	'uint32',\n\
	'na',\n\
	'1',\n\
	'0',\n\
	'0',\n\
	'4294967295',\n\
	'60',\n\
	'0',\n\
	'4294967295',\n\
	'10d',\n\
	'Transactions',\n\
	'This monitor point provides the number of transactions between the AMBSI and the slave device.'\n\
);\n\
\n\
propid := propid + 1;\n\
INSERT INTO PropertyType VALUES (\n\
	propid,\n\
	asstid,\n\
	'float',\n\
   	'FloatProperty',\n\
	'temperature',\n\
	'0x30003',\n\
	'N',\n\
	'uint32',\n\
	'float',\n\
	'Degrees Celsius',\n\
	'1.0',\n\
	'0.0',\n\
	'-55.0',\n\
	'125.0',\n\
	'600',\n\
	'-55.0',\n\
	'125.0',\n\
	'8.3f',\n\
	'AMBSI ambient temperature.',\n\
	'The ambient temperature reported by the AMBSI DS1820 device.'\n\
);\n\
\n");

const std::string preProp("propid := propid + 1;\n\
INSERT INTO PropertyType VALUES (\n\
	propid,\n\
	asstid,\n\
	'Data type',\n\
	'Property table name',\n\
	'SUBSTITUTE_ME',\n");

const std::string postProp("	'RCA',\n\
	'TE-related? Y/N',\n\
	'raw data type',\n\
	'world data type',\n\
	'Unit',\n\
	'Scale',\n\
	'Offset',\n\
	'Min range',\n\
	'Max range',\n\
	'Sampling interval',\n\
	'Graph min',\n\
	'Graph max',\n\
	'printf formatter',\n\
	'Graph title',\n\
	'Description'\n\
);\n\
\n");

const std::string footer("\n\
assid := assid + 1;\n\
INSERT INTO Assembly VALUES (\n\
	assid,\n\
	asstid,\n\
	'CONTROL/ALMA01/SUBSTITUTE_ME'\n\
);\n\
\n\
assid := assid + 1;\n\
INSERT INTO Assembly VALUES (\n\
	assid,\n\
	asstid,\n\
	'CONTROL/ALMA02/SUBSTITUTE_ME'\n\
);\n\
\n\
COMMIT;\n\
END;\n\
/\n\
.\n\
QUIT;\n");

const std::string::size_type headerPos1(header.find("SUBSTITUTE_ME"));
const std::string::size_type prePropPos(preProp.find("SUBSTITUTE_ME"));
const std::string::size_type footerPos1(footer.find("SUBSTITUTE_ME"));

int main(int argc, char* argv[])
{
	if(argc < 3)
	{
		std::string me(argv[0]);
		std::string::size_type cutHere(me.rfind('/'));
		if(cutHere != std::string::npos)
		{
			me.erase(0, cutHere + 1);
		}

		std::cout << "Pass the device name as first and the IDL file as second "
			"parameter!" << std::endl
			<< "Example: " << me << " FOO willie/idl/bar.midl" << std::endl;

		return -1;
	}

	const std::string device(argv[1]);
	std::string filename(argv[2]);
	std::cout << "Using " << filename << " as input file." << std::endl;

	std::ifstream in(filename.c_str(), std::ios::in);
	if(in == false)
	{
		std::cout << "Could not open the input file." << std::endl;

		return -1;
	}

	std::string::size_type pos;
	pos = filename.rfind('/');
	if(pos != std::string::npos)
	{
		filename.erase(0, pos + 1);
	}

	pos = filename.rfind(".idl");
	if(pos == std::string::npos)
	{
		pos = filename.rfind(".midl");
	}

	filename.erase(pos);
	filename.append("Add.sql");
	std::ofstream out(filename.c_str(), std::ios::out | std::ios::trunc);
	if(out == false)
	{
		std::cout << "Could not open output file " << filename << "." << std::endl;
		in.close();

		return -1;
	}
	else
	{
		std::cout << "Using " << filename << " as output file." << std::endl;
	}

	std::string newHeader;
	newHeader.assign(header);
	newHeader.replace(headerPos1, 13, device);
	std::string::size_type headerPos2(newHeader.rfind("SUBSTITUTE_ME"));
	newHeader.replace(headerPos2, 13, device);
	out << newHeader;

	std::string input, rdonly, attr, type, name, realName, newPreProp;
	std::size_t counter(0);
	while(in.eof() == false)
	{
		std::getline(in, input);
		pos = input.find("readonly attribute");
		if(pos != std::string::npos)
		{
			std::istringstream istr(input);
			istr >> rdonly >> attr >> type >> name;
			if(attr.find("attribute") == std::string::npos)
			{
				continue;
			}

			pos = name.find(';');
			if(pos != std::string::npos)
			{
				name.erase(pos);
				realName = name;
			}
			else
			{
				realName = name;
			}

			// Length of SUBSTITUTE_ME = 13.
			newPreProp.assign(preProp);
			newPreProp.replace(prePropPos, 13, realName);
			out << newPreProp;
			out << postProp;

			++counter;
		}
	}

	in.close();

	std::string newFooter;
	newFooter.assign(footer);
	newFooter.replace(footerPos1, 13, device);
	std::string::size_type footerPos2(newFooter.rfind("SUBSTITUTE_ME"));
	newFooter.replace(footerPos2, 13, device);
	out << newFooter;

	out.close();

	std::cout << "Got " << counter << " properties." << std::endl;

	return 0;
};
