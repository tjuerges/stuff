SET SERVEROUTPUT ON;

DECLARE
	lruid NUMBER;
	assid NUMBER;
	asstid NUMBER;
	propid NUMBER;

BEGIN

SELECT COUNT(lrutypeid) INTO lruid FROM lrutype;
SELECT COUNT(assemblyid) INTO assid FROM assembly;
SELECT COUNT(assemblytypeid) INTO asstid FROM assemblytype;
SELECT COUNT(propertytypeid) INTO propid FROM propertytype;

lruid := lruid + 1;
INSERT INTO LRUType VALUES (
	lruid,
	'MountController',
	'Long device name',
	'ICD reference',
	'ICD date',
	'Long description, ICD abstract.',
	''
);

asstid := asstid + 1;
INSERT INTO AssemblyType VALUES (
	asstid,
	lruid,
	'MountController',
	'Long device name',
	'Node id',
	'Channel number',
	'Long description, ICD abstract.',
	''
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'integer',
	'IntegerProperty',
	'errors',
	'0x30001',
	'N',
	'uint32',
	'uint32',
	'na',
	'1',
	'0',
	'0',
	'4294967295',
	'60',
	'0',
	'4294967295',
	'10d',
	'Errors',
	'This monitor point provides the number of communication errors between the AMBSI and the slave device.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'integer',
	'IntegerProperty',
	'transactions',
	'0x30002',
	'N',
	'uint32',
	'uint32',
	'na',
	'1',
	'0',
	'0',
	'4294967295',
	'60',
	'0',
	'4294967295',
	'10d',
	'Transactions',
	'This monitor point provides the number of transactions between the AMBSI and the slave device.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'float',
   	'FloatProperty',
	'temperature',
	'0x30003',
	'N',
	'uint32',
	'float',
	'Degrees Celsius',
	'1.0',
	'0.0',
	'-55.0',
	'125.0',
	'600',
	'-55.0',
	'125.0',
	'8.3f',
	'AMBSI ambient temperature.',
	'The ambient temperature reported by the AMBSI DS1820 device.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_acu_error',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_az_brake',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_az_motor_currents',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_az_motor_temps',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_az_motor_torque',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_az_servo_coeff_0',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_az_servo_coeff_1',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_az_servo_coeff_2',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_az_servo_coeff_3',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_az_servo_coeff_4',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_az_servo_coeff_5',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_az_servo_coeff_6',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_az_servo_coeff_7',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_az_servo_coeff_8',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_az_servo_coeff_9',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_az_servo_coeff_A',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_az_servo_coeff_B',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_az_servo_coeff_C',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_az_status',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_el_brake',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_el_motor_currents',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_el_motor_temps',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_el_motor_torque',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_el_servo_coeff_0',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_el_servo_coeff_1',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_el_servo_coeff_2',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_el_servo_coeff_3',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_el_servo_coeff_4',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_el_servo_coeff_5',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_el_servo_coeff_6',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_el_servo_coeff_7',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_el_servo_coeff_8',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_el_servo_coeff_9',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_el_servo_coeff_A',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_el_servo_coeff_B',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_el_servo_coeff_C',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_el_status',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_idle_stow_time',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_ip_address',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_system_status',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_shutter',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_stow_pin',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_az_brake',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_az_servo_coeff_0',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_az_servo_coeff_1',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_az_servo_coeff_2',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_az_servo_coeff_3',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_az_servo_coeff_4',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_az_servo_coeff_5',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_az_servo_coeff_6',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_az_servo_coeff_7',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_az_servo_coeff_8',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_az_servo_coeff_9',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_az_servo_coeff_A',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_az_servo_coeff_B',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_az_servo_coeff_C',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_el_brake',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_el_servo_coeff_0',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_el_servo_coeff_1',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_el_servo_coeff_2',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_el_servo_coeff_3',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_el_servo_coeff_4',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_el_servo_coeff_5',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_el_servo_coeff_6',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_el_servo_coeff_7',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_el_servo_coeff_8',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_el_servo_coeff_9',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_el_servo_coeff_A',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_el_servo_coeff_B',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_el_servo_coeff_C',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_idle_stow_time',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_stow_pin',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_shutter',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'localAccessMode',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'setAzAxisMode',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'azAxisMode',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'setElAxisMode',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'elAxisMode',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'setAzBrakeEngaged',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'setElBrakeEngaged',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'setTolerance',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'onTarget',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'cloudsat',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'setApplyAcuPointingModel',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'applyAcuPointingModel',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'recordPositionData',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'azEncoder',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'elEncoder',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);


assid := assid + 1;
INSERT INTO Assembly VALUES (
	assid,
	asstid,
	'CONTROL/ALMA01/MountController'
);

assid := assid + 1;
INSERT INTO Assembly VALUES (
	assid,
	asstid,
	'CONTROL/ALMA02/MountController'
);

COMMIT;
END;
/
.
QUIT;
