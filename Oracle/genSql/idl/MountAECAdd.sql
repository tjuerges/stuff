SET SERVEROUTPUT ON;

DECLARE
	lruid NUMBER;
	assid NUMBER;
	asstid NUMBER;
	propid NUMBER;

BEGIN

SELECT COUNT(lrutypeid) INTO lruid FROM lrutype;
SELECT COUNT(assemblyid) INTO assid FROM assembly;
SELECT COUNT(assemblytypeid) INTO asstid FROM assemblytype;
SELECT COUNT(propertytypeid) INTO propid FROM propertytype;

lruid := lruid + 1;
INSERT INTO LRUType VALUES (
	lruid,
	'MountAEC',
	'Long device name',
	'ICD reference',
	'ICD date',
	'Long description, ICD abstract.',
	''
);

asstid := asstid + 1;
INSERT INTO AssemblyType VALUES (
	asstid,
	lruid,
	'MountAEC',
	'Long device name',
	'Node id',
	'Channel number',
	'Long description, ICD abstract.',
	''
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'integer',
	'IntegerProperty',
	'errors',
	'0x30001',
	'N',
	'uint32',
	'uint32',
	'na',
	'1',
	'0',
	'0',
	'4294967295',
	'60',
	'0',
	'4294967295',
	'10d',
	'Errors',
	'This monitor point provides the number of communication errors between the AMBSI and the slave device.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'integer',
	'IntegerProperty',
	'transactions',
	'0x30002',
	'N',
	'uint32',
	'uint32',
	'na',
	'1',
	'0',
	'0',
	'4294967295',
	'60',
	'0',
	'4294967295',
	'10d',
	'Transactions',
	'This monitor point provides the number of transactions between the AMBSI and the slave device.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'float',
   	'FloatProperty',
	'temperature',
	'0x30003',
	'N',
	'uint32',
	'float',
	'Degrees Celsius',
	'1.0',
	'0.0',
	'-55.0',
	'125.0',
	'600',
	'-55.0',
	'125.0',
	'8.3f',
	'AMBSI ambient temperature.',
	'The ambient temperature reported by the AMBSI DS1820 device.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_az_motor',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_az_servo_coeff_D',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_az_servo_coeff_E',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_az_servo_coeff_F',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_az_motion_lim',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_el_motor',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_el_servo_coeff_D',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_el_servo_coeff_E',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_el_servo_coeff_F',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_el_motion_lim',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_pt_model_coeff_0',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_pt_model_coeff_1',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_pt_model_coeff_2',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_pt_model_coeff_3',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_pt_model_coeff_4',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_pt_model_coeff_5',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_pt_model_coeff_6',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_pt_model_coeff_7',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_pt_model_coeff_8',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_pt_model_coeff_9',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_subref_abs_posn',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_subref_delta_posn',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_subref_limits',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_subref_status',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_metr_mode',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_metr_tilt_1',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_metr_tilt_2',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_metr_temps_1',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_metr_temps_2',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_metr_temps_3',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_metr_temps_4',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_metr_temps_5',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_metr_temps_6',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_metr_temps_7',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_metr_temps_8',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_metr_temps_9',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_metr_temps_10',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_metr_temps_11',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_metr_temps_12',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_metr_temps_13',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_metr_temps_14',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_metr_temps_15',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_metr_temps_16',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_metr_temps_17',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_metr_temps_18',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_metr_temps_19',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_metr_temps_20',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_metr_temps_21',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_metr_temps_22',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_metr_temps_23',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_metr_temps_24',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_metr_temps_25',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_metr_temps_26',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_power_status',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_ac_status',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_az_motor_power',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_az_motor_driver',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_az_servo_coeff_D',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_az_servo_coeff_E',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_az_servo_coeff_F',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_az_motion_lim',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_el_motor_power',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_el_motor_driver',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_el_servo_coeff_D',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_el_servo_coeff_E',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_el_servo_coeff_F',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_el_motion_lim',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_ip_address',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_pt_model_coeff_0',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_pt_model_coeff_1',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_pt_model_coeff_2',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_pt_model_coeff_3',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_pt_model_coeff_4',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_pt_model_coeff_5',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_pt_model_coeff_6',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_pt_model_coeff_7',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_pt_model_coeff_8',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_pt_model_coeff_9',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_subref_abs_posn',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_subref_delta_posn',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_metr_mode',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_air_conditioning',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);


assid := assid + 1;
INSERT INTO Assembly VALUES (
	assid,
	asstid,
	'CONTROL/ALMA01/MountAEC'
);

assid := assid + 1;
INSERT INTO Assembly VALUES (
	assid,
	asstid,
	'CONTROL/ALMA02/MountAEC'
);

COMMIT;
END;
/
.
QUIT;
