SET SERVEROUTPUT ON;

DECLARE
	lruid NUMBER;
	assid NUMBER;
	asstid NUMBER;
	propid NUMBER;

BEGIN

SELECT COUNT(lrutypeid) INTO lruid FROM lrutype;
SELECT COUNT(assemblyid) INTO assid FROM assembly;
SELECT COUNT(assemblytypeid) INTO asstid FROM assemblytype;
SELECT COUNT(propertytypeid) INTO propid FROM propertytype;

lruid := lruid + 1;
INSERT INTO LRUType VALUES (
	lruid,
	'MountVA',
	'Long device name',
	'ICD reference',
	'ICD date',
	'Long description, ICD abstract.',
	''
);

asstid := asstid + 1;
INSERT INTO AssemblyType VALUES (
	asstid,
	lruid,
	'MountVA',
	'Long device name',
	'Node id',
	'Channel number',
	'Long description, ICD abstract.',
	''
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'integer',
	'IntegerProperty',
	'errors',
	'0x30001',
	'N',
	'uint32',
	'uint32',
	'na',
	'1',
	'0',
	'0',
	'4294967295',
	'60',
	'0',
	'4294967295',
	'10d',
	'Errors',
	'This monitor point provides the number of communication errors between the AMBSI and the slave device.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'integer',
	'IntegerProperty',
	'transactions',
	'0x30002',
	'N',
	'uint32',
	'uint32',
	'na',
	'1',
	'0',
	'0',
	'4294967295',
	'60',
	'0',
	'4294967295',
	'10d',
	'Transactions',
	'This monitor point provides the number of transactions between the AMBSI and the slave device.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'float',
   	'FloatProperty',
	'temperature',
	'0x30003',
	'N',
	'uint32',
	'float',
	'Degrees Celsius',
	'1.0',
	'0.0',
	'-55.0',
	'125.0',
	'600',
	'-55.0',
	'125.0',
	'8.3f',
	'AMBSI ambient temperature.',
	'The ambient temperature reported by the AMBSI DS1820 device.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_az_aux_mode',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_az_enc_status',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_el_aux_mode',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'get_el_enc_status',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_az_aux_mode',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'Data type',
	'Property table name',
	'set_el_aux_mode',
	'RCA',
	'TE-related? Y/N',
	'raw data type',
	'world data type',
	'Unit',
	'Scale',
	'Offset',
	'Min range',
	'Max range',
	'Sampling interval',
	'Graph min',
	'Graph max',
	'printf formatter',
	'Graph title',
	'Description'
);


assid := assid + 1;
INSERT INTO Assembly VALUES (
	assid,
	asstid,
	'CONTROL/ALMA01/MountVA'
);

assid := assid + 1;
INSERT INTO Assembly VALUES (
	assid,
	asstid,
	'CONTROL/ALMA02/MountVA'
);

COMMIT;
END;
/
.
QUIT;
