/* @(#) $Id$
 *
 * Copyright (C) 2001
 * Associated Universities, Inc. Washington DC, USA.
 *
 * Produced for the ALMA project
 *
 * This library is free software; you can redistribute it and/or modify it it 
 * under the terms of the GNU Library General Public License as published by 
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is distributed in the hope that it will be useful but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public 
 * License for more details.
 *
 * You should have received a copy of the GNU Library General Public License 
 * along with this library; if not, write to the Free Software Foundation, 
 * Inc., 675 Massachusetts Ave, Cambridge, MA, 02139, USA.
 *
 * Correspondence concerning ALMA should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */
#ifndef METROLOGYVA_IDL
#define METROLOGYVA_IDL

#include <enumpropMACRO.idl>
#include <ControlInterfaces.idl>
#include <ControlExceptions.idl>
#include <ambDevice.idl>
#include <enumpropStd.idl>
#include <baci.idl>

#pragma prefix "alma"

module Control
{

  /*
   * Possible UPS inverter switch positions
   */
  enum InverterSwitchPosition { INVERTER_SWITCH_OFF, 
		  INVERTER_SWITCH_ON};

  ACS_ENUM(InverterSwitchPosition);


    /**
     * A HardwareDevice providing access to the antenna metrology system.
     *
     * Five tiltmeters and numerous temperature sensors are provided.  Each 
     * tiltmeter gives the tilt in two directions (X,Y) plus the temperature.
     *
     * The interface achieves access to the hardware through the antenna 
     * vendor's Pointing Computer (PTC) via the CAN bus.
     */
    interface MetrologyVA : ControlCommon::AmbDevice
        {
        /**
         *   @if index <br> CLEAR_FAULT_CMD @endif
         */
        void clear_fault_cmd()
	    raises(ControlExceptions::CAMBErrorEx);
    
        /**
         *   @if index <br> INIT_SUBREF_CMD @endif
         */
        void init_subref_cmd()
	    raises(ControlExceptions::CAMBErrorEx);
    
        /**
         *   @if index <br> RESET_PTC_CMD @endif
         */
        void reset_ptc_cmd()
	    raises(ControlExceptions::CAMBErrorEx);
    
        /**
         *   @if index <br> RESET_UPS_CMD_1 @endif
         */
        void reset_ups_cmd_1()
	    raises(ControlExceptions::CAMBErrorEx);
    
        /**
         *   @if index <br> RESET_UPS_CMD_2 @endif
         */
        void reset_ups_cmd_2()
	    raises(ControlExceptions::CAMBErrorEx);
    
        /**
         *   @if index <br> SUBREF_DELTA_ZERO_CMD @endif
         */
        void subref_delta_zero_cmd()
	    raises(ControlExceptions::CAMBErrorEx);
    
        /**
         *   @if index <br> UPS_BATTERY_TEST_CMD_1 @endif
         */
        void ups_battery_test_cmd_1()
	    raises(ControlExceptions::CAMBErrorEx);
    
        /**
         *   @if index <br> UPS_BATTERY_TEST_CMD_2 @endif
         */
        void ups_battery_test_cmd_2()
	    raises(ControlExceptions::CAMBErrorEx);
    
	/**
	 * %Metrology Mode Property.
	 * This Property allows enabling and disabling of the individual parts 
	 * of the metrology pointing correction.  Also see PointModel::mode.
	 *
	 * Byte 0 -
	 * @li bit 0: tiltmeter compensation enabled. @if index <br>
	 *   SET_METR_MODE byte 0 bit 1, and GET_METR_MODE byte 0 bit 1 @endif
	 * @li bit 1: temperature compensation enabled @if index <br>
	 *   SET_METR_MODE byte 0 bit 2, and GET_METR_MODE byte 0 bit 2 @endif
	 * @li bit 2: laser based compensation enabled @if index <br>
	 *   SET_METR_MODE byte 0 bit 3, and GET_METR_MODE byte 0 bit 3 @endif
	 * @li bit 3: friction compensation algorithm enabled @if index <br>
	 *   SET_METR_MODE byte 0 bit 4, and GET_METR_MODE byte 0 bit 4 @endif
	 * @li bit 4: automatic subreflector position correction
	 *   enabled @if index <br> SET_METR_MODE
	 *   byte 0 bit 5, and GET_METR_MODE byte 0 bit 5 @endif
	 */
	// readonly attribute ACS::RWlong mode;

	/**
	 * %Metrology Status Property.
	 * All conditions are faults; once set a call to 
	 * Metrology::clearFaults() is required to reset them.
	 *
	 * Byte 0 -
	 * @li bit 0: local CAN bus error @if index <br>
	 *   GET_METR_EQUIP_STATUS byte 0 bit 0 @endif
	 * @li bit 1: tiltmeter 1 readout error @if index <br>
	 *   GET_METR_EQUIP_STATUS byte 0 bit 1 @endif
	 * @li bit 2: tiltmeter 2 readout error @if index <br>
	 *   GET_METR_EQUIP_STATUS byte 0 bit 2 @endif
	 * @li bit 3: tiltmeter 3 readout error @if index <br>
	 *   GET_METR_EQUIP_STATUS byte 0 bit 3 @endif
	 * @li bit 4: tiltmeter 4 readout error @if index <br>
	 *   GET_METR_EQUIP_STATUS byte 0 bit 4 @endif
	 * @li bit 5: tiltmeter 5 readout error @if index <br>
	 *   GET_METR_EQUIP_STATUS byte 0 bit 5 @endif
	 * @li bit 6: temperature sensor readout error @if index <br>
	 *   GET_METR_EQUIP_STATUS byte 0 bit 6 @endif
	 */
	// readonly attribute ACS::ROpattern status;

	// Properties as listed in the ICD

	/**
	 * Air conditioniing subsystem status
	 *   GET_AC_STATUS @endif
	 */
	readonly attribute ACS::ROlongSeq get_ac_status;

        /**
         * Report ACU 32 bit IP address
         * The IP is reported as a sequence of 4 bytes (byte 0 to byte 3)
         * where byte 0 is the most significant, or first, byte of the address
         */
        readonly attribute ACS::ROlongSeq get_ip_address;

	/**
	 * Metrology system displacement sensor readouts.
         * @if index <br>
	 *   GET_METR_DISPL_N @endif
	 */
	readonly attribute ACS::ROdouble get_metr_displ_1;
	readonly attribute ACS::ROdouble get_metr_displ_2;

        /**
         * TBD
         */
        readonly attribute ACS::ROlongSeq get_metr_equip_status;

        /**
         * TBD
         */
        readonly attribute ACS::ROlongSeq get_metr_mode;

	/**
	 * Temperature Properties.
	 * 100 temperature sensors are provided.
	 * Each one of these properties returns 4 temperatures
         * @if index <br>
	 *   GET_METR_TEMPS_N @endif
	 */
	readonly attribute ACS::ROdoubleSeq get_metr_temps_1;
	readonly attribute ACS::ROdoubleSeq get_metr_temps_2;
	readonly attribute ACS::ROdoubleSeq get_metr_temps_3;
	readonly attribute ACS::ROdoubleSeq get_metr_temps_4;
	readonly attribute ACS::ROdoubleSeq get_metr_temps_5;
	readonly attribute ACS::ROdoubleSeq get_metr_temps_6;
	readonly attribute ACS::ROdoubleSeq get_metr_temps_7;
	readonly attribute ACS::ROdoubleSeq get_metr_temps_8;
	readonly attribute ACS::ROdoubleSeq get_metr_temps_9;
	readonly attribute ACS::ROdoubleSeq get_metr_temps_10;
	readonly attribute ACS::ROdoubleSeq get_metr_temps_11;
	readonly attribute ACS::ROdoubleSeq get_metr_temps_12;
	readonly attribute ACS::ROdoubleSeq get_metr_temps_13;
	readonly attribute ACS::ROdoubleSeq get_metr_temps_14;
	readonly attribute ACS::ROdoubleSeq get_metr_temps_15;
	readonly attribute ACS::ROdoubleSeq get_metr_temps_16;
	readonly attribute ACS::ROdoubleSeq get_metr_temps_17;
	readonly attribute ACS::ROdoubleSeq get_metr_temps_18;
	readonly attribute ACS::ROdoubleSeq get_metr_temps_19;
	readonly attribute ACS::ROdoubleSeq get_metr_temps_20;
	readonly attribute ACS::ROdoubleSeq get_metr_temps_21;
	readonly attribute ACS::ROdoubleSeq get_metr_temps_22;
	readonly attribute ACS::ROdoubleSeq get_metr_temps_23;
	readonly attribute ACS::ROdoubleSeq get_metr_temps_24;
	readonly attribute ACS::ROdoubleSeq get_metr_temps_25;

	/**
	 * Tiltmeter1 X Property (radian). @if index
	 *   GET_METR_TILT_N, N=1,5 @endif
	 */
	readonly attribute ACS::ROdoubleSeq get_metr_tilt_1;
	readonly attribute ACS::ROdoubleSeq get_metr_tilt_2;
	readonly attribute ACS::ROdoubleSeq get_metr_tilt_3;
	readonly attribute ACS::ROdoubleSeq get_metr_tilt_4;
	readonly attribute ACS::ROdoubleSeq get_metr_tilt_5;

        /**
         * TBD
         */
        readonly attribute ACS::ROlongSeq get_other_status;

        /**
         * TBD
         */
        readonly attribute ACS::ROlongSeq get_power_status;

        /**
         * TBD
         */
	readonly attribute ACS::ROdouble get_pt_model_coeff_0;
	readonly attribute ACS::ROdouble get_pt_model_coeff_1;
	readonly attribute ACS::ROdouble get_pt_model_coeff_2;
	readonly attribute ACS::ROdouble get_pt_model_coeff_3;
	readonly attribute ACS::ROdouble get_pt_model_coeff_4;
	readonly attribute ACS::ROdouble get_pt_model_coeff_5;
	readonly attribute ACS::ROdouble get_pt_model_coeff_6;
	readonly attribute ACS::ROdouble get_pt_model_coeff_7;
	readonly attribute ACS::ROdouble get_pt_model_coeff_8;
	readonly attribute ACS::ROdouble get_pt_model_coeff_9;

	/**
	 * TBD
	 **/
	readonly attribute ACS::ROlongSeq get_ptc_error;
	/**
	 * Subreflector absolute position
         * The property returns three values:
         * @li X azis subreflector absolute position in um
         * @li Y azis subreflector absolute position in um
         * @li Z azis subreflector absolute position in um
         * @if index <br>
	 *   GET_SUBREF_ABS_POSN @endif
	 */
	readonly attribute ACS::ROlongSeq get_subref_abs_posn;

	/**
	 * Subreflector delta  position
         * The property returns three values:
         * @li X azis subreflector delta position in um
         * @li Y azis subreflector delta position in um
         * @li Z azis subreflector delta position in um
         * @if index <br>
	 *   GET_SUBREF_DELTA_POSN @endif
	 */
	readonly attribute ACS::ROlongSeq get_subref_delta_posn;

	/**
	 * Subreflector limits
         * The property returns three bytes coded as integers
	 *   GET_SUBREF_LIMITS @endif
	 */
	readonly attribute ACS::ROlongSeq get_subref_limits;

        /**
         * TBD
         */
        readonly attribute ACS::ROlongSeq get_subref_rotation;

	/**
	 * Subreflector status
         * The property returns four bytes coded as integers
	 *   GET_SUBREF_STATUS @endif
	 */
	readonly attribute ACS::ROlongSeq get_subref_status;

	/**
         * TBD
	 */
	readonly attribute ACS::ROlongSeq get_ups_alarms_1;
	readonly attribute ACS::ROlongSeq get_ups_alarms_2;

	/**
         * TBD
	 */
	readonly attribute ACS::ROlongSeq get_ups_battery_output_1;
	readonly attribute ACS::ROlongSeq get_ups_battery_output_2;

 	/**
         * TBD
	 */
	readonly attribute ACS::ROlongSeq get_ups_battery_status_1;
	readonly attribute ACS::ROlongSeq get_ups_battery_status_2;

 	/**
         * TBD
	 */
	readonly attribute ACS::ROlongSeq get_ups_bypass_volts_1;
	readonly attribute ACS::ROlongSeq get_ups_bypass_volts_2;

 	/**
         * TBD
	 */
	readonly attribute ACS::ROlongSeq get_ups_freqs_1;
	readonly attribute ACS::ROlongSeq get_ups_freqs_2;

 	/**
         * TBD
	 */
        readonly attribute ROInverterSwitchPosition get_ups_inverter_sw_1;
        readonly attribute ROInverterSwitchPosition get_ups_inverter_sw_2;

 	/**
         * TBD
	 */
	readonly attribute ACS::ROlongSeq get_ups_inverter_volts_1;
	readonly attribute ACS::ROlongSeq get_ups_inverter_volts_2;

 	/**
         * TBD
	 */
	readonly attribute ACS::ROlongSeq get_ups_output_current_1;
	readonly attribute ACS::ROlongSeq get_ups_output_current_2;

 	/**
         * TBD
	 */
	readonly attribute ACS::ROlongSeq get_ups_output_volts_1;
	readonly attribute ACS::ROlongSeq get_ups_output_volts_2;

        /**
         * TBD
         */
        readonly attribute ACS::ROlongSeq get_ups_status_1;
        readonly attribute ACS::ROlongSeq get_ups_status_2;

        /**
         * TBD
         */
        readonly attribute ACS::RWlongSeq set_metr_mode;

	/**
	 * TBD
	 */
        readonly attribute ACS::RWdouble set_pt_model_coeff_0;
        readonly attribute ACS::RWdouble set_pt_model_coeff_1;
        readonly attribute ACS::RWdouble set_pt_model_coeff_2;
        readonly attribute ACS::RWdouble set_pt_model_coeff_3;
        readonly attribute ACS::RWdouble set_pt_model_coeff_4;
        readonly attribute ACS::RWdouble set_pt_model_coeff_5;
        readonly attribute ACS::RWdouble set_pt_model_coeff_6;
        readonly attribute ACS::RWdouble set_pt_model_coeff_7;
        readonly attribute ACS::RWdouble set_pt_model_coeff_8;
        readonly attribute ACS::RWdouble set_pt_model_coeff_9;

 	/**
	 * TBD
	 */
	readonly attribute ACS::RWlongSeq set_subref_abs_posn;

 	/**
	 * TBD
	 */
	readonly attribute ACS::RWlongSeq set_subref_delta_posn;

 	/**
	 * TBD
	 */
	readonly attribute ACS::RWlongSeq set_subref_rotation;

       };
};

#endif /* ! METROLOGYVA_IDL */
