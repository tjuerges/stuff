#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>
#include <occi.h>
#include <archiveMonitorData.h>

int main(int argc, char* argv[])
{
	oracle::occi::Environment* env(
		oracle::occi::Environment::createEnvironment(
			oracle::occi::Environment::DEFAULT));

	/*
	 * Make sure you use the correct DB and user/pw combo.
	 */
	oracle::occi::Connection* conn(env->createConnection(
		"monitorData", "alma$dba", "aarchive"));
	oracle::occi::Statement* sqlStatement(0);

	std::string queryCommand("SELECT FloatProperty.sample_time, "
		"FloatProperty.value FROM FloatProperty ORDER BY "
		"FloatProperty.sample_time");

	sqlStatement = conn->createStatement(queryCommand);
	oracle::occi::ResultSet* resultSet(0);

	resultSet = sqlStatement->executeQuery();

	while(resultSet->next() == oracle::occi::ResultSet::DATA_AVAILABLE)
	{
		oracle::occi::Timestamp timeStamp(resultSet->getTimestamp(1));

		std::cout << "Print out the first timestamp in the result NOT using "
			"the oracle::occi::Timestamp::toText method."
			<< std::endl;

		unsigned int month, day, hour, minute, second, fs;
		int year;

		timeStamp.getDate(year, month, day);
		timeStamp.getTime(hour, minute, second, fs);
		std::cout << "timestamp = "
			<< year << "-" << month << "-" << day
			<< "T"
			<< hour << ":" << minute << ":" << second << "."
			<< std::setw(9) << std::setfill('0') << fs
			<< std::endl << std::endl;

		std::cout << "Now try to print out the first timestamp in the result "
			"USING the oracle::occi::Timestamp::toText method."
			<< std::endl;

		try
		{
			std::cout << "timestamp = "
				<< timeStamp.toText("YYYY-MM-DD\"T\"HH24:MI:SS.FF9", 9)
				<< std::endl << std::endl;
			std::cout << "timestamp = "
				<< timeStamp.toText("YYYY-MM-DD\"T\"HH24:MI:SS.FF9", 9)
				<< std::endl << std::endl;
		}
		catch(...)
		{
			std::cout << "Oops." << std::endl;
		}
	}

	sqlStatement->closeResultSet(resultSet);
	conn->terminateStatement(sqlStatement);

	env->terminateConnection(conn);
	oracle::occi::Environment::terminateEnvironment(env);

	std::cout << "Now with ArchiveMonitorData class..." << std::endl;

	ArchiveMonitorData* demo = new ArchiveMonitorData("aarchive",
	    "monitorData",
	    "alma$dba");

	ArchiveMonitorData::DeviceDataQuery< CORBA::Float > query;
	query.deviceName = query.serialNumber = "CONTROL/ALMA001/LORR";
	query.propertyName = "VDC_15";

	if(demo->retrieveData< CORBA::Float >(query) == false)
	{
		std::cout << "Nothing came back" << std::endl;
	}
	else
	{
		std::cout << "Data came back!" << std::endl;
		std::cout << query.samples.size() << std::endl;
	}

	delete demo;
	return 0;
};

