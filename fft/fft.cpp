/**
 * $Id$
 */


#include <fstream>
#include <iostream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <sstream>

#include <fftw3.h>


int main(int argc, char* argv[])
{
    if(argc != 3)
    {
        std::cout << "Usage: "
            << argv[0]
            << " sampleRate [double] file name [string, no spaces or quoted]"
                "\n\n";
        return -1;
    }


    // Convert argv[1] to a sample rate.
    double sampleRate(0.0);
    try
    {
        std::istringstream in(argv[1]);
        in >> sampleRate;
    }
    catch(...)
    {
        std::cout << "Cannot convert the sample rate to a double.\n\n";
        return -1;
    }

    if(sampleRate <= 0.0)
    {
        std::cout << "Sample rate has to be > 0.0!\n\n";
        return -1;
    }
    else
    {
        std::cout << "Sample rate = "
            << sampleRate
            << "\n";
    }

    // Convert argv[3] to an input file.
    std::ifstream input(argv[2], std::ios_base::in);
    if(!input)
	{
        std::cout << "No data file!\n\n";
        return -1;
    }
    else
    {
        std::cout << "Reading file "
            << argv[2]
            << ".\n";
    }


    // Read the data.
    std::vector< double > data;
    double dummy(0UL);

    while(true)
    {
        input >> dummy;
        if(input.good() == true)
        {
            data.push_back(dummy);
        }
        else
        {
            input.close();
            break;
        }
    };

    std::cout << "Read "
        << data.size()
        << " elements.\n";

    // Pad data if the size is odd.
    if((data.size() % 2) == 1)
    {
        data.push_back(0.0);
    }

    // Vector for the FFT values.
    // Re = [0; data.size() / 2 - 1]
    // Im = [data.size() / 2; data.size()]
    const unsigned long long size(data.size());
    const unsigned long long halfSize(size / 2ULL);
    std::vector< double > result(size);
    // Create a plan for the FFTW lib.
    fftw_plan plan(fftw_plan_r2r_1d(size, &(data[0]), &(result[0]),
        FFTW_R2HC, FFTW_ESTIMATE));
    // Execute the FFT.
    fftw_execute(plan);
    // Destroy the FFT plan.
    fftw_destroy_plan(plan);

    // Write the FFT data to a file.
    const std::string fileName(std::string(argv[2]) + std::string(".fft"));
    std::ofstream output(fileName.c_str(), std::ios_base::out);
    if(!output)
    {
        std::cout << "Cannot create result file!\n\n";
        return -1;
    }
    else
    {
        // 15 digits precision.
        output.setf(std::ios::fixed, std::ios::floatfield);
        output.precision(10);

        unsigned long long index(0U);
        double amplitude(0.0);
        double phase(0.0);
        std::vector< double >::const_iterator riter(result.begin());
        std::vector< double >::const_iterator iiter(result.begin() + halfSize);
        for(; index < halfSize; ++riter, ++iiter, ++index)
        {
            phase = std::atan2(*iiter, *riter);
            amplitude =
                std::sqrt(((*riter) * (*riter)) +  ((*iiter) * (*iiter)));

            output << index
                << " "
                << *riter
                << " "
                << *iiter
                << " "
                << amplitude
                << " "
                << phase
                << "\n";
        }

        output.close();
    }

    return 0;
}
