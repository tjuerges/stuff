#! /usr/bin/env python
#
# $Id$
#

import math

sigma = 0.5
factor = 1.0 / (sigma * math.sqrt(2.0 * math.pi))
exponentFactor = - 1.0 / (2.0 * sigma**2)
samplerate = 50.0
frequency = 2000.0
min = -10.0
max = 10.0
step = (max - min) / (frequency * samplerate)

print "sample rate = %f, frequency = %f, min = %f, max = %f, step = %f" % (samplerate, frequency, min, max, step)

f = file("data", "w+")

i = min
while i < max:
    f.write("%.12f\n" % (math.sin(frequency * i)))
    i += step

f.close()
