#ifndef ETHERNETDEVICEIMPL_H
#define ETHERNETDEVICEIMPL_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
*/

/************************************************************************
 * This is the baseclass for ethernet device drivers.  It defines the class
 * EthernetDevice which provides methods for monitor and control
 * interactions with the ethernet and the default
 * set of proporties and methods for every device.
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <ControlExceptions.h>
#include <EthernetDeviceExceptions.h>
#include <ethernetDeviceS.h>
#include <controlDeviceImpl.h>
#include <ambCDBAccess.h>
#include <ethernetInterface.h>
#include <string>
#include <baciSmartPropertyPointer.h>
#include <baciROlong.h>


class EthernetDeviceImpl: public ControlCommon::CharacteristicControlDeviceImpl,
    public AmbCDBAccessor,
    public virtual POA_ControlCommon::EthernetDevice
{
    public:
    EthernetDeviceImpl(const ACE_CString& name,
        maci::ContainerServices* containerServices)
        throw(ControlExceptions::CDBErrorExImpl);

    virtual ~EthernetDeviceImpl();

    /*
     * ----------------------- Properties Interface ----------------------
     */
    virtual ACS::ROlong_ptr ethErrors() throw(CORBA::SystemException);

    virtual ACS::ROlong_ptr ethTransactions() throw(CORBA::SystemException);

    /*
     * --------------------- Lifecycle Interface -------------------------
     */
    virtual void initialize() throw(acsErrTypeLifeCycle::LifeCycleExImpl);

    virtual void execute() throw(acsErrTypeLifeCycle::LifeCycleExImpl);

    virtual void cleanUp() throw(acsErrTypeLifeCycle::LifeCycleExImpl);

    virtual void aboutToAbort() throw(acsErrTypeLifeCycle::LifeCycleExImpl);

  /*------------------------- CORBA Interface ------------------------- */

    virtual CORBA::Long createConnection(const char* deviceName,
        const int devicePortNum)
        throw(CORBA::SystemException, EthernetDeviceExceptions::EthernetErrorEx);

    virtual CORBA::Long closeConnection(const int sockfd)
        throw(CORBA::SystemException, EthernetDeviceExceptions::EthernetErrorEx);

    virtual CORBA::Long getSocketfd()
        throw(CORBA::SystemException, EthernetDeviceExceptions::EthernetErrorEx);

    virtual void command(const int socketfd, const char* command)
        throw(CORBA::SystemException, EthernetDeviceExceptions::EthernetErrorEx);

    virtual void monitor(const int socketfd, const char* command, char* getMsg,
        const unsigned long buf_size)
        throw(CORBA::SystemException, EthernetDeviceExceptions::EthernetErrorEx);


    virtual void setSubdeviceError(const char* subdeviceName)
        throw(CORBA::SystemException);

    virtual void clearSubdeviceError(const char* subdeviceName)
        throw(CORBA::SystemException);


  /*------------------------- Sub Lifecycle Methods --------------------
   * These are the lifecycle methods broken down into a start and end portion
   * in order to allow correct inheritance behavior
   */
    virtual void startInitialize() throw(acsErrTypeLifeCycle::LifeCycleExImpl);
    virtual void endInitialize() throw(acsErrTypeLifeCycle::LifeCycleExImpl);

    virtual void startExecute() throw(acsErrTypeLifeCycle::LifeCycleExImpl);
    virtual void endExecute() throw(acsErrTypeLifeCycle::LifeCycleExImpl);

    virtual void startCleanUp() throw(acsErrTypeLifeCycle::LifeCycleExImpl);
    virtual void endCleanUp() throw(acsErrTypeLifeCycle::LifeCycleExImpl);

    virtual void startAboutToAbort() throw(acsErrTypeLifeCycle::LifeCycleExImpl);
    virtual void endAboutToAbort() throw(acsErrTypeLifeCycle::LifeCycleExImpl);

    private:
    SmartPropertyPointer< baci::ROlong > eth_Errors_sp;
    SmartPropertyPointer< baci::ROlong > eth_Trans_sp;

    CORBA::Long socketfd;
    EthernetInterface* interface_p;
    unsigned int devicePortNum;
    std::string deviceName;
};

#endif /* !ETHERNETDEVICEIMPL_H */
