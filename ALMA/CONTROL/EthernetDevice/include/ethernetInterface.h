#ifndef ETHERNETINTERFACE_H
#define ETHERNETINTERFACE_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
*/

/************************************************************************
 * This file defines the ethernetInterface class. It provides an interface
 * to communicate with devices through ethernet.
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <ControlExceptions.h>
#include <EthernetDeviceExceptions.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

class EthernetInterface
{
    public:
    static EthernetInterface* getInstance()
       throw(EthernetDeviceExceptions::EthernetErrorExImpl);
    static void deleteInstance();

    // return the control computer port number
    // int createTCPSocket( )
    // throw(EthernetDeviceExceptions::EthernetErrorExImpl);

    int connectToInstrument(const char* instName, const int instPortNum)
       throw(EthernetDeviceExceptions::EthernetErrorExImpl);

    void closeSocket(const int sockfd)
       throw(EthernetDeviceExceptions::EthernetErrorExImpl);

    int writeMessage(const int socketfd, const char* msg)
       throw(EthernetDeviceExceptions::EthernetErrorExImpl);
    long readMessage(const int sock, const char* command, char* result,
        const unsigned long maxLength)
       throw(EthernetDeviceExceptions::EthernetErrorExImpl);
    /*
     * unsigned long getPortNumber( )
     * throw(EthernetDeviceExceptions::EthernetErrorExImpl); void bindToHost()
     * throw(EthernetDeviceExceptions::EthernetErrorExImpl); void
     * acceptClient() throw(EthernetDeviceExceptions::EthernetErrorExImpl);
     * void listenOnSocket(int sizeOfQueue); int readFromClient()
     * throw(EthernetDeviceExceptions::EthernetErrorExImpl);
     */
    private:
    EthernetInterface() throw(EthernetDeviceExceptions::EthernetErrorExImpl);
    ~EthernetInterface();

    void staticConstructor()
        throw(EthernetDeviceExceptions::EthernetErrorExImpl);
    /*
     * Prevent use of the copy and assignment operator
     */
    EthernetInterface(const EthernetInterface &);
    EthernetInterface& operator=(const EthernetInterface &);

    static EthernetInterface* instance_mp;
};

#endif /* !ETHERNETINTERFACE_H */
