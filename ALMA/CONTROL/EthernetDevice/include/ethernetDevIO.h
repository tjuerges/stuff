#ifndef ETHERNETDEVIO_H
#define ETHERNETDEVIO_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Jan 31, 2007  created
*/

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

#include <baciDevIO.h>
#include <loggingACEMACROS.h>
#include <ambCDBAccess.h>
#include <EthernetDeviceExceptions.h>

class EthernetDeviceImpl;

/**
 * Base class for all Read Only Dev IO classes
 * The class is templated by the type of the value T.
 */
template< class T > class EthernetRODevIO: public DevIO< T >
{
    public:
    /**
     * Constructor
     * @param element std::auto_ptr<const AmbCDBParser>- The parser containing
     * the RO property definition.
     * @exception ControlExceptions::CDBErrorExImpl - Thrown if there is an error
     * accessing the CDB.
     */
    EthernetRODevIO(std::auto_ptr< const AmbCDBParser > cdbElement,
        EthernetDeviceImpl* ethDevice)
        throw(ControlExceptions::CDBErrorExImpl);

    /**
     * Destructor
     */
    virtual ~EthernetRODevIO();

    /**
     * Method to retrieve the value from the Ethernet.
     * This overrides the DevIO<T> call read().
     * @exception ControlExceptions::EthernetErrorExImpl Thrown if there is an
     * error accessing the Ethernet.
     */
    virtual T read(ACS::Time& timestamp)
        throw(EthernetDeviceExceptions::EthernetErrorExImpl);

    private:
    EthernetDeviceImpl* device_p;
    std::string read_command;
    long data_length;
};

//######################################################################
/**
 * Base class for all Read Write Dev IO Classes
 * The class is templated by the type of the value T.
 */
template< class T > class EthernetRWDevIO: public DevIO< T >
{
    public:
    EthernetRWDevIO(std::auto_ptr< const AmbCDBParser > element,
        EthernetDeviceImpl* ethDevice)
        throw(ControlExceptions::CDBErrorExImpl);

    /**
     * Destructor
     */
    virtual ~EthernetRWDevIO();

     /**
     * Method to retrieve the last value written to the Ethernet.
     * This overrides the DevIO<T> call write().
     * @param timestamp ACS::Time& - Location to store the current time.
     */
    virtual T read(ACS::Time& timestamp)
        throw(EthernetDeviceExceptions::EthernetErrorExImpl);

    /**
     * Method to write the value(s) to the Ethernet.
     * This overrides the DevIO<T> call write().
     * @exception ControlExceptions::EthernetErrorExImpl Thrown if there is an
     * error accessing the Ethernet.
     */
    virtual void write(const T& value, ACS::Time& timestamp)
       throw(EthernetDeviceExceptions::EthernetErrorExImpl);

    private:
    /** This stores the last value(s) written to the Ethernet */
    T lastWrittenValue_m;

    EthernetDeviceImpl* device_p;
    std::string cmd_prepend;
    std::string cmd_append;
    long data_length;
};

#endif /* ETHERNETDEVIO_H */
