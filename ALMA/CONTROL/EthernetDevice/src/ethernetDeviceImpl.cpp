/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTAB2ILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
*/

/************************************************************************
*    This is the baseclass for all Ethernet devices
*------------------------------------------------------------------------
*/

#include <ethernetDeviceImpl.h>
#include <ethernetDevIO.h>
#include <maciContainerServices.h>
#include <xercesc/dom/DOM.hpp>
#include <baciROlong.h>


EthernetDeviceImpl::EthernetDeviceImpl(const ACE_CString& name,
    maci::ContainerServices* containerServices)
    throw(ControlExceptions::CDBErrorExImpl):
        ControlCommon::CharacteristicControlDeviceImpl(name, containerServices),
        AmbCDBAccessor(containerServices),
        eth_Errors_sp(this),
        eth_Trans_sp(this),
        socketfd(0)
{
    ACS_TRACE("EthernetDeviceImpl::EthernetDeviceImpl");

    std::auto_ptr< const AmbCDBParser > address(getElement("Address"));

    try
    {
       devicePortNum = address->getLongAttribute("DevicePortNum");
       deviceName = address->getStringAttribute("DeviceName");
    }
    catch(const ControlExceptions::CDBErrorExImpl& ex)
    {
        throw ControlExceptions::CDBErrorExImpl(ex, __FILE__, __LINE__,
            "EthernetDeviceImpl::EthernetDeviceImpl");
    }
}

EthernetDeviceImpl::~EthernetDeviceImpl()
{
    ACS_TRACE("EthernetDeviceImpl::~EthernetDeviceImpl");

    if(getComponent())
    {
        getComponent()->stopAllThreads();
    }
}

/* ----------------------- [ Lifecycle Interface ] ------------------ */

void EthernetDeviceImpl::initialize()
    throw(acsErrTypeLifeCycle::LifeCycleExImpl)
{
    ACS_TRACE("EthernetDeviceImpl::initialize");

    startInitialize();
    endInitialize();
}

void EthernetDeviceImpl::execute() throw(acsErrTypeLifeCycle::LifeCycleExImpl)
{
    ACS_TRACE("EthernetDeviceImpl::execute");

    startExecute();
    endExecute();
}

void EthernetDeviceImpl::cleanUp() throw(acsErrTypeLifeCycle::LifeCycleExImpl)
{
    ACS_TRACE("EthernetDeviceImpl::cleanUp");

    startCleanUp();
    endCleanUp();
}

void EthernetDeviceImpl::aboutToAbort()
throw(acsErrTypeLifeCycle::LifeCycleExImpl)
{
    ACS_TRACE("EthernetDeviceImpl::aboutToAbort");

    startAboutToAbort();
    endAboutToAbort();
}

/* ----------------------[ Sub Lifecycle Interface ] ------------------*/
void EthernetDeviceImpl::startInitialize()
    throw(acsErrTypeLifeCycle::LifeCycleExImpl)
{

    ACS_TRACE("EthernetDeviceImpl::startInitialize");

    if(interface_p == NULL)
    {
        interface_p = EthernetInterface::getInstance();
    }

    // createConnection("CVR1.AOC.NRAO.EDU", 7777);
    createConnection(deviceName.c_str(), devicePortNum);

    /*
     * Now use the cdbName to get the properties
     */
    try
    {
        {
            const ACE_CString propName(cdbName_m + "/errors");
            eth_Errors_sp = new baci::ROlong(propName, getComponent(),
                new EthernetRODevIO< CORBA::Long >(
                    getElement("errors"), this), true);
        }

        {
            const ACE_CString propName(cdbName_m + "/transactions");
            eth_Trans_sp = new baci::ROlong(propName, getComponent(),
                new EthernetRODevIO< CORBA::Long >(
                    getElement("transactions"), this), true);
        }
    }
    catch(const ControlExceptions::CDBErrorExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(__FILE__, __LINE__,
            "EthernetDeviceImpl::startInitalize");
    }
}

void EthernetDeviceImpl::endInitialize()
    throw(acsErrTypeLifeCycle::LifeCycleExImpl)
{
    ACS_TRACE("EthernetDeviceImpl::endInitialize");
}

void EthernetDeviceImpl::startExecute()
    throw(acsErrTypeLifeCycle::LifeCycleExImpl)
{
    ACS_TRACE("EthernetDeviceImpl:::startExecute");
}

void EthernetDeviceImpl::endExecute()
    throw(acsErrTypeLifeCycle::LifeCycleExImpl)
{
    ACS_TRACE("EthernetDeviceImpl::endExecute");
}

void EthernetDeviceImpl::startCleanUp()
    throw(acsErrTypeLifeCycle::LifeCycleExImpl)
{
    ACS_TRACE("EthernetDeviceImpl::startCleanUp");

    // close(socketfd);
    closeConnection(socketfd);
}

void EthernetDeviceImpl::endCleanUp()
    throw(acsErrTypeLifeCycle::LifeCycleExImpl)
{
    ACS_TRACE("EthernetDeviceImpl::endCleanUp");

    if(interface_p != NULL)
    {
        interface_p->deleteInstance();
        interface_p = NULL;
    }
}

void EthernetDeviceImpl::startAboutToAbort()
    throw(acsErrTypeLifeCycle::LifeCycleExImpl)
{
    ACS_TRACE("EthernetDeviceImpl::startAboutToAbort");
}

void EthernetDeviceImpl::endAboutToAbort()
throw(acsErrTypeLifeCycle::LifeCycleExImpl)
{
    ACS_TRACE("EthernetDeviceImpl::endAboutToAbort");

    if(interface_p != NULL)
    {
        interface_p->deleteInstance();
        interface_p = NULL;
    }
}

/* ------------------------- [IMPLEMENTATION ]------------------------- */
CORBA::Long EthernetDeviceImpl::createConnection(const char* hostName,
    const int portNumber)
    throw(CORBA::SystemException, EthernetDeviceExceptions::EthernetErrorEx)
{
    ACS_TRACE("EthernetDeviceImpl::createConnection");

    try
    {
        socketfd = interface_p->connectToInstrument(hostName, portNumber);
    }
    catch(const EthernetDeviceExceptions::EthernetErrorExImpl& ex)
    {
        throw EthernetDeviceExceptions::EthernetErrorExImpl(ex, __FILE__,
            __LINE__, "EthernetDeviceImpl::createConnection");
    }

    return socketfd;
}

CORBA::Long EthernetDeviceImpl::closeConnection(const int sockfd)
    throw(CORBA::SystemException, EthernetDeviceExceptions::EthernetErrorEx)
{
    try
    {
        interface_p->closeSocket(sockfd);
        socketfd = 0;
    }
    catch(const EthernetDeviceExceptions::EthernetErrorExImpl& ex)
    {
        throw EthernetDeviceExceptions::EthernetErrorExImpl(ex, __FILE__,
            __LINE__, "EthernetDeviceImpl::closeSocket");
    }

    return CORBA::Long(1);
}

CORBA::Long EthernetDeviceImpl::getSocketfd() throw(CORBA::SystemException,
    EthernetDeviceExceptions::EthernetErrorEx)
{
    try
    {
       assert(socketfd >= 0);
    }
    catch(const EthernetDeviceExceptions::EthernetErrorExImpl& ex)
    {
        throw EthernetDeviceExceptions::EthernetErrorExImpl(ex, __FILE__,
            __LINE__, "EthernetDeviceImpl::getSocketfd");
    }

    return socketfd;
}

void EthernetDeviceImpl::monitor(const int socketfd, const char* command,
    char* getMsg, const unsigned long buf_size)
    throw(CORBA::SystemException, EthernetDeviceExceptions::EthernetErrorEx)
{
    try
    {
       // command = "FREQ:CW?\n";
       char inputCommand[256];

        strncpy(inputCommand, command, strlen(command));
        inputCommand[strlen(command)] = '\n';
        inputCommand[strlen(command) + 1] = '\0';
        int r(interface_p->readMessage(socketfd, inputCommand, getMsg, buf_size));
    std::cout << std::endl
        << "r = *"
        << r
        << "*, getMsg = *"
        << getMsg
        << std::endl
        << "*"
        << std::endl;
    }
    catch(const EthernetDeviceExceptions::EthernetErrorExImpl& ex)
    {
        throw EthernetDeviceExceptions::EthernetErrorExImpl(ex, __FILE__,
            __LINE__, "EthernetDeviceImpl::monitor");
    }
}

void EthernetDeviceImpl::command(const int socketfd, const char* command)
    throw(CORBA::SystemException, EthernetDeviceExceptions::EthernetErrorEx)
{
    try
    {
        char inputCommand[256];

        strncpy(inputCommand, command, strlen(command));
        inputCommand[strlen(command)] = '\n';
        inputCommand[strlen(command) + 1] = '\0';
        interface_p->writeMessage(socketfd, inputCommand);
        // interface_p->writeMessage(socketfd, command);
    }
    catch(const EthernetDeviceExceptions::EthernetErrorExImpl& ex)
    {
        throw EthernetDeviceExceptions::EthernetErrorExImpl(ex, __FILE__,
            __LINE__, "EthernetDeviceImpl::command");
    }
}

void EthernetDeviceImpl::setSubdeviceError(const char* subdeviceName)
    throw(CORBA::SystemException)
{
    ACS_TRACE("EthernetDeviceImpl::setSubdeviceError Called");
}

void EthernetDeviceImpl::clearSubdeviceError(const char* subdeviceName)
    throw(CORBA::SystemException)
{
    ACS_TRACE("EthernetDeviceImpl::clearSubdeviceError Called");
}

/* ------------- Property Interface ------------------------------ */
ACS::ROlong_ptr EthernetDeviceImpl::ethErrors() throw(CORBA::SystemException)
{
    ACS::ROlong_var prop = ACS::ROlong::_narrow(
        eth_Errors_sp->getCORBAReference());

    return prop._retn();
}

ACS::ROlong_ptr EthernetDeviceImpl::ethTransactions()
    throw(CORBA::SystemException)
{
    ACS::ROlong_var prop = ACS::ROlong::_narrow(
        eth_Trans_sp->getCORBAReference());

    return prop._retn();
}

/////////////////////////////////////////////////
// MACI DLL support functions
/////////////////////////////////////////////////
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(EthernetDeviceImpl)
