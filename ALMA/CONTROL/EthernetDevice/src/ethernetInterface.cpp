/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2005
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* dguo  2005-06-30  created
*------------------------------------------------------------------------
*/

static char *rcsId = "@(#) $Id$";
static void *use_rcsId = ((void)&use_rcsId, (void *)&rcsId);

#include <ethernetInterface.h>
#include <iostream>
#include <string>


EthernetInterface* EthernetInterface::instance_mp = NULL;


EthernetInterface::EthernetInterface()
    throw(EthernetDeviceExceptions::EthernetErrorExImpl)
{
    ACS_TRACE("EthernetInterface::EthernetInterface");

    try
    {
        staticConstructor();
    }
    catch(const EthernetDeviceExceptions::EthernetErrorExImpl& ex)
    {
        throw EthernetDeviceExceptions::EthernetErrorExImpl(ex, __FILE__,
            __LINE__, "EthernetInterface::EthernetInterface");
    }
}

void EthernetInterface::staticConstructor()
    throw(EthernetDeviceExceptions::EthernetErrorExImpl)
{
    ACS_TRACE("EthernetInterface::staticConstructor");
}

EthernetInterface::~EthernetInterface()
{
    ACS_TRACE("EthernetInterface::~EthernetInterface");
}

EthernetInterface* EthernetInterface::getInstance()
    throw(EthernetDeviceExceptions::EthernetErrorExImpl)
{
    ACS_TRACE("EthernetInterface::getInstance");

    try
    {
        if(instance_mp == NULL)
        {
          instance_mp = new EthernetInterface();
        }

        return instance_mp;
    }
    catch(const EthernetDeviceExceptions::EthernetErrorExImpl& ex)
    {
        throw EthernetDeviceExceptions::EthernetErrorExImpl(ex, __FILE__,
        __LINE__, "EthernetInterface::getInstance");
    }
}

void EthernetInterface::deleteInstance()
{
    ACS_TRACE("EthernetInterface::deleteInstance");

    /*
     * No Instances left, delete the instance
     */
    delete instance_mp;
    instance_mp = NULL;
}

// ----------------- Connection --------------------------------------------
int EthernetInterface::connectToInstrument(const char* deviceName,
    const int instPortNum)
    throw(EthernetDeviceExceptions::EthernetErrorExImpl)
{
    ACS_TRACE("EthernetInterface::connectToInstrument");

    // serverDevice is the instrument
    struct hostent* serverDevice;

    // inst_addr is the instrument addr.
    struct sockaddr_in inst_addr;
    int sockfd;
    int connect_error;

    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    try
    {
       serverDevice = gethostbyname(deviceName);
    }
    catch(const EthernetDeviceExceptions::EthernetErrorExImpl& ex)
    {
        throw EthernetDeviceExceptions::EthernetErrorExImpl(ex, __FILE__,
        __LINE__, "EthernetInterface::connectToInstrument::gethostbyname");
    }

    std::memset(&inst_addr, 0, sizeof(struct sockaddr_in));
    std::memcpy(&inst_addr.sin_addr.s_addr, serverDevice->h_addr,
        serverDevice->h_length);

    inst_addr.sin_family = AF_INET;
    inst_addr.sin_port = htons(static_cast< unsigned short >(instPortNum));
    try
    {
        connect_error = connect(sockfd,
            reinterpret_cast< const struct sockaddr* >(&inst_addr),
            sizeof(struct sockaddr_in));
    }
    catch(const EthernetDeviceExceptions::EthernetErrorExImpl& ex)
    {
        throw EthernetDeviceExceptions::EthernetErrorExImpl(ex, __FILE__,
            __LINE__, "EthernetInterface::connectToInstrument");
    }

    return sockfd;
}

void EthernetInterface::closeSocket(const int sockfd)
    throw(EthernetDeviceExceptions::EthernetErrorExImpl)
{
    ACS_TRACE("EthernetInterface::closeSocket");

    try
    {
        close(sockfd);
    }
    catch(const EthernetDeviceExceptions::EthernetErrorExImpl& ex)
    {
        throw EthernetDeviceExceptions::EthernetErrorExImpl(ex, __FILE__,
            __LINE__, "EthernetInterface::closeSocket");
    }
}

// ------------------------ write --------------------------------
int EthernetInterface::writeMessage(const int sock, const char* msg)
    throw(EthernetDeviceExceptions::EthernetErrorExImpl)
{
    ACS_TRACE("EthernetInterface::writeMessage");

    if(std::strchr(msg, '\n') == NULL)
    {
        std::string trace("EthernetInterface::writeMessage: "
            "Warning: missing newline on command: ");
        trace.append(msg);
        ACS_TRACE(trace.c_str());
    }

    int bytes(-1);
    try
    {
        bytes = send(sock, msg, std::strlen(msg), 0);
    }
    catch(const EthernetDeviceExceptions::EthernetErrorExImpl& ex)
    {
        throw EthernetDeviceExceptions::EthernetErrorExImpl(ex, __FILE__,
            __LINE__, "EthernetInterface::writeMessage");
    }

    return bytes;
}

// --------------- help function for read -------------------------------
char* recv_line(const int sock, char* buffer, const int maxLength)
{
    ACS_TRACE("EthernetInterface::recv_line");

    FILE* instFile(fdopen(sock, "r+"));
    if(instFile == NULL)
    {
        ACS_TRACE("EthernetInterface::recv_line: Unable to create FILE* structure!");
        return NULL;
    }

    return fgets(buffer, maxLength, instFile);
}

// --------------------- read ------------------------------------------------
// This method is almost the same as the queryInstrument() function in
//  Programming Guide, Agilent Technologies, PSG Family Signal Generators.
// It may be needed modified for generic applications.

long EthernetInterface::readMessage(const int sock, const char* command,
    char* result, const unsigned long maxLength)
    throw(EthernetDeviceExceptions::EthernetErrorExImpl)
{
    ACS_TRACE("EthernetInterface::readMessage");

    // * Send command to signal generator
    try
    {
        writeMessage(sock, command);
    }
    catch(const EthernetDeviceExceptions::EthernetErrorExImpl& ex)
    {
        throw EthernetDeviceExceptions::EthernetErrorExImpl(ex, __FILE__,
            __LINE__, "EthernetInterface::readMessage");
    }

    // * Read response from signal generator
    char tmp_buf[8];
    int count(recv(sock, tmp_buf, 1, 0));    /* read 1 char */
    long ch(tmp_buf[0]);
    if((count < 1) || (ch == EOF) || (ch == '\n'))
    {
        *result = '\0';    /* null terminate result for ascii */
        return 0;
    }

    long resultBytes(0L);
    do
    {
        if(ch == '#')
        {        /* binary data encountered */
            long numDigits(0L);
            long numBytes(0L);

            count = recv(sock, tmp_buf, 1, 0);    /* read 1 char */
            ch = tmp_buf[0];
            if((count < 1) || (ch == EOF))
            {
                break;    /* End of file */
            }
            else if(ch < '0' || ch > '9')
            {
                break;    /* unexpected char */
            }

            numDigits = ch - '0';
            if(numDigits != 0)
            {    /* read numDigits bytes into result string. */
                count = recv(sock, result, (int)numDigits, 0);
                result[count] = 0;    /* null terminate */
                numBytes = std::atol(result);
            }

            if(numBytes != 0L)
            {
                resultBytes = 0;
                /*
                 * Loop until getting all the bytes requested.
                 */
                do
                {
                    int rcount(recv(sock, result, static_cast< int >(numBytes), 0));
                    resultBytes += rcount;
                    result += rcount;    /* Advance pointer */
                }
                while(resultBytes < numBytes);

    /************************************************************
     * For LAN dumps, there is always an extra trailing newline
     * Since there is no EOI line. For ASCII dumps this is
     * great but for binary dumps, it is not needed.
     ***********************************************************/
                if(resultBytes == numBytes)
                {
                    char junk;

                    count = recv(sock, &junk, 1, 0);
                }
            }
            else
            {    /* indefinite block ... dump til we can an
                 * extra line feed */
                do
                {
                    if(recv_line(sock, result, maxLength) == NULL)
                    {
                        break;
                    }
                    else if(strlen(result) == 1 && *result == '\n')
                    {
                        break;
                    }

                    resultBytes += std::strlen(result);
                    result += std::strlen(result);
                }
                while(1);
            }
        }
        else
        {        /* ASCII response (not a binary block) */

            *result = static_cast< char >(ch);
            if(recv_line(sock, result + 1, maxLength - 1) == NULL)
            {
                return 0;
            }

            /*
             * REMOVE trailing newline, if present. And terminate
             * string.
             */
            resultBytes = std::strlen(result);
            if(result[resultBytes - 1] == '\n')
            {
                --resultBytes;
            }

            result[resultBytes] = '\0';
        }
    }
    while(0);

    return resultBytes;
}                // end of readMessage

/*___oOo___*/
