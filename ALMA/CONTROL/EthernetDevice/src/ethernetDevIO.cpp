/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2005
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
*/

#include <ethernetDevIO.h>
#include <ethernetDeviceImpl.h>
#include <loggingACEMACROS.h>
#include <baciTime.h>
#include <memory>
#include <cstdlib>
#include <xercesc/dom/DOM.hpp>


/* ------------------------- Single Value Classes ---------------------- */
template< class T > EthernetRODevIO< T >::EthernetRODevIO(
    std::auto_ptr< const AmbCDBParser > cdbElement,
    EthernetDeviceImpl* ethDevice)
    throw(ControlExceptions::CDBErrorExImpl):
        device_p(ethDevice)
{
    ACS_TRACE("EthernetRODevIO::EthernetRODevIO");
    std::auto_ptr< const AmbCDBParser > ioElement;

    try
    {
        DevIO< T >::m_initialize = cdbElement->getBoolAttribute("initialize");
        ioElement = cdbElement->getElement("ethernet:ethMap");
        read_command = ioElement->getStringAttribute("read_command");
        data_length = ioElement->getLongAttribute("data_length");
    }
    catch(const ControlExceptions::CDBErrorExImpl& ex)
    {
        throw ControlExceptions::CDBErrorExImpl(ex, __FILE__, __LINE__,
            "EthernetRODevIO::EthernetRODevIO");
    }
}

template< class T > EthernetRODevIO< T >::~EthernetRODevIO()
{
   ACS_TRACE("EthernetRODevIO::~EthernetRODevIO");
}

template< class T > T EthernetRODevIO< T >::read(ACS::Time& timestamp)
    throw(EthernetDeviceExceptions::EthernetErrorExImpl)
{
    ACS_TRACE("EthernetRODevIO::Read");

    char* getMessage = static_cast< char *>(std::malloc(data_length));

    try
    {
        device_p->monitor(device_p->getSocketfd(), read_command.c_str(),
            getMessage, static_cast < unsigned long >(data_length));
    }
    catch(const EthernetDeviceExceptions::EthernetErrorExImpl& ex)
    {
        std::free(getMessage);
        throw EthernetDeviceExceptions::EthernetErrorExImpl(ex, __FILE__,
            __LINE__, "EthernetRWMap::read");
    }

    timestamp = baci::getTimeStamp();
    const T returnValue(static_cast< T >(std::atof(getMessage)));
    std::free(getMessage);

    return returnValue;
}


template< class T > EthernetRWDevIO< T >::EthernetRWDevIO(
    std::auto_ptr< const AmbCDBParser > element, EthernetDeviceImpl * ethDevice)
    throw(ControlExceptions::CDBErrorExImpl):
        device_p(ethDevice)
{
    ACS_TRACE("EthernetRWDevIO::EthernetRWDevIO");

    try
    {
        DevIO< T >::m_initialize = element->getBoolAttribute("initialize");
        std::auto_ptr < const AmbCDBParser > ioElement(
            element->getElement("ethernet:ethMap"));
        cmd_prepend = ioElement->getStringAttribute("write_prepend");
        cmd_append = ioElement->getStringAttribute("write_append");
        data_length = ioElement->getLongAttribute("data_length");
    }
    catch(const ControlExceptions::CDBErrorExImpl& ex)
    {
        throw ControlExceptions::CDBErrorExImpl(ex, __FILE__, __LINE__,
            "EthernetRWDevIO::EthernetRWDevIO");
    }
};

template< class T > EthernetRWDevIO< T >::~EthernetRWDevIO()
{
   ACS_TRACE("EthernetRWDevIO::~EthernetRWDevIO");
}

template< class T > T EthernetRWDevIO< T >::read(ACS::Time& timestamp)
    throw(EthernetDeviceExceptions::EthernetErrorExImpl)
{
   ACS_TRACE("EthernetRWDevIO::read");

   timestamp = baci::getTimeStamp();
   return lastWrittenValue_m;
}

template< class T > void EthernetRWDevIO< T >::write(const T& value,
    ACS::Time& timestamp)
    throw(EthernetDeviceExceptions::EthernetErrorExImpl)
{
    ACS_TRACE("EthernetRWDevIO::write");

    char writeCommand[data_length];
    const char *prepend = cmd_prepend.data();
    const char *append = cmd_append.data();
    sprintf(writeCommand, "%s %lf %s", prepend, static_cast< double >(value),
        append);

    try
    {
       device_p->command(device_p->getSocketfd(), writeCommand);
    }
    catch(const EthernetDeviceExceptions::EthernetErrorExImpl& ex)
    {
        throw EthernetDeviceExceptions::EthernetErrorExImpl(ex, __FILE__,
        __LINE__, "EthernetDevIO::write");
    }

    timestamp = baci::getTimeStamp();
    lastWrittenValue_m = value;
}

template class EthernetRODevIO< CORBA::Long >;
template class EthernetRWDevIO< CORBA::Long >;
template class EthernetRODevIO< CORBA::Double >;
template class EthernetRWDevIO< CORBA::Double >;
