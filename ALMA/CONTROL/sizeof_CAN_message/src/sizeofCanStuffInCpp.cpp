#include <iostream>
#include <tpmc901.h>
#include <ambDefs.h>

int main(int argc, char* argv[])
{
    std::cout << "Size of one TPMC901 CAN-bus message in bytes = "
        << sizeof(TP901_MSG_BUF)
        << std::endl;

    std::cout << "Size of one AMB message in bytes = "
        << sizeof(AmbMessage_t)
        << std::endl;

    std::cout << "Size of one AMB response in bytes = "
        << sizeof(AmbResponse_t)
        << std::endl;

    return 0;
};

