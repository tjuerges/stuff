/**
 * @(#) $Id$
 */

#include <stdio.h>
#include <tpmc901.h>
#include <ambDefs.h>

int main(int argc, char* argv[])
{
    printf("Size of one TPMC901 CAN-bus message in bytes = %u\n",
        sizeof(TP901_MSG_BUF));

    printf("Size of one AMB message in bytes = %u\n",
        sizeof(AmbMessage_t));

    printf("Size of one AMB response in bytes = %u\n",
        sizeof(AmbResponse_t));

    return 0;
};

