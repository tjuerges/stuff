#ifndef deviceMonitoringTimerThread_h
#define deviceMonitoringTimerThread_h
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2006
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Jun 2, 2006  created
*/

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

#include <acsThread.h>

/**
 * Forward declarations.
 */
class DeviceMonitoringManager;

/**
 * An ACS::thread which triggers every 60 seconds the archival of MonitorPoints.
 */
class DeviceMonitoringTimerThread:public ACS::Thread
{
    public:
    /**
     * Constructor.
     * @param name The thread name.
     * @param manager Instance of the DeviceMonitoringManager. Is used to
     * call DeviceMonitoringManager::sendDataToArchiveNow.
     * @param suspended: This thread starts right after the creation through
     * the ACS::ThreadManager.
     * @param responseTime Passed to the ACS::Thread base class.
     * @param sleepTime Set to 60 seconds.
     */
    DeviceMonitoringTimerThread(const ACE_CString& name,
        DeviceMonitoringManager* manager,
        const ACS::TimeInterval& responseTime = ACS::TimeInterval(600 * 1000 * 1000),
        const ACS::TimeInterval& sleepTime = ACS::TimeInterval(600 * 1000 * 1000));

    /**
     * Destructor.
     */
    virtual ~DeviceMonitoringTimerThread();

    /**
     * The thread worker method. Run every 60 seconds.
     */
    virtual void runLoop();


    private:
    /**
     * Instance of the DeviceMonitoringManager class. Used to call every
     * 60 seconds DeviceMonitoringManager::sendDataToArchiveNow.
     */
    DeviceMonitoringManager* manager;
};
#endif
