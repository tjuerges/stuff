#ifndef deviceMonitoringImpl_h
#define deviceMonitoringImpl_h
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2006
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Jun 2, 2006  created
*/

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

#include <deviceMonitoringS.h>
#include <acscomponentImpl.h>
#include <deviceMonitoringManager.h>

/**
 * Forward declarations.
 */
namespace maci
{
    class ContainerServices;
};


/**
 * The implementation of the DeviceMonitorIF IDL interface.
 */
class DeviceMonitoringImpl: public virtual acscomponent::ACSComponentImpl,
	public POA_Control::DeviceMonitoring
{
	public:
    /**
     * Constructor.
     * @param name The name of this component.
     * @param container A pointer to a maci::ContainerService instance.
     */
    DeviceMonitoringImpl(const ACE_CString& name,
        maci::ContainerServices* container);

    /**
     * Destructor.
     */
    virtual ~DeviceMonitoringImpl();

    /**
     * Implementation of the stop method.
     */
    virtual void stop();

    /**
     * Implementation of the start method.
     */
    virtual void start();

    /**
     * Implementation of the storeToDisc method.
     */
    virtual void storeToArchive(CORBA::Boolean onoff);

    /**
     * Implementation of the storeToDisc method.
     */
    virtual void storeToDisc(CORBA::Boolean onoff);

    /**
     * ACS::Component lifecycle method.
     * \exception acsErrTypeLifeCycle::LifeCycleExImpl
     */
    virtual void execute();

    /**
     * ACS::Component lifecycle method.
     * \exception acsErrTypeLifeCycle::LifeCycleExImpl
     */
    virtual void cleanUp();


	private:
    /**
     * No copy constructor.
     */
    DeviceMonitoringImpl(const DeviceMonitoringImpl&);

    /**
     * No copy assignment constructor.
     */
    DeviceMonitoringImpl& operator=(const DeviceMonitoringImpl&);

    /**
     * Pointer to a maci::ContainerServices instance.
     */
    maci::ContainerServices* container;

    /**
     * Pointer to the instance of the DeviceMonitoringManager. It does
     * the actual work.
     */
    DeviceMonitoringManager* manager;
};
#endif
