#ifndef archivingThread_h
#define archivingThread_h
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2006
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Jun 2, 2006  created
*/

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

#include <acscommonC.h>
#include <acsThread.h>
#include <deviceQueue.h>
#include <string>

class DeviceMonitoringManager;

/**
 * The thread which controls the VOTable creation and the storage of them into
 * the archive. Every 60 seconds a new instance of this is created. After the
 * work is done, the thread destroys itself.
 */
class ArchivingThread: public ACS::Thread
{
    public:
    /**
     * Structure which is filled by the DeviceMonitoringManager class.
     * @param manager Pointer to the instance of the DeviceMonitoringManager.
     * @param archiver Pointer to the Archiver instance, which actually
     * does the storage work.
     * @param queue Queue of devices which keeps vecorts of MonitorPoints.
     * The data in this queue is max. 60 seconds old, since every 60 seconds
     * a new instance of this thread is created and a new queue is used.
     * @param my_name The name of this thread. For logging purposes only.
     */
    typedef struct
    {
        DeviceMonitoringManager* manager;
        const DeviceQueue* queue;
    } structForThread;

    /**
     * Constructor.
     * @param name Passed to the parent class ACS::Thread.
     * @param data A structForThread struct, which contains essential data
     * for this thread.
     * @param suspended Set to false. This thread starts working right after
     * the ACS::ThreadManager has created it.
     * @param responseTime The timespan it takes, until this thread responses
     * to inquiries from the manager.
     * @param sleepTime Timespan how long this thread sleeps after every
     * runLoop cycle.
     */
    ArchivingThread(const ACE_CString& name,
        const structForThread& data,
        const ACS::TimeInterval& responseTime = ACS::TimeInterval(10 * 1000 * 1000),
        const ACS::TimeInterval& sleepTime = ACS::TimeInterval(50 * 1000 * 1000));

    /**
     * Destructor.
     */
    virtual ~ArchivingThread();

    /**
     * The worker method for this thread. Here are done the following things:
     * The Queue class is asked to prepare the data for every device. Then
     * DeviceMonitoringManager::sendDataToArchiveNow() is called for every
     * device entry in the queue.
     */
    virtual void runLoop();

    /**
     * The device queue is cleared, i.e. the memory is freed.
     */
    virtual void onStop();

    private:
    std::string my_name;

    DeviceMonitoringManager* manager;
    DeviceQueue queue;
    ACS::ThreadBase::SleepReturn sleepReturn;
};
#endif
