#ifndef deviceMonitoringManager_h
#define deviceMonitoringManager_h
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2006
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Jun 2, 2006  created
*/

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

#include <vector>
#include <string>
#include <ace/Thread_Mutex.h>
#include <acsncArchiveConsumer.h>
#include <bulkDataReceiverC.h>
#include <device.h>

#ifdef DEBUG_TIMECONSUMPTION
#include <High_Res_Timer.h>
#endif

/**
 * Forward declarations.
 */
class maci::ContainerServices;
class MonitorPoint;
class DeviceQueue;
class DeviceMonitoringTimerThread;

/**
 * The actual work horse of the DeviceMonitortingIF.
 * Derived from nc::ArchiveConsumer::ArchiveHandler to receive archiving channel
 * events. Through this channel, ACS propagates samplings of properties, which
 * shall be stored into the archive.
 *
 */
class DeviceMonitoringManager: public nc::ArchiveConsumer::ArchiveHandler
{
	public:
    /**
     * Constructor.
     * Used from the DeviceMonitoringIFImpl.
     * @param container Pointer to an active maci::ContainerService instance.
     */
    DeviceMonitoringManager(maci::ContainerServices* container);

    /**
     * Constructor.
     * Used from the tests.
     * @param _path The path where the VOTable should be stored.
     */
    DeviceMonitoringManager(const std::string& _path);

    /**
     * Destructor.
     */
    virtual ~DeviceMonitoringManager();

    /**
     * Called from DeviceMonitoringImpl::stop(). Destroys all threads,
     * cleans up and stores all data which had not already been stored.
     */
    void stop();

    /**
     * Enable/disable storage to archive.
     */
    void storeToArchive(const bool onoff)
    {
        Device::storeToArchive(onoff);
    };

    /**
     * Enable/disable storage to archive.
     */
    void storeToDisc(const bool onoff)
    {
        Device::storeToDisc(onoff);
    };

    /**
     * This method is called every 60 seconds by the DeviceMonitoringTimerThread
     * thread. Creates a new DeviceQueue which collects the incoming
     * MonitorPoints. Creates a new ArchivingThread, too. This thread takes
     * care of creating the VOTable file from the entries in the old
     * DeviceQueue and triggers the archival.
     * \exception acsthreadErrType::CanNotCreateThreadExImpl
     */
    void sendDataToArchiveNow();

    /**
     * The overridden receiver method of the nc::ArchiveConsumer::ArchiveHandler.
     * @param timeStamp time stamp of the property sampling.
     * @param device The device name of the property.
     * @param property The property name.
     * @param value The value of the property.
     */
    virtual void receive(ACS::Time timeStamp, const std::string& device,
        const std::string& property, const CORBA::Any& value);

    /**
     * Return the archive computer name + port.
     */
    static const std::string getArchiveComputer()
    {
        return archiveComputer;
    };

    /**
     * Return the archive database.
     */
    static const std::string getArchiveDb()
    {
        return archiveDb;
    };

    /**
     * Return the archive user name.
     */
    static const std::string getArchiveUserName()
    {
        return archiveUserName;
    };

    /**
     * Return the archive password.
     */
    static const std::string getArchivePassword()
    {
        return archivePassword;
    };

	private:
    /**
     * No default constructor allowed.
     */
    DeviceMonitoringManager();

    /**
     * No copyconstructor allowed.
     */
    DeviceMonitoringManager(const DeviceMonitoringManager&);

    /**
     * No assignment operator allowed.
     */
    DeviceMonitoringManager& operator=(const DeviceMonitoringManager&);

    /**
     * Pointer to an active maci::ContainerServices instance.
     */
    maci::ContainerServices* container;

    /**
     * The active DeviceQueue.
     */
    DeviceQueue* queue;

    /**
     * The timer thread, which triggers every 60 seconds the
     * sendDataToArchiveNow() method.
     */
    DeviceMonitoringTimerThread* timer;

    /**
     * The instance of the nc::ArchiveConsumer.
     */
    nc::ArchiveConsumer* consumer;

    /**
     * Concurrency lock.
     */
    ACE_Thread_Mutex manager_mutex;

    /**
     * Path where the data is stored in case of a non-functional
     * ARCHIVING_MONITORRECEIVER. Setup through the environment variable
     * $ACSDATA. If it is undefined, the current path is used.
     */
    std::string path;

    /**
     * Takes care, that never threads with the same names are created. Every
     * thread name has this (increased) number appended.
     */
    unsigned long thread_counter;

    /**
     * Keeps the name of the last thread which had been successfully
     * created. Used when the component is shutdown and all running threads
     * shall be stopped before.
     */
    std::string lastArchivingThreadName;

    /**
     * This value takes care of the limit of thread_counter. If thread_counter
     * reaches this value, it is reset to 0.
     */
    static const unsigned long unsigned_long_limit;

    /**
     * Name of the timer thread which triggers archive storing.
     */
    static const std::string Timer_Name;

    /**
     * FQDN + port of archive computer.
     */
    static std::string archiveComputer;

    /**
     * DB-name of archive.
     */
    static std::string archiveDb;

    /**
     * Archive user name.
     */
    static std::string archiveUserName;

    /**
     * Archive password.
     */
    static std::string archivePassword;

    #ifdef DEBUG_TIMECONSUMPTION
    ACE_High_Res_Timer highResTimer;
    static const char* highResTimer_text;
    #endif
};
#endif
