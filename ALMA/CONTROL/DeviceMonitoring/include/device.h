#ifndef device_h
#define device_h
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2006
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Jun 2, 2006  created
*/

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

#include <monitorPoint.h>
#include <Guard_T.h>
#include <Mutex.h>
#include <acsThreadBase.h>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <acstime.h>

/**
 * This class keeps MonitorPoints for every property in a std::multimap. The key
 * is the property name, the data are the property values received through the
 * archiving channel.
 * The devices are distinguishable through their bar code/serial number.
 */
class Device
{
    public:
    /**
     * Typedef to shorten the type name.
     */
    typedef std::map< std::string, std::string > FilenameMap;

    /**
     * Typedef to shorten the type name.
     */
    typedef std::map< std::string, std::string >::const_iterator
        FilenameMap_citer;

    /**
     * Typedef to shorten the type name.
     */
    typedef std::map< std::string, std::string >::iterator FilenameMap_iter;

    /**
     * A std::vector of this is passed to the DeviceQueue. Every device is
     * one entry in the vector. This struct is then filled there and the, to
     * characters converted MonitorPoint information for every property of
     * the device.
     */
    typedef struct
    {
        std::vector< unsigned char > data;
    } ArchiveData;

    /**
     * Constructor.
     * @param mp MonitorPoint which is directly added to the new device.
     */
    Device(const MonitorPoint& mp);

    /**
     * Copyconstructor.
     */
    Device(const Device&);

    /**
     * Assignment operator.
     */
    Device& operator=(const Device&);

    /**
     * Destructor.
     */
    ~Device();

    /**
     * Enable/disable storage to archive.
     */
    static void storeToArchive(const bool onoff)
    {
        ACE_Guard< ACE_Mutex > guard(Device::storeTo_mutex);

        storeToRealDb = onoff;
    };

    /**
     * Enable/disable storage to archive.
     */
    static void storeToDisc(const bool onoff)
    {
        ACE_Guard< ACE_Mutex > guard(Device::storeTo_mutex);

        storeToFile = onoff;
    };

    /**
     * Add a MonitorPoint to this device.
     * @param mp MonitorPoint to be added.
     */
    void addMonitorPoint(const MonitorPoint& mp);

    /**
     * Read the unique bar code of this device.
     */
    const std::string& getBarCode()
    {
        ACE_Guard< ACE_Mutex > guard(device_mutex);

        return barCode;
    };

    /**
     * Read the unique device name which is like:
     * idl://alma/CONTROL/Mount_4
     */
    const std::string& getDeviceName()
    {
        ACE_Guard< ACE_Mutex > guard(device_mutex);

        return deviceName;
    };

    /**
      * This is the main function for formatting the Devices and their
      * MonitorPoints and store the data into the Archive or into a
      * CSV-file.
      * @param timeStamp The ACS::Time when the output has been generated.
      */
    void createData(EpochHelper& timeStamp);


    private:
    /**
     * No standard constructor.
     */
    Device();

    /**
     * Get the MonitorPoints of this device in the native storage format.
     * It is a std::multimap, which has the property name as key and
     * multiple entries for every property per key.
     */
    const MP_multimap& getMonitorPoints()
    {
        ACE_Guard< ACE_Mutex > guard(device_mutex);

        return mps;
    };

    /**
     * Store the device data into a real database.
     */
    void saveDeviceToRealDb(EpochHelper&) const;

    /**
     * Set the path up where device data shall be stored on disc.
     */
    void setupPath();

    /**
     * Set up the name of the file which will contain the device data.
     */
    void setupFilename();

    /**
     * Store the device data to a file on disc. The file will contain a header and
     * then CSV-lines.
     */
    void saveDeviceAsCSV(EpochHelper&) const;

    /**
     * The device name.
     */
    std::string deviceName;

    /**
     * The unique bar code which identifies this device.
     */
    std::string barCode;

    /**
     * This variable is set to true whenever the data shall be stored to the real
     * database.
     */
    static bool storeToRealDb;

    /**
     * This variable is set to true whenever the data shall be stored to disc..
     */
    static bool storeToFile;

    /**
     * This mutex protects all accesses to storeToRealDb and storeToFile.
     */
    static ACE_Mutex storeTo_mutex;

    /**
     * This std::multimap<std::string, MonitorPoint> stores the property
     * data for one device.
     */
    MP_multimap mps;

    /**
     * Concurrency lock.
     */
    ACE_Mutex device_mutex;

    /**
     * FQDN + port of archive computer.
     */
    std::string archiveComputer;

    /**
     * DB-name of archive.
     */
    std::string archiveDb;

    /**
     * Archive user name.
     */
    std::string archiveUserName;

    /**
     * Archive password.
     */
    std::string archivePassword;

    /**
     * The path where the data files will be stored to.
     */
    static std::string path;

    /**
     * A map which contains all files. This ensures that only one file per device
     * exists.
     */
    static FilenameMap filenames;
};
#endif
