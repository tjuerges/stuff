#ifndef monitorPoint_h
#define monitorPoint_h
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2006
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Jun 2, 2006  created
*/

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

#include <cstddef>
#include <string>
#include <vector>
#include <sstream>
#include <iterator>
#include <acstime.h>

/**
 * Forward declarations.
 */
class MonitorPoint;

/**
 * Typedefs which are used throughout the methods which deal with MonitorPoints.
 */
typedef std::multimap< std::string, MonitorPoint > MP_multimap;
typedef std::multimap< std::string, MonitorPoint >::iterator MP_multimap_iter;
typedef std::multimap< std::string, MonitorPoint >::const_iterator
    MP_multimap_citer;
typedef std::pair< MP_multimap_citer, MP_multimap_citer > MP_range;

/**
 * This class stores a device name, a property name, a time stamp and a property
 * value. All this is received through the archiving channel by the main
 * worker class DeviceMonitoringManager.
 *
 * The different MonitorPoints of a device are put together in the Device class.
 */
class MonitorPoint
{
    public:
    /**
     * Constructor.
     * @param timeStamp The time stamp of the data sampling.
     * @param deviceName The name of the device, the property is from.
     * @param propertyName The name of the property the data is from.
     * @param value The value of the property at the time timeStamp.
     */
    MonitorPoint(const ACS::Time& timeStamp,
        const std::string& deviceName,
        const std::string& propertyName,
        const CORBA::Any& value);

    /**
     * Copy constructor.
     */
    MonitorPoint(const MonitorPoint&);

    /**
     * Destructor.
     */
    ~MonitorPoint();

    /**
     * Read the time stamp of the MonitorPoint.
     */
    const ACS::Time& getTimeStamp() const
    {
        return timeStamp;
    };

    /**
     * Read the time stamp of the MonitorPoint as std::string.
     */
    const std::string& getTimeStampString() const
    {
        return timeStampString;
    };

    /**
     * Read the bar code of the MonitorPoint.
     */
    const std::string& getBarCode() const
    {
        return barCode;
    };

    /**
     * Read the device name of the MonitorPoint.
     */
    const std::string& getDeviceName() const
    {
        return deviceName;
    };

    /**
     * Read the property name of the MonitorPoint.
     */
    const std::string& getPropertyName() const
    {
        return propertyName;
    };

    /**
     * Read the value of the MonitorPoint.
     */
    const CORBA::Any& getValue() const
    {
        return value;
    };

    /**
     * Read the value of the MonitorPoint as std::string.
     */
    const std::string& getValueString() const
    {
        return valueString;
    };

    /**
     * Read the physical unit of the MonitorPoint value.
     */
    const std::string& getUnits() const
    {
        return units;
    };

    private:
    /**
     * No default constructor.
     */
    MonitorPoint();

    /**
     * No copy assignment constructor.
     */
    MonitorPoint& operator=(const MonitorPoint&);

    /**
     * Read the value type of the MonitorPoint. This is essential for
     * the VOTable file creation. Some ACS types have to be translated
     * into VOTable compliant types.
     */
    const std::string& getValueType() const
    {
        return valueType;
    };

    /**
     * Calculates from an ACS::Time a std::string representation.
     * @param timeStamp The timeStamp which shall be converted
     * to a std::string.
     */
    const std::string calcTimeStampString(const ACS::Time&) const;

    /**
     * Calculates from a CORBA::Any the type information. The string
     * representation of the value, the sequence length (if necessary)
     * and the type information for the VOTable are created, too.
     */
    void gatherInformationFromAny();

    #ifdef MP_DEBUG
    /**
     * Print some debug information about the monitor point.
     */
    void printOutDebugInformation();
    #endif

    /**
     * The time stamp of the MonitorPoint.
     */
    const ACS::Time timeStamp;

    /**
     * The string representation of the time stamp.
     */
    const std::string timeStampString;

    /**
     * The bar code from which this MonitorPoint originated.
     */
    const std::string barCode;

    /**
     * The device name from which this MonitorPoint originated.
     */
    const std::string deviceName;

    /**
     * The property from which this MonitorPoint originated.
     */
    const std::string propertyName;

    /**
     * The value of the property at the time timeStamp.
     */
    CORBA::Any value;

    /**
     * The CORBA type of the Any.
     */
    std::string valueType;

    /**
     * The physical unit of the MonitorPoint value.
     */
    const std::string units;

    /**
     * The std::string representation of the property value.
     */
    std::string valueString;
};
#endif
