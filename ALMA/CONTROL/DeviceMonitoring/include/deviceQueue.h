#ifndef deviceQueue_h
#define deviceQueue_h
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2006
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Jun 2, 2006  created
*/

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

#include <vector>
#include <string>
#include <device.h>
#include <Guard_T.h>
#include <Mutex.h>
#include <acsThreadBase.h>

/**
 * Forward declarations.
 */
class MonitorPoint;

/**
  * Creates a queue of Devices. This queue contains one device class per device
  * instance. The device class keeps MonitorPoint information for this device.
  * If a given device already exists in the queue, data is just appended to the
  * entry.
  */
class DeviceQueue
{
    public:
    /**
     * Constructor
     * Reserves space for 50 devices to speed up std::vector accesses.
     */
    DeviceQueue();

    /**
     * Copy constructor.
     *
     * Copies the data from one DeviceQueue instance to another.
     * @param source The DeviceQueue from which data is copied into this.
     */
    DeviceQueue(const DeviceQueue& source);

    /**
     * Destructor
     */
    ~DeviceQueue();

    /**
      * If the device exists for the given monitor point. It is added to that
      * device. If there is no deivce yet which contains that MP, the deivce
      * is created and the mp is added to it.
      */
    void addMonitorPoint(const MonitorPoint& mp);

    /**
      * With a given number of devices this function
      * formats the data of the MonitorPoints and stores it in the archive.
      */
    void createDataForArchive();

    /**
     * Clears the data vectors and resizes them to size(0).
     */
    void clear()
    {
        ACE_Guard< ACE_Mutex > guard(device_queue_mutex);
        devices.clear();
    };

    /**
     * Tests if both Device vector and ArchiveData vector are empty.
     */
    const bool empty()
    {
        ACE_Guard< ACE_Mutex > guard(device_queue_mutex);
        return devices.empty();
    };


    private:
    /**
     * No copy assignment constructor.
     */
    DeviceQueue& operator=(const DeviceQueue&);

    /**
     * Return the vector which constains the Devices. Needed by assign.
     */
    const std::vector< Device >& getDevices()
    {
        ACE_Guard< ACE_Mutex > guard(device_queue_mutex);
        return devices;
    };

    /**
      * See if the device with the given bar code/name is aready created
      * in this queue.
      * @param bc Barcode of the new device.
      */
    const std::vector< Device >::iterator deviceExists(
        const std::string& bc);

    /**
      * This vector holds all the devices.
      */
    std::vector< Device > devices;

    ACE_Mutex device_queue_mutex;
};
#endif
