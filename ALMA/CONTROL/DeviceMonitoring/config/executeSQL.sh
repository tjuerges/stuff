#! /bin/bash

if [ $# -ne 1 ]; then
	echo "Please provide an SQL-script."
	exit
fi

sqlplus 'monitorData/alma$dba@aarchive' @$1
