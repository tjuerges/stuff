#! /bin/bash
#
# $Id$
#

which sqlplus >&/dev/null
if [ $? -ne 0 ]; then
	echo "This script makes use of Oracle's sqlplus program. Run this script on the"
	echo "archive computer! Otherwise have Oracle's instantclient-packages "
	echo "installed and modify this script in such a way that the"
	echo "sqlplus command uses the correct computer name, db-name, user, pw"
	echo "and port."
	exit -1
fi

if [ $# -lt 1 ]; then
	echo "The device name must be provided as option!"
	echo "Example: `basename $0` FOO"
	exit -1
else
	DEVICE="$1"
fi

SQL_COMMANDS=$$
SQL_SCRIPT_NAME="sql_script.sql.$SQL_COMMANDS"

sed -e "s|CHANGE_DEVICENAME_HERE|$DEVICE|g" > $SQL_SCRIPT_NAME << EOF
SET SERVEROUTPUT ON;

DECLARE
	asstid NUMBER;
	propid NUMBER;

BEGIN

SELECT assemblytypeid INTO asstid FROM assemblytype WHERE assembly_name='CHANGE_DEVICENAME_HERE';
SELECT COUNT(propertytypeid) INTO propid FROM propertytype;

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'integer',
	'IntegerProperty',
	'errors',
	'0x30001',
	'N',
	'uint32',
	'uint32',
	'na',
	'1',
	'0',
	'0',
	'4294967295',
	'60',
	'0',
	'4294967295',
	'10d',
	'Errors',
	'This monitor point provides the number of communication errors between the AMBSI and the slave device.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'integer',
	'IntegerProperty',
	'transactions',
	'0x30002',
	'N',
	'uint32',
	'uint32',
	'na',
	'1',
	'0',
	'0',
	'4294967295',
	'60',
	'0',
	'4294967295',
	'10d',
	'Transactions',
	'This monitor point provides the number of transactions between the AMBSI and the slave device.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'float',
   	'FloatProperty',
	'temperature',
	'0x30003',
	'N',
	'uint32',
	'float',
	'Degrees Celsius',
	'1.0',
	'0.0',
	'-55.0',
	'125.0',
	'600',
	'-55.0',
	'125.0',
	'8.3f',
	'AMBSI ambient temperature.',
	'The ambient temperature reported by the AMBSI DS1820 device.'
);

COMMIT;
END;
/
EOF

sqlplus 'monitorData/alma$dba@archive1' @$SQL_SCRIPT_NAME
rm -f $SQL_SCRIPT_NAME
