-- $Id$

SET SERVEROUTPUT ON;

DECLARE
	lruid NUMBER;
	assid NUMBER;
	asstid NUMBER;
	propid NUMBER;

BEGIN

SELECT COUNT(lrutypeid) INTO lruid FROM lrutype;
SELECT COUNT(assemblyid) INTO assid FROM assembly;
SELECT COUNT(assemblytypeid) INTO asstid FROM assemblytype;
SELECT COUNT(propertytypeid) INTO propid FROM propertytype;

lruid := lruid + 1;
INSERT INTO LRUType VALUES (
	lruid,
	'MountController',
	'Mount Controller',
	'-',
	'06-SEP-06',
	'Mount Controller',
	''
);

asstid := asstid + 1;
INSERT INTO AssemblyType VALUES (
	asstid,
	lruid,
	'MountController',
	'MountController',
	'0x300',
	'0',
	'Mount Controller',
	''
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'integer',
	'IntegerProperty',
	'errors',
	'0x30001',
	'N',
	'uint32',
	'uint32',
	'na',
	'1',
	'0',
	'0',
	'4294967295',
	'60',
	'0',
	'4294967295',
	'10d',
	'Errors',
	'This monitor point provides the number of communication errors between the AMBSI and the slave device.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'integer',
	'IntegerProperty',
	'transactions',
	'0x30002',
	'N',
	'uint32',
	'uint32',
	'na',
	'1',
	'0',
	'0',
	'4294967295',
	'60',
	'0',
	'4294967295',
	'10d',
	'Transactions',
	'This monitor point provides the number of transactions between the AMBSI and the slave device.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'float',
   	'FloatProperty',
	'temperature',
	'0x30003',
	'N',
	'uint32',
	'float',
	'Degrees Celsius',
	'1.0',
	'0.0',
	'-55.0',
	'125.0',
	'600',
	'-55.0',
	'125.0',
	'8.3f',
	'AMBSI ambient temperature.',
	'The ambient temperature reported by the AMBSI DS1820 device.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'double',
	'DoubleProperty',
	'actualRA',
	'1',
	'N',
	'double',
	'double',
	'Degrees',
	'1.0',
	'0',
	'-9.9e20',
	'9.9e20',
	'10',
	'-180',
	'180',
	'10.5f',
	'Actual RA',
	'Current rightascension.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'double',
	'DoubleProperty',
	'actualDec',
	'1',
	'N',
	'double',
	'double',
	'Degrees',
	'1.0',
	'0',
	'-9.9e20',
	'9.9e20',
	'10',
	'-180',
	'180',
	'10.5f',
	'Actual Dec',
	'Current declination.'
);

assid := assid + 1;
INSERT INTO Assembly VALUES (
	assid,
	asstid,
	'CONTROL/ALMA01/MountController'
);

assid := assid + 1;
INSERT INTO Assembly VALUES (
	assid,
	asstid,
	'CONTROL/ALMA02/MountController'
);

COMMIT;
END;
/
