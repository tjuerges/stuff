-- $Id$

SET SERVEROUTPUT ON;

DECLARE
	lruid NUMBER;
	assid NUMBER;
	asstid NUMBER;
	propid NUMBER;

BEGIN

SELECT COUNT(lrutypeid) INTO lruid FROM lrutype;
SELECT COUNT(assemblyid) INTO assid FROM assembly;
SELECT COUNT(assemblytypeid) INTO asstid FROM assemblytype;
SELECT COUNT(propertytypeid) INTO propid FROM propertytype;

lruid := lruid + 1;
INSERT INTO LRUType VALUES (
	lruid,
	'Metrology',
	'Mount Controller/Metrology',
	'-',
	'06-SEP-07',
	'Mount Controller/Metrology',
	''
);

asstid := asstid + 1;
INSERT INTO AssemblyType VALUES (
	asstid,
	lruid,
	'Metrology',
	'MountController/Metrology',
	'0x301',
	'0',
	'Mount Controller/Metrology',
	''
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'integer',
	'IntegerProperty',
	'errors',
	'0x30001',
	'N',
	'uint32',
	'uint32',
	'na',
	'1',
	'0',
	'0',
	'4294967295',
	'60',
	'0',
	'4294967295',
	'10d',
	'Errors',
	'This monitor point provides the number of communication errors between the AMBSI and the slave device.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'integer',
	'IntegerProperty',
	'transactions',
	'0x30002',
	'N',
	'uint32',
	'uint32',
	'na',
	'1',
	'0',
	'0',
	'4294967295',
	'60',
	'0',
	'4294967295',
	'10d',
	'Transactions',
	'This monitor point provides the number of transactions between the AMBSI and the slave device.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'float',
   	'FloatProperty',
	'temperature',
	'0x30003',
	'N',
	'uint32',
	'float',
	'Degrees Celsius',
	'1.0',
	'0.0',
	'-55.0',
	'125.0',
	'600',
	'-55.0',
	'125.0',
	'8.3f',
	'AMBSI ambient temperature.',
	'The ambient temperature reported by the AMBSI DS1820 device.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'double',
	'DoubleProperty',
	'displacement1',
	'1',
	'N',
	'double',
	'double',
	'Degrees',
	'1.0',
	'0',
	'-9.9e20',
	'9.9e20',
	'10',
	'-9.9e20',
	'9.9e20',
	'10.5f',
	'displacement1',
	'Current displacement1.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'double',
	'DoubleProperty',
	'displacement2',
	'1',
	'N',
	'double',
	'double',
	'Degrees',
	'1.0',
	'0',
	'-9.9e20',
	'9.9e20',
	'10',
	'-9.9e20',
	'9.9e20',
	'10.5f',
	'displacement2',
	'Current displacement2.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'double',
	'DoubleProperty',
	'tilt1Temperature',
	'1',
	'N',
	'double',
	'double',
	'Degrees',
	'1.0',
	'0',
	'-9.9e20',
	'9.9e20',
	'10',
	'-9.9e20',
	'9.9e20',
	'10.5f',
	'tilt1Temperature',
	'Current tilt1Temperature.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'double',
	'DoubleProperty',
	'tilt2Temperature',
	'1',
	'N',
	'double',
	'double',
	'Degrees',
	'1.0',
	'0',
	'-9.9e20',
	'9.9e20',
	'10',
	'-9.9e20',
	'9.9e20',
	'10.5f',
	'tilt2Temperature',
	'Current tilt2Temperature.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'double',
	'DoubleProperty',
	'tilt3Temperature',
	'1',
	'N',
	'double',
	'double',
	'Degrees',
	'1.0',
	'0',
	'-9.9e20',
	'9.9e20',
	'10',
	'-9.9e20',
	'9.9e20',
	'10.5f',
	'tilt3Temperature',
	'Current tilt3Temperature.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'double',
	'DoubleProperty',
	'tilt4Temperature',
	'1',
	'N',
	'double',
	'double',
	'Degrees',
	'1.0',
	'0',
	'-9.9e20',
	'9.9e20',
	'10',
	'-9.9e20',
	'9.9e20',
	'10.5f',
	'tilt4Temperature',
	'Current tilt4Temperature.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'double',
	'DoubleProperty',
	'tilt5Temperature',
	'1',
	'N',
	'double',
	'double',
	'Degrees',
	'1.0',
	'0',
	'-9.9e20',
	'9.9e20',
	'10',
	'-9.9e20',
	'9.9e20',
	'10.5f',
	'tilt5Temperature',
	'Current tilt5Temperature.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'double',
	'DoubleProperty',
	'tilt1X',
	'1',
	'N',
	'double',
	'double',
	'Degrees',
	'1.0',
	'0',
	'-9.9e20',
	'9.9e20',
	'10',
	'-9.9e20',
	'9.9e20',
	'10.5f',
	'tilt1X',
	'Current tilt1X.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'double',
	'DoubleProperty',
	'tilt1Y',
	'1',
	'N',
	'double',
	'double',
	'Degrees',
	'1.0',
	'0',
	'-9.9e20',
	'9.9e20',
	'10',
	'-9.9e20',
	'9.9e20',
	'10.5f',
	'tilt1Y',
	'Current tilt1Y.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'double',
	'DoubleProperty',
	'tilt2X',
	'1',
	'N',
	'double',
	'double',
	'Degrees',
	'1.0',
	'0',
	'-9.9e20',
	'9.9e20',
	'10',
	'-9.9e20',
	'9.9e20',
	'10.5f',
	'tilt2X',
	'Current tilt2X.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'double',
	'DoubleProperty',
	'tilt2Y',
	'1',
	'N',
	'double',
	'double',
	'Degrees',
	'1.0',
	'0',
	'-9.9e20',
	'9.9e20',
	'10',
	'-9.9e20',
	'9.9e20',
	'10.5f',
	'tilt2Y',
	'Current tilt2Y.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'double',
	'DoubleProperty',
	'tilt3X',
	'1',
	'N',
	'double',
	'double',
	'Degrees',
	'1.0',
	'0',
	'-9.9e20',
	'9.9e20',
	'10',
	'-9.9e20',
	'9.9e20',
	'10.5f',
	'tilt3X',
	'Current tilt3X.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'double',
	'DoubleProperty',
	'tilt3Y',
	'1',
	'N',
	'double',
	'double',
	'Degrees',
	'1.0',
	'0',
	'-9.9e20',
	'9.9e20',
	'10',
	'-9.9e20',
	'9.9e20',
	'10.5f',
	'tilt3Y',
	'Current tilt3Y.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'double',
	'DoubleProperty',
	'tilt4X',
	'1',
	'N',
	'double',
	'double',
	'Degrees',
	'1.0',
	'0',
	'-9.9e20',
	'9.9e20',
	'10',
	'-9.9e20',
	'9.9e20',
	'10.5f',
	'tilt4X',
	'Current tilt4X.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'double',
	'DoubleProperty',
	'tilt4Y',
	'1',
	'N',
	'double',
	'double',
	'Degrees',
	'1.0',
	'0',
	'-9.9e20',
	'9.9e20',
	'10',
	'-9.9e20',
	'9.9e20',
	'10.5f',
	'tilt4Y',
	'Current tilt4Y.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'double',
	'DoubleProperty',
	'tilt5X',
	'1',
	'N',
	'double',
	'double',
	'Degrees',
	'1.0',
	'0',
	'-9.9e20',
	'9.9e20',
	'10',
	'-9.9e20',
	'9.9e20',
	'10.5f',
	'tilt5X',
	'Current tilt5X.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'double',
	'DoubleProperty',
	'tilt5Y',
	'1',
	'N',
	'double',
	'double',
	'Degrees',
	'1.0',
	'0',
	'-9.9e20',
	'9.9e20',
	'10',
	'-9.9e20',
	'9.9e20',
	'10.5f',
	'tilt5Y',
	'Current tilt5Y.'
);

assid := assid + 1;
INSERT INTO Assembly VALUES (
	assid,
	asstid,
	'CONTROL/ALMA01/MountController/Metrology'
);

assid := assid + 1;
INSERT INTO Assembly VALUES (
	assid,
	asstid,
	'CONTROL/ALMA02/MountController/Metrology'
);

COMMIT;
END;
/
