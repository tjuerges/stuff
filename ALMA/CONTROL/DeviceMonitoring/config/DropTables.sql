DELETE FROM IntegerProperty;
DROP TABLE IntegerProperty;

DELETE FROM DoubleProperty;
DROP TABLE DoubleProperty;

DELETE FROM BooleanProperty;
DROP TABLE BooleanProperty;

DELETE FROM StringProperty;
DROP TABLE StringProperty;

DELETE FROM FloatProperty;
DROP TABLE FloatProperty;

DELETE FROM Antenna;
DROP TABLE Antenna;

DELETE FROM Assembly;
DROP TABLE Assembly;

DELETE FROM PropertyType;
DROP TABLE PropertyType;

DELETE FROM AssemblyType;
DROP TABLE AssemblyType;

DELETE FROM LRUType;
DROP TABLE LRUType;

COMMIT;
/
.
QUIT;
