SET SERVEROUTPUT ON;

DECLARE
	lruid NUMBER;
	assid NUMBER;
	asstid NUMBER;
	propid NUMBER;

BEGIN

SELECT COUNT(lrutypeid) INTO lruid FROM lrutype;
SELECT COUNT(assemblyid) INTO assid FROM assembly;
SELECT COUNT(assemblytypeid) INTO asstid FROM assemblytype;
SELECT COUNT(propertytypeid) INTO propid FROM propertytype;

lruid := lruid + 1;
INSERT INTO LRUType VALUES (
	lruid,
	'LORR',
	'Local Oscillator Reference Receiver',
	'ALMA-55.04.00.00-70.35.30.00-A-ICD',
	'04-SEP-24',
	'The Local Oscillator Reference Receiver (LORR) is located in the receiver cabin of each antenna. It is connected, via fibre optic cable, to the ALMA Operations Site Technical Building and it demodulates the signals transmitted by the central reference distributor to provide, at the antenna, the four fundamental reference/timing signals (2GHz, 125MHz, 25MHz and 48ms).',
	''
);

asstid := asstid + 1;
INSERT INTO AssemblyType VALUES (
	asstid,
	lruid,
	'LORR',
	'Local Oscillator Reference Receiver',
	'0x22',
	'0',
	'The Local Oscillator Reference Receiver (LORR) is located in the receiver cabin of each antenna. It is connected, via fibre optic cable, to the ALMA Operations Site Technical Building and it demodulates the signals transmitted by the central reference distributor to provide, at the antenna, the four fundamental reference/timing signals (2GHz, 125MHz, 25MHz and 48ms).',
	''
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'integer',
	'IntegerProperty',
	'errors',
	'0x30001',
	'N',
	'uint32',
	'uint32',
	'na',
	'1',
	'0',
	'0',
	'4294967295',
	'60',
	'0',
	'4294967295',
	'10d',
	'Errors',
	'This monitor point provides the number of communication errors between the AMBSI and the slave device.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'integer',
	'IntegerProperty',
	'transactions',
	'0x30002',
	'N',
	'uint32',
	'uint32',
	'na',
	'1',
	'0',
	'0',
	'4294967295',
	'60',
	'0',
	'4294967295',
	'10d',
	'Transactions',
	'This monitor point provides the number of transactions between the AMBSI and the slave device.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'float',
   	'FloatProperty',
	'temperature',
	'0x30003',
	'N',
	'uint32',
	'float',
	'Degrees Celsius',
	'1.0',
	'0.0',
	'-55.0',
	'125.0',
	'600',
	'-55.0',
	'125.0',
	'8.3f',
	'AMBSI ambient temperature.',
	'The ambient temperature reported by the AMBSI DS1820 device.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'integer',
	'IntegerProperty',
	'STATUS',
	'1',
	'Y',
	'ubyte',
	'ubyte',
	'na',
	'1',
	'0',
	'0',
	'63',
	'6',
	'0',
	'255',
	'3d',
	'Status',
	'This monitor point provides a number of staus bits that summarize the operation of critical parts of the LORR..'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'float',
		'FloatProperty',
	'EFC_25_MHZ',
	'2',
	'N',
	'int16',
	'double',
	'Volts',
	'4.8840048799999999E-3',
	'0',
	'1',
	'9',
	'60',
	'0',
	'10',
	'8.3f',
	'25MHz Electronic Frequency Control Voltage',
	'Electronic Frequency Control (tuning) voltage of 25 MHz PLL. This voltage should be steady; fluctuating voltage indicates a problem.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'float',
		'FloatProperty',
	'EFC_2_GHZ',
	'3',
	'N',
	'int16',
	'double',
	'Volts',
	'4.8840048799999999E-3',
	'0',
	'1',
	'9',
	'60',
	'0',
	'10',
	'8.3f',
	'2GHz Electronic Frequency Control Voltage',
	'Electronic Frequency Control (tuning) voltage of 2 GHz PLL. This voltage should be steady; fluctuating voltage indicates a problem.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'float',
		'FloatProperty',
	'PWR_25_MHZ',
	'4',
	'N',
	'int16',
	'double',
	'Volts',
	'4.8840048799999999E-3',
	'0',
	'0',
	'10',
	'60',
	'-1',
	'11',
	'8.3f',
	'25MHz RF Output Power',
	'25 MHz RF output power level. The output power is nominally in dBm but the conversion factor between volts and power is variable.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'float',
		'FloatProperty',
	'PWR_125_MHZ',
	'5',
	'N',
	'int16',
	'double',
	'Volts',
	'4.8840048799999999E-3',
	'0',
	'0',
	'10',
	'60',
	'-1',
	'11',
	'8.3f',
	'125MHz RF Output Power',
	'125 MHz RF output power level. The output power is nominally in dBm but the conversion factor between volts and power is variable.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'float',
		'FloatProperty',
	'PWR_2_GHZ',
	'6',
	'N',
	'int16',
	'double',
	'Volts',
	'4.8840048799999999E-3',
	'0',
	'0',
	'10',
	'60',
	'-1',
	'11',
	'8.3f',
	'2GHz RF Output Power',
	'2 GHz RF output power level. The output power is nominally in dBm but the conversion factor between volts and power is variable.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'float',
		'FloatProperty',
	'RX_OPT_PWR',
	'7',
	'N',
	'int16',
	'double',
	'milliWatts',
	'4.8840048799999999E-3',
	'0',
	'2.5000000000000001E-2',
	'1',
	'60',
	'1',
	'10',
	'8.3f',
	'Received Optical Power',
	'Received optical power. The actual value is a voltage representation of the photodiode receiver current.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'float',
		'FloatProperty',
	'VDC_12',
	'9',
	'N',
	'int16',
	'double',
	'Volts',
	'9.7680097679999998E-3',
	'0',
	'11.5',
	'12.5',
	'60',
	'0',
	'20',
	'7.2f',
	'12V Power Supply Voltage',
	'12 VDC voltage regulator output monitor. The 12 V runs through a �2 voltage divider to accommodate the ADC range.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
	propid,
	asstid,
	'float',
		'FloatProperty',
	'VDC_15',
	'10',
	'N',
	'int16',
	'double',
	'Volts',
	'9.7680097679999998E-3',
	'0',
	'14.5',
	'15.5',
	'60',
	'0',
	'20',
	'7.2f',
	'15V Power Supply Voltage',
	'15 VDC voltage regulator output monitor. The 15 V runs through a �2 voltage divider to accommodate the ADC range.'
);

assid := assid + 1;
INSERT INTO Assembly VALUES (
	assid,
	asstid,
	'CONTROL/DV01/LORR'
);

assid := assid + 1;
INSERT INTO Assembly VALUES (
	assid,
	asstid,
	'CONTROL/DA41/LORR'
);

COMMIT;
END;
/
