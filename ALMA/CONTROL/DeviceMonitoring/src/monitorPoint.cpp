/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2006
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Jun 2, 2006  created
*/

#include <monitorPoint.h>
#include <string>
#include <algorithm>
#include <logging.h>
#include <acsutilAnyAide.h>
#include <acsncORBHelper.h>
#include <tao/DynamicAny/DynamicAny.h>

/**
 * Define MP_DEBUG to generate some more output through std::cout.
 */
//#define MP_DEBUG

#ifdef MP_DEBUG
#include <iostream>
#endif


MonitorPoint::MonitorPoint(const ACS::Time& _timeStamp,
    const std::string& _deviceName,
    const std::string& _propertyName,
    const CORBA::Any& _value):
    timeStamp(_timeStamp),
    timeStampString(calcTimeStampString(_timeStamp)),
    barCode(_deviceName),
    deviceName(_deviceName),
    propertyName(_propertyName),
    value(_value),
    valueType(AnyAide::getId(_value)),
    units("Units not implemented yet")
{
    gatherInformationFromAny();
}

MonitorPoint::MonitorPoint(const MonitorPoint& mp):
    timeStamp(mp.getTimeStamp()),
    timeStampString(mp.getTimeStampString()),
    barCode(mp.getBarCode()),
    deviceName(mp.getDeviceName()),
    propertyName(mp.getPropertyName()),
    value(mp.getValue()),
    valueType(mp.getValueType()),
    units(mp.getUnits()),
    valueString(mp.getValueString())
{
}

MonitorPoint::~MonitorPoint()
{
}

const std::string MonitorPoint::calcTimeStampString(
    const ACS::Time& _timeStamp) const
{
    EpochHelper epochHelper(_timeStamp);
    std::string timeStampString(epochHelper.toString(acstime::TSArray, //time system
                                                            "%G", //format
                                                            0L, //array2TAI,
                                                            0L)); //tai2utc
    timeStampString.erase(timeStampString.end()-1);

    long milliseconds(static_cast< long >(
        epochHelper.microSecond() / CORBA::Long(1000L)));

    std::ostringstream ostr;
    ostr << ".";

    ostr.width(3);
    ostr.fill('0');
    ostr << milliseconds;

    timeStampString += ostr.str();
    return timeStampString;
}

void MonitorPoint::gatherInformationFromAny()
{
    try
    {
        if(AnyAide::isEnum(value) == true)
        {
            int argc(1);
            char* orbArgs[] = {""};
            CORBA::ORB_ptr orb_mp(CORBA::ORB_init(argc, orbArgs, ""));
            ACE_ASSERT(!CORBA::is_nil(orb_mp));

            //get the dynamic any factory
            CORBA::Object_var factory_obj(
                orb_mp->resolve_initial_references("DynAnyFactory"));
            //narrow it
            DynamicAny::DynAnyFactory_var dynany_factory(
                DynamicAny::DynAnyFactory::_narrow(factory_obj.in()));
            //sanity check
            ACE_ASSERT(!CORBA::is_nil(dynany_factory.in()));

            //get the dynamic any
            /**
             * TODO:
             * The following line of the DynAny creation leads to an uncatchable
             * exception CORBA::Bounds when the get_as_string method is later
             * called. The TAO method get_as_string does not specify the
             * CORBA::Bounds exception so the bug lies in TAO. :-(
             * Therefore the DynAny is created via the typecode which - strange
             * enough - seems to work.
             * Thomas, 2006-09-07
             *
             DynamicAny::DynAny_var dynValue = dynany_factory->create_dyn_any(value);
             */
            CORBA::TypeCode_var tc(value.type());
            DynamicAny::DynAny_var dynValue(
                dynany_factory->create_dyn_any_from_type_code(tc));

            //sanity check
            ACE_ASSERT(!CORBA::is_nil(dynValue.in()));
            //narrow it to an enum
            DynamicAny::DynEnum_var dynEnum(
                DynamicAny::DynEnum::_narrow(dynValue));
            //another sanity check
            ACE_ASSERT(!CORBA::is_nil(dynEnum.in()));

            try
            {
                CORBA::String_var tString(dynEnum->get_as_string());
                valueString.assign(tString.in());
            }
            catch(...)
            {
                ACS_SHORT_LOG((LM_ERROR,
                    "MonitorPoint::gatherInformationFromAny: Could not "
                    "retrieve the enum's string equivalent for the property "
                    "%s/%s. Not storing this monitor point!",
                    deviceName.c_str(), propertyName.c_str()));

                valueString.clear();

                dynEnum->destroy();
                CORBA::release(orb_mp);
                orb_mp = 0;

                return;
            }

            //destroy the dynamic any
            dynEnum->destroy();
            CORBA::release(orb_mp);
            orb_mp = 0;

            value <<= valueString.c_str();
            valueType = AnyAide::getId(value);
        }
        else
        {
            valueString = AnyAide::anyToString(value);
        }
    }
    catch(const AnyAide::UnsupportedType& ex)
    {
        ACS_SHORT_LOG((LM_ERROR, "MonitorPoint::gatherInformationFromAny: "
            "An unknown CORBA type shall be archived for the %s/%s property. "
            "Not storing this monitor point!", deviceName.c_str(),
            propertyName.c_str()));

        valueString.clear();

        return;
    }
    catch(...)
    {
        ACS_SHORT_LOG((LM_ERROR, "MonitorPoint::gatherInformationFromAny: The "
            "property %s/%s cannot be archived because of an unexpected "
            "exception. Not storing this monitor point!", deviceName.c_str(),
            propertyName.c_str()));

        valueString.clear();

        return;
    }

    if((valueType == AnyAide::longType_m)
    || (valueType == AnyAide::floatType_m)
    || (valueType == AnyAide::doubleType_m)
    || (valueType == AnyAide::patternType_m)
    || (valueType == AnyAide::longLongType_m)
    || (valueType == AnyAide::uLongLongType_m))
    {
        valueString.erase(valueString.end() - 1);
    }
    else if((valueType == AnyAide::longSeqType_m)
    || (valueType == AnyAide::floatSeqType_m)
    || (valueType == AnyAide::doubleSeqType_m))
    {

        if(valueType == AnyAide::floatSeqType_m)
        {
            valueString = AnyAide::anyToString(value, 8);
        }

        /**
         * AnyAide::anyToString returns a " std::ends" as last 2 characters.
         * Both characters must be removed.
         */
        valueString.assign(valueString.begin(), valueString.end() - 2);

        /**
         * Replace any occurrence of ' ' by ','.
         */
        std::replace(valueString.begin(), valueString.end(), ' ', ',');
    }

    #ifdef MP_DEBUG
    printOutDebugInformation();
    #endif
}

#ifdef MP_DEBUG
void MonitorPoint::printOutDebugInformation(void)
{
    std::cout << std::endl
        << std::endl
        << "type = "
        << valueType
        << " string = "
        <<value
        <<std::endl;
}
#endif

#ifdef MP_DEBUG
#undef MP_DEBUG
#endif
