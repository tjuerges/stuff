/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2006
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Jun 2, 2006  created
*/

#include <deviceQueue.h>
#include <list>
#include <Guard_T.h>
#include <monitorPoint.h>
#include <logging.h>
#include <acstime.h>

DeviceQueue::DeviceQueue()
{
    devices.reserve(50);
}

DeviceQueue::DeviceQueue(const DeviceQueue& source)
{
    if(&source != this)
    {
        ACE_Guard< ACE_Mutex > guard(device_queue_mutex);

        devices.assign(
            const_cast< DeviceQueue& >(source).getDevices().begin(),
            const_cast< DeviceQueue& >(source).getDevices().end());
    }
}

DeviceQueue::~DeviceQueue()
{
}

void DeviceQueue::addMonitorPoint(const MonitorPoint& mp)
{
    ACE_Guard< ACE_Mutex > guard(device_queue_mutex);

    std::vector< Device >::iterator pos(deviceExists(mp.getBarCode()));

    if(pos != devices.end())
    {
        pos->addMonitorPoint(mp);
    }
    else
    {
        devices.push_back(Device(mp));
    }

}

const std::vector<Device>::iterator DeviceQueue::deviceExists(
    const std::string& bc)
{
    //Search devices for one with the given barcode.
    std::vector< Device >::iterator i(devices.begin());

    for(; i != devices.end(); ++i)
    {
        if(i->getBarCode() == bc)
        {
            break;
        }
    }

    return i;
}

void DeviceQueue::createDataForArchive()
{
    ACE_Guard< ACE_Mutex > guard(device_queue_mutex);

    ACS_SHORT_LOG((LM_DEBUG, "DeviceQueue::getDataForArchive: Creating data "
        "for Monitor Store archive."));

    EpochHelper now(TimeUtil::ace2epoch(ACE_OS::gettimeofday()));

    for(std::vector< Device >::iterator i(devices.begin()); i != devices.end();
        ++i)
    {
        i->createData(now);
    }
}
