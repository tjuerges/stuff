/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2006
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Jun 2, 2006  created
*/

#include <deviceMonitoringTimerThread.h>
#include <deviceMonitoringManager.h>

DeviceMonitoringTimerThread::DeviceMonitoringTimerThread(const ACE_CString& name,
    DeviceMonitoringManager* mgr,
    const ACS::TimeInterval& responseTime,
    const ACS::TimeInterval& sleepTime):
    ACS::Thread(name, responseTime, sleepTime, true),
    manager(mgr)
{
    ACS_SHORT_LOG((LM_DEBUG,
        "DeviceMonitoringTimerThread::DeviceMonitoringTimerThread: Timer "
        "created."));
}

DeviceMonitoringTimerThread::~DeviceMonitoringTimerThread()
{
    ACS_SHORT_LOG((LM_DEBUG,
        "DeviceMonitoringTimerThread::~DeviceMonitoringTimerThread: Timer "
        "destroyed."));

    /**
     * ATTENTION: with the Jan-2006 patch of ACS threads, it is not possible to
     * use the logger after the terminate call.
     */
    this->terminate();
}

void DeviceMonitoringTimerThread::runLoop()
{
    ACS_SHORT_LOG((LM_DEBUG, "DeviceMonitoringTimerThread::runLoop: Sending "
        "next queue's data to archive."));

    manager->sendDataToArchiveNow();
}
