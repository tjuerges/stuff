/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2006
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Jun 2, 2006  created
*/

#include <deviceMonitoringManager.h>
#include <sstream>
#include <fstream>
#include <memory>
#include <limits>
#include <maciContainerServices.h>
#include <monitorPoint.h>
#include <deviceQueue.h>
#include <deviceMonitoringTimerThread.h>
#include <acsthreadErrType.h>
#include <archivingThread.h>
#include <acstime.h>
#include <archive_xmlstore_ifC.h>


const unsigned long DeviceMonitoringManager::unsigned_long_limit(
    std::numeric_limits< unsigned long >::max());
const std::string DeviceMonitoringManager::Timer_Name("TimerThread");
std::string DeviceMonitoringManager::archiveComputer("archive1:1521");
std::string DeviceMonitoringManager::archiveDb("aarchive");
std::string DeviceMonitoringManager::archiveUserName("monitorDb");
std::string DeviceMonitoringManager::archivePassword("alma$dba");

#ifdef DEBUG_TIMECONSUMPTION
const char* DeviceMonitoringManager::highResTimer_text(
    "DeviceMonitoringManager::receive: Time needed to process the incoming "
    "monitor point =");
#endif

/**
 * Constructor which is called from the DeviceMonitor component.
 */
DeviceMonitoringManager::DeviceMonitoringManager(maci::ContainerServices* cs):
    container(cs),
    queue(0),
    timer(0),
    consumer(0),
    thread_counter(0),
    lastArchivingThreadName("")
{
    /**
     * Setup the path, VOTable files are saved to in case of any archive
     * failure.
     */
    const char* env_path(ACE_OS::getenv("ACSDATA"));

    if(env_path != 0)
    {
        path = env_path;
    }
    else
    {
        path = "/var/tmp";
    }

    ACS_SHORT_LOG((LM_INFO, "DeviceMonitoringManager::DeviceMonitoringManager: "
        "Saving archive data in case of BulkDataReceiver or ArchiveIdentifier "
        "failure to %s.",
        path.c_str()));

    xmlstore::ArchiveConnection_var archConn(
        cs->getComponent< xmlstore::ArchiveConnection >(
            "ARCHIVE_CONNECTION"));

    xmlstore::Administrative_var xmlAdmin(archConn->getAdministrative(
        "Bulkstore"));
    archiveComputer = xmlAdmin->config("archive.monitorDB.location");
    archiveDb = xmlAdmin->config("archive.monitorDB.service");
    archiveUserName = xmlAdmin->config("archive.monitorDB.user");
    /**
     * FIXME:
     * Thomas Juerges, 2006-10-05:
     * For the time being, ARCHIVE does not support to retrieve the password.
     * So it is hardcoded here which I, for obvious reasons, do not like.
    archivePassword = xmlAdmin->config("archive.monitorDB.passwd");
     */
    archivePassword = "alma$dba";
    cs->releaseComponent("ARCHIVE_CONNECTION");

    ACS_SHORT_LOG((LM_INFO, "DeviceMonitoringManager::DeviceMonitoringManager: "
        "Archive computer/port: %s, "
        "archive database: %s, "
        "archive user name: %s, "
        "archive password: %s.",
        archiveComputer.c_str(),
        archiveDb.c_str(),
        archiveUserName.c_str(),
        archivePassword.c_str()));

    /**
     * Setup a new device queue which keeps a vector of Devices. Those then
     * keep vectors vor every property monitor poont.
     */
    queue = new DeviceQueue;

    /**
     * Needed to placate g++.
     */
    DeviceMonitoringManager* ptr(this);

    #ifndef NO_TIMER
    /**
     * Create and start the timer thread which triggers every 60 seconds the
     * data storage.
     */
    timer = container->getThreadManager()->
        create< DeviceMonitoringTimerThread, DeviceMonitoringManager* >(
            Timer_Name.c_str(), ptr);
    container->getThreadManager()->resume(Timer_Name.c_str());
    #endif

    #ifndef NO_CONSUMER
    // Startup receiving of MonitorPoint data.
    consumer = new nc::ArchiveConsumer(this);
    consumer->consumerReady();
    #endif

    ACS_SHORT_LOG((LM_INFO, "DeviceMonitoringManager::DeviceMonitoringManager: "
        "DeviceMonitoringManager created."));
}

/**
 * Constructor which is called from the test client.
 */
DeviceMonitoringManager::DeviceMonitoringManager(const std::string& _path):
    container(0),
    queue(0),
    timer(0),
    consumer(0),
    path(_path),
    thread_counter(0),
    lastArchivingThreadName("")
{
    manager_mutex.acquire();
    queue = new DeviceQueue;
    manager_mutex.release();

    ACS_SHORT_LOG((LM_DEBUG, "DeviceMonitoringManager::DeviceMonitoringManager: "
        "DeviceMonitoringManager created."));
}

DeviceMonitoringManager::~DeviceMonitoringManager()
{
    manager_mutex.acquire();
    delete queue;
    manager_mutex.release();

    ACS_SHORT_LOG((LM_DEBUG, "DeviceMonitoringManager::~DeviceMonitoringManager: "
        "DeviceMonitoringManager destroyed."));
}

void DeviceMonitoringManager::stop()
{
    #ifndef NO_CONSUMER
    if(consumer != 0)
    {
        consumer->disconnect();
        /*
         * Do NOT delete the consumer instance. Because this is the consumer,
         * it will shoot itself into the foot twice.
         */
        consumer = 0;
    }
    #endif

    /**
     * Wait for maximum 10 seconds.
     */
    unsigned int counter(10U);
    ACE_Thread_Mutex waitMutex;
    waitMutex.acquire();

    const ACE_Time_Value tenSeconds(1, 0);
    ACE_Time_Value timeToSleep;

    if(container != 0)
    {
        container->getThreadManager()->stop(lastArchivingThreadName.c_str());

        while((counter > 0)
        && (container->getThreadManager()->
                isAlive(lastArchivingThreadName.c_str()) == true))
        {
            ACS_SHORT_LOG((LM_DEBUG, "DeviceMonitoringManager::stop: waiting "
                "1s for the %s thread to finish. %d tries left.",
                lastArchivingThreadName.c_str(),
                counter));

            timeToSleep = (ACE_OS::gettimeofday() + tenSeconds);
            waitMutex.acquire(timeToSleep);

            --counter;
        }
    }

    #ifndef NO_TIMER
    if(container != 0)
    {
        container->getThreadManager()->stop(Timer_Name.c_str());

        counter = 10U;
        while((counter > 0)
        && (container->getThreadManager()->
                isAlive(Timer_Name.c_str()) == true))
        {
            ACS_SHORT_LOG((LM_DEBUG, "DeviceMonitoringManager::stop: waiting "
                "1s for the %s thread to finish. %d tries left.",
                Timer_Name.c_str(),
                counter));

            timeToSleep = (ACE_OS::gettimeofday() + tenSeconds);
            waitMutex.acquire(timeToSleep);

            --counter;
        }

        if(container->getThreadManager()->isAlive(Timer_Name.c_str()) == true)
        {
            container->getThreadManager()->terminate(Timer_Name.c_str());
        }
    }
    #endif

    /**
     * Send the last portions of data which has not been stored yet.
     */
    sendDataToArchiveNow();

    counter = 10U;
    while((counter > 0)
    && (container->getThreadManager()->
            isAlive(lastArchivingThreadName.c_str()) == true))
    {
        ACS_SHORT_LOG((LM_DEBUG, "DeviceMonitoringManager::stop: waiting "
            "1s for the %s thread to finish. %d tries left.",
            lastArchivingThreadName.c_str(),
            counter));
        timeToSleep = (ACE_OS::gettimeofday() + tenSeconds);
        waitMutex.acquire(timeToSleep);
        --counter;
    }

    manager_mutex.acquire();
    delete queue;
    queue = 0;
    manager_mutex.release();
}

void DeviceMonitoringManager::receive(ACS::Time timeStamp,
    const std::string& device,
    const std::string& property,
    const CORBA::Any& value)
{
    #ifdef DEBUG_TIMECONSUMPTION
    highResTimer.start();
    #endif

    MonitorPoint mp(timeStamp, device, property, value);

    manager_mutex.acquire();
    queue->addMonitorPoint(mp);
    manager_mutex.release();

    #ifdef DEBUG_TIMECONSUMPTION
    highResTimer.stop();
    highResTimer.print_ave(highResTimer_text);
    #endif
}

void DeviceMonitoringManager::sendDataToArchiveNow()
{
    if((queue != 0) && (queue->empty() == true))
    {
        ACS_SHORT_LOG((LM_DEBUG, "DeviceMonitoringManager::sendDataToArchiveNow: "
            "No devices in queue yet."));

        return;
    }

    std::auto_ptr< ArchivingThread::structForThread > foo(
        new ArchivingThread::structForThread);

    foo->manager = this;
    DeviceQueue* old_queue(queue);
    queue = new DeviceQueue;
    manager_mutex.acquire();
    foo->queue = old_queue;
    manager_mutex.release();

    ACS_SHORT_LOG((LM_DEBUG, "DeviceMonitoringManager::sendDataToArchiveNow: "
        "DeviceMonitoringManager has now a new queue."));

    /* Try to create a new thread 10 times or until it is successful.
     * FIXME: Add meaningful error handling if thread creation fails.
     */
    unsigned long counter(0UL);
    std::ostringstream c;
    do
    {
        c << "ArchivingThread" << thread_counter++ << std::ends;

        try
        {
            container->getThreadManager()->
                create< ArchivingThread, ArchivingThread::structForThread >(
                    c.str().c_str(), *foo);
            container->getThreadManager()->resume(c.str().c_str());

            // Thread creation is successful (no exception), so leave while loop.
            lastArchivingThreadName = c.str();
            break;
        }
        catch(acsthreadErrType::ThreadAlreadyExistExImpl)
        {
            ACS_SHORT_LOG((LM_WARNING,
                "DeviceMonitoringManager::sendDataToArchiveNow: A thread with "
                "the name \"%s\" already exists. Trying next one.",
                c.str().c_str()));
        }
        catch(acsthreadErrType::CanNotCreateThreadExImpl)
        {
            ACS_SHORT_LOG((LM_WARNING,
                "DeviceMonitoringManager::sendDataToArchiveNow: Cannot create "
                "a new archive thread! Trying again..."));
        }

        ++counter;

        if(counter == 10)
        {
            // Houston, we have a problem.
            ACS_SHORT_LOG((LM_ERROR,
                "DeviceMonitoringManager::sendDataToArchiveNow:\n"
                "********************************************************************************\n"
                "Tried to create a new thread 10 times. No success, so giving up.\n"
                "THIS SHOULD NEVER HAPPEN AND IS A CLEAR EVIDENCE FOR SYSTEM INSTABILITY.\n"
                "This component will be terminated!\n"
                "********************************************************************************"));
            throw(acsthreadErrType::CanNotCreateThreadExImpl(__FILE__, __LINE__,
                "Tried to create a new thread 10 times. No success, giving "
                "up."));
        }
    }
    while(true);

    delete old_queue;

    /* Take care that the thread count number does not exceed
     * the unsigned long type limit.
     * Ok, ok, this would take a while, but be prepared.
     */
    if(thread_counter == unsigned_long_limit)
    {
        thread_counter = 0;
    }

    ACS_SHORT_LOG((LM_DEBUG, "DeviceMonitoringManager::sendDataToArchiveNow: "
        "New archive thread (%s) has been created.", c.str().c_str()));
}
