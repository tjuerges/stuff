/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2006
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Jun 2, 2006  created
*/


#include <archivingThread.h>
#include <deviceMonitoringManager.h>
#include <ArchiveMonitorDataErrType.h>
#include <occiControl.h>
#include <logging.h>


ArchivingThread::ArchivingThread(const ACE_CString &name,
    const structForThread& data,
    const ACS::TimeInterval& responseTime,
    const ACS::TimeInterval& sleepTime):
    ACS::Thread(name, responseTime, sleepTime, true),
    my_name(name.c_str()),
    manager(data.manager),
    queue(*(data.queue))
{
    ACS_SHORT_LOG((LM_DEBUG, "ArchivingThread::ArchivingThread: %s created.",
        my_name.c_str()));
}

ArchivingThread::~ArchivingThread()
{
    ACS_SHORT_LOG((LM_DEBUG, "ArchivingThread::~ArchivingThread: %s destroyed.",
        my_name.c_str()));

    /**
     * ATTENTION: with the Jan-2006 patch of ACS threads, it is not possible to
     * use the logger after the terminate call.
     */
    this->terminate();
}

void ArchivingThread::runLoop()
{
    try
    {
        queue.createDataForArchive();
    }
    catch(const oracle::occi::SQLException& ex)
    {
        ACS_SHORT_LOG((LM_ERROR, "ArchivingThread::run: Exception: "
            "oracle::occi::SQLException (%s)", ex.getMessage().c_str()));
    }
    catch(ArchiveMonitorDataErrType::ConnectFailExImpl& ex)
    {
        ACS_SHORT_LOG((LM_ERROR, "ArchivingThread::run: Exception: "
            "ConnectFailExImpl (%s)", ex.toString().c_str()));
    }
    catch(ArchiveMonitorDataErrType::GetAssemblyIdFailExImpl& ex)
    {
        ACS_SHORT_LOG((LM_ERROR, "ArchivingThread::run: Exception: "
            "GetAssemblyIdFailExImpl (%s)", ex.toString().c_str()));
    }
    catch(ArchiveMonitorDataErrType::GetPropertyTypeIdFailExImpl& ex)
    {
        ACS_SHORT_LOG((LM_ERROR, "ArchivingThread::run: Exception: "
            "GetPropertyTypeIdFailExImpl (%s)", ex.toString().c_str()));
    }
    catch(ArchiveMonitorDataErrType::InsertDataFailExImpl& ex)
    {
        ACS_SHORT_LOG((LM_ERROR, "ArchivingThread::run: Exception: "
            "InsertDataFailExImpl (%s)", ex.toString().c_str()));
    }
    catch(ArchiveMonitorDataErrType::CannotDeterminePropertyTypeExImpl& ex)
    {
        ACS_SHORT_LOG((LM_ERROR, "ArchivingThread::run: Exception: "
            "CannotDeterminePropertyTypeExImpl (%s)", ex.toString().c_str()));
    }
    catch(...)
    {
        ACS_SHORT_LOG((LM_ERROR,"ArchivingThread::run: (%s) "
            "ERROR getting archive and/or sending data.",
            my_name.c_str()));
    }

    this->exit();
}

void ArchivingThread::onStop()
{
    // Erase the device queue.
    queue.clear();
}
