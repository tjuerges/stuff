/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2006
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Jun 2, 2006  created
*/


#include <deviceMonitoringImpl.h>
#include <maciContainerServices.h>


DeviceMonitoringImpl::DeviceMonitoringImpl(const ACE_CString& name,
    maci::ContainerServices* containerServices):
    ACSComponentImpl(name,containerServices),
    container(containerServices),
    manager(0)
{
    ACS_SHORT_LOG((LM_DEBUG, "DeviceMonitoringImpl::DeviceMonitoringImpl: "
        "DeviceMonitoring created."));
}

DeviceMonitoringImpl::~DeviceMonitoringImpl()
{
    ACS_SHORT_LOG((LM_DEBUG, "DeviceMonitoringImpl::~DeviceMonitoringImpl: "
        "DeviceMonitoring destroyed."));
}

void DeviceMonitoringImpl::start()
{
    if(manager == 0)
    {
        manager = new DeviceMonitoringManager(container);

        ACS_SHORT_LOG((LM_INFO, "DeviceMonitoringImpl::start: "
            "DeviceMonitoringManager instance has been created."));
    }
    else
    {
        ACS_SHORT_LOG((LM_INFO, "DeviceMonitoringImpl::start: A "
            "DeviceMonitoring manager instance has already been created."));
    }
}

void DeviceMonitoringImpl::stop()
{
    if(manager != 0)
    {
        /**
         * Tell the DeviceMonitoringManager to shutdown everything.
         * Make sure subsequent calls cannot interfere with the shutdown
         * of the DeviceMonitoringManager::Stop() method. It should not be
         * called twice.
         */
        DeviceMonitoringManager* temp_manager(manager);
        manager = 0;

        temp_manager->stop();

        delete temp_manager;
    }
    else
    {
        ACS_SHORT_LOG((LM_INFO, "DeviceMonitoringImpl::stop: DeviceMonitoring "
            "manager is already stopping."));
    }
}

void DeviceMonitoringImpl::storeToArchive(CORBA::Boolean onoff)
{
    if(manager != 0)
    {
        manager->storeToArchive(onoff);

        if(onoff == true)
        {
            ACS_SHORT_LOG((LM_INFO, "DeviceMonitoringImpl::storeToArchive: "
                "Monitor data storage to the archive is enabled."));
        }
        else
        {
            ACS_SHORT_LOG((LM_INFO, "DeviceMonitoringImpl::storeToArchive: "
                "Monitor data storage to the archive is disabled."));
        }
    }
}

void DeviceMonitoringImpl::storeToDisc(CORBA::Boolean onoff)
{
    if(manager != 0)
    {
        manager->storeToDisc(onoff);

        if(onoff == true)
        {
            ACS_SHORT_LOG((LM_INFO, "DeviceMonitoringImpl::storeToDisc: "
                "Monitor data storage to disc is enabled."));
        }
        else
        {
            ACS_SHORT_LOG((LM_INFO, "DeviceMonitoringImpl::storeToDisc: "
                "Monitor data storage to disc is disabled."));
        }
    }
}

void DeviceMonitoringImpl::execute()
{
    start();
}

void DeviceMonitoringImpl::cleanUp()
{
    stop();

    ACS_SHORT_LOG((LM_INFO, "DeviceMonitoringImpl::cleanup: All threads are "
        "stopped, connections disabled."));
}

/////////////////////////////////////////////////
// MACI DLL support functions
/////////////////////////////////////////////////
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(DeviceMonitoringImpl)
