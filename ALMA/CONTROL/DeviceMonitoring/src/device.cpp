/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2006
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Jun 2, 2006  created
*/

#include <device.h>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <Guard_T.h>
#include <deviceMonitoringManager.h>
#include <archiveMonitorData.h>


std::string Device::path;
Device::FilenameMap Device::filenames;
bool Device::storeToRealDb(true);
bool Device::storeToFile(true);
ACE_Mutex Device::storeTo_mutex;

Device::Device(const MonitorPoint& mp):
    archiveComputer(DeviceMonitoringManager::getArchiveComputer()),
    archiveDb(DeviceMonitoringManager::getArchiveDb()),
    archiveUserName(DeviceMonitoringManager::getArchiveUserName()),
    archivePassword(DeviceMonitoringManager::getArchivePassword())
{
    ACE_Guard< ACE_Mutex > guard(device_mutex);

    barCode = mp.getBarCode();
    deviceName = mp.getDeviceName();

    mps.insert(std::make_pair(mp.getPropertyName(), mp));

    if(path.empty() == true)
    {
        setupPath();
    }

    setupFilename();
}

Device::Device(const Device& d):
    archiveComputer(DeviceMonitoringManager::getArchiveComputer()),
    archiveDb(DeviceMonitoringManager::getArchiveDb()),
    archiveUserName(DeviceMonitoringManager::getArchiveUserName()),
    archivePassword(DeviceMonitoringManager::getArchivePassword())

{
    if(this != &d)
    {
        ACE_Guard< ACE_Mutex > guard(device_mutex);

        mps = const_cast< Device& >(d).getMonitorPoints();
        barCode = const_cast< Device& >(d).getBarCode();
        deviceName = const_cast< Device& >(d).getDeviceName();
    }
}

Device& Device::operator=(const Device& d)
{
    if(this != &d)
    {
        ACE_Guard< ACE_Mutex > guard(device_mutex);

        mps = const_cast< Device& >(d).getMonitorPoints();
        barCode = const_cast< Device& >(d).getBarCode();
        deviceName = const_cast< Device& >(d).getDeviceName();

        archiveComputer = DeviceMonitoringManager::getArchiveComputer();
        archiveDb = DeviceMonitoringManager::getArchiveDb();
        archiveUserName = DeviceMonitoringManager::getArchiveUserName();
        archivePassword = DeviceMonitoringManager::getArchivePassword();
    }

    return *this;
}

Device::~Device()
{
}

void Device::addMonitorPoint(const MonitorPoint& mp)
{
    ACE_Guard< ACE_Mutex > guard(device_mutex);
    mps.insert(std::make_pair(mp.getPropertyName(), mp));
}

void Device::createData(EpochHelper& now)
{
    ACE_Guard< ACE_Mutex > guard(device_mutex);
    ACE_Guard< ACE_Mutex > storageGuard(Device::storeTo_mutex);

    if(storeToFile == true)
    {
        saveDeviceAsCSV(now);
    }

    if(storeToRealDb == true)
    {
        saveDeviceToRealDb(now);
    }
}

void Device::saveDeviceAsCSV(EpochHelper& now) const
{
    FilenameMap_citer i(filenames.find(barCode));

    if(i != filenames.end())
    {
        const std::string filename(i->second);
        std::ofstream output(filename.c_str(),
            std::ios_base::out | std::ios_base::app);
        if(output)
        {
            for(MP_multimap_citer i(mps.begin()); i != mps.end();)
            {
                // Find first and last iterators for entries with the same key:
                MP_range range(mps.equal_range(i->first));
                const std::string propertyName(i->first);

                for(MP_multimap_citer j(range.first); j != range.second; ++j)
                {
                    output << propertyName;
                    output << ", ";
                    output << j->second.getTimeStampString();
                    output << ", ";
                    output << j->second.getValueString();
                    output << "\n";
                }
                /* Now find the next key since the above loop iterated through
                 * all entries for the last key.
                 */
                i = mps.upper_bound(i->first);
            }
            output.close();
        }
        else
        {
            ACS_SHORT_LOG((LM_ERROR, "Device::saveDeviceAsCSV: ERROR! "
                "Could not write data to file %s.",
                filename.c_str()));
        }
    }
    else
    {
        ACS_SHORT_LOG((LM_ERROR, "Device::saveDeviceAsCSV: It seems that there "
            "is a serious problem with the file which belongs to the barcode %s."
            " Check if the disk is full or other possibilities.",
            barCode.c_str()));
    }
}

void Device::setupFilename()
{
    FilenameMap_citer alreadyActive(filenames.find(barCode));

    if(alreadyActive == filenames.end())
    {
        acstime::Epoch my_epoch(TimeUtil::ace2epoch(ACE_OS::gettimeofday()));
        EpochHelper my_epochhelper(my_epoch);

        std::string iso_timedate(my_epochhelper.toString(
            acstime::TSUTC, "%G", 0L, 0L));
        iso_timedate.erase(iso_timedate.end()-1);

        long milliseconds(static_cast< long >(
            my_epochhelper.microSecond() / CORBA::Long(1000L)));
        std::ostringstream ostr;
        ostr << iso_timedate << ".";
        ostr.width(3);
        ostr.fill('0');
        ostr << milliseconds;

        std::string filename(ostr.str());

        filename.append("-");
        filename.append(barCode);
        filename.append(".csv");
        std::replace(filename.begin(), filename.end(), '/', '_');

        filename.insert(0, "/");
        filename.insert(0, path);

        filenames.insert(std::make_pair(barCode, filename));

        std::ofstream output(filename.c_str(),
            std::ios_base::out | std::ios_base::app);
        if(output)
        {
            output << "Monitoring the device ";
            output << barCode;
            output << ".\nTimestamp of begin of data acquisition: ";
            output << iso_timedate;
            output << "\n\nProperty, ISO-8601 timestamp, Value\n";
            output.close();
        }
        else
        {
            ACS_SHORT_LOG((LM_ERROR, "Device::setupFilename: Error "
                "while setup up of the data file %s. Cannot open the file!",
                filename.c_str()));
        }

        ACS_SHORT_LOG((LM_INFO,"Device::setupFilename: Ok, "
            "will save data for this device (%s) to file %s.",
            barCode.c_str(),
            filename.c_str()));
    }
    else
    {
        ACS_SHORT_LOG((LM_DEBUG,"Device::setupFilename: OK, "
            "the file for this barcode (%s) is already active. "
            "No new file will be created.",
            barCode.c_str()));
    }
}

void Device::setupPath()
{
    const char* env_path(ACE_OS::getenv("ACSDATA"));

    if(env_path != NULL)
    {
        path = env_path;
    }
    else
    {
        path = "/var/tmp";
    }

    ACS_SHORT_LOG((LM_INFO,"Device::setupPath: Saving CSV monitor point data "
        "to %s.", path.c_str()));
}

/*
struct PropertySample
{
    EpochHelper sampleTime;
    CORBA::Any value;
};

struct DeviceData
{
  std::string serialNumber;
  std::string propertyTypeName;
  std::string deviceName;
  EpochHelper archivalTimeStamp;
  std::vector< PropertySample > samples;
};
*/
void Device::saveDeviceToRealDb(EpochHelper& now) const
{
    ArchiveMonitorData* archive(0);
    std::ostringstream archiveConnection("//");
    archiveConnection << archiveComputer
        << "/"
        << archiveDb;
    try
    {
        archive = new ArchiveMonitorData(archiveConnection.str(),
            archiveUserName,
            archivePassword);
    }
    catch(const ArchiveMonitorDataErrType::ConnectFailExImpl&)
    {
        ACS_SHORT_LOG((LM_ERROR, "Device::saveDeviceToRealDb: "
            "Could not connect to the DB. This is a critical failure. No "
            "archiving is possible."));
        return;
    }

    for(MP_multimap_citer i(mps.begin()); i != mps.end();)
    {
        /**
         * Now i iterates through the multimap.
         * The multimap looks like this:
         * std::pair< std::string("Property name"), MonitorPoint >
         *
         * "bar", MP1
         * "dummy", MP2
         * "dummy", MP3
         * "foo", MP4
         * "foo", MP5
         * "foobar", MP6
         *
         * Yes, the std::multimap is always automagically sorted.
         *
         * Now find first and last iterators for entries with the same key:
         */
        MP_range range(mps.equal_range(i->first));

        /**
         * range->first is now pointing to "bar", range.second = the first "dummy"
         * entry.
         *
         * In the next loop, range.first = first "dummy", range.second = first
         * "foo".
         */

        ArchiveMonitorData::DeviceData data;
        ArchiveMonitorData::PropertySample sample;

        /**
         * Set the serial number.
         */
        data.serialNumber = barCode;

        /**
         * Fill in the device name.
         */
        data.deviceName = deviceName;

        /**
         * Set the archival time stamp.
         */
        data.archivalTimeStamp = now.value().value;

        /**
         * Fill the property name in.
         */
        data.propertyName = i->first;

        for(MP_multimap_citer j(range.first); j != range.second; ++j)
        {
            /**
             * j->second is now the MonitorPoint.
             */

            /**
             * Fill the data into the sample point.
             */
            sample.sampleTime = j->second.getTimeStamp();

            sample.value = j->second.getValue();

            /**
             * Append sample point to the DeviceData.
             */
            data.samples.push_back(sample);
            // Done.
        }

        /**
         * Store the stuff into the archive.
         */
        try
        {
            archive->insertData(data);
        }
        catch(const ArchiveMonitorDataErrType::InsertDataFailExImpl& ex)
        {
            ACS_SHORT_LOG((LM_WARNING, "Device::saveDeviceToRealDb: "
                "The data for the property %s/%s could not be handed over "
                "to the archive. The reason for this is with high probability "
                "one of the following two: "
                "1. The Oracle-Db is not ready to store this property. Check "
                "if there are appropriate entries in the ASSEMBLYTYPE and "
                "PROPERTYTYPE tables which match the component name, the "
                "property name and the property type."
                "2. Does the property contain a sequence? The Oracle-"
                "Db cannot store sequences and it seems that no conversion to "
                "single values has been done in the component which created "
                "this property.",
                data.deviceName.c_str(),
                data.propertyName.c_str()));
        }

        /**
         * Now find the next key since the above loop iterated through
         * all entries for the last key.
         */
        i = mps.upper_bound(i->first);
    }

    delete archive;
}
