// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2006
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

static char *rcsId="@(#) $Id$";
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <ambDeviceInt.h> // Base class for BareAMBDeviceInt
#include <semaphore.h> // for sem_t
#include <ambDefs.h> // For AMBERR defs
#include <acstimeEpochHelper.h> // for EpochHelper
#include <acstimeDurationHelper.h> // for DurationHelper
#include <TETimeUtil.h> // for TETimeUtil
#include <unistd.h> // for usleep
#include <deque> // for deque
#include <iomanip> // for std::setprecision and std::fixed
#include <iostream> // for std::cout and std::endl
#include <ace/Get_Opt.h>
#include <sstream>

const static double defaultDuration(10.0);
const static unsigned short int defaultNodeId(60U);
const static unsigned short int defaultChannel(0U);
const static unsigned short int defaultNumberRequests(40U);
const static unsigned int defaultRca(0x30000U);

double duration(defaultDuration);
unsigned short int nodeId(defaultNodeId);
unsigned short int channel(defaultChannel);
unsigned short int requests(defaultNumberRequests);
unsigned int rca(defaultRca);

// This is how often the monitor loop runs (once every 20 TE's).
const ACS::TimeInterval loopTime(20 * TETimeUtil::TE_PERIOD_DURATION.value);
// time format string.
const std::string timeFmt("%Y-%m-%dT%H:%M:%S.%3q");

const char* Errors[] ={
    "AMBERR_NOERR",
    "AMBERR_CONTINUE",
    "AMBERR_BADCMD",
    "AMBERR_BADCHAN",
    "AMBERR_UNKNOWNDEV",
    "AMBERR_INITFAILED",
    "AMBERR_WRITEERR",
    "AMBERR_READERR",
    "AMBERR_FLUSHED",
    "AMBERR_TIMEOUT",
    "AMBERR_RESPFIFO",
    "AMBERR_NOMEM",
    "AMBERR_PENDING"
};

void usage(char *argv0)
{
    std::cout << std::endl
        << argv0
        << "::usage:"
        << std::endl
        << "Options:"
        << std::endl
        << "[--help|h]:"
        << std::endl
        << "     You are reading it!"
        << std::endl
        << "[--duration=|-d<TIME in fractional s>]:"
        << std::endl
        << "      Duration of the test. Default = "
        << defaultDuration
        << std::endl
        << "[--nodeId=|-n<NODEID>]:"
        << std::endl
        << "      NodeId to be used. Default = "
        << defaultNodeId
        << std::endl
        << "[--channel=|-c<CHANNEL>]:"
        << std::endl
        << "      ABM channel. Default = "
        << defaultChannel
        << std::endl
        << "[--monitorrequests=|-m<NUMBER>]:"
        << std::endl
        << "      Number of monitor requests to send per TE Min. = 1; max. = 40. "
        << "Default = "
        << defaultNumberRequests
        << std::endl
        << "[--rca=|-r<RCA>]:"
        << std::endl
        << "      RCA of monitor request to use per request. Min. = 0x1; max. = "
        << "0x7fffff. Default = "
        << defaultRca
        << std::endl;
};

// This trivial class gets me past the restriction that all the constructors
// for the AmbDeviceInt class are protected.
class BareAMBDeviceInt: public AmbDeviceInt
{
    public:
    BareAMBDeviceInt():
        AmbDeviceInt()
    {
    };

    virtual ~BareAMBDeviceInt()
    {
        AmbInterface::deleteInstance();
    };

    virtual bool initialize(int AMBChannel, int nodeID)
    {
        m_channel = AMBChannel;
        m_nodeAddress = nodeID;
        try
        {
            interface_mp = AmbInterface::getInstance();
        }
        catch(ControlExceptions::CAMBErrorExImpl& ex)
        {
            std::cout << "Unable to get an instance of AmbInterface" << std::endl;
            std::cout << "Have you loaded the kernel modules?" << std::endl;
            return false;
        }

        return true;
    }
};

struct RequestStruct
{
    AmbRelativeAddr RCA;
    ACS::Time TargetTime;
    AmbDataLength_t DataLength;
    AmbDataMem_t Data[8];
    ACS::Time Timestamp;
    AmbErrorCode_t Status;
};

int main(int argc, char* argv[])
{
    ACE_Get_Opt options(argc, argv);

    options.long_option("help", 'h');
    options.long_option("duration", 'd', ACE_Get_Opt::ARG_OPTIONAL);
    options.long_option("nodeId", 'n', ACE_Get_Opt::ARG_OPTIONAL);
    options.long_option("channel", 'c', ACE_Get_Opt::ARG_OPTIONAL);
    options.long_option("monitorrequests", 'm', ACE_Get_Opt::ARG_OPTIONAL);
    options.long_option("rca", 'r', ACE_Get_Opt::ARG_OPTIONAL);

    int c(0);
    while((c = options()) != EOF)
    {
        switch(c)
        {
            case 'd':
            {
                if(options.opt_arg() != 0)
                {
                    std::istringstream value;
                    value.str(options.opt_arg());
                    double val(0);
                    value >> val;
                    if(val < 0.001)
                    {
                        std::cout << "The minimum duration is 0.001s. The "
                            << "value will not be used."
                            << std::endl;

                    }
                    else
                    {
                        duration = val;
                        std::cout << "The test duration has been set to "
                            << duration
                            << "s."
                            << std::endl;
                    }
                }
            }
                break;

            case 'n':
            {
                if(options.opt_arg() != 0)
                {
                    std::istringstream value;
                    value.str(options.opt_arg());
                    unsigned int val(0U);
                    value >> val;
                    if((val < 0) || (val > 0x200))
                    {
                        std::cout << "The node ID has to be in the range of "
                            << "(0; 512). The value will not be used."
                            << std::endl;
                    }
                    else
                    {
                        nodeId = val;
                        std::cout << "The node ID has been set to "
                            << nodeId
                            << "."
                            << std::endl;
                    }
                }
            }
                break;

            case 'c':
            {
                if(options.opt_arg() != 0)
                {
                    std::istringstream value;
                    value.str(options.opt_arg());
                    short int val(0);
                    value >> val;
                    if((val < 0) || (val > 5))
                    {
                        std::cout << "The channel number has to be in the "
                            << "range of (0; 5). The value will not be used."
                            << std::endl;
                    }
                    else
                    {
                        channel = val;
                        std::cout << "The ABM channel has been set to "
                            << channel
                            << "."
                            << std::endl;
                    }
                }
            }
                break;

            case 'm':
            {
                if(options.opt_arg() != 0)
                {
                    std::istringstream value;
                    value.str(options.opt_arg());
                    unsigned short val(0U);
                    value >> val;
                    if((val < 1) || (val > 1000))
                    {
                        std::cout << "The number of requests has to be in the "
                            << "range (1; 1000)! The value will not be used."
                            << std::endl;
                    }
                    else
                    {
                        requests = val;
                        std::cout << "The number of monitor requests per TE "
                            << "has been set to "
                            << requests
                            << "."
                            << std::endl;
                    }
                }
            }
                break;

            case 'r':
            {
                if(options.opt_arg() != 0)
                {
                    std::istringstream value;
                    value.str(options.opt_arg());
                    unsigned int val(0U);
                    value >> val;
                    if((val < 1) || (val > 0x7fffff))
                    {
                        std::cout << "The RCA has to be in the range "
                            << "(0x1; 0x7fffff)! The value will not be used."
                            << std::endl;
                    }
                    else
                    {
                        rca = val;
                        std::cout << "The RCA has been set to 0x"
                            << std::hex
                            << rca
                            << std::dec
                            << "."
                            << std::endl;
                    }
                }
            }
                break;

            case '?':
            {
                std::cout << "Option -"
                    << options.opt_opt()
                    << "requires an argument!"
                    << std::endl;
            }
                // Fall through...
            case 'h':
            default:    // Display help.
            {
                usage(argv[0]);
                return -1;
            }
        }
    }

    BareAMBDeviceInt mc;
    if(mc.initialize(channel, nodeId) == false)
    {
        return -ENODEV;
    }

    sem_t monitorSemaphore;
    sem_init(&monitorSemaphore, 0, 0);

    // This is a list of all the RCA's that need to be monitored every TE.
    std::vector< AmbRelativeAddr > rcaList;
    for(unsigned short int i(0); i < requests; ++i)
    {
        rcaList.push_back(rca);
    }

    EpochHelper timeNow(TETimeUtil::ceilTE(
        TimeUtil::ace2epoch(ACE_OS::gettimeofday())));

    EpochHelper lastScheduledTime(timeNow.value());
    lastScheduledTime.add(DurationHelper(loopTime).value());

    EpochHelper stopTime(lastScheduledTime.value());
    stopTime.add(DurationHelper(static_cast< long double >(duration)).value());

    EpochHelper horizon(timeNow.value());
    horizon.add(DurationHelper(loopTime * 2).value());
    std::deque< RequestStruct > monitorQueue;

    unsigned long total(0UL);
    unsigned long failure(0UL);
    unsigned long success(0UL);
    unsigned long noMem(0UL);
    unsigned long timeOut(0UL);
    unsigned long tooLate(0);
    unsigned long pending(0UL);
    unsigned long cont(0UL);

    double minTime(1E30);
    double maxTime(-1E30);
    bool doExit(false);

    do
    {
        if((horizon >= stopTime.value()))
        {
            horizon.value(stopTime.value());
            doExit = true;
        }

        {
            DurationHelper queueTime(horizon.difference(
                lastScheduledTime.value()));
            const unsigned int numTEs(
                queueTime.value().value /
                    TETimeUtil::TE_PERIOD_DURATION.value);
            int monitorsPerTE(rcaList.size());
            const unsigned int numRequests(numTEs * monitorsPerTE);
            EpochHelper currentDateTime(
                TimeUtil::ace2epoch(ACE_OS::gettimeofday()));
            std::cout << currentDateTime.toString(
                acstime::TSArray, timeFmt.c_str(), 0,0).c_str()
                << ": Queueing "
                << numRequests
                << " monitor requests ("
                << queueTime.toSeconds()
                << " seconds worth)."
                << std::endl
                << "Queue size before addition = "
                << monitorQueue.size()
                << std::endl
                << "Queue size after addition = "
                << monitorQueue.size() + numRequests
                << std::endl;

                total += numRequests;
        }

        RequestStruct thisRequest;
        // Need to schedule the monitor requests 24ms after the TE
        thisRequest.TargetTime = lastScheduledTime.value().value +
            TETimeUtil::TE_PERIOD_DURATION.value / 2;
        thisRequest.Status = AMBERR_PENDING;
        thisRequest.DataLength = 8;
        thisRequest.RCA = rca;
        thisRequest.Timestamp = 0;
        const ACS::Time lastTime(horizon.value().value);
        ACS::Time lastTargetTime(0ULL);

        while(thisRequest.TargetTime < lastTime)
        {
            for(std::vector< AmbRelativeAddr >::const_iterator i(
                rcaList.begin()); i != rcaList.end(); ++i)
            {
                thisRequest.RCA = *i;
                monitorQueue.push_back(thisRequest);
                // As the push_back function (above) will do a copy this gets a
                // reference to the copy and this is needed as the monitorTE function
                // uses the addresses.
                RequestStruct& nextRequest(monitorQueue.back());
                mc.monitorTE(nextRequest.TargetTime,
                    nextRequest.RCA,
                    nextRequest.DataLength,
                    nextRequest.Data,
                    &monitorSemaphore,//NULL,
                    &nextRequest.Timestamp,
                    &nextRequest.Status);

                lastTargetTime = nextRequest.TargetTime;
            }

            thisRequest.TargetTime += TETimeUtil::TE_PERIOD_DURATION.value;
        }

        std::cout << "Last time queued = "
            << lastTargetTime
            << std::endl
            << "Difference last time queued - current time = "
            << lastTargetTime
                - TimeUtil::ace2epoch(ACE_OS::gettimeofday()).value
            << std::endl;

        lastScheduledTime.value(horizon.value());
        usleep(static_cast< useconds_t >(DurationHelper(loopTime).toSeconds()
            * 1E6));

        pending = 0UL;
        unsigned int skipped = 0UL;
        cont = 0UL;
        unsigned int counter(0U);
        while(sem_trywait(&monitorSemaphore) == 0)
        {
            ++counter;

            std::deque< RequestStruct >::iterator curMonitor(
                monitorQueue.begin());
            while(curMonitor != monitorQueue.end() && (*curMonitor).Status == AMBERR_PENDING)
            {
                ++curMonitor;
                ++skipped;
            }

            if(curMonitor == monitorQueue.end())
            {
                std::cout << "Argh!" << std::endl;
                return -EFAULT;
            }

            EpochHelper commandedTime((*curMonitor).TargetTime);
            EpochHelper actualTime((*curMonitor).Timestamp);
            DurationHelper diff(actualTime.difference(commandedTime.value()));
            const double executionTime(diff.toSeconds());

            switch((*curMonitor).Status)
            {
                case AMBERR_CONTINUE:
                {
                    ++cont;
                }
                break;

                case AMBERR_TIMEOUT:
                {
                    ++timeOut;

                    std::cout << "*** TIMEOUT ***"
                        << std::endl
                        << "Commanded Time: "
                        << commandedTime.toString(
                            acstime::TSArray, timeFmt.c_str(), 0,0).c_str()
                        << ". Actual Time "
                        << actualTime.toString(
                            acstime::TSArray, timeFmt.c_str(), 0,0).c_str()
                        << std::fixed
                        << std::setprecision(6)
                        << ", difference = "
                        << executionTime
                        << ", RCA = "
                        << (*curMonitor).RCA
                        << ", datalength = "
                        << (*curMonitor).DataLength
                        << ", status = "
                        << Errors[(*curMonitor).Status]
                        << std::endl << std::endl;
                }
                break;

                case AMBERR_NOMEM:
                {
                    ++noMem;

                    std::cout << "*** NO MEMORY ***"
                        << std::endl
                        << "Commanded Time = "
                        << commandedTime.toString(
                            acstime::TSArray, timeFmt.c_str(), 0,0).c_str()
                        << ", actual Time = "
                        << actualTime.toString(
                            acstime::TSArray, timeFmt.c_str(), 0,0).c_str()
                        << std::fixed
                        << std::setprecision(6)
                        << ", difference = "
                        << executionTime
                        << ", RCA = "
                        << (*curMonitor).RCA
                        << ", datalength = "
                        << (*curMonitor).DataLength
                        << ", status = "
                        << Errors[(*curMonitor).Status]
                       << std::endl << std::endl;
                }
                break;

                default:
                {
                    if(executionTime > 0.020)
                    {
                        ++tooLate;
                    }

                    if(executionTime < minTime)
                    {
                        minTime = executionTime;
                    }

                    if(executionTime > maxTime)
                    {
                        maxTime = executionTime;
                    }

                    if((*curMonitor).Status != AMBERR_NOERR)
                    {
                        ++failure;

                        std::cout << "Commanded Time = "
                            << commandedTime.toString(
                                acstime::TSArray, timeFmt.c_str(), 0,0)
                            << ", actual Time = "
                            << actualTime.toString(
                                acstime::TSArray, timeFmt.c_str(), 0,0)
                            << std::fixed
                            << std::setprecision(6)
                            << ", difference = "
                            << executionTime
                            << ", RCA = "
                            << (*curMonitor).RCA
                            << ", datalength = "
                            << (*curMonitor).DataLength
                            << ", status = "
                            << Errors[(*curMonitor).Status]
                            << std::endl;
                    }
                    else
                    {
                        ++success;
                    }
                }
            }

            monitorQueue.erase(curMonitor);
        }

        ACS::Time targetTime(monitorQueue.front().TargetTime);
        ACS::Time targetTeDiff(lastTargetTime - targetTime);
        ACS::Time reallyNow(TimeUtil::ace2epoch(ACE_OS::gettimeofday()).value);

        std::cout << "Front of queue target time = "
            << targetTime
            << std::endl
            << "Difference = "
            << targetTeDiff / 1e7
            << std::endl
            << "Difference front of queue time - current time = ";
            if (targetTime > reallyNow) 
                std::cout << targetTime - reallyNow << std::endl;
            else
                std::cout <<  "-" << reallyNow - targetTime << std::endl;
             

        horizon.value(
            TETimeUtil::ceilTE(TimeUtil::ace2epoch(ACE_OS::gettimeofday())));
        horizon.add(DurationHelper(loopTime * 2).value());

        std::cout << "Queue size after processing = "
            << monitorQueue.size()
            << std::endl
            << "Total number of requests = "
            << total
            << std::endl
            << "Successfully executed requests = "
            << success
            << std::endl
            << "Requests with status AMBERR_CONTINUE = "
            << cont
            << std::endl
            << "Processed requests = "
            << counter
            << std::endl
            << "Skipped requests = "
            << skipped
            << std::endl
            << "Requests executed too late = "
            << tooLate
            << std::endl
            << "Requests failed (AMBERR_TIMEOUT) = "
            << timeOut
            << std::endl
            << "Requests failed (AMBERR_NOMEM) = "
            << noMem
            << std::endl
            << "Requests failed (other reason) = "
            << failure
            << std::endl << std::endl;
    }
    while(doExit == false);

    usleep(static_cast< useconds_t >(
        DurationHelper(loopTime * 5).toSeconds() * 1E6));

    while(monitorQueue.empty() == false)
    {
        RequestStruct& curMonitor(monitorQueue.front());
        monitorQueue.pop_front();
        EpochHelper commandedTime(curMonitor.TargetTime);
        EpochHelper actualTime(curMonitor.Timestamp);
        DurationHelper diff(actualTime.difference(commandedTime.value()));
        double executionTime(diff.toSeconds());

        switch(curMonitor.Status)
        {
            case AMBERR_PENDING:
            {
                ++pending;
            }
            break;

            case AMBERR_CONTINUE:
            {
                ++cont;
            }
            break;

            case AMBERR_TIMEOUT:
            {
                ++timeOut;

                std::cout << "*** TIMEOUT ***"
                    << std::endl
                    << "Commanded Time: "
                    << commandedTime.toString(
                        acstime::TSArray, timeFmt.c_str(), 0,0).c_str()
                    << ". Actual Time "
                    << actualTime.toString(
                        acstime::TSArray, timeFmt.c_str(), 0,0).c_str()
                    << std::fixed
                    << std::setprecision(6)
                    << ", difference = "
                    << executionTime
                    << ", RCA = "
                    << curMonitor.RCA
                    << ", datalength = "
                    << curMonitor.DataLength
                    << ", status = "
                    << Errors[curMonitor.Status]
                    << std::endl << std::endl;
            }
            break;

            case AMBERR_NOMEM:
            {
                ++noMem;

                std::cout << "*** NO MEMORY ***"
                    << std::endl
                    << "Commanded Time = "
                    << commandedTime.toString(
                        acstime::TSArray, timeFmt.c_str(), 0,0).c_str()
                    << ", actual Time = "
                    << actualTime.toString(
                        acstime::TSArray, timeFmt.c_str(), 0,0).c_str()
                    << std::fixed
                    << std::setprecision(6)
                    << ", difference = "
                    << executionTime
                    << ", RCA = "
                    << curMonitor.RCA
                    << ", datalength = "
                    << curMonitor.DataLength
                    << ", status = "
                    << Errors[curMonitor.Status]
                   << std::endl << std::endl;
            }
            break;

            default:
            {
                if(executionTime > 0.020)
                {
                    ++tooLate;
                }

                if(executionTime < minTime)
                {
                    minTime = executionTime;
                }

                if(executionTime > maxTime)
                {
                    maxTime = executionTime;
                }

                if(curMonitor.Status != AMBERR_NOERR)
                {
                    ++failure;

                    std::cout << "Commanded Time = "
                        << commandedTime.toString(
                            acstime::TSArray, timeFmt.c_str(), 0,0)
                        << ", actual Time = "
                        << actualTime.toString(
                            acstime::TSArray, timeFmt.c_str(), 0,0)
                        << std::fixed
                        << std::setprecision(6)
                        << ", difference = "
                        << executionTime
                        << ", RCA = "
                        << curMonitor.RCA
                        << ", datalength = "
                        << curMonitor.DataLength
                        << ", status = "
                        << Errors[curMonitor.Status]
                        << std::endl;
                }
                else
                {
                    ++success;
                }

            }
        }

        monitorQueue.pop_front();
    }

    sem_destroy(&monitorSemaphore);

    std::cout << std::endl << std::endl
        << "Sent "
        << total
        << " monitor points. Problems with "
        << failure
        << " of them ("
        << 100.0 * (failure + noMem + timeOut) / total
        << "%)."
        << std::endl
        << "Min / Max execution times: "
        << minTime * 1000
        << "ms / "
        << maxTime * 1000
        << "ms"
        << std::endl
        << tooLate
        << " monitor points ("
        << 100.0 * tooLate / total
        << "%) were executed later than 20ms after the requested time."
        << std::endl
        << cont
        << " monitor points ("
        << 100.0 * cont / total
        << "%) had status AMBERR_CONTINUE."
        << std::endl
        << pending
        << " monitor points ("
        << 100.0 * pending / total
        << "%) were pending."
        << std::endl
        << noMem
        << " monitor points ("
        << 100.0 * noMem / total
        << "%) failed because of no memory."
        << std::endl
        << timeOut
        << " monitor points ("
        << 100.0 * timeOut / total
        << "%) timed out."
        << std::endl;

    return 0;
}
