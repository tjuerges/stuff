#! /usr/bin/env python
#
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2009
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# $Id$
#


import CCL.AmbManager
import CCL.logging
import CCL.HardwareController
import CCL.CVR
import CCL.LS
import CCLExceptionsImpl
import Control
import string


def printEx(ex = None):
    if ex == None:
        print "Please provide an ACS exception!"
    else:
        Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace).log(Acspy.Common.Log.getLogger());

class testLS(CCL.HardwareController.HardwareController):
    def __init__(self, photonicReference = None,
                 componentName = None, stickyFlag = False):
        '''
        The constructor creates a PhotonicReference object using default
        constructor.  You can specify which LS you would like either by number
        (1-6) or by PhotonicReference (i.e. PhotonicReference2)
        EXAMPLE:
        import CCL.PhotonicReference
        # To get the PhotonicReference associated with photonic reference 3
        pr = CCL.PhotonicReference.PhotonicReference(3)
        or 
        pr = CCL.PhotonicReference.PhotonicReference("PhotonicReference3")
        '''
        self.__logger = CCL.logging.getLogger()

        if componentName is None:
            if photonicReference is None:
                raise IllegalParameterErrorEx('You must specify either a '
                                              + ' photonic reference');
            if isinstance(photonicReference, int):
                photonicReference= 'PhotonicReference%d' % photonicReference

            componentName = "CONTROL/CentralLO/%s" % photonicReference

        name = componentName.split('/')[2]
        self._prNo = int(name[len(name)-1])
        self._componentName = componentName

        try:
            CCL.HardwareController.HardwareController.__init__(self,
                componentName, stickyFlag)
        except Exception, e:
            print 'PhotonicReference component is not running \n' + str(e)

    def status(self):
        '''
        Get the combined status, formed by asking the underlying
        controllers for their state and combining them.
    
        See ScriptImpl.CCLObject for the complete documentation of this
        method.
        '''
        devStates = {Control.HardwareController.Shutdown: 'Shutdown',
                     Control.HardwareController.Operational: 'Operational',
                     Control.HardwareController.Degraded: 'Degraded'}
        return devStates[self._HardwareController__controller.getState()]

    def setFrequency(self, frequency = None):
        '''
        Set the frequency to which the LS shall be locked to.
        The parameter frequency is in Hertz!
        '''
        if frequency == None:
            self.__logger.logWarning(
                "You must provide a LS target frequency in Hertz!")
            return

        self._HardwareController__controller.setFrequency(frequency)

    def getFrequency(self):
        '''
        Get the current LS frequency.
        '''
        return self._HardwareController__controller.getFrequency()

    def setFrequencyAdvanced(self, targetFrequency = None, harmonic = None):
        '''
        This method allows to tune the LS without using the preset harmonics.
        advancedTuning(targetFrequency = nu[HZ|GHz], harmonic = float)
        The method will either return the list of errors in the LS or it will
        log a success message.
        ''' 
        if targetFrequency == None:
            self.__logger.logWarning("You must provide a target frequency!")
            return
    
        if targetFrequency < 1000.0:
            self.__logger.logInfo("I am assuming you entered the target" 
                "frequency (%f) in GHz." % targetFrequency)
            targetFrequency = targetFrequency * 1.0e9
            self.__logger.logInfo("Set the target frequency to %fHz."
                % targetFrequency)
    
        maxTimeout = 30.0
        sleepTime = 0.5
    
        import time
    
        # import CCL.SIConverter
        # si =  CCL.SIConverter.SIConverter()
        # Calculate the master Laser frequency.  The Master Laser is a single
        # frequency device which is extremely stable.  Therefore the wave
        # length given in the ICD, p. 9, is used as a constant throughout all
        # the photonic reference code.
        # MasterLaserFrequency = si.toHertz("1556 nm")
        MasterLaserFrequency = 192668674807197.937500
        self.__logger.logInfo("Master Laser frequency = %fHz."
            % MasterLaserFrequency)
    
        import CCL.CVR
        cvr = CCL.CVR.CVR(self._prNo)
    
        # Use the 5th harmonic if targetFrequency <= 80GHz.  7th if
        # it is > 80GHz.
        if harmonic == None:
            if targetFrequency > 80.0e9:
                harmonic = 7.0
            else:
                harmonic = 5.0
    
        self.__logger.logInfo("Harmonic = %f." % harmonic)
    
        import CCL.LS
        ls = CCL.LS.LS(self._prNo)
    
        # Clear all previous errors.
        ls.SET_SYSTEM_CLEAR_ERRORS()
    
        # Set the LS to standby mode.
        ls.SET_SYSTEM_STANDBY_MODE_REQUEST()
    
        # Start the tuning.
        # nu_Tuning = nu_Master_Laser - nu_target
        tuningFrequency = MasterLaserFrequency - targetFrequency
        self.__logger.logInfo("LS tuning frequency = %fHz." % tuningFrequency)
        ls.SET_PHASELOCK_COMMAND_TUNING_INIT(tuningFrequency)
    
        # Wait for the LS tuning algorithm to flag it is ready.
        status = ls.GET_PHASELOCK_GET_STATUS_TUNING_READY()[0]
        timeout = 0
        self.__logger.logInfo("Waiting for the tuning system to become "
            "ready...")
        while status != True and timeout != 30 and timeout != maxTimeout:
            time.sleep(sleepTime)
            timeout = timeout + sleepTime
            status = ls.GET_PHASELOCK_GET_STATUS_TUNING_READY()[0]
    
        # Unlock the LS.
        ls.SET_PHASELOCK_COMMAND_TUNING_UNLOCK()
    
        # Wait until the RF_READY flag is set.
        status = ls.GET_PHASELOCK_GET_STATUS_RF_INPUT_READY()[0]
        timeout = 0
        sleepTime = 0.5
        self.__logger.logInfo("Waiting for the RF input to become ready...")
        while status != True and timeout != 30 and timeout != maxTimeout:
            time.sleep(sleepTime)
            timeout = timeout + sleepTime
            status = ls.GET_PHASELOCK_GET_STATUS_RF_INPUT_READY()[0]
    
        # Set the CVR frequency.  The frequency is in GHz!
        # nu = (nu_target + 125.0e6 Hz) / harmonic
        cvr.setFrequency((targetFrequency + 125.0e6) / harmonic * 1e-9)
    
        # Finish the tuning.
        ls.SET_PHASELOCK_COMMAND_TUNING_FINALIZE()
    
        status = ls.GET_SYSTEM_GET_STATUS_SYSTEM_STATE()[0]
        self.__logger.logInfo("Waiting for system to become Operational "
            "(locked) or Standby (error)...")
        while status != 2 and status != 4 and timeout != maxTimeout:
            time.sleep(sleepTime)
            timeout = timeout + sleepTime
            status = ls.GET_SYSTEM_GET_STATUS_SYSTEM_STATE()[0]
    
        if status != 4:
            self.__logger.logInfo("The LS could not lock!")
            error = ls.GET_SYSTEM_GET_ERROR()[0]
            while error != 0:
                self.__logger.logInfo("Error:  %d" % error)
                error = ls.GET_SYSTEM_GET_ERROR()[0]
        else:
            self.__logger.logInfo("The LS is locked.")



def setLSFrequencyCpp(photonicReference = None, componentName = None,
    stickyFlag = False, frequency = 0.0):
    '''
    LS locking as it is done in the C++ code.

    Parameters:
        photonicReference: Either a string like PhotonicReference3 or a number
        for a photonic reference.

        componentName: Can be specified instead of the photonicReference
        parameter.

        stickyFlag: load component sticky?  Default is no.

        frequency: target frequency for the LS.

    You can specify which LS you would like either by number
    (1-6) or by PhotonicReference (i.e. PhotonicReference2)
    EXAMPLE:
    # To get the LS associated with photonic reference 3
    ls = setLSFrequencyCpp(photonicReference = 3, frequency = 1.2e9)
    or 
    ls = setLSFrequencyCpp(photonicReference = "PhotonicReference3", frequency = 1.2e9)
    '''
    if componentName is None:
        if photonicReference is None:
            raise IllegalParameterErrorEx('You must specify either a '
                                          + ' photonic reference');
        if isinstance(photonicReference, int):
            photonicReference= 'PhotonicReference%d' % photonicReference

        componentName = "CONTROL/CentralLO/%s/LS" % photonicReference

    name = componentName.split('/')[2]
    _prNo = int(name[len(name)-1])
    _componentName = componentName

    if name == None:
        print "Component name cannot be empty!"
        return -1
    if frequency <= 0.0:
        print "Frequency must be > 0!"
        return -1.

    from Acspy.Clients.SimpleClient import PySimpleClient
    import Acspy.Common.ErrorTrace

    ls = None
    mySimpleClient = PySimpleClient.getInstance()
    try:
        if stickyFlag == True:
            ls = mySimpleClient.getComponent(_componentName)
        else:
            ls = mySimpleClient.getComponentNonSticky(_componentName)
    except Exception, ex:
        printEx(ex)
        return -1

    def releaseComponent(stickyFlag, mySimpleClient, _componentName):
        if self.stickyFlag == True:
            try:
                mySimpleClient.releaseComponent(_componentName)
            except Exception, ex:
                printEx(ex)

    import math
    import time

    maxTimeout = 30.0
    sleepTime = 1.0
    status = None

    #Step 0: check if we are already locked in the requested frequency.
    # FreqTolerance is set to 0.0 in C++.
    FreqTolerance = 0.0

    freqDiff = self.getFrequency() - frequency
    if (math.abs(freqDiff) - FreqTolerance) <= 0:
        #The requested frequency is the same as the current freq or inside
        #of the tolerance but we need to make sure that the system is in
        # locked state.
        try:
            status = ls.GET_PHASELOCK_GET_STATUS_LOCKED()[0]
            print "GET_PHASELOCK_GET_STATUS_LOCKED = %d" % state
            if status == 1:
                releaseComponent(stickyFlag, mySimpleClient, _componentName)
                print "LS is already locked.  Doing nothing."
                return 0
        except Exception, ex:
            releaseComponent(stickyFlag, mySimpleClient, _componentName)
            printEx(ex)
            return -1

    # Step 1. Send LS device to Standby state 
    print "Send SET_SYSTEM_STANDBY_MODE_REQUEST()"
    try:
        ls.SET_SYSTEM_STANDBY_MODE_REQUEST()
    except Exception, ex:
        releaseComponent(stickyFlag, mySimpleClient, _componentName)
        printEx(ex)
        return -1


    status = None
    timeout = 0
    print "Waiting for STANDBY..."
    while status != 2 and timeout != 10 and timeout != maxTimeout:
        time.sleep(sleepTime)
        timeout = timeout + sleepTime
        try:
            status = ls.GET_SYSTEM_GET_STATUS_SYSTEM_STATE()[0]
        except Exception, ex:
            releaseComponent(stickyFlag, mySimpleClient, _componentName)
            printEx(ex)
            return -1

    if status != 2:
        print "LS not in STANDBY!"
        releaseComponent(stickyFlag, mySimpleClient, _componentName)
        return -1
    else:
        print "LS is now in STANDBY."


    # calculate the Slave Laser Frequency basing the required LS frequency
    # double refLaserFreq = 192.643913E12;
    refLaserFreq = ls.GET_PHASELOCK_REF_LASER_FREQUENCY()[0]
    slaveLaserFreq = refLaserFreq - frequency
    # Step 2: Send PHASELOCK_COMMAND_TUNING_INIT command
    print "Sending PHASELOCK_COMMAND_TUNING_INIT(%f[Hz])" % slaveLaserFreq
    try:
        ls.SET_PHASELOCK_COMMAND_TUNING_INIT(slaveLaserFreq)
    except Exception, ex:
        releaseComponent(stickyFlag, mySimpleClient, _componentName)
        printEx(ex)
        return -1
    print "Slave laser tuned to %f" % slaveLaserFreq


    # Step 3: Wait for "Tuning Ready" in PHASELOCK_GET_STATUS
    status = None
    timeout = 0
    print "Waiting for 'Tuning ready' flag"
    while status != True and timeout != 30 and timeout != maxTimeout:
        time.sleep(sleepTime)
        timeout = timeout + sleepTime
        try:
            status = ls.GET_PHASELOCK_GET_STATUS_TUNING_READY()[0]
        except Exception, ex:
            releaseComponent(stickyFlag, mySimpleClient, _componentName)
            printEx(ex)
            return -1

    if status != True:
        print "LS not in TUNING READY state!"
        releaseComponent(stickyFlag, mySimpleClient, _componentName)
        return -1
    else:
        print "LS is now in TUNING READY STATE."


    # Step 4: Send PHASELOCK_COMMAND_TUNING_UNLOCK command
    print "Send TUNING_UNLOCK Command"
    try:
        ls.SET_PHASELOCK_COMMAND_TUNING_UNLOCK()
    except Exception, ex:
        releaseComponent(stickyFlag, mySimpleClient, _componentName)
        printEx(ex)
        return -1


    # Step 5 Wait for "RF input Ready" in PHASELOCK_GET_STATUS
    status = None
    timeout = 0
    print "Waiting for RF INPUT READY flag"
    while status != True and timeout != 30 and timeout != maxTimeout:
        time.sleep(sleepTime)
        timeout = timeout + sleepTime
        try:
            status = ls.GET_PHASELOCK_GET_STATUS_RF_INPUT_READY()[0]
        except Exception, ex:
            releaseComponent(stickyFlag, mySimpleClient, _componentName)
            printEx(ex)
            return -1

    if status != True:
        print "LS not in RF INPUT READY STATE state!"
        releaseComponent(stickyFlag, mySimpleClient, _componentName)
        return -1
    else:
        print "LS is now in RF INPUT READY STATE."


    # Step 6: Send PHASELOCK_COMMAND_TUNING_FINALIZE command
    print "Sending PHASELOCK_COMMAND_TUNING_FINALIZE command"
    try:
        ls.SET_PHASELOCK_COMMAND_TUNING_FINALIZE()
    except Exception, ex:
        releaseComponent(stickyFlag, mySimpleClient, _componentName)
        printEx(ex)
        return -1


    status = None
    timeout = 0
    print "Waiting for UNLOCKED state..."
    while status == 3 and timeout != 60 and timeout != maxTimeout:
        time.sleep(sleepTime)
        timeout = timeout + sleepTime
        try:
            status = ls.GET_SYSTEM_GET_STATUS_SYSTEM_STATE()[0]
        except Exception, ex:
            releaseComponent(stickyFlag, mySimpleClient, _componentName)
            printEx(ex)
            return -1

    if status == 3:
        print "Timeout! LS not in LOCKED but still in PHASE LOCKING state!"
        releaseComponent(stickyFlag, mySimpleClient, _componentName)
        return -1
    elif ls.GET_PHASELOCK_GET_STATUS_LOCKED()[0] == False and \
        ls.GET_PHASELOCK_GET_STATUS_LOCKED()[0] == False:
        print "LS is NOT LOCKED!"
        releaseComponent(stickyFlag, mySimpleClient, _componentName)
        return -1
    else:
        print "LS is LOCKED."

    # Step 7: Check LS in Operational state

    status = None
    timeout = 0
    print "Waiting for \"Operational\" state..."
    while status != 4 and timeout != 3 and timeout != maxTimeout:
        time.sleep(sleepTime)
        timeout = timeout + sleepTime
        try:
            status = ls.GET_SYSTEM_GET_STATUS_SYSTEM_STATE()[0]
        except Exception, ex:
            releaseComponent(stickyFlag, mySimpleClient, _componentName)
            printEx(ex)
            return -1

    if status != 4:
        releaseComponent(stickyFlag, mySimpleClient, _componentName)
        print "Timeout!  LS is not in OPERATIONAL state."
        return -1
    else:
        print "LS successfully locked at %f[Ghz]." % frequency

    releaseComponent(stickyFlag, mySimpleClient, _componentName)


def setLSFrequencyAmbManager(photonicReference = None, frequency = 0.0):
    '''
    LS locking as it is described in Appendix A od the LS ICD using the
    AmbManager.

    Parameters:
        photonicReference: Either a string like PhotonicReference3 or a number
        for a photonic reference.

        frequency: target frequency for the LS.
    '''
    def releaseMgr(mgrState, aosTiming):
        if self.mgrState == False:
            try:
                aosTiming.stopAmbManager()
            except Exception, ex:
                print "Could not stop the AmbManager."
                printEx(ex)

    if frequency <= 0.0:
        print "Frequency must be > 0!"
        return -1.

    if photonicReference is None:
        raise IllegalParameterErrorEx('You must specify a  photonic reference')
    if isinstance(photonicReference, int):
        photonicReference= 'PhotonicReference%d' % photonicReference

    componentName = "CONTROL/CentralLO/%s/LS" % photonicReference

    name = componentName.split('/')[2]
    _prNo = int(name[len(name)-1])
    address = _prNo + 55

    from Acspy.Clients.SimpleClient import PySimpleClient
    import Acspy.Common.ErrorTrace

    mgr = None
    mySimpleClient = PySimpleClient.getInstance()
    try:
        aosTiming = mySimpleClient.getComponentNonSticky("CONTROL/AOSTiming")
        mgrState = aosTiming.isAmbManagerRunning()
        if mgrState == False:
            aosTiming.startAmbManager()
        mgr = mySimpleClient.getComponentNonSticky("CONTROL/AOSTiming/AmbManager")
    except Exception, ex:
        printEx(ex)
        return -1

    import math
    import time

    maxTimeout = 30.0
    sleepTime = 1.0
    status = None

    #Step 0: check if we are already locked in the requested frequency.
    # FreqTolerance is set to 0.0 in C++.
    FreqTolerance = 0.0

    freqDiff = self.getFrequency() - frequency
    if (math.abs(freqDiff) - FreqTolerance) <= 0:
        #The requested frequency is the same as the current freq or inside
        #of the tolerance but we need to make sure that the system is in
        # locked state.
        try:
            # status = ls.GET_PHASELOCK_GET_STATUS_LOCKED()[0]
            status = mgr.monitor(0, address, 0x03020)[0]
            status = status[0] & 1 
            print "GET_PHASELOCK_GET_STATUS_LOCKED = %d" % status
            if status == 1:
                releaseMgr(mgrState, aosTiming)
                print "LS is already locked.  Doing nothing."
                return 0
        except Exception, ex:
            releaseMgr(mgrState, aosTiming)
            printEx(ex)
            return -1

    # Step 1. Send LS device to Standby state 
    print "Send SET_SYSTEM_STANDBY_MODE_REQUEST()"
    try:
        # ls.SET_SYSTEM_STANDBY_MODE_REQUEST()
        mgr.control(0, address, 0x01001, 0)
    except Exception, ex:
        releaseMgr(mgrState, aosTiming)
        printEx(ex)
        return -1


    status = None
    timeout = 0
    print "Waiting for STANDBY..."
    while status != 2 and timeout != 10 and timeout != maxTimeout:
        time.sleep(sleepTime)
        timeout = timeout + sleepTime
        try:
            # status = ls.GET_SYSTEM_GET_STATUS_SYSTEM_STATE()[0]
            status = mgr.monitor(0, address, 0x01100)[0]
            status = (status[0] >> 3) & 7
        except Exception, ex:
            releaseMgr(mgrState, aosTiming)
            printEx(ex)
            return -1

    if status != 2:
        print "LS not in STANDBY!"
        return -1
    else:
        print "LS is now in STANDBY."


    # calculate the Slave Laser Frequency basing the required LS frequency
    # double refLaserFreq = 192.643913E12;
    try:
        refLaserFreq = ls.GET_PHASELOCK_REF_LASER_FREQUENCY()[0]
    except Exception, ex:
        releaseMgr(mgrState, aosTiming)
        printEx(ex)
        return -1
    slaveLaserFreq = refLaserFreq - frequency
    # Step 2: Send PHASELOCK_COMMAND_TUNING_INIT command
    print "Sending PHASELOCK_COMMAND_TUNING_INIT(%f[Hz])" % slaveLaserFreq
    try:
        # ls.SET_PHASELOCK_COMMAND_TUNING_INIT(slaveLaserFreq)
        data = struct.pack("!I", int(slaveLaserFreq / 1000000.0 + 0.5))
        mgr.control(0, address, 0x03000, data)
    except Exception, ex:
        releaseMgr(mgrState, aosTiming)
        printEx(ex)
        return -1
    print "Slave laser tuned to %f" % slaveLaserFreq


    # Step 3: Wait for "Tuning Ready" in PHASELOCK_GET_STATUS
    status = None
    timeout = 0
    print "Waiting for 'Tuning ready' flag"
    while status != True and timeout != 30 and timeout != maxTimeout:
        time.sleep(sleepTime)
        timeout = timeout + sleepTime
        try:
            # status = ls.GET_PHASELOCK_GET_STATUS_TUNING_READY()[0]
            status = mgr.monitor(0, address, 0x03020)[0]
            status = (status[0] >> 6) & 1
        except Exception, ex:
            releaseMgr(mgrState, aosTiming)
            printEx(ex)
            return -1

    if status != 1:
        print "LS not in TUNING READY state!"
        releaseMgr(mgrState, aosTiming)
        return -1
    else:
        print "LS is now in TUNING READY STATE."


    # Step 4: Send PHASELOCK_COMMAND_TUNING_UNLOCK command
    print "Send TUNING_UNLOCK Command"
    try:
        # ls.SET_PHASELOCK_COMMAND_TUNING_UNLOCK()
        mgr.control(0, address, 0x03005, 0)
    except Exception, ex:
        releaseMgr(mgrState, aosTiming)
        printEx(ex)
        return -1


    # Step 5 Wait for "RF input Ready" in PHASELOCK_GET_STATUS
    status = None
    timeout = 0
    print "Waiting for RF INPUT READY flag"
    while status != 1 and timeout != 30 and timeout != maxTimeout:
        time.sleep(sleepTime)
        timeout = timeout + sleepTime
        try:
            # status = ls.GET_PHASELOCK_GET_STATUS_RF_INPUT_READY()[0]
            status = mgr.monitor(0, address, 0x03020)[0]
            status = (status[0] >> 7) & 1
        except Exception, ex:
            releaseMgr(mgrState, aosTiming)
            printEx(ex)
            return -1

    if status != 1:
        print "LS not in RF INPUT READY STATE state!"
        return -1
    else:
        print "LS is now in RF INPUT READY STATE."


    # Step 6: Send PHASELOCK_COMMAND_TUNING_FINALIZE command
    print "Sending PHASELOCK_COMMAND_TUNING_FINALIZE command"
    try:
        # ls.SET_PHASELOCK_COMMAND_TUNING_FINALIZE()
        mgr.control(0, address, 0x03001, 0)
    except Exception, ex:
        releaseMgr(mgrState, aosTiming)
        printEx(ex)
        return -1

    status = None
    timeout = 0
    print "Waiting for UNLOCKED state..."
    while status == 3 and timeout != 60 and timeout != maxTimeout:
        time.sleep(sleepTime)
        timeout = timeout + sleepTime
        try:
            # status = ls.GET_SYSTEM_GET_STATUS_SYSTEM_STATE()[0]
            status = mgr.monitor(0, address, 0x01100)[0]
            status = (status[0] >> 3) & 7
        except Exception, ex:
            releaseMgr(mgrState, aosTiming)
            printEx(ex)
            return -1

    try:
        if status == 3:
            releaseMgr(mgrState, aosTiming)
            print "Timeout! LS not in LOCKED but still in PHASE LOCKING state!"
            return -1
        elif ls.GET_PHASELOCK_GET_STATUS_LOCKED()[0] == False and \
            ls.GET_PHASELOCK_GET_STATUS_LOCKED()[0] == False:
            releaseMgr(mgrState, aosTiming)
            print "LS is NOT LOCKED!"
            printEx(ex)
            return -1
        else:
            print "LS is LOCKED."
    except Exception, ex:
        releaseMgr(mgrState, aosTiming)
        printEx(ex)


    # Step 7: Check LS in Operational state
    status = None
    timeout = 0
    print "Waiting for \"Operational\" state..."
    while status != 4 and timeout != 3 and timeout != maxTimeout:
        time.sleep(sleepTime)
        timeout = timeout + sleepTime
        try:
            # status = ls.GET_SYSTEM_GET_STATUS_SYSTEM_STATE()[0]
            status = mgr.monitor(0, address, 0x01100)[0]
            status = (status[0] >> 3) & 7
        except Exception, ex:
            releaseMgr(mgrState, aosTiming)
            printEx(ex)
            return -1
    if status != 4:
        releaseMgr(mgrState, aosTiming)
        print "Timeout!  LS is not in OPERATIONAL state."
        return -1
    else:
        print "LS successfully locked at %f[Ghz]." % frequency

    releaseMgr(mgrState, aosTiming)
