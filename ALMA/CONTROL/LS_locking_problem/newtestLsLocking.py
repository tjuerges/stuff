#! /usr/bin/env python
#
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2009
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# $Id$
#


import math
import time
import struct

from Acspy.Clients.SimpleClient import PySimpleClient
import Acspy.Common.ErrorTrace

import CCL.AOSTiming
import CCL.AmbManager
import CCL.LS


def printEx(ex = None):
    if ex == None:
        print "Please provide an ACS exception!"
    else:
        try:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace).log(
                Acspy.Common.Log.getLogger());
        except:
            print ex


def setLSFrequencyCpp(photonicReference = None, componentName = None,
    stickyFlag = False, frequency = 0.0, Jason = False):
    '''
    LS locking as it is done in the C++ code.  The CORBA interface of the
    C++ component is used for the communication with the hardware device.

    Parameters:
        photonicReference: Either a string like PhotonicReference3 or a number
        for a photonic reference.

        componentName: Can be specified instead of the photonicReference
        parameter.

        stickyFlag: load component sticky?  Default is no.

        frequency: target frequency for the LS.

        Jason: Jason Castro suggested to add a 0.1s sleep between some of the
               locking steps.  Set this variable to True to activate the
               sleeps.

    You can specify which LS you would like either by number
    (1-6) or by PhotonicReference (i.e. PhotonicReference2)
    EXAMPLE:
    # To get the LS associated with photonic reference 3
    ls = setLSFrequencyCpp(photonicReference = 3, frequency = 1.2e9)
    or 
    ls = setLSFrequencyCpp(photonicReference = "PhotonicReference3", frequency = 1.2e9)
    '''

    def releaseComponent(stickyFlag, mySimpleClient, _componentName):
        '''
        Relases the LS component.
        '''
        if stickyFlag == True:
            try:
                mySimpleClient.releaseComponent(_componentName)
            except Exception, ex:
                printEx(ex)


    if componentName is None:
        if photonicReference is None:
            raise IllegalParameterErrorEx('You must specify either a '
                                          + ' photonic reference');
        if isinstance(photonicReference, int):
            photonicReference= 'PhotonicReference%d' % photonicReference

        componentName = "CONTROL/CentralLO/%s/LS" % photonicReference

    # Awkward name shuffling, I know.  I copied this from the CCL code.
    name = componentName.split('/')[2]
    _prNo = int(name[len(name)-1])
    _componentName = componentName

    if name == None:
        print "Component name cannot be empty!"
        return -1
    if frequency <= 0.0:
        print "Frequency must be > 0!"
        return -1.

    ls = None
    mySimpleClient = PySimpleClient.getInstance()

    try:
        if stickyFlag == True:
            ls = mySimpleClient.getComponent(_componentName)
        else:
            ls = mySimpleClient.getComponentNonSticky(_componentName)
    except Exception, ex:
        printEx(ex)
        return -1

    # Variables needed in the steps below.
    maxTimeout = 30.0
    sleepTime = 1.0
    status = None

    # Step 0: check if we are already locked in the requested frequency.
    # FreqTolerance is set to 0.0 in C++.
    # Removed because ls.getFrequency(), which is a call to the C++ component
    # returns the wrong frequency.


    # Step 1. If the LS is NOT in operational state, send LS device to Standby
    # state
    status = ls.GET_SYSTEM_GET_STATUS_SYSTEM_STATE()[0]
    if status != 4:
        # LS is not in Operational state, set it to Standby state.
        print "Send SET_SYSTEM_STANDBY_MODE_REQUEST()"
        try:
            ls.SET_SYSTEM_STANDBY_MODE_REQUEST()
        except Exception, ex:
            releaseComponent(stickyFlag, mySimpleClient, _componentName)
            printEx(ex)
            return -1

        status = None
        timeout = 0
        print "Waiting for STANDBY..."
        while status != 2 and timeout != 10 and timeout != maxTimeout:
            time.sleep(sleepTime)
            timeout = timeout + sleepTime
            try:
                status = ls.GET_SYSTEM_GET_STATUS_SYSTEM_STATE()[0]
                print "GET_PHASELOCK_GET_STATUS_LOCKED()[0] = ", ls.GET_PHASELOCK_GET_STATUS_LOCKED()[0]
                print "GET_SYSTEM_GET_STATUS_SYSTEM_STATE()[0] = ", status
            except Exception, ex:
                releaseComponent(stickyFlag, mySimpleClient, _componentName)
                printEx(ex)
                return -1
        if status != 2:
            print "LS not in STANDBY!"
            releaseComponent(stickyFlag, mySimpleClient, _componentName)
            return -1
        else:
            print "LS is now in STANDBY."
    else:
        print "LS is already in Operational state, I will not set it to Standby state."

    # Suggested by Jason Castro
    if Jason == True:
        time.sleep(0.1)

    # calculate the Slave Laser Frequency basing the required LS frequency
    # double refLaserFreq = 192.643913E12;
    refLaserFreq = ls.GET_PHASELOCK_REF_LASER_FREQUENCY()[0]
    slaveLaserFreq = refLaserFreq - frequency


    # Step 2: Send PHASELOCK_COMMAND_TUNING_INIT command
    print "Sending PHASELOCK_COMMAND_TUNING_INIT(%f[Hz])" % slaveLaserFreq
    try:
        ls.SET_PHASELOCK_COMMAND_TUNING_INIT(slaveLaserFreq)
    except Exception, ex:
        releaseComponent(stickyFlag, mySimpleClient, _componentName)
        printEx(ex)
        return -1
    print "Slave laser tuned to %f" % slaveLaserFreq

    # Suggested by Jason Castro
    if Jason == True:
        time.sleep(0.1)


    # Step 3: Wait for "Tuning Ready" in PHASELOCK_GET_STATUS
    status = None
    timeout = 0
    print "Waiting for 'TUNING READY' flag"
    while status != True and timeout != 30 and timeout != maxTimeout:
        time.sleep(sleepTime)
        timeout = timeout + sleepTime
        try:
            status = ls.GET_PHASELOCK_GET_STATUS_TUNING_READY()[0]
            print "GET_PHASELOCK_GET_STATUS_TUNING_READY()[0] = ", status
            print "GET_PHASELOCK_GET_STATUS_LOCKED()[0] = ", ls.GET_PHASELOCK_GET_STATUS_LOCKED()[0]
            print "GET_SYSTEM_GET_STATUS_SYSTEM_STATE()[0] = ", ls.GET_SYSTEM_GET_STATUS_SYSTEM_STATE()[0]
        except Exception, ex:
            releaseComponent(stickyFlag, mySimpleClient, _componentName)
            printEx(ex)
            return -1

    if status != True:
        print "LS not in TUNING READY state!"
        releaseComponent(stickyFlag, mySimpleClient, _componentName)
        return -1
    else:
        print "LS is now in TUNING READY state."


    # Step 4: Send PHASELOCK_COMMAND_TUNING_UNLOCK command
    print "Send TUNING_UNLOCK Command"
    try:
        ls.SET_PHASELOCK_COMMAND_TUNING_UNLOCK()
    except Exception, ex:
        releaseComponent(stickyFlag, mySimpleClient, _componentName)
        printEx(ex)
        return -1


    # Step 5 Wait for "RF input Ready" in PHASELOCK_GET_STATUS
    status = None
    timeout = 0
    print "Waiting for RF INPUT READY flag"
    while status != True and timeout != 30 and timeout != maxTimeout:
        time.sleep(sleepTime)
        timeout = timeout + sleepTime
        try:
            status = ls.GET_PHASELOCK_GET_STATUS_RF_INPUT_READY()[0]
            print "GET_PHASELOCK_GET_STATUS_RF_INPUT_READY()[0] = ", status
            print "GET_PHASELOCK_GET_STATUS_LOCKED()[0] = ", ls.GET_PHASELOCK_GET_STATUS_LOCKED()[0]
            print "GET_SYSTEM_GET_STATUS_SYSTEM_STATE()[0] = ", ls.GET_SYSTEM_GET_STATUS_SYSTEM_STATE()[0]
        except Exception, ex:
            releaseComponent(stickyFlag, mySimpleClient, _componentName)
            printEx(ex)
            return -1

    if status != True:
        print "LS not in RF INPUT READY state!"
        releaseComponent(stickyFlag, mySimpleClient, _componentName)
        return -1
    else:
        print "LS is now in RF INPUT READY state."


    # Step 6: Send PHASELOCK_COMMAND_TUNING_FINALIZE command
    print "Sending PHASELOCK_COMMAND_TUNING_FINALIZE command"
    try:
        ls.SET_PHASELOCK_COMMAND_TUNING_FINALIZE()
    except Exception, ex:
        releaseComponent(stickyFlag, mySimpleClient, _componentName)
        printEx(ex)
        return -1

    status = 3
    plStatus = False
    timeout = 0
    print "Waiting for UNLOCKED state..."
    while status == 3 and timeout != 60 and timeout != maxTimeout:
        time.sleep(sleepTime)
        timeout = timeout + sleepTime
        try:
            status = ls.GET_SYSTEM_GET_STATUS_SYSTEM_STATE()[0]
            print "PHASELOCK_GET_STATUS_LOCKED()[0] = ", ls.GET_PHASELOCK_GET_STATUS_LOCKED()[0]
            print "GET_SYSTEM_GET_STATUS_SYSTEM_STATE()[0] = ", status
        except Exception, ex:
            releaseComponent(stickyFlag, mySimpleClient, _componentName)
            print "Exception!"
            printEx(ex)
            return -1

    if status == 3 and plStatus == False:
        print "Timeout! LS not in LOCKED but still in PHASE LOCKING state!"
        releaseComponent(stickyFlag, mySimpleClient, _componentName)
        return -1
    elif ls.GET_PHASELOCK_GET_STATUS_LOCKED()[0] == False and \
        ls.GET_PHASELOCK_GET_STATUS_LOCKED()[0] == False:
        print "PHASELOCK_GET_STATUS_LOCKED()[0] = ", ls.GET_PHASELOCK_GET_STATUS_LOCKED()[0]
        print "GET_SYSTEM_GET_STATUS_SYSTEM_STATE()[0] = ", ls.GET_SYSTEM_GET_STATUS_SYSTEM_STATE()[0]
        print "LS is NOT LOCKED!"
        releaseComponent(stickyFlag, mySimpleClient, _componentName)
        return -1
    else:
        print "LS is LOCKED."


    # Step 7: Check LS in Operational state
    status = None
    timeout = 0
    print "Waiting for Operational state..."
    while status != 4 and timeout != 3 and timeout != maxTimeout:
        time.sleep(sleepTime)
        timeout = timeout + sleepTime
        try:
            status = ls.GET_SYSTEM_GET_STATUS_SYSTEM_STATE()[0]
            print "PHASELOCK_GET_STATUS_LOCKED()[0] = ", ls.GET_PHASELOCK_GET_STATUS_LOCKED()[0]
            print "GET_SYSTEM_GET_STATUS_SYSTEM_STATE()[0] = ", status
        except Exception, ex:
            releaseComponent(stickyFlag, mySimpleClient, _componentName)
            printEx(ex)
            return -1

    if status != 4:
        releaseComponent(stickyFlag, mySimpleClient, _componentName)
        print "Timeout!  LS is not in OPERATIONAL state."
        return -1
    else:
        print "LS successfully locked at %f[Ghz]." % frequency

    releaseComponent(stickyFlag, mySimpleClient, _componentName)


def setLSFrequencyAmbManager(
    photonicReference = None, frequency = 0.0, Jason = False):
    '''
    LS locking as it is described in Appendix A of the LS ICD using the
    AmbManager.

    Parameters:
        photonicReference: Either a string like PhotonicReference3 or a number
        for a photonic reference.

        frequency: target frequency for the LS.

        Jason: Jason Castro suggested to add a 0.1s sleep between some of the
               locking steps.  Set this variable to True to activate the
               sleeps.
    '''
    def releaseMgr(mgrState, aosTiming):
        '''
        Releases the AmbManager.
        '''
        if mgrState == False:
            try:
                aosTiming.stopAmbManager()
            except Exception, ex:
                print "Could not stop the AmbManager."
                printEx(ex)

    def getPhaselockStatus(
        address = None, byte = None, shift = None, mask = None):
        '''
        Read the LS phaselock status via AmbManager.
        '''
        try:
            status = mgr.monitor(0, address, 0x03020)[0]
        except Exception, ex:
            releaseMgr(mgrState, aosTiming)
            printEx(ex)
            raise ex
        status = struct.unpack("!2B", status)
        print "Phaselock status 0x03020 = ", status
        status = (status[byte] >> shift) & mask 
        return status

    def getSystemStatus(
        address = None, byte = None, shift = None, mask = None):
        '''
        Read the LS system status via AmbManager.
        '''
        try:
            status = mgr.monitor(0, address, 0x01100)[0]
        except Exception, ex:
            releaseMgr(mgrState, aosTiming)
            printEx(ex)
            raise ex
        status = struct.unpack("!4B", status)
        print "System status 0x01100 = ", status
        status = (status[byte] >> shift) & mask
        return status


    if frequency <= 0.0:
        print "Frequency must be > 0!"
        return -1.

    if photonicReference is None:
        raise IllegalParameterErrorEx('You must specify a  photonic reference')

    if isinstance(photonicReference, int):
        photonicReference= 'PhotonicReference%d' % photonicReference

    componentName = "CONTROL/CentralLO/%s/LS" % photonicReference

    name = componentName.split('/')[2]
    _prNo = int(name[len(name)-1])
    address = _prNo + 55

    mgr = None
    try:
        aosTiming = CCL.AOSTiming.AOSTiming()
        mgrState = aosTiming.isAmbManagerRunning()
        if mgrState == False:
            aosTiming.startAmbManager()
        mgr = CCL.AmbManager.AmbManager("AOSTiming")
    except Exception, ex:
        printEx(ex)
        return -1

    # Variables which are used in the steps below.
    maxTimeout = 30.0
    sleepTime = 1.0
    status = None

    # Step 0: check if we are already locked in the requested frequency.
    # FreqTolerance is set to 0.0 in C++.
    # Removed because ls.getFrequency(), which is a call to the C++ component
    # returns the wrong frequency.


    # Step 1. If the LS is NOT in operational state, send LS device to Standby
    # state
    status = getSystemStatus(
                address = address, byte = 3, shift = 3, mask = 7)
    if status != 4:
        print "Send SET_SYSTEM_STANDBY_MODE_REQUEST()"
        try:
            # ls.SET_SYSTEM_STANDBY_MODE_REQUEST()
            mgr.command(0, address, 0x01001, struct.pack("!B", 1))
        except Exception, ex:
            releaseMgr(mgrState, aosTiming)
            printEx(ex)
            return -1

        status = None
        timeout = 0
        print "Waiting for STANDBY..."
        while status != 2 and timeout != 10 and timeout != maxTimeout:
            time.sleep(sleepTime)
            timeout = timeout + sleepTime
            try:
                # status = ls.GET_SYSTEM_GET_STATUS_SYSTEM_STATE()[0]
                status = getSystemStatus(
                    address = address, byte = 3, shift = 3, mask = 7)
            except Exception, ex:
                releaseMgr(mgrState, aosTiming)
                printEx(ex)
                return -1

        if status != 2:
            print "LS not in STANDBY!"
            return -1
        else:
            print "LS is now in STANDBY."
    else:
        print "LS is already in Operational state, will not set " \
            "it to Standby state."

    # Suggested by Jason Castro
    if Jason == True:
        time.sleep(0.1)

    # calculate the Slave Laser Frequency basing the required LS frequency
    # double refLaserFreq = 192.643913E12;
    try:
        refLaserFreq = mgr.monitor(0, address, 0x03032)[0]
        refLaserFreq = struct.unpack("!I", refLaserFreq)[0]
        refLaserFreq *= 1000000.0
        print refLaserFreq
    except Exception, ex:
        releaseMgr(mgrState, aosTiming)
        printEx(ex)
        return -1
    slaveLaserFreq = refLaserFreq - frequency


    # Step 2: Send PHASELOCK_COMMAND_TUNING_INIT command
    print "Sending PHASELOCK_COMMAND_TUNING_INIT(%f[Hz])" % slaveLaserFreq
    try:
        # ls.SET_PHASELOCK_COMMAND_TUNING_INIT(slaveLaserFreq)
        data = struct.pack("!I", int(slaveLaserFreq / 1000000.0 + 0.5))
        mgr.command(0, address, 0x03000, data)
    except Exception, ex:
        releaseMgr(mgrState, aosTiming)
        printEx(ex)
        return -1
    print "Slave laser tuned to %f" % slaveLaserFreq

    # Suggested by Jason Castro
    if Jason == True:
        time.sleep(0.1)


    # Step 3: Wait for "Tuning Ready" in PHASELOCK_GET_STATUS
    status = None
    timeout = 0
    print "Waiting for 'Tuning ready' flag"
    while status != True and timeout != 30 and timeout != maxTimeout:
        time.sleep(sleepTime)
        timeout = timeout + sleepTime
        try:
            # status = ls.GET_PHASELOCK_GET_STATUS_TUNING_READY()[0]
            status = getPhaselockStatus(
                address = address, byte = 1, shift = 6, mask = 1)
        except Exception, ex:
            releaseMgr(mgrState, aosTiming)
            printEx(ex)
            return -1

    if status != 1:
        print "LS not in TUNING READY state!"
        releaseMgr(mgrState, aosTiming)
        return -1
    else:
        print "LS is now in TUNING READY STATE."


    # Step 4: Send PHASELOCK_COMMAND_TUNING_UNLOCK command
    print "Send TUNING_UNLOCK Command"
    try:
        # ls.SET_PHASELOCK_COMMAND_TUNING_UNLOCK()
        mgr.command(0, address, 0x03005, struct.pack("!B", 0))
    except Exception, ex:
        releaseMgr(mgrState, aosTiming)
        printEx(ex)
        return -1


    # Step 5 Wait for "RF input Ready" in PHASELOCK_GET_STATUS
    status = None
    timeout = 0
    print "Waiting for RF INPUT READY flag"
    while status != 1 and timeout != 30 and timeout != maxTimeout:
        time.sleep(sleepTime)
        timeout = timeout + sleepTime
        try:
            # status = ls.GET_PHASELOCK_GET_STATUS_RF_INPUT_READY()[0]
            status = getPhaselockStatus(
                address = address, byte = 1, shift = 7, mask = 7)
        except Exception, ex:
            releaseMgr(mgrState, aosTiming)
            printEx(ex)
            return -1

    if status != 1:
        print "LS not in RF INPUT READY STATE state!"
        return -1
    else:
        print "LS is now in RF INPUT READY STATE."

    # Suggested by Jason Castro
    if Jason == True:
        time.sleep(0.012)


    # Step 6: Send PHASELOCK_COMMAND_TUNING_FINALIZE command
    print "Sending PHASELOCK_COMMAND_TUNING_FINALIZE command"
    try:
        # ls.SET_PHASELOCK_COMMAND_TUNING_FINALIZE()
        mgr.command(0, address, 0x03001, struct.pack("!B", 0))
    except Exception, ex:
        releaseMgr(mgrState, aosTiming)
        printEx(ex)
        return -1

    status = 3
    timeout = 0
    print "Waiting for UNLOCKED state..."
    while status == 3 and timeout != 60 and timeout != maxTimeout:
        time.sleep(sleepTime)
        timeout = timeout + sleepTime
        try:
            # status = ls.GET_SYSTEM_GET_STATUS_SYSTEM_STATE()[0]
            status = getSystemStatus(
                address = address, byte = 3, shift = 3, mask = 7)
        except Exception, ex:
            releaseMgr(mgrState, aosTiming)
            print "Exception!"
            printEx(ex)
            return -1

    try:
        if status == 3:
            releaseMgr(mgrState, aosTiming)
            print "Timeout! LS not in LOCKED but still in PHASE LOCKING state!"
            return -1
        else:
            #ls.GET_PHASELOCK_GET_STATUS_LOCKED()[0] == False and \
            #ls.GET_PHASELOCK_GET_STATUS_LOCKED()[0] == False:
            status = getPhaselockStatus(
                address = address, byte = 1, shift = 0, mask = 1)
            if status != 1:
                releaseMgr(mgrState, aosTiming)
                print "LS is NOT LOCKED!"
                return -1
            else:
                print "LS is LOCKED."
    except Exception, ex:
        releaseMgr(mgrState, aosTiming)
        printEx(ex)


    # Step 7: Check LS in Operational state
    status = None
    timeout = 0
    print "Waiting for \"Operational\" state..."
    while status != 4 and timeout != 3 and timeout != maxTimeout:
        time.sleep(sleepTime)
        timeout = timeout + sleepTime
        try:
            # status = ls.GET_SYSTEM_GET_STATUS_SYSTEM_STATE()[0]
            status = getSystemStatus(
                address = address, byte = 3, shift = 3, mask = 7)
        except Exception, ex:
            releaseMgr(mgrState, aosTiming)
            printEx(ex)
            return -1
    if status != 4:
        releaseMgr(mgrState, aosTiming)
        print "Timeout!  LS is not in OPERATIONAL state."
        return -1
    else:
        print "LS successfully locked at %f[Ghz]." % frequency

    releaseMgr(mgrState, aosTiming)


def testLoopCpp(photonicReference = None, frequency = None, Jason = None, delay = None):
    '''
    Runs the setLSFrequencyCpp function in a loop.
    '''
    try:
        while True:
            setLSFrequencyCpp(
                photonicReference = photonicReference,
                frequency = frequency,
                Jason = Jason)
            time.sleep(delay)
    except Exception, ex:
        printEx(ex)


def testLoopAmbManager(photonicReference = None, frequency = None, Jason = None, delay = None):
    '''
    Runs the setLSFrequencyAmbManager function in a loop.
    '''
    try:
        while True:
            setLSFrequencyAmbManager(
                photonicReference = photonicReference,
                frequency = frequency,
                Jason = Jason)
            time.sleep(delay)
    except Exception, ex:
        printEx(ex)

def testLoopComponent(photonicReference = None):
    '''
    Runs the LS frequency setting in an endless loop.
    '''
    import time
    import random
    import CCL.CVR, CCL.LS
    cvr = CCL.CVR.CVR(photonicReference)
    ls = CCL.LS.LS(photonicReference)

    def clearErrorStack(ls = None):
        status, timeStamp = ls.GET_SYSTEM_GET_ERROR()
        while status != 0:
            print "Error stack: %d" % status
            status, timeStamp = ls.GET_SYSTEM_GET_ERROR()
        ls.SET_SYSTEM_CLEAR_ERRORS()

    print "Clear all previous errors..."
    clearErrorStack(ls)

    counter = 0
    frequency = 0.0e9
    f_ref3 = 125.0e6

    while True:
        try:
            #frequency = random.randrange(27.26e9, 121.71e9)
            frequency += 1.0e9
            harmonic = 0
            if (frequency - f_ref3) > 27.26e9 and (frequency - f_ref3) < 33.05e9:
                harmonic = 2.0
            elif (frequency - f_ref3) > 65.46e9 and (frequency - f_ref3) <= 89.875e9:
                harmonic = 5.0
            elif (frequency - f_ref3) > 89.875e9 and (frequency - f_ref3) < 121.71e9:
                harmonic = 7.0

            if harmonic == 0.0:
                continue
            elif (frequency - f_ref3) >= 121.71e9:
                break
            else:
                counter += 1
                print "\nCycle #%d" % counter
                print "CVR frequency = %f, harmonic = %f" % (((frequency + 125.0e6) / harmonic), harmonic)
                cvr.setFrequency((frequency + 125.0e6) / harmonic);
                print "LS frequency = %f" % frequency
                ls.setFrequency(frequency)
                clearErrorStack(ls)

        except Exception, ex:
            print "Locking failed!"
            printEx(ex)
            clearErrorStack(ls)
            pass
