/**
 * $Id$
 */


#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>

#include <cxxabi.h>
#include <vector>

const std::string demangle(const char* mangled)
{
    std::size_t s(1024);
    std::vector< char > buffer(s, 0);
    int status(0);

    const std::string demangled(
        __cxxabiv1::__cxa_demangle(mangled, &(buffer[0]), &s, &status));
        return demangled;
}


#define LM_DEBUG
#define LOG_TO_DEVELOPER(a, b) { std::cout << b << "\n"; };


std::string getAlarmMemberName(const std::string& tmpName)
{
    // This string looks like:
    // - "CONTROL/CentralLO" if the device itself is
    //   "CentalLO/ML".
    // - "PhotonicReference3" if the device itself is
    //   "CONTROL/CentralLO/PhotonicReference3/CRD".
    // - "DV01" if the device itself is
    //   "CONTROL/DV01/LORR".
    // - "DA41" if the device itself is
    //   "CONTROL/DA41/FrontEnd/WCA7".
    std::string alarmMemberName(tmpName);

    // Remove "CONTROL/" from the front.
    std::string searchFor("CONTROL/");
    std::string::size_type pos(alarmMemberName.find_first_of(searchFor));
    if(pos == 0)
    {
        alarmMemberName = alarmMemberName.erase(pos, searchFor.size());
    }

    // If the name contains more than one "/", then check for special
    // case below.  Handle here the part before a single and lonely "/".
    // This is OK for:
    // - "CONTROL/CentralLO" -> "CentalLO/ML"
    //   "CONTROL/DV01/LORR" -> "DV01"
    if(std::count(
        alarmMemberName.begin(), alarmMemberName.end(), '/') == 1)
    {
        searchFor = "/";
        pos = alarmMemberName.find(searchFor);
        alarmMemberName = alarmMemberName.erase(pos);

        // That was quick.  Done!
        return alarmMemberName;
    }

    // Cases remaining with multiple occurences of "/":
    // - "CentralLO/PhotonicReference3/CRD"
    // - "DA41/FrontEnd/WCA7"

    // Check if it is a FrontEnd device.
    searchFor = "/FrontEnd/";
    pos = alarmMemberName.find(searchFor);
    if(pos != std::string::npos)
    {
        // This is a FrontEnd subdevice. The Parent String is the one
        // in front of the "/FrontEnd/" string.
        // - "DA41/FrontEnd/WCA7" -> "DA41"
        alarmMemberName = alarmMemberName.erase(pos);

        // That was quick, too.  Done.
        return alarmMemberName;
    }

    // Cases remaining with multiple occurences of "/":
    // - "CentralLO/PhotonicReference3/CRD"
    // The parent is the one between the two "/".

    searchFor = "/";
    pos = alarmMemberName.find_first_of(searchFor) + 1;
    alarmMemberName = alarmMemberName.substr(pos,
            alarmMemberName.find_last_of(searchFor) - pos);

    return alarmMemberName;
}


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    std::string name;

    if(argc == 2)
    {
        name = argv[1];
    }
    else
    {
        std::cout << "Component name required!\n";
        return -1;
    }

    std::cout << "Full component name = \""
        << name
        << "\" -> alarm member name = \""
        << getAlarmMemberName(name)
        << "\"\n";

    return 0;
};
