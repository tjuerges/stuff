#
# $Id$
#

TEMPLATE = app
TARGET = alarmMemberNames

LANGUAGE = C++

CONFIG += rtti stl exceptions debug warn_on
CONFIG -= qt

QMAKE_CXXFLAGS_RELEASE += $$system(cflags.sh QMAKE_RELEASE_CXX)
QMAKE_CXXFLAGS_DEBUG += $$system(cflags.sh QMAKE_DEBUG_CXX)

SOURCES += alarmMemberNames.cpp
