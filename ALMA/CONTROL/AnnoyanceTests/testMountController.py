#! /usr/bin/env python
#
# $Id$
#


import Acspy.Clients.SimpleClient
import Control
import ControlExceptions
import TMCDB_IDL;
import asdmIDLTypes;
import CCL.MountController


s = Acspy.Clients.SimpleClient.PySimpleClient.getInstance()
master = s.getComponent("CONTROL/MASTER")
if(str(master.getMasterState()) == "INACCESSIBLE"):
    antennaName = 'DA41'
    mountConfig = TMCDB_IDL.AssemblyLocationIDL("Mount", "Mount", 0x0, 0, 0)
    sai = TMCDB_IDL.StartupAntennaIDL(antennaName, "P006", "", 1, [],
                                      [mountConfig])
    tmcdb = s.getDefaultComponent("IDL:alma/TMCDB/TMCDBComponent:1.0");
    tmcdb.setStartupAntennasInfo([sai])
    ai = TMCDB_IDL.AntennaIDL(0, 0, antennaName,  "",
                              asdmIDLTypes.IDLLength(12),
                              asdmIDLTypes.IDLArrayTime(0),
                              asdmIDLTypes.IDLLength(1.0),
                              asdmIDLTypes.IDLLength(2.0),
                              asdmIDLTypes.IDLLength(10.0),
                              asdmIDLTypes.IDLLength(0.0),
                              asdmIDLTypes.IDLLength(0.0),
                              asdmIDLTypes.IDLLength(0.0), 0)
    tmcdb.setAntennaInfo(antennaName, ai)
    pi = TMCDB_IDL.PadIDL(0, 0, "P006", asdmIDLTypes.IDLArrayTime(0),
                          asdmIDLTypes.IDLLength( 2202229.615),
                          asdmIDLTypes.IDLLength(-5445184.762),
                          asdmIDLTypes.IDLLength(-2485382.116))
    tmcdb.setAntennaPadInfo(antennaName, pi)
    master.startupPass1()
    master.startupPass2()
    s.releaseComponent(tmcdb._get_name());

mc = CCL.MountController.MountController("DA41")

#if(str(master.getMasterState()) == "OPERATIONAL"):
#    master.shutdownPass1()
#    master.shutdownPass2()
#    s.releaseComponent(master._get_name())
#
#s.disconnect()
