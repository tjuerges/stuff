#! /usr/bin/env python
#
# $Id$
#


import random
import Acspy.Clients.SimpleClient
import CCL.HardwareController
import Control


componentName = "CONTROL/DA41/DGCK"


def getMP(device):
    device.GET_CHANNEL_NUMBER()
    device.GET_CURRENT_PHASE_1()
    device.GET_CURRENT_PHASE_2()
    device.GET_DELAY()
    device.GET_LAST_PHASE_COMMAND_1()
    device.GET_LAST_PHASE_COMMAND_2()
    device.GET_LOCK_VOLTAGE()
    device.GET_MISSED_COMMAND_FLAG()
    device.GET_MODULE_CODES()
    device.GET_MODULE_CODES_CDAY()
    device.GET_MODULE_CODES_CMONTH()
    device.GET_MODULE_CODES_DIG1()
    device.GET_MODULE_CODES_DIG2()
    device.GET_MODULE_CODES_DIG4()
    device.GET_MODULE_CODES_DIG6()
    device.GET_MODULE_CODES_SERIAL()
    device.GET_MODULE_CODES_VERSION()
    device.GET_MODULE_CODES_YEAR()
    device.GET_NODE_ADDRESS()
    device.GET_PLL_LOCK_FLAG()
    device.GET_PS_VOLTAGE()
    device.GetLastPhaseCommand()
    device.delayTrackingEnabled()
    device.GetCurrentPhase()


def setCP(device):
    seq = [[0, 0] for x in range(0, 8)]
    phaseCommand = map(lambda l: Control.DGCK.DelayCommand(phase = l[0] + random.randint(0, 255), timeOffset = l[1] + random.randint(-32768, 32767)), seq)
    device.SetPhaseCommand(phaseCommand)
    device.SET_DELAY(random.randint(0, 0xffffff))
    seq = [0 for x in range(0, 8)]
    seq = map(lambda l: l + random.randint(0, 255), seq)
    device.SET_PHASE_COMMAND_1(seq)
    seq = [0 for x in range(0, 8)]
    seq = map(lambda l: l + random.randint(0, 255), seq)
    device.SET_PHASE_COMMAND_2(seq)
    device.SET_DGCK_INIT()
    device.enableDelayTracking(random.randint(0, 1))
    device.SET_RESET_MISSED_COMMAND_FLAG()
    device.SET_RESET_PLL_LOCK_FLAG()



s = Acspy.Clients.SimpleClient.PySimpleClient.getInstance()
device = s.getComponent(componentName)

if device.getHwState() != Control.HardwareDevice.Operational:
    device.hwConfigure()
    device.hwInitialize()
    device.hwOperational()

print "Monitoring on..."
device.monitoringOn()
print "Test is running..."

while True:
    try:
        getMP(device)
        setCP(device)
    except KeyboardInterrupt, ex:
        break

s.releaseComponent(device._get_name())
s.disconnect()
