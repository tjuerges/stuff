#! /usr/bin/env python
#
# $Id$
#


import math
import random
import Acspy.Clients.SimpleClient
import CCL.HardwareController
import Control
import ControlExceptions
import MountError
import TMCDB_IDL;
import asdmIDLTypes;


componentName = "CONTROL/DA41/Mount"


def getMP(device):
    device.GET_ACU_ERROR()
    device.GET_AC_STATUS()
    device.GET_ANTENNA_TEMPS()
    device.GET_AZ_BRAKE()
    device.GET_AZ_ENCODER_OFFSET()
    device.GET_AZ_MOTOR_CURRENTS()
    device.GET_AZ_MOTOR_TEMPS()
    device.GET_AZ_MOTOR_TORQUE()
    device.GET_AZ_SERVO_COEFF_0()
    device.GET_AZ_SERVO_COEFF_1()
    device.GET_AZ_SERVO_COEFF_2()
    device.GET_AZ_SERVO_COEFF_3()
    device.GET_AZ_SERVO_COEFF_4()
    device.GET_AZ_SERVO_COEFF_5()
    device.GET_AZ_SERVO_COEFF_6()
    device.GET_AZ_SERVO_COEFF_7()
    device.GET_AZ_SERVO_COEFF_8()
    device.GET_AZ_SERVO_COEFF_9()
    device.GET_AZ_SERVO_COEFF_A()
    device.GET_AZ_SERVO_COEFF_B()
    device.GET_AZ_SERVO_COEFF_C()
    device.GET_AZ_SERVO_COEFF_D()
    device.GET_AZ_SERVO_COEFF_E()
    device.GET_AZ_SERVO_COEFF_F()
    device.GET_AZ_STATUS()
    device.GET_AZ_TRAJ()
    device.GET_BASE_ADDRESS()
    device.GET_CAN_ERROR()
    device.GET_CHANNEL_NUMBER()
    device.GET_EL_BRAKE()
    device.GET_EL_ENCODER_OFFSET()
    device.GET_EL_MOTOR_CURRENTS()
    device.GET_EL_MOTOR_TEMPS()
    device.GET_EL_MOTOR_TORQUE()
    device.GET_EL_SERVO_COEFF_0()
    device.GET_EL_SERVO_COEFF_1()
    device.GET_EL_SERVO_COEFF_2()
    device.GET_EL_SERVO_COEFF_3()
    device.GET_EL_SERVO_COEFF_4()
    device.GET_EL_SERVO_COEFF_5()
    device.GET_EL_SERVO_COEFF_6()
    device.GET_EL_SERVO_COEFF_7()
    device.GET_EL_SERVO_COEFF_8()
    device.GET_EL_SERVO_COEFF_9()
    device.GET_EL_SERVO_COEFF_A()
    device.GET_EL_SERVO_COEFF_B()
    device.GET_EL_SERVO_COEFF_C()
    device.GET_EL_SERVO_COEFF_D()
    device.GET_EL_SERVO_COEFF_E()
    device.GET_EL_SERVO_COEFF_F()
    device.GET_EL_STATUS()
    device.GET_EL_TRAJ()
    device.GET_IDLE_STOW_TIME()
    device.GET_IP_ADDRESS()
    device.GET_IP_GATEWAY()
    device.GET_METR_DELTAPATH()
    device.GET_METR_DELTAS_TEMP()
    device.GET_METR_DISPL_0()
    device.GET_METR_DISPL_1()
    device.GET_METR_DISPL_2()
    device.GET_METR_DISPL_3()
    device.GET_METR_DISPL_4()
    device.GET_METR_DISPL_5()
    device.GET_METR_EQUIP_STATUS()
    device.GET_METR_MODE()
    device.GET_METR_TEMPS_00()
    device.GET_METR_TEMPS_01()
    device.GET_METR_TEMPS_02()
    device.GET_METR_TEMPS_03()
    device.GET_METR_TEMPS_04()
    device.GET_METR_TEMPS_05()
    device.GET_METR_TEMPS_06()
    device.GET_METR_TEMPS_07()
    device.GET_METR_TEMPS_08()
    device.GET_METR_TEMPS_09()
    device.GET_METR_TEMPS_0A()
    device.GET_METR_TEMPS_0B()
    device.GET_METR_TEMPS_0C()
    device.GET_METR_TEMPS_0D()
    device.GET_METR_TEMPS_0E()
    device.GET_METR_TEMPS_0F()
    device.GET_METR_TEMPS_10()
    device.GET_METR_TEMPS_11()
    device.GET_METR_TEMPS_12()
    device.GET_METR_TEMPS_13()
    device.GET_METR_TEMPS_14()
    device.GET_METR_TEMPS_15()
    device.GET_METR_TEMPS_16()
    device.GET_METR_TEMPS_17()
    device.GET_METR_TEMPS_18()
    device.GET_METR_TEMPS_19()
    device.GET_METR_TILT_0()
    device.GET_METR_TILT_1()
    device.GET_METR_TILT_2()
    device.GET_METR_TILT_3()
    device.GET_METR_TILT_4()
    device.GET_NODE_ADDRESS()
    device.GET_NUM_TRANS()
    device.GET_POWER_STATUS()
    device.GET_PT_MODEL_COEFF_00()
    device.GET_PT_MODEL_COEFF_01()
    device.GET_PT_MODEL_COEFF_02()
    device.GET_PT_MODEL_COEFF_03()
    device.GET_PT_MODEL_COEFF_04()
    device.GET_PT_MODEL_COEFF_05()
    device.GET_PT_MODEL_COEFF_06()
    device.GET_PT_MODEL_COEFF_07()
    device.GET_PT_MODEL_COEFF_08()
    device.GET_PT_MODEL_COEFF_09()
    device.GET_PT_MODEL_COEFF_0A()
    device.GET_PT_MODEL_COEFF_0B()
    device.GET_PT_MODEL_COEFF_0C()
    device.GET_PT_MODEL_COEFF_0D()
    device.GET_PT_MODEL_COEFF_0E()
    device.GET_PT_MODEL_COEFF_0F()
    device.GET_PT_MODEL_COEFF_10()
    device.GET_PT_MODEL_COEFF_11()
    device.GET_PT_MODEL_COEFF_12()
    device.GET_PT_MODEL_COEFF_13()
    device.GET_PT_MODEL_COEFF_14()
    device.GET_PT_MODEL_COEFF_15()
    device.GET_PT_MODEL_COEFF_16()
    device.GET_PT_MODEL_COEFF_17()
    device.GET_PT_MODEL_COEFF_18()
    device.GET_PT_MODEL_COEFF_19()
    device.GET_PT_MODEL_COEFF_1A()
    device.GET_PT_MODEL_COEFF_1B()
    device.GET_PT_MODEL_COEFF_1C()
    device.GET_PT_MODEL_COEFF_1D()
    device.GET_PT_MODEL_COEFF_1E()
    device.GET_PT_MODEL_COEFF_1F()
    device.GET_SHUTTER()
    device.GET_STOW_PIN()
    device.GET_SUBREF_ABS_POSN()
    device.GET_SUBREF_DELTA_POSN()
    device.GET_SUBREF_LIMITS()
    device.GET_SUBREF_ROTATION()
    device.GET_SUBREF_STATUS()
    device.GET_SW_REVISION_LEVEL()
    device.GET_SYSTEM_ID()
    device.GET_SYSTEM_STATUS()
    device.GET_UPS_OUTPUT_CURRENT()
    device.GET_UPS_OUTPUT_VOLTS()
    device.getAuxPointingModel()
    device.getAxisMode()
    device.getErrorMessage()
    device.getFocusModel()
    device.getFocusOffset()
    device.getFocusZeroPoint()
    device.getMountStatusData()
    device.getPointingModel()
    device.getTolerance()
    device.inLocalMode()
    device.inShutdownMode()
    device.inStandbyMode()
    device.isAuxPointingModelEnabled()
    device.isCLOUDSATRegionAvoided()
    device.isFocusModelEnabled()
    device.isInsideCLOUDSATRegion()
    device.isMonitoring()
    device.isMountStatusDataPublicationEnabled()
    device.isMoveable()
    device.isPointingModelEnabled()
    device.isShutterClosed()
    device.isShutterOpen()
    device.onSource()


def setCP(device):
    device.SET_AZ_SERVO_DEFAULT()
    device.SET_AZ_SERVO_COEFF_0(random.random())
    device.SET_AZ_SERVO_COEFF_1(random.random())
    device.SET_AZ_SERVO_COEFF_2(random.random())
    device.SET_AZ_SERVO_COEFF_3(random.random())
    device.SET_AZ_SERVO_COEFF_4(random.random())
    device.SET_AZ_SERVO_COEFF_5(random.random())
    device.SET_AZ_SERVO_COEFF_6(random.random())
    device.SET_AZ_SERVO_COEFF_7(random.random())
    device.SET_AZ_SERVO_COEFF_8(random.random())
    device.SET_AZ_SERVO_COEFF_9(random.random())
    device.SET_AZ_SERVO_COEFF_A(random.random())
    device.SET_AZ_SERVO_COEFF_B(random.random())
    device.SET_AZ_SERVO_COEFF_C(random.random())
    device.SET_AZ_SERVO_COEFF_D(random.random())
    device.SET_AZ_SERVO_COEFF_E(random.random())
    device.SET_AZ_SERVO_COEFF_F(random.random())
    device.SET_CLEAR_FAULT_CMD()
    device.SET_EL_SERVO_DEFAULT()
    device.SET_EL_SERVO_COEFF_0(random.random())
    device.SET_EL_SERVO_COEFF_1(random.random())
    device.SET_EL_SERVO_COEFF_2(random.random())
    device.SET_EL_SERVO_COEFF_3(random.random())
    device.SET_EL_SERVO_COEFF_4(random.random())
    device.SET_EL_SERVO_COEFF_5(random.random())
    device.SET_EL_SERVO_COEFF_6(random.random())
    device.SET_EL_SERVO_COEFF_7(random.random())
    device.SET_EL_SERVO_COEFF_8(random.random())
    device.SET_EL_SERVO_COEFF_9(random.random())
    device.SET_EL_SERVO_COEFF_A(random.random())
    device.SET_EL_SERVO_COEFF_B(random.random())
    device.SET_EL_SERVO_COEFF_C(random.random())
    device.SET_EL_SERVO_COEFF_D(random.random())
    device.SET_EL_SERVO_COEFF_E(random.random())
    device.SET_EL_SERVO_COEFF_F(random.random())
    device.SET_IDLE_STOW_TIME(random.randint(0, 10))
    device.SET_PT_MODEL_COEFF_00(random.random())
    device.SET_PT_MODEL_COEFF_01(random.random())
    device.SET_PT_MODEL_COEFF_02(random.random())
    device.SET_PT_MODEL_COEFF_03(random.random())
    device.SET_PT_MODEL_COEFF_04(random.random())
    device.SET_PT_MODEL_COEFF_05(random.random())
    device.SET_PT_MODEL_COEFF_06(random.random())
    device.SET_PT_MODEL_COEFF_07(random.random())
    device.SET_PT_MODEL_COEFF_08(random.random())
    device.SET_PT_MODEL_COEFF_09(random.random())
    device.SET_PT_MODEL_COEFF_0A(random.random())
    device.SET_PT_MODEL_COEFF_0B(random.random())
    device.SET_PT_MODEL_COEFF_0C(random.random())
    device.SET_PT_MODEL_COEFF_0D(random.random())
    device.SET_PT_MODEL_COEFF_0E(random.random())
    device.SET_PT_MODEL_COEFF_0F(random.random())
    device.SET_PT_MODEL_COEFF_10(random.random())
    device.SET_PT_MODEL_COEFF_11(random.random())
    device.SET_PT_MODEL_COEFF_12(random.random())
    device.SET_PT_MODEL_COEFF_13(random.random())
    device.SET_PT_MODEL_COEFF_14(random.random())
    device.SET_PT_MODEL_COEFF_15(random.random())
    device.SET_PT_MODEL_COEFF_16(random.random())
    device.SET_PT_MODEL_COEFF_17(random.random())
    device.SET_PT_MODEL_COEFF_18(random.random())
    device.SET_PT_MODEL_COEFF_19(random.random())
    device.SET_PT_MODEL_COEFF_1A(random.random())
    device.SET_PT_MODEL_COEFF_1B(random.random())
    device.SET_PT_MODEL_COEFF_1C(random.random())
    device.SET_PT_MODEL_COEFF_1D(random.random())
    device.SET_PT_MODEL_COEFF_1E(random.random())
    device.SET_PT_MODEL_COEFF_1F(random.random())
    device.SET_SELFTEST_CMD()
    device.SET_SUBREF_DELTA_ZERO_CMD()
    seq = [0 for x in range(0, 3)]
    seq = map(lambda l: l + random.randint(-1e9, 1e9), seq)
    device.SET_SUBREF_ABS_POSN(seq)
    seq = [0 for x in range(0, 3)]
    seq = map(lambda l: l + random.randint(-1e9, 1e9), seq)
    device.SET_SUBREF_DELTA_POSN(seq)
    seq = [0 for x in range(0, 3)]
    seq = map(lambda l: l + random.randint(-1e9, 1e9), seq)
    device.SET_SUBREF_ROTATION(seq)
    try:
        device.autonomous()
    except MountError.TimeOutEx, ex:
        print "Timeout autonomous"
    try:
        device.encoder()
    except MountError.TimeOutEx, ex:
        print "Timeout encoder"
    try:
        device.shutdown()
    except MountError.TimeOutEx, ex:
        print "Timeout shutdown"
    try:
        device.standby()
    except MountError.TimeOutEx, ex:
        print "Timeout standby"
    device.avoidCLOUDSATRegion(random.randint(0, 1))
    device.closeShutter()
    device.enableAuxPointingModel(random.randint(0, 1))
    device.enableFocusModel(random.randint(0, 1))
    device.enablePointingModel(random.randint(0, 1))
    device.flushTrajectory(random.randint(0, 1))
    try:
        device.maintenanceStow()
    except MountError.TimeOutEx, ex:
        print "Timeout maintenanceStow"
    device.openShutter()
    try:
        device.reloadPointingModel()
    except MountError.TMCDBUnavailableEx, ex:
        print "TMCDB unavailable reloadPointingModel"
    device.reportFocusModel()
    device.reportFocusPosition()
    device.reportPointingModel()
    device.setFocusZeroPoint(random.random() * random.randint(-1e9, 1e9), random.random() * random.randint(-1e9, 1e9), random.random() * random.randint(-1e9, 1e9))
    try:
        device.autonomous()
    except MountError.TimeOutEx, ex:
        print "Timeout autonomous"
    try:
        device.setMaintenanceAzEl(math.radians(random.random() * random.randint(0, 360)), math.radians(random.random() * random.randint(0, 90)))
    except (ControlExceptions.IllegalParameterErrorEx, MountError.TimeOutEx,
            MountError.IllegalAxisModeEx), ex:
        print "Exception: setMaintenanceAzEl"
    device.setTolerance(math.radians(random.random()))
    try:
        device.survivalStow()
    except MountError.TimeOutEx, ex:
        print "Timeout survivalStow"


s = Acspy.Clients.SimpleClient.PySimpleClient.getInstance()
device = s.getComponent(componentName)


master = s.getComponent("CONTROL/MASTER")
if(str(master.getMasterState()) == "INACCESSIBLE"):
    antennaName = 'DA41'
    mountConfig = TMCDB_IDL.AssemblyLocationIDL("Mount", "Mount", 0x0, 0, 0)
    sai = TMCDB_IDL.StartupAntennaIDL(antennaName, "P006", "", 1, [],
                                      [mountConfig])
    tmcdb = s.getDefaultComponent("IDL:alma/TMCDB/TMCDBComponent:1.0");
    tmcdb.setStartupAntennasInfo([sai])
    ai = TMCDB_IDL.AntennaIDL(0, 0, antennaName,  "",
                              asdmIDLTypes.IDLLength(12),
                              asdmIDLTypes.IDLArrayTime(0),
                              asdmIDLTypes.IDLLength(1.0),
                              asdmIDLTypes.IDLLength(2.0),
                              asdmIDLTypes.IDLLength(10.0),
                              asdmIDLTypes.IDLLength(0.0),
                              asdmIDLTypes.IDLLength(0.0),
                              asdmIDLTypes.IDLLength(0.0), 0)
    tmcdb.setAntennaInfo(antennaName, ai)
    pi = TMCDB_IDL.PadIDL(0, 0, "P006", asdmIDLTypes.IDLArrayTime(0),
                          asdmIDLTypes.IDLLength( 2202229.615),
                          asdmIDLTypes.IDLLength(-5445184.762),
                          asdmIDLTypes.IDLLength(-2485382.116))
    tmcdb.setAntennaPadInfo(antennaName, pi)
    master.startupPass1()
    master.startupPass2()
    s.releaseComponent(tmcdb._get_name());


if device.getHwState() != Control.HardwareDevice.Operational:
    device.hwConfigure()
    device.hwInitialize()
    device.hwOperational()

print "Monitoring on..."
device.monitoringOn()
device.enableMountStatusDataPublication(True)
print "Test is running..."

while True:
    try:
        getMP(device)
        setCP(device)
    except KeyboardInterrupt, ex:
        break

s.releaseComponent(device._get_name())

if(str(master.getMasterState()) == "OPERATIONAL"):
    master.shutdownPass1()
    master.shutdownPass2()
    s.releaseComponent(master._get_name())

s.disconnect()
