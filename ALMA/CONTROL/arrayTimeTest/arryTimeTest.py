#
# $Id$
#


import string
import time
import datetime
import sys
import Acspy.Common.TimeHelper
import acstime, TETimeUtil
from ACS import acsFALSE, acsTRUE
import ambManager
import CCL.ArrayTime
import CCL.GPS


at = None
gps = None


def tj_statusLORR(abm = None, mgr = None):
    if abm == None:
        print "Provide an ABM!"
        return
    else:
        abm = abm.upper()

    deleteMgr = False
    
    if mgr == None:
        mgr = ambManager.AmbManager(abm.upper())
        deleteMgr = True

    (r, t) = mgr.monitor(0, 0x22, 0x01)
    r = struct.unpack("!2B", r)
    print "Status = [0x%x, 0x%x]" % (r[0], r[1])

    if deleteMgr == True:
        del mgr


def tj_resyncLORR(abm = None):
    if abm == None:
        print "Provide an ABM!"
        return
    else:
        abm = abm.upper()

    mgr = ambManager.AmbManager(abm)

    tj_statusLORR(abm, mgr)

    mgr.command(0, 0x22, 0x82, struct.pack("!b", 1))
    time.sleep(1.0)
    mgr.command(0, 0x22, 0x81, struct.pack("!b", 1))
    time.sleep(1.0)
    mgr.command(0, 0x22, 0x82, struct.pack("!b", 1))
    time.sleep(1.0)

    tj_statusLORR(abm, mgr)

    del mgr


def tj_convertTimeStampArray(tsArray = [], printIt = True):
    for i in range(len(tsArray)):
        acsTimestamp = acstime.Epoch(tsArray[i])
        epoch = Acspy.Common.TimeHelper.TimeUtil()
        epochPy = epoch.epoch2py(acsTimestamp)
        pyEpoch = epoch.py2epoch(epochPy)
        fracSecs = tsArray[i] - pyEpoch.value
        t = time.gmtime(epochPy)
        if printIt == False:
            return (t, fracSecs)
        print "\n%d = %04d-%02d-%02dT%02d:%02d:%02d.%07d\n" % (tsArray[i], t.tm_year, t.tm_mon, t.tm_mday, t.tm_hour, t.tm_min, t.tm_sec, fracSecs)
    return


def compareClocks():
    global at, gps

    if gps == None:
        print "No GPS component available!"
        return  
    if at == None:
        print "No ArrayTime component available!"
        return  

    print "The Delta between the GPS clock and the ArrayTime of this antenna " \
        "should be far less than 1 TE = 0.048s!"
#    try:
    while True:
        t_gps = gps.GET_GPS_TIME() - (14.0 * 1e7)
        data = at.getClockData()
        t_at = (data.ticks * 480000) + data.t0
        tj_convertTimeStampArray([t_gps, t_at])
        print "Delta = %f" % ((t_at - t_gps) / 1e7)
        time.sleep(1.0)
#    except:
#        print "Some exception happened!"
#        pass


if __name__ == '__main__':
    if len(sys.argv) == 2:
        antenna = sys.argv[1]
    else:
        antenna = "da41"

    print "Getting an ArrayTime reference for antenna %s..." % antenna.upper()
    at = CCL.ArrayTime.ArrayTime("CONTROL/" + antenna.upper() + "/ArrayTime")
    
    print "Getting a GPS reference..."
    gps = CCL.GPS.GPS()
#    gps.hwConfigure(); gps.hwInitialize(); gps.hwOperational();


    if at.isSynchronized() == False:
        print "ArrayTime component is NOT synchronised! Trying to resync."
#        tj_resyncLORR(antenna)
#        at.resynchronize()
#        at.synchronizeTime()
#        if at.isSynchronized() == False:
#            print "ArrayTime component is still NOT synchronised! " \
#                "This is now an error condition!\n" \
#                "Check:\n"
#                "- LORR hardware okay?\n"
#                "- ArrayTime component logs?\n"
#                "- TimeSource or CRD logs in ARTM?"
    else:
        print "ArrayTime is synchronised."
    print at.GET_MODE()
    print at.GET_TYPE()

    compareClocks()

#    gps.Stop(); gps.Start()

    del gps
    del at
