/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2006
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Dec 18, 2006  created
*/

#include <ambDeviceInt.h>
#include <ControlExceptions.h>

// This trivial class gets me past the restriction that all the constructors
// for the AmbDeviceInt class are protected.
class BareAMBDeviceInt: public AmbDeviceInt
{
    public:
    BareAMBDeviceInt():
        AmbDeviceInt()
    {
    };

    virtual ~BareAMBDeviceInt()
    {
        AmbInterface::deleteInstance();
        std::cout << "AmbInterface instance released." << std::endl;
    };

    virtual bool initialize(void)
    {
        try
        {
            interface_mp = AmbInterface::getInstance();
        }
        catch(ControlExceptions::CAMBErrorExImpl& ex)
        {
            std::cout << "Unable to get an instance of AmbInterface" << std::endl;
            std::cout << "Have you loaded the kernel modules?" << std::endl;
            return false;
        }

        return true;
    }
};

int main(int argc, char* argv[])
{
    BareAMBDeviceInt* mc(new BareAMBDeviceInt());
    if (mc->initialize() == false)
    {
        return -ENODEV;
    }

    return 0;
}
