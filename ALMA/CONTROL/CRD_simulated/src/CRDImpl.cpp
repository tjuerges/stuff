/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2008
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id$"
 *
 * who       when      what
 * --------  --------  ----------------------------------------------
 * tjuerges  Jan 23, 2008  created
 */


#include <OS_NS_sys_time.h>
#include <acstimeTimeUtil.h>
#include <acstimeEpochHelper.h>
#include <TETimeUtil.h>

#include "CRDImpl.h"


CRDImpl::d_maserVsGPSCounter::d_maserVsGPSCounter(CRDImpl* _crd):
    crd(_crd)
{
}

CRDImpl::d_maserVsGPSCounter::~d_maserVsGPSCounter()
{
}

CORBA::ULongLong CRDImpl::d_maserVsGPSCounter::read(ACS::Time& timestamp)
    throw(ACSErr::ACSbaseExImpl)
{
    EpochHelper epoch(TimeUtil::ace2epoch(ACE_OS::gettimeofday()));
    timestamp = epoch.value().value;

    ACS::Time counter(timestamp - crd->resetTime);
    // Round down to second.
    counter /= TETimeUtil::ACS_ONE_SECOND;
    // Make it 125MHz ticks.
    counter *= 125000000ULL;

    return counter;
}

CRDImpl::d_maserCounter::d_maserCounter(CRDImpl* _crd) :
    crd(_crd)
{
}

CRDImpl::d_maserCounter::~d_maserCounter()
{
}

CORBA::ULongLong CRDImpl::d_maserCounter::read(ACS::Time& timestamp)
    throw(ACSErr::ACSbaseExImpl)
{
    EpochHelper epoch(TimeUtil::ace2epoch(ACE_OS::gettimeofday()));
    timestamp = epoch.value().value;

    ACS::Time counter(timestamp - crd->resetTime);
    // Make it 125MHz ticks. Order is here important.
    counter *= 125000000ULL;
    counter /= TETimeUtil::ACS_ONE_SECOND;

    return counter;
}

CRDImpl::d_getResetTime::d_getResetTime(CRDImpl* _crd) :
    crd(_crd)
{
}

CRDImpl::d_getResetTime::~d_getResetTime()
{
}

CORBA::ULongLong CRDImpl::d_getResetTime::read(ACS::Time& timestamp)
    throw(ACSErr::ACSbaseExImpl)
{
    EpochHelper epoch(TimeUtil::ace2epoch(ACE_OS::gettimeofday()));
    timestamp = epoch.value().value;

    return crd->resetTime;
}

CRDImpl::d_setResetTime::d_setResetTime(CRDImpl* _crd) :
    crd(_crd)
{
}

CRDImpl::d_setResetTime::~d_setResetTime()
{
}

CORBA::ULongLong CRDImpl::d_setResetTime::read(ACS::Time& timestamp)
    throw(ACSErr::ACSbaseExImpl)
{
    EpochHelper epoch(TimeUtil::ace2epoch(ACE_OS::gettimeofday()));
    timestamp = epoch.value().value;

    return crd->resetTime;
}

void CRDImpl::d_setResetTime::write(const CORBA::ULongLong& value,
    ACS::Time& timestamp) throw(ACSErr::ACSbaseExImpl)
{
    EpochHelper epoch(TimeUtil::ace2epoch(ACE_OS::gettimeofday()));
    timestamp = epoch.value().value;

    crd->resetTime = value;
}

CRDImpl::CRDImpl(const ACE_CString& name, maci::ContainerServices* cs)
    throw(ControlExceptions::CDBErrorExImpl):
    HardwareDeviceImpl(name, cs), AmbCDBAccessor(cs),
        m_maserVsGPSCounter_sp(this), m_maserCounter_sp(this),
        m_setResetTime_sp(this), m_getResetTime_sp(this), m_efc5MHz_sp(this),
        m_pwr5MHz_sp(this), m_pwr25MHz_sp(this), m_pwr125MHz_sp(this),
        m_pwr2GHz_sp(this), m_laserCurrent_sp(this), m_vdcMinus5_sp(this),
        m_vdc15_sp(this), m_status_sp(this), m_xilinxPromReset_sp(this),
        m_resetMasterCounterRCA_p(0)
{
    ACS_LOG(LM_SOURCE_INFO, "CRDImpl::CRDImpl", (LM_TRACE, ""));
}

CRDImpl::~CRDImpl()
{
    ACS_LOG(LM_SOURCE_INFO, "CRDImpl::~CRDImpl", (LM_TRACE, ""));
}

/* ------------------------[ Lifecycle Methods ] -------------------------*/
void CRDImpl::initialize() throw(acsErrTypeLifeCycle::LifeCycleExImpl)
{
    ACS_LOG(LM_SOURCE_INFO, "CRDImpl::initialize", (LM_TRACE, 0));

    try
    {
        Control::HardwareDeviceImpl::initialize();

        const ACE_CString nameWithSep(cdbName_m + ":");

        {
            m_maserVsGPSCounter_sp = new baci::ROuLongLong(
                nameWithSep + "maserVsGPSCounter", getComponent(),
                new d_maserVsGPSCounter(this), true);
        }

        {
            m_maserCounter_sp = new baci::ROuLongLong(
                nameWithSep + "maserCounter", getComponent(),
                new d_maserCounter(this), true);
        }

        {
            m_getResetTime_sp = new baci::ROuLongLong(
                nameWithSep + "getResetTime", getComponent(),
                new d_getResetTime(this), true);
        }

        {
            m_setResetTime_sp = new baci::RWuLongLong(
                nameWithSep + "setResetTime", getComponent(),
                new d_setResetTime(this), true);
        }

        ACS::Time timeStamp(0ULL);
        {
            m_efc5MHz_sp = new baci::ROdouble(
                nameWithSep + "efc5MHz", getComponent());
            m_efc5MHz_sp->getDevIO()->write(1.27, timeStamp);
        }

        {
            m_pwr5MHz_sp = new baci::ROdouble(
                nameWithSep + "pwr5MHz", getComponent());
            m_pwr5MHz_sp->getDevIO()->write(0.683, timeStamp);
        }

        {
            m_pwr25MHz_sp = new baci::ROdouble(
                nameWithSep + "pwr25MHz", getComponent());
            m_pwr25MHz_sp->getDevIO()->write(1.362, timeStamp);
        }

        {
            m_pwr125MHz_sp = new baci::ROdouble(
                nameWithSep + "pwr125MHz", getComponent());
            m_pwr125MHz_sp->getDevIO()->write(0.351, timeStamp);
        }

        {
            m_pwr2GHz_sp = new baci::ROdouble(
                nameWithSep + "pwr2GHz", getComponent());
            m_pwr2GHz_sp->getDevIO()->write(1.83, timeStamp);
        }

        {
            m_laserCurrent_sp = new baci::ROdouble(
                nameWithSep + "laserCurrent", getComponent());
            m_laserCurrent_sp->getDevIO()->write(57, timeStamp);
        }

        {
            m_vdcMinus5_sp = new baci::ROdouble(
                nameWithSep + "vdcMinus5", getComponent());
            m_vdcMinus5_sp->getDevIO()->write(-5.3, timeStamp);
        }

        {
            m_vdc15_sp = new baci::ROdouble(
                nameWithSep + "vdc15", getComponent());
            m_vdc15_sp->getDevIO()->write(15.2, timeStamp);
        }

        {
            m_status_sp = new baci::ROpattern(
                nameWithSep + "status", getComponent());
            m_status_sp->getDevIO()->write(0, timeStamp);
        }

        {
            m_xilinxPromReset_sp = new RWAcsBool(
                nameWithSep + "xilinxPromReset", getComponent());
            m_xilinxPromReset_sp->getDevIO()->write(ACS::acsFALSE,
                timeStamp);
        }

        // obtain non-Property RCAs from database
        //m_resetMasterCounterRCA_p = getRCA("resetMasterCounterRCA");
    }
    catch(const ControlExceptions::CDBErrorExImpl& ex)
    {
        // getElement can throw this exception
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            "CRDImpl::initialize");
    }
}

void CRDImpl::hwStartAction()
{
    ACS_LOG(LM_SOURCE_INFO, "CRDImpl::hwStartAction", (LM_TRACE, ""));
}

void CRDImpl::hwConfigureAction()
{
    ACS_LOG(LM_SOURCE_INFO, "CRDImpl::hwConfigureAction", (LM_TRACE, ""));
}

void CRDImpl::hwInitializeAction()
{
    ACS_LOG(LM_SOURCE_INFO, "CRDImpl::hwInitializeAction", (LM_TRACE, ""));
}

void CRDImpl::hwOperationalAction()
{
    ACS_LOG
        (LM_SOURCE_INFO, "CRDImpl::hwOperationalAction", (LM_TRACE, ""));

    resetTime = 0ULL;
}

void CRDImpl::hwStopAction()
{
    ACS_LOG(LM_SOURCE_INFO, "CRDImpl::hwStopAction", (LM_TRACE, 0));
}

//------------------------------------------------------------------------------

ACS::Time CRDImpl::masterCounterReset(ACS::Time execute_time)
    throw(CORBA::SystemException, ControlExceptions::CAMBErrorEx)
{
    ACS_LOG(LM_SOURCE_INFO, "CRDImpl::masterCounterReset", (LM_TRACE, ""));

    ACS_LOG(LM_SOURCE_INFO, "CRDImpl::masterCounterReset", (LM_DEBUG,
        "Reset master counter at time %llu", execute_time));

    // Reset the master counter
    ACS::Time timestamp(0ULL);
    AmbErrorCode_t er(resetMasterCounter(execute_time, timestamp));
    if(er != AMBERR_NOERR)
    {
        ControlExceptions::CAMBErrorExImpl ex(__FILE__, __LINE__,
            "CRDImpl::masterCounterReset");
        ex.addData("Detail", "Failed to reset the master counter: AMB error.");
        ex.log();
        throw ex.getCAMBErrorEx();
    }

    // Set the reset time
    m_setResetTime_sp->getDevIO()->write(execute_time, timestamp);

    ACS_LOG(LM_SOURCE_INFO,"CRDImpl::masterCounterReset",
        (LM_DEBUG, "Set the reset time to %llu[100ns].", execute_time));

    return execute_time;
}

//------------------------------------------------------------------------------

AmbErrorCode_t CRDImpl::resetMasterCounter(const ACS::Time reset_time,
    ACS::Time& timestamp)
{
    timestamp = reset_time;
    resetTime = reset_time;

    return AMBERR_NOERR;
}

//-----------------------------------------------------------------------------
// Property interface
//-----------------------------------------------------------------------------
#define PROP(type,property) \
    type##_ptr CRDImpl::property( \
    ) \
    throw (CORBA::SystemException) \
    { \
    if ( ! m_##property##_sp) \
    return type::_nil(); \
    type##_var prop = type::_narrow( \
        m_##property##_sp->getCORBAReference()); \
        return prop._retn(); \
    }
PROP(ACS::ROuLongLong,maserVsGPSCounter)
;
PROP(ACS::ROuLongLong,maserCounter)
;
PROP(ACS::RWuLongLong,setResetTime)
;
PROP(ACS::ROuLongLong,getResetTime)
;
PROP(ACS::ROdouble,efc5MHz)
;
PROP(ACS::ROdouble,pwr5MHz)
;
PROP(ACS::ROdouble,pwr25MHz)
;
PROP(ACS::ROdouble,pwr125MHz)
;
PROP(ACS::ROdouble,pwr2GHz)
;
PROP(ACS::ROdouble,laserCurrent)
;
PROP(ACS::ROdouble,vdcMinus5)
;
PROP(ACS::ROdouble,vdc15)
;
PROP(ACS::ROpattern,status)
;
PROP(ACS::RWBool,xilinxPromReset)
;

/////////////////////////////////////////////////
// MACI DLL support functions
/////////////////////////////////////////////////
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(CRDImpl)
