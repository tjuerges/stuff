#ifndef CRD_H
#define CRD_H

/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2008
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id$"
 *
 * who       when      what
 * --------  --------  ----------------------------------------------
 * tjuerges  Jan 23, 2008  created
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

// ACS header files
#include <acserr.h>
#include <baci.h>
#include <logging.h>
#include <baciROdouble.h>
#include <baciROlong.h>
#include <baciROpattern.h>
#include <baciROuLongLong.h>
#include <baciRWuLongLong.h>
#include <enumpropRWImpl.h>
#include <baciSmartPropertyPointer.h>
#include <acsBool.h> // This is needed to define the ROAcsBool/RWAcsBool types
#include <enumpropStdC.h>

#include <ControlDeviceExceptionsC.h>

//Control base device
#include <hardwareDeviceImpl.h>
#include <ambCDBAccess.h>
#include <ambDefs.h>

//CORBA servant header
#include "CRDS.h"

#include <OS_NS_sys_time.h>
#include <Time_Value.h>
#include <baciDevIO.h>

// Forward declarations for classes that this component uses
template< class T > class DevIO;
class ACS::ROdouble;
class ROdouble_ptr;
class ACS::ROlong;
class ROlong_ptr;
class ACS::ROpattern;
class ROpattern_ptr;
class ACS::ROuLongLong;
class ROuLongLong_ptr;
class ACS::RWuLongLong;
class RWuLongLong_ptr;
class ACS::RWBool;
class RWBool_ptr;

class CRDImpl: public Control::HardwareDeviceImpl, public AmbCDBAccessor,
    public virtual POA_Control::CRD
{
    public:
    /**
     * Constructor
     * @param poa Poa which will activate this and also all other components.
     * @param name component's name. This is also the name that will
     * be used to find the configuration data for the component in the
     * Configuration Database.
     */
    CRDImpl(const ACE_CString& name, maci::ContainerServices* cs)
        throw(ControlExceptions::CDBErrorExImpl);
    /**
     * Destructor
     */
    virtual ~CRDImpl(void);

    /* --------------------- [ LifeCycle interface ] -------------------*/
    /** Overriden from the HardwareDevice lifecycle interface.
     *
     * @return void
     */
    virtual void initialize() throw(acsErrTypeLifeCycle::LifeCycleExImpl);

    /** Overriden from the HardwareDevice lifecycle interface.
     *
     * @return void
     */
    virtual void hwStartAction();

    /** Overriden from the HardwareDevice lifecycle interface.
     *
     * @return void
     */
    virtual void hwConfigureAction();

    /** Overriden from the HardwareDevice lifecycle interface.
     *
     * @return void
     */
    virtual void hwInitializeAction();

    /** Overriden from the HardwareDevice lifecycle interface.
     *
     * @return void
     */
    virtual void hwOperationalAction();

    /** Overriden from the HardwareDevice lifecycle interface.
     *
     * @return void
     */
    virtual void hwStopAction();

    /* --------------------- [ CORBA interface ] ----------------------*/

    /**
     * Reset the master counter at given time.
     * The reset is synchronized to the TE.
     */
    ACS::Time masterCounterReset(ACS::Time whenToExecute)
        throw(CORBA::SystemException, ControlExceptions::CAMBErrorEx);

    // Properties

    virtual ACS::ROuLongLong_ptr maserVsGPSCounter()
        throw(CORBA::SystemException);

    virtual ACS::ROuLongLong_ptr maserCounter() throw(CORBA::SystemException);

    virtual ACS::RWuLongLong_ptr setResetTime() throw(CORBA::SystemException);

    virtual ACS::ROuLongLong_ptr getResetTime() throw(CORBA::SystemException);

    virtual ACS::ROdouble_ptr efc5MHz() throw(CORBA::SystemException);

    virtual ACS::ROdouble_ptr pwr5MHz() throw(CORBA::SystemException);

    virtual ACS::ROdouble_ptr pwr25MHz() throw(CORBA::SystemException);

    virtual ACS::ROdouble_ptr pwr125MHz() throw(CORBA::SystemException);

    virtual ACS::ROdouble_ptr pwr2GHz() throw(CORBA::SystemException);

    virtual ACS::ROdouble_ptr laserCurrent() throw(CORBA::SystemException);

    virtual ACS::ROdouble_ptr vdcMinus5() throw(CORBA::SystemException);

    virtual ACS::ROdouble_ptr vdc15() throw(CORBA::SystemException);

    virtual ACS::ROpattern_ptr status() throw(CORBA::SystemException);

    virtual ACS::RWBool_ptr xilinxPromReset() throw(CORBA::SystemException);

    private:
    class d_maserVsGPSCounter: public DevIO< CORBA::ULongLong >
    {
        public:
        d_maserVsGPSCounter(CRDImpl* _crd);

        virtual ~d_maserVsGPSCounter();

        virtual CORBA::ULongLong read(ACS::Time& timestamp)
            throw(ACSErr::ACSbaseExImpl);

        private:
        CRDImpl* crd;

        // No normal constructor.
        d_maserVsGPSCounter();

        /**
         * ALMA coding standards: copy constructor is disabled.
         */
        d_maserVsGPSCounter(const d_maserVsGPSCounter& other);

        /**
         * ALMA coding standards: assignment operator is disabled.
         */
        d_maserVsGPSCounter& operator=(const d_maserVsGPSCounter& other);
    };
    friend class d_maserVsGPSCounter;

    class d_maserCounter: public DevIO< CORBA::ULongLong >
    {
        public:
        d_maserCounter(CRDImpl* _crd);

        virtual ~d_maserCounter();

        virtual CORBA::ULongLong read(ACS::Time& timestamp)
            throw(ACSErr::ACSbaseExImpl);

        private:
        CRDImpl* crd;

        // No normal constructor.
        d_maserCounter();

        /**
         * ALMA coding standards: copy constructor is disabled.
         */
        d_maserCounter(const d_maserCounter& other);

        /**
         * ALMA coding standards: assignment operator is disabled.
         */
        d_maserCounter& operator=(const d_maserCounter& other);
    };
    friend class d_maserCounter;

    class d_getResetTime: public DevIO< CORBA::ULongLong >
    {
        public:
        d_getResetTime(CRDImpl* _crd);

        virtual ~d_getResetTime();

        virtual CORBA::ULongLong read(ACS::Time& timestamp)
            throw(ACSErr::ACSbaseExImpl);

        private:
        CRDImpl* crd;

        // No normal constructor.
        d_getResetTime();

        /**
         * ALMA coding standards: copy constructor is disabled.
         */
        d_getResetTime(const d_getResetTime& other);

        /**
         * ALMA coding standards: assignment operator is disabled.
         */
        d_getResetTime& operator=(const d_getResetTime& other);
    };
    friend class d_getResetTime;

    class d_setResetTime: public DevIO< CORBA::ULongLong >
    {
        public:
        d_setResetTime(CRDImpl* _crd);

        virtual ~d_setResetTime();

       virtual CORBA::ULongLong read(ACS::Time& timestamp)
            throw(ACSErr::ACSbaseExImpl);

       virtual void write(const CORBA::ULongLong& value,
            ACS::Time& timestamp) throw(ACSErr::ACSbaseExImpl);

        private:
        CRDImpl* crd;

        // No normal constructor.
        d_setResetTime();

        /**
         * ALMA coding standards: copy constructor is disabled.
         */
        d_setResetTime(const d_setResetTime& other);

        /**
         * ALMA coding standards: assignment operator is disabled.
         */
        d_setResetTime& operator=(const d_setResetTime& other);
    };
    friend class d_setResetTime;

    /**
     * This method resets the master counter at time executeTime. It is synchronized to
     * the 48 ms tick.
     *
     * @param resetTime   : Time in 100 ns units to define when the reset shall be done.
     *                      If it is less than the actual time the reset will be done at
     *                      the next tick.
     * @param value       : The error code (as defined in the ACS Error System) that
     *                      occured while reading this value or 0 if no error occured.
     * @param timestamp   : time when the reset was performed.
     */
    virtual AmbErrorCode_t resetMasterCounter(const ACS::Time resetTime,
        ACS::Time& timestamp);

    // Properties
    SmartPropertyPointer< baci::ROuLongLong > m_maserVsGPSCounter_sp;
    SmartPropertyPointer< baci::ROuLongLong > m_maserCounter_sp;
    SmartPropertyPointer< baci::RWuLongLong > m_setResetTime_sp;
    SmartPropertyPointer< baci::ROuLongLong > m_getResetTime_sp;
    SmartPropertyPointer< baci::ROdouble > m_efc5MHz_sp;
    SmartPropertyPointer< baci::ROdouble > m_pwr5MHz_sp;
    SmartPropertyPointer< baci::ROdouble > m_pwr25MHz_sp;
    SmartPropertyPointer< baci::ROdouble > m_pwr125MHz_sp;
    SmartPropertyPointer< baci::ROdouble > m_pwr2GHz_sp;
    SmartPropertyPointer< baci::ROdouble > m_laserCurrent_sp;
    SmartPropertyPointer< baci::ROdouble > m_vdcMinus5_sp;
    SmartPropertyPointer< baci::ROdouble > m_vdc15_sp;
    SmartPropertyPointer< baci::ROpattern > m_status_sp;
    SmartPropertyPointer< RWAcsBool > m_xilinxPromReset_sp;

    // RCA for resetMaster Counter
    AmbRelativeAddr m_resetMasterCounterRCA_p;

    CORBA::ULongLong resetTime;
};

#endif
