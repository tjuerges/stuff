#*******************************************************************************
# E.S.O. - ACS project
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# ramestic  2003-07-21  created
#

global PID

set ACS_TMP $env(PWD)/tatlogs/run$PID
set env(ACS_TMP) $ACS_TMP
set env(ACS_LOG_STDOUT) 4

#
# ___oOo___
