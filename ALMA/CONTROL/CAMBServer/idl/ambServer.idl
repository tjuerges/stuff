#ifndef AMBSERVER_IDL
#define AMBSERVER_IDL
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2003-12-17  created
*/

/************************************************************************
 * The following definitions define several types which are used 
 * throughout the rest of the ambBus system.  No interface is defined
 * here
 *----------------------------------------------------------------------
 */

#pragma prefix "alma"

//! Module AMBSystem
/*!
    This module defines a number of types which are used throughout the
    ambSystem.  No interface is defined here.

    \b NOTE: An important nomenclature point, all \e channels  attached to
    a single AMB controller are consieder to constitute a single AMB \e Bus.
    The separate physical communication lines are referred to as \e channels .
*/
module AMBSystem {

  /*! Type definition for referring to a particular channel of the bus*/
  typedef unsigned long AmbChannel;

  /*! Type definition for referring to a particular node */
  typedef unsigned long AmbNodeAddr;

  /*! Type definition for referring to a particular node's type */
  typedef unsigned long AmbNodeType;

  /*! Type definition for a relative can address */
  typedef unsigned long AmbRelativeAddr;

  /*! Type definition for complete CAN Address */
  typedef unsigned long AmbAddr;

  /*! Type definiton for a serial number of a device */
  typedef sequence<octet, 8> AmbSerialNumber;

  /*! This is a complete encapsulation of a nodes location information */
  struct AmbNode {
    AmbChannel      channel;   /*!< Channel the device resides on */
    AmbNodeAddr     node;      /*!< Node number of the device */
    AmbSerialNumber sn;        /*!< This device's serial number */
    AmbNodeType     nodeType;  /*!< This device's type */
  };

  /*! A sequence of AmbNodes */
  typedef sequence<AmbNode>  AmbNodeSeq;

  /*! Type definition for contents of a CAN message */
  typedef sequence<octet> MsgData;
};


#endif /*!AMBSERVER_IDL*/
