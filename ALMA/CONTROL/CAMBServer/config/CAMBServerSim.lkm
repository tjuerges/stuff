# @(#) $Id$
#-------------------------------------------------------------------------
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2004, 2005
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
#-------------------------------------------------------------------------
# This file initializes the CAMBServer with the kernel space loopback
# simulator enabled.  Currently the Simulator only supports a single
# channel.  TEs are handled in a soft mode way.  And NTPD is not turned off
# Jeff Kern 9/21/04
#-------------------------------------------------------------------------

MODULE rtai_hal
MODULE rtai_ksched
MODULE rtai_fifos
MODULE rtai_sem
MODULE rtai_mbx
MODULE rtLog
MODULE rtTools
MODULE rtDevDrv

# This replaces the need for the PIO-IP board driver
MODULE DEParallelDrvDummy

# This loads the symbols for the hardware CAN interfaces
MODULE tpmc901dummy

MODULE canSimDrv

# Enable the teHandler is soft mode
MODULE teHandler maxJitClip=100 maxStd=-50
MODULE teSched

# ambServer now takes 2 different parameters
# the first intPerTE is an integer which specifies when in relation to the
# TE the module wakes.  By default intPerTE=2 which means that it wakes at the
# TE and 24 ms after.
# the second parameter is channelOpt which has the following meaning
#
# channelDefinition:
# This string defines the mapping from hardware to channel numbers.
# A channel is defined by 2 characters the first is the device type:
#   e -> TMPC816  That is "e" as in eight
#   o -> TIP901   That is "o" as in one
#   t -> TCIP903  That is "t" as in three
#   s -> AMBSIM   s is for simulator
#  The second character is a digit which specifies the device specific channel
#  number (i.e. the hardware port)
#
# I'm not terribly fond of this mapping if someone has a better suggestion
# please let me know.
#
# The unit for the timeout values is us!
# Defaults: CANReadTimeout="480" CANWriteTimeout="480".
#
# by default channelOpt = "e0e1"
MODULE ambServer channelOpt="s0" CANReadTimeout="2000" CANWriteTimeout="2000"
