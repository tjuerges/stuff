/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2003-12-17  created
*/

/************************************************************************
*   NAME
*     ambInterface
*
*   SYNOPSIS
*
*     This singleton class provides the interface between user space and
*   kernel space for control of the AMB bus.
*
*   PUBLIC METHODS
*
*     static AmbInterface* getInstance();
*     static void          deleteInstance();
*     void                 sendMsg(AmbMessage_t& msg);
*
*   PRIVATE METHODS
*     AmbInterface();
*    ~AmbInterface();
*     static void* replyThread(void*);
*     void ambServerShutdown(void);
*
*   PRIVATE DATA MEMBERS
*
*    static AmbInterface*    instance_mp;
*     static pthread_mutex_t  m_instanceLock;
*     static sem_t            m_sem_count;
*     static int              m_ShutDownFlag;
*     pthread_mutex_t         m_cmdFifo_mtx;
*     int                     m_cmdFifo_fd;
*     pthread_t               m_replyTID;
*
*   FILES
*
*    ambInterface.cpp
*    ambInterface.h
*    ambDefs.h
*    ambServerC.h //Created by IDL
*
*------------------------------------------------------------------------
*/

#include "ambInterface.h"
#include <pthread.h>
#include <rtTools.h>
#include <rtai_fifos.h>
#include <string>

//using namespace ambError;

/* Static Member Definitions */
AmbInterface*    AmbInterface::instance_mp = NULL;
pthread_mutex_t  AmbInterface::instanceLock_m = PTHREAD_MUTEX_INITIALIZER;
int              AmbInterface::shutdownFlag_m;    // When true;exit reply thead
sem_t            AmbInterface::semCount_m;

AmbInterface::AmbInterface()
  throw(ControlExceptions::CAMBErrorExImpl)
{
  const string fnName = "AmbInterface::AmbInterface";
  ACS_TRACE(fnName);
  try {
    staticConstructor();
  }
  catch (ControlExceptions::CAMBErrorExImpl& ex){
    /* Try to unload the module */
    throw ControlExceptions::CAMBErrorExImpl(ex,__FILE__,__LINE__,
                         fnName.c_str());
  }
}

/**
   This function is used to move the initialization of global variables
   out of the constructor.  This ensures that they are created before
   they are used.
   See: http://websqa.hq.eso.org/common/CodingStandards/manuals/ucs_30.htm
   for more info.
*/
void AmbInterface::staticConstructor()
 throw(ControlExceptions::CAMBErrorExImpl)
{
  const string fnName = "AmbInterface::staticContstructor";
  ACS_TRACE(fnName);

  /* Initialize the semaphore which counts references */
  if (sem_init(&semCount_m, 0, 0) != 0) {
    ControlExceptions::CAMBErrorExImpl ex =
      ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,fnName.c_str());
    ex.addData("Detail","Unable to initialize instance count semaphore");
    throw ex;
  }

  /**
   * Open the cmdFifo and create a mutex to protect it
   */
  try {
    pthread_mutex_init(&cmdFifoMtx_m,
               reinterpret_cast<const pthread_mutexattr_t*>("fast"));
    cmdFifoFd_m = open(CMDFIFO_DEV, O_WRONLY);
    if (cmdFifoFd_m  < 0) {
      ControlExceptions::CAMBErrorExImpl ex =
    ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,fnName.c_str());
      ex.addData("Detail","Unable to open the command FIFO");
      throw ex;
    }

    /**
    To prevent corruption make sure the fifo is empty before we
    do anything else
    */
    if (rtf_reset(cmdFifoFd_m)) {
      ControlExceptions::CAMBErrorExImpl ex =
    ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,fnName.c_str());
      ex.addData("Detail","Unable to initialize the command");
      throw ex;
    }

    /**
       Spawn a thread which handles all responses from the server
       This gets closed down when the instance is deleted
    */
    sem_t threadSynchSem;
    sem_init(&threadSynchSem, 0 ,0);
    shutdownFlag_m = 0;
    if (pthread_create(&replyTID_m, NULL,
               reinterpret_cast<void*(*)(void*)>
               (AmbInterface::replyThread),
               static_cast<void*>(&threadSynchSem))!= 0){
      fprintf(stderr,"Unable to start reply processing thread");
      sem_destroy(&threadSynchSem);
      ControlExceptions::CAMBErrorExImpl ex =
    ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,fnName.c_str());
      ex.addData("Detail","Unable to start reply processing thread");
      throw ex;
    }

    /* Give a half second timeout waiting for the thread to synchronize */
    struct timeval  currentTime;
    struct timespec timeout;
    gettimeofday(&currentTime, NULL);
    if (currentTime.tv_usec < 500000) {
      timeout.tv_sec   = currentTime.tv_sec;
      timeout.tv_nsec  = (currentTime.tv_usec + 500000)*1000;
    } else {
      timeout.tv_nsec  = (currentTime.tv_usec - 500000)*1000;
      timeout.tv_sec   = currentTime.tv_sec + 1;
    }

    if (sem_timedwait(&threadSynchSem, &timeout)) {
      /* Timedout starting the thread, Throw and exception */
      fprintf(stderr,"Unable to synchronize reply processing thread");
      sem_destroy(&threadSynchSem);
      ControlExceptions::CAMBErrorExImpl ex =
    ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,fnName.c_str());
      ex.addData("Detail","Unable to synchronize reply processing thread");
      throw ex;
    }
    sem_destroy(&threadSynchSem);
  }

  catch (ControlExceptions::CAMBErrorEx& ex){
    /* if the fifo was opened close it */
    if (cmdFifoFd_m >= 0) {
      close(cmdFifoFd_m);
    }
    /* Destroy the semaphore */
    sem_destroy(&semCount_m);

    /* Continue the Error Trace */
    throw ControlExceptions::CAMBErrorExImpl(ex,__FILE__,__LINE__,
                         fnName.c_str());
  }
}

AmbInterface::~AmbInterface(){
  const string fnName = "AmbInterface::AmbInterface";
  ACS_TRACE(fnName);

  /* Send a message to shut down the server */
  shutdownFlag_m = 1;
  ambServerShutdown();
  pthread_join(replyTID_m, NULL);     //Wait for thread to shutdown

  /* Now Close the communication links */
  close(cmdFifoFd_m);

  if (sem_destroy(&semCount_m) != 0) {
    fprintf(stderr,"Error destroying semaphore\n");
  }
}

void* AmbInterface::replyThread(void* synchSemVoid){
  const string fnName = "AmbInterface::replyThread";

  sem_t* synchSem = static_cast<sem_t*>(synchSemVoid);


  AmbResponse_t response;      //Structure holding response infomation
  int           respFifoFd;    //Fd of the response fifo

  respFifoFd = open(RESPFIFO_DEV, O_RDONLY);

  if (respFifoFd < 0) {
    fprintf(stderr,"AmbInterface::replyThread %s\n",
            "Error opening reply FIFO");
    pthread_exit(NULL);
  }

  /**
      To prevent corruption make sure the fifo is empty before we
      do anything else
  */
  if (rtf_reset(respFifoFd)) {
    fprintf(stderr,"AmbInterface::replyThread %s\n",
            "Unable to initialize reply thread");
    pthread_exit(NULL);
  }

  /* Signal the semaphore that we have succeeded in intializing the
     thread.  The semaphore will be destroyed after this use and
     should never be used again
  */
  sem_post(synchSem);


  /* Now start infinite loop */
  while (shutdownFlag_m == 0) {
    /* Blocking read to read message from FIFO */
    if (rtf_read_all_at_once(respFifoFd, &response, sizeof(response)) !=
    sizeof(response)) {
      fprintf(stderr,"Error reading from fifo\n");
    }

    /* The contlock should only be defined for "long" return (more than
     * one message payload). The initial state should be "set" by the
     * reader
     */

    if(response.magic == 0xdeadbeef)
    {
    	if(response.completion_p != NULL)
	    {
    		if(response.completion_p->contLock_p != NULL)
		    {
    			sem_wait(response.completion_p->contLock_p);
		    }
    
    		/*
    		 * Here we load the data into the return structure if necessary 
    		 */
    		if(response.completion_p != NULL)
		    {
    			fillCompletion(response);
		    }
    
    		/*
    		 * If the sychlock exists post to it 
    		 */
    		if(response.completion_p->synchLock_p != NULL)
		    {
    			sem_post(response.completion_p->synchLock_p);
		    }
    
    		if(response.status != AMBERR_CONTINUE)
		    {
    			/*
    			 * Terminal message, delete the completion structure 
    			 */
    			delete response.completion_p;
		    }
	    }
    }
    else if(response.magic == 0xbeefdead)
    {
    	/**
         * We shall shutdown the system. Usually we shouldn't get here. It is
         * just a safety measure.
         */
    	ACS_SHORT_LOG((LM_WARNING, "Forced shutdown through a magic response "
	       "coming from the kernel module ambServer."));
    
    	break;
    }
    else
    {
    	fprintf(stderr, "ERROR: Corrupted magic word: 0x%x", response.magic);
    }
  }
  close(respFifoFd);
  pthread_exit(NULL);
}


/*
 * This function copies the information into the correct places
 * in the completion structure; provided that they are defined.
 */
void AmbInterface::fillCompletion(AmbResponse_t& response) {
  if (response.completion_p->dataLength_p != NULL) {
    *(response.completion_p->dataLength_p) = response.dataLen;
  }

  if (response.completion_p->data_p != NULL) {
    memcpy(response.completion_p->data_p, response.data, response.dataLen);
  }

  if (response.completion_p->channel_p != NULL) {
    *(response.completion_p->channel_p) = response.channel;
  }

  if (response.completion_p->address_p != NULL) {
    *(response.completion_p->address_p) = response.address;
  }

  if (response.completion_p->timestamp_p != NULL) {
    *(response.completion_p->timestamp_p) = response.timestamp;
  }

  if (response.completion_p->type_p != NULL)
    *(response.completion_p->type_p) = response.type;

  if (response.completion_p->status_p    != NULL) {
     *(response.completion_p->status_p) = response.status;
  }
}

void AmbInterface::ambServerShutdown()
 throw(ControlExceptions::CAMBErrorExImpl)
{
  const string fnName = "AmbInterface::ambServerShutdown";
  ACS_TRACE(fnName);

  AmbMessage_t      control;

  /* Load the data into the message request structure */
  control.requestType  = AMB_SHUTDOWN;
  control.completion_p = reinterpret_cast<AmbCompletion_t*>(NULL);
  control.dataLen      = 0;
  control.targetTE     = 0;
  memset(control.data,0,8);
  control.channel       =0;
  control.address       =0;

  /* Lock the mutex and send the message out */
  pthread_mutex_lock(&cmdFifoMtx_m);
  write(cmdFifoFd_m, &control, sizeof(AmbMessage_t));
  pthread_mutex_unlock(&cmdFifoMtx_m);
}

const AmbInterface* AmbInterface::getInstance()
  throw(ControlExceptions::CAMBErrorExImpl)
 {
  const string fnName = "AmbInterface::getInstance";
  ACS_TRACE(fnName);

  try {
    pthread_mutex_lock(&instanceLock_m);
    if (instance_mp == NULL) {
      instance_mp = new AmbInterface();
    }
    sem_post(&semCount_m);
    pthread_mutex_unlock(&instanceLock_m);
    return instance_mp;
  }
  catch (ControlExceptions::CAMBErrorExImpl& ex){
    pthread_mutex_unlock(&instanceLock_m);
    throw ControlExceptions::CAMBErrorExImpl(ex,__FILE__,__LINE__,
                         fnName.c_str());
  }
}

void AmbInterface::deleteInstance(){
  const string fnName = "AmbInterface::deleteInstance";
  ACS_TRACE(fnName);

  int currentValue;

  pthread_mutex_lock(&instanceLock_m);

  if (sem_trywait(&semCount_m) == 0) {
    sem_getvalue(&semCount_m, &currentValue);
    if (currentValue == 0) {
      /* No Instances left, delete the instance */
      delete instance_mp;
      instance_mp = NULL;
    }
  }
  pthread_mutex_unlock(&instanceLock_m);
}

void AmbInterface::sendMessage(const AmbMessage_t& control) const
  throw(ControlExceptions::CAMBErrorExImpl)
{
  /* Here we want to check to ensure that for extended structures the
   *  synchlock and contlock are defined
   */

  if ((control.requestType == AMB_BLOCK_READ) |
      (control.requestType == AMB_BLOCK_REQUEST) |
      (control.requestType == AMB_GET_NODES) ){

    if ((control.completion_p->synchLock_p == NULL) |
    (control.completion_p->contLock_p == NULL)) {
      /* Error */
      ControlExceptions::CAMBErrorExImpl ex =
    ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,
                       "AmbInterface::sendMessage");
      ex.addData("Detail",
         "Both semaphores are required for this type of request");
      throw ex;
    }
  }
  pthread_mutex_lock(&cmdFifoMtx_m);
  write(cmdFifoFd_m, &control, sizeof(AmbMessage_t));
  pthread_mutex_unlock(&cmdFifoMtx_m);
}

AmbErrorCode_t AmbInterface::findSN(AmbDataMem_t   serialNumber[],
                    AmbChannel     channel,
                    AmbNodeAddr    nodeAddress,
                    Time&          timestamp) const
  throw(ControlExceptions::CAMBErrorExImpl)
{
  const string fnName = "AmbInterface::findSN";

  AmbCompletion_t* completion_p = new AmbCompletion_t;
  AmbMessage_t     message;

  AmbDataLength_t  dataLength;
  AmbErrorCode_t   status;
  sem_t            synchLock;
  sem_init(&synchLock, 0, 0);

  /* Build the completion block */
  completion_p->dataLength_p = &dataLength;
  completion_p->data_p       = serialNumber;
  completion_p->channel_p    = NULL;
  completion_p->address_p    = NULL;
  completion_p->timestamp_p  = &timestamp;
  completion_p->status_p     = &status;
  completion_p->synchLock_p  = &synchLock;
  completion_p->contLock_p   = NULL;
  completion_p->type_p       = NULL;

  /* Build the message block */
  message.requestType  = AMB_MONITOR;
  message.channel      = channel;
  message.address      = createAMBAddr(nodeAddress,0);
  message.dataLen      = 0;
  message.completion_p = completion_p;
  message.targetTE     = 0;

  /* Send the message and wait for a return */
  try {
    sendMessage(message);
  }
  catch (ControlExceptions::CAMBErrorExImpl& ex){
    sem_destroy(&synchLock);
    throw ControlExceptions::CAMBErrorExImpl(ex,__FILE__,__LINE__,
                         fnName.c_str());
  }
  sem_wait(&synchLock);
  sem_destroy(&synchLock);

  switch (status){
  case AMBERR_NOERR:
    break;
  default:
    ControlExceptions::CAMBErrorExImpl ex =
      ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,fnName.c_str());
    ex.addData("ErrorCode",status);
    throw ex;
  }

  return status;
}

AmbErrorCode_t AmbInterface::findSNUsingBroadcast(unsigned char serialNumber[],
                          AmbChannel    channel,
                          AmbNodeAddr   nodeAddress,
                          Time&       timestamp) const
  throw(ControlExceptions::CAMBErrorExImpl)
{
  const string fnName = "AmbInterface::findSNUsingBroadcast";

  AmbCompletion_t* completion = new AmbCompletion_t;
  AmbMessage_t     message;
  bool             foundNode = false;

  AmbChannel       channel_resp;
  AmbAddr          node;
  AmbDataLength_t  dataLength;
  AmbDataMem_t     data[8];
  sem_t            synchLock;
  sem_t            contLock;
  AmbErrorCode_t   status;
  sem_init(&synchLock, 0, 0);
  sem_init(&contLock, 0, 1);

  /* Build the completion block */
  completion->dataLength_p = &dataLength;
  completion->data_p       = data;
  completion->channel_p    = &channel_resp;
  completion->address_p    = &node;
  completion->timestamp_p  = &timestamp;
  completion->status_p     = &status;
  completion->synchLock_p  = &synchLock;
  completion->contLock_p   = &contLock;
  completion->type_p       = NULL;

  /* Build the message block */
  message.requestType  = AMB_GET_NODES;
  message.channel      = channel;
  message.address      = 0;
  message.dataLen      = 0;
  message.completion_p = completion;
  message.targetTE     = 0;

  try {
    sendMessage(message);
  }
  catch (ControlExceptions::CAMBErrorExImpl& ex) {
    sem_destroy(&synchLock);
    sem_destroy(&contLock);
    throw ControlExceptions::CAMBErrorExImpl(ex,__FILE__,__LINE__,
                         fnName.c_str());
  }

  sem_wait(&synchLock);
  while (status == AMBERR_CONTINUE) {
    if (node == nodeAddress) {
      if (dataLength != 8) {
    ACS_SHORT_LOG((LM_ERROR,
               "Node Reported incorrect length Serial Number"));
      } else {
    for (int idx = 0; idx < 8; idx++)
      serialNumber[idx] = data[idx];
    foundNode = true;
      }
    }
    sem_post(&contLock);
    sem_wait(&synchLock);
  }

  if (!foundNode) {
    throw  ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,
                          fnName.c_str());
  }
  return status;
}

AmbErrorCode_t AmbInterface::flush(AmbChannel channel,
                   Time       flushTime,
                   Time&      timestamp) const
  throw(ControlExceptions::CAMBErrorExImpl)
{
  const string fnName = "AmbInterface::flush [channel]";

  AmbCompletion_t* completion_p = new AmbCompletion_t;
  AmbMessage_t     message;

  AmbErrorCode_t   status;
  sem_t            synchLock;
  sem_init(&synchLock, 0, 0);

  /* Build the completion block */
  completion_p->dataLength_p = NULL;
  completion_p->data_p       = NULL;
  completion_p->channel_p    = NULL;
  completion_p->address_p    = NULL;
  completion_p->timestamp_p  = &timestamp;
  completion_p->status_p     = &status;
  completion_p->synchLock_p  = &synchLock;
  completion_p->contLock_p   = NULL;
  completion_p->type_p       = NULL;

  /* Build the message block */
  message.requestType  = AMB_FLUSH;
  message.channel      = channel;
  message.address      = 0;
  message.dataLen      = sizeof(Time);
  memcpy(message.data,&flushTime,message.dataLen);
  message.completion_p = completion_p;
  message.targetTE     = 0;

  /* Send the message and wait for a return */
  try {
    sendMessage(message);
  }
  catch (ControlExceptions::CAMBErrorExImpl& ex){
    sem_destroy(&synchLock);
    throw ControlExceptions::CAMBErrorExImpl(ex,__FILE__,__LINE__,
                         fnName.c_str());
  }
  sem_wait(&synchLock);
  sem_destroy(&synchLock);

  switch (status){
  case AMBERR_NOERR:
    break;
  default:
    ControlExceptions::CAMBErrorExImpl ex =
      ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,fnName.c_str());
    ex.addData("ErrorCode",status);
    throw ex;
  }
  return status;
}
AmbErrorCode_t AmbInterface::flush(AmbChannel      channel,
                   AmbNodeAddr     nodeAddress,
                   Time            flushTime,
                   Time&           timestamp) const
  throw(ControlExceptions::CAMBErrorExImpl){
  const string fnName = "AmbInterface::flush [node]";

  AmbCompletion_t* completion_p = new AmbCompletion_t;
  AmbMessage_t     message;

  AmbErrorCode_t   status;
  sem_t            synchLock;
  sem_init(&synchLock, 0, 0);

  /* Build the completion block */
  completion_p->dataLength_p = NULL;
  completion_p->data_p       = NULL;
  completion_p->channel_p    = NULL;
  completion_p->address_p    = NULL;
  completion_p->timestamp_p  = &timestamp;
  completion_p->status_p     = &status;
  completion_p->synchLock_p  = &synchLock;
  completion_p->contLock_p   = NULL;
  completion_p->type_p       = NULL;

  /* Build the message block */
  message.requestType  = AMB_FLUSH_NODE;
  message.channel      = channel;
  message.address      = createAMBAddr(nodeAddress,0);
  message.dataLen      = sizeof(Time);
  memcpy(message.data,&flushTime,message.dataLen);
  message.completion_p = completion_p;
  message.targetTE     = 0;

  /* Send the message and wait for a return */
  try {
    sendMessage(message);
  }
  catch (ControlExceptions::CAMBErrorExImpl& ex){
    sem_destroy(&synchLock);
    throw ControlExceptions::CAMBErrorExImpl(ex,__FILE__,__LINE__,
                         fnName.c_str());
  }
  sem_wait(&synchLock);
  sem_destroy(&synchLock);

  switch (status){
  case AMBERR_NOERR:
    break;
  default:
    ControlExceptions::CAMBErrorExImpl ex =
      ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,fnName.c_str());
    ex.addData("ErrorCode",status);
    throw ex;
  }
  return status;
}

AmbErrorCode_t AmbInterface::flush(AmbChannel      channel,
                   AmbNodeAddr     nodeAddress,
                   AmbRelativeAddr RCA,
                   Time            flushTime,
                   Time&           timestamp) const
  throw(ControlExceptions::CAMBErrorExImpl){
  const string fnName = "AmbInterface::flush [RCA]";

  AmbCompletion_t* completion_p = new AmbCompletion_t;
  AmbMessage_t     message;

  AmbErrorCode_t   status;
  sem_t            synchLock;
  sem_init(&synchLock, 0, 0);

  /* Build the completion block */
  completion_p->dataLength_p = NULL;
  completion_p->data_p       = NULL;
  completion_p->channel_p    = NULL;
  completion_p->address_p    = NULL;
  completion_p->timestamp_p  = &timestamp;
  completion_p->status_p     = &status;
  completion_p->synchLock_p  = &synchLock;
  completion_p->contLock_p   = NULL;
  completion_p->type_p       = NULL;

  /* Build the message block */
  message.requestType  = AMB_FLUSH_RCA;
  message.channel      = channel;
  message.address      = createAMBAddr(nodeAddress,RCA);
  message.dataLen      = sizeof(Time);
  memcpy(message.data,&flushTime,message.dataLen);
  message.completion_p = completion_p;
  message.targetTE     = 0;

  /* Send the message and wait for a return */
  try {
    sendMessage(message);
  }
  catch (ControlExceptions::CAMBErrorExImpl& ex){
    sem_destroy(&synchLock);
    throw ControlExceptions::CAMBErrorExImpl(ex,__FILE__,__LINE__,
                         fnName.c_str());
  }
  sem_wait(&synchLock);
  sem_destroy(&synchLock);

  switch (status){
  case AMBERR_NOERR:
    break;
  default:
    ControlExceptions::CAMBErrorExImpl ex =
      ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__, fnName.c_str());
    ex.addData("ErrorCode",status);
    throw ex;
  }
  return status;
}
/*___oOo___*/
