/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
*
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2003-11-06  created
*/

/************************************************************************
 * These are the very low level routines which interact with the device
 * drivers.
 *------------------------------------------------------------------------
 */

/*
 * System Headers
 */
#include <linux/kernel.h>
#include <linux/module.h>

#include <linux/types.h>
#include <tpmc816.h>
#include <tip903.h>
#include <tpmc901.h>
#include <rtDevDrv.h>

#include <rtlog.h>
/*
 * Local Headers
 */
#include <ambServer.h>
#include "ambServerInternals.h"


/**
 * External definitions.
 */
extern int minRWTime;
extern int nodesIDsleepTime;
extern unsigned long CANReadTimeout;
extern unsigned long CANWriteTimeout;


/**
 * Because Tews is inconsistant in their naming conventions for their
 * drivers I define the generic MSG_BUF type here.  Then to ensure
 * compatability I test them.
*/
typedef struct
{
    unsigned long Identifier;    /* Message identifier 29-bit or 11-bit */
    unsigned long Timeout;        /* in clock ticks */
    unsigned char RxQueueNum;    /* Receive queue number ( 1..n ) */
    unsigned char Extended;    /* TRUE if extended identifier */
    unsigned char Status;    /* extra message status field */
    unsigned char MsgLen;    /* CAN message length ( 0..8 ) */
    unsigned char Data[8];    /* up to 8 message bytes */
} MSG_BUF;

AmbErrorCode_t testTewsStructures(void)
{
    /*
     * This piece of code ensures that the BitTiming, Acceptance Mask, and
     * Message buffers can safely be cast to each other. By Default we use the
     * 816 structures to store data
     */
    MSG_BUF dummy;

    /*
     * Verify against the TPMC816
     */
    if(&(dummy.Identifier) != &(((TP816_MSG_BUF *) (&dummy))->Identifier))
    {
        return AMBERR_NOMEM;
    }
    if(&(dummy.Timeout) != &(((TP816_MSG_BUF *) (&dummy))->Timeout))
    {
        return AMBERR_NOMEM;
    }
    if(&(dummy.RxQueueNum) != &(((TP816_MSG_BUF *) (&dummy))->RxQueueNum))
    {
        return AMBERR_NOMEM;
    }
    if(&(dummy.Extended) != &(((TP816_MSG_BUF *) (&dummy))->Extended))
    {
        return AMBERR_NOMEM;
    }
    if(&(dummy.Status) != &(((TP816_MSG_BUF *) (&dummy))->Status))
    {
        return AMBERR_NOMEM;
    }
    if(&(dummy.MsgLen) != &(((TP816_MSG_BUF *) (&dummy))->MsgLen))
    {
        return AMBERR_NOMEM;
    }
    if(dummy.Data != ((TP816_MSG_BUF *) (&dummy))->Data)
    {
        return AMBERR_NOMEM;
    }

    /*
     * Verify against the TIP903
     */
    if(&(dummy.Identifier) != &(((T903_MSG_BUF *) (&dummy))->Identifier))
    {
        return AMBERR_NOMEM;
    }
    if(&(dummy.Timeout) != &(((T903_MSG_BUF *) (&dummy))->Timeout))
    {
        return AMBERR_NOMEM;
    }
    if(&(dummy.RxQueueNum) != &(((T903_MSG_BUF *) (&dummy))->RxQueueNum))
    {
        return AMBERR_NOMEM;
    }
    if(&(dummy.Extended) != &(((T903_MSG_BUF *) (&dummy))->Extended))
    {
        return AMBERR_NOMEM;
    }
    if(&(dummy.Status) != &(((T903_MSG_BUF *) (&dummy))->Status))
    {
        return AMBERR_NOMEM;
    }
    if(&(dummy.MsgLen) != &(((T903_MSG_BUF *) (&dummy))->MsgLen))
    {
        return AMBERR_NOMEM;
    }
    if(dummy.Data != ((T903_MSG_BUF *) (&dummy))->Data)
    {
        return AMBERR_NOMEM;
    }

    /*
     * Verify againt the TPMC901
     */
    if(&(dummy.Identifier) != &(((TP901_MSG_BUF *) (&dummy))->identifier))
    {
        return AMBERR_NOMEM;
    }
    if(&(dummy.Timeout) != &(((TP901_MSG_BUF *) (&dummy))->timeout))
    {
        return AMBERR_NOMEM;
    }
    if(&(dummy.RxQueueNum) != &(((TP901_MSG_BUF *) (&dummy))->rx_queue_num))
    {
        return AMBERR_NOMEM;
    }
    if(&(dummy.Extended) != &(((TP901_MSG_BUF *) (&dummy))->extended))
    {
        return AMBERR_NOMEM;
    }
    if(&(dummy.Status) != &(((TP901_MSG_BUF *) (&dummy))->status))
    {
        return AMBERR_NOMEM;
    }
    if(&(dummy.MsgLen) != &(((TP901_MSG_BUF *) (&dummy))->msg_len))
    {
        return AMBERR_NOMEM;
    }
    if(dummy.Data != ((TP901_MSG_BUF *) (&dummy))->data)
    {
        return AMBERR_NOMEM;
    }

    return AMBERR_NOERR;
}

int ambBusOn(int channel)
{
    int result = 0;

    /*
     * Switch the bus on
     */
    switch(threadInfo[channel].deviceType)
    {
        case AMBSIM:
        case TPMC816:
        {
            tpmc816IOCTLStruct ioctlRequest;

            ioctlRequest.channel = threadInfo[channel].deviceChannel;

            if(rtIoctl(threadInfo[channel].deviceID,
                TP816_IOCBUSON,
                (void *)&ioctlRequest)
            < 0)
            {
                rtlogRecord_t logRecord;

                RTLOG_ERROR(modName, ": Failed to turn the CAN-bus on (result=%d)!",
                    result);

                return result;
            }

            /*
             * Flush the FIFO.
             */
            ioctlRequest.channel = threadInfo[channel].deviceChannel;
            ioctlRequest.arg = 0L;

            result = rtIoctl(threadInfo[channel].deviceID,
                TP816_IOCFLUSH,
                (void *)&ioctlRequest);
        }
        break;

        case TIP903:
        {
            tip903IOCTLStruct ioctlRequest;

            ioctlRequest.channel = threadInfo[channel].deviceChannel;

            if(rtIoctl(threadInfo[channel].deviceID,
                T903_IOCBUSON,
                (void *)&ioctlRequest)
            < 0)
            {
                rtlogRecord_t logRecord;

                RTLOG_ERROR(modName, ": Failed to turn the CAN-bus on (result=%d)!",
                    result);

                return result;
            }

            ioctlRequest.channel = threadInfo[channel].deviceChannel;
            ioctlRequest.arg = 0L;

            result = rtIoctl(threadInfo[channel].deviceID,
                T903_IOCFLUSH,
                (void *)&ioctlRequest);
        }
        break;

        case TPMC901:
        {
            tpmc901IOCTLStruct ioctlRequest;

            ioctlRequest.channel = threadInfo[channel].deviceChannel;

            if(rtIoctl(threadInfo[channel].deviceID,
                TP901_IOCBUSON,
                (void *)&ioctlRequest)
            < 0)
            {
                rtlogRecord_t logRecord;

                RTLOG_ERROR(modName, ": Failed to turn the CAN-bus on (result=%d)!",
                    result);

                return result;
            }

            ioctlRequest.channel = threadInfo[channel].deviceChannel;
            ioctlRequest.arg = 0L;

            result = rtIoctl(threadInfo[channel].deviceID,
                TP901_IOCFLUSH,
                (void *)&ioctlRequest);
        }
        break;

        default:
        {
            return AMBERR_UNKNOWNDEV;
        }
    }

    if(result < 0)
    {
        rtlogRecord_t logRecord;

        RTLOG_ERROR(modName, ": Failed to flush fifo (result=%d)!", result);

        return AMBERR_INITFAILED;
    }

    return result;
}

AmbErrorCode_t ambChannelInit(int channel)
{
    int result = 0;

    rtlogRecord_t logRecord;

    switch(threadInfo[channel].deviceType)
    {
        case TPMC816:
        {
            RTLOG_INFO(modName, "Initializing channel %d to TPMC816 channel %d",
                channel, threadInfo[channel].deviceChannel);
        }
        break;

        case TIP903:
        {
            RTLOG_INFO(modName, "Initializing channel %d to TIP903 channel %d",
                channel, threadInfo[channel].deviceChannel);
        }
        break;

        case TPMC901:
        {
            RTLOG_INFO(modName, "Initializing channel %d to TPMC901 channel %d",
                channel, threadInfo[channel].deviceChannel);
        }
        break;

        case AMBSIM:
        {
            RTLOG_INFO(modName, "Initializing channel %d to Simulated channel "
                "channel %d",
                channel, threadInfo[channel].deviceChannel);
        }
        break;

        default:
        {
            return AMBERR_UNKNOWNDEV;
        }
    }

    switch(threadInfo[channel].deviceType)
    {
        case AMBSIM:
        case TPMC816:
        {
            /*
             * Set the bus baud rate to 1 MBit/s
             */
            tpmc816IOCTLStruct ioctlRequest;
            TP816_BITTIMING BitTiming;

            BitTiming.TimingValue = TP816_1MBIT;
            BitTiming.ThreeSamples = 0;

            ioctlRequest.channel = threadInfo[channel].deviceChannel;
            ioctlRequest.arg = (unsigned long)&BitTiming;

            result = rtIoctl(threadInfo[channel].deviceID,
                TP816_IOCSBITTIMING,
                (void *)&ioctlRequest);
        }
        break;

        case TIP903:
        {
            tip903IOCTLStruct ioctlRequest;
            T903_BITTIMING BitTiming;

            BitTiming.TimingValue = T903_1MBIT;
            BitTiming.ThreeSamples = 0;

            ioctlRequest.channel = threadInfo[channel].deviceChannel;
            ioctlRequest.arg = (unsigned long)&BitTiming;

            result = rtIoctl(threadInfo[channel].deviceID,
                T903_IOCSBITTIMING,
                (void *)&ioctlRequest);
        }
        break;

        case TPMC901:
        {
            tpmc901IOCTLStruct ioctlRequest;
            TP901_BITTIMING BitTiming;

            BitTiming.timing_value = TP901_1MBIT;
            BitTiming.three_samples = 0;

            ioctlRequest.channel = threadInfo[channel].deviceChannel;
            ioctlRequest.arg = (unsigned long)&BitTiming;

            result = rtIoctl(threadInfo[channel].deviceID,
                TP901_IOCSBITTIMING,
                (void *)&ioctlRequest);
        }
        break;

        default:
        {
            return AMBERR_UNKNOWNDEV;
        }
    }

    if(result < 0)
    {
        /*
         * Error Condition
         */
        RTLOG_ERROR(modName, ":scanBusInit: Error setting baud rate.");
        return AMBERR_INITFAILED;
    }

    /*
     * Switch the bus on
     */

    result = ambBusOn(channel);

    if(result < 0)
    {
        /*
         * Error Condition
         */
        RTLOG_ERROR(modName, ":ambBusInit: Error turning bus on.");
        return AMBERR_INITFAILED;
    }

    switch(threadInfo[channel].deviceType)
    {
        case AMBSIM:
        case TPMC816:
        {
            /*
             * Set all of the acceptance masks to zero
             */
            tpmc816IOCTLStruct ioctlRequest;
            TP816_ACCEPT_MASKS AcceptMasks;

            AcceptMasks.GlobalMaskStandard = 0x0;
            AcceptMasks.GlobalMaskExtended = 0x0;
            AcceptMasks.Message15Mask = 0x0;

            ioctlRequest.channel = threadInfo[channel].deviceChannel;
            ioctlRequest.arg = (unsigned long)&AcceptMasks;

            result = rtIoctl(threadInfo[channel].deviceID,
                TP816_IOCSSETFILTER,
                (void *)&ioctlRequest);
        }
        break;

        case TIP903:
        {
            tip903IOCTLStruct ioctlRequest;
            T903_ACCEPT_MASKS AcceptMasks;

            AcceptMasks.GlobalMaskStandard = 0x0;
            AcceptMasks.GlobalMaskExtended = 0x0;
            AcceptMasks.Message15Mask = 0x0;

            ioctlRequest.channel = threadInfo[channel].deviceChannel;
            ioctlRequest.arg = (unsigned long)&AcceptMasks;

            result = rtIoctl(threadInfo[channel].deviceID,
                T903_IOCSSETFILTER,
                (void *)&ioctlRequest);
        }
        break;

        case TPMC901:
        {
            tpmc901IOCTLStruct ioctlRequest;
            TP901_ACCEPT_MASKS AcceptMasks;

            AcceptMasks.global_mask_standard = 0x0;
            AcceptMasks.global_mask_extended = 0x0;
            AcceptMasks.message_15_mask = 0x0;

            ioctlRequest.channel = threadInfo[channel].deviceChannel;
            ioctlRequest.arg = (unsigned long)&AcceptMasks;

            result = rtIoctl(threadInfo[channel].deviceID,
                TP901_IOCSSETFILTER,
                (void *)&ioctlRequest);
        }
        break;

        default:
        {
            return AMBERR_UNKNOWNDEV;
        }
    }

    if(result < 0)
    {
        /*
         * Error Condition
         */
        RTLOG_ERROR(modName, ":ambBusInit: Error setting acceptance masks.");
        return AMBERR_INITFAILED;
    }

    /*
     * Set up message object 15 as a receive object for an
     * extended ID of 0x0 (all messages will be received)
     */
    switch(threadInfo[channel].deviceType)
    {
        case AMBSIM:
        case TPMC816:
        {
            tpmc816IOCTLStruct ioctlRequest;
            TP816_BUF_DESC BufDesc;

            BufDesc.MsgObjNum = 15;
            BufDesc.Extended = 1;    /* TRUE */
            BufDesc.Identifier = 0x0;
            BufDesc.RxQueueNum = 1;

            ioctlRequest.channel = threadInfo[channel].deviceChannel;
            ioctlRequest.arg = (unsigned long)&BufDesc;

            result = rtIoctl(threadInfo[channel].deviceID,
                TP816_IOCSDEFRXBUF,
                (void *)&ioctlRequest);
        }
        break;

        case TIP903:
        {
            tip903IOCTLStruct ioctlRequest;
            T903_BUF_DESC BufDesc;

            BufDesc.MsgObjNum = 15;
            BufDesc.Extended = 1;    /* TRUE */
            BufDesc.Identifier = 0x0;
            BufDesc.RxQueueNum = 1;

            ioctlRequest.channel = threadInfo[channel].deviceChannel;
            ioctlRequest.arg = (unsigned long)&BufDesc;

            result = rtIoctl(threadInfo[channel].deviceID,
                T903_IOCSDEFRXBUF,
                (void *)&ioctlRequest);
        }
        break;

        case TPMC901:
        {
            tpmc901IOCTLStruct ioctlRequest;
            TP901_BUF_DESC BufDesc;

            BufDesc.msg_obj_num = 15;
            BufDesc.extended = 1;    /* TRUE */
            BufDesc.identifier = 0x0;
            BufDesc.rx_queue_num = 1;

            ioctlRequest.channel = threadInfo[channel].deviceChannel;
            ioctlRequest.arg = (unsigned long)&BufDesc;

            result = rtIoctl(threadInfo[channel].deviceID,
                TP901_IOCSDEFRXBUF,
                (void *)&ioctlRequest);
        }
        break;

        default:
        {
            return AMBERR_UNKNOWNDEV;
        }
    }

    if(result < 0)
    {
        /*
         * Error Condition
         */
        RTLOG_ERROR(modName, ":ambBusInit: Error setting up extended message obj.");
        return AMBERR_INITFAILED;
    }

    return AMBERR_NOERR;
}

AmbErrorCode_t ambGetNodes(int channel, AmbResponse_t* msgOut, int respFifo)
{
        AmbAddr numNodes = 0;
        int result;
        AmbNodeType tmpNodeType;
        MSG_BUF msgBuf;

        if(ambBusOn(channel) < 0)
        {
        return AMBERR_INITFAILED;
    }

    /*
     * Send the identify broadcast message on AMB ID 0x0
     */
    msgBuf.Timeout = CANWriteTimeout;
    msgBuf.Extended = 1;    /* TRUE */
    msgBuf.Identifier = 0x0;
    msgBuf.MsgLen = 0;

    switch(threadInfo[channel].deviceType)
    {
        case AMBSIM:
        case TIP903:
        case TPMC816:
        case TPMC901:
        {
            result = rtWrite(threadInfo[channel].deviceID,
                &threadInfo[channel].deviceChannel,
                (void *)&msgBuf,
                sizeof(msgBuf),
                CANWriteTimeout);
        }
        break;

        default:
        {
            return AMBERR_UNKNOWNDEV;
        }
    }

    if(result < 0)
    {
        if(result == -ETIME)
        {
            /*
             * We timed out trying to send the broadcast, this usually
             * means no nodes found assume it is true and return no
             * nodes
             */
            *((AmbAddr *)msgOut->data) = 0;
            msgOut->dataLen = sizeof(AmbAddr);
            return AMBERR_NOERR;
        }
        else
        {
            rtlogRecord_t logRecord;

            RTLOG_ERROR(modName, ":ambServer: Failed sending broadcast message "
                "(result=%d).", result);

            return AMBERR_WRITEERR;
                }
        }

        rt_sleep(nano2count(nodesIDsleepTime * 1000LL));
        while(1)
        {
                /*
         * Loop until we are done reading
         */
        msgBuf.Timeout = CANReadTimeout;
        msgBuf.RxQueueNum = 1;
        msgBuf.Extended = 1;

        result = rtRead(threadInfo[channel].deviceID,
            &threadInfo[channel].deviceChannel,
            (void *)&msgBuf,
            sizeof(msgBuf),
            CANReadTimeout);

                if(result == -ETIME)
                {
                        /*
                         * Timed out, assume that is all the nodes
                         */
                        break;
                }
                else if(result < 0)
        {
            rtlogRecord_t logRecord;

            RTLOG_ERROR(modName, "failed reading broadcast response.");

            return AMBERR_READERR;
                }
                else
                {
                        ++numNodes;
                        msgOut->address = (msgBuf.Identifier / AMB_NODE_BASE_ADDR) - 1;
                        tmpNodeType = msgBuf.Identifier % AMB_NODE_BASE_ADDR;
            // Is this a valid node type?
            if((tmpNodeType != UNKNOWN_NODE_TYPE)
            && ((tmpNodeType <= MIN_NODE_TYPE)
            || (tmpNodeType >= MAX_NODE_TYPE)))
            {
                rtlogRecord_t logRecord;

                // No, for the time being set to unknown
                RTLOG_ERROR(modName, "ambGetNodes: Invalid node type: %ld",
                    tmpNodeType);

                tmpNodeType = UNKNOWN_NODE_TYPE;
            }

            msgOut->type = tmpNodeType;
                        memcpy(msgOut->data, msgBuf.Data, msgBuf.MsgLen);
                        msgOut->dataLen = msgBuf.MsgLen;
                        msgOut->status = AMBERR_CONTINUE;
            msgOut->magic = 0xdeadbeef;

                        /*
                         * Write this node out
                         */
            if(rtf_put(respFifo,
                msgOut,
                sizeof(AmbResponse_t))
            != sizeof(AmbResponse_t))
            {
                rtlogRecord_t logRecord;

                RTLOG_ERROR(modName, ": Failed to send request to fifo.");
            }
        }
    }

    // The last piece of data is the number of nodes
    *((AmbAddr *)msgOut->data) = numNodes;
    msgOut->dataLen = sizeof(AmbAddr);

    return AMBERR_NOERR;
}

AmbErrorCode_t ambMonitor(int channel,
    AmbAddr address,
    AmbDataLength_t* dataLen,
    AmbDataMem_t* data)
{
    int result;
    MSG_BUF msgBuf;        // Message buffer read/write arguments
    Time startTime = teHandlerGetTime();
    long sleeptime;

    if(ambBusOn(channel) < 0)
    {
        return AMBERR_INITFAILED;
    }

    /*
     * Send the monitor request
     */
    msgBuf.Timeout = CANWriteTimeout;
    msgBuf.Extended = 1;    /* TRUE */
    msgBuf.Identifier = address;
    msgBuf.MsgLen = 0;

    result = rtWrite(threadInfo[channel].deviceID,
        &threadInfo[channel].deviceChannel,
        (void *)&msgBuf,
        sizeof(msgBuf), CANWriteTimeout);

    if(result < 0)
    {
        rtlogRecord_t logRecord;

        RTLOG_ERROR(modName, ": monitor request failed for channel %d, node "
            "0x%x.", channel, (unsigned int)address);

        return AMBERR_WRITEERR;
    }

    /*
     * We need to check to mke sure the correct amb node responded: Strategy:
     * Continue to read until one of the following conditions is met: The
     * message identifier is from the correct address We time out We get an
     * error
     */
    msgBuf.Identifier = 0;
    while(msgBuf.Identifier != address)
    {
        /*
         * Try to read
         */
        msgBuf.Timeout = CANReadTimeout;
        msgBuf.RxQueueNum = 1;
        msgBuf.Extended = 1;
        msgBuf.Identifier = 0;    // Not a valid address

        result = rtRead(threadInfo[channel].deviceID,
            &threadInfo[channel].deviceChannel,
            (void *)&msgBuf,
            sizeof(msgBuf), CANReadTimeout);

        /*
         * This is just a timeout
         */
        if(result == -ETIME)
        {
            rtlogRecord_t logRecord;

            *dataLen = 0;
            RTLOG_ERROR(modName, ":monitor: timeout on receive for channel %d, "
            "node 0x%x.", channel, (unsigned int)address);


            return AMBERR_TIMEOUT;
        }
        /*
         * These are the real errors
         */
        else if(result < 0)
        {
            rtlogRecord_t logRecord;

            *dataLen = 0;
            RTLOG_ERROR(modName, ":monitor: failed reading AMB for channel %d, "
            "node 0x%x.", channel, (unsigned int)address);


            return AMBERR_READERR;
        }

    }

    // Got a valid response: copy data length and data out
    *dataLen = msgBuf.MsgLen;
    memcpy(data, msgBuf.Data, msgBuf.MsgLen);

    if((sleeptime = startTime + minRWTime - teHandlerGetTime()) > 0)
    {
        rt_sleep(nano2count(sleeptime * 100LL));
    }

    return AMBERR_NOERR;
}

AmbErrorCode_t ambControl(int channel,
    AmbAddr address,
    AmbDataLength_t dataLen,
    AmbDataMem_t* data)
{
    int result;
    MSG_BUF msgBuf;        // Message buffer read/write arguments
    Time startTime = teHandlerGetTime();
    long sleeptime;

    if(ambBusOn(channel) < 0)
    {
        return AMBERR_INITFAILED;
    }

    /*
     * Send the control request
     */
    msgBuf.Timeout = CANWriteTimeout;
    msgBuf.Extended = 1;    /* TRUE */
    msgBuf.Identifier = address;
    msgBuf.MsgLen = dataLen;

    // Copy in control data
    memcpy(msgBuf.Data, data, dataLen);

    result = rtWrite(threadInfo[channel].deviceID,
        &threadInfo[channel].deviceChannel,
        (void *)&msgBuf,
        sizeof(msgBuf), CANWriteTimeout);

    if(result < 0)
    {
        rtlogRecord_t logRecord;

        RTLOG_ERROR(modName, ": control request failed.");

        return AMBERR_WRITEERR;
    }

    if((sleeptime = startTime + minRWTime - teHandlerGetTime()) > 0)
    {
        rt_sleep(nano2count(sleeptime * 100LL));
    }

    return AMBERR_NOERR;
}


AmbErrorCode_t ambBlockRead(int channel,
    AmbAddr address,
    AmbDataLength_t numBlocksExpected,
    AmbResponse_t* msgOut,
    int respFifo)
{
    AmbDataLength_t numBlocksRcvd = 0;    // number of blocks received in CAN bus
    AmbDataLength_t numBlocksQueued = 0;    // number of blocks queued to
                        // ambInterface
    int result;
    MSG_BUF msgBuf;        // Message buffer read/write arguments

    if(ambBusOn(channel) < 0)
    {
        return AMBERR_INITFAILED;
    }

    while(1)
    {
        /*
         * Loop until we are done reading
         */
        msgBuf.Timeout = CANReadTimeout;
        msgBuf.RxQueueNum = 1;
        msgBuf.Extended = 1;

        result = rtRead(threadInfo[channel].deviceID,
            &threadInfo[channel].deviceChannel,
            (void *)&msgBuf,
            sizeof(msgBuf), CANReadTimeout);

        if(result == -ETIME)
        {
            /*
             * Timed out
             */
            // The last piece of data is the actual number of queued
            // blocks
            *((AmbAddr *) msgOut->data) = numBlocksQueued;
            msgOut->dataLen = sizeof(AmbAddr);

            return AMBERR_TIMEOUT;
        }
        else if(result < 0)
        {
            rtlogRecord_t logRecord;

            RTLOG_ERROR(modName, "ambBlockRead: Failed reading response with "
                "return code: %d.",
                result);

            return AMBERR_READERR;
        }
        else
        {
            unsigned int numBytes;

            msgOut->address = msgBuf.Identifier;
            memcpy(msgOut->data, msgBuf.Data, msgBuf.MsgLen);
            msgOut->dataLen = msgBuf.MsgLen;
            numBlocksRcvd++;
            numBlocksQueued++;
            msgOut->status = AMBERR_CONTINUE;
            msgOut->magic = 0xdeadbeef;

            numBytes = rtf_put(respFifo, msgOut, sizeof(AmbResponse_t));

            if(numBytes != sizeof(AmbResponse_t))
            {
                rtlogRecord_t logRecord;

                --numBlocksQueued;
                rtf_reset(respFifo);

                RTLOG_ERROR(modName, "ambBlockRead: Failed writing response to "
                    "fifo -- resetting fifo. --> Number of bytes written: %d.",
                    numBytes);
                break;
            }

            if(numBlocksRcvd == numBlocksExpected)
            {
                break;
            }
        }
    }

    // The last piece of data is the actual number of queued blocks
    *((AmbDataLength_t *)msgOut->data) = numBlocksQueued;
    msgOut->dataLen = sizeof(AmbDataLength_t);
    if(numBlocksRcvd != numBlocksQueued)
    {
        return AMBERR_RESPFIFO;
    }

    return AMBERR_NOERR;
}

AmbErrorCode_t ambBlockRequest(int channel,
    AmbAddr address,
    AmbDataLength_t numBlocksExpected,
    AmbDataMem_t* rqMsg,
    AmbResponse_t* msgOut,
    int respFifo)
{
    AmbDataLength_t numBlocksRcvd = 0;    // number of blocks received in CAN bus
    AmbDataLength_t numBlocksQueued = 0;    // number of blocks queued to
                        // ambInterface
    int result;
    MSG_BUF msgBuf;        // Message buffer read/write arguments

    if(ambBusOn(channel) < 0)
    {
        return AMBERR_INITFAILED;
    }

    /*
     * Send the non-monitor request message on AMB ID
     */
    msgBuf.MsgLen = 8;
    memcpy(msgBuf.Data, rqMsg, msgBuf.MsgLen);
    msgBuf.Timeout = CANWriteTimeout;
    msgBuf.Extended = 1;    /* TRUE */
    msgBuf.Identifier = address;

    result = rtWrite(threadInfo[channel].deviceID,
        &threadInfo[channel].deviceChannel,
        (void *)&msgBuf,
        sizeof(msgBuf), CANWriteTimeout);

    if(result < 0)
    {
        rtlogRecord_t logRecord;

        RTLOG_ERROR(modName, "ambBlockRequest: Failed sending request message "
            "with return code: %d.",
            result);

        return AMBERR_WRITEERR;
    }

    while(1)
    {
        /*
         * Loop until we are done reading
         */
        msgBuf.Timeout = CANReadTimeout;
        msgBuf.RxQueueNum = 1;
        msgBuf.Extended = 1;

        result = rtRead(threadInfo[channel].deviceID,
            &threadInfo[channel].deviceChannel,
            (void *)&msgBuf,
            sizeof(msgBuf), CANReadTimeout);

        if(result == -ETIME)
        {
            /*
             * Timed out
             */
            /*
             * The last piece of data is the number of queued blocks
             */
            *((AmbAddr *)msgOut->data) = numBlocksQueued;
            msgOut->dataLen = sizeof(AmbAddr);
            return AMBERR_TIMEOUT;
        }
        else if(result < 0)
        {
            rtlogRecord_t logRecord;

            RTLOG_ERROR(modName, "ambBlockRequest: Failed reading response "
                "with return code: %d.",
                result);

            return AMBERR_READERR;
        }
        else if((msgBuf.Identifier == address)
        || (msgBuf.Identifier == address + 1))
        {
            /*
             * Is this message an actual response or the signal that
             * all reponses have been sent?
             */
            unsigned int numBytes;

            msgOut->address = msgBuf.Identifier;
            memcpy(msgOut->data, msgBuf.Data, msgBuf.MsgLen);
            msgOut->dataLen = msgBuf.MsgLen;
            numBlocksRcvd++;
            numBlocksQueued++;
            msgOut->status = AMBERR_CONTINUE;
            msgOut->magic = 0xdeadbeef;

            numBytes = rtf_put(respFifo, msgOut, sizeof(AmbResponse_t));

            if(numBytes != sizeof(AmbResponse_t))
            {
                rtlogRecord_t logRecord;

                --numBlocksQueued;
                rtf_reset(respFifo);

                RTLOG_ERROR(modName, "ambBlockRequest: Failed writing response "
                    "to fifo -- resetting fifo. ambBlockRequest--> Number of "
                    "bytes written: %d.",
                    numBytes);

                break;
            }

            if((numBlocksRcvd == numBlocksExpected)
            || (msgBuf.Identifier == address + 1))
            /*
             * We either received all of the blocks requested or
             * the block signalling that all blocks have been sent
             */
            break;
        }
    }

    // The last piece of data is the actual number of queued blocks
    *((AmbDataLength_t *)msgOut->data) = numBlocksQueued;
    msgOut->dataLen = sizeof(AmbDataLength_t);

    if(numBlocksRcvd != numBlocksQueued)
    {
        return AMBERR_RESPFIFO;
    }

    return AMBERR_NOERR;
}
