/*******************************************************************************
*
* "@(#) $Id$"
*
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* who       when        what
* --------  ----------  ----------------------------------------------
* tjuerges  2006-04-03  created
*/

/************************************************************************
*   NAME
*
*   SYNOPSIS
*
*   DESCRIPTION
*
*   FILES
*
*   ENVIRONMENT
*
*   RETURN VALUES
*
*   CAUTIONS
*
*   EXAMPLES
*
*   SEE ALSO
*
*   BUGS
*
*------------------------------------------------------------------------
*/

static char* rcsId = "@(#) $Id$";
static void* use_rcsId = ((void)&use_rcsId, (void*)&rcsId);

/*
 * System stuff
 */
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/delay.h>
#include <asm/atomic.h>
/* ... as you need them ... */

/*
 * RTAI stuff
 */
#include <rtai_sched.h>
#include <rtai_registry.h>
#include <rtai_sem.h>
#include <rtai_malloc.h>
#include <rtai_fifos.h>
/* ... as you need them ... */

/*
 * ACS stuff
 */
#include <rtTools.h>
#include <rtlog.h>

/*
 * Local stuff
 */
#include <teHandler.h>
#include <teSched.h>
#include <DEParallelDrv.h>
#include <ambServer.h>
#include "ambServerInternals.h"


MODULE_AUTHOR("Jeff Kern <jkern@nrao.edu>, J. Perez <jperez@nrao.edu>, Thomas Juerges <tjuerges@nrao.edu>");
MODULE_DESCRIPTION("Kernel module to interface CAN communication between different CAN-bus cards and kernel/user space.");
MODULE_LICENSE("GPL");

/**
 * Module parameters.
 */
static unsigned int initTimeout = 1000;    /* msec */
static unsigned int cleanUpTimeout = 1000;    /* msec */

/*
 * channelDefinition:
 * This string defines the mapping from hardware to channel numbers.
 * A channel is defined by 2 characters the first is the device type:
 *   e -> TMPC816  That is "e" as in eight
 *   o -> TIP901   That is "o" as in one
 *   t -> TCIP903  That is "t" as in three
 *   s -> AMBSIM   s is for simulator
 *  The second character is a digit which specifies the device specific channel
 * number (i.e. the hardware port)
 *
 * I'm not terribly fond of this mapping if someone has a better suggestion
 * please let me know.
 */
static char* channelOpt = "e0e1";

/*
 * intPerTe is the number of times the TE related stuff is processed per
 * TE.  The default (2) is appropriate for control.  For Correlator (3) might
 * be a better value as this will wake the TE handler at 0, 16, and 32 ms
 * after the TE.
 * To change this value do: "modprobe ambServer intPerTE=<n>"
 */
static int intPerTE   = 2;

/*
 * minRWTime is the MINIMUM time (in 100 ns units) that a read or write
 * can take this is useful because some devices (AMBSI-2) cannot respond to
 * two commands back to back.  The default time is 450 us
 * Note that the minRWTime may (and usually is) longer than the can timeout
 */
int minRWTime = 4500;

/* nodesIDsleepTime is the time to sleep between a nodes ID request and
 * the first nodes ID write to the user space fifo.  When this sleep is not there
 * in fast simulated tests in UP machines the user fifos get corrupt messages.
 */
int nodesIDsleepTime = 0;

/*
 * Timeout definition:
 * The parameter CANWriteTimeout and CANReadTimeout control the timeout
 * of the respective CAN io.  These are currently specified in ms and must
 * be integers.
 * The default for both of these is 2 ms = 2000 us. The unit is us!
 */
unsigned long CANReadTimeout = 2000;
unsigned long CANWriteTimeout = 2000;

/*
 * Stack size for created RTOS tasks.
 */
static unsigned int stackSize = 8192;

module_param(channelOpt, charp, S_IRUGO);
module_param(initTimeout, uint, S_IRUGO);
module_param(cleanUpTimeout, uint, S_IRUGO);
module_param(minRWTime, int, S_IRUGO);
module_param(intPerTE, int, S_IRUGO);
module_param(CANReadTimeout, ulong, S_IRUGO);
module_param(CANWriteTimeout, ulong, S_IRUGO);
module_param(stackSize, uint, S_IRUGO);
module_param(nodesIDsleepTime, int, S_IRUGO);


/*
 * my local data
 */

/* This is the queue for availible message holders */
static int availMsgHead = -1;
static int availMsgTail = -1;
static SEM availMsgSem;
static AmbMessageList_t* msgPool = 0;
static int poolSize = 3072;
static long long maxPoolUsed = 100LL;
static long long msgsIntoPool = 0LL;
static long long msgsOutOfPool = 0LL;
static atomic_t exitFlag = ATOMIC_INIT(0);

static int numAmbChannels;

static SEM* teSem[MAX_INT_PER_TE];
static SEM msgWaitingSem;

/* Tasks */
static RT_TASK dispatch_ID;
static RT_TASK dist_ID;

static int numAmbChannels = 0;

//#include "ambCANIO.c"

/* This is the handler on the fifo, it notifies the dispatcher that there is
   a new message to be handled.
*/
int dispatchSignal(unsigned int fifo, int rw)
{
    rt_sem_signal(&msgWaitingSem);
    return 0;
}

/* This routine is responsable for waking on semaphores given by the teSched
   module and setting the semaphores to wake the te Handler.
*/
void teDist(long junk)
{
    rtlogRecord_t logRecord;    // For the logging channel
    int channel;
    int idx = 0;

    while(atomic_read(&exitFlag) == 0)
    {
        /*
         * Post to all te handlers
         */
        for(channel = 0; channel < numAmbChannels; ++channel)
        {
            rt_sem_signal(&threadInfo[channel].teSem);
        }

        idx = (idx + 1) % intPerTE;
        rt_sem_wait(teSem[idx]);
    }

    for(idx = 0; idx < intPerTE; ++idx)
    {
        teSchedRelSem(teSem[idx]);
    }

    RTLOG_INFO(modName, "Normal exit of teDist task.");

    rt_task_delete(rt_whoami());
}


/* This is the handler on the fifo, it reads the message into the next
   availible message block and then routes it to the correct place.  For
   "global" events they are handled by this routine itself.
*/
void dispatcher(long junk)
{
    int channel = 0;        // get the channel from the message
    int currentMessage;
    AmbResponse_t msgOut;    // Memory for returning responses
    rtlogRecord_t logRecord;    // For the logging channel

    RTLOG_INFO(modName, "Dispatcher Started.");

    rt_sem_wait(&msgWaitingSem);
    while(atomic_read(&exitFlag) == 0)
    {
        /*
         * Find the current message to use, allocating additional space if
         * necessary
         */
        rt_sem_wait(&availMsgSem);
        if(availMsgHead < 0)
        {
            /*
             * Unable to allocate more memory, return an error
             */
            AmbMessage_t errIn;
            memset(&msgOut, 0, sizeof(AmbResponse_t));

            RTLOG_ERROR(modName, "Out of memory!  The current message is lost!");

            if(rtf_get(CMDFIFO, &errIn, sizeof(AmbMessage_t)) < 0)
            {
                /*
                 * ERROR
                 */
                RTLOG_ERROR(modName, "Failed to read request from fifo.");
            }

            msgOut.timestamp = teHandlerGetTime();
            msgOut.completion_p = errIn.completion_p;
            msgOut.status = AMBERR_NOMEM;
            msgOut.magic = 0xdeadbeef;

            if(rtf_put(RESPFIFO, &msgOut, sizeof(AmbResponse_t))
            != sizeof(AmbResponse_t))
            {
                RTLOG_ERROR(modName, "Failed to send request to fifo.");
            }

            rt_sem_signal(&availMsgSem);

            /*
             * JSK 05/04/05: I've had to change the structure
             * of the system here so I can no longer simply
             * return from this point to exit the function.  I
             * now need to go to the end of the loop and wait
             * for the next semaphore. This is really ugly, but
             * I think that in this case it is probably the
             * best solution.
             */
            goto EndOfLoop;
        }

        currentMessage = availMsgHead;
        ++msgsIntoPool;
        availMsgHead = msgPool[currentMessage].next;
        if((msgsIntoPool - msgsOutOfPool) > maxPoolUsed)
        { // The depth of the list increased by 100 - log message
            RTLOG_INFO(modName,"CAMBServer message pool high water mark: %lld",
                maxPoolUsed);
            maxPoolUsed += 100;
        }
        if(currentMessage == availMsgTail)
        {
            availMsgTail = -1;
        }

        rt_sem_signal(&availMsgSem);

        /*
         * Now read in the message from the fifo
         */
        if(rtf_get(CMDFIFO, &msgPool[currentMessage].msg, sizeof(AmbMessage_t))
        < 0)
        {
            /*
             * ERROR
             */
            RTLOG_ERROR(modName, "Failed to read request from fifo.");
        }

        channel = msgPool[currentMessage].msg.channel;

        /*
         * The following messages must get handled on a global scope
         */
        switch (msgPool[currentMessage].msg.requestType)
        {
            case AMB_SHUTDOWN:
            {
                int idx;
                memset(&msgOut, 0, sizeof(AmbResponse_t));

                msgOut.completion_p = msgPool[currentMessage].msg.completion_p;
                for(idx = 0; idx < numAmbChannels; ++idx)
                {
                    /*
                     * Ensure that there are no remaining
                     * messages
                     */
                    flush(idx, 0, 0, 0);
                }

                msgOut.timestamp = teHandlerGetTime();
                msgOut.magic = 0xdeadbeef;
                if(rtf_put(RESPFIFO, &msgOut, sizeof(AmbResponse_t))
                != sizeof(AmbResponse_t))
                {
                    RTLOG_ERROR(modName, "Failed to send request to fifo.");
                }

                /*
                 * Return the current message to the Message pool
                 */
                rt_sem_wait(&availMsgSem);
                if(availMsgTail != -1)
                {
                    msgPool[availMsgTail].next = currentMessage;
                }
                else
                {
                    availMsgHead = currentMessage;
                }
                ++msgsOutOfPool;
                msgPool[currentMessage].next = -1;
                availMsgTail = currentMessage;
                rt_sem_signal(&availMsgSem);
            }
            break;

            case AMB_GET_NO_CHANNELS:
            {
                /*
                 * Return the number of Channels
                 */
                memset(&msgOut, 0, sizeof(AmbResponse_t));

                msgOut.timestamp = teHandlerGetTime();
                msgOut.completion_p = msgPool[currentMessage].msg.completion_p;
                msgOut.dataLen = sizeof(AmbChannel);
                *((AmbChannel*)msgOut.data) = numAmbChannels;
                msgOut.status = AMBERR_NOERR;
                msgOut.magic = 0xdeadbeef;

                if(rtf_put(RESPFIFO, &msgOut, sizeof(AmbResponse_t))
                != sizeof(AmbResponse_t))
                {
                    RTLOG_ERROR(modName, "Failed to send request to fifo.");
                }

                /*
                 * Return the current message to the Message pool
                 */
                rt_sem_wait(&availMsgSem);
                if(availMsgTail != -1)
                {
                    msgPool[availMsgTail].next = currentMessage;
                }
                else
                {
                    availMsgHead = currentMessage;
                }

                ++msgsOutOfPool;
                msgPool[currentMessage].next = -1;
                availMsgTail = currentMessage;
                rt_sem_signal(&availMsgSem);
            }
            break;

            case AMB_FLUSH:
            case AMB_FLUSH_NODE:
            case AMB_FLUSH_RCA:
            {
                /*
                 * Although not really executed in global scope
                 * there is no need to queue this one so we execute
                 * it here
                 */
                memset(&msgOut, 0, sizeof(AmbResponse_t));

                msgOut.completion_p = msgPool[currentMessage].msg.completion_p;
                switch (msgPool[currentMessage].msg.requestType)
                {
                    case AMB_FLUSH:
                    {
                        msgOut.status = flush(channel, 0, 0,
                            *((Time*)msgPool[currentMessage].msg.data));
                    }
                    break;

                    case AMB_FLUSH_NODE:
                    {
                        msgOut.status = flush(channel, 0x1FFC0000LL,
                            msgPool[currentMessage].msg.address,
                            *((Time*)msgPool[currentMessage].msg.data));
                    }
                    break;

                    case AMB_FLUSH_RCA:
                    {
                        msgOut.status = flush(channel, 0x1FFFFFFFLL,
                            msgPool[currentMessage].msg.address,
                            *((Time*)msgPool[currentMessage].msg.data));
                    }
                    break;

                    default:
                    {
                        // Never Happens, just to make compiler
                        // happy.
                        RTLOG_ERROR(modName, "Should Never Execute!");
                    }
                }

                /*
                 * This is the response to the flush command
                 */
                msgOut.timestamp = teHandlerGetTime();
                msgOut.magic = 0xdeadbeef;

                if(rtf_put(RESPFIFO, &msgOut, sizeof(AmbResponse_t))
                != sizeof(AmbResponse_t))
                {
                    RTLOG_ERROR(modName, "Failed to send request to fifo.");
                }

                /*
                 * Return the current message to the Message pool
                 */
                rt_sem_wait(&availMsgSem);
                if(availMsgTail != -1)
                {
                    msgPool[availMsgTail].next = currentMessage;
                }
                else
                {
                    availMsgHead = currentMessage;
                }

                ++msgsOutOfPool;
                msgPool[currentMessage].next = -1;
                availMsgTail = currentMessage;
                rt_sem_signal(&availMsgSem);
            }
            break;

            case AMB_MONITOR_NEXT:
            case AMB_CONTROL_NEXT:
            {
                /*
                 * This is to be executed in the Monitor or Control
                 * window of the next TE, here we adjust the
                 * targetTe to accomplish this
                 */
                msgPool[currentMessage].msg.targetTE = teHandlerGetLast() + TEacs;
                if(msgPool[currentMessage].msg.requestType == AMB_MONITOR_NEXT)
                {
                    msgPool[currentMessage].msg.targetTE += TEacs / 2;
                    msgPool[currentMessage].msg.requestType = AMB_MONITOR;
                }
                else
                {
                    msgPool[currentMessage].msg.requestType = AMB_CONTROL;
                }

                if(msgPool[currentMessage].msg.targetTE - teHandlerGetTime()
                < 10000)
                {
                    /*
                     * We are within a ms of the deadline, lets
                     * make our life easy and put it off for a
                     * TE
                     */
                    msgPool[currentMessage].msg.targetTE +=    TEacs;
                }
            }    // Note the intentional fall through to the
                // default case

            default:
            {
            /*
             * Not executed in global scope enqueue in correct
             * channel
             */
                if(msgPool[currentMessage].msg.channel >= numAmbChannels)
                {
                    /*
                     * Error
                     */
                    memset(&msgOut, 0, sizeof(AmbResponse_t));

                    msgOut.status = AMBERR_BADCHAN;
                    msgOut.timestamp = teHandlerGetTime();
                    msgOut.completion_p =
                        msgPool[currentMessage].msg.completion_p;
                    msgOut.magic = 0xdeadbeef;

                    if(rtf_put(RESPFIFO, &msgOut, sizeof(AmbResponse_t))
                    != sizeof(AmbResponse_t))
                    {
                        RTLOG_ERROR(modName, "Failed to send request to fifo.");
                    }

                    /*
                     * Return the current message to the Message
                     * pool
                     */
                    rt_sem_wait(&availMsgSem);
                    if(availMsgTail != -1)
                    {
                        msgPool[availMsgTail].next = currentMessage;
                    }
                    else
                    {
                        availMsgHead = currentMessage;
                    }

                    ++msgsOutOfPool;
                    msgPool[currentMessage].next = -1;
                    availMsgTail = currentMessage;
                    rt_sem_signal(&availMsgSem);
                }
                else
                {
                    if(msgPool[currentMessage].msg.targetTE == 0)
                    {
                        /*
                         * This is an ASAP message queue it as
                         * such
                         */
                        rt_sem_wait(&threadInfo[channel].asapChainSem);
                        if(threadInfo[channel].asapHead == -1)
                        {
                            threadInfo[channel].asapHead = currentMessage;
                        }
                        else
                        {
                            msgPool[threadInfo[channel].asapTail].next =
                                currentMessage;
                        }

                        threadInfo[channel].asapTail = currentMessage;
                        msgPool[currentMessage].next = -1;

                        rt_sem_signal(&threadInfo[channel].asapChainSem);
                        rt_sem_signal(&threadInfo[channel].asapSem);
                    }
                    else
                    {
                        /*
                         * This is a TE message This logic
                         * puts the te in the correct channel
                         * list according to the target TE.
                         */
                        int msgIter;
                        int last;

                        rt_sem_wait(&threadInfo[channel].teChainSem);
                        msgIter = threadInfo[channel].teHead;
                        last = -1;

                        while(msgIter != -1)
                        {
                            if(msgPool[currentMessage].msg.targetTE
                            < msgPool[msgIter].msg.targetTE)
                            {
                                break;
                            }

                            last = msgIter;
                            msgIter = msgPool[msgIter].next;
                        }

                        if(last == -1)
                        {
                            /*
                             * Then we are adding this at
                             * the head
                             */
                            threadInfo[channel].teHead = currentMessage;
                        }
                        else
                        {
                            msgPool[last].next = currentMessage;
                        }

                        msgPool[currentMessage].next = msgIter;
                        rt_sem_signal(&threadInfo[channel].teChainSem);
                    }
                }
            }
        }
        /*
         * This label if for use ONLY for the out of memory case above!
         */
EndOfLoop:
        rt_sem_wait(&msgWaitingSem);
    }

    RTLOG_INFO(modName, "Dispatcher Stopped.");

    rt_task_delete(rt_whoami());
}

void asapThread(long channel)
{
    int currentMessage;
    rtlogRecord_t logRecord;    // For the logging channel

    RTLOG_INFO(modName, "asapThread Started: Channel %ld.", channel);

    rt_sem_wait(&threadInfo[channel].asapSem);
    while(atomic_read(&exitFlag) == 0)
    {
        /*
         * Get the current message, and remove it from the front of the
         * asap chain
         */
        rt_sem_wait(&threadInfo[channel].asapChainSem);
        currentMessage = threadInfo[channel].asapHead;
        threadInfo[channel].asapHead = msgPool[currentMessage].next;
        rt_sem_signal(&threadInfo[channel].asapChainSem);

        /*
         * Now we want to process the message at the head of the asapList
         */
        rt_sem_wait(&threadInfo[channel].channelSem);
        ambMsgProcess(&msgPool[currentMessage].msg);
        rt_sem_signal(&threadInfo[channel].channelSem);

        /*
         * Now place this message block back in the availible list
         */
        rt_sem_wait(&availMsgSem);
        if(availMsgTail != -1)
        {
            msgPool[availMsgTail].next = currentMessage;
        }
        else
        {
            availMsgHead = currentMessage;
        }

        availMsgTail = currentMessage;
        msgPool[currentMessage].next = -1;
        ++msgsOutOfPool;
        rt_sem_signal(&availMsgSem);

        /*
         * Now wait for the next message
         */
        rt_sem_wait(&threadInfo[channel].asapSem);
    }

    rt_sem_signal(&threadInfo[channel].asapDone);

    RTLOG_INFO(modName, "asapThread Exiting: Channel %ld.", channel);

    rt_task_delete(rt_whoami());
}


void teThread(long channel)
{
    Time currentTime;
    int msgIter;
    int startMsg;
    long long iterCounter = 0;
    int last;
    rtlogRecord_t logRecord;    // For the logging channel

    RTLOG_INFO(modName, "teThread started channel %ld.", channel);

    /*
     * Wait for the first teSched semaphore
     */
    rt_sem_wait(&threadInfo[channel].teSem);
    while(atomic_read(&exitFlag) == 0)
    {
        currentTime = teHandlerGetTime() + 10000;    // Add a ms to make up
        // for jitter

        /*
         * Get the resource lock on the teChain and the channel
         */
        rt_sem_wait(&threadInfo[channel].teChainSem);
        rt_sem_wait(&threadInfo[channel].channelSem);

        msgIter = threadInfo[channel].teHead;
        startMsg = msgIter;
        last = -1;

        while(msgIter != -1)
        {
            /*
             * Here we go through executing the list
             */
            if(currentTime < msgPool[msgIter].msg.targetTE)
            {
                break;
            }

            ambMsgProcess(&msgPool[msgIter].msg);
            last = msgIter;
            msgIter = msgPool[msgIter].next;
            ++iterCounter;
        }

        /*
         * Release the semaphore on the channel
         */
        rt_sem_signal(&threadInfo[channel].channelSem);

        /*
         * Move the head of the teChain
         */
        threadInfo[channel].teHead = msgIter;
        if(last != -1)
        {
            msgPool[last].next = -1;
        }

        /*
         * Release the semaphore on the teChain
         */
        rt_sem_signal(&threadInfo[channel].teChainSem);

        if(msgIter != startMsg)
        {
            /*
             * We have executed at least one message return it to the
             * availible list.
             *
             *
             * Get the semaphore on the availible chain.
             */
            rt_sem_wait(&availMsgSem);

            /*
             * Add the executed messages to the chain
             */
            if(availMsgTail != -1)
            {
                msgPool[availMsgTail].next = startMsg;
            }
            else
            {
                availMsgHead = startMsg;
            }

            availMsgTail = last;
            msgsOutOfPool += iterCounter;
            iterCounter = 0;

            /*
             * Release the semaphore on the availible chain
             */
            rt_sem_signal(&availMsgSem);
        }

        /*
         * Now wait on the teSched semaphore
         */
        rt_sem_wait(&threadInfo[channel].teSem);
    }
    /*
     * Disconnect from the teSchedular here
     */
    rt_sem_delete(&threadInfo[channel].teSem);

    /*
     * Flush any remaining requests
     */
    flush(channel, 0, 0, 0);

    rt_sem_signal(&threadInfo[channel].teDone);

    RTLOG_INFO(modName, "teThread stopped channel %ld.", channel);

    rt_task_delete(rt_whoami());
}

void ambMsgProcess(AmbMessage_t* msgIn)
{
    rtlogRecord_t logRecord;    // For the logging channel
    AmbResponse_t msgOut;    // Memory for returning responses

    /**
     * Erase the memoery of the response structure.
     */
    memset(&msgOut, 0, sizeof(AmbResponse_t));

    /*
     * Store the response completion pointer
     */
    msgOut.completion_p = msgIn->completion_p;

    /*
     * Put in the timestamp here
     */
    msgOut.timestamp = teHandlerGetTime();
    msgOut.channel = msgIn->channel;
    msgOut.address = msgIn->address;

    /*
     * Now send the command out
     */
    switch (msgIn->requestType)
    {
        case AMB_RESET:
        {
            /*
             * Reset the specified channel
             */
            resetChannel((int)(msgIn->channel));
            msgOut.status = AMBERR_NOERR;
        }
        break;

        case AMB_MONITOR:
        {
            /*
             * Request data from slave
             */
            msgOut.status = ambMonitor(msgIn->channel, msgIn->address,
                &msgOut.dataLen, msgOut.data);
        }
        break;

        case AMB_CONTROL:
        {
            /*
             * Transmit data to slave
             */
            msgOut.status = ambControl(msgIn->channel, msgIn->address,
                msgIn->dataLen, msgIn->data);
        }
        break;

        case AMB_BLOCK_READ:
        {
            /*
             * Transmit data to slave
             */
            msgOut.status = ambBlockRead(msgIn->channel, msgIn->address,
                msgIn->dataLen, &msgOut, RESPFIFO);
        }
        break;

        case AMB_BLOCK_REQUEST:
        {
            /*
             * Request data from slave
             */
            msgOut.status = ambBlockRequest(msgIn->channel, msgIn->address,
                msgIn->dataLen, msgIn->data, &msgOut, RESPFIFO);
        }
        break;

        case AMB_GET_NODES:
        {
            /*
             * Return all of the nodes on a given channel
             */
            msgOut.status = ambGetNodes(msgIn->channel, &msgOut, RESPFIFO);
        }
        break;

        default:
        {
            msgOut.status = AMBERR_BADCMD;

            RTLOG_ERROR(modName, "Bad Command Request.");
        }
    }

    msgOut.timestamp = teHandlerGetTime();
    msgOut.magic = 0xdeadbeef;

    if(rtf_put(RESPFIFO, &msgOut, sizeof(AmbResponse_t))
    != sizeof(AmbResponse_t))
    {
        RTLOG_ERROR(modName, "Failed to send request to fifo.");
    }

    return;
}

AmbErrorCode_t flush(int channel, AmbAddr mask, AmbAddr address, Time flushTime)
{
    rtlogRecord_t logRecord;    // For the logging channel
    int msgIter;
    int lastFlush;
    int lastGood;
    int flushHead;
    AmbResponse_t msgOut;    // Memory for returning responses

    RTLOG_INFO(modName, "Flushing channel %d.", channel);

    /**
     * Erase the memoery of the response structure.
     */
    memset(&msgOut, 0, sizeof(AmbResponse_t));

    lastFlush = -1;
    lastGood = -1;
    flushHead = -1;

    /*
     * Mask the important portion of the address
     */
    address &= mask;

    /*
     * Grab the semaphore on the chain
     */
    rt_sem_wait(&threadInfo[channel].teChainSem);
    msgIter = threadInfo[channel].teHead;

    /*
     * Go through this list finding the messages that need to be flushed
     */
    while(msgIter != -1)
    {
        if(msgPool[msgIter].msg.targetTE >= flushTime)
        {
            /*
             * Time is correct, check if this should be flushed
             */
            if((msgPool[msgIter].msg.address & mask) == address)
            {
                /*
                 * Remove this from the queue
                 */
                if(lastGood == -1)
                {
                    /*
                     * There are no previous good messages make
                     * the next the head
                     */
                    threadInfo[channel].teHead = msgPool[msgIter].next;
                }
                else
                {
                    /*
                     * Append this to the list of good ones
                     */
                    msgPool[lastGood].next = msgPool[msgIter].next;
                }

                /*
                 * Add this message to the queue to be flushed
                 */
                if(flushHead == -1)
                {
                    flushHead = msgIter;
                }
                else
                {
                    msgPool[lastFlush].next = msgIter;
                }

                lastFlush = msgIter;
                msgIter = msgPool[msgIter].next;
                msgPool[lastFlush].next = -1;
            }
            else
            {
                lastGood = msgIter;
                msgIter = msgPool[msgIter].next;
            }
        }
        else
        {
            lastGood = msgIter;
            msgIter = msgPool[msgIter].next;
        }
    }

    rt_sem_signal(&threadInfo[channel].teChainSem);

    /*
     * Now we want to flush those messages selected above
     */
    msgIter = flushHead;
    while(msgIter != -1)
    {
        /*
         * Send back the message that this has been flushed
         */
        msgOut.timestamp = teHandlerGetTime();
        msgOut.completion_p = msgPool[msgIter].msg.completion_p;
        msgOut.magic = 0xdeadbeef;
        msgOut.status = AMBERR_FLUSHED;

        if(rtf_put(RESPFIFO, &msgOut, sizeof(AmbResponse_t))
        != sizeof(AmbResponse_t))
        {
            RTLOG_ERROR(modName, "Failed to send request to fifo.");
        }

        lastFlush = msgIter;    // Keep this so we know where the end is
        msgIter = msgPool[msgIter].next;
        ++msgsOutOfPool;
    }


    /*
     * Now add the flushed messages back to the avalilible pool
     */
    rt_sem_wait(&availMsgSem);
    if(availMsgTail == -1)
    {
        availMsgHead = flushHead;
    }
    else
    {
        msgPool[availMsgTail].next = flushHead;
    }

    if(lastFlush != -1)
    {
        availMsgTail = lastFlush;
    }

    ++msgsOutOfPool;
    rt_sem_signal(&availMsgSem);

    return AMBERR_NOERR;
}

/*
 * exported API
 */
/**
 * No symbols are exported from here.
 */


/*
 * intialization task
 */
static void initTaskFunction(long flag)
{
    rtlogRecord_t logRecord;
    int status = 0;
    int retryCount = 50;    // Try up to 50 times dispatch(5 s)
    int result = 0;
    int channel;
    int idx;
    int loop;

    atomic_t *flag_p = (atomic_t *) flag;

    RTLOG_INFO(modName, "Initialization task started...");

    /**
      * Initialise each channel.
      */
    for(channel = 0; channel < numAmbChannels; ++channel)
    {
        if(ambChannelInit(channel) != AMBERR_NOERR)
        {
            RTLOG_ERROR(modName, "Initialisation of CAN channel #%d failed. "
                "Continuing the init does not make sense, aborting. "
                "CAMBServer module will not work!",
                channel);

            goto TheEnd;
        }
    }

    /*
     * Here we build the initial block of availible threads
     */
    if((msgPool
        = (AmbMessageList_t*)(rt_malloc(poolSize * sizeof(AmbMessageList_t))))
    == 0)
    {
        RTLOG_ERROR(modName, "Failed to allocate memory for message pool");

        goto TheEnd;
    }

    for(idx = 0; idx < poolSize - 1; ++idx)
    {
        msgPool[idx].next = idx + 1;
    }

    msgPool[poolSize - 1].next = -1;
    availMsgHead = 0;
    availMsgTail = poolSize - 1;

    /*
     * Finally get the Dispatcher running
     */
    if(rtf_create_handler(CMDFIFO, X_FIFO_HANDLER(dispatchSignal)) < 0)
    {
        RTLOG_ERROR(modName, "Unable to attach fifoHandler to command fifo.");

        goto TheEnd;
    }

    /*
     * Now start all of the threads
     */
    for(channel = 0; channel < numAmbChannels; ++channel)
    {
        if(rt_task_resume(&(threadInfo[channel].task_ID_asap)) != 0)
        {
            RTLOG_ERROR(modName, "Could not resume the ASAP task for CAN "
                "channel %d. Aborting init. The CAMBServer will not work!",
                channel);

            goto TheEnd;
        }

        if(rt_task_resume(&(threadInfo[channel].task_ID_te)) != 0)
        {
            RTLOG_ERROR(modName, "Could not resume the TE task for CAN "
                "channel %d. Aborting init. The CAMBServer will not work!",
                channel);

            goto TheEnd;
        }
    }

    for(idx = 0; idx < intPerTE; ++idx)
    {
        teSem[idx] = 0;

        while((teSem[idx] = teSchedGetSem(0, idx * (48 / intPerTE) * 10000, 0))
        == 0)
        {
            --retryCount;
            rt_sleep(nano2count(100000000LL));    // Sleep 100 ms

            if(retryCount == 0)
            {
                /*
                 * Give up we have really failed
                 */
                RTLOG_ERROR(modName, "Error allocating teSchedTE. CAMBServer "
                    "will not work beyond this point!");

                goto TheEnd;
            }
        }
    }

    /*
     * Start off by cycling twice to get "synched"
     */
    for(loop = 0; loop < 2; ++loop)
    {
        for(idx = 0; idx < intPerTE; ++idx)
        {
            rt_sem_wait(teSem[idx]);
        }
    }

    RTLOG_INFO(modName, "Starting to distribute Timing Event Notification");

    idx = 0;    // Keep track of which semaphore we are pending on

    result = rt_sem_wait(teSem[idx]);
    if((result == SEM_ERR) || (result == SEM_TIMOUT))
    {
        /*
         * Error waiting for semaphore this should never happen, but if it
         * does exit cleanly
         */
        RTLOG_ERROR(modName, "Error getting timing semaphore. CAMBServer will "
            "not work.");

        goto TheEnd;
    }

    if(rt_task_resume(&dispatch_ID) != 0)
    {
        RTLOG_ERROR(modName, "Could not resume the dispatch task. Aborting "
            "init. The CAMBServer will not work!");

        goto TheEnd;
    }

    if(rt_task_resume(&dist_ID) != 0)
    {
        RTLOG_ERROR(modName, "Could not resume the TE distribution task. "
            "Aborting init. The CAMBServer will not work!");

        goto TheEnd;
    }

    status = 1;

  TheEnd:
    RTLOG_INFO(modName, "Initialization task exit.");

    atomic_set(flag_p, status);

    rt_task_delete(rt_whoami());
}

/*
 * clean up task
 */
static void cleanUpTaskFunction(long flag)
{
    rtlogRecord_t logRecord;
    AmbResponse_t finalMessage;
    int channel = numAmbChannels - 1;
    int idx = intPerTE - 1;
    int status = 0;
    atomic_t *flag_p = (atomic_t *) flag;

    RTLOG_INFO(modName, "Clean up task started...");

    atomic_set(&exitFlag, 1);        // To tell everything to shut down

    for(; channel >= 0; --channel)
    {
        rt_sem_signal(&threadInfo[channel].asapSem);
        rt_sem_signal(&threadInfo[channel].teSem);
    }

    for(; idx >= 0; --idx)
    {
        if(teSem[idx] != 0)
        {
            rt_sem_signal(teSem[idx]);
        }
    }

    rt_sem_signal(&msgWaitingSem);

    /**
     * Give other tasks 0.1s to finish.
     */
    rt_sleep(nano2count(100000000ULL));

    /**
     * The magic word 0xbeefdead is a signal for the ambInterface response
     * listener to give up and exit.
     */
    finalMessage.magic = 0xbeefdead;
    if(rtf_put(
        RESPFIFO, &finalMessage, sizeof(AmbResponse_t))
    != sizeof(AmbResponse_t))
    {
        RTLOG_ERROR(modName, "Failed to send the termination response to the"
            "response fifo.");
    }

    /**
     * Hopefully it is sufficient to grant the user process 0.1s
     * to receive the message before we continue.
     */
    rt_sleep(nano2count(100000000ULL));

    status = 1;

//  TheEnd:
    RTLOG_INFO(modName, "Clean up task exit.");

    atomic_set(flag_p, status);

    rt_task_delete(rt_whoami());
}

static int firstInitSection(void)
{
    rtlogRecord_t logRecord;

    if(testTewsStructures() != AMBERR_NOERR)
    {
        rtlogRecord_t logRecord;

        RTLOG_ERROR(modName, "ERROR in definition of CAN Structures");

        return RT_TOOLS_MODULE_INIT_ERROR;
    }
    else
    {
        /**
         * Clear the threadInfo structure array. This helps to identify, which
         * channels could successfully be initialised.
         */
        memset(threadInfo, 0, sizeof(AmbThreadInfo) * MAX_NUM_AMB_CHANNELS);
    }

    while(channelOpt[numAmbChannels*2] != '\0')
    {
        switch(channelOpt[numAmbChannels*2])
        {
            case 'e':
            {
                threadInfo[numAmbChannels].deviceID =
                    rtOpen(TPMC816_RT_DEV_DRV_NAME);
                threadInfo[numAmbChannels].deviceType = TPMC816;
            }
            break;

            case 's':
            {
                threadInfo[numAmbChannels].deviceID =
                    rtOpen(AMBSIM_RT_DEV_DRV_NAME);
                threadInfo[numAmbChannels].deviceType = AMBSIM;
            }
            break;

            case 't':
            {
                threadInfo[numAmbChannels].deviceID =
                    rtOpen(TIP903_RT_DEV_DRV_NAME);
                threadInfo[numAmbChannels].deviceType = TIP903;
            }
            break;

            case 'o':
            {
                threadInfo[numAmbChannels].deviceID =
                    rtOpen(TPMC901_RT_DEV_DRV_NAME);
                threadInfo[numAmbChannels].deviceType = TPMC901;
            }
            break;

            default:
            {
                RTLOG_ERROR(modName, "Error parsing channel option string.");

                return RT_TOOLS_MODULE_INIT_ERROR;
            }
        }

        if((channelOpt[(numAmbChannels*2)+1] < 0x30)
        || (channelOpt[(numAmbChannels*2)+1] > 0x39))
        {
            RTLOG_ERROR(modName, "Error parsing channel option string.");

            return RT_TOOLS_MODULE_INIT_ERROR;
        }

        /* Error Checking */
        if(threadInfo[numAmbChannels].deviceID <= 0)
        {
            switch(threadInfo[numAmbChannels].deviceID)
            {
                case -E_RTDD_BUSY:
                {
                    RTLOG_ERROR(modName, "Channel %d Device Driver cannot be "
                        "reopened.", numAmbChannels);
                }
                break;

                case -E_RTDD_DEVICE_NOT_FOUND:
                {
                    RTLOG_ERROR(modName, "Channel %d Device Driver not found.",
                        numAmbChannels);
                }
                break;

                default:
                {
                    RTLOG_ERROR(modName, "Channel %d unrecognized error opening "
                        "device driver.", numAmbChannels);
                }
            }

            return RT_TOOLS_MODULE_INIT_ERROR;
        }

        threadInfo[numAmbChannels].deviceChannel =
            channelOpt[(numAmbChannels*2) + 1] - 0x30;

        /* Initialization of the queue for the asap channel */
        threadInfo[numAmbChannels].asapHead = -1;
        threadInfo[numAmbChannels].asapTail = -1;

        /* Initialization of the queue for the TE channel */
        threadInfo[numAmbChannels].teHead = -1;

        ++numAmbChannels;
    }

    return RT_TOOLS_MODULE_INIT_SUCCESS;
}
static int secondInitSection(void)
{
    rtlogRecord_t logRecord;

    /* RTOS related init stuff. */

    /*
     * Now open the FIFOs
     */
    if(rtf_create(CMDFIFO, 0x10000) < 0)
    {
        RTLOG_ERROR(modName, "Failed to create command fifo.");

        return RT_TOOLS_MODULE_INIT_ERROR;
    }
    else if(rtf_reset(CMDFIFO) != 0)
    {
        RTLOG_ERROR(modName, "Failed to reset command fifo");

        return RT_TOOLS_MODULE_INIT_ERROR;
    }

  /**
   * Create the respFifo for messages going back out to the user space
   */
    if(rtf_create(RESPFIFO, 0x40000) < 0)
    {
        RTLOG_ERROR(modName, "Failed to create response fifo");

        return RT_TOOLS_MODULE_INIT_ERROR;
    }
    else if(rtf_reset(RESPFIFO) != 0)
    {
        RTLOG_ERROR(modName, "Failed to reset response fifo");

        return RT_TOOLS_MODULE_INIT_ERROR;
    }

    return RT_TOOLS_MODULE_INIT_SUCCESS;
}
static int thirdInitSection(void)
{
    int channel = 0;

    rt_typed_sem_init(&availMsgSem, 1, RES_SEM);
    rt_typed_sem_init(&msgWaitingSem, 0, CNT_SEM);

    // Here I initialize the structure for each thread
    for(; channel < numAmbChannels; ++channel)
    {
        rt_typed_sem_init(&(threadInfo[channel].channelSem), 1, RES_SEM);

        rt_typed_sem_init(&threadInfo[channel].asapChainSem, 1, RES_SEM);
        rt_typed_sem_init(&(threadInfo[channel].asapSem), 0, CNT_SEM);

        // Initialize semaphores for the TE related events
        rt_typed_sem_init(&threadInfo[channel].teSem, 0, BIN_SEM);
        rt_typed_sem_init(&threadInfo[channel].teChainSem, 1, RES_SEM);

        /* Initialization of the done semaphores for synchronization */
        rt_typed_sem_init(&threadInfo[channel].asapDone, 0, BIN_SEM);
        rt_typed_sem_init(&threadInfo[channel].teDone, 0, BIN_SEM);
    }

    return RT_TOOLS_MODULE_INIT_SUCCESS;
}

static int fourthInitSection(void)
{
    rtlogRecord_t logRecord;
    int channel;


    for(channel = 0; channel < numAmbChannels; ++channel)
    {
        if(rt_task_init(&(threadInfo[channel].task_ID_asap), asapThread, channel,
            stackSize, ASAP_PRIORITY, 0, 0)
        != 0)
        {
            RTLOG_ERROR(modName, "Could not initialise RTOS ASAP task for "
                "channel %d. The CAMBserver will not work!",
                channel);

            return RT_TOOLS_MODULE_INIT_ERROR;
        }

        if(rt_task_init(&(threadInfo[channel].task_ID_te), teThread, channel,
            stackSize, TE_PRIORITY, 0, 0)
        != 0)
        {
            RTLOG_ERROR(modName, "Could not initialise RTOS TE task for "
                "channel %d. The CAMBserver will not work!",
                channel);

            return RT_TOOLS_MODULE_INIT_ERROR;
        }
     }

    /**
     * Create the task which distributes the TEs to the te handler routines
     */
    if(rt_task_init(&dist_ID, teDist, 0, stackSize, TE_DIST_PRIORITY, 0, 0)
    != 0)
    {
        RTLOG_ERROR(modName, "Could not initialise RTOS distributor task. The "
            "CAMBserver will not work!");

        return RT_TOOLS_MODULE_INIT_ERROR;
    }

    /**
     * Create the task which handles incoming messages
     */
    if(rt_task_init(&dispatch_ID, dispatcher, 0, stackSize, DISPATCH_PRIORITY,
        0, 0)
    != 0)
    {
        RTLOG_ERROR(modName, "Could not initialise RTOS dispatcher task. The "
            "CAMBserver will not work!");

        return RT_TOOLS_MODULE_INIT_ERROR;
    }

    return RT_TOOLS_MODULE_INIT_SUCCESS;
}

static int firstCleanUpSection(void)
{
    /*
     * Close all of the modules I have opened
     */
    while(numAmbChannels > 0)
    {
        --numAmbChannels;
        rtClose(threadInfo[numAmbChannels].deviceID);
    }

    return RT_TOOLS_MODULE_EXIT_SUCCESS;
}

static int secondCleanUpSection(void)
{
    rtlogRecord_t logRecord;
    int status = 0;

    /**
     * RTOS related cleanup stuff.
     */
    if(msgPool != 0)
    {
        rt_free(msgPool);
        msgPool = 0;
    }

    /**
     * Close the FIFOs.
     *
     * Command FIFO.
     * First reset it, then destroy it.
     */
    if(rtf_reset(CMDFIFO) != 0)
    {
        RTLOG_ERROR(modName, "Failed to reset Command FIFO. Continuig with "
            "its destruction. Problems could occur now.");
    }

    do
    {
        status = rtf_destroy(CMDFIFO);
    }
    while(status > 0);

    if(status < 0)
    {
        RTLOG_ERROR(modName, "Error while Command FIFO sestruction! Continuing "
            "with the clean-up anyway.");
    }

    /**
     * Response FIFO.
     * First reset it, then destroy it.
     */
    if(rtf_reset(RESPFIFO) != 0)
    {
        RTLOG_ERROR(modName, "Failed to reset response fifo. Continuig with "
            "its destruction. Problems could occur now.");
    }

    do
    {
        status = rtf_destroy(RESPFIFO);
    }
    while(status > 0);

    if(status < 0)
    {
        RTLOG_ERROR(modName, "Error while Response FIFO sestruction! Continuing "
            "with the clean-up anyway.");
    }

    return RT_TOOLS_MODULE_EXIT_SUCCESS;
}

static int thirdCleanUpSection(void)
{
    int channel = numAmbChannels - 1;

    /**
     * Destroy the per-thread semaphores in recursive order.
     */
    for(; channel >= 0; --channel)
    {
        rt_sem_delete(&(threadInfo[channel].teDone));
        rt_sem_delete(&(threadInfo[channel].asapDone));

        rt_sem_delete(&(threadInfo[channel].teChainSem));
        rt_sem_delete(&(threadInfo[channel].teSem));

        rt_sem_delete(&(threadInfo[channel].asapSem));
        rt_sem_delete(&(threadInfo[channel].asapChainSem));

        rt_sem_delete(&(threadInfo[channel].channelSem));
    }

    rt_sem_delete(&msgWaitingSem);
    rt_sem_delete(&availMsgSem);

    return RT_TOOLS_MODULE_EXIT_SUCCESS;
}

static int fourthCleanUpSection(void)
{
    /**
     * Here the tasks would be deleted, but they delete themselves.
     */

    return RT_TOOLS_MODULE_EXIT_SUCCESS;
}

static int ambServer_main(int stage)
{
    int status;
    int temporaryStatus = RT_TOOLS_MODULE_CLEANUP_ERROR;
    int cleanUpStatus = RT_TOOLS_MODULE_EXIT_SUCCESS;
    RT_TASK initTask, cleanUpTask;
    atomic_t initFlag = ATOMIC_INIT(0);
    atomic_t cleanUpFlag = ATOMIC_INIT(0);
    rtlogRecord_t logRecord;

    if(stage == RT_TOOLS_MODULE_STAGE_INIT)
    {
        status = RT_TOOLS_MODULE_INIT_ERROR;

        /*
         * log RCS ID (cvs version)
         */
        RTLOG_INFO(modName, "%s", rcsId);

        RTLOG_INFO(modName, "initializing module...");

        RTLOG_INFO(modName, "initTimeout  = %d ms", initTimeout);
        RTLOG_INFO(modName, "cleanUpTimeout = %d ms", cleanUpTimeout);
        RTLOG_INFO(modName, "stackSize = %d bytes", stackSize);
        goto Initialization;
    }
    else
    {
        status = RT_TOOLS_MODULE_EXIT_SUCCESS;

        RTLOG_INFO(modName, "Cleaning up module...");

        goto FullCleanUp;
    }

  Initialization:

    if((status = firstInitSection()) != RT_TOOLS_MODULE_INIT_SUCCESS)
    {
        goto Level1CleanUp;
    }

    if((status = secondInitSection()) != RT_TOOLS_MODULE_INIT_SUCCESS)
    {
        goto Level2CleanUp;
    }

    if((status = thirdInitSection()) != RT_TOOLS_MODULE_INIT_SUCCESS)
    {
        goto Level3CleanUp;
    }

    if((status = fourthInitSection()) != RT_TOOLS_MODULE_INIT_SUCCESS)
    {
        goto Level4CleanUp;
    }



    /*
     * initialization of resources is finished. Now spawn the rtai
     * task that takes care of doing the asynchronous part of the
     * initialization.
     */
    if(rt_task_init(&initTask,
        initTaskFunction,
        (long)&initFlag,
        RT_TOOLS_INIT_TASK_STACK,
        RT_TOOLS_INIT_TASK_PRIO, 0, 0)
    != 0)
    {
        RTLOG_ERROR(modName, "Failed to spawn initialization task!");

        status = RT_TOOLS_MODULE_NO_INIT_TASK_ERROR;

        goto Level4CleanUp;
    }
    else
    {
        RTLOG(modName, RTLOG_INFO,
          "Created initialization task, address = 0x%p.", &initTask);
    }

    /*
     * resume the initialization task
     */
    if(rt_task_resume(&initTask) != 0)
    {
        rt_task_delete(&initTask);
        status = RT_TOOLS_MODULE_NO_INIT_TASK_ERROR;

        RTLOG_ERROR(modName, "Cannot resume initalisation task!");

        goto FullCleanUp;
    }

    /*
     * wait for the initialization task to be ready
     */
    if(rtToolsWaitOnFlag(&initFlag, initTimeout) != 0)
    {
        /*
         * okay, the init task was not ready on time, now forcibly remove
         * it for the rtai's scheduler
         */
        rt_task_suspend(&initTask);
        rt_task_delete(&initTask);

        RTLOG(modName, RTLOG_ERROR,
          "Initialization task did not report back on time!");

        status = RT_TOOLS_MODULE_INIT_TIMEOUT;

        goto FullCleanUp;
    }
    else
    {
        /*
         * Waited successfully for the init task to run through.
         */
        status = RT_TOOLS_MODULE_INIT_SUCCESS;
    }

    goto Exit;

  FullCleanUp:
    /*
     * asynchronous clean up phase to be handled by this task
     */
    if(rt_task_init(&cleanUpTask,
            cleanUpTaskFunction,
            (long)&cleanUpFlag,
            RT_TOOLS_CLEANUP_TASK_STACK,
            RT_TOOLS_CLEANUP_TASK_PRIO, 0, 0) != 0)
    {
        RTLOG_ERROR(modName, "Failed to spawn clean up task!");

        /*
         * Set the status to this error only, if the stage is not the init stage.
         * Whenever it happens, that something went wrong with the init task,
         * we are here, too. Then an error in the clean up is not of inerest,
         * because the init failed. This should then be reported.
         */
        cleanUpStatus = RT_TOOLS_MODULE_CLEANUP_ERROR;

        goto Level4CleanUp;
    }
    else
    {
        RTLOG(modName, RTLOG_INFO,
          "Created clean up task, address = 0x%p.", &cleanUpTask);
    }

    /*
     * resume the clean up task
     */
    if(rt_task_resume(&cleanUpTask) != 0)
    {
        rt_task_delete(&cleanUpTask);
        cleanUpStatus = RT_TOOLS_MODULE_CLEANUP_ERROR;

        RTLOG_ERROR(modName, "Cannot resume clean up task!");

        goto Level4CleanUp;
    }

    /*
     * wait for the initialization task to be ready
     */
    if(rtToolsWaitOnFlag(&cleanUpFlag, cleanUpTimeout) != 0)
    {
        /*
         * okay, the init task was not ready on time, now forcibly remove
         * it for the rtai's scheduler
         */
        rt_task_suspend(&cleanUpTask);
        rt_task_delete(&cleanUpTask);

        RTLOG_ERROR(modName, "Clean up task did not report back on time!");

        if(stage != RT_TOOLS_MODULE_STAGE_INIT)
        {
            cleanUpStatus = RT_TOOLS_MODULE_CLEANUP_ERROR;
        }
    }
    else
    {
        if(stage != RT_TOOLS_MODULE_STAGE_INIT)
        {
            cleanUpStatus = RT_TOOLS_MODULE_EXIT_SUCCESS;
        }
    }

  Level4CleanUp:
    temporaryStatus = fourthCleanUpSection();
    if(temporaryStatus != RT_TOOLS_MODULE_EXIT_SUCCESS)
    {
        RTLOG_ERROR(modName, "Failed to run the 4th clean up function! "
            "Continuing anyway with the clean up.");

        if(cleanUpStatus == RT_TOOLS_MODULE_EXIT_SUCCESS)
        {
            cleanUpStatus = temporaryStatus;
        }
    }

  Level3CleanUp:
    temporaryStatus = thirdCleanUpSection();
    if(temporaryStatus != RT_TOOLS_MODULE_EXIT_SUCCESS)
    {
        RTLOG_ERROR(modName, "Failed to run the 3rd clean up function! "
            "Continuing anyway with the clean up.");

        if(cleanUpStatus == RT_TOOLS_MODULE_EXIT_SUCCESS)
        {
            cleanUpStatus = temporaryStatus;
        }
    }

  Level2CleanUp:
    temporaryStatus = secondCleanUpSection();
    if(temporaryStatus != RT_TOOLS_MODULE_EXIT_SUCCESS)
    {
        RTLOG_ERROR(modName, "Failed to run the 2nd clean up function! "
            "Continuing anyway with the clean up.");

        if(cleanUpStatus == RT_TOOLS_MODULE_EXIT_SUCCESS)
        {
            cleanUpStatus = temporaryStatus;
        }
    }

  Level1CleanUp:
    temporaryStatus = firstCleanUpSection();
    if(temporaryStatus != RT_TOOLS_MODULE_EXIT_SUCCESS)
    {
        RTLOG_ERROR(modName, "Failed to run the 1st clean up function! "
            "Continuing anyway with the clean up.");

        if(cleanUpStatus == RT_TOOLS_MODULE_EXIT_SUCCESS)
        {
            cleanUpStatus = temporaryStatus;
        }
    }

  Exit:
    if(stage != RT_TOOLS_MODULE_STAGE_INIT)
    {
        status = cleanUpStatus;
    }

    return status;
}

static int __init ambServer_init(void)
{
    rtlogRecord_t logRecord;
    int status = RT_TOOLS_MODULE_INIT_ERROR;

    if((status = ambServer_main(RT_TOOLS_MODULE_STAGE_INIT))
    == RT_TOOLS_MODULE_INIT_SUCCESS)
    {
        RTLOG_INFO(modName, "Module initialized successfully.");
    }
    else
    {
        RTLOG_ERROR(modName, "Failed to initialize module!");
    }

    return status;
}

static void __exit ambServer_exit(void)
{
    rtlogRecord_t logRecord;

    if(ambServer_main(RT_TOOLS_MODULE_STAGE_EXIT)
    == RT_TOOLS_MODULE_EXIT_SUCCESS)
    {
        RTLOG_INFO(modName, "Module cleaned up successfully.");
    }
    else
    {
        RTLOG_ERROR(modName, "Failed to clean up module!");
    }
}

module_init(ambServer_init);
module_exit(ambServer_exit);
