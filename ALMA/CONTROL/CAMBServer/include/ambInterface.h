#ifndef AMBINTERFACE_H
#define AMBINTERFACE_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2003-12-17  created
*/

/************************************************************************
 * This file defines the ambInterface class.  This is a singleton class
 * which is the interface between the Obj Oriented world of user space
 * and the C world of RTAI.  All communications to the AMB bus must go
 * through this part.
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <ambDefs.h>
#include <ControlExceptions.h>

class AmbInterface {
 public:
  static const AmbInterface* getInstance()
    throw(ControlExceptions::CAMBErrorExImpl);
  static void          deleteInstance();

  /**
   * This method sends the message to the real time system if te=0 then
   * the command is executed asap, otherwise the command is executed at
   * the specified TE.  If the specified time does not correspond to a
   * timing event the event is executed at the next timing event.
   */
  void                 sendMessage(const AmbMessage_t& msg) const
    throw(ControlExceptions::CAMBErrorExImpl);

  AmbErrorCode_t       findSN(unsigned char  serialNumber[],
			      AmbChannel     channel,
			      AmbNodeAddr    nodeAddress,
			      Time&          timestamp) const
    throw(ControlExceptions::CAMBErrorExImpl);


  /**
   * This method is a work around because of the bug in the early
   * version of the AMBSI-1.  As soon as all AMBSI-1 Devices have
   * been updated to respond to node ID requests at RCA 0 this 
   * method will be deleted.
   */
  AmbErrorCode_t       findSNUsingBroadcast(unsigned char  serialNumber[],
					    AmbChannel     channel,
					    AmbNodeAddr    nodeAddress,
					    Time&          timestamp) const
    throw(ControlExceptions::CAMBErrorExImpl); 

  AmbErrorCode_t       flush(AmbChannel      channel,
			     Time            flushTime,
			     Time&           timestamp) const
    throw(ControlExceptions::CAMBErrorExImpl);

  AmbErrorCode_t       flush(AmbChannel      channel,
			     AmbNodeAddr     nodeAddress,
			     Time            flushTime,
			     Time&           timestamp) const
    throw(ControlExceptions::CAMBErrorExImpl);

  AmbErrorCode_t       flush(AmbChannel      channel,
			     AmbNodeAddr     nodeAddress,
			     AmbRelativeAddr RCA,
			     Time            flushTime,
			     Time&           timestamp) const
    throw(ControlExceptions::CAMBErrorExImpl);

 private:
  /* Constructor and Destructor are private because 
   * this is a singleton class
   */
  
  AmbInterface()     throw(ControlExceptions::CAMBErrorExImpl);
  ~AmbInterface();

  /** 
      This method is really part of the Constructor, but is moved to a 
      subroutine to force the initialization in the correct order
  */
  void staticConstructor()  throw(ControlExceptions::CAMBErrorExImpl);

  /* Prevent use of the copy and assignment operator */
  AmbInterface(const AmbInterface&);
  AmbInterface& operator= (const AmbInterface&);

  /**
   * The replyThread method blocks on the replyFIFO and processes the
   * replies as they are returned to user space.
   * The argument is not used, just defined to make the compiler happy
   * defined static so the compiler can find it
   */
  static void* replyThread(void*); 

  /* This routine just copies info into the response locations*/
  static void fillCompletion(AmbResponse_t&);

  /**
   * This function sends the message to close the server cleanly
   */
  void ambServerShutdown(void) throw(ControlExceptions::CAMBErrorExImpl); 

  static AmbInterface*    instance_mp;
  static pthread_mutex_t  instanceLock_m;
  static sem_t            semCount_m; 

  static int              shutdownFlag_m; 

  mutable pthread_mutex_t cmdFifoMtx_m;// Mutex protection for cmdFifo
  int                     cmdFifoFd_m;// FIFO for processing CMD requests
  pthread_t               replyTID_m;    // Thread ID for reply thread
};

/**
 * This utility is used to translate Node and RCA addresses to a complete
 * address, this is used in the other AMBsystem modules and is stored here
 * since they all load this library as well
 */

inline AmbAddr createAMBAddr(const AmbNodeAddr&     baseAddress,
			     const AmbRelativeAddr& relativeAddress) {
  return ((baseAddress + 1) << 18) | relativeAddress;
}
#endif /*!AMBINTERFACE_H*/
