#ifndef AMBSERVERINTERNALS_H_
#define AMBSERVERINTERNALS_H_

/*
 * @(#) $Id$
 */

#ifdef __cplusplus
  extern "C" {
#endif

#ifdef __KERNEL__
/*
 * used for logging
 */
#define modName "ambServer"


/* Forward definitions */
void asapThread(long);
void teThread(long);
void teDist(long);
int dispatchSignal(unsigned int fifo, int rw);
void ambMsgProcess(AmbMessage_t* msgIn);
AmbErrorCode_t flush(int channel, AmbAddr mask, AmbAddr address, Time flushTime);

const unsigned long AMB_NODE_BASE_ADDR = 0x40000l;

AmbThreadInfo threadInfo[MAX_NUM_AMB_CHANNELS];
#endif // __KERNEL__

#ifdef __cplusplus
}
#endif // __cplusplus

#endif /*AMBSERVERINTERNALS_H_*/
