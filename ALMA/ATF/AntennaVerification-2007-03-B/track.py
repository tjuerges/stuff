#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# tjuerges  Mar 5, 2007  created
#

import optparse
import sys
import traceback
import string
import datetime
import time
import math
import re
from Acspy.Clients.SimpleClient import PySimpleClient
import Control

def parseHorizonsOutput(content):
	# Format:
	# Bla
	#$$SOE
	# 2007-Mar-06 22:56:53.505,*, ,327.0117222,-11.2652451,    -8.06,   -39.40,
	# 2007-Mar-06 22:57:53.505,*, ,327.0116841,-11.2654274,    -8.04,   -39.39,
	#[...]
	#$$EOE
	# Bla

	print "Parsing the ephemeris data..."
	positions = []
	startString = "$$SOE"
	endString = "$$EOE"

	start = content.find(startString)
	if start == -1:
		print "Could not find then start of ephemeris data. Check the input!"
		return positions
	else:
		# Adjust the index to where data actiually starts.
		start = start + len(startString) + 1
		print "Found start of data at index %d" % start

	end = content.find(endString)
	if end == -1:
		print "Could not find the end of ephemeris data. Check the input!"
		return positions
	else:
		# Adjust the index to where data actiually ends.
		print "Found end of data at index %d" % end

	output = content[start:end]
	line_by_line = output.split("\r\n")
	line_by_line.pop()
	print "Found %d lines which contain ephemeris information." % len(line_by_line)
	for single_line in line_by_line:
		tokens = single_line.split(",")
		# Each line consists of eight fields.
		if len(tokens) != 8:
			print "It seems, that the ephemeris file does not contain enough information."
			print "Found only %d data fields when 8 were expected." % len(tokens)
			print "The ephemeris section should look like this:"
			print " 2007-Mar-06 22:56:53.505,*, ,327.0117222,-11.2652451,    -8.06,   -39.40,"
			print "Make sure to select fields #1 and #3 (R.A., Dec. and R.A. rate, Dec. rate)!"
			print "Aborting!"
			return positions
		timestamp = tokens[0].lstrip()
		ra = float(tokens[3])
		dec = float(tokens[4])
		# Positions are in degrees. Convert them to radians.
		raRad = ra / 180.0 * math.pi
		decRad = dec / 180.0 * math.pi
		# Alpha rate and Delta rate are in "/hr.
		# The alpha rate is actually multiplied with cos(delta).
		# So divide it by cos(delta).
		raRate = float(tokens[5]) / math.cos(dec)
		# Convert it to "/s.
		raRate = raRate / 3600.0
		decRate = float(tokens[6]) / 3600.0
		# Convert it to degree/s.
		raRate = raRate / 3600.0
		decRate = decRate / 3600.0
		# Convert it to rad/s.
		raRate = raRate / 180.0 * math.pi
		decRate = decRate / 180.0 * math.pi
		positions.append((timestamp, raRad, decRad, raRate, decRate))

	print "Done."
	return positions


def readEphemerisFromFile(filename):
	print "Reading ephemeris data from file %s..." % filename
	f = file(filename, "r")
	lines = f.read()
	f.close()
	print "Done."
	return lines



def distance(x,y):
	return (x**2+y**2)**0.5


def decdegToDeg(x):
	deg = []
	temp = x
	deg.append(int(temp))
	temp = temp - int(temp)
	temp = temp * 60.0
	deg.append(int(temp))
	temp = temp - int(temp)
	temp = temp * 60.0
	deg.append(temp)
	return deg


def moveTo(mc, x0, y0, x1, y1, dwellTime, step):
	while (x0 != x1) or (y0 != y1):
		if (x1 > x0):
			x0 += 1
		elif (x1 < x0):
			x0 -= 1
		if (y1 > y0):
			y0 += 1
		elif (y1 < y0):
			y0 -= 1
        mc.setOffsetAzEl(math.radians(x0 * step) / math.cos(el), math.radians(y0 * step))
        time.sleep(dwellTime)
	deg = []
	azDeg = decdegToDeg(x0 * step)
	elDeg = decdegToDeg(y0 * step)
	print "Offset by (az = %2d:%2d:%2.2f, el = %2d:%2d:%2.2f) degrees." \
	% (azDeg[0], azDeg[1], azDeg[2], elDeg[0], elDeg[1], elDeg[2])
	return (x0, y0)

def calcTimeRemaining(targetTime):
	now = datetime.datetime.utcnow()
	delta = targetTime - now
	return delta

def track(antenna, positions, dwellTime = 0.0, maxDist = 0.0, step = 0.0):
	# Check if a spiral search shall be done.
	# It will only be done if and only if
	# dwellTime and
	# maxDist and
	# step
	# are not 0.
	doSearch = False
	if dwellTime != 0.0 and maxDist != 0.0 and step != 0.0:
		doSearch = True

	client = PySimpleClient()
	mc = client.getComponent("CONTROL/" + antenna + "/MountController")
	raOffsetProperty = mc._get_offsetRA()
	decOffsetProperty = mc._get_offsetDec()

	# Now start the loop
	firstTime = True
	try:
		for pos in positions:
			timestamp = re.sub("\.\d{3,3}$", "", pos[0])
			targetTime = datetime.datetime(*time.strptime(timestamp, "%Y-%b-%d %H:%M:%S")[0:6])
			delta = calcTimeRemaining(targetTime)
			remainingTime = 0.0

			if delta.days < 0:
				# This timestamp is in the past. Skip it.
				continue
			else:
				# The timstamp is in the future. Sleep until then.
				remainingTime = delta.days * 86400 + delta.seconds
				if firstTime == True:
					print "Will sleep %ds until the next timestamp %s." % (remainingTime, timestamp)
					time.sleep(remainingTime)
					firstTime = False
				else:
					x = 0
					y = 0
					turn = 1
					mystep = 1
					delta = calcTimeRemaining(targetTime)
					remainingTime = delta.days * 86400 + delta.seconds
					# Have a cushion for the remaining time og 0.1s.
					# Just in case that the function calls take very long.
					while (remainingTime > dwellTime + 0.1) and (distance(x * step, y * step) <= maxDist):
						if (turn % 4 == 1):
							(x,y) = moveTo(mc, x, y, x + mystep, y, dwellTime, step)
						elif (turn%4 == 2):
							(x, y) = moveTo(mc, x, y, x, y + mystep, dwellTime, step)
							mystep += 1
						elif (turn%4 == 3):
							(x, y) = moveTo(mc, x, y, x - mystep, y, dwellTime, step)
						elif (turn%4 == 0):
							(x, y) = moveTo(mc, x, y, x, y - mystep, dwellTime, step)
							mystep += 1
						turn += 1
						(raOffset, completion) = raOffsetProperty.get_sync()
						(decOffset, completion) = decOffsetProperty.get_sync()
						print "Offsets are set to (%.12f; %.12f)rad." % (raOffset, decOffset)
						delta = calcTimeRemaining(targetTime)
						remainingTime = delta.days * 86400 + delta.seconds
			print "Tracking now on (%.12f; %.12f)rad with rate (%.12f; %.12f)rad/s." % (pos[1], pos[2], pos[3], pos[4])
			mc.sourceEquatorial(pos[1], pos[2], 2000.0, 0.0, 0.0, 0.0, 0.0, Control.Mean, Control.NearestAz, Control.UnderTheTop)
			mc.sourceEquatorialUpdate(pos[3], pos[4])
	except KeyboardInterrupt, ex:
		print "Tracking stopped. Good bye!"
	except:
		ex = sys.exc_info()
		traceback.print_exception(ex[0], ex[1], ex[2])

	client.releaseComponent(mc._get_name())
	client.disconnect()


if __name__ == "__main__":
	antenna = ""
	timeStep = 1.0

	parser = optparse.OptionParser()
	parser.add_option("-f", "--file", dest = "filename", default = "0",
					type = "string",
					help = "Epehemeris file name.",
					metavar = "FILE")

	parser.add_option("-a", "--antenna", dest = "antenna", default = "ALMA01",
					type = "string",
					help = "Antenna to use for tracking.",
					metavar = "ANTENNA")

	parser.add_option("--search", dest = "doSearch", default = "no",
					type = "string",
					help = "Do a spiral search? Default: %default.",
		metavar = "YESNO")

	parser.add_option("-t", "--time", dest = "dwellTime", default = 2,
					type="float",
					help = "How long to dwell at each position. Default: %default seconds.",
					metavar = "DWELLTIME")

	parser.add_option("-m", "--max", dest="maxDist", default = 4.0,
					type="float",
					help="How far out to search [arcmin]. Default: %default arcmin.")

	parser.add_option("-s", "--step", dest="step", default = 55.0, 
                  type="float", 
                  help="How far to step for each new position [arcsec]. Default %default arcsec.")

	(options, args) = parser.parse_args()
	if options.filename == "0":
		parser.error("Incorrect number of arguments. Provide at least the ephemeris file name! Try option --help or -h.")
		sys.exit(-1)

	antenna = options.antenna
	output = readEphemerisFromFile(options.filename)
	positions = []
	positions = parseHorizonsOutput(output)
	doSearch = string.lower(options.doSearch)
	dwellTime = options.dwellTime
	maxDist = options.maxDist / 60.0
	step = options.step / 3600.0

	# Only start tracking if there are enough data points. 
	if len(positions) > 0:
		print("\nNow beginning the track loop. Interrupt with CTRL-C.")
		if doSearch == "no":
			track(antenna, positions)
		else:
			track(antenna, positions, dwellTime, maxDist, step)
		print "Exiting track program."
