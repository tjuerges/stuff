# $Id$
#-------------------------------------------------------------------------
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2004, 2005
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
#-------------------------------------------------------------------------
# This is the default configuration file for the CAMBServer, as it is
# implemented for the ABMs and ARTMs in the PSIL Lab.  I have put in
# notes below on how to customize this for other applications.
# Jeff Kern 9/21/04
#
# This file is actually a modified default version which loads the
# serial interface kernel modules. The ARTM talks via the serial interface
# to the GPS hardware.
#
# It contains additional NTP magic which starts/stops the NTP server as
# real Array Time server or as client.
#-------------------------------------------------------------------------
LOAD SEQUENCE:
MODULE serial_core
MODULE 8250
MODULE 8250_pci
MODULE rtai_hal
MODULE rtai_up
MODULE rtai_fifos
MODULE rtai_sem
MODULE rtai_mbx
MODULE rtLog
MODULE rtTools
MODULE rtDevDrv

# These three modules are only needed if the hardware TEs are to be used
# If we are using hardware TEs we want to stop ntpd so they don't conflict
MODULE vme_universe_rtai
MODULE avme9670drv
MODULE DEParallelDrv

# Stop the ntp server.
SYSTEM service ntpd stop

# Load the 901 driver
MODULE tpmc901drv

# Enable the teHandler is HARD mode by specifing a hardware module
# Give the teHandler a chance to get Synched before starting the teSched
MODULE teHandler hwMod="DEParallelDrv" maxJitClip=100 maxStd=-50

MODULE teSched

# ambServer now takes 2 different parameters
# the first intPerTE is an integer which specifies when in relation to the
# TE the module wakes.  By default intPerTE=2 which means that it wakes at the
# TE and 24 ms after.
# the second parameter is channelOpt which has the following meaning
#
# channelDefinition:
# This string defines the mapping from hardware to channel numbers.
# A channel is defined by 2 characters the first is the device type:
#   e -> TMPC816  That is "e" as in eight
#   o -> TIP901   That is "o" as in one
#   t -> TCIP903  That is "t" as in three
#   s -> AMBSIM   s is for simulator
#  The second character is a digit which specifies the device specific channel
#  number (i.e. the hardware port)
#
# I'm not terribly fond of this mapping if someone has a better suggestion
# please let me know.
#
# The unit for the timeout values is us!
# Defaults: CANReadTimeout="480" CANWriteTimeout="480".
#
# by default channelOpt="e0e1";
# This is if you only have a TPMC816 installed
#MODULE ambServer channelOpt="e0e1"
# Instead use the following if you have both a TPMC816 and a TIP903 installed
#MODULE ambServer channelOpt="e0e1t0t1t2"
# Instead use the following if you have a TPMC901 installed
MODULE ambServer channelOpt="o0o1o2o3o4o5" CANReadTimeout="2000" CANWriteTimeout="2000"

# By now the NTP server can be started again. This time publishing
# Array Time. Relink /etc/ntp.conf for this purpose.
SYSTEM if [ -n /etc/ntp.conf ]; then rm -f /etc/ntp.conf; ln -s /etc/ntp-teHandler.conf /etc/ntp.conf; service ntpd start; else echo "Could not restart NTP. Check /etc/ntp.conf!"; fi

#-----------------------------------------------------------------------
UNLOAD SEQUENCE:
# Stop the NTP server which publishes the Array Time.
SYSTEM service ntpd stop

MODULE ambServer
MODULE teSched
MODULE teHandler

#Unload the dummy driver
MODULE tpmc901drv

# Start ntpd with normal NTP server configuration (gns). By now
# the NTP server can be started again. This time receiving the
# Array Time from gns which keeps the Array Time locally.
# Relink /etc/ntp.conf for this purpose.
SYSTEM if [ -n /etc/ntp.conf ]; then rm -f /etc/ntp.conf; ln -s /etc/ntp-no-teHandler.conf /etc/ntp.conf; service ntpd start; else echo "Could not restart NTP. Check /etc/ntp.conf!"; fi
MODULE DEParallelDrv
MODULE avme9670drv
MODULE vme_universe_rtai

MODULE rtDevDrv
MODULE rtTools
MODULE rtLog
MODULE rtai_mbx
MODULE rtai_sem
MODULE rtai_fifos
MODULE rtai_up
MODULE rtai_hal
MODULE 8250_pci
MODULE 8250
MODULE serial_core
