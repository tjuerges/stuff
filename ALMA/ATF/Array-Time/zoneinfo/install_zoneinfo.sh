#! /bin/bash
#
# $Id$

if [ "x$USER" != "xroot" ]; then
	echo "This script must be run with root privileges!"
	exit -1
fi

mv /etc/localtime /etc/localtime.non-ALMA \
&& cp `dirname $0`/Etc/TAI /etc/localtime \
&& chown root.root /etc/localtime \
&& echo "date should now report TAI." && date && exit 0

exit -1
