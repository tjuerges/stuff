#! /usr/bin/env python
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# import the module containing the python/C++ wrapper for the asdm classes.
from alma.asdm import asdm as asdm

#
# After this import statement one has to prefix with "asdm." any class name related to the ASDM as it's defined in C++.
#
# Examples :
# ds = asdm.ASDM()
# antennaId = asdm.Tag(1, asdm.TagType.Antenna())
# print asdm.ConversionException.getMessage()
#
# Very important note !!!
# The Enumerations are known under the namespace asdm, i.e. any reference to one of those Enumerations must be prefixed by "asdm."
#
# Examples :
# print asdm.TimeSampling.INTEGRATION()
# if ant.antennaMake() == asdm.AntennaMake.GROUND_BASED() : ....
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#
# how to create an ASDM in memory from its disk representation.
#

import sys
#
# The path to the ASDM is expected in argv[1]
#
ds = asdm.ASDM();
if (sys.argv.count < 2) :
    print "Usage : python forRalph.py <path-to-ASDM>"
    exit(1)

name = sys.argv[1];

try :
    ds.setFromFile(name)
except asdm.ConversionException, e:  # Note that  asdm.ConversionException class is known
    print e.getMessage()
    exit (1)



#
# Access the Pointing table.
#
pointingT = ds.pointingTable()

#
# Log informations about pointingT
#
print "The Pointinag table contains " , pointingT.size(), " rows."
if pointingT.size == 0:
    exit (0)

for pointing in pointingT.get() :
    print "TimeOrigin = " , pointing.timeOrigin()
    print "Target = ", pointing.target()[0][0], "," , pointing.target()[0][1]
    print "Offset = ", pointing.offset()[0][0], ",", pointing.offset()[0][1]
    print "PointingDirection = ", pointing.pointingDirection()[0][0], ",", pointing.pointingDirection()[0][1]
    print "Encoder = ", pointing.encoder()[0], ", " , pointing.encoder()[1]
    if pointing.isSourceOffsetExists():
        print "SourceOffset = ", pointing.sourceOffset()[0][0], ", ", pointing.sourceOffset()[0][1]

