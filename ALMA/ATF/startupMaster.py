#!/usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import rlcompleter
import readline
readline.parse_and_bind("tab: complete")
import Acspy.Clients.SimpleClient
import Acspy.Common.QoS
import TMCDB_IDL;
import asdmIDLTypes;
import optparse
import string

parser = optparse.OptionParser()
parser.add_option("-a", "--antenna", dest = "antenna", default = "ALMA01",
	help = "Antenna to use for tracking.", metavar = "ANTENNA")
parser.add_option("-u", "--useTrunk", dest = "useTrunk", default = "no",
	help = "Use TRUNK version of TMCDB. Default=no.", metavar = "YESNO")
(options, args) = parser.parse_args()

antennaName = options.antenna
useTrunk = False
if string.lower(options.useTrunk) == "yes":
	useTrunk = True

client = Acspy.Clients.SimpleClient.PySimpleClient.getInstance()
master = client.getComponent('CONTROL/MASTER')
Acspy.Common.QoS.setObjectTimeout(master, 60000)
if (str(master.getMasterState()) == 'INACCESSIBLE'):
	mcConfig = TMCDB_IDL.AssemblyLocationIDL("MountController & Mount", \
											 "MountController", 0, 0, 0)
	sai = TMCDB_IDL.StartupAntennaIDL(antennaName, "PAD1", "", [], \
									  [mcConfig])
	tmcdb = client.getDefaultComponent("IDL:alma/TMCDB/TMCDBComponent:1.0");
	tmcdb.setStartupAntennasInfo([sai])
	ai = TMCDB_IDL.AntennaIDL(0, 0, antennaName,  "", \
							  asdmIDLTypes.IDLLength(12), \
							  asdmIDLTypes.IDLArrayTime(0), \
							  asdmIDLTypes.IDLLength(0.0), \
							  asdmIDLTypes.IDLLength(0.0), \
							  asdmIDLTypes.IDLLength(0.0), \
							  asdmIDLTypes.IDLLength(0.0), \
							  asdmIDLTypes.IDLLength(0.0), \
							  asdmIDLTypes.IDLLength(0.0), 0, 0, 0)
	if useTrunk == True:
		tmcdb.setAntennaInfo(antennaName, ai)
	else:
		tmcdb.setAntennaInfo(ai)
	pi = TMCDB_IDL.PadIDL(0, 0, "", asdmIDLTypes.IDLArrayTime(0), \
						  asdmIDLTypes.IDLLength(0), \
						  asdmIDLTypes.IDLLength(0), \
						  asdmIDLTypes.IDLLength(0))
	if useTrunk == True:
		tmcdb.setAntennaPadInfo(antennaName, pi)
	else:
		tmcdb.setAntennaPadInfo(pi)
	master.startupPass1()
	master.startupPass2()
	client.releaseComponent(tmcdb._get_name());
