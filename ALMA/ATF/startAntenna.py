#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# tjuerges  Mar 15, 2007  created
#

import rlcompleter
import readline
readline.parse_and_bind("tab: complete")
import optparse
import sys
import time
import traceback
from Acspy.Clients.SimpleClient import PySimpleClient
import Control

parser = optparse.OptionParser()
parser.add_option("-a", "--antenna", dest = "antenna", default = "ALMA01",
	help = "Antenna to use for tracking.", metavar = "ANTENNA")

(options, args) = parser.parse_args()
antenna = options.antenna

client = PySimpleClient.getInstance()
mount = client.getComponent("CONTROL/" + antenna + "/MountController/Mount")
setAzAxisMode = mount._get_setAzAxisMode()
setElAxisMode = mount._get_setElAxisMode()
mode = Control.STANDBY_MODE
try:
	print "Set axis modes to standby..."
	completion = setAzAxisMode.set_sync(mode)
	if completion.code != 0:
		print completion.code

	completion = setElAxisMode.set_sync(mode)
	if completion.code != 0:
		print completion.code

	time.sleep(2)

	print "Set axis modes to track..."
	mode = Control.TRACK_MODE
	completion = setAzAxisMode.set_sync(mode)
	if completion.code != 0:
		print completion.code

	completion = setElAxisMode.set_sync(mode)
	if completion.code != 0:
		print completion.code

	time.sleep(2)

	print "Set equatorial coordinates..."
	mount.objstar(0.12345678, 1.87654321, 2000.0, 0.0, 0.0, 0.0, 0.0, Control.MEAN, Control.NEARESTAZ, Control.UNDERTHETOP)

	time.sleep(2)

	print "Set equatorial offsets..."
	mount.offsetEquatorial(1.5, 0.5)
except:
		ex = sys.exc_info()
		traceback.print_exception(ex[0], ex[1], ex[2])

#client.releaseComponent(mount._get_name())
#client.disconnect()

