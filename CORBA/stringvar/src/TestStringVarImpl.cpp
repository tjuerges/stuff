/*
 * "@(#) $Id$"
 *
 * $Log$
 *
 */

#include <TestStringVarImpl.h>



stringvarImpl::stringvarImpl(const ACE_CString& name, 
		                             maci::ContainerServices* containerServices)
  : ACSComponentImpl(name, containerServices)


{

}


stringvarImpl::~stringvarImpl()
{
}


void stringvarImpl::initialize(void)
	throw (acsErrTypeLifeCycle::acsErrTypeLifeCycleExImpl)
{

}


void stringvarImpl::execute(void)
	throw (acsErrTypeLifeCycle::acsErrTypeLifeCycleExImpl)
{

}


void stringvarImpl::cleanUp(void)
{

}


void stringvarImpl::aboutToAbort(void)
{

}


/* ------------------ [ Functions ] ----------------- */

char* stringvarImpl::testMethod() throw(CORBA::SystemException)
{
	/**
	 * If you are using a classic C++ compiler the following 2 statements
	 * are wrong. In classic C++ the type of a string literal is char *,
	 * not const char * like in standard C++. The CORBA::String_var will
	 * take ownership of the passed string and when destroyed will invoke
	 * string_free trying to free non-heap memory which in many implementations
	 * will cause a core dump.
	 */
	//CORBA::String_var ret_value("OK");
	//CORBA::String_var ret_value = "OK";

	
	/**
	 * These 4 sentences are always right. The first 2 force a deep copy of 
	 * hte string and the last 2 create a explicit copy of the string. Whenever
	 * possible use one of the first 2 ways because they are more efficient.
	 */
	//CORBA::String_var ret_value((const char *)"OK");
	//CORBA::String_var ret_value = (const char *)"OK";
	//CORBA::String_var ret_value = CORBA::string_dup("OK");
	CORBA::String_var ret_value(CORBA::string_dup("OK"));


	/**
	 * This compiles but memory will be incorrectly deallocated twice, once by 
	 * the destructor of the variable and a second time by the caller.
	 * Your component will crash.
	 */
	//return ret_value;

	
	/**
	 * The _retn member function returns the pointer held by a String_var
	 * and also yields ownership of the string making the caller responsible
	 * for freeing it.
	 */
	return ret_value._retn();

	
	/**
	 * The following piece of code compiles but if an exception occurs the
	 * dynamically allocated memory by string_dup will not be freed and you
	 * will be leaking memory. To avoid this use always CORBA::String_var
	 * variables which will free the allocated memory when they go out of scope.
	 */
	//char * ret_value2;
	//ret_value2 = CORBA::string_dup("OK");
	// if_exception_then_memory_leak
	//return ret_value2;
}




/* --------------------- [ CORBA interface ] ----------------------*/




/* --------------- [ MACI DLL support functions ] -----------------*/

#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(stringvarImpl)
