#include <maciSimpleClient.h>
#include <stringvarC.h>

using namespace maci;

/*******************************************************************************/
    
int main(int argc, char *argv[])
{
    // Creates and initializes the SimpleClient object
    SimpleClient client;
    if (client.init(argc,argv) == 0)
	{
	return -1;
	}
    else
	{
	//Must log into manager before we can really do anything
	client.login();
	}
   
    //Get the specific component we have requested on the command-line
    TestStringVar::stringvar_var foo = client.getComponent<TestStringVar::stringvar>(argv[1], 0, true);
   
    //Call the method existing in the interface
    for (unsigned int i = 0; i < 100; i++)
    {
	    /**
	     * The string returned by the interface method is allocated dynamically so you are supposed to free it
	     * otherwise you will be leaking memory.
	     */

	    /**
	     * Using a CORBA::String_var variable is always safe. No memory leaks here.
	     */
	    CORBA::String_var bar = foo->testMethod();

	    /**
	     * This setence causes a memory leak even if we don't assign the returned string to any variable.
	     */
	    //foo->testMethod();
    }
    
    //We release our component and logout from manager
    client.releaseComponent(argv[1]);
    client.logout();
    
    //Sleep for 3 sec to allow everytihng to cleanup and stablize
    ACE_OS::sleep(3);
    return 0;
}

/*___oOo___*/

