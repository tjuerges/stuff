#ifndef TestStringVarImpl_h
#define TestStringVarImpl_h

/*
 * "@(#) $Id$"
 *
 * $Log$
 *
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <acscomponentImpl.h>


#include <stringvarS.h>


using namespace acscomponent;


class stringvarImpl : public virtual acscomponent::ACSComponentImpl,
                        public virtual POA_TestStringVar::stringvar

{
	public:
		/**
	 	 * Constructor
	   * @param containerServices ContainerServices which are needed for 
		 * various component related methods.
	   * @param name component name
	   */
		stringvarImpl(const ACE_CString& name, 
				            maci::ContainerServices* containerServices);

		/**
	 	 * Destructor
	 	 */
		virtual ~stringvarImpl();

		
/* ------------------- [ Lifecycle START interface ] --------------------- */

		/**
	 	 * Lifecycle method called at component initialization state.
	 	 * Overwrite it in case you need it.
	 	 */
		virtual void initialize(void) throw (acsErrTypeLifeCycle::acsErrTypeLifeCycleExImpl);

		/**
	 	 * Lifecycle method called at component execute state.
	 	 * Overwrite it in case you need it.
	 	 */
		virtual void execute(void) throw (acsErrTypeLifeCycle::acsErrTypeLifeCycleExImpl);

		/**
	 	 * Lifecycle method called at component clean up state.
	 	 * Overwrite it in case you need it.
	 	 */
		virtual void cleanUp(void);

		/**
	   * Lifecycle method called at component abort state.
	   * Overwrite it in case you need it.
	   */
		virtual void aboutToAbort(void);

/* ------------------- [ Lifecycle END interface ] --------------- */

/* --------------------- [ CORBA START interface ] ----------------*/

	virtual char* testMethod() throw(CORBA::SystemException);


/* --------------------- [ CORBA END interface ] ----------------*/

/* ----------------------------------------------------------------*/

	private:
		/**
		 * Copy constructor is not allowed following the ACS desgin rules.
		 */
		stringvarImpl(const stringvarImpl&);

		/**
	 	 * Assignment operator is not allowed due to ACS design rules.
	 	 */
		void operator=(const stringvarImpl&);


/* --------------------- [ Constants START ] ----------------------*/

/* --------------------- [ Constants END ] ------------------------*/

/* --------------------- [ Properties START ] ----------------------*/


/* --------------------- [ Properties END ] ------------------------*/

/* --------------------- [ Local properties START ] ----------------*/

/* --------------------- [ Local properties END ] ------------------*/
};


#endif /* TestStringVarImpl_h */

