#include <iostream>
#include <array>
#include <omniORB4/CORBA.h>


static void do_stuff(void)
{
    // The strings that will be passed around.
    const std::array< std::string, 2 > input_string{"Hello", "World!"};

    // Create a StringSeq (sequence< string >) on the heap.
    CORBA::StringSeq* string_sequence_in{new CORBA::StringSeq};
    // Resize from 0 to the size of the input_string.
    string_sequence_in->length(input_string.size());

    // Assign the input strings to the StringSeq elements.
    std::size_t index{0U};
    for(const auto& str: input_string)
    {
        // The assignment would make a deep copy, but I replicate here
        // the implementation that I have seen in Tango Controls.
        (*string_sequence_in)[index++] = CORBA::string_dup(str.c_str());
    }

    // Let's see how the strings in the sequence look like.
    std::cout << "string_sequence_in: "
        << (*string_sequence_in)[0]
        << ", "
        << (*string_sequence_in)[1]
        << "\n";

    // Create an Any on the stack.
    CORBA::Any any_string_seq;

    // The <<= operator would make a deep copy if the insertion were by
    // reference. But here it is by pointer and hence the <<= operator
    // transfers ownership to the Any.
    any_string_seq <<= string_sequence_in;

    // Create another StringSeq to extract the content of the Any to.
    // The data in the StringSeq is read-only and must not be released by the
    // StringSeq.
    CORBA::StringSeq* string_sequence_out;
    any_string_seq >>= string_sequence_out;

    std::cout << "string_sequence_out: "
        << (*string_sequence_out)[0]
        << ", "
        << (*string_sequence_out)[1]
        << "\n";
}

int main(int _a __attribute__((unused)), char* _b[] __attribute__((unused)))
{
    do_stuff();
    return 0;
}
