TEMPLATE = app
TARGET = CORBA_Any-no_memory_leak
LANGUAGE = C++
CONFIG += sdk_no_version_check c++14 strict_c++ rtti stl warn_on debug cmdline
CONFIG -= qt
LIBS += -lomniDynamic4
SOURCES += CORBA_Any-no_memory_leak.cpp

# Needed for qmake to use pkg-config
unix {
    CONFIG += link_pkgconfig
#
# ATTENTION
# Modify this path!
#
    PKGCONFIG += /usr/local/opt/omniorb/lib/pkgconfig/omniORB4.pc
}
