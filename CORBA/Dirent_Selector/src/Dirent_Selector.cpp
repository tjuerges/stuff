#include <iostream>
#include <string>
#include <ace/OS.h>
#include <ace/Dirent_Selector.h>

std::string pattern;

int selector(const dirent* d)
{
	std::cout << "Entry = " << d->d_name << std::endl;

	if(ACE_OS::strncmp(pattern.c_str(), d->d_name, pattern.size()) == 0)
	{
		return 1;
	}
	else
	{
		return 0;
	}
};

//ACE_Dirent_Selector::open (const ACE_TCHAR *dir,
//                           int (*sel)(const dirent *d),
//                           int (*cmp) (const dirent **d1,
//                                       const dirent **d2))

int main(int, char**)
{
	ACE_Dirent_Selector a;
	std::string path(ACE_OS::getenv("ACSDATA"));
	path += "/tmp/";
	pattern = "uid:__X";

	int files(a.open(path.c_str(), selector, 0));

	std::cout << "Number or files = " << files << std::endl;

	for(int i(0); i < files; ++i)
	{
		std::cout << (a[i]->d_name) << std::endl;
	}

	return 0;
}
