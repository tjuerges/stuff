#include <Semaphore.h>
#include <Time_Value.h>
#include <iostream>
#include <iomanip>

int main(int argc, char *argv[])
{
	ACE_Semaphore sem(1); // (0) means: wait for timeout only.
	sem.acquire();

	const unsigned int precision(11);
	const double wait_time(2.555555);

	std::cout << "Trying to wait "
	<< std::setprecision(precision)
	<< wait_time
	<< " seconds with the help of an ACE_Semaphore..."
	<< std::endl;


	ACE_Time_Value wait_ACE_time;
	wait_ACE_time.set(wait_time);
	const ACE_Time_Value begin(ACE_OS::gettimeofday());
	ACE_Time_Value to(begin + wait_ACE_time);

	sem.acquire(to);

	const ACE_Time_Value end(ACE_OS::gettimeofday());

	std::cout << "Timestamp begin = "
		<< std::setprecision(precision)
		<< to.msec() / 1e3
		<< "."
		<< std::endl
		<< "Should wait until timestamp = "
		<< std::setprecision(precision)
		<< to.msec() / 1e3
		<< "."
		<< std::endl
		<< "Timestamp end = "
		<< end.msec() / 1e3
		<< std::endl
		<< "Waited "
		<< std::setprecision(precision)
		<< (end - begin).msec() / 1e3
		<< " seconds."
		<< std::endl;

	return 0;
}
