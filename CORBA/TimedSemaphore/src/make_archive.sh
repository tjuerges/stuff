#!/bin/bash
#
#  "@(#) $Id$"
#

MODULE_NAME="ACE_Timed_Semaphore_Test"

make clean;
pushd ..;

if [ -d ../ws ]; then
	pushd ../lcu/src;
	make clean;
	popd;
	pushd ..;
	tar --exclude={.svn,CVS,*~,*.bak,*.o,lib*,*.a,#*} -cjf ${MODULE_NAME}.tar.bz2 ws/{config,idl,include,rtai,src} lcu/{config,idl,include,rtai,src};
	popd;
else
	tar --exclude={.svn,CVS,*~,*.bak,*.o,lib*,*.a,#*} -cjf ${MODULE_NAME}.tar.bz2 ./{config,idl,include,rtai,src};
fi;

popd
exit 0
