#include <iostream>
#include <cmath>
#include <boost/format.hpp>
#include <boost/units/io.hpp>
#include <boost/units/systems/si/length.hpp>
#include <boost/units/systems/si/plane_angle.hpp>
#include <boost/units/systems/angle/degrees.hpp>


int main(int _a, char* _b[])
{
    // Define a length l1.
    boost::units::quantity< boost::units::si::length > l1(1.234 *
        boost::units::si::meter);
    // Define another length l2.
    boost::units::quantity< boost::units::si::length > l2(2.123 *
        boost::units::si::meter);
    /**
     * Print out l1, l2, the sum l1 + l2 and the product l1 * l2.
     * All values including their units are automatically returned as
     * std::strings.
     */
    std::cout << boost::str(boost::format(
        "l1 = %s, l2 = %s\n"
        "l1 + l2 = %s\n"
        "l1 * l2 = %s\n\n")
        % l1
        % l2
        % (l1 + l2)
        % (l1 * l2));

    // Scaling to human readable units is very easy:
    std::cout << "l1 * 100000.0 = "
        << (l1 * 100000.0)
        << "\n"
        << "l1 * 100000.0 (scaled to human readable units) = "
        << boost::units::engineering_prefix
        << (l1 * 100000.0)
        << "\n";

    /**
     * I need the value of the product as a double:  just call the value()
     * method.
     */
    std::cout << boost::str(boost::format("value of l1 * l2 = %d\n\n")
        % (l1 * l2).value());

    // Conversion from radians to degrees.
    boost::units::quantity< boost::units::si::plane_angle >
        angleRad((M_PI * 0.5) * boost::units::si::radians);
    std::cout << boost::str(boost::format("%s = %s\n")
        % angleRad
        % boost::units::quantity< boost::units::degree::plane_angle >
            (angleRad));

    return 0;
}
