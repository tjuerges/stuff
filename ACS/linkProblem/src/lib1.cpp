/**
 * $Id$
 */


#include <iostream>
#include <cmath>


void bar()
{
    std::cout << "\nI am lib1::bar.\nCalling std::sin...\n"
        << std::sin(M_PI_2)
        << "\nstd::sin called.\n";
}
