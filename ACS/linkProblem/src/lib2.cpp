/**
 * $Id$
 */


#include <iostream>
#include <lib1.h>


void foo()
{
    std::cout << "\nI am lib2::foo.\nCalling lib1::bar...\n";
    bar();
    std::cout << "\nlib1::bar called.\n";
}
