/**
 * $Id$
 */


#include <lib2.h>
#include <iostream>

// Necessary declaration that allows to call std::cos without including
// the cmath header file.  This is to avoid any smart stuff being done
// in the ACS make system.
namespace std
{
    extern double cos(double);
};

// Same here.
extern void bar();


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    std::cout << "I am main.\nCalling lib2::foo...\n";
    foo();
    std::cout << "lib2::foo called.\nCalling lib1::bar...\n";
    bar();
    std::cout << "\nlib1::bar called.\nCalling std::cos...\n"
        << std::cos(1.2345678L)
        << "\nstd::cos called.\n";

    return 0;
};
