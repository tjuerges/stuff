/*
 *
 * "@(#) $Id$"
 *
 * $Log: hptStarUtils.h,v $
 * Revision 1.37  2005/05/27 15:45:20  tjuerges
 * - Added method get_filename to write_fits to get the current filename without path for Save as... in GUI.
 * - Added virtual inheritance from POA classes of monitor and alarm methods to be conformant with acsexamples.
 * - Added const method modifier to is_done of myCBvoid.
 *
 * Revision 1.36  2005/03/09 16:09:35  tjuerges
 * - Changed onValue monitors to update every 0.15 seconds.
 *
 * Revision 1.35  2005/03/09 13:35:26  tjuerges
 * - Added value triggered monitor functionality for ACS 4.0. If ACS can handle value triggered monitors again, go back to CVS version 1.34!
 *
 * Revision 1.34  2005/03/01 12:04:56  tjuerges
 * - Removed unnecessary hptStatus enum.
 *
 * Revision 1.33  2005/02/21 19:49:21  tjuerges
 * - Added monitor for ACS::Enums.
 * - Doxygenized documentation.
 *
 * Revision 1.32  2004/12/14 17:53:02  tjuerges
 * - Added ATTENTION section in myMonitor which reminds the programmer to set the default_timer_trig value to 2.0 in the appropriate CDB/alma/foo/foo.xml file.
 *
 * Revision 1.31  2004/12/14 17:16:42  tjuerges
 * - Added more comments on how to use the monitor template class.
 *
 * Revision 1.30  2004/12/10 16:48:12  tjuerges
 * - Fixed a major bug in myMonitor. Previous versions crashed due to nonexistent done method.
 *
 * Revision 1.29  2004/12/08 15:50:41  tjuerges
 * - Added alarm template for sequences. (Crashes, seems to ba a bug in ACS!)
 * - Added monitor template for strings. (Needed to incorrect monitor deployment in ACS' baci.idl.)
 *
 * Revision 1.28  2004/11/25 17:48:16  tjuerges
 * - write_fits: M2 leg length is double not long.
 *
 * Revision 1.27  2004/11/25 17:41:35  tjuerges
 * - write_fits: SHA positions are in double not in long. :-(
 *
 * Revision 1.26  2004/11/25 17:23:30  tjuerges
 * - Added CORBAString define.
 * - Added more private constructors to myComponentClient.
 *
 * Revision 1.25  2004/11/18 08:56:34  tjuerges
 * - Description of myMonitor changed.
 *
 * Revision 1.24  2004/11/18 08:22:01  tjuerges
 * - Added alarm template for ACS. Usage is similar to the monitor template. No alarm template for Sequences yet!
 *
 * Revision 1.23  2004/11/16 14:57:37  tjuerges
 * *** empty log message ***
 *
 * Revision 1.22  2004/11/16 12:20:58  tjuerges
 * - missing definition of ACSpattern_var added.
 * - added description of third parameter in myMonitor::create_monitor
 *
 * Revision 1.21  2004/11/15 12:57:11  tjuerges
 * - myMonitor: kosmetische �nderungen.
 *
 * Revision 1.20  2004/11/15 10:34:22  tjuerges
 * - myMonitor::set_trigger: delta_t in Sekunden, nicht als ACS::TimeInterval.
 *
 * Revision 1.19  2004/11/15 10:29:45  tjuerges
 * - myMonitor: set_trigger hinzugef�gt.
 *
 * Revision 1.18  2004/11/15 10:21:38  tjuerges
 * - myMonitor nun auch f�r Sequenzen verf�gbar.
 *
 * Revision 1.17  2004/11/12 17:44:07  tjuerges
 * - Modified myMonitor.
 *
 * Revision 1.16  2004/11/12 14:11:50  tjuerges
 * - Added (partial) support for sequence monitors; these monitors are not usable yet.
 *
 * Revision 1.15  2004/11/12 12:41:22  tjuerges
 * - Added more CORBA type short forms for Qt designer.
 *
 * Revision 1.14  2004/11/12 10:37:46  tjuerges
 * - Modified the myMonitor class heavily. See the description for myMonitor itself.
 *
 *
*/

#ifndef HPTSTARUTILS_H
#define HPTSTARUTILS_H

#include <string>
#include <vector>
#include <cmath>
#include <acstime.h>
#include <maciContainerImpl.h>
#include <maciContainerServices.h>
#include <maciSimpleClient.h>

// Some definitions for the Qt Designer.
#ifndef CORBAChar
#define CORBAChar CORBA::Char
#define CORBAchar CORBA::Char
#endif

#ifndef CORBAWChar
#define CORBAWChar CORBA::WChar
#define CORBAWchar CORBA::WChar
#endif

#ifndef CORBAString
#define CORBAString CORBA::String_var
#define CORBAstring CORBA::String_var
#endif

#ifndef CORBAFloat
#define CORBAFloat CORBA::Float
#define CORBAfloat CORBA::Float
#endif

#ifndef CORBADouble
#define CORBADouble CORBA::Double
#define CORBAdouble CORBA::Double
#endif

#ifndef CORBALongDouble
#define CORBALongDouble CORBA::LongDouble
#define CORBAlongDouble CORBA::LongDouble
#endif

#ifndef CORBAShort
#define CORBAShort CORBA::Short
#define CORBAshort CORBA::Short
#endif

#ifndef CORBAUShort
#define CORBAUShort CORBA::UShort
#define CORBAuShort CORBA::UShort
#endif

#ifndef CORBALong
#define CORBALong CORBA::Long
#define CORBAlong CORBA::Long
#endif

#ifndef CORBAULong
#define CORBAULong CORBA::ULong
#define CORBAuLong CORBA::ULong
#define CORBApattern CORBA::ULong
#endif

#ifndef CORBALongLong
#define CORBALongLong CORBA::LongLong
#define CORBAlongLong CORBA::LongLong
#endif

#ifndef CORBAULongLong
#define CORBAULongLong CORBA::ULongLong
#define CORBAuLongLong CORBA::ULongLong
#endif

#ifndef ACSpattern
#define ACSpattern ACS::pattern
#define ACSpattern_var ACS::pattern_var
#endif

#ifndef ACSlongSeq
#define ACSlongSeq ACS::longSeq
#define ACSlongSeq_var ACS::longSeq_var
#endif

#ifndef ACSstringSeq
#define ACSstringSeq ACS::stringSeq
#define ACSstringSeq_var ACS::stringSeq_var
#endif

#ifndef ACSpatternSeq
#define ACSpatternSeq ACS::patternSeq
#define ACSpatternSeq_var ACS::patternSeq_var
#endif

#ifndef ACSdoubleSeq
#define ACSdoubleSeq ACS::doubleSeq
#define ACSdoubleSeq_var ACS::doubleSeq_var
#endif


/**
 * Define exposure types for FITS images.
 */
enum ImageType
{
	BIAS,
	DARK,
	FLAT,
	OBJECT
};

/**
 * Helper class for FITS images.
 *
 * This class keeps the FITS header information which will
 * prepend every CCD frame.
 */
class fits_c
{
	public:
	fits_c::fits_c(void):
	airtempe(-9999.9),az(-9999.9),az_offset(-9999.9),
	cdelt1(-9999.9),cdelt2(-9999.9),crota1(-9999.9),
	crota2(-9999.9),crpix1(-9999.9),crpix2(-9999.9),
	crval1(-9999.9),crval2(-9999.9),ctype1(""),
	ctype2(""),datamax(65535.0),datamin(0),
	date_obs(""),datestop(""),dec(-9999.9),
	dec_offset(-9999.9),ele(-9999.9),ele_offset(-9999.9),
	equinox(-9999.9),exptime(-9999.9),filename(""),
	filter(""),focallen(-9999.9),gain(-9999.9),
	imagetyp(""),instrume(""),object(""),
	observer(""),	origin(""),pol(-9999.9),
	pol_offset(-9999.9),ra(-9999.9),ra_offset(-9999.9),
	telescop(""),temperat(-9999.9),timesys(""),
	type(""),shaposx(0L),shaposy(0L)
	{
		m1actpos=new std::vector<long>(36,-9999L);
		m2leglen=new std::vector<double>(6,-9999.9);
	};

	virtual fits_c::~fits_c(void)
	{
		delete m1actpos;
		delete m2leglen;
	};

  virtual fits_c& fits_c::operator=(const fits_c& in)
  {
  	airtempe=in.airtempe;
		az=in.az;
		az_offset=in.az_offset;
		cdelt1=in.cdelt1;
		cdelt2=in.cdelt2;
		crota1=in.crota1;
		crota2=in.crota2;
		crpix1=in.crpix1;
		crpix2=in.crpix2;
		crval1=in.crval1;
		crval2=in.crval2;
		ctype1=in.ctype1;
		ctype2=in.ctype2;
		datamax=in.datamax;
		datamin=in.datamin;
		date_obs=in.date_obs;
		datestop=in.datestop;
		dec=in.dec;
		dec_offset=in.dec_offset;
		ele=in.ele;
		ele_offset=in.ele_offset;
		equinox=in.equinox;
		exptime=in.exptime;
		filename=in.filename;
		filter=in.filter;
		focallen=in.focallen;
		gain=in.gain;
		imagetyp=in.imagetyp;
		instrume=in.instrume;
		object=in.object;
		observer=in.observer;
		origin=in.origin;
		pol=in.pol;
		pol_offset=in.pol_offset;
		ra=in.ra;
		ra_offset=in.ra_offset;
		telescop=in.telescop;
		temperat=in.temperat;
		timesys=in.timesys;
		type=in.type;
		xpixsz=in.xpixsz;
		ypixsz=in.ypixsz;
		shaposx=in.shaposx;
		shaposy=in.shaposy;
		*m1actpos=*(in.m1actpos);
		*m2leglen=*(in.m2leglen);
	return *this;
	};

  double airtempe;
  double az;
  double az_offset;
  double cdelt1;
  double cdelt2;
  double crota1;
  double crota2;
  double crpix1;
  double crpix2;
  double crval1;
  double crval2;
  std::string ctype1;
  std::string ctype2;
	double datamax;
	double datamin;
  std::string date_obs;
  std::string datestop;
  double dec;
  double dec_offset;
  double ele;
  double ele_offset;
  double equinox;
  double exptime;
  std::string filename;
  std::string filter;
  double focallen;
  double gain;
  std::string imagetyp;
  std::string instrume;
  std::string object;
  std::string observer;
  std::string origin;
  double pol;
  double pol_offset;
  double ra;
  double ra_offset;
  std::string telescop;
  double temperat;
  std::string timesys;
  std::string type;
  double xpixsz;
  double ypixsz;
  double shaposx;
  double shaposy;
  std::vector<long>* m1actpos;
  std::vector<double>* m2leglen;
};

/**
 * A small template class which makes it easier to connect
 * to ACS characteristic components.
 */
template< class component >class myComponentClient
{
	public:
	/*
	 * The constructor called from within a component.
	 */
	myComponentClient(const std::string& component_name,
		maci::ContainerServices* _cs):
	cs(_cs),
	sc(0),
	myComponentName(component_name),
	object(component::_nil())
	{
	};

	/*
	 * The constructor which is called from within a simpleClient
	 * environment.
	 */
	myComponentClient(
		const std::string& component_name,
		maci::SimpleClient* _sc):
	cs(0),
	sc(_sc),
	myComponentName(component_name),
	object(component::_nil())
	{
	};

	~myComponentClient()
	{
		disconnect();
		/*
		 * if(cs)
		 * {
		 * delete cs;
		 * cs=0;
		 * }
		 */
	};

	component* get_object(void) const
	{
		return object;
	};

	bool connect(void)
	{
		bool ret(false);

		try
		{
			// Called from a component, so the simpleClient object is empty:
	  		if((cs != 0) && (sc == 0))
	  		{
				object = cs->getComponent< component >(myComponentName.c_str());
	  		}
	  		// Called from a maci::SimpleClient:
	  		else if((cs == 0) && (sc != 0))
	  		{
				object=sc->getComponent< component >(myComponentName.c_str(), 0, true);
	  		}
			// Something unforeseen happened:
	  		else
	  		{
				ACS_SHORT_LOG((LM_ERROR,"myComponentClient::connect: "
					"Could connect to the component, neither the component nor the "
					"simple client constructor have setup their stuff."));
	  		}

			if(CORBA::is_nil(object) == true)
			{
				object = component::_nil();
				ACS_SHORT_LOG((LM_ERROR, "myComponentClient::connect: "
					"Needed component %s is not available.", myComponentName.c_str()));
			}
			else
			{
				ACS_SHORT_LOG((LM_INFO, "myComponentClient::connect: "
					"Component %s is activated.", myComponentName.c_str()));

				ret = true;
			}
		}
	  	catch(...)
		{
			if(CORBA::is_nil(object) == false)
			{
			  	ACS_SHORT_LOG((LM_ERROR, "myComponentClient::connect: "
			  		"Something weird happened during component activation, which has "
			  		"been aborted!"));
				if((cs != 0) && (sc == 0))
				{
					cs->releaseComponent(myComponentName.c_str());
				}
				else
				{
					sc->releaseComponent(myComponentName.c_str());
				}

	  			object = component::_nil();
			}

  			ACS_SHORT_LOG((LM_ERROR, "myComponentClient::connect: "
  				"Access to component %s failed.", myComponentName.c_str()));
		}

	  	return ret;
	};

	void disconnect(void)
	{
	  if(CORBA::is_nil(object) == false)
	  {
		try
			{
				// Called from a component:
		  	if((cs!=0)&&(sc==0))
		  	{
		  		ACS_SHORT_LOG((LM_INFO,"myComponentClient::disconnect: Disconnecting (from component side) %s...",myComponentName.c_str()));
			 		cs->releaseComponent(myComponentName.c_str());
		  	}
		  	// Called from a maci::SimpleClient:
		  	else if((cs==0)&&(sc!=0))
		  	{
		  		ACS_SHORT_LOG((LM_INFO,"myComponentClient::disconnect: Disconnecting (from component simple client side) %s...",myComponentName.c_str()));
				  sc->releaseComponent(myComponentName.c_str());
		  	}
		  	else
		  	{
				  ACS_SHORT_LOG((LM_ERROR,"myComponentClient::disconnect: Could not release the component, neither the component nor the simple client constructor have setup their stuff."));
				  return;
		  	}
			  ACS_SHORT_LOG((LM_INFO,"myComponentClient::disconnect: Releasing component %s done.",myComponentName.c_str()));
			}
			catch(...)
			{
		 		ACS_SHORT_LOG((LM_ERROR,"myComponentClient::disconnect: Something weird happened during release of component %s.",myComponentName.c_str()));
			};

	  	object=component::_nil();
		}
	};

	protected:
	maci::ContainerServices* cs;
	maci::SimpleClient* sc;
	std::string myComponentName;
	component* object;

	private:
	myComponentClient();
 	myComponentClient(const myComponentClient&);
 	void operator=(const myComponentClient&);
};

/**
 *
 */ 
class myTimeService
{
	public:
	myTimeService(void);
	virtual ~myTimeService(void);
	virtual bool connect(void);
	virtual void disconnect(void);
	virtual void get_time(std::string&) const;
	virtual void get_time(std::string&,double&) const;
	virtual void get_time(ACS::Time&) const;

	protected:
	//acstime::Clock_ptr timeservice;
	long array2tai,tai2utc;
};

/**
 * \def myMonitorEnum
 * \brief myMonitorEnum:
 *	A simple and easy to use monitor class for ACS Enum properties.
 * 
 * ATTENTION: Every ACS property which shall be monitored must have the CDB value
 * 		default_timer_trig
 * set to a minimum of 2.0, i.e. a property entry in the appropriate xml file should
 * look like:
 *
 * <myProperty default_timer_trig="2.0"/>
 *
 * It seems that setting it to value of 1.0 does not activate the monitor.
 *
 *
 * This is due to a bug in ACS 3.1.2 and seems not to be fixed in ACS 4,0.
 *
 *
 * Usage:
 *
 * Constructor
 * 	- myMonitorEnum(IDL-Interface,class_of_property,class_which_instanciates_the_monitor)
 *
 * Example:
 * 	- myMonitorEnum(hptM1,M1_Status,hptM1GUI)* status;
 *
 * Instantiation
 * 	- monitorInstanceEnum=new myMonitorEnum(IDL-Module,class_of_property,class_which_instanciates_the_monitor)(std::string("property_name_for_logging"),&owner_class::monitor_method,owner)
 *
 * Example:
 * 	- status=new myMonitorEnum(hptM1,M1_status,hptM1GUI)(std::string("Enum monitor for M1 status"),&hptM1GUI::M1statusChanged,this);
 *
 * Activation of the monitor itself
 * 	- monitorInstanceEnum[.|->]create_monitor(property_pointer,double timer_trigger,C++_type delta_value);
 *
 * Example:
 * 	- if(!status->create_monitor(component->status(),0.0,1,true))
 * 		{
 * 			delete status;
 * 			status=0;
 * 		};
 *
 * Destructor
 * 	- delete monitorInstance;
 *
 * Example:
 * 	- delete status;
 *
 * \sa myMonitor, myMonitorSeq, myMonitorString
 */
/**
 * \def myMonitorSeq
 * \brief myMonitorSeq
 *	A simple and easy to use monitor class for ACS sequence properties.
 * 
 * ATTENTION: Every ACS property which shall be monitored must have the CDB value
 * 		default_timer_trig
 * set to a minimum of 2.0, i.e. a property entry in the appropriate xml file should
 * look like:
 *
 * <myProperty default_timer_trig="2.0"/>
 *
 * It seems that setting it to value of 1.0 does not activate the monitor.
 *
 *
 * This is due to a bug in ACS 3.1.2 and seems not to be fixed in ACS 4,0.
 *
 *
 * Usage:
 *
 * Constructor
 * 	- myMonitorSeq(class_of_a_single_property_element,class_which_instanciates_the_monitor)
 *
 * Example:
 * 	- myMonitorSeq(long,hptM1GUI)* piezoSequence;
 *
 * Instantiation
 * 	- monitorInstanceSeq=new myMonitor(class_of_property_element,class_which_instanciates_the_monitor)(std::string("property_name_for_logging"),&owner_class::monitor_method,owner)
 *
 * Example:
 * 	- piezoSequence=new myMonitorSeq(long,hptM1GUI)(std::string("piezoSequence"),&hptM1GUI::piezoSequence_changed,this);
 *
 * Activation of the monitor itself
 * 	- monitorInstanceSeq[.|->]create_monitor(property_pointer,double timer_trigger,C++_type delta_value);
 *
 * Example:
 * 	- if(!piezoSequence->create_monitor(component->piezoSequence(),10.0,1L,true))
 * 		{
 * 			delete piezoSequence;
 * 			piezoSequence=0;
 * 		};
 *
 * Destructor
 * 	- delete monitorInstance;
 *
 * Example:
 * 	- delete piezoSequence;
 *
 * \sa myMonitor, myMonitorEnum, myMonitorString
 */
/**
 * \def myMonitorString
 * \brief myMonitorString
 *	A simple and easy to use monitor class for ACS string properties.
 * 
 * ATTENTION: Every ACS property which shall be monitored must have the CDB value
 * 		default_timer_trig
 * set to a minimum of 2.0, i.e. a property entry in the appropriate xml file should
 * look like:
 *
 * <myProperty default_timer_trig="2.0"/>
 *
 * It seems that setting it to value of 1.0 does not activate the monitor.
 *
 *
 * This is due to a bug in ACS 3.1.2 and seems not to be fixed in ACS 4,0.
 *
 *
 * Usage:
 *
 * Constructor
 * 	- myMonitorString(class_of_property,class_which_instanciates_the_monitor)
 *
 * Example:
 * 	- myMonitorString(string,hptUPSGUI)* model;
 *
 * Instantiation
 * 	- monitorInstanceString=new myMonitorString(class_of_property,class_which_instanciates_the_monitor)(std::string("property_name_for_logging"),&owner_class::monitor_method,owner)
 *
 * Example:
 * 	- model=new myMonitorString(string,hptUPSGUI)(std::string("model"),&hptUPSGUI::model_changed,this);
 *
 * Activation of the monitor itself
 * 	- monitorInstanceString[.|->]create_monitor(property_pointer,double timer_trigger);
 *
 * Example:
 * 	- if(!model->create_monitor(component->model(),5.0))
 * 		{
 * 			delete model;
 * 			model=0;
 * 		};
 *		As you might see, here, the delta_value is missing. I think, you know why.
 *
 * Destructor
 * 	- delete monitorInstance;
 *
 * Example:
 * 	- delete model;
 *
 * \sa myMonitor, myMonitorEnum, myMonitorSeq
 */
 /**
 * \def myMonitor
 * \brief myMonitor:
 *	A simple and easy to use monitor class for ACS properties.
 * 
 * ATTENTION: Every ACS property which shall be monitored must have the CDB value
 * 		default_timer_trig
 * set to a minimum of 2.0, i.e. a property entry in the appropriate xml file should
 * look like:
 *
 * <myProperty default_timer_trig="2.0"/>
 *
 * It seems that setting it to value of 1.0 does not activate the monitor.
 *
 *
 * This is due to a bug in ACS 3.1.2 and seems not to be fixed in ACS 4,0.
 *
 *
 * Usage:
 *
 * Constructor
 * 	- myMonitor(class_of_property,class_which_instanciates_the_monitor)
 *
 * Example:
 * 	- myMonitor(double,hptUPSGUI)* lineVoltage;
 *
 * Instantiation
 * 	- monitorInstance=new myMonitor(class_of_property,class_which_instanciates_the_monitor)(std::string("property_name_for_logging"),&owner_class::monitor_method,owner)
 *
 * Example:
 * 	- lineVoltage=new myMonitor(double,hptUPSGUI)(std::string("lineVoltage"),&hptUPSGUI::lineVoltage_changed,this);
 *
 * Activation of the monitor itself
 * 	- monitorInstance[.|->]create_monitor(property_pointer,double timer_trigger,C++_type delta_value);
 *
 * Example:
 * 	- if(!lineVoltage->create_monitor(component->lineVoltage(),0,0.01,true))
 * 		{
 * 			delete lineVoltage;
 * 			lineVoltage=0;
 * 		};
 *
 * Destructor
 * 	- delete monitorInstance;
 *
 * Example:
 * 	- delete lineVoltage;
 *
 * \sa myMonitorEnum, myMonitorSeq, myMonitorString
 */
 /**
 * \class myMonitor
 * \brief myMonitor:
 *	A simple and easy to use monitor class for ACS properties.
 * 
 * ATTENTION: Every ACS property which shall be monitored must have the CDB value
 * 		default_timer_trig
 * set to a minimum of 2.0, i.e. a property entry in the appropriate xml file should
 * look like:
 *
 * <myProperty default_timer_trig="2.0"/>
 *
 * It seems that setting it to value of 1.0 does not activate the monitor.
 *
 *
 * This is due to a bug in ACS 3.1.2 and seems not to be fixed in ACS 4,0.
 *
 *
 * Usage:
 *
 * Constructor
 * 	- myMonitor(class_of_property,class_which_instanciates_the_monitor)
 *
 * Example:
 * 	- myMonitor(double,hptUPSGUI)* lineVoltage;
 *
 * Instantiation
 * 	- monitorInstance=new myMonitor(class_of_property,class_which_instanciates_the_monitor)(std::string("property_name_for_logging"),&owner_class::monitor_method,owner)
 *
 * Example:
 * 	- lineVoltage=new myMonitor(double,hptUPSGUI)(std::string("lineVoltage"),&hptUPSGUI::lineVoltage_changed,this);
 *
 * Activation of the monitor itself
 * 	- monitorInstance[.|->]create_monitor(property_pointer,double timer_trigger,C++_type delta_value);
 *
 * Example:
 * 	- if(!lineVoltage->create_monitor(component->lineVoltage(),0,0.01,true))
 * 		{
 * 			delete lineVoltage;
 * 			lineVoltage=0;
 * 		};
 *
 * Destructor
 * 	- delete monitorInstance;
 *
 * Example:
 * 	- delete lineVoltage;
 *
 *
 *
 * A longer example:
 * You have a client class (hptPSGUI) for a component which controls a power supply.
 * The current output voltage (an ACS::R[O|W]double) shall be monitored. Set up of the monitor is like this:
 *
 * myMonitor(double,hptPSGUI)* monitor_voltage;
 *
 *
 * Now instanciate the monitor:
 *
 * monitor_voltage=new myMonitor(double,hptPSGUI)(std::string("voltage"),&hptPSGUI::voltage_changed,this);
 *
 * hptPSGUI is the class which owns the the meonitor callback method. The string "voltage" is
 * arbitrary so that any string can be passed to it. It just makes the log files easier to read.
 * voltage_changed is a method in hptPSGUI (the owner of the method voltage_changed) which
 * is called by the monitor every time a monitor event occurs.
 * This has to be passed to let the monitor know of the owning class instance in order
 * to call the appropriate instance of the voltage_changed method.
 *
 * After all, activate the monitor:
 *
 * bool success(monitor_voltage->create_monitor(ps->voltage(),1.5,0.01,true));
 * 
 * if(success)
 * {
 * 		// Do something meaningful, e.g. print out a log message.
 * }
 * else
 * {
 * 		// Creation of the monitor failed. Perhaps the component was not active.
 * }
 *
 * ps is a pointer to the component, voltage is the component's property. The
 * values following the property give
 * - double: If a time triggerd monitor is wanted (in seconds) or/and
 * - CORBA::Double (depend on the property type): delta value of the property
 *		which triggers the monitor, too.
 * - true: enable value triggered mode.
 *
 * Destruction of the monitor is done as soon as the context is left (for stack allocated
 * objects) or by explicit deletion of the monitor instance (for heap allocated objects).
 *
 * \sa myMonitor, myMonitorEnum, myMonitorSeq, myMonitorString
 */
#define myMonitor(type,owner_class) \
myMonitor_TemplateDefault<POA_ACS::CB##type,CORBA##type,ACS::P##type##_var,ACS::Monitor##type,ACS::Monitor##type##_var,ACS::CB##type,ACS::CB##type##_var,owner_class>

#define myMonitorEnum(idl_module,type,owner_class) \
myMonitor_TemplateDefault<POA_ACS::CBpattern,ACS::pattern,idl_module::P##type##_var,ACS::Monitorpattern,ACS::Monitorpattern_var,ACS::CBpattern,ACS::CBpattern_var,owner_class>

#define myMonitorSeq(type,owner_class) \
myMonitor_TemplateSeq<CORBA##type,POA_ACS::CB##type##Seq,ACS::type##Seq,ACS::type##Seq##_var,ACS::P##type##Seq_var,ACS::Monitor##type,ACS::Monitor##type##_var,ACS::CB##type##Seq,ACS::CB##type##Seq_var,owner_class>

#define myMonitorString(type,owner_class) \
myMonitor_TemplateString<POA_ACS::CB##type,std::string,ACS::P##type##_var,ACS::Monitor,ACS::Monitor_var,ACS::CB##type,ACS::CB##type##_var,owner_class>


/**
 * myMonitor_Template:
 * Template for every other monitor template class.
 */
template<class poa_class,class corba_class,class ACSProperty_Type_var,class ACSMonitor_Type,class ACSMonitor_Type_var,class ACSCallback_Type,class ACSCallback_Type_var,class owner_class>
class myMonitor_Template: public virtual poa_class
{
	public:
 	myMonitor_Template(const std::string& _prop,void(owner_class::*_myCB)(void),owner_class* _ownerInstance)
	:prop(_prop),
	timer_triggered(false),
	value_triggered(false),
	monitor_var(ACSMonitor_Type::_nil()),
	callback(ACSCallback_Type::_nil()),
	ownerInstance(_ownerInstance),
	myCallBack(_myCB),
	myCallBack_withValue(0)
	{
		ACS_SHORT_LOG((LM_INFO,"myMonitor::myMonitor: The monitor will start to receive values from now on. It will observe the property \"%s\".",prop.c_str()));
	};

 	myMonitor_Template(const std::string& _prop,void(owner_class::*_myCB)(const corba_class&),owner_class* _ownerInstance)
	:prop(_prop),
	timer_triggered(false),
	value_triggered(false),
	monitor_var(ACSMonitor_Type::_nil()),
	callback(ACSCallback_Type::_nil()),
	ownerInstance(_ownerInstance),
	myCallBack(0),
	myCallBack_withValue(_myCB)
	{
		ACS_SHORT_LOG((LM_INFO,"myMonitor::myMonitor: The monitor will start to receive values from now on. It will observe the property \"%s\".",prop.c_str()));
	};

	virtual ~myMonitor_Template(void)
	{
		if(!CORBA::is_nil(monitor_var.in()))
		{
			monitor_var->destroy();
		  monitor_var=ACSMonitor_Type::_nil();
		  callback=ACSCallback_Type::_nil();
		  ACS_SHORT_LOG((LM_INFO,"myMonitor::~myMonitor: %s monitor destroyed.",prop.c_str()));
	  }
	};

	virtual void set_trigger(const double delta_time,const corba_class& delta_value,const bool delta_value_onoff)=0;

	virtual void set_trigger(const double delta_time)
	{
		double dummy(delta_time);
		if(std::abs(delta_time)>0.1)
		{
			timer_triggered=true;
		}
		else
		{
			dummy=0.15;
			timer_triggered=false;
		}
		ACS::TimeInterval time_interval(static_cast<ACS::TimeInterval>(ACE_ONE_SECOND_IN_NSECS*dummy)/100);
	monitor_var->set_timer_trigger(time_interval);
	};

	virtual const corba_class& get_value(void) const
	{
		return oldValue;
	};

	virtual CORBA::Boolean negotiate(ACS::TimeInterval,const ACS::CBDescOut&) throw (CORBA::SystemException)
	{
		ACS_SHORT_LOG((LM_INFO,"myMonitor::negotiate: No idea when this method (myMonitor::negotiate) will be called."));
		return true;
	};

	virtual void done(corba_class,const ACSErr::Completion&,const ACS::CBDescOut&) throw (CORBA::SystemException)
	{
		ACS_SHORT_LOG((LM_INFO,"myMonitor::done: The monitor will be destroyed."));
	};

	virtual void done(const corba_class&,const ACSErr::Completion&,const ACS::CBDescOut&) throw (CORBA::SystemException)
	{
		ACS_SHORT_LOG((LM_INFO,"myMonitor::done: The monitor will be destroyed."));
	};

	virtual void done(const char*,const ACSErr::Completion&,const ACS::CBDescOut&) throw (CORBA::SystemException)
	{
		ACS_SHORT_LOG((LM_INFO,"myMonitor::done: The monitor will be destroyed."));
	};

	protected:
  std::string prop;
	corba_class oldValue;

	bool timer_triggered;
	bool value_triggered;

	ACSMonitor_Type_var monitor_var;
	ACSCallback_Type_var callback;
	ACS::CBDescIn callback_description;
	owner_class* ownerInstance;

  void(owner_class::*myCallBack)(void);
  void(owner_class::*myCallBack_withValue)(const corba_class&);

 	private:
 	myMonitor_Template();
 	myMonitor_Template(const myMonitor_Template&);
 	void operator=(const myMonitor_Template&);
};

template<class poa_class,class corba_class,class ACSProperty_Type_var,class ACSMonitor_Type,class ACSMonitor_Type_var,class ACSCallback_Type,class ACSCallback_Type_var,class owner_class>
class myMonitor_TemplateDefault: public myMonitor_Template<poa_class,corba_class,ACSProperty_Type_var,ACSMonitor_Type,ACSMonitor_Type_var,ACSCallback_Type,ACSCallback_Type_var,owner_class>
{
	public:
 	myMonitor_TemplateDefault(const std::string& _prop,void(owner_class::*_myCB)(void),owner_class* _ownerInstance)
	:myMonitor_Template<poa_class,corba_class,ACSProperty_Type_var,ACSMonitor_Type,ACSMonitor_Type_var,ACSCallback_Type,ACSCallback_Type_var,owner_class>(_prop,_myCB,_ownerInstance)
	{};

 	myMonitor_TemplateDefault(const std::string& _prop,void(owner_class::*_myCB)(const corba_class&),owner_class* _ownerInstance)
	:myMonitor_Template<poa_class,corba_class,ACSProperty_Type_var,ACSMonitor_Type,ACSMonitor_Type_var,ACSCallback_Type,ACSCallback_Type_var,owner_class>(_prop,_myCB,_ownerInstance)
	{};

	virtual ~myMonitor_TemplateDefault()
	{};

	virtual bool create_monitor(ACSProperty_Type_var property,const double delta_time,const corba_class& delta_value,const bool delta_value_onoff)
	{
		bool ret(false);

	  ACSErr::Completion_var completion;
	  this->oldValue=property->get_sync(completion.out());

	  this->callback=this->_this();
	  this->monitor_var=property->create_monitor(this->callback.in(),this->callback_description);
	  if(!CORBA::is_nil(this->monitor_var.ptr()))
	  {
	  	set_trigger(delta_time,delta_value,delta_value_onoff);
		ACS_SHORT_LOG((LM_INFO,"myMonitor::create_monitor: %s monitor object created.",this->prop.c_str()));
		ret=true;
	  }
	  else
	  {
		ACS_SHORT_LOG((LM_ERROR,"myMonitor::create_monitor: %s monitor object NOT created. It seems that the property is not available.",this->prop.c_str()));
		this->monitor_var=ACSMonitor_Type::_nil();
		  this->callback=ACSCallback_Type::_nil();
	  }
	  return ret;
	};

	virtual bool create_monitor(ACSProperty_Type_var property,const double delta_time)
	{
		bool ret(false);

	  ACSErr::Completion_var completion;
	  this->oldValue=property->get_sync(completion.out());

	  this->callback=this->_this();
	  this->monitor_var=property->create_monitor(this->callback.in(),this->callback_description);
	  if(!CORBA::is_nil(this->monitor_var.ptr()))
	  {
	  	myMonitor_Template<poa_class,corba_class,ACSProperty_Type_var,ACSMonitor_Type,ACSMonitor_Type_var,ACSCallback_Type,ACSCallback_Type_var,owner_class>::set_trigger(delta_time);
		ACS_SHORT_LOG((LM_INFO,"myMonitor::create_monitor: %s monitor object created.",this->prop.c_str()));
		ret=true;
	  }
	  else
	  {
		ACS_SHORT_LOG((LM_ERROR,"myMonitor::create_monitor: %s monitor object NOT created. It seems that the property is not available.",this->prop.c_str()));
		this->monitor_var=ACSMonitor_Type::_nil();
		  this->callback=ACSCallback_Type::_nil();
	  }
	  return ret;
	};

	virtual void set_trigger(const double delta_time,const corba_class& delta_value,const bool delta_value_onoff)
	{
		this->value_triggered=delta_value_onoff;
		this->delta=std::abs(static_cast<double>(delta_value));
		/**
		 * ACS 4.0 does not support value triggerd monitors.
		 * Don't ask me why. :-(
		 * monitor_var->set_value_trigger(delta_value,static_cast<CORBA::Boolean>(delta_value_onoff));
		 */
		myMonitor_Template<poa_class,corba_class,ACSProperty_Type_var,ACSMonitor_Type,ACSMonitor_Type_var,ACSCallback_Type,ACSCallback_Type_var,owner_class>::set_trigger(delta_time);
	};

	virtual void working(corba_class value,const ACSErr::Completion& c,const ACS::CBDescOut& desc) throw (CORBA::SystemException)
	{
		if((this->timer_triggered)||((this->value_triggered)&&(values_differ(value))))
		{
	 		this->oldValue=value;
	 		if(this->myCallBack_withValue)
	 		{
	 			(this->ownerInstance->*(this->myCallBack_withValue))(this->oldValue);
	 		}
	 		else if(this->myCallBack)
	 		{
	 			(this->ownerInstance->*(this->myCallBack))();
	 		}
		}
	};

	protected:
	virtual bool values_differ(const corba_class& value) const
	{
		double diff(std::abs(static_cast<double>(value)-static_cast<double>(this->oldValue)));
		if(diff>=this->delta)
		{
			return true;
		}
		else
		{
			return false;
		}
	};

	double delta;
};

/*
 * The same monitor template for Sequences. Pass something like
 * long as first parameter to the template if you have an ACS::longSeq.
 * The monitor callback which may be given the current sequence
 * must have the signature foo::monitor(const ACSlongSeq& values) where
 * ACSlongSeq is a short form according to those defined here (see top
 * of file).
 */
template<class corba_class_element_type,class poa_class,class corba_class,class corba_class_var,class ACSProperty_Type_var,class ACSMonitor_Type,class ACSMonitor_Type_var,class ACSCallback_Type,class ACSCallback_Type_var,class owner_class>
class myMonitor_TemplateSeq: public myMonitor_Template<poa_class,corba_class,ACSProperty_Type_var,ACSMonitor_Type,ACSMonitor_Type_var,ACSCallback_Type,ACSCallback_Type_var,owner_class>
{
	public:
 	myMonitor_TemplateSeq(const std::string& _prop,void(owner_class::*_myCB)(void),owner_class* _ownerInstance)
	:myMonitor_Template<poa_class,corba_class,ACSProperty_Type_var,ACSMonitor_Type,ACSMonitor_Type_var,ACSCallback_Type,ACSCallback_Type_var,owner_class>(_prop,_myCB,_ownerInstance)
	{};

 	myMonitor_TemplateSeq(const std::string& _prop,void(owner_class::*_myCB)(const corba_class&),owner_class* _ownerInstance)
	:myMonitor_Template<poa_class,corba_class,ACSProperty_Type_var,ACSMonitor_Type,ACSMonitor_Type_var,ACSCallback_Type,ACSCallback_Type_var,owner_class>(_prop,_myCB,_ownerInstance)
	{};

	virtual ~myMonitor_TemplateSeq()
	{};

	virtual bool create_monitor(ACSProperty_Type_var property,const double delta_time,const corba_class& delta_value,const bool delta_value_onoff)
	{
		return create_monitor(property,delta_time,delta_value[0],delta_value_onoff);
	};

	virtual bool create_monitor(ACSProperty_Type_var property,const double delta_time,const corba_class_element_type& delta_value,const bool delta_value_onoff)
	{
		bool ret(false);

	  ACSErr::Completion_var completion;
	  oldValue=property->get_sync(completion.out());

	  this->callback=this->_this();
	  this->monitor_var=property->create_monitor(this->callback.in(),this->callback_description);
	  if(!CORBA::is_nil(this->monitor_var.ptr()))
	  {
	  	set_trigger(delta_time,delta_value,delta_value_onoff);
		ACS_SHORT_LOG((LM_INFO,"myMonitorSeq::create_monitor: %s monitor object created.",this->prop.c_str()));
		ret=true;
	  }
	  else
	  {
		ACS_SHORT_LOG((LM_ERROR,"myMonitorSeq::create_monitor: %s monitor object NOT created. It seems that the property is not available.",this->prop.c_str()));
		this->monitor_var=ACSMonitor_Type::_nil();
		  this->callback=ACSCallback_Type::_nil();
	  }
	  return ret;
	};

	virtual void set_trigger(const double delta_time,const corba_class& delta_value,const bool delta_value_onoff)
	{
		set_trigger(delta_time,delta_value[0],delta_value_onoff);
	};

	virtual void set_trigger(const double delta_time,const corba_class_element_type& delta_value,const bool delta_value_onoff)
	{
		this->value_triggered=delta_value_onoff;
		delta=std::abs(static_cast<double>(delta_value));
		/**
		 * ACS 4.0 does not support value triggerd monitors.
		 * Don't ask me why. :-(
		 * monitor_var->set_value_trigger(delta_value,static_cast<CORBA::Boolean>(delta_value_onoff));
		 */
  	myMonitor_Template<poa_class,corba_class,ACSProperty_Type_var,ACSMonitor_Type,ACSMonitor_Type_var,ACSCallback_Type,ACSCallback_Type_var,owner_class>::set_trigger(delta_time);
	};

	virtual const corba_class& get_value(void) const
	{
		return this->oldValue.in();
	};

	virtual bool sequence_differs(const corba_class& value) const
	{
		bool ret(false);
		const long old_length(oldValue->length()-1L),new_length(value.length()-1L);
		long check_size(old_length);

		if(old_length!=new_length)
		{
			if(new_length<old_length)
			{
				check_size=new_length;
			}
			else
			{
				ret=true;
			}
		}

		if(!ret)
		{
			try
			{
				double diff;
				for(long i(check_size);i>=0;--i)
				{
					diff=std::abs(static_cast<double>(value[i])-static_cast<double>((oldValue.in())[i]));
					if(diff>=delta)
					{
						ret=true;
						break;
					}
				}
			}
			catch(...)
			{
				ret=true;
			}
		}
		return ret;
	}

	virtual void working(const corba_class& value,const ACSErr::Completion& c,const ACS::CBDescOut& desc) throw (CORBA::SystemException)
	{
		if((this->timer_triggered)||((this->value_triggered)&&(sequence_differs(value))))
		{
	 		oldValue=value;
	 		if(this->myCallBack_withValue)
	 		{
	 			(this->ownerInstance->*(this->myCallBack_withValue))(oldValue.in());
	 		}
	 		else if(this->myCallBack)
	 		{
	 			(this->ownerInstance->*(this->myCallBack))();
	 		}
		}
	}

	protected:
	corba_class_var oldValue;
	double delta;

 	private:
 	myMonitor_TemplateSeq();
 	myMonitor_TemplateSeq(const myMonitor_TemplateSeq&);
 	void operator=(const myMonitor_TemplateSeq&);
};

template<class poa_class,class corba_class,class ACSProperty_Type_var,class ACSMonitor_Type,class ACSMonitor_Type_var,class ACSCallback_Type,class ACSCallback_Type_var,class owner_class>
class myMonitor_TemplateString: public myMonitor_Template<poa_class,corba_class,ACSProperty_Type_var,ACSMonitor_Type,ACSMonitor_Type_var,ACSCallback_Type,ACSCallback_Type_var,owner_class>
{
	public:
 	myMonitor_TemplateString(const std::string& _prop,void(owner_class::*_myCB)(void),owner_class* _ownerInstance)
	:myMonitor_Template<poa_class,corba_class,ACSProperty_Type_var,ACSMonitor_Type,ACSMonitor_Type_var,ACSCallback_Type,ACSCallback_Type_var,owner_class>(_prop,_myCB,_ownerInstance)
	{};

 	myMonitor_TemplateString(const std::string& _prop,void(owner_class::*_myCB)(const corba_class&),owner_class* _ownerInstance)
	:myMonitor_Template<poa_class,corba_class,ACSProperty_Type_var,ACSMonitor_Type,ACSMonitor_Type_var,ACSCallback_Type,ACSCallback_Type_var,owner_class>(_prop,_myCB,_ownerInstance)
	{};

	virtual ~myMonitor_TemplateString()
	{};

	virtual bool create_monitor(ACSProperty_Type_var property,const double delta_time,const corba_class& delta_value,const bool delta_value_onoff)
	{
		this->value_triggered=delta_value_onoff;
		return create_monitor(property,delta_time);
	}

	virtual bool create_monitor(ACSProperty_Type_var property,const double delta_time)
	{
		bool ret(false);

	  ACSErr::Completion_var completion;
	  this->oldValue=property->get_sync(completion.out());

	  this->callback=this->_this();
	  this->monitor_var=property->create_monitor(this->callback.in(),this->callback_description);
	  if(!CORBA::is_nil(this->monitor_var.ptr()))
	  {
	  	myMonitor_Template<poa_class,corba_class,ACSProperty_Type_var,ACSMonitor_Type,ACSMonitor_Type_var,ACSCallback_Type,ACSCallback_Type_var,owner_class>::set_trigger(delta_time);
		ACS_SHORT_LOG((LM_INFO,"myMonitor::create_monitor: %s monitor object created.",this->prop.c_str()));
		ret=true;
	  }
	  else
	  {
		ACS_SHORT_LOG((LM_ERROR,"myMonitor::create_monitor: %s monitor object NOT created. It seems that the property is not available.",this->prop.c_str()));
		this->monitor_var=ACSMonitor_Type::_nil();
		  this->callback=ACSCallback_Type::_nil();
	  }
	  return ret;
	};

	virtual void set_trigger(const double delta_time,const corba_class& delta_value,const bool delta_value_onoff)
	{
		this->value_triggered=delta_value_onoff;
		myMonitor_Template<poa_class,corba_class,ACSProperty_Type_var,ACSMonitor_Type,ACSMonitor_Type_var,ACSCallback_Type,ACSCallback_Type_var,owner_class>::set_trigger(delta_time);
	}

	virtual void working(const char* value,const ACSErr::Completion& c,const ACS::CBDescOut& desc) throw (CORBA::SystemException)
	{
		if((this->timer_triggered)||((this->value_triggered)&&(this->oldValue!=value)))
		{
	 		this->oldValue=value;
	 		if(this->myCallBack_withValue)
	 		{
	 			(this->ownerInstance->*(this->myCallBack_withValue))(this->oldValue);
	 		}
	 		else if(this->myCallBack)
	 		{
	 			(this->ownerInstance->*(this->myCallBack))();
	 		}
		}
	};

 	private:
 	myMonitor_TemplateString();
 	myMonitor_TemplateString(const myMonitor_TemplateString&);
 	void operator=(const myMonitor_TemplateString&);
};

/*
 * Alarm class template.
 * Usage: the same as for the monitor template but two methods
 * have to be passed to the constructor. one for alarm raise and one
 * for alarm clear events.
 */
 #define myAlarm(type,owner_class) \
myAlarm_Template<CORBA##type,POA_ACS::Alarm##type,ACS::RO##type##_var,ACS::Alarm##type,ACS::Alarm##type##_var,owner_class>

#define myAlarmSeq(type,owner_class) \
myAlarm_Template<CORBA##type,POA_ACS::Alarm##type,ACS::RO##type##Seq_var,ACS::Alarm##type,ACS::Alarm##type##_var,owner_class>

template<class corba_class,class poa_class,class ACSProperty_Type_var,class ACSAlarm_Type,class ACSAlarm_Type_var,class owner_class>
class myAlarm_Template: public virtual poa_class
{
	public:
 	myAlarm_Template(const std::string& _prop,void(owner_class::*_myCB_raised)(void),void(owner_class::*_myCB_cleared)(void),owner_class* _ownerInstance)
	:prop(_prop),
	alarm_var(ACS::Subscription::_nil()),
	callback(ACSAlarm_Type::_nil()),
	ownerInstance(_ownerInstance),
	myCallBack_raised(_myCB_raised),
	myCallBack_raised_withValue(0),
	myCallBack_cleared(_myCB_cleared),
	myCallBack_cleared_withValue(0)
	{
		ACS_SHORT_LOG((LM_INFO,"myAlarm::myAlarm: The alarm will observe the property \"%s\".",prop.c_str()));
	};

 	myAlarm_Template(const std::string& _prop,void(owner_class::*_myCB_raised)(const corba_class&),void(owner_class::*_myCB_cleared)(const corba_class&),owner_class* _ownerInstance)
	:prop(_prop),
	alarm_var(ACS::Subscription::_nil()),
	callback(ACSAlarm_Type::_nil()),
	ownerInstance(_ownerInstance),
	myCallBack_raised(0),
	myCallBack_raised_withValue(_myCB_raised),
	myCallBack_cleared(0),
	myCallBack_cleared_withValue(_myCB_cleared)
	{
		ACS_SHORT_LOG((LM_INFO,"myAlarm::myAlarm: The alarm will observe the property \"%s\".",prop.c_str()));
	};

 	myAlarm_Template(const std::string& _prop,void(owner_class::*_myCB_raised)(const corba_class&),void(owner_class::*_myCB_cleared)(void),owner_class* _ownerInstance)
	:prop(_prop),
	alarm_var(ACS::Subscription::_nil()),
	callback(ACSAlarm_Type::_nil()),
	ownerInstance(_ownerInstance),
	myCallBack_raised(0),
	myCallBack_raised_withValue(_myCB_raised),
	myCallBack_cleared(_myCB_cleared),
	myCallBack_cleared_withValue(0)
	{
		ACS_SHORT_LOG((LM_INFO,"myAlarm::myAlarm: The alarm observe the property \"%s\".",prop.c_str()));
	};

 	myAlarm_Template(const std::string& _prop,void(owner_class::*_myCB_raised)(void),void(owner_class::*_myCB_cleared)(const corba_class&),owner_class* _ownerInstance)
	:prop(_prop),
	alarm_var(ACS::Subscription::_nil()),
	callback(ACSAlarm_Type::_nil()),
	ownerInstance(_ownerInstance),
	myCallBack_raised(_myCB_raised),
	myCallBack_raised_withValue(0),
	myCallBack_cleared(0),
	myCallBack_cleared_withValue(_myCB_cleared)
	{
		ACS_SHORT_LOG((LM_INFO,"myAlarm::myAlarm: The alarm will observe the property \"%s\".",prop.c_str()));
	};

	virtual ~myAlarm_Template(void)
	{
		if(!CORBA::is_nil(alarm_var.ptr()))
		{
		try
			{
			  alarm_var->destroy();
			  alarm_var=ACS::Subscription::_nil();
			  callback=ACSAlarm_Type::_nil();
			  ACS_SHORT_LOG((LM_INFO,"myAlarm::~myAlarm: %s alarm destroyed.",prop.c_str()));
			}
		catch(...)
			{
		  	ACS_SHORT_LOG((LM_ERROR,"myAlarm::~myAlarm: Something weird happed while destroying the current alarm. Check the source code for bugs!"));
			};
	  }
	};

	virtual bool create_alarm(ACSProperty_Type_var property)
	{
		bool ret(false);

	  callback=this->_this();
	  alarm_var=property->new_subscription_Alarm(callback.in(),callback_description);
	  if(!CORBA::is_nil(alarm_var.ptr()))
	  {
		ACS_SHORT_LOG((LM_INFO,"myAlarm::create_alarm: %s alarm object created.",prop.c_str()));
		ret=true;
	  }
	  else
	  {
		ACS_SHORT_LOG((LM_ERROR,"myAlarm::create_alarm: %s alarm object NOT created. It seems that the property is not available.",prop.c_str()));
		alarm_var->destroy();
		  alarm_var=ACS::Subscription::_nil();
		  callback=ACSAlarm_Type::_nil();
	  }
	  return ret;
	};

	virtual void alarm_raised(corba_class value,const ACSErr::Completion& c,const ACS::CBDescOut& desc) throw (CORBA::SystemException)
	{
 		if(myCallBack_raised_withValue)
 		{
 			(ownerInstance->*myCallBack_raised_withValue)(value);
 		}
 		else if(myCallBack_raised)
 		{
 			(ownerInstance->*myCallBack_raised)();
 		}
	};

	virtual void alarm_cleared(corba_class value,const ACSErr::Completion& c,const ACS::CBDescOut& desc) throw (CORBA::SystemException)
	{
 		if(myCallBack_cleared_withValue)
 		{
 			(ownerInstance->*myCallBack_cleared_withValue)(value);
 		}
 		else if(myCallBack_cleared)
 		{
 			(ownerInstance->*myCallBack_cleared)();
 		}
	};

	virtual void done(corba_class,const ACSErr::Completion&,const ACS::CBDescOut&) throw (CORBA::SystemException)
	{
		ACS_SHORT_LOG((LM_INFO,"myAlarm::done: The alarm will be destroyed."));
	}

	virtual CORBA::Boolean negotiate(ACS::TimeInterval,const ACS::CBDescOut&) throw (CORBA::SystemException)
	{
		ACS_SHORT_LOG((LM_INFO,"myAlarm::negotiate: No idea when this method (myAlarm::negotiate) will be called."));
		return true;
	};

	protected:
  std::string prop;

	ACS::Subscription_var alarm_var;
	ACSAlarm_Type_var callback;
	ACS::CBDescIn callback_description;
	owner_class* ownerInstance;

  void(owner_class::*myCallBack_raised)(void);
  void(owner_class::*myCallBack_raised_withValue)(const corba_class&);
  void(owner_class::*myCallBack_cleared)(void);
  void(owner_class::*myCallBack_cleared_withValue)(const corba_class&);

 	private:
 	myAlarm_Template();
 	myAlarm_Template(const myAlarm_Template&);
 	void operator=(const myAlarm_Template&);
};

class myCBvoid: public virtual POA_ACS::CBvoid
{
	public:
	myCBvoid(void);
	virtual ~myCBvoid(void);
	virtual void working(const ACSErr::Completion&,const ACS::CBDescOut&) throw (CORBA::SystemException);
	virtual void done(const ACSErr::Completion&,const ACS::CBDescOut&) throw (CORBA::SystemException);
	virtual CORBA::Boolean negotiate(ACS::TimeInterval,const ACS::CBDescOut&) throw (CORBA::SystemException);
	virtual bool is_done(void) const;

	protected:
	bool ready;
};
#endif
