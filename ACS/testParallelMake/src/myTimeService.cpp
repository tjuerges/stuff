/*
  "@(#) $Id$"
*/

#include <hptStarUtils.h>

myTimeService::myTimeService(void):array2tai(0L),tai2utc(0L)
{
	//timeservice=acstime::Clock::_nil();
}

myTimeService::~myTimeService(void)
{
  //disconnect();
}

void myTimeService::disconnect(void)
{
/*
  if(!CORBA::is_nil(timeservice))
	{
  	try
		{
	  	ACS_SHORT_LOG((LM_INFO,"myTimeService::disconnect_time_service: Releasing component Clock."));

		  CORBA::release(timeservice);

		  ACS_SHORT_LOG((LM_INFO,"myTimeService::disconnect: Releasing component Clock done."));
		}
  	catch(...)
		{
	  	ACS_SHORT_LOG((LM_ERROR,"myTimeService::disconnect: Something weird happened during release of component Clock."));
		};

  	timeservice=acstime::Clock::_nil();
  }
*/
}

bool myTimeService::connect(void)
{
/*
  bool ret=false;

  ACS_SHORT_LOG((LM_INFO,"myTimeService::connect: Connecting to hptTimeService..."));
  try
  {
  	CORBA::Object_ptr corbaobject=maci::ContainerImpl::getContainer()->get_object("Clock",0,true);
    if(!CORBA::is_nil(corbaobject))
    {
			timeservice=acstime::Clock::_narrow(corbaobject);
			if(!CORBA::is_nil(timeservice))
	    {
	      ACSErr::Completion_var completion;
	      ACS::CharacteristicComponentDesc_var descriptor=timeservice->descriptor();
	      ACS_SHORT_LOG((LM_INFO,"Got descriptor for time service.\nDescriptor of time service:\n\tname: %s",descriptor->name.in()));
	      array2tai=static_cast<long>(timeservice->array2TAI()->get_sync(completion.out()));
	      tai2utc=static_cast<long>(timeservice->TAI2UTC()->get_sync(completion.out()));
	      ret=true;
	    }
	  	else
	    {
	      ACS_SHORT_LOG((LM_ERROR,"myTimeService::connect: Could not narrow CORBA object for time service."));
	      timeservice=acstime::Clock::_nil();
	      ret=false;
	    }
		}
    else
		{
	  	ACS_SHORT_LOG((LM_ERROR,"myTimeService::connect: Could not connect to time service. No appropriate objects found."));
	  	timeservice=acstime::Clock::_nil();
	  	ret=false;
		}
    CORBA::release(corbaobject);
  }
  catch(...)
  {
    timeservice=acstime::Clock::_nil();
    ACS_SHORT_LOG((LM_ERROR,"myTimeService::connect: Access to time service failed."));
    return ret;
  };

  return ret;
*/
	return true;
}

void myTimeService::get_time(std::string& myTime,double& myEpoch) const
{
/*  if(!CORBA::is_nil(timeservice))
  {
    try
		{
		  ACSErr::Completion_var completion;
		  acstime::Epoch my_epoch;
		  acstime::Epoch my_epoch(TimeUtil::ace2epoch(ACE_OS::gettimeofday()));
		  EpochHelper my_epochhelper;
		  long milliseconds=0L;

		  my_epoch.value=timeservice->now()->get_sync(completion.out());
	  	std::ostringstream iso_timedate;
	  	iso_timedate<<my_epochhelper.toString(acstime::TSUTC,"%G",array2tai,tai2utc);
	  	milliseconds=static_cast<long>(my_epochhelper.microSecond()/CORBA::Long(1000));
	  	iso_timedate<<"."<<milliseconds<<std::ends;
	  	myTime=iso_timedate.str();
	  	myEpoch=my_epochhelper.toJulianYear(array2tai,tai2utc);
		}
    catch(...)
		{
	  	ACS_SHORT_LOG((LM_ERROR,"myTimeService::get_time:  Exception while getting time from hptTimeService."));
		};
  }
*/
//Eventuell geht auch  acstime::Epoch my_epoch(TimeUtil::ace2epoch(baci::getTimeStamp()));
  acstime::Epoch my_epoch(TimeUtil::ace2epoch(ACE_OS::gettimeofday()));
  EpochHelper my_epochhelper;
  long milliseconds=0L;

 	my_epochhelper.value(my_epoch);
 	std::ostringstream iso_timedate;
	iso_timedate<<my_epochhelper.toString(acstime::TSUTC,"%G",array2tai,tai2utc);
 	milliseconds=static_cast<long>(my_epochhelper.microSecond()/CORBA::Long(1000));
 	iso_timedate<<"."<<milliseconds<<std::ends;
 	myTime=iso_timedate.str();
 	myEpoch=my_epochhelper.toJulianYear(array2tai,tai2utc);
}

void myTimeService::get_time(std::string& myTime) const
{
/*
  if(!CORBA::is_nil(timeservice))
  {
    try
		{
	  	ACSErr::Completion_var completion;
		  acstime::Epoch my_epoch;
		  EpochHelper my_epochhelper;
		  long milliseconds=0L;

		  my_epoch.value=timeservice->now()->get_sync(completion.out());
		  my_epoch.value=TimeUtil::ace2epoch(ACE_OS::gettimeofday());
		  std::ostringstream iso_timedate;
		  iso_timedate<<my_epochhelper.toString(acstime::TSUTC,"%G",array2tai,tai2utc);
		  milliseconds=static_cast<long>(my_epochhelper.microSecond()/CORBA::Long(1000));
		  iso_timedate<<"."<<milliseconds<<std::ends;
		  myTime=iso_timedate.str();
		}
    catch(...)
		{
			ACS_SHORT_LOG((LM_ERROR,"myTimeService::get_time: Exception while getting time from hptTimeService."));
		};
  }
*/
  EpochHelper my_epochhelper;
  long milliseconds=0L;
  acstime::Epoch my_epoch(TimeUtil::ace2epoch(ACE_OS::gettimeofday()));

  my_epochhelper.value(my_epoch);
  std::ostringstream iso_timedate;
  iso_timedate<<my_epochhelper.toString(acstime::TSUTC,"%G",array2tai,tai2utc);
  milliseconds=static_cast<long>(my_epochhelper.microSecond()/CORBA::Long(1000));
  iso_timedate<<"."<<milliseconds<<std::ends;
  myTime=iso_timedate.str();
}

void myTimeService::get_time(ACS::Time& myTime) const
{
/*
  if(!CORBA::is_nil(timeservice))
  {
    try
    {
			ACSErr::Completion_var completion;
      myTime=timeservice->now()->get_sync(completion.out());
		}
    catch(...)
    {
	  	ACS_SHORT_LOG((LM_ERROR,"myTimeService::get_time: Exception while getting time from hptTimeService."));
		};
	}
*/
  acstime::Epoch my_epoch(TimeUtil::ace2epoch(ACE_OS::gettimeofday()));
	myTime=my_epoch.value;
}
