/*
 *
 * "@(#) $Id$"
 *
 * $Log: myCBvoid.cpp,v $
 * Revision 1.3  2005/05/27 14:36:34  tjuerges
 * - Made method is_done const.
 *
 * Revision 1.2  2005/05/24 15:53:28  tjuerges
 * - Removed unnecessary semicolon.
 *
 * Revision 1.1  2004/08/17 11:59:37  tjuerges
 * - dummyCBvoid nach myCBvoid umbenannt, ist keine Inline-Klasse mehr.
 *
 *
*/

#include <hptStarUtils.h>

myCBvoid::myCBvoid(void):ready(false)
{
}

myCBvoid::~myCBvoid(void)
{
}

void myCBvoid::working(const ACSErr::Completion& c,const ACS::CBDescOut& desc) throw(CORBA::SystemException)
{
}
 
void myCBvoid::done(const ACSErr::Completion& c,const ACS::CBDescOut& desc) throw(CORBA::SystemException)
{
	ready=true;
}

CORBA::Boolean myCBvoid::negotiate(ACS::TimeInterval time_to_transmit,const ACS::CBDescOut& desc) throw(CORBA::SystemException)
{
 	return true;
}

bool myCBvoid::is_done(void) const
{
	return ready;
}
