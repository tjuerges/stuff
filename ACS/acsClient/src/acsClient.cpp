/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Feb 27, 2007  created
*/

#include <acsClient.h>
#include <maciSimpleClient.h>
#include <cstdlib>
#include <sstream>
#include <loggingMACROS.h>
#include <acsThreadManager.h>
#include <acsThread.h>
#include <acsClientEventLoop.h>
#include <Guard_T.h>
#include <Mutex.h>


maci::AcsClient::AcsClient(int argc, char* argv[]):
    maci::SimpleClient::SimpleClient(),
    alreadyLoggedOut(false)
{
    AUTO_TRACE("maci::AcsClient::AcsClient");

    const int rnd(std::rand());
    std::ostringstream randomNumber;
    randomNumber << rnd;
    EventThreadName = "AcsClientEventThread";
    EventThreadName.append(randomNumber.str());

    if(init(argc, argv) == 0)
    {
        ACS_SHORT_LOG((LM_ERROR, "maci::AcsClient::AcsClient: "
            "Could not initialise maci::SimpleClient."));
        throw Exception();
    }
    else
    {
        if(login() == 0)
        {
            ACS_SHORT_LOG((LM_ERROR, "maci::AcsClient::AcsClient: "
                "Could not login with maci::SimpleClient."));
            throw Exception();
        }
        else
        {
            maci::AcsClient* _this(this);
            threadManager.create< EventLoop, maci::AcsClient* >(
                EventThreadName.c_str(), _this);
            threadManager.resume(EventThreadName.c_str());
       }
    }
}

int maci::AcsClient::logout()
{
    AUTO_TRACE("maci::AcsClient::logout");

    ACE_Guard< ACE_Mutex > guard(lock);

    if(alreadyLoggedOut == false)
    {
        alreadyLoggedOut = true;
        return maci::SimpleClient::logout();
    }

    return 1;
}

maci::AcsClient::~AcsClient()
{
    AUTO_TRACE("maci::AcsClient::~AcsClient");

    threadManager.stop(EventThreadName.c_str());
    maci::AcsClient::logout();
}

char* maci::AcsClient::name()
{
    AUTO_TRACE("maci::AcsClient::name");

    return CORBA::string_dup("AcsClient");
}

char* maci::AcsClient::authenticate(const char* question)
{
    AUTO_TRACE("maci::AcsClient::authenticate");

    return CORBA::string_dup("CACS Client");
}
