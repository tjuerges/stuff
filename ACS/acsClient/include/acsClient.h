#ifndef ACSCLIENT_H_
#define ACSCLIENT_H_
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Feb 27, 2007  created
*/

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

#include <string>
#include <acsThreadManager.h>
#include <maciSimpleClient.h>
#include <exception>
#include <Mutex.h>


namespace maci
{
    class AcsClient: public maci::SimpleClient
    {
        public:
        AcsClient(int argc, char* argv[]);
        virtual ~AcsClient();
        virtual int logout();
        virtual char* name()
        virtual char* authenticate(const char* question);

        class Exception: public std::exception
        {
        };

        private:
        AcsClient();
        AcsClient(const AcsClient&);
        AcsClient& operator=(const AcsClient&);

        std::string EventThreadName;
        ACS::ThreadManager threadManager;
        bool alreadyLoggedOut;
        ACE_Mutex lock;
    };
};

#endif /*ACSCLIENT_H_*/
