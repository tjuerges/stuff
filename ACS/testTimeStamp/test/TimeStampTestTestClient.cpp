/*
 * "@(#) $Id$"
 */

#include <acsutil.h>
#include <maciSimpleClient.h>
#include <baciC.h>
#include <logging.h>
#include <string>

#include <testTimeStampC.h>

int main(int argc, char* argv[])
{
    // Creates and initializes the SimpleClient object
    maci::SimpleClient client;

    if(client.init(argc,argv) == 0)
    {
        ACS_SHORT_LOG((LM_ERROR,"Cannot init client"));
        return -1;
    }

    std::string component_name("TimeStampTest");
    // Now get the specific component we have requested on the command line.
    if(argc==2)
    {
        component_name=argv[1];
    }

    ACS_SHORT_LOG((LM_INFO, "Welcome to the TimeStampTestTestClient!"));
    ACS_SHORT_LOG((LM_INFO, "Login in maciManager..."));
    client.login();

    try
    {
        // List all components the Manager knows of our type.
        ACS_SHORT_LOG((LM_INFO, "Listing all componentes of type *TimeStampTest*"));
        maci::HandleSeq seq;
        maci::ComponentInfoSeq_var components = client.manager()->get_component_info(client.handle(), seq, "*", "*TimeStampTest*", false);

        for(CORBA::ULong i=0; i<components->length(); i++)
        {
            ACS_SHORT_LOG((LM_INFO,"%s (%s)", components[i].name.in(), components[i].type.in()));
        }

        // Now get the specific component we have requested on the command line.
        ACS_SHORT_LOG((LM_INFO, "Getting component %s...",component_name.c_str()));
        Test::TimeStampTest_var component = client.get_object<Test::TimeStampTest>(component_name.c_str(), 0, true);

        if(!CORBA::is_nil(component.in()))
        {
            // Prints the descriptor of the requested component.
            ACS_SHORT_LOG((LM_INFO, "Requesting descriptor()... "));
            ACS::CharacteristicComponentDesc_var descriptor = component->descriptor();

            ACS_SHORT_LOG((LM_INFO,"Descriptor:"));
            ACS_SHORT_LOG((LM_INFO,"\tname: %s", descriptor->name.in()));

			ACSErr::Completion_var completion;

			ACS_SHORT_LOG((LM_INFO, "Getting component property: %s/bar", component_name.c_str()));
			if(CORBA::is_nil(component->bar()) == false)
			{ 
				CORBA::Long value = component->bar()->get_sync(completion.out());
				ACS_SHORT_LOG((LM_INFO, "bar = %d", value))
			}
		} /* end if component reference OK */
	} /* end main try */
	catch(...)
	{
		ACS_SHORT_LOG((LM_ERROR, "Error in TimeStampTestTestClient::main!"));
	}

	/* Another try section where we release our component
	 * and logout from the Manager.
	 */
	try
	{
		ACS_SHORT_LOG((LM_INFO,"Releasing..."));
		client.manager()->release_component(client.handle(), component_name.c_str());
		client.logout();
	}
	catch(...)
	{
		ACS_SHORT_LOG((LM_ERROR, "Error in TimeStampTestTestClient::main!"));
	}

	/*
	 * sleep for 3 sec to allow everytihng to cleanup and stabilize
	 * so that the tests can be determinitstic.
	 */
	ACE_OS::sleep(3);
	return 0;
}
