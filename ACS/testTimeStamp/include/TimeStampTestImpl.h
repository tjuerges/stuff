#ifndef TimeStampTestImpl_h
#define TimeStampTestImpl_h

/*
 * "@(#) $Id$"
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <baciCharacteristicComponentImpl.h>

#include <testTimeStampS.h>

#include <baciRWlong.h>


#include <baciSmartPropertyPointer.h>

#include <TimeStampTest_IncludeFiles.h>


class TimeStampTestImpl: public virtual CharacteristicComponentImpl,
    public virtual POA_Test::TimeStampTest
{
    public:
    /**
     * Constructor
     * @param containerServices ContainerServices which are needed for various component
     * related methods.
     * @param name component name
     */
    TimeStampTestImpl(const ACE_CString& name, maci::ContainerServices* containerServices);

    /**
     * Destructor
     */
    virtual ~TimeStampTestImpl();

    /**
     * Lifecycle method called at component initialization state.
     * Overwrite it in case you need it.
     */
    virtual void initialize(void) throw (acsErrTypeLifeCycle::acsErrTypeLifeCycleExImpl);

    /**
     * Lifecycle method called at component execute state.
     * Overwrite it in case you need it.
     */
    virtual void execute(void) throw (acsErrTypeLifeCycle::acsErrTypeLifeCycleExImpl);

    /**
     * Lifecycle method called at component clean up state.
     * Overwrite it in case you need it.
     */
    virtual void cleanUp(void);

    /**
     * Lifecycle method called at component abort state.
     * Overwrite it in case you need it.
     */
    virtual void aboutToAbort(void);




/* --------------------- [ CORBA START interface ] ----------------*/
	virtual ACS::RWlong_ptr bar() throw(CORBA::SystemException);

	virtual void foo() throw(CORBA::SystemException);


/* --------------------- [ CORBA END interface ] ----------------*/

/* ----------------------------------------------------------------*/

    private:
    /**
     * Copy constructor is not allowed following the ACS desgin rules.
     */
    TimeStampTestImpl(const TimeStampTestImpl&);

    /**
     * Assignment operator is not allowed due to ACS design rules.
     */
    void operator=(const TimeStampTestImpl&);

/* --------------------- [ Properties START ] ----------------------*/

    maci::ContainerServices* container;
    std::string component_name_m;
	SmartPropertyPointer< baci::RWlong > bar_m;

/* --------------------- [ Properties END ] ------------------------*/

/* --------------------- [ Local properties START ] ----------------*/
#include <TimeStampTestImpl_LocalProperties.h>
/* --------------------- [ Local properties END ] ------------------*/
};


#endif /* TimeStampTestImpl_h */
