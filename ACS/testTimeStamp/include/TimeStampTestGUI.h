/****************************************************************************
** Form interface generated from reading ui file 'TimeStampTestGUI.ui'
**
** Created: Wed Jan 31 13:43:51 2007
**      by: The User Interface Compiler ($Id$)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef TIMESTAMPTESTGUI_H
#define TIMESTAMPTESTGUI_H

#include <qvariant.h>
#include <qwidget.h>
#include <maciSimpleClient.h>
#include <acserr.h>
#include <logging.h>
#include <tao/SystemException.h>
#include <testTimeStampC.h>
#include <qapplication.h>
#include <TimeStampTestGUIEventLoop.h>
#include <qmessagebox.h>
#include <string>
#include <hptStarUtils.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QPushButton;
template < class T > class myComponentClient;
extern EventLoop* myEventLoop;
extern maci::SimpleClient* sc;
extern QApplication* qApp;

class TimeStampTestGUI : public QWidget
{
    Q_OBJECT

public:
    TimeStampTestGUI( QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~TimeStampTestGUI();

    QPushButton* Connect;
    QPushButton* On;
    QPushButton* Off;
    QPushButton* Disconnect;
    QPushButton* Quit;

protected:
    Test::TimeStampTest_var component;
    ACSErr::Completion_var completion;
    ACS::Time timestamp;
    myComponentClient< Test::TimeStampTest >* myClient;
    std::string component_name;

    virtual void setup_monitors(void);
    virtual void destroy_monitors(void);

    QVBoxLayout* layout2;

protected slots:
    virtual void languageChange();

    virtual void on(void);
    virtual void off(void);
    virtual void byebye();
    virtual void disconnect_from_component(void);
    virtual void connect_to_component(void);


private:
    void init();
    void destroy();

};

#endif // TIMESTAMPTESTGUI_H
