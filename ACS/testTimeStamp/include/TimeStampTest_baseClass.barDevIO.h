#ifndef TimeStampTest_baseClass_barDevIO_h
#define TimeStampTest_baseClass_barDevIO_h

/*
 * "@(#) $Id$"
 */

#include <baciDevIO.h>
#include <acstime.h>

/*
 * Definition of barDevIO base class.
 */

class barDevIO: public DevIO< CORBA::Long >
{
	public:
	barDevIO();
	virtual ~barDevIO();
	virtual CORBA::Long read(ACS::Time& timestamp) throw(ACSErr::ACSbaseExImpl);
	virtual void write(const CORBA::Long& value, ACS::Time& timestamp) throw(ACSErr::ACSbaseExImpl);

	private:
	
};
#endif
