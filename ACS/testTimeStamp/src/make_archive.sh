#!/bin/bash
#
#  "@(#) $Id$"
#
#  $Log$
#

INTERFACE_NAME=TimeStampTest

make clean

pushd ..

if [ -d ../ws ]; then
	pushd ..
	tar --exclude={.svn,CVS,*~,*.bak,*.o,lib*,*.a,#*} -cjf $INTERFACE_NAME.tar.bz2 ./{.project,.cdtproject} ws/{config,idl,include,rtai,src,test} lcu/{config,idl,include,rtai,src,test}
	popd
else
	tar --exclude={.svn,CVS,*~,*.bak,*.o,lib*,*.a,#*} -cjf $INTERFACE_NAME.tar.bz2 ./{.project,.cdtproject,config,idl,include,rtai,src,test}
fi

popd
exit 0
