/****************************************************************************
 ** ui.h extension file, included from the uic-generated form implementation.
 **
 ** If you wish to add, delete or rename functions or slots use
 ** Qt Designer which will update this file, preserving your code. Create an
 ** init() function in place of a constructor, and a destroy() function in
 ** place of a destructor.
 *****************************************************************************/
/*
 * "@(#) $Id$"
 */

void TimeStampTestGUI::init()
{
    myClient = 0;
    component = Test::TimeStampTest::_nil();
    if(qApp->argc() == 2)
    {
        component_name = qApp->argv()[1];
    }
    else
    {
        component_name = "TimeStampTest";
    }
}

void TimeStampTestGUI::destroy()
{
    disconnect_from_component();
}

void TimeStampTestGUI::connect_to_component(void)
{
    myClient = new myComponentClient< Test::TimeStampTest >(component_name, sc);
    myClient->connect();
    component = myClient->get_object();

    if(CORBA::is_nil(component.in()) == true)
    {
        delete myClient;
        myClient = 0;

        QMessageBox::critical
        (
            this,
            QString("TimeStampTestGUI"),
            QString("Unable to connect to the wanted component %1.\n"
                "If the component is already running:\n\n"
                "- check the hardware\n").arg(component_name.c_str())
        );
    }
    else
    {
        Connect->setEnabled(false);
        Disconnect->setEnabled(true);
        On->setEnabled(true);
        Off->setEnabled(false);
        setup_monitors();
    }
}

void TimeStampTestGUI::disconnect_from_component(void)
{
    off();
    destroy_monitors();

    delete myClient;
    myClient = 0;

    component = Test::TimeStampTest::_nil();

    Connect->setEnabled(true);
    Disconnect->setEnabled(false);
    On->setEnabled(false);
    Off->setEnabled(false);
}

void TimeStampTestGUI::on(void)
{
    if(CORBA::is_nil(component.in()) == false)
    {
        Connect->setEnabled(false);
        Disconnect->setEnabled(true);
        On->setEnabled(false);
        Off->setEnabled(true);

        /*
         * Be careful!
         * This is a call to the synchronous
         * method on() in the component.
         * If you have an asynchronous on
         * method, create a callback and a
         * callback description. Call then
         * component->on(callback,callback_description);
         *
         * component->on();
         */
    }
}

void TimeStampTestGUI::off(void)
{
    if(CORBA::is_nil(component.in()) == false)
    {
        /*
         * Be careful!
         * This is a call to the synchronous
         * method off() in the component.
         * If you have an asynchronous off
         * method, create a callback and a
         * callback description. Call then
         * component->off(callback,callback_description);
         *
         * component->off();
         */

        Connect->setEnabled(false);
        Disconnect->setEnabled(true);
        On->setEnabled(true);
        Off->setEnabled(false);
    }
}

void TimeStampTestGUI::byebye()
{
    disconnect_from_component();
    close();
}

void TimeStampTestGUI::setup_monitors(void)
{
    if(CORBA::is_nil(component.in()) == true)
    {
        ACS_SHORT_LOG((LM_ERROR,"TimeStampTestGUI::setup_monitors: "
            "Cannot create monitors. Component is not active."));
        return;
    }

/*
 * monitor = new myMonitor(double, TimeStampTestGUI)(
 *      std::string("Monitor description"),
 *      &TimeStampTestGUI::SET_THE_MONITOR_METHOD_HERE, this);
 * if(monitor->create_monitor(
 *      component->TYPE_THE_COMPONENTS_PROPERTY_HERE(),
 *      ENTER_TIMER_TRIGGER_AS_DOUBLE_HERE,
 *      ENTER_DELTA_VALUE_AS_TYPE_OF_PROPERTY_HERE,
 *      ENABLE_OR_DISABLE_DELTA_MONITOR_true_OR_false) == false)
 * {
 *     delete monitor;
 *     monitor = 0;
 * }
 */
}

void TimeStampTestGUI::destroy_monitors(void)
{

/*
 * delete monitor;
 * monitor = 0;
 */
}

/*
 * An example for a monitor method, which receives the
 * value of the monitored property.
 *
 * Attention: if you want to modify a widget,
 * e.g. display a text or a number, you must
 * enclose the action by the QApplication's
 * locking mechanism. Otherwise many SIGSEGs will
 * occur in Qt's event loop! Therefore the
 * QApplication pointer qApp is declared globally.
 *
 * TimeStampTestGUI::monitor_method_for_double(const CORBAdouble& value)
 * {
 *         Do something with the value.
 *        qApp->lock();
 *         // Set a widget
 *         qApp->unlock();
 * }
 *
 *
 *
 * The monitor can be a method which ignores the
 * current value of the property as well.
 *
 * Attention: if you want to modify a widget,
 * e.g. display a text or a number, you must
 * enclose the action by the QApplication's
 * locking mechanism. Otherwise many SIGSEGs will
 * occur in Qt's event loop! Therefore the
 * QApplication pointer qApp is declared globally.
 *
 * TimeStampTestGUI::monitor_method_for_double()
 * {
 *         Do something.
 *        qApp->lock();
 *         // Set a widget
 *         qApp->unlock();
 * }
 */
