/****************************************************************************
** TimeStampTestGUI meta object code from reading C++ file 'TimeStampTestGUI.h'
**
** Created: Wed Jan 31 13:43:51 2007
**      by: The Qt MOC ($Id$)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#undef QT_NO_COMPAT
#include "../include/TimeStampTestGUI.h"
#include <qmetaobject.h>
#include <qapplication.h>

#include <private/qucomextra_p.h>
#if !defined(Q_MOC_OUTPUT_REVISION) || (Q_MOC_OUTPUT_REVISION != 26)
#error "This file was generated using the moc from 3.3.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

const char *TimeStampTestGUI::className() const
{
    return "TimeStampTestGUI";
}

QMetaObject *TimeStampTestGUI::metaObj = 0;
static QMetaObjectCleanUp cleanUp_TimeStampTestGUI( "TimeStampTestGUI", &TimeStampTestGUI::staticMetaObject );

#ifndef QT_NO_TRANSLATION
QString TimeStampTestGUI::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "TimeStampTestGUI", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString TimeStampTestGUI::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "TimeStampTestGUI", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* TimeStampTestGUI::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = QWidget::staticMetaObject();
    static const QUMethod slot_0 = {"languageChange", 0, 0 };
    static const QUMethod slot_1 = {"on", 0, 0 };
    static const QUMethod slot_2 = {"off", 0, 0 };
    static const QUMethod slot_3 = {"byebye", 0, 0 };
    static const QUMethod slot_4 = {"disconnect_from_component", 0, 0 };
    static const QUMethod slot_5 = {"connect_to_component", 0, 0 };
    static const QMetaData slot_tbl[] = {
	{ "languageChange()", &slot_0, QMetaData::Protected },
	{ "on()", &slot_1, QMetaData::Protected },
	{ "off()", &slot_2, QMetaData::Protected },
	{ "byebye()", &slot_3, QMetaData::Protected },
	{ "disconnect_from_component()", &slot_4, QMetaData::Protected },
	{ "connect_to_component()", &slot_5, QMetaData::Protected }
    };
    metaObj = QMetaObject::new_metaobject(
	"TimeStampTestGUI", parentObject,
	slot_tbl, 6,
	0, 0,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_TimeStampTestGUI.setMetaObject( metaObj );
    return metaObj;
}

void* TimeStampTestGUI::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "TimeStampTestGUI" ) )
	return this;
    return QWidget::qt_cast( clname );
}

bool TimeStampTestGUI::qt_invoke( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->slotOffset() ) {
    case 0: languageChange(); break;
    case 1: on(); break;
    case 2: off(); break;
    case 3: byebye(); break;
    case 4: disconnect_from_component(); break;
    case 5: connect_to_component(); break;
    default:
	return QWidget::qt_invoke( _id, _o );
    }
    return TRUE;
}

bool TimeStampTestGUI::qt_emit( int _id, QUObject* _o )
{
    return QWidget::qt_emit(_id,_o);
}
#ifndef QT_NO_PROPERTIES

bool TimeStampTestGUI::qt_property( int id, int f, QVariant* v)
{
    return QWidget::qt_property( id, f, v);
}

bool TimeStampTestGUI::qt_static_property( QObject* , int , int , QVariant* ){ return FALSE; }
#endif // QT_NO_PROPERTIES
