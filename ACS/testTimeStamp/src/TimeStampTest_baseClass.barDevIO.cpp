/*
 * "@(#) $Id$"
 */

#include "TimeStampTest_baseClass.barDevIO.h"
#include <acscommonC.h>

#include <acsutilTimeStamp.h>

/*
 * barDevIO base class.
 */

barDevIO::barDevIO()
{
}

barDevIO::~barDevIO()
{
}

CORBA::Long barDevIO::read(ACS::Time& timestamp) throw(ACSErr::ACSbaseExImpl)
{
	CORBA::Long ret_val(static_cast< CORBA::Long >(0));
	timestamp = ::getTimeStamp();

	return ret_val;
}

void barDevIO::write(const CORBA::Long& value, ACS::Time& timestamp) throw(ACSErr::ACSbaseExImpl)
{
	/**
	 * Do something with value.
	 */
	timestamp = ::getTimeStamp();
}

