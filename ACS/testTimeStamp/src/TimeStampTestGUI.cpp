/****************************************************************************
** Form implementation generated from reading ui file 'TimeStampTestGUI.ui'
**
** Created: Wed Jan 31 13:43:51 2007
**      by: The User Interface Compiler ($Id$)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "../include/TimeStampTestGUI.h"

#include <qvariant.h>
#include <qpushbutton.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include "TimeStampTestGUI.ui.h"

/*
 *  Constructs a TimeStampTestGUI as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
TimeStampTestGUI::TimeStampTestGUI( QWidget* parent, const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "TimeStampTestGUI" );
    setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)5, 0, 0, sizePolicy().hasHeightForWidth() ) );
    setMinimumSize( QSize( 10, 10 ) );
    setMaximumSize( QSize( 32767, 32767 ) );

    QWidget* privateLayoutWidget = new QWidget( this, "layout2" );
    privateLayoutWidget->setGeometry( QRect( 345, 10, 105, 260 ) );
    layout2 = new QVBoxLayout( privateLayoutWidget, 10, 4, "layout2"); 

    Connect = new QPushButton( privateLayoutWidget, "Connect" );
    layout2->addWidget( Connect );

    On = new QPushButton( privateLayoutWidget, "On" );
    On->setEnabled( FALSE );
    layout2->addWidget( On );

    Off = new QPushButton( privateLayoutWidget, "Off" );
    Off->setEnabled( FALSE );
    layout2->addWidget( Off );

    Disconnect = new QPushButton( privateLayoutWidget, "Disconnect" );
    Disconnect->setEnabled( FALSE );
    layout2->addWidget( Disconnect );

    Quit = new QPushButton( privateLayoutWidget, "Quit" );
    layout2->addWidget( Quit );
    languageChange();
    resize( QSize(461, 289).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );

    // signals and slots connections
    connect( On, SIGNAL( clicked() ), this, SLOT( on() ) );
    connect( Off, SIGNAL( clicked() ), this, SLOT( off() ) );
    connect( Connect, SIGNAL( clicked() ), this, SLOT( connect_to_component() ) );
    connect( Disconnect, SIGNAL( clicked() ), this, SLOT( disconnect_from_component() ) );
    connect( Quit, SIGNAL( clicked() ), this, SLOT( byebye() ) );
    init();
}

/*
 *  Destroys the object and frees any allocated resources
 */
TimeStampTestGUI::~TimeStampTestGUI()
{
    destroy();
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void TimeStampTestGUI::languageChange()
{
    setCaption( tr( "TimeStampTestGUI" ) );
    Connect->setText( tr( "Connect" ) );
    On->setText( tr( "On" ) );
    Off->setText( tr( "Off" ) );
    Disconnect->setText( tr( "Disconnect" ) );
    Quit->setText( tr( "Quit" ) );
}

