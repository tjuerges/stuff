/*
 * "@(#) $Id$"
 */

#include <TimeStampTestImpl.h>
#include "TimeStampTestImpl_IncludeFiles.h"
#include "TimeStampTestImpl_HelperFunctions.cpp"

#include "TimeStampTest_baseClass.barDevIO.h"

TimeStampTestImpl::TimeStampTestImpl(const ACE_CString& name, maci::ContainerServices* containerServices)
	:CharacteristicComponentImpl(name, containerServices)
	, container(containerServices)
	,bar_m(this)
{
	ACS_TRACE("TimeStampTestImpl::TimeStampTestImpl");
	component_name_m = name.c_str();

	#include <TimeStampTestImpl_Init.cpp>
}


TimeStampTestImpl::~TimeStampTestImpl()
{
	ACS_TRACE("TimeStampTestImpl::~TimeStampTestImpl");
}


void TimeStampTestImpl::initialize(void)
	throw (acsErrTypeLifeCycle::acsErrTypeLifeCycleExImpl)
{
	ACS_TRACE("TimeStampTestImpl::initialize");
	if(getComponent() != 0)
	{
		#include <TimeStampTestImpl_initialize.body.cpp>
	}
}


void TimeStampTestImpl::execute(void)
	throw (acsErrTypeLifeCycle::acsErrTypeLifeCycleExImpl)
{
	ACS_TRACE("TimeStampTestImpl::execute");
	if(getComponent() != 0)
	{
		#include <TimeStampTestImpl_execute.body.cpp>
	}
}


void TimeStampTestImpl::cleanUp(void)
{
	ACS_TRACE("TimeStampTestImpl::cleanUp");
	if(getComponent() != 0)
	{
		#include <TimeStampTestImpl_cleanUp.body.cpp>

		getComponent()->stopAllThreads();
	}
}


void TimeStampTestImpl::aboutToAbort(void)
{
	ACS_TRACE("TimeStampTestImpl::aboutToAbort");
	if(getComponent() != 0)
	{
		#include <TimeStampTestImpl_aboutToAbort.body.cpp>

		getComponent()->stopAllThreads();
	}
}


/* ------------------ [ Functions ] ----------------- */

void TimeStampTestImpl::foo() throw(CORBA::SystemException)
{
	ACS_TRACE("TimeStampTestImpl::foo");
	if(component_running == true)
	{

		#include "TimeStampTestImpl_Function-foo.body.cpp"

	}
	else
	{
		ACS_SHORT_LOG((LM_ERROR, "TimeStampTestImpl::foo: Function is not executed. Check if component is running!"));
	}
}

/* --------------------- [ CORBA interface ] ----------------------*/
ACS::RWlong_ptr TimeStampTestImpl::bar() throw(CORBA::SystemException)
{
	ACS_TRACE("TimeStampTestImpl::bar");
	if(bar_m == 0)
	{
		return ACS::RWlong::_nil();
	}
	ACS::RWlong_var prop = ACS::RWlong::_narrow(bar_m->getCORBAReference());
	return prop._retn();
}


/* --------------- [ MACI DLL support functions ] -----------------*/

#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(TimeStampTestImpl)
