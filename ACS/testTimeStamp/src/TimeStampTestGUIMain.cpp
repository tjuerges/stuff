/*
 * "@(#) $Id$"
 */

#include <TimeStampTestGUI.h>
#include <TimeStampTestGUIEventLoop.h>
#include <qapplication.h>
#include <maciSimpleClient.h>

maci::SimpleClient* sc;
EventLoop* myEventLoop;
QApplication* qApp;

int main(int argc, char* argv[])
{
    qApp = new QApplication(argc, argv);

    sc = new maci::SimpleClient;
    myEventLoop = new EventLoop;
    myEventLoop->setSimpleClient(sc);

    try
    {
        /*
         * Create the instance of Client and Init() it.
         */
        if(sc->init(qApp->argc(), qApp->argv()) == 0)
        {
            ACS_SHORT_LOG((LM_ERROR, "TimeStampTestGUI::main: Cannot init client."));
            delete sc;
            return -1;
        }

        if(sc->login() == 0)
        {
            ACS_SHORT_LOG((LM_ERROR, "TimeStampTestGUI::main: Cannot login."));
            delete sc;
            return -1;
        }
    }
    catch(...)
    {
        ACS_SHORT_LOG((LM_ERROR, "TimeStampTestDGUI::main: Exception in init!"));
        delete sc;
        return -1;
    }

    TimeStampTestGUI* w(new TimeStampTestGUI);
    qApp->setMainWidget(w);

    myEventLoop->start();

    w->show();

    qApp->exec();

    ACS_SHORT_LOG((LM_INFO, "TimeStampTestGUI: Waiting 5s (max.) for the event loop to "
        "finish."));
    myEventLoop->stop();
    myEventLoop->wait();
    delete myEventLoop;

    sc->logout();
    delete w;
    delete qApp;
    return 0;
}
