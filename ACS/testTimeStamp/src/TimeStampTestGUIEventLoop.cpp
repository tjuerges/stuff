/*
 * "@(#) $Id$"
 */

#include <TimeStampTestGUIEventLoop.h>
#include <maciSimpleClient.h>
#include <ace/Time_Value.h>

void EventLoop::setSimpleClient(maci::SimpleClient* theClient)
{
    sc = theClient;
}

void EventLoop::run(void)
{
    eventEnd = false;
    ACE_Time_Value t;

    while(eventEnd == false)
    {
        t.set(5L, 0L);
        sc->run(t);
    }
}

void EventLoop::stop(void)
{
    eventEnd = true;
}
