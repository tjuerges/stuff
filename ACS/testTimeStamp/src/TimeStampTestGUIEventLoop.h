#ifndef TimeStampTestGUI_EventLoop_h
#define TimeStampTestGUI_EventLoop_h

/*
 * "@(#) $Id$"
*/

#include <maciSimpleClient.h>
#include <qthread.h>

class EventLoop: public QThread
{
    public:
    virtual void run(void);
    virtual void setSimpleClient(maci::SimpleClient*);
    virtual void stop(void);

    private:
    maci::SimpleClient* sc;
    bool eventEnd;
};

#endif
