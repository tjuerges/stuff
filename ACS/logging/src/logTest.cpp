//
// $Id$
//


#include <iostream>
#include <string>
#include <memory>
#include <ControlExceptions.h>


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    ControlExceptions::IllegalParameterErrorExImpl ex(__FILE__, __LINE__,
        __func__);
    std::string msg("My important detail");
    ex.addData("Detail", msg);

    std::cout << "\n\nLogging the exception without Logging system "
        "initialisation...\n";
    ex.log();


    std::auto_ptr< LoggingProxy >logger(new LoggingProxy(0, 0, 31));
    LoggingProxy::init(&*logger);

    std::cout << "\n\nLogging the exception with Logging system "
        "initialisation...\n";
    ex.log();


    std::cout << "\nLogging done.\n\n";

    return 0;
}
