//
// $Id$
//


#include <iostream>
#include <sstream>
#include <string>
#include <memory>
#include <ControlExceptions.h>


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    std::auto_ptr< LoggingProxy >logger(new LoggingProxy(0, 0, 31));
    LoggingProxy::init(&*logger);


    try
    {
        ControlExceptions::IllegalParameterErrorExImpl ex(__FILE__, __LINE__,
            __func__);
        const std::string msg("My important detail");
        ex.addData("Detail", msg);

        std::cout << "Initial exception:\n";
        ex.log();
        std::cout << "Initial exception.\n\n";
        throw ex;
    }
    catch(const ControlExceptions::IllegalParameterErrorExImpl& ex)
    {
        ControlExceptions::IllegalParameterErrorExImpl nex1(ex, __FILE__,
            __LINE__, __func__);
        ControlExceptions::IllegalParameterErrorExImpl nex2(ex, __FILE__,
            __LINE__, __func__);

        const std::string msg1(" Another piece of important data.");
        nex1.addData("Detail", msg1);

        std::string msg2(nex2.getData("Detail").c_str());
        msg2 += msg1;
        nex2.addData("Detail", msg2);

        std::cout << "Exception #1 where Detail is just added:\n";
        nex1.log();
        std::cout << "\n\nException #2 where Detail is concatenated:\n";
        nex2.log();
        std::cout << "\n\n";
    }

    try
    {
        ControlExceptions::IllegalParameterErrorExImpl ex(__FILE__, __LINE__,
            __func__);
        std::ostringstream msg;
        msg << "My important detail";
        ex.addData("Detail", msg.str());

        std::cout << "Initial exception:\n";
        ex.log();
        std::cout << "Initial exception.\n\n";
        throw ex;
    }
    catch(const ControlExceptions::IllegalParameterErrorExImpl& ex)
    {
        ControlExceptions::IllegalParameterErrorExImpl nex1(ex, __FILE__,
            __LINE__, __func__);
        ControlExceptions::IllegalParameterErrorExImpl nex2(ex, __FILE__,
            __LINE__, __func__);

        std::ostringstream msg1;
        msg1 << " Another piece of important data.";
        nex1.addData("Detail", msg1.str());

        std::ostringstream msg2;
        msg2 << nex2.getData("Detail").c_str();
        msg2 << msg1;
        nex2.addData("Detail", msg2.str());

        std::cout << "Exception #1 where Detail is just added:\n";
        nex1.log();
        std::cout << "\n\nException #2 where Detail is concatenated:\n";
        nex2.log();
        std::cout << "\n\n";
    }

    
    return 0;
}
