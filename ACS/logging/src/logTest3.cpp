//
// $Id$
//


#include <iostream>
#include <string>
#include <acsThread.h>
#include <acsThreadManager.h>
#include <OS_NS_sys_select.h>
#include <Time_Value.h>


class Thread1: public ACS::Thread
{
    public:
    Thread1(const ACE_CString& name):
        ACS::Thread(name)
    {
        const std::string fnName("Thread1::Thread1: ACS, then AUTO");
        ACS_TRACE(fnName);
        AUTO_TRACE(fnName);
    };

    virtual ~Thread1()
    {
        const std::string fnName("Thread1::~Thread1: ACS, then AUTO");
        ACS_TRACE(fnName);
        AUTO_TRACE(fnName);

        terminate();
    };

    virtual void runLoop()
    {
        const std::string fnName("Thread2::runLoop: ACS, then AUTO");
        std::cout << "Yo!  I am thread1...\n";
        ACS_TRACE(fnName);
        AUTO_TRACE(fnName);
        std::cout << "Yo!  I am thread1 and sleep now.\n";
    }
};



class Thread2: public ACS::Thread
{
    public:
    Thread2(const ACE_CString& name):
        ACS::Thread(name)
    {
        const std::string fnName("Thread2::Thread2: AUTO, then ACS");
        AUTO_TRACE(fnName);
        ACS_TRACE(fnName);
    };

    virtual ~Thread2()
    {
        const std::string fnName("Thread2::~Thread2: AUTO, then ACS");
        AUTO_TRACE(fnName);
        ACS_TRACE(fnName);

        terminate();
    };

    virtual void runLoop()
    {
        const std::string fnName("Thread2::runLoop: AUTO, then ACS");
        std::cout << "\tYo!  I am thread2...\n";
        AUTO_TRACE(fnName);
        ACS_TRACE(fnName);
        std::cout << "\tYo!  I am thread2 and sleep now.\n";
    }
};



int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    ACS::ThreadManager mgr;

    std::cout << "Creating Thread1...\n";
    const std::string thread1Name("thread1");
    Thread1* thread1(mgr.create< Thread1 >(thread1Name.c_str()));

    std::cout << "Creating Thread2...\n";
    const std::string thread2Name("thread2");
    Thread2* thread2(mgr.create< Thread2 >(thread2Name.c_str()));

    thread1->setSleepTime(ACE_ONE_SECOND_IN_NSECS / 100ULL);
    thread2->setSleepTime(ACE_ONE_SECOND_IN_NSECS / 100ULL);

    mgr.resume(thread1Name.c_str());
    mgr.resume(thread2Name.c_str());

    ACE_Time_Value sleepTime(10);
    ACE_OS::select(0, 0, 0, 0, &sleepTime);

    return 0;
}
