#include <iostream>
#include <cstdlib>
#include <rtLog.h>
#include <rtai_lxrt.h>

int main(int argc, char* argv[])
{
	if(argc != 2)
	{
		std::cout << "Pass the 0-based log level as parameter!";
		return -1;
	}

	RTLogLevelType rtlog_level(static_cast< RTLogLevelType >(strtol(argv[1], NULL, 10)));

   // this works only if IDL enum at first position is mapped to number 0
	rtlog_level += RTLOG_TRACE_LEVEL; // RTLOG_TRACE_LEVEL is offset of value 2

/* implementation using lxrt */
	RT_TASK* task;

	rt_allow_nonroot_hrt();
/*    if ( mlockall(MCL_CURRENT | MCL_FUTURE) != 0)
	{
	throw CanNotSetLevelExImpl(__FILE__, __LINE__, "rtlogImpl::setLevel").getCanNotSetLevelEx();
	}
*/
	if (!(task = rt_task_init_schmod(nam2num("setLevel"), 10, 0, 0, SCHED_FIFO, 0xFF)))
	{
		std::cout << "Could not set the new log level." << std::endl;
		return -1;
	};//if

	rtlogSetLevel(rtlog_level);

	rt_task_delete(task);

	std::cout << "rtlog_level has been set to " << rtlog_level << "." << std::endl;
	return 0;
}
