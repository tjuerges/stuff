/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2006
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Sep 18, 2006  created
*/

/************************************************************************
*   NAME
*
*   SYNOPSIS
*
*   DESCRIPTION
*
*   FILES
*
*   ENVIRONMENT
*
*   RETURN VALUES
*
*   CAUTIONS
*
*   EXAMPLES
*
*   SEE ALSO
*
*   BUGS
*
*------------------------------------------------------------------------
*/

static char *rcsId="@(#) $Id$";
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

/*
 * System stuff
 */
#include <linux/moduleparam.h>
#include <linux/delay.h>
#include <asm/atomic.h>
/* ... as you need them ... */

/*
 * ACS stuff
 */
#include <rtlog.h>
extern int rtlogSetLevel(RTLogLevelType level);
/*
 * Local stuff
 */

MODULE_AUTHOR("Thomas Juerges <tjuerges@nrao.edu>");
MODULE_DESCRIPTION("switchRTLogLevel");
MODULE_LICENSE("GPL");

/*
 * used for logging
 */
#define modName    "switchRTLogLevel"

/*
 * module parameters
 */
static unsigned int logLevel = 4;
module_param(logLevel, uint, S_IRUGO);

static int __init switchRTLogLevel_init(void)
{
	rtlogSetLevel((RTLogLevelType)(logLevel));
	return 0;
}

static void __exit switchRTLogLevel_exit(void)
{
}

module_init(switchRTLogLevel_init);
module_exit(switchRTLogLevel_exit);
