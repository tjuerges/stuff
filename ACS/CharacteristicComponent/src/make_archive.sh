#!/bin/bash
#
#  "@(#) $Id$"
#
#  $Log$
#

INTERFACE_NAME=Dummy

make clean

pushd ..

if [ -d ../ws ]; then
	pushd ..
	tar --exclude={.svn,CVS,*~,*.bak,*.o,lib*,*.a,#*} -cjf $INTERFACE_NAME.tar.bz2 ./{.project,.cdtproject} ws/{config,idl,include,rtai,src} lcu/{config,idl,include,rtai,src}
	popd
else
	tar --exclude={.svn,CVS,*~,*.bak,*.o,lib*,*.a,#*} -cjf $INTERFACE_NAME.tar.bz2 ./{.project,.cdtproject,config,idl,include,rtai,src}
fi

popd
exit 0
