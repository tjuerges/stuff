/*
 * "@(#) $Id$"
 *
 * $Log$
 *
 */

#include <DummyImpl.h>
#include <DummyImpl_IncludeFiles.h>
#include <DummyImpl_HelperFunctions.cpp>





DummyImpl::DummyImpl(const ACE_CString& name, 
		                             maci::ContainerServices* containerServices)
	: baci::CharacteristicComponentImpl(name, containerServices)

	,doubleProp_m(this)
	,longProp_m(this)
	,enumProp_m(this)

{
	component_name = name.c_str();

	#include <DummyImpl_Init.cpp>
	#include <DummyImpl_ThreadInit.cpp>
}


#include <DummyImpl_ThreadImpl.cpp>


DummyImpl::~DummyImpl()
{
}


void DummyImpl::initialize(void)
	throw (acsErrTypeLifeCycle::acsErrTypeLifeCycleExImpl)
{
	if (getComponent() != 0)
	{
		#include <DummyImpl_initialize.body.cpp>
	}

}


void DummyImpl::execute(void)
	throw (acsErrTypeLifeCycle::acsErrTypeLifeCycleExImpl)
{
	if (getComponent() != 0)
	{
		#include <DummyImpl_execute.body.cpp>
	}

}


void DummyImpl::cleanUp(void)
{
	if (getComponent() != 0)
	{
		#include <DummyImpl_cleanUp.body.cpp>

		getComponent()->stopAllThreads();
		ACE_OS::sleep(2);
	}

}


void DummyImpl::aboutToAbort(void)
{
	if (getComponent() != 0)
	{
		#include <DummyImpl_aboutToAbort.body.cpp>
	}

}


/* ------------------ [ Functions ] ----------------- */


/* --------------------- [ CORBA interface ] ----------------------*/



ACS::RWdouble_ptr DummyImpl::doubleProp() throw(CORBA::SystemException)
{
	if (doubleProp_m == 0)
	{
		return ACS::RWdouble::_nil();
	}
	ACS::RWdouble_var prop = ACS::RWdouble::_narrow(doubleProp_m->getCORBAReference());
	return prop._retn();
}


ACS::RWlong_ptr DummyImpl::longProp() throw(CORBA::SystemException)
{
	if (longProp_m == 0)
	{
		return ACS::RWlong::_nil();
	}
	ACS::RWlong_var prop = ACS::RWlong::_narrow(longProp_m->getCORBAReference());
	return prop._retn();
}


Test::RWDummyEnum_ptr DummyImpl::enumProp() throw(CORBA::SystemException)
{
	if (enumProp_m == 0)
	{
		return Test::RWDummyEnum::_nil();
	}
	Test::RWDummyEnum_var prop = Test::RWDummyEnum::_narrow(enumProp_m->getCORBAReference());
	return prop._retn();
}



/* --------------- [ MACI DLL support functions ] -----------------*/

#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(DummyImpl)
