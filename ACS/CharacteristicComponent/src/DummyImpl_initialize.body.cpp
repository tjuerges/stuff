/*
 * "@(#) $Id$"
 *
 * $Log$
 *
 */

	doubleProp_m = new baci::RWdouble((component_name + std::string(":doubleProp")).c_str(), 
			                  getComponent());
	longProp_m = new baci::RWlong((component_name + std::string(":longProp")).c_str(), 
			                  getComponent());
	enumProp_m = new ::RWEnumImpl< ACS_ENUM_T(Test::DummyEnum), POA_Test::RWDummyEnum >((component_name + std::string(":enumProp")).c_str(), getComponent());


