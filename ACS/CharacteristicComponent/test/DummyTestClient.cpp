/*
 * "@(#) $Id$"
 *
 * $Log$
 *
 */

#include <acsutil.h>
#include <maciSimpleClient.h>
#include <baciC.h>
#include <logging.h>

#include <dummyC.h>

using namespace maci;


char* cobname;

int main(int argc, char* argv[])
{
	// Check command line arguments
	if (argc < 2)
	{
		cobname = "Dummy";
	}
	else
	{
		cobname = argv[1];
	}

	// Creates and initializes the SimpleClient object
	SimpleClient client;

	if(client.init(argc,argv) == 0)
	{
		ACS_SHORT_LOG((LM_ERROR,"Cannot init client"));
		return -1;
	}

	ACS_SHORT_LOG((LM_INFO, "Welcome to TestTestClient!"));
	ACS_SHORT_LOG((LM_INFO, "Login in maciManager..."));
	client.login();

	try
	{
		// List all components the Manager knows of our type.
		ACS_SHORT_LOG((LM_INFO, "Listing all componentes of type *Dummy*"));
		maci::HandleSeq seq;
		maci::ComponentInfoSeq_var components = 
			       client.manager()->get_component_info(client.handle(), seq, "*", 
								                                  "*Dummy*", false);

		for (CORBA::ULong i = 0; i < components->length(); i++)
		{
			ACS_SHORT_LOG((LM_INFO,"%s (%s)", components[i].name.in(), 
						                            components[i].type.in()));
		}

		// Now get the specific component we have requested on the command line.
		ACS_SHORT_LOG((LM_INFO, "Getting component Dummy..."));
		Test::Dummy_var component = 
			       client.get_object<Test::Dummy>("Dummy", 0, true);

		if (!CORBA::is_nil(component.in()))
		{
			ACS_SHORT_LOG((LM_INFO, "... got component Dummy"));
			// Prints the descriptor of the requested component.
			ACS_SHORT_LOG((LM_INFO, "Requesting descriptor()... "));
			ACS::CharacteristicComponentDesc_var descriptor = 
				                                       component->descriptor();

			ACS_SHORT_LOG((LM_INFO, "Descriptor:"));
			ACS_SHORT_LOG((LM_INFO, "\tname: %s", descriptor->name.in()));

			ACSErr::Completion_var completion;


			ACS_SHORT_LOG((LM_INFO, "Getting component property: %s::doubleProp", 
						                                                       cobname));
			if (CORBA::is_nil(component->doubleProp()) == false)
			{ 
				CORBA::Double value = component->doubleProp()->get_sync(completion.out());
				ACS_SHORT_LOG((LM_INFO, "Value: %f", value));
			}


			ACS_SHORT_LOG((LM_INFO, "Getting component property: %s::longProp", 
						                                                       cobname));
			if (CORBA::is_nil(component->longProp()) == false)
			{ 
				CORBA::Long value = component->longProp()->get_sync(completion.out());
				ACS_SHORT_LOG((LM_INFO, "Value: %d", value));
			}


			ACS_SHORT_LOG((LM_INFO, "Getting component property: %s::enumProp", 
						                                                       cobname));
			if (CORBA::is_nil(component->enumProp()) == false)
			{ 
				ACS_SHORT_LOG((LM_INFO, "Trying to map enum on long."));
				CORBA::Long value = component->enumProp()->get_sync(completion.out());
				ACS_SHORT_LOG((LM_INFO, "Value: %d", value));
			}



		} /* end if component reference OK */
		else
		{
			ACS_SHORT_LOG((LM_INFO, "Component Dummy is nil !!!"));
		}
	} /* end main try */
	catch (...)
	{
		ACS_SHORT_LOG((LM_ERROR, "Error in TestClient::main!"));
	}

	/* Another try section where we release our component
	 * and logout from the Manager.
	 */
	try
	{
		ACS_SHORT_LOG((LM_INFO, "Releasing..."));
		client.manager()->release_component(client.handle(), cobname);
		client.logout();
	}
	catch (...)
	{
		ACS_SHORT_LOG((LM_ERROR, "Error in TestClient::main!"));
	}

	/*
	 * sleep for 3 sec to allow everytihng to cleanup and stabilize
	 * so that the tests can be determinitstic.
	 */
	ACE_OS::sleep(3);
	return 0;
}
