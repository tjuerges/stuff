#ifndef DummyImpl_h
#define DummyImpl_h

/*
 * "@(#) $Id$"
 *
 * $Log$
 *
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <baciCharacteristicComponentImpl.h>
#include <baciSmartPropertyPointer.h>


#include <enumpropRWImpl.h>
#include <baciRWdouble.h>
#include <baciRWlong.h>

#include <dummyS.h>
#include <Dummy_IncludeFiles.h>


class DummyImpl : public virtual baci::CharacteristicComponentImpl,
                        public virtual POA_Test::Dummy

{
	public:
		/**
	 	 * Constructor
	   * @param containerServices ContainerServices which are needed for 
		 * various component related methods.
	   * @param name component name
	   */
		DummyImpl(const ACE_CString& name, 
				            maci::ContainerServices* containerServices);

		/**
	 	 * Destructor
	 	 */
		virtual ~DummyImpl();

		
/* ------------------- [ Lifecycle START interface ] --------------------- */

		/**
	 	 * Lifecycle method called at component initialization state.
	 	 * Overwrite it in case you need it.
	 	 */
		virtual void initialize(void) throw (acsErrTypeLifeCycle::acsErrTypeLifeCycleExImpl);

		/**
	 	 * Lifecycle method called at component execute state.
	 	 * Overwrite it in case you need it.
	 	 */
		virtual void execute(void) throw (acsErrTypeLifeCycle::acsErrTypeLifeCycleExImpl);

		/**
	 	 * Lifecycle method called at component clean up state.
	 	 * Overwrite it in case you need it.
	 	 */
		virtual void cleanUp(void);

		/**
	   * Lifecycle method called at component abort state.
	   * Overwrite it in case you need it.
	   */
		virtual void aboutToAbort(void);

/* ------------------- [ Lifecycle END interface ] --------------- */

/* --------------------- [ CORBA START interface ] ----------------*/

	virtual ACS::RWdouble_ptr doubleProp() throw(CORBA::SystemException);

	virtual ACS::RWlong_ptr longProp() throw(CORBA::SystemException);

	virtual Test::RWDummyEnum_ptr enumProp() throw(CORBA::SystemException);


/* --------------------- [ CORBA END interface ] ----------------*/

/* ----------------------------------------------------------------*/

	private:
		/**
		 * Copy constructor is not allowed following the ACS desgin rules.
		 */
		DummyImpl(const DummyImpl&);

		/**
	 	 * Assignment operator is not allowed due to ACS design rules.
	 	 */
		void operator=(const DummyImpl&);


/* --------------------- [ Constants START ] ----------------------*/

/* --------------------- [ Constants END ] ------------------------*/

/* --------------------- [ Properties START ] ----------------------*/

	SmartPropertyPointer< baci::RWdouble > doubleProp_m;
	SmartPropertyPointer< baci::RWlong > longProp_m;
	SmartPropertyPointer< ::RWEnumImpl< ACS_ENUM_T(Test::DummyEnum), POA_Test::RWDummyEnum > > enumProp_m;

/* --------------------- [ Properties END ] ------------------------*/

/* --------------------- [ Local properties START ] ----------------*/
#include <DummyImpl_LocalProperties.h>
/* --------------------- [ Local properties END ] ------------------*/
};


#endif /* DummyImpl_h */

