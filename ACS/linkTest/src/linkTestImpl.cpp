/**
 * $Id$
 */


#include <linkTestImpl.h>


linkTestImpl::linkTestImpl(const ACE_CString& name,
    maci::ContainerServices* containerServices):
    ACSComponentImpl(name, containerServices),
    container(containerServices)
{
	component_name_m = name.c_str();

	#include <linkTestImpl_Init.cpp>
}


linkTestImpl::~linkTestImpl()
{
}


void linkTestImpl::initialize(void)
	throw (acsErrTypeLifeCycle::acsErrTypeLifeCycleExImpl)
{
	#include <linkTestImpl_initialize.body.cpp>
}


void linkTestImpl::execute(void)
	throw (acsErrTypeLifeCycle::acsErrTypeLifeCycleExImpl)
{
	#include <linkTestImpl_execute.body.cpp>
}


void linkTestImpl::cleanUp(void)
{
	#include <linkTestImpl_cleanUp.body.cpp>
}


void linkTestImpl::aboutToAbort(void)
{
	#include <linkTestImpl_aboutToAbort.body.cpp>
}


/* ------------------ [ Functions ] ----------------- */

void linkTestImpl::start() throw(CORBA::SystemException)
{
	if(component_running == true)
	{

		#include "linkTestImpl_Function-start.body.cpp"

	}
	else
	{
		ACS_SHORT_LOG((LM_ERROR,"linkTestImpl::start: Function is not executed. Check if component is running!"));
	}
}

void linkTestImpl::stop() throw(CORBA::SystemException)
{
	if(component_running == true)
	{

		#include "linkTestImpl_Function-stop.body.cpp"

	}
	else
	{
		ACS_SHORT_LOG((LM_ERROR,"linkTestImpl::stop: Function is not executed. Check if component is running!"));
	}
}

/* --------------------- [ CORBA interface ] ----------------------*/
/* --------------- [ MACI DLL support functions ] -----------------*/

#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(linkTestImpl)
