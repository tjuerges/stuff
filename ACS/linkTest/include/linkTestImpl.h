#ifndef linkTestImpl_h
#define linkTestImpl_h

/*
 * "@(#) $Id$"
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <baciCharacteristicComponentImpl.h>

#include <linkTestS.h>



#include <baciSmartPropertyPointer.h>

#include <linkTest_IncludeFiles.h>


/*using namespace baci;*/

class linkTestImpl: public virtual acscomponent::ACSComponentImpl,
	public virtual POA_Test::linkTest
{
	public:
	/**
	 * Constructor
	 * @param containerServices ContainerServices which are needed for various component
	 * related methods.
	 * @param name component name
	 */
	linkTestImpl(const ACE_CString& name, maci::ContainerServices* containerServices);

	/**
	 * Destructor
	 */
	virtual ~linkTestImpl();

	/**
	 * Lifecycle method called at component initialization state.
	 * Overwrite it in case you need it.
	 */
	virtual void initialize(void) throw (acsErrTypeLifeCycle::acsErrTypeLifeCycleExImpl);

	/**
	 * Lifecycle method called at component execute state.
	 * Overwrite it in case you need it.
	 */
	virtual void execute(void) throw (acsErrTypeLifeCycle::acsErrTypeLifeCycleExImpl);

	/**
	 * Lifecycle method called at component clean up state.
	 * Overwrite it in case you need it.
	 */
	virtual void cleanUp(void);

	/**
	 * Lifecycle method called at component abort state.
	 * Overwrite it in case you need it.
	 */
	virtual void aboutToAbort(void);




/* --------------------- [ CORBA START interface ] ----------------*/
	virtual void start() throw(CORBA::SystemException);

	virtual void stop() throw(CORBA::SystemException);


/* --------------------- [ CORBA END interface ] ----------------*/

/* ----------------------------------------------------------------*/

	private:
	/**
	 * Copy constructor is not allowed following the ACS desgin rules.
	 */
	linkTestImpl(const linkTestImpl&);

	/**
	 * Assignment operator is not allowed due to ACS design rules.
	 */
	void operator=(const linkTestImpl&);

/* --------------------- [ Properties START ] ----------------------*/

	maci::ContainerServices* container;
	std::string component_name_m;

/* --------------------- [ Properties END ] ------------------------*/

/* --------------------- [ Local properties START ] ----------------*/
#include <linkTestImpl_LocalProperties.h>
/* --------------------- [ Local properties END ] ------------------*/
};


#endif /* linkTestImpl_h */
