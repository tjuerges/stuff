/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2008
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id$"
 *
 * ExceptionTestClient.cpp
 *
 *  Created on: Sep 3, 2008
 *      Author: tjuerges
 */


#include <iostream>
#include <maciSimpleClient.h>
#include <ExceptionTestC.h>


// If THROW_STL_EXCEPTION == 1, an STL exception will be thrown from within
// the component.
#define THROW_STL_EXCEPTION 0


int main(int argc, char* argv[])
{
    // Instanciate a maci::SimpleClient.
    maci::SimpleClient sc;
    try
    {
        if(sc.init(argc, argv) == 0)
        {
            std::cout << "SimpleClient init failed.\n";
            return -1;
        }
        else
        {
            sc.login();
        }
    }
    catch(...)
    {
        std::cout << "Caught an exception during SimpleClient instanciation.\n";
        return -1;
    };

    // Instanciate both test components.
    Test::ExceptionTest_var componentWith(
        sc.getComponent<  Test::ExceptionTest >(
            "ExceptionTestWith", 0, true));

    Test::ExceptionTest_var componentWithout(
        sc.getComponent<  Test::ExceptionTest >(
            "ExceptionTestWithout", 0, true));

    std::cout << "Testing the component without exception specifications...\n";

    try
    {
        std::cout << "Calling Test::ExceptionTest::"
            "doDeclaredCorbaException...\n";
        componentWithout->doDeclaredCorbaException();
    }
    catch(const Test::Bang& ex)
    {
        std::cout << "Caught a Test::Bang exception.\n";
    }
    catch(const CORBA::UserException& ex)
    {
        std::cout << "Caught a CORBA::UserException.\n";
    }
    catch(const CORBA::SystemException& ex)
    {
        std::cout << "Caught a CORBA::SystemException.\n";
    }
    catch(...)
    {
        std::cout << "Caught a ... exception.\n";
    }

    try
    {
        std::cout << "Calling Test::ExceptionTest::doCorbaException...\n";
        componentWithout->doCorbaException();
    }
    catch(const Test::Bang& ex)
    {
        std::cout << "Caught a Test::Bang exception.\n";
    }
    catch(const CORBA::UserException& ex)
    {
        std::cout << "Caught a CORBA::UserException.\n";
    }
    catch(const CORBA::SystemException& ex)
    {
        std::cout << "Caught a CORBA::SystemException.\n";
    }
    catch(...)
    {
        std::cout << "Caught a ... exception.\n";
    }

#if THROW_STL_EXCEPTION == 1
    try
    {
        std::cout << "Calling Test::ExceptionTest::doSTLException...\n";
        componentWithout->doSTLException();
    }
    catch(const std::runtime_error& ex)
    {
        std::cout << "Caught a std::runtime_error exception.\n";
    }
    catch(...)
    {
        std::cout << "Caught a ... exception.\n";
    }
#endif

    std::cout << "Testing the component with exception specifications...\n";

    try
    {
        std::cout << "Calling Test::ExceptionTest::"
            "doDeclaredCorbaException...\n";
        componentWith->doDeclaredCorbaException();
    }
    catch(const Test::Bang& ex)
    {
        std::cout << "Caught a Test::Bang exception.\n";
    }
    catch(const CORBA::UserException& ex)
    {
        std::cout << "Caught a CORBA::UserException.\n";
    }
    catch(const CORBA::SystemException& ex)
    {
        std::cout << "Caught a CORBA::SystemException.\n";
    }
    catch(...)
    {
        std::cout << "Caught a ... exception.\n";
    }

    try
    {
        std::cout << "Calling Test::ExceptionTest::doCorbaException...\n";
        componentWith->doCorbaException();
    }
    catch(const Test::Bang& ex)
    {
        std::cout << "Caught a Test::Bang exception.\n";
    }
    catch(const CORBA::UserException& ex)
    {
        std::cout << "Caught a CORBA::UserException.\n";
    }
    catch(const CORBA::SystemException& ex)
    {
        std::cout << "Caught a CORBA::SystemException.\n";
    }
    catch(...)
    {
        std::cout << "Caught a ... exception.\n";
    }

#if THROW_STL_EXCEPTION == 1
    try
    {
        std::cout << "Calling Test::ExceptionTest::doSTLException...\n";
        componentWith->doSTLException();
    }
    catch(const std::runtime_error& ex)
    {
        std::cout << "Caught a std::runtime_error exception.\n";
    }
    catch(...)
    {
        std::cout << "Caught a ... exception.\n";
    }
#endif

    try
    {
        sc.releaseComponent("ExceptionTestWithout");
        sc.releaseComponent("ExceptionTestWith");

        sc.logout();
    }
    catch(...)
    {
        // Nothing we can do here.  Just make the compiler happy.
        #if 0
        #endif
    };

    return 0;
}
