#ifndef ExceptionTestWith_h
#define ExceptionTestWith_h
/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2008
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id$"
 *
 * who       when      what
 * --------  --------  ----------------------------------------------
 * tjuerges  Aug 27, 2008  created
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif


#include <string>
#include <acscomponentImpl.h>
#include <ExceptionTestS.h>


namespace maci
{
    class ContainerServices;
};


class ExceptionTestWith:
    public acscomponent::ACSComponentImpl,
    public virtual POA_Test::ExceptionTest
{
    public:
    /// Constructor
    /// @param containerServices ContainerServices which are needed for
    /// various component related methods.
    /// @param name component name
    ExceptionTestWith(const ACE_CString& name,
        maci::ContainerServices* containerServices);

    /// Destructor
    virtual ~ExceptionTestWith();

    /// Throws an STL exception. Cannot be declared.
    virtual void doSTLException() throw(CORBA::SystemException);

    /// Throw an undeclared CORBA exception.
    virtual void doCorbaException() throw(CORBA::SystemException);

    /// Throw a declared CORBA exception.
    virtual void doDeclaredCorbaException()
        throw(CORBA::SystemException, Test::Bang);


    private:
    /// Default constructor does not make sense.
    ExceptionTestWith();

    /// Copy constructor is not allowed under ALMA coding standards.
    ExceptionTestWith(const ExceptionTestWith&);

    /// Assignment operator is not allowed under ALMA coding standards.
    void operator=(const ExceptionTestWith&);


    maci::ContainerServices* cs;
    std::string name;
};
#endif
