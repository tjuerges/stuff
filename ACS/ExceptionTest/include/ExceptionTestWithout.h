#ifndef ExceptionTestWithout_h
#define ExceptionTestWithout_h
/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2008
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id$"
 *
 * who       when      what
 * --------  --------  ----------------------------------------------
 * tjuerges  Aug 27, 2008  created
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif


#include <string>
#include <acscomponentImpl.h>
#include <ExceptionTestS.h>


namespace maci
{
    class ContainerServices;
};


class ExceptionTestWithout:
    public acscomponent::ACSComponentImpl,
    public virtual POA_Test::ExceptionTest
{
    public:
    /// Constructor
    /// @param containerServices ContainerServices which are needed for
    /// various component related methods.
    /// @param name component name
    ExceptionTestWithout(const ACE_CString& name,
        maci::ContainerServices* containerServices);

    /// Destructor
    virtual ~ExceptionTestWithout();

    /// Throws an STL exception. Cannot be declared.
    virtual void doSTLException();

    /// Throw an undeclared CORBA exception.
    virtual void doCorbaException();

    /// Throw a declared CORBA exception.
    virtual void doDeclaredCorbaException();


    private:
    /// Default constructor does not make sense.
    ExceptionTestWithout();

    /// Copy constructor is not allowed under ALMA coding standards.
    ExceptionTestWithout(const ExceptionTestWithout&);

    /// Assignment operator is not allowed under ALMA coding standards.
    void operator=(const ExceptionTestWithout&);


    maci::ContainerServices* cs;
    std::string name;
};
#endif
