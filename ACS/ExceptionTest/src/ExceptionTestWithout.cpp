/*
 * "@(#) $Id$"
 *
 * $Log$
 *
 */


#include <ExceptionTestWithout.h>
#include <exception>
#include <maciContainerServices.h>


ExceptionTestWithout::ExceptionTestWithout(const ACE_CString& _name,
    maci::ContainerServices* containerServices):
    acscomponent::ACSComponentImpl(_name, containerServices),
    cs(containerServices),
    name(_name.c_str())
{
    const std::string fnName("ExceptionTestWithout::ExceptionTestWithout");
    AUTO_TRACE(fnName.c_str());
}

ExceptionTestWithout::~ExceptionTestWithout()
{
    const std::string fnName("ExceptionTestWithout::~ExceptionTestWithout");
    AUTO_TRACE(fnName.c_str());
}

void ExceptionTestWithout::doSTLException()
{
    const std::string fnName("ExceptionTestWithout::doSTLException");
    AUTO_TRACE(fnName.c_str());
    throw std::runtime_error("Outch!");
}

void ExceptionTestWithout::doCorbaException()
{
    const std::string fnName("ExceptionTestWithout::doCorbaException");
    AUTO_TRACE(fnName.c_str());

    throw Test::Bang();
}

void ExceptionTestWithout::doDeclaredCorbaException()
{
    const std::string fnName("ExceptionTestWithout::doDeclaredCorbaException");
    AUTO_TRACE(fnName.c_str());

    throw Test::Bang();
}


#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(ExceptionTestWithout)
