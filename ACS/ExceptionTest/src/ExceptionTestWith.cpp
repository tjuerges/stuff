/*
 * "@(#) $Id$"
 *
 * $Log$
 *
 */


#include <ExceptionTestWith.h>
#include <exception>
#include <iostream>
#include <maciContainerServices.h>


ExceptionTestWith::ExceptionTestWith(const ACE_CString& _name,
    maci::ContainerServices* containerServices):
    acscomponent::ACSComponentImpl(_name, containerServices),
    cs(containerServices),
    name(_name.c_str())
{
    const std::string fnName("ExceptionTestWith::ExceptionTestWith");
    AUTO_TRACE(fnName.c_str());
}

ExceptionTestWith::~ExceptionTestWith()
{
    const std::string fnName("ExceptionTestWith::~ExceptionTestWith");
    AUTO_TRACE(fnName.c_str());
}

void ExceptionTestWith::doSTLException() throw(CORBA::SystemException)
{
    const std::string fnName("ExceptionTestWith::doSTLException");
    AUTO_TRACE(fnName.c_str());

    throw std::runtime_error("Outch!");
}

void ExceptionTestWith::doCorbaException() throw(CORBA::SystemException)
{
    const std::string fnName("ExceptionTestWith::doCorbaException");
    AUTO_TRACE(fnName.c_str());

    throw Test::Bang();
}

void ExceptionTestWith::doDeclaredCorbaException()
    throw(CORBA::SystemException, Test::Bang)
{
    const std::string fnName("ExceptionTestWith::doDeclaredCorbaException");
    AUTO_TRACE(fnName.c_str());

    throw Test::Bang();
}


#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(ExceptionTestWith)
