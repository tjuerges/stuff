/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2006
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Oct 11, 2006  created
*/

#include <vector>
#include <iostream>
#include <acstime.h>
#include <acsThread.h>
#include <acsThreadManager.h>


class demoThread: public ACS::Thread
{
	public:
		demoThread(
			const ACE_CString& _name = "demoThread",
			char* foo = 0,
			const ACS::TimeInterval& _responseTime = ACS::ThreadBase::defaultResponseTime,
			const ACS::TimeInterval& _sleepTime = ACS::TimeInterval(100*1000*1000), // 10s
			const bool _delete = true);

		virtual ~demoThread();

		virtual void run();

	private:
		demoThread(const demoThread&);
		demoThread& operator=(const demoThread&);

		const std::string my_name;
		char* foo;
};

demoThread::demoThread(
			const ACE_CString& _name,
			char* _foo,
			const ACS::TimeInterval& _responseTime,
			const ACS::TimeInterval& _sleepTime,
			const bool _delete):
	ACS::Thread(_name, _responseTime, _sleepTime, _delete),
	my_name(_name.c_str()),
	foo(_foo)
{
	std::cout << "demoThread::demoThread: Thread created." << std::endl;
}

demoThread::~demoThread()
{
	std::cout << "demoThread::demoThread: Thread destroyed." << std::endl;
}

void demoThread::run()
{
	if((check() == true) && (isSuspended() == false))
	{
		std::cout << "demoThread::run: I'm alive..." << std::endl;
	}

	std::cout << "demoThread::run: Leaving run." << std::endl;
}


int main(int c, char* a[])
{
	std::cout << "Create thread..." << std::endl;

	char* foo = "Bar!";
	const ACE_CString name("DemoThread");

	ACS::ThreadManager manager;

	manager.create< demoThread, char* >(name, foo);

	std::cout << "Sleep for ten seconds..." << std::endl;
	sleep(10);
	std::cout << "" << std::endl;

	manager.suspend(name.c_str());
	manager.stop(name.c_str());

	std::cout << "Thread is stopped. Exit." << std::endl;

	return 0;
}
