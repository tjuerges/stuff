/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Feb 27, 2007  created
*/

#include <acsClientEventLoop.h>
#include <Time_Value.h>
#include <acsClient.h>
#include <acsThread.h>
#include <loggingMACROS.h>

EventLoop::EventLoop(
    const ACE_CString& _name,
    maci::AcsClient* _client,
    const ACS::TimeInterval& _responseTime,
    const ACS::TimeInterval& _sleepTime,
    const bool _delete):
    ACS::Thread(_name, _responseTime, _sleepTime, _delete),
    myName(_name.c_str()),
    client(_client),
    eventLoopTime(1, 0) // Let event thread run one second.
{
}

EventLoop::~EventLoop()
{
    try
    {
    	terminate();
    }
    catch(...)
    {
        std::cout << "Exception in destructor!" << std::endl;
    }
}

void EventLoop::runLoop()
{
    static ACE_Time_Value time;

    time = eventLoopTime;
    try
    {
    	client->run(time);
    }
    catch(...)
    {
        std::cout << "Exception in loop!" << std::endl;
    }
}
