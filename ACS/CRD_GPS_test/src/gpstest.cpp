/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Feb 27, 2007  created
*/

#include <iostream>
#include <string>

#include <acsClient.h>
#include <loggingMACROS.h>

#include <GPSC.h>
#include <acsComponentSmartPtr.h>
#include <acsutilTimeStamp.h>


int main(int argc, char* argv[])
{
    maci::AcsClient client(argc, argv);

    const std::string lruName("CONTROL/AOSTiming/GPS");
    maci::ComponentSmartPtr< Control::GPS > lru;
    try
    {
        lru = client.getComponentSmartPtr< Control::GPS >(
            lruName.c_str(), 0, true);
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        std::cout << "Cannot get the "
            << lruName
            << " component."
            << std::endl;
        return -1;
    }

    if(CORBA::is_nil(&*lru) == true)
    {
        std::cout << "The "
            << lruName
            << " component reference is NIL."
            << std::endl;
        return -2;
    }

    ACSErr::Completion_var completion;
    const ACS::Time t(lru->GPS_Time()->get_sync(completion.out()));
    ACSErr::CompletionImpl c(completion.in());
    if(c.isErrorFree() == false)
    {
        c.log();
        return -3;
    }

    std::cout << "Time = "
        << t
        << "."
        << std::endl;

    return 0;
}
