#ifndef ACSCLIENTEVENTLOOP_H_
#define ACSCLIENTEVENTLOOP_H_
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Feb 27, 2007  created
*/

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

#include <acsThread.h>
#include <Time_Value.h>
#include <acsClient.h>


class EventLoop: public ACS::Thread
{
    public:
    EventLoop(
        const ACE_CString& _name = "AcsClientEventLoop",
        maci::AcsClient* _client = 0,
        const ACS::TimeInterval& _responseTime =
            ACS::TimeInterval(2 * 10 * 1000 * 1000), // 2s
        const ACS::TimeInterval& _sleepTime =
            ACS::TimeInterval(0.1 * 10 * 1000 * 1000), // 0.1s
        const bool _delete = true);

    virtual ~EventLoop();

    virtual void runLoop();


    private:
    EventLoop(const EventLoop&);
    EventLoop& operator=(const EventLoop&);

    const std::string myName;
    maci::AcsClient* client;
    const ACE_Time_Value eventLoopTime;
};

#endif /*ACSCLIENTEVENTLOOP_H_*/
