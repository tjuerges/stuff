/*
 * "@(#) $Id$"
 *
 * $Log$
 *
 */

#include <threadManagerTestImpl.h>
#include "threadManagerTestImpl_IncludeFiles.h"
#include "threadManagerTestImpl_HelperFunctions.cpp"

using namespace baci;


threadManagerTestImpl::threadManagerTestImpl(const ACE_CString& name, maci::ContainerServices* containerServices)
	:CharacteristicComponentImpl(name, containerServices)
{
	component_name_m = name.c_str();

	#include <threadManagerTestImpl_Init.cpp>

	#include <threadManagerTestImpl_ThreadInit.cpp>
}


#include <threadManagerTestImpl_ThreadImpl.cpp>


threadManagerTestImpl::~threadManagerTestImpl()
{
}


void threadManagerTestImpl::initialize(void)
	throw (acsErrTypeLifeCycle::acsErrTypeLifeCycleExImpl)
{
	if(getComponent() != 0)
	{
		#include <threadManagerTestImpl_initialize.body.cpp>

	}
}


void threadManagerTestImpl::execute(void)
	throw (acsErrTypeLifeCycle::acsErrTypeLifeCycleExImpl)
{
	if(getComponent() != 0)
	{
		#include <threadManagerTestImpl_execute.body.cpp>
	}
}


void threadManagerTestImpl::cleanUp(void)
{
	if(getComponent() != 0)
	{
		#include <threadManagerTestImpl_cleanUp.body.cpp>

		getComponent()->stopAllThreads();
	}
}


void threadManagerTestImpl::aboutToAbort(void)
{
	if(getComponent() != 0)
	{
		#include <threadManagerTestImpl_aboutToAbort.body.cpp>
	}
}


/* --------------- [ Action implementator interface ] -------------- */

ActionRequest threadManagerTestImpl::invokeAction(int function,
	BACIComponent* cob,
	const int& callbackID,
	const CBDescIn& descIn,
	BACIValue* value,
	Completion& completion,
	CBDescOut& descOut)
{
	switch(function)
	{
		default:
		{
			return reqDestroy;
		}
	}
}


/* ------------------ [ Action implementations ] ----------------- */

/* ------------------ [ Functions ] ----------------- */

void threadManagerTestImpl::start() throw(CORBA::SystemException)
{
	if(component_running == true)
	{

		#include "threadManagerTestImpl_Function-start.body.cpp"

	}
	else
	{
		ACS_SHORT_LOG((LM_ERROR,"threadManagerTestImpl::start: Function is not executed. Check if component is running!"));
	}
}

/* --------------------- [ CORBA interface ] ----------------------*/
/* --------------- [ MACI DLL support functions ] -----------------*/

#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(threadManagerTestImpl)
