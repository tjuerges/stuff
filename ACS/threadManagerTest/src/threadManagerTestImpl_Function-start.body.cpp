/*
 * "@(#) $Id$"
 *
 * $Log$
 *
 */


/*
 * This method is void and returns nothing.
 *
 */

#ifdef USE_PTHREADS
#elif USE_ACE_THREADS
testThread t;
#else
ACS::ThreadManager* mgr(getContainerServices()->getThreadManager());
#endif

const bool print(false);
const unsigned long max(50000UL);
const unsigned long print_every(1000UL);

for(int i(0); i < 20; ++i)
{
	finishCounter = 0UL;


	#if (defined USE_PTHREADS) || (defined USE_ACE_THREADS)
		testThread::structForThread foo;
		foo.tm = this;
	#else
		testThread::structForThread* foo(new testThread::structForThread);
		foo->tm = this;
	#endif

	do
	{
		std::ostringstream c;
		c << "Thread" << thread_counter++ << std::ends;

		try
		{
	#ifdef USE_PTHREADS
			// The thread must be created on the heap.
			SafePt::Thread<testThread>* t(new SafePt::Thread<testThread>(foo));
	#elif defined USE_ACE_THREADS
			t.setUp(foo);
			t.activate(THR_NEW_LWP | THR_DETACHED, 1, 1);
	#else
			mgr->create<testThread, testThread::structForThread*>(c.str().c_str(), foo);
	#endif

			if((print == true) || (thread_counter % print_every == 0))
			{
				std::cout << "Created thread " << c.str() << std::endl;
			}
			// Thread creation is successful (no exception), so leave while loop.
			continue;
		}
		catch(acsthreadErrType::ThreadAlreadyExistExImpl)
		{
	#ifdef NO_SHORT_LOG
			std::cout << "testThreadManagerImpl::start: A thread with the name \"" << c.str() << "\" already exists. Trying next one." << std::endl;
	#else
			ACS_SHORT_LOG((LM_ERROR, "testThreadManagerImpl::start: A thread with the name \"%s\" already exists. Trying next one.", c.str().c_str()));
	#endif
		}
		catch(acsthreadErrType::CanNotCreateThreadExImpl)
		{
	#ifdef NO_SHORT_LOG
			std::cout << "testThreadManagerImpl::start: Cannot create a new thread! (" << c.str() << ")" << std::endl;
	#else
			ACS_SHORT_LOG((LM_ERROR, "testThreadManagerImpl::start: Cannot create a new thread! (%s)", c.str().c_str()));
	#endif
		}
	}
	while(thread_counter < max);

	#if (!defined USE_PTHREADS) && (!defined USE_ACE_THREADS)
	delete foo;
	#endif


	#ifdef NO_SHORT_LOG
			std::cout << "testThreadManagerImpl::start: Counter of finished threads = " << finishCounter << ". Sleeping 5s." << std::endl;
	#else
			ACS_SHORT_LOG((LM_INFO,"testThreadManagerImpl::start: Counter of finished threads = %d. Sleeping 5s.", finishCounter));
	#endif

	ACE_OS::sleep(5);

	#ifdef NO_SHORT_LOG
			std::cout << "testThreadManagerImpl::start: Counter of finished threads = " << finishCounter << ". Sleeping 5s." << std::endl;
	#else
			ACS_SHORT_LOG((LM_INFO,"testThreadManagerImpl::start: Counter of finished threads = %d. Sleeping 5s.", finishCounter));
	#endif

	ACE_OS::sleep(5);

	#ifdef NO_SHORT_LOG
			std::cout << "testThreadManagerImpl::start: Counter of finished threads = " << finishCounter << "." << std::endl;
	#else
			ACS_SHORT_LOG((LM_INFO,"testThreadManagerImpl::start: Counter of finished threads = %d.", finishCounter));
	#endif


	#ifdef NO_SHORT_LOG
			std::cout << "testThreadManagerImpl::start: Ready with run " << i << "." << std::endl;
	#else
			ACS_SHORT_LOG((LM_INFO,"testThreadManagerImpl::start: Ready with run %d.", i));
	#endif

	thread_counter = 0UL;
}
