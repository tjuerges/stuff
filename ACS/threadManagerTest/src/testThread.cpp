#include <testThread.h>
#include <logging.h>
#include <cmath>

#ifdef USE_ACE_THREADS
void testThread::setUp(const structForThread& data)
{
	tm = data.tm;
}
#elif defined USE_PTHREADS
testThread::testThread(structForThread& data):
	tm(data.tm)
{
}
#else
testThread::testThread(const ACE_CString &name,
						 const structForThread* data,
						 const bool suspended,
						 const TimeInterval &responseTime,
						 const TimeInterval &sleepTime,
						 const bool del):
	ACS::Thread(name, suspended, responseTime, sleepTime, del),
	my_name(name.c_str()),
	tm(data->tm)
{
}
#endif

#ifndef USE_ACE_THREADS
testThread::~testThread()
{
#ifdef NO_SHORT_LOG
//	std::cout << "testThread::~testThread: " << my_name << " destroyed." << std::endl;
#else
	ACS_SHORT_LOG((LM_INFO, "testThread::~testThread: %s destroyed.", my_name.c_str()));
#endif
}
#endif

#ifdef USE_PTHREADS
void testThread::operator()()
#elif defined(USE_ACE_THREADS)
int testThread::svc()
#else
void testThread::run()
#endif
{
	double j(0);

	for(int i(0); i < 3; ++i)
	{
		j += std::pow(j, i);
		j /= std::sin(j / 2.0 * M_PI);
#ifdef NO_SHORT_LOG
//		std::cout << "testThread::run: (" << my_name << ") Running... Step " << i << std::endl;
#else
		ACS_SHORT_LOG((LM_INFO,"testThread::run: (%s) Running... Step ", my_name.c_str(), i));
#endif
	}

	tm->increaseFinishCounter();

#ifdef USE_PTHREADS
	delete this;
#elif defined(USE_ACE_THREADS)
	return 0;
#else
	this->exit();
#endif
}
