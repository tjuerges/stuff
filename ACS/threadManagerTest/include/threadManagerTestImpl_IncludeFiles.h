/*
 * "@(#) $Id$"
 *
 * $Log$
 *
 */

#include <testThread.h>

#ifdef USE_PTHREADS
#include <safept/thread.h>
#else
#include <acsThread.h>
#endif

#ifdef NO_SHORT_LOG
#include <iostream>
#else
#include <logging.h>
#endif

#include <sstream>
