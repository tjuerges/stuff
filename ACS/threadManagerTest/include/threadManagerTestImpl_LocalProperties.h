/*
 * "@(#) $Id$"
 *
 * $Log$
 *
 */



ACE_Thread_Mutex mutex;
bool component_running;
unsigned long thread_counter;
unsigned long finishCounter;

public:
void increaseFinishCounter()
{
	mutex.acquire();
	++finishCounter;
	mutex.release();
};
