#ifndef threadManagerTestImpl_h
#define threadManagerTestImpl_h

/*
 * "@(#) $Id$"
 *
 * $Log$
 *
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <baciCharacteristicComponentImpl.h>

#include <threadManagerTestS.h>



#include <baciSmartPropertyPointer.h>

#include <threadManagerTest_IncludeFiles.h>

using namespace baci;

class threadManagerTestImpl: public virtual CharacteristicComponentImpl,
	public virtual POA_Test::threadManagerTest, public ActionImplementator
{
	public:
	/**
	 * Constructor
	 * @param containerServices ContainerServices which are needed for various component
	 * related methods.
	 * @param name component name
	 */
	threadManagerTestImpl(const ACE_CString& name, maci::ContainerServices* containerServices);

	/**
	 * Destructor
	 */
	virtual ~threadManagerTestImpl();

	/**
	 * Lifecycle method called at component initialization state.
	 * Overwrite it in case you need it.
	 */
	virtual void initialize(void) throw (acsErrTypeLifeCycle::acsErrTypeLifeCycleExImpl);

	/**
	 * Lifecycle method called at component execute state.
	 * Overwrite it in case you need it.
	 */
	virtual void execute(void) throw (acsErrTypeLifeCycle::acsErrTypeLifeCycleExImpl);

	/**
	 * Lifecycle method called at component clean up state.
	 * Overwrite it in case you need it.
	 */
	virtual void cleanUp(void);

	/**
	 * Lifecycle method called at component abort state.
	 * Overwrite it in case you need it.
	 */
	virtual void aboutToAbort(void);

	/**
	 * Action handler for asynchronus calls.
	 */
	virtual ActionRequest invokeAction (int function,
		BACIComponent* cob_p,
		const int& callbackID,
		const CBDescIn& descIn,
		BACIValue* value_p,
		Completion& completion,
		CBDescOut& descOut);


/* --------------- [ Action START implementator interface ] ------ */

/* --------------- [ Action END   implementator interface ] ------ */

/* --------------------- [ CORBA START interface ] ----------------*/
	virtual void start() throw(CORBA::SystemException);


/* --------------------- [ CORBA END interface ] ----------------*/

/* ----------------------------------------------------------------*/

	private:
	/**
	 * Copy constructor is not allowed following the ACS desgin rules.
	 */
	threadManagerTestImpl(const threadManagerTestImpl&);

	/**
	 * Assignment operator is not allowed due to ACS design rules.
	 */
	void operator=(const threadManagerTestImpl&);

/* --------------------- [ Properties START ] ----------------------*/
	std::string component_name_m;

/* --------------------- [ Properties END ] ------------------------*/

/* --------------------- [ Local properties START ] ----------------*/
#include <threadManagerTestImpl_LocalProperties.h>
/* --------------------- [ Local properties END ] ------------------*/
};


#endif /* threadManagerTestImpl_h */
