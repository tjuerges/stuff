#ifndef testThread_h
#define testThread_h

#ifdef USE_PTHREADS
#include <safept/thread.h>
#include <safept/impl/callback.h>
#elif defined (USE_ACE_THREADS)
#include <ace/Task.h>
#else
#include <acsThread.h>
#endif

#include <string>
#include <threadManagerTestImpl.h>


#ifdef USE_PTHREADS
class testThread
#elif defined (USE_ACE_THREADS)
class testThread: public ACE_Task_Base
#else
class testThread: public ACS::Thread
#endif
{
	public:
		typedef struct
		{
			threadManagerTestImpl* tm;
		} structForThread;

#ifdef USE_PTHREADS
		testThread(structForThread& data);
#elif defined USE_ACE_THREADS
		void setUp(const structForThread& data);
#else
		testThread(const ACE_CString &name,
			const structForThread* data,
			const bool suspended=false,
			const ACS::TimeInterval& responseTime=ThreadBase::defaultResponseTime,
			const ACS::TimeInterval& sleepTime=ThreadBase::defaultSleepTime,
			const bool del=true);
#endif

#ifndef USE_ACE_THREADS
		virtual ~testThread();
#endif

#ifdef USE_PTHREADS
		virtual void operator()();
#elif defined USE_ACE_THREADS
		virtual int svc();
#else
		virtual void run();
#endif

	private:
#if !defined(USE_PTHREADS) && !defined(USE_ACE_THREADS)
		std::string my_name;
#endif
		threadManagerTestImpl* tm;
};
#endif
