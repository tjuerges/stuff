#! /bin/bash
# $Id$

c=1

while [ $c -ne 500 ]; do
	rm -rf ExceptionTest$c
	mkdir ExceptionTest$c
	cp ExceptionTest/ExceptionTest.xml ExceptionTest$c/ExceptionTest$c.xml
	sed -i -e "s|ExceptionTest xmlns|ExceptionTest$c xmlns|g" ExceptionTest$c/ExceptionTest$c.xml
	sed -i -e "s|</ExceptionTest|</ExceptionTest$c|g" ExceptionTest$c/ExceptionTest$c.xml
	let c=c+1
done
