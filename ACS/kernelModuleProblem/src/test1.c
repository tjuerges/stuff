/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Jan 23, 2007  created
*/

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/completion.h>


MODULE_AUTHOR("Thomas Juerges <tjuerges@nrao.edu>");
MODULE_LICENSE("GPL");

/**
 * Module parameters.
 */
unsigned int foo = 1000U;
module_param(foo, uint, S_IRUGO);

/**
 * Local stuff
 */
int thread_id = 0;
wait_queue_head_t wq;
DECLARE_COMPLETION(on_exit);

/**
 * External kernel thread. Look in test2.c.
 */
extern int thread_code(void* data);


static int __init test_init(void)
{
    init_waitqueue_head(&wq);
    thread_id = kernel_thread(thread_code, NULL, CLONE_KERNEL);

    if(thread_id == 0)
    {
        return -EIO;
    }

    return 0;
}

static void __exit test_exit(void)
{
    printk("Module test stopped excution. Exiting...\n");
    if(thread_id != 0)
    {
        kill_proc(thread_id, SIGTERM, 1);
    }

    wait_for_completion(&on_exit);
}


module_init(test_init);
module_exit(test_exit);
