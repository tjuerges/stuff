/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Jan 23, 2007  created
*/
#include <asm/param.h>
#include <linux/sched.h>
#include <linux/completion.h>


/**
 * Symbols which should come from test1.c
 */
extern struct completion on_exit;
extern int thread_id;
extern wait_queue_head_t wq;
extern unsigned int foo;


int thread_code(void* data)
{
    unsigned long timeout = 0UL;
    int i = 0;

    daemonize("MyKThread");
    allow_signal(SIGTERM);
    for(; i < 10; ++i)
    {
        timeout = HZ; // wait 1 second
        timeout = wait_event_interruptible_timeout(wq, (timeout == 0), timeout);
        printk("thread_code: woke up, foo = %u...\n", foo);
        if(timeout == -ERESTARTSYS)
        {
            printk("got signal, break\n");
            break;
        }
    }

    thread_id = 0;
    complete_and_exit(&on_exit, 0);
}
