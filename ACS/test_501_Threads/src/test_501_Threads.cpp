#include <loggingMACROS.h>
#include <acsThreadManager.h>
#include <acsThread.h>

using namespace std;

namespace Control
{
	class LoopingThread: public ACS::Thread
	{
		public:
		LoopingThread(const ACE_CString& threadName,
					const ACS::TimeInterval& responseTime,
					const ACS::TimeInterval& sleepTime,
					bool del):
			ACS::Thread(threadName.c_str(), responseTime, sleepTime, del)
		{
		};

		~LoopingThread()
		{
		    this->terminate();
		}

		virtual void runLoop()
		{
			ACS_STATIC_SHORT_LOG((LM_INFO,"Here I am in a looping thread"));
		};
	};

	class LoopingThreadManager: public ACS::Thread
	{
		public:
		LoopingThreadManager(const ACE_CString& threadName,
					const ACS::TimeInterval& responseTime,
					const ACS::TimeInterval& sleepTime,
					bool del):
			ACS::Thread(threadName.c_str(), responseTime, sleepTime, del)
		{
		};

		~LoopingThreadManager()
		{
		    this->terminate();
		}

		virtual void runLoop()
		{
			ACS_STATIC_SHORT_LOG((LM_INFO,"Here I am in a looping manager thread"));
		};
	};

	class SinglePassThread: public ACS::Thread
	{
		public:
		SinglePassThread(const ACE_CString& threadName,
						const ACS::TimeInterval& responseTime,
						const ACS::TimeInterval& sleepTime,
						bool del):
			ACS::Thread(threadName.c_str(), responseTime, sleepTime, del)
		{
		};

		~SinglePassThread()
		{
		    this->terminate();
		}

		virtual void run()
		{
			ACS_STATIC_SHORT_LOG((LM_INFO,"This thread only executes once"));
		};
	};

	class SinglePassThreadManager: public ACS::Thread
	{
		public:
		SinglePassThreadManager(const ACE_CString& threadName,
						const ACS::TimeInterval& responseTime,
						const ACS::TimeInterval& sleepTime,
						bool del):
			ACS::Thread(threadName.c_str(), responseTime, sleepTime, del)
		{
		};

		~SinglePassThreadManager()
		{
		    this->terminate();
		}

		virtual void run()
		{
			ACS_STATIC_SHORT_LOG((LM_INFO,"This manager thread only executes once"));
		};
	};
};

int main(int argc, char* argv[])
{
	const ACS::TimeInterval sleepTime(10*1000*1000);

	std::cout << "Creating suspended auto-delete threads without thread manager..." << std::endl;
	/**
	 * Do not mess with the pointers!
	 * Otherwise things are screwed up and the thread propably cannot
	 * terminate/delete itself appropriately. Think of setting this
	 * pointer somewhere else = 0. Then the thread would try:
	 * delete this;
	 * Obviously a very bad idea with this=0.
	 */
	Control::SinglePassThread* singlePassThread1(new Control::SinglePassThread("SinglePass1", true, ACS::ThreadBase::defaultResponseTime, sleepTime, true));
	Control::LoopingThread* loopingThread1(new Control::LoopingThread("Loop1", true, ACS::ThreadBase::defaultResponseTime, sleepTime, true));

	std::cout << "Sleeping 5 seconds..." << std::endl;
	ACE_OS::sleep(5);

	std::cout << "Resuming the looping thread..." << std::endl;
	loopingThread1->resume();

	ACE_OS::sleep(5);

	std::cout << "Suspending the thread for 5 seconds..." << std::endl;
	loopingThread1->suspend();
	std::cout << "Done." << std::endl;
	ACE_OS::sleep(5);
	std::cout << "Resuming." << std::endl;
	loopingThread1->resume();

	std::cout << "Resuming the single pass thread..." << std::endl;
	singlePassThread1->resume();

	for(int i(10); i > 0; --i)
	{
		std::cout << "Running..." << std::endl;
		ACE_OS::sleep(1);
	}

	std::cout << "Stopping looping thread via stop() method..." << std::endl;
	loopingThread1->stop();

	std::cout << "Giving threads 10 seconds time to clean up." << std::endl;
	ACE_OS::sleep(10);

	std::cout << "Creating resumed auto-delete threads..." << std::endl;
	ACS::ThreadManager manager;
	manager.create<Control::SinglePassThreadManager>("SinglePass2", false, ACS::ThreadBase::defaultResponseTime, sleepTime, true);
	manager.create<Control::LoopingThreadManager>("Loop2", false, ACS::ThreadBase::defaultResponseTime, sleepTime, true);

	for(int i(10); i > 0; --i)
	{
		std::cout << "Running..." << std::endl;
		ACE_OS::sleep(1);
	}

	return 0;
}
