/**
 * $Id$
 */

#include <iostream>
#include <iomanip>
#include <acstime.h>
#include <TETimeUtil.h>


int main(int __attribute__((unused)) c, char* __attribute__((unused)) v[])
{
	EpochHelper now(TimeUtil::ace2epoch(ACE_OS::gettimeofday()));
	now.fromString(acstime::TSArray, "2005-10-10T16:21:17.30");
	EpochHelper e1980;
	e1980.fromString(acstime::TSArray, "1980-01-01T00:00:00.0");
	DurationHelper diff(now.difference(e1980.value()));
	const long double milliseconds(diff.toSeconds() * 1000.0);

	std::cout << now.value().value
        << ", "
        << e1980.value().value
        << ", "
        << diff.value().value
        << std::endl;
	std::cout << now.toString(acstime::TSArray, "%G", CORBA::Long(0L), CORBA::Long(0L))
        << std::endl;
	std::cout << e1980.toString(acstime::TSArray, "%G", CORBA::Long(0L), CORBA::Long(0L))
        << std::endl;
	std::cout << diff.toString("")
        << ", "
        << "Milliseconds since 1980 = "
        << std::setprecision(20)
        << milliseconds
        << std::endl << std::endl;

	acstime::Epoch a = TimeUtil::ace2epoch(ACE_OS::gettimeofday());
	acstime::Epoch b = TETimeUtil::unix2epoch(time(0));
	std::cout << TETimeUtil::toTimeDateString(a)
        << std::endl
        << TETimeUtil::toTimeDateString(b)
        << std::endl;

    return 0;
}
