/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2009
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * $Id$
 *
 * who      when     what
 * tjuerges  Dec 16, 2009  created
 */


#include <logTimingTestImpl.h>
#include <sstream>
#include <iostream>
#include <ios>
#include <iomanip>
#include <deque>
#include <numeric>
#include <cmath>
#include <loggingMACROS.h>
#include <LogToAudience.h>
#include <ace/Log_Priority.h>
#include <acscommonC.h>
#include <acsutilTimeStamp.h>
#include <TETimeUtil.h>
#include <maciACSComponentDefines.h>


template< typename T > T squareIt(T current, T newValue)
{
    return (current + (newValue * newValue));
}


Test::logTimingTest::logTimingTest(const ACE_CString& _name,
    maci::ContainerServices* _cs):
    acscomponent::ACSComponentImpl(_name, _cs),
    myName(_name.c_str()),
    cs(_cs),
    stopIt(false)
{
}

Test::logTimingTest::~logTimingTest()
{
    stopIt = false;
}

void Test::logTimingTest::runOnce(CORBA::UShort selectedLoggingType)
{
    std::ios::fmtflags oldFlags(std::cout.flags());
    std::cout.setf(std::ios::scientific, std::ios::floatfield);
    std::cout.precision(16);

    doTheLog(selectedLoggingType);

    std::cout.setf(oldFlags);
}

void Test::logTimingTest::runContinuous(CORBA::UShort selectedLoggingType,
    CORBA::Double sleepTime)
{
    if(sleepTime > 1.0)
    {
        sleepTime = 1.0;
    }

    stopIt = false;

    std::ostringstream output;
    output.setf(std::ios::scientific, std::ios::floatfield);
    output.precision(16);

    ACE_Time_Value waitFor(0ULL);
    ACS::Time start(0ULL);
    double duration(0.0), tmpValue(0.0);

    std::deque< double > stats(1000, 0ULL);
    std::deque< double >::size_type statsSize(stats.size());
    std::deque< double >::size_type index(0);

    while((stopIt == false) && ((++index) != statsSize))
    {
        start = ::getTimeStamp();
        runOnce(selectedLoggingType);
        stats.push_back(static_cast< double >(::getTimeStamp() - start)
            / TETimeUtil::ACS_ONE_SECOND * 1e6);
        waitFor.set(sleepTime);
        // Use ACE_OS::select to sleep.  This is an interruptible call
        // and other threads/processes can continue.
        ACE_OS::select(0, 0, 0, 0, waitFor);
    };

    // Calculate the numbers for the first batch.
    statsSize = stats.size();
    double tmp(0.0);
    double sum(std::accumulate(stats.begin(), stats.end(), tmp));
    double mean(sum / statsSize);
    double sumSquared(sum * sum);
    tmp = 0.0;
    double sumOfSquares(
        std::accumulate(stats.begin(), stats.end(), tmp, squareIt< double >));
    double variance((sumOfSquares - sumSquared / statsSize) / (statsSize - 1));

    double manualVariance(0.0);

    for(std::deque< double >::const_iterator i(stats.begin());
        i != stats.end(); ++i)
    {
        manualVariance += ((*i - mean) * (*i - mean));
    }
    manualVariance /= (statsSize - 1);

    output << "Initial Sum = "
        << sum
        << "[us], initial Mean = "
        << mean
        << "[us], initial Sigma = "
        << std::sqrt(variance)
        << "[us], manual Sigma = "
        << std::sqrt(manualVariance)
        << "[us].";
    LOG_TO_DEVELOPER(::LM_ALERT, output.str());

    // Now continue and update the numbers as we go.
    index = 0;
    while(stopIt == false)
    {
        ++index;

        start = ::getTimeStamp();
        runOnce(selectedLoggingType);
        duration = static_cast< double >(::getTimeStamp() - start)
            / TETimeUtil::ACS_ONE_SECOND * 1e6;

        tmpValue = stats.front();
        stats.pop_front();
        stats.push_back(duration);

        // Remove the oldest value from the sums.
        sum -= tmpValue;
        sum += duration;

        // New mean.
        mean = (sum / statsSize);

        // New squared mean.
        sumSquared = (sum * sum) / statsSize;

        // Remove the oldest value from the sum of the squared values.
        sumOfSquares -= (tmpValue * tmpValue);
        // Add the latest value.
        sumOfSquares += (duration * duration);

        // New variance.
        variance = (sumOfSquares - sumSquared / statsSize) / (statsSize - 1);

        if(index >= (1.0 / sleepTime))
        {
            output.str("");
            output << "Index = "
                << index
                << ", Sum = "
                << sum
                << "[us], Mean = "
                << mean
                << "[us], Sigma = "
                << std::sqrt(variance)
                << "[us].";
            LOG_TO_DEVELOPER(::LM_ALERT, output.str());
            index = 0;
        }

        waitFor.set(sleepTime);
        // Use ACE_OS::select to sleep.  This is an interruptible call
        // and other threads/processes can continue.
        ACE_OS::select(0, 0, 0, 0, waitFor);
    };

    output.str("");
    output << "Sum = "
        << sum
        << "[us], Mean = "
        << mean
        << "[us], Sigma = "
        << std::sqrt(variance)
        << "[us].";
    LOG_TO_DEVELOPER(::LM_ALERT, output.str());

    stopIt = false;
}

void Test::logTimingTest::stopContinuous()
{
    stopIt = true;
}


inline void Test::logTimingTest::doTheLog(
    CORBA::UShort selectedLoggingType) const
{
    switch(selectedLoggingType)
    {
        case 0:
        {
            ACS_LOG(LM_SOURCE_INFO, "Test::logTimingTest::doTheLog",
                (::LM_INFO, "This is a log."));
        }
        break;

        case 1:
        {
            LOG_TO_DEVELOPER(::LM_INFO, "This is a log.");
        }
        break;

        case 2:
        {
            LOG(Logging::BaseLog::LM_INFO, "Test::logTimingTest::doTheLog",
                "This is a log.");
        }
        break;

        default:
        break;
    };
}


MACI_DLL_SUPPORT_FUNCTIONS(Test::logTimingTest)
