#ifndef logTimingTestImpl_h
#define logTimingTestImpl_h
#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2009
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * $Source$
 * $Id$
 *
 * who      when     what
 * tjuerges  Dec 16, 2009  created
 */


#include <logTimingTestS.h>
#include <acscomponentImpl.h>
#include <string>


namespace maci
{
    class ContainerServices;
}


namespace Test
{
    class logTimingTest: public virtual POA_Test::LogTimingTest,
        public acscomponent::ACSComponentImpl
    {
        public:
        logTimingTest(const ACE_CString& _name,
            maci::ContainerServices* _cs);

        virtual ~logTimingTest();

        virtual void runOnce(CORBA::UShort selectedLoggingType);

        virtual void runContinuous(CORBA::UShort selectedLoggingType,
            CORBA::Double sleepTime);

        virtual void stopContinuous();


        private:
        logTimingTest(const logTimingTest&);

        logTimingTest& operator=(const logTimingTest&);

        void doTheLog(CORBA::UShort selectedLoggingType) const;


        const std::string myName;
        maci::ContainerServices* cs;
        bool stopIt;
    };
}
#endif
