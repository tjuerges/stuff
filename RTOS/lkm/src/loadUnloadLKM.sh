#!/bin/bash

TZ=UTC
export TZ

if [ "x$1" == "x" ] ; then
    echo "usage: loadUnloadLKM LKMcfgFile [number of iterations]"
    exit
fi

LKM=$1

trap "unloadLkmModule $LKM; exit" SIGINT SIGTERM

max_iter=1000
if [ "x$2" != "x" ] ; then
    max_iter=$2
fi

i=0
while [ $i -lt $max_iter ]
do
echo $i
top -b -n 1|awk '/Mem:/ {print strftime() " " $2 " " $4 " " $6}' | sed -e 's/k//g'
loadLkmModule $LKM
unloadLkmModule $LKM
i=`expr $i + 1`
done
