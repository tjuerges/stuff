/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of tyhe GNU Lesser General Public
*License as published by the Fre`e Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more detail2s.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* 
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2003-11-06  created
*/

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!   */
/*    This file is just needed for building (un)loadLmModule     */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!   */

/* 
 * System Headers
 */
#ifndef __KERNEL__
#define __KERNEL__
#endif
#ifndef MODULE
#define MODULE
#endif

//#include <linux/module.h>
//#include <linux/errno.h>
//#include <linux/vmalloc.h>


//#include <rtai.h>
//#include <rtai_sched.h>
//#include <rtai_fifos.h>
//#include <rtai_sem.h>

//MODULE_LICENSE("GPL");

/**************************************************
 * Module Initialization
 **************************************************/
int init_module(void){
  return 0;
}

/***************************************************
 * Module Termintaion
 ***************************************************/

void cleanup_module(void) {
  return;
}
