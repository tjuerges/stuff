/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2004 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* srankin  2004-07-15  created 
*/

#include "vltPort.h"

static char *rcsId="@(#) $Id$";
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <LKM.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/utsname.h>
#include <unistd.h>
#include <limits.h>

void LKM::becomeRoot(void) throw (LKMerror)
{
  int error = setuid(0);

  if (0 != error) {
    throw LKMerror("this program must be owned by root to run.", "");
  }
}

void LKM::system(const char* command) throw (LKMerror)
{
  int rval = std::system(command);

  if (0 != rval) {
    throw LKMerror("can not execute system command", command);
  }
}


void LKM::loadMod(const char* moduleLoad) throw (LKMerror) 
{
  char fullPath[NAME_MAX];
  char cmd[NAME_MAX];
  char moduleName[NAME_MAX];
  char* moduleOpt;
  int  rval;

 
  strcpy(moduleName,moduleLoad);
  /* Actually the moduleName can contain options as well, so we need to 
     only find the filename up to the first white space
  */
  if (strchr(moduleName, ' ') != NULL) {
    moduleOpt  = strchr(moduleLoad, ' ') +1;
    *strchr(moduleName, ' ') = '\0';
  } else {
    moduleOpt = NULL;
  }
  
/* Here we are checking for the fight scheduler  */
  if ( !verbatim_m &&
       (strcmp(moduleName, "rtai_up")==0 || 
       strcmp(moduleName, "rtai_smp")==0 ||
       strcmp(moduleName, "rtai_ksched")==0)
      )
      {
      if ( getNumberOfCPUs()>1 )
	  {
	  if (strcmp(moduleName, "rtai_smp")!=0)
	      {
	      printf("WARNING: Scheduler rtai_smp is going to be loaded instead specifed: %s\n", moduleName);
	      strcpy(moduleName, "rtai_smp");
	      }
	  }
      else/* =1*/
	  {
	   if (strcmp(moduleName, "rtai_up")!=0)
	      {
	      printf("WARNING: Scheduler rtai_up is going to be loaded instead specifed: %s\n", moduleName);
	      strcpy(moduleName, "rtai_up");
	      }//if
	  }
      }//if

  sprintf(cmd, "grep -c -e\"^%s \" /proc/modules > /dev/null", moduleName);
  rval = std::system(cmd);
  if (rval==0)
      {
      printf("WARNING: Module: %s is already loaded\n", moduleName);
      return;
      }

  struct utsname systemInfo;
  std::string kernelPath("rtai" + '.');
  if(uname(&systemInfo) == -1)
  {
  	kernelPath.clear();
  }
  else
  {
  	kernelPath += systemInfo.release;
  }

  if (findFile(fullPath, moduleName, kernelPath.c_str(), "ko")
  || findFile(fullPath, moduleName, "rtai", "ko")) {
    /* The module exists in the ALMA Tree use it */
    if (moduleOpt == NULL) {
      sprintf(cmd,"%s %s","/sbin/insmod",fullPath);
    } else {
      sprintf(cmd,"%s %s %s","/sbin/insmod",fullPath,moduleOpt);
    }
    printf("Using %s\n", fullPath);
  } else {
  printf("We did not find the module in the ALMA tree, try using the default linux location\n");
    /* We didn't find the module in the ALMA tree, try using the default
       linux location 
       In this case we use modprobe instead insmod as suggested in SPR: ALMASW2007032 
    */
    if (moduleOpt == NULL) {
      sprintf(cmd,"%s %s","/sbin/modprobe",moduleName);
    } else {
      sprintf(cmd,"%s %s %s","/sbin/modprobe",moduleName,moduleOpt);
    }
  }
 
  rval = std::system(cmd);
  if (0 != rval) {
    throw LKMerror("can not load kernel module", moduleName);
  }
}


void LKM::unloadMod(const char* moduleName) throw (LKMerror)
{
  char cmd[256];
  int  rval;
  char module[NAME_MAX];

  strcpy(module, moduleName);

/* Here we are checking for the fight scheduler  */
  if ( !verbatim_m &&
       (strcmp(moduleName, "rtai_up")==0 || 
	strcmp(moduleName, "rtai_smp")==0 ||
	strcmp(moduleName, "rtai_ksched")==0)
      )
      {
      if ( getNumberOfCPUs()>1 )
	  {
	  if (strcmp(moduleName, "rtai_smp")!=0)
	      {
	      printf("WARNING: Scheduler rtai_smp is going to be unloaded instead specifed: %s\n", moduleName);
	      strcpy(module, "rtai_smp");
	      }
	  }
      else/* =1*/
	  {
	  if (strcmp(moduleName, "rtai_up")!=0)
	      {
	      printf("WARNING: Scheduler rtai_up is going to be unloaded instead specifed: %s\n", moduleName);
	      strcpy(module, "rtai_up");
	      }//if
	  }
      }//if

  sprintf(cmd,"%s %s","/sbin/rmmod", module);
  rval = std::system(cmd);

  if (0 != rval) {
    throw LKMerror("can not unload kernel module", module);
  }
}//unloadMod

bool LKM::parseConfigFile(FILE* fd, 
			  std::vector<lkmCommand>& cmds, 
			  bool unload)
  throw (LKMerror)
{
  lkmCommand tmpCmd;
  char*      buffer;
  char*      tok;
  long       length;

  bool       loadLabelFound = false;
  bool       unloadLabelFound = false;
  

  /* This is easier to parse as a character buffer than from file
     so load it all into the file
  */
  if(fseek(fd, 0, SEEK_END)){
    throw LKMerror("error reading configuration file.","");
  }
  length = ftell(fd);
  rewind(fd);

  buffer = new char[length+1];
  if (fread(buffer, sizeof(char), length, fd) != (unsigned long)length) {
    throw LKMerror("error reading configuration file.", "");
  }

  /* Now make a first pass though looking for the labels */
  tok = strtok(buffer,"\n");
  while (tok != NULL) {
    if (!strncmp(tok,"UNLOAD SEQUENCE:",strlen("UNLOAD SEQUENCE:"))){
      unloadLabelFound = true;
    }

    if (!strncmp(tok,"LOAD SEQUENCE:",strlen("LOAD SEQUENCE:"))){
      loadLabelFound = true;
    }
    tok = strtok(NULL,"\n");
  }

  /* Now reload the buffer */
  rewind(fd);
  if (fread(buffer, sizeof(char), length, fd) != (unsigned long)length) {
    throw LKMerror("error reading configuration file", "");
  }

  /* Now we want to advance through the buffer to the correct 
   * start position
   */
  tok = strtok(buffer,"\n");
  if (unloadLabelFound && unload) {
    /* Advance to the unload label */
     while (strncmp(tok,"UNLOAD SEQUENCE:",strlen("UNLOAD SEQUENCE:"))) {
       tok = strtok(NULL,"\n");
     }
     tok = strtok(NULL,"\n");
  } else if (loadLabelFound) {
    /* Advance to the load label */
    while (strncmp(tok,"LOAD SEQUENCE:",strlen("LOAD SEQUENCE:"))) {
      tok = strtok(NULL,"\n");
    }
    tok = strtok(NULL,"\n");
  }
    
  // Now start parsing lines
  while (tok != NULL) {
    /* Get rid of all the comment lines */
    if (tok[0] != '#') {
      if (!strncmp(tok,"MODULE",strlen("MODULE"))){
	/* Module definition*/
	tmpCmd.lkm_op = LKM_MODULE;
	if (unload) {
	  /* Ignore all but the module name */
	  char* tmpChar = strdup(strchr(tok,' ' )+1);
	  if (strchr(tmpChar,' ') != NULL) { 
	    *strchr(tmpChar,' ') = '\0';
	  }
	  tmpCmd.options = tmpChar;
	} else {
	  tmpCmd.options = strchr(tok,' ')+1;
	}
	cmds.push_back(tmpCmd);
      }
      if (!strncmp(tok,"SYSTEM",strlen("SYSTEM"))){
	/* System Definiton */
	tmpCmd.lkm_op = LKM_SYSTEM;
	tmpCmd.options = strchr(tok,' ')+1;
	cmds.push_back(tmpCmd);
      }
      if (!strncmp(tok,"UNLOAD SEQUENCE:",strlen("UNLOAD SEQUENCE:"))) {
	/* This is an exit condition */
	break;
      }
    }
    tok = strtok(NULL,"\n");
  }

  delete[] buffer;
  return unloadLabelFound;
}

bool LKM::findFile(char*       fullPath,
		   const char* filename,
		   const char* subDir,
		   const char* suffix) {
  char*  envVarValue;
// here can be used acsfindFile, but than we have to staticaly link whole ACE in loadXXX...

/* if modpath is set than we should look in ../ as well */
  envVarValue = getenv("MODPATH");
  if (envVarValue != NULL) 
      {
      envVarValue = strdup("..");
      if (suffix == NULL) 
	  {
	  sprintf(fullPath,"%s/%s/%s",envVarValue,subDir, filename);
	  } 
      else 
	  {
	  sprintf(fullPath,"%s/%s/%s.%s",envVarValue,subDir, filename,suffix);
	  }  
      free(envVarValue);
      if (!access(fullPath, R_OK)) 
	  {
	  /* Great we found the file */
	  return true;
	  }
      }//MODPATH


  envVarValue = getenv("MODROOT");
  if (envVarValue != NULL) {
    if (suffix == NULL) {
      sprintf(fullPath,"%s/%s/%s",envVarValue,subDir, filename);
    } else {
      sprintf(fullPath,"%s/%s/%s.%s",envVarValue,subDir, filename,suffix);
    }
    if (!access(fullPath, R_OK)) {
      /* Great we found the file */
      return true;
    }
  }

  /* Next priority is the INTROOT */
  envVarValue = getenv("INTROOT");
  if (fullPath != NULL) {
    if (suffix == NULL) {
      sprintf(fullPath,"%s/%s/%s",envVarValue,subDir, filename);
    } else {
      sprintf(fullPath,"%s/%s/%s.%s",envVarValue,subDir, filename,suffix);
    }
    if (!access(fullPath, R_OK)) {
      /* Great we found the file */
      return true;
    }
  }

  /* Now check through the INTLIST if it is defined */
  envVarValue = getenv("INTLIST");
  if (envVarValue != NULL) {
    envVarValue = strdup(getenv("INTLIST")); // we have to make copy otherwise next time the INTLIST is corupted
    char* tok = strtok(envVarValue,":");
    while (tok != NULL) {
      if (suffix == NULL) {
	sprintf(fullPath,"%s/%s/%s", tok, subDir, filename);
      } else {
	sprintf(fullPath,"%s/%s/%s.%s", tok, subDir, filename,suffix);
      }
      if (!access(fullPath, R_OK)) {
	/* Great we found the file */
	return true;
      } 
      tok = strtok(NULL, ":");
    }
    free(envVarValue);
  }

  /* look in the ACSROOT for the file */
  envVarValue = getenv("ACSROOT");
  if (envVarValue != NULL) {
    if (suffix == NULL) {
      sprintf(fullPath,"%s/%s/%s",envVarValue,subDir, filename);
    } else {
      sprintf(fullPath,"%s/%s/%s.%s",envVarValue,subDir, filename,suffix);
    }
    if (!access(fullPath, R_OK)) {
      /* Great we found the file */
      return true;
    }
  }

 /* Finally look in the RTAI_HOME/modules for the file */
  envVarValue = getenv("RTAI_HOME");
  if (envVarValue != NULL) {
    if (suffix == NULL) {
      sprintf(fullPath,"%s/modules/%s",envVarValue, filename);
    } else {
      sprintf(fullPath,"%s/modules/%s.%s",envVarValue, filename, suffix);
    }
    if (!access(fullPath, R_OK)) {
      /* Great we found the file */
      return true;
    }
  }

  return false;
}


FILE* LKM::findConfigFile(const char* filename)
{
    char      fullPath[NAME_MAX];

    if (strchr(filename, '/')==NULL)  // filename does not contain path
	{
	if (!findFile(fullPath, filename, "config")) 
	    return(NULL);
	}
    else
	{
	sprintf(fullPath, "%s", filename);
	}
    
    printf("LKM configuration file: %s\n", fullPath);
    return(fopen(fullPath, "r"));
}


int LKM::getNumberOfCPUs()
{
    char line[1024];
    int processorCount = 0;
    char *pCharsRead;

    FILE *procCpuInfoFP = fopen("/proc/cpuinfo","r");
    if( procCpuInfoFP == 0 )
	{
	printf("WARNING: can't open /proc/cpuinfo - asuming there is one CPU\n");
	fclose(procCpuInfoFP);
	return 1;   // at least 1 cpu even if we can't read cpuinfo
	}

    // read each line of /proc/cpuinfo counting 'processor' lines
    do
	{
	pCharsRead = fgets(&line[0], 1023, procCpuInfoFP);
	if( strncmp(line,"processor",9) == 0 )
	    processorCount++;
	}while( pCharsRead );
    fclose(procCpuInfoFP);

    // just in case we mis-counted
    if( processorCount == 0 )
	processorCount = 1;

    return processorCount;
} 

/*___oOo___*/
