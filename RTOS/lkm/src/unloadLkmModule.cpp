#include <LKM.h>
#include <iostream>
using namespace std;

int main(int argc, char** argv) {
  LKM                     lkm;
  FILE*                   lkmFd;
  std::vector<lkmCommand> cmds;

  bool unloadFound;

  try {
    lkm.becomeRoot();
  }
  catch (LKMerror &ex) {
    cerr << "ERROR: Problem obtaining root privilege" << endl
	 << "Check that " << argv[0] << " is setuid root." << endl
	 << "Error message is:" << endl;
    ex.printErr();
    return -LKMERR_NOSUID;
  }

  if (argc < 2) {
    /*Error no file specified */
    cerr << "ERROR: No configuration file specified" << endl;
    cout << "Usage: unloadLkmModule LKM_configuration_file [--verbatim]" << endl;
    return -LKMERR_NOFILE;
  }

  if ( argc>2 && strcmp(argv[2], "--verbatim")==0 )
      {
      cout << "Verbatim mode of reading LKM configuration file will be used." << endl;
      lkm.setVerbatim(true);
      }


  lkmFd = lkm.findConfigFile(argv[1]);
  if (lkmFd == NULL) {
    /* This is an error opening the file */
    cerr << "ERROR: Configuration file " << argv[1] << " not found" << endl;
     return -LKMERR_FILENOTFOUND;
  }
  
  try {
    unloadFound = lkm.parseConfigFile(lkmFd, cmds, true);
  }
  catch (LKMerror &ex) {
    cerr << "ERROR: Problem parsing the configuration file called " << argv[1]
	 << endl << "Error message is:" << endl;
    ex.printErr();
    return -LKMERR_PARSEERROR;
  }

  fclose(lkmFd);

  int allMod=0, succUnld=0;
  try {
    if (unloadFound) {
      std::vector<lkmCommand>::iterator iter;
      iter = cmds.begin();
      while ( iter != cmds.end()) {
	switch(iter->lkm_op) {
	case LKM_MODULE:
	  try {
	  allMod++;
	  lkm.unloadMod((iter->options).c_str());
	  succUnld++;
	  }
	  catch (LKMerror &ex) {
	    cerr << "WARNING: Unable to unload module " << iter->options 
		 << endl;
	  }
	  break;
	case LKM_SYSTEM:
	  try {
	    lkm.system((iter->options).c_str());
	  }
	  catch (LKMerror &ex) {
	    cerr << "WARNING: System command failed to execute." << endl
		 << "Error message is:" << endl;
	    ex.printErr();
	  }
	  break;
	}
	iter++;
      }
    } else {
      std::vector<lkmCommand>::reverse_iterator iter;
      iter = cmds.rbegin();
      while ( iter != cmds.rend()) {
	switch(iter->lkm_op) {
	case LKM_MODULE:
	  try {
	  allMod++;
	  lkm.unloadMod((iter->options).c_str());
	  succUnld++;
	  }
	  catch (LKMerror &ex) {
	    cerr << "WARNING: Unable to unload module " << iter->options 
		 << endl;
	  }
	  break;
	case LKM_SYSTEM:
	  try {
	    lkm.system((iter->options).c_str());
	  }
	  catch (LKMerror &ex) {
	    cerr << "WARNING: System command failed to execute." << endl
		 << "Error message is:" << endl;
	  }
	  break;
	}
	iter++;
      }
    }
  }
  catch (LKMerror &ex) {
    cerr << "ERROR: Problem executing the configuration file. " << endl
	 << "Error message is:" << endl;
    ex.printErr();
    return -LKMERR_UNLOADERROR;
  }

  printf("Unloaded: %d of %d kernel modules\n", succUnld, allMod);
  return 0;
}
