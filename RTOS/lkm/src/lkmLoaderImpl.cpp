/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2003-12-19  created 
*/

/************************************************************************
*     This is the AmbController class, this provides an interface which 
*   can be used to controll the AMB.  This does not provide device level
*   interaction, use the ambDevice class for that.
*------------------------------------------------------------------------
*/

#include "lkmLoaderImpl.h"
#include <LKM.h>
#include <baciDB.h>

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

lkmLoaderImpl::lkmLoaderImpl( const ACE_CString& name, maci::ContainerServices *cs):
  acscomponent::ACSComponentImpl(name, cs) 
{
  configFile_p = NULL;
}

lkmLoaderImpl::~lkmLoaderImpl(){
  if (configFile_p != NULL) 
      {
      delete[] configFile_p;
      }
}

/* ----------------------- [ Lifecycle Interface ] ------------------ */

void lkmLoaderImpl::initialize()
    throw (acsErrTypeLifeCycle::acsErrTypeLifeCycleExImpl) {

  if (configFile_p == NULL) {
    /* Get the name of the config file from the CDB */
    
    ACE_CString fullName = baci::DBConnector::getFullAddress(this->name());
    Field fld;
    String cfgFileStr;
    char *fldName1 = "configuration_file";

    if (! baci::DBConnector::getDBTable()->GetField(fullName,fldName1,fld)) 
	{
	ACS_SHORT_LOG((LM_ERROR,"lkmLoaderImpl: DB access failed for %s.",
		     fldName1));
	throw acsErrTypeLifeCycle::LifeCycleExImpl(__FILE__, __LINE__, "lkmLoaderImpl::initialize");
    } 
    else 
	{
	try
	    {
	    fld.GetString(cfgFileStr);
	    loadModules(cfgFileStr.c_str());
	    }
	catch (lkmErrType::loadModulesErrorEx &ex)
	    {
	    throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__, "lkmLoaderImpl::initialize");
	    }
	}//if-else
  }//if
}

void lkmLoaderImpl::cleanUp(void) {
    if (configFile_p != NULL) 
	{
	try
	    {
	    unloadModules();
	    }
	catch (lkmErrType::unloadModulesErrorEx &_ex)
	    {
	      lkmErrType::unloadModulesErrorExImpl ex(_ex);
	      ex.log();
	    }//try-catch
	}
}

void lkmLoaderImpl::aboutToAbort(void) {
  if (configFile_p != NULL) 
      {
      try
	  {
	  unloadModules();
	  }
      catch (lkmErrType::unloadModulesErrorEx &_ex)
	  {
	    lkmErrType::unloadModulesErrorExImpl ex(_ex);
	    ex.log();
	  }//try-catch
      }
}

/* ----------------------- [ Implementation ] ------------------------- */
void lkmLoaderImpl::loadModules(const char* filename) 
  throw(CORBA::SystemException, lkmErrType::loadModulesErrorEx) 
{
    if (configFile_p == NULL) 
	{
	char cmd[256];
	sprintf(cmd,"loadLkmModule %s",filename);
	ACS_LOG(LM_RUNTIME_CONTEXT, "lkmLoaderImpl::loadModules", (LM_INFO, "Command: \"%s\" is going to be executed", cmd));
	int st = system(cmd);
	if (st!=0)
	    {	
	      lkmErrType::loadModulesErrorExImpl ex(__FILE__, __LINE__, "lkmLoaderImpl::loadModules");
	      ex.setConfigFile(filename);
	      char retv=(st >> 8);
	      ex.setReturnCode(retv);
	      throw ex.getloadModulesErrorEx();
	    }
	if (configFile_p != NULL) 
	    delete[] configFile_p;
	configFile_p = strdup(filename);
	}
    else 
	{
	  lkmErrType::loadModulesErrorExImpl ex(__FILE__, __LINE__, "lkmLoaderImpl::loadModules");
	  ex.setConfigFile("NULL");
	  throw ex.getloadModulesErrorEx();
	}
}//lkmLoaderImpl::loadModules

void lkmLoaderImpl::unloadModules(void) 
  throw(CORBA::SystemException, lkmErrType::unloadModulesErrorEx) 
{

    if (configFile_p != NULL) 
	{
	char cmd[256];
	sprintf(cmd,"unloadLkmModule %s",configFile_p);
	ACS_LOG(LM_RUNTIME_CONTEXT, "lkmLoaderImpl::unloadModules", (LM_INFO, "Command: \"%s\" is going to be executed", cmd));
	int st = system(cmd);

	if (st!=0)
	    {
	      lkmErrType::unloadModulesErrorExImpl ex(__FILE__, __LINE__, "lkmLoaderImpl::unloadModules");
	      ex.setConfigFile(configFile_p);
	      char retv=(st >> 8);
	      ex.setReturnCode(retv);
	      throw ex.getunloadModulesErrorEx();
	    }
	free(configFile_p);
	configFile_p = NULL;
	}
    else 
	{
	  lkmErrType::unloadModulesErrorExImpl ex(__FILE__, __LINE__, "lkmLoaderImpl::unloadModules");
	  ex.setConfigFile("NULL");
	  throw ex.getunloadModulesErrorEx();
	}
}//lkmLoaderImpl::unloadModules


CORBA::Boolean lkmLoaderImpl::isLoaded(const char *lkm)
    throw(CORBA::SystemException)
{
    char cmd[64];
    int  rval;
    
    sprintf(cmd, "grep -c -e\"^%s \" /proc/modules > /dev/null", lkm);
    rval = std::system(cmd);
    
    return rval==0;
}//lkmLoaderImpl::isLoaded

/* -------------------- [ MACI DLL support functions ] ------------------*/
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(lkmLoaderImpl)
/*___oOo___*/
