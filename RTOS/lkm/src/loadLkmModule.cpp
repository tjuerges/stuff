#include <LKM.h>
#include <iostream>
using namespace std;

int main(int argc, char** argv) {
  LKM                     lkm;
  FILE*                   lkmFd;
  std::vector<lkmCommand> cmds;
  std::vector<lkmCommand>::iterator iter;

  try {
    lkm.becomeRoot();
  }
  catch (LKMerror &ex) {
    cerr << "ERROR: Problem obtaining root privilege" << endl
	 << "Check that " << argv[0] << " is setuid root." << endl
	 << "Error message is:" << endl;
    ex.printErr();
    return -LKMERR_NOSUID;
  }

  if (argc < 2) {
    /*Error no file specified */
    cerr << "ERROR: No configuration file specified" << endl;
    cout << "Usage: loadLkmModule LKM_configuration_file [--verbatim]" << endl;
    return -LKMERR_NOFILE;
  }

  if ( argc>2 && strcmp(argv[2], "--verbatim")==0 )
      {
      cout << "Verbatim mode of reading LKM configuration file will be used." << endl;
      lkm.setVerbatim(true);
      }

  lkmFd = lkm.findConfigFile(argv[1]);
  if (lkmFd == NULL) {
    /* This is an error opening the file */
    cerr << "ERROR: Configuration file " << argv[1] << " not found or can not be accessible" << endl;
    return -LKMERR_FILENOTFOUND;
  }
  
  try {
    lkm.parseConfigFile(lkmFd, cmds, false);
  }
  catch (LKMerror &ex) {
    cerr << "ERROR: Problem parsing the configuration file called " << argv[1]
	 << endl << "Error message is:" << endl;
    ex.printErr();
    return -LKMERR_PARSEERROR;
  }

  fclose(lkmFd);

  int allMod=0, succLoad=0;
  iter = cmds.begin();
  try {
    while ( iter != cmds.end()) {
      switch(iter->lkm_op) {
      case LKM_MODULE:
	try {
	allMod++;
	  lkm.loadMod((iter->options).c_str());
	  succLoad++;
	} 
	catch (LKMerror &ex) {
	  cerr << "WARNING: Unable to load module " << iter->options << endl;
	}
	break;
      case LKM_SYSTEM:
	try {
	  lkm.system((iter->options).c_str());
	} 
	catch (LKMerror &ex) {
	  cerr << "WARNING: System command failed to execute." << endl
	       << "Error message is:" << endl;
	  ex.printErr();
	}
	break;
      }
      iter++;
    }
  }
  catch (LKMerror &ex) {
    cerr << "ERROR: Problem executing the configuration file. " << endl
	 << "Error message is:" << endl;
    ex.printErr();
    return -LKMERR_LOADERROR;
  }

  printf("Loaded: %d of %d kernel modules\n", succLoad, allMod);
  
  return 0;
}
