#ifndef LKMLOADERIMPL_H
#define LKMLOADERIMPL_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2003-12-17  created
*/

/************************************************************************
 * This is the implementation header for the CAN controller
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <acscomponentImpl.h>
#include <lkmLoaderS.h>
#include <lkmErrType.h>

//using namespace acscomponent;

class lkmLoaderImpl : public virtual acscomponent::ACSComponentImpl,
		      public virtual POA_ACS_RT::lkmLoader
{
 public:
  lkmLoaderImpl(const ACE_CString& name, maci::ContainerServices *);
  ~lkmLoaderImpl();

  /* Lifecycle Interface */
  void initialize()
      throw(acsErrTypeLifeCycle::acsErrTypeLifeCycleExImpl);
  void cleanUp(void);
  void aboutToAbort(void);

  /* Implementation */
  virtual void loadModules(const char* configFile)
    throw(CORBA::SystemException, lkmErrType::loadModulesErrorEx);
  
  virtual void unloadModules(void)
    throw(CORBA::SystemException, lkmErrType::unloadModulesErrorEx);

  virtual CORBA::Boolean isLoaded(const char *lkm)
    throw(CORBA::SystemException);
  
 private:
  char* configFile_p;
};
#endif /*!LKMLOADERIMPL*/
