#ifndef _LKM__H_
#define _LKM__H_
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2004 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* srankin  2004-07-15  created
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif


#include <stdlib.h>
#include <string.h>
#include <string>
#include <stdio.h>
#include <vector>

typedef enum {
  LKM_MODULE,
  LKM_SYSTEM
} lkmOperation;

typedef struct {
  lkmOperation  lkm_op;
  std::string   options;
} lkmCommand;         

typedef enum {
  LKMERR_NOSUID       = 1,
  LKMERR_NOFILE       = 2,
  LKMERR_FILENOTFOUND = 3,
  LKMERR_PARSEERROR   = 4,
  LKMERR_LOADERROR    = 5,
  LKMERR_UNLOADERROR  = 6
} LKMErrorType;

// This is an exception class used to report the failure to 
//  load or unload a module.

class LKMerror
{

  public:

    LKMerror(char* task, const char* detail) {
      m_task = strdup(task);
      m_detail = strdup(detail);
    }
    
    ~LKMerror(){
      free(m_detail);
      free(m_task);
    }

    void printErr(){fprintf(stderr,"LKM Error: %s %s\n",m_task, m_detail);}

// copy constructor
  LKMerror(const  LKMerror& le){ 
      m_task = strdup(le.m_task);
      m_detail = strdup(le.m_detail);
  }

  protected: 

    // None
  private:

   char* m_detail;
   char* m_task;

};


// This is the class that does the real work.
class LKM
{ 

  public:
    LKM() {verbatim_m = false;}

    void becomeRoot(void) throw (LKMerror);
    void loadMod(const char* moduleName) throw (LKMerror);
    void unloadMod(const char* moduleName) throw (LKMerror);
    void system(const char* command) throw (LKMerror);

    FILE* findConfigFile(const char* filename);
    bool  parseConfigFile(FILE* fd, std::vector<lkmCommand>& cmds,
			  bool unload) throw (LKMerror);

    void setVerbatim(bool flag) { verbatim_m = flag; }

  protected:
    /* flag which indicates if the LKM configuration file should be interpreted verbatim */
    bool verbatim_m;


  private:
    /* This function is essentially a re-implementation of the acsFindFile 
       utility, but because loading libraries from SETUID process is unwise
       I put it here again.

       If the file is found and is accessible (readable) 
       the entire path is placed in fullPath and the 
       return flag is set to true.
    */
    bool findFile(char*       fullPath,
		  const char* filename,
		  const char* subDir,
		  const char* suffix = NULL);
    /*
      It retunrs number of CPU,
      which can be used to determine which scheduer to use.
     */
    int getNumberOfCPUs();
};

#endif /* _LKM__H_ */
