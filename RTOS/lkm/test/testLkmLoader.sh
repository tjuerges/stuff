#! /bin/bash

# since we need installed loadLkmModule we have to add temporary INTROOT
PATH=$PWD/tmp/introot/bin:$PATH
ACS_INSTANCE=`cat $PWD/tmp/acs_instance`
export MODROOT=$PWD/..
export ACS_LOG_STDOUT=4
acsStartContainer -cpp LkmTestContainer &
sleep 35
#check if module was loaded
ssh -x root@$HOST /sbin/lsmod | grep rtai_hal
acsStopContainer LkmTestContainer > /dev/null
sleep 2
# check if module was unloaded
ssh -x root@$HOST /sbin/lsmod | grep rtai_hal
