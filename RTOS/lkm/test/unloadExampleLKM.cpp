#include <LKM.h>


int main(int argc, char *argv[])
{
  LKM lkm;

  try {
    lkm.becomeRoot();
    lkm.unloadMod("ExampleLKM");
    lkm.unloadMod("rtai_hal");
  }
  catch (LKMerror ex) {
    ex.printErr();
    return -1;
  }
  return 0;
}

