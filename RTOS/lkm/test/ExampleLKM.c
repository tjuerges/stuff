/* Original by Jeff Kern, mangled by Scott Rankin */

#ifndef __KERNEL__
#define __KERNEL__
#endif
#ifndef MODULE
#define MODULE
#endif

#include <linux/module.h>
#include <rtai.h>


MODULE_LICENSE("GPL");

#define STACK_SIZE (2000)


static void runningProc(int t) {
    printk("ExampleLKM: runningProc called. \n");
}


int init_module(void) {
    printk("ExampleLKM: init_module called. \n");
    return 0;
}


void cleanup_module(void) {
    printk("ExampleLKM: cleanup_module called. \n\n");
}

