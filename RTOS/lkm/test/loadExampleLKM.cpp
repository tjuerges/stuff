#include <LKM.h>

// Required for sleep function.
#include <unistd.h>

int main(int argc, char *argv[])
{
  LKM lkm;

  try {
    lkm.becomeRoot();    
    lkm.loadMod("rtai_hal");
  }
  catch (LKMerror ex) {
    ex.printErr();
    return -1;
  }

  try {
    lkm.loadMod("ExampleLKM");
  }
  catch (LKMerror ex) {
    ex.printErr();
     lkm.unloadMod("rtai_hal");
    return -1;
  }

  return 0;
}
