#include <iostream>
#include <cerrno>
#include <cstring>
#include <fcntl.h>
#include <unistd.h>


void openNullDevice()
{
    const int fd(open("/null", O_RDWR, 0));
    const int error(errno);
    if(fd == -1)
    {
        std::cout << std::strerror(error)
            << "\n";
    }
    else
    {
        std::cout << "Opening of \"/null\" was successful.  fd = "
            << fd
            << ".\n";
        close(fd);
    }
}
