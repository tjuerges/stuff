/*
 * $Id$
 */
#include <linux/kernel.h>
#include <linux/module.h>

#include <rtai_sched.h>
#include <rtai_sem.h>

MODULE_AUTHOR("Rodrigo Amestica");
MODULE_DESCRIPTION("dummy test");
MODULE_LICENSE("GPL");

/*
 * global variables
 */
RT_TASK task0;
SEM sem;

#define modName"test"

void test_task(long id)
{
    int stat;

    rt_printk("test task started (%ld)\n", id);

    rt_sleep(nano2count(1000000000ULL));

    stat = rt_sem_wait(&sem);
    rt_printk("stat1=%d\n", stat);

    stat = rt_sem_wait(&sem);
    rt_printk("stat2=%d\n", stat);

    rt_printk("test task finished (%ld)\n", id);
}

/*
 * kernel module handling
 */
static int __init rtSemTest_init(void)
{
    printk("test module loading...\n");

    rt_typed_sem_init(&sem, 0, RES_SEM);
    
    if ( rt_task_init(&task0, test_task, 0, 32768, 10, 1, 0)
 != 0 )
    {
printk("CANNOT SPAWN THE TEST TASK #1.\n");
return 0;
    }
    
    rt_set_oneshot_mode();
    start_rt_timer_ns(0);

    rt_task_resume(&task0);
 
    printk("test module loaded\n");

    return 0;
}

static void __exit rtSemTest_exit(void)
{
    printk("test module cleaning up...\n");

    stop_rt_timer();

    rt_task_delete(&task0);

    rt_sem_delete(&sem);
    
    printk("test module cleaned up\n");
}

module_init(rtSemTest_init);
module_exit(rtSemTest_exit);

/*___oOo___*/
