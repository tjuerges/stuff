/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2006
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Jun 21, 2006  created
*/

/************************************************************************
*   NAME
*
*   SYNOPSIS
*
*   DESCRIPTION
*
*   FILES
*
*   ENVIRONMENT
*
*   RETURN VALUES
*
*   CAUTIONS
*
*   EXAMPLES
*
*   SEE ALSO
*
*   BUGS
*
*------------------------------------------------------------------------
*/

static char *rcsId="@(#) $Id$";
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

/*
 * System stuff
 */
#include <linux/moduleparam.h>
/* ... as you need them ... */
#include <asm/uaccess.h>
#include <asm/io.h>

/*
 * RTAI stuff
 */
#include <rtai_sched.h>
#include <rtai_registry.h>
#include <rtai_sem.h>
/* ... as you need them ... */

/*
 * ACS stuff
 */
#include <rtTools.h>
#include <rtlog.h>

/*
 * Local stuff
 */
#include "DEParallelDrv.h"

/*
 * used for logging
 */
#define modName    "rtosSignalTest"


MODULE_AUTHOR("Thomas Juerges <tjuerges@nrao.edu>");
MODULE_DESCRIPTION(modName);
MODULE_LICENSE("GPL");


/*
 * module parameters
 */
static unsigned int initTimeout = 1000;	/* msec */
static unsigned int cleanUpTimeout = 1000;	/* msec */

module_param(initTimeout, uint, S_IRUGO);
module_param(cleanUpTimeout, uint, S_IRUGO);

#define HOSTNAME_SIZE 256
static char hostname[HOSTNAME_SIZE];

#define PARPORT 0x378


void teTriggeredLogging(int foo)
{
	static rtlogRecord_t logRecord;
	unsigned int value = inb(PARPORT);

	outb(value ^ 0x01, PARPORT);
	RTLOG_INFO(modName, "%s: This is the TE triggered log message. Bit #0 should be toggled now.",
		hostname);
}

/*
 * intialization task
 */
static void initTaskFunction(long flag)
{
	int status = 0;
	atomic_t* flag_p = (atomic_t*) flag;
	rtlogRecord_t logRecord;

	RTLOG_INFO(modName, "Initialization task started...");

	/*
	 * do the real work here, if it fails do not set status and go to TheEnd.
	 */
	enableTE(teTriggeredLogging);

	status = 1;

	RTLOG_INFO(modName, "Initialization task exit.");

	atomic_set(flag_p, status);

	rt_task_delete(rt_whoami());
}

/*
 * clean up task
 */
static void cleanUpTaskFunction(long flag)
{
	int status = 0;
	atomic_t* flag_p = (atomic_t*) flag;
	rtlogRecord_t logRecord;

	RTLOG_INFO(modName, "Clean up task started...");

	/*
	 * do the real work here, if it fails do not set status and go to TheEnd.
	 */
	disableTE();

	status = 1;

	RTLOG_INFO(modName, "Clean up task exit.");

	atomic_set(flag_p, status);

	rt_task_delete(rt_whoami());
}

static int rtosSignalTest_main(int stage)
{
	int status;
	int cleanUpStatus = RT_TOOLS_MODULE_EXIT_SUCCESS;
	RT_TASK initTask, cleanUpTask;
	atomic_t initFlag = ATOMIC_INIT(0);
	atomic_t cleanUpFlag = ATOMIC_INIT(0);
	rtlogRecord_t logRecord;

	if(stage == RT_TOOLS_MODULE_STAGE_INIT)
	{
		status = RT_TOOLS_MODULE_INIT_ERROR;

		/*
		 * log RCS ID (cvs version)
		 */
		RTLOG_INFO(modName, "%s", rcsId);

		RTLOG_INFO(modName, "initializing module...");

		RTLOG_INFO(modName, "initTimeout    = %dms", initTimeout);
		RTLOG_INFO(modName, "cleanUpTimeout = %dms", cleanUpTimeout);

		goto Initialization;
	}
	else
	{
		status = RT_TOOLS_MODULE_EXIT_SUCCESS;

		RTLOG_INFO(modName, "Cleaning up module...");

		goto FullCleanUp;
	}

Initialization:
	if(rt_task_init(&initTask,
			  initTaskFunction,
			  (long)  &initFlag,
			  RT_TOOLS_INIT_TASK_STACK,
			  RT_TOOLS_INIT_TASK_PRIO,
			  0, 0)
	 != 0)
	{
		RTLOG_ERROR(modName, "Failed to spawn initialization task!");

		status = RT_TOOLS_MODULE_NO_INIT_TASK_ERROR;

		goto FullCleanUp;
	}
	else
	{
		RTLOG_INFO(modName,
			"Created initialization task, address = 0x%p.", &initTask);
	}

	/*
	 * resume the initialization task
	 */
	if(rt_task_resume(&initTask) != 0)
	{
		rt_task_delete(&initTask);
		status = RT_TOOLS_MODULE_NO_INIT_TASK_ERROR;

		RTLOG_ERROR(modName, "Cannot resume initalisation task!");

		goto FullCleanUp;
	}

	/*
	 * wait for the initialization task to be ready
	 */
	if(rtToolsWaitOnFlag(&initFlag, initTimeout) != 0)
	{
		/*
		 * okay, the init task was not ready on time, now forcibly remove
		 * it for the rtai's scheduler
		 */
		rt_task_suspend(&initTask);
		rt_task_delete(&initTask);

		RTLOG_ERROR(modName,
			  "Initialization task did not report back on time!");

		status  = RT_TOOLS_MODULE_INIT_TIMEOUT;

		goto FullCleanUp;
	}
	else
	{
		/*
		 * Waited successfully for the init task to run through.
		 */
		status = RT_TOOLS_MODULE_INIT_SUCCESS;
	}

	goto Exit;

FullCleanUp:
	/*
	 * asynchronous clean up phase to be handled by this task
	 */
	if(rt_task_init(&cleanUpTask,
			cleanUpTaskFunction,
			(long) &cleanUpFlag,
			RT_TOOLS_CLEANUP_TASK_STACK,
			RT_TOOLS_CLEANUP_TASK_PRIO,
			0, 0)
	!= 0)
	{
		RTLOG_ERROR(modName, "Failed to spawn clean up task!");

		/*
		 * Set the status to this error only, if the stage is not the init stage.
		 * Whenever it happens, that something went wrong with the init task,
		 * we are here, too. Then an error in the clean up is not of inerest,
		 * because the init failed. This should then be reported.
		 */
		cleanUpStatus = RT_TOOLS_MODULE_CLEANUP_ERROR;

		goto Exit;
	}
	else
	{
		RTLOG_INFO(modName,
			"Created clean up task, address = 0x%p.", &cleanUpTask);
	}

	 /*
	  * resume the clean up task
	  */
	if(rt_task_resume(&cleanUpTask) != 0)
	{
		rt_task_delete(&cleanUpTask);
		cleanUpStatus = RT_TOOLS_MODULE_CLEANUP_ERROR;

		RTLOG_ERROR(modName, "Cannot resume clean up task!");

		goto Exit;
	}

	 /*
	  * wait for the initialization task to be ready
	  */
	if(rtToolsWaitOnFlag(&cleanUpFlag, cleanUpTimeout) != 0)
	{
		/*
		 * okay, the init task was not ready on time, now forcibly remove
		 * it for the rtai's scheduler
		 */
		rt_task_suspend(&cleanUpTask);
		rt_task_delete(&cleanUpTask);

		RTLOG_ERROR(modName, "Clean up task did not report back on time!");

		if(stage != RT_TOOLS_MODULE_STAGE_INIT)
		{
			cleanUpStatus = RT_TOOLS_MODULE_CLEANUP_ERROR;
		}
	}
	else
	{
		if(stage != RT_TOOLS_MODULE_STAGE_INIT)
		{
			cleanUpStatus = RT_TOOLS_MODULE_EXIT_SUCCESS;
		}
	 }

Exit:
	if(stage != RT_TOOLS_MODULE_STAGE_INIT)
	{
		status = cleanUpStatus;
	}

	return status;
}

static int __init rtosSignalTest_init(void)
{
	int status = 0;
	rtlogRecord_t logRecord;

	down_read(&uts_sem);
	strcpy(hostname, system_utsname.nodename);
	up_read(&uts_sem);

	RTLOG_INFO(modName, "%s: This kernel module will send out a message"
		"and toggle bit #0 on the parallel port, every time a TE  is sensed.",
		hostname);

 	if((status = rtosSignalTest_main(RT_TOOLS_MODULE_STAGE_INIT))
	 == RT_TOOLS_MODULE_INIT_SUCCESS)
	{
		RTLOG_INFO(modName, "Module initialized successfully.");
	}
	else
	{
		RTLOG_ERROR(modName, "Failed to initialize module!");
	}

	return status;
}

static void __exit rtosSignalTest_exit(void)
{
	rtlogRecord_t logRecord;

	if(rtosSignalTest_main(RT_TOOLS_MODULE_STAGE_EXIT)
	 == RT_TOOLS_MODULE_EXIT_SUCCESS)
	{
		RTLOG_INFO(modName, "Module cleaned up successfully.");
	}
	else
	{
		RTLOG_ERROR(modName, "Failed to clean up module!");
	}
}

module_init(rtosSignalTest_init);
module_exit(rtosSignalTest_exit);

/*___oOo___*/
