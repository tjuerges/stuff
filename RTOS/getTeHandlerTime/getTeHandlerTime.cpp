/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  May 15, 2007  created
*/

#include <ctime>
#include <sys/time.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <rtTools.h>
#include <rtToolsFifoCmd.h>
#include <teHandler.h>
#include <signal.h>
#include <asm/atomic.h>

#define TE_HANDLER_UNIX_OFFSET      122192928000000000ULL    /* 100ns */

atomic_t stop = ATOMIC_INIT(1);

void leave(int)
{
    atomic_set(&stop, 0);
}

int main(int, char**)
{
    (void)signal(SIGINT,leave);

    rtToolsFifoCmd* cmdFifo(0);

    try
    {
        cmdFifo = new rtToolsFifoCmd(TE_HANDLER_CMD_FIFO_DEV);
    }
    catch(...)
    {
        std::cout << "failed to create rtToolsFifoCmd instance" << std::endl;
        return -1;
    }

    try
    {
        unsigned long long kt(0ULL), ktms(0ULL);
        time_t kts;
        struct timeval utv;
        int cmd(teHandlerCMD_GETTIME);
        struct tm kttm;
        struct tm uttm;

        do
        {
            cmdFifo->sendRecvCmd(&cmd, sizeof(cmd), (void *)&kt, sizeof(kt), 60);

            if((kt == 0) || (gettimeofday(&utv, 0) != 0))
            {
                std::cout << "ERROR" << std::endl;
            }
            else
            {
                ktms = (kt % 10000000ULL) / 10000ULL;

                //
                // unix seconds
                //
                kts = (kt - TE_HANDLER_UNIX_OFFSET) / 10000000ULL;

                //
                // broken-down time
                //
                gmtime_r(&kts, &kttm);
                gmtime_r(&(utv.tv_sec), &uttm);

                //
                // output in ISO format
                //
                std::cout << "teHandler =    "
                    << (1900 + kttm.tm_year) << "-"
                    << std::setfill('0') << std::setw(2) << (1 + kttm.tm_mon) << "-"
                    << std::setfill('0') << std::setw(2) << kttm.tm_mday << "T"
                    << std::setfill('0') << std::setw(2) << kttm.tm_hour << ":"
                    << std::setfill('0') << std::setw(2) << kttm.tm_min << ":"
                    << std::setfill('0') << std::setw(2) << kttm.tm_sec << "."
                    << std::setfill('0') << std::setw(3) << ktms
                    << std::endl
                    << "gettimeofday = "
                    << (1900 + uttm.tm_year) << "-"
                    << std::setfill('0') << std::setw(2) << (1 + uttm.tm_mon) << "-"
                    << std::setfill('0') << std::setw(2) << uttm.tm_mday << "T"
                    << std::setfill('0') << std::setw(2) << uttm.tm_hour << ":"
                    << std::setfill('0') << std::setw(2) << uttm.tm_min << ":"
                    << std::setfill('0') << std::setw(2) << uttm.tm_sec << "."
                    << std::setfill('0') << std::setw(3) << (utv.tv_usec / 1000U)
                    << std::endl
                    << "Difference =  "
                    << std::setfill('0') << std::setw(6)
                    <<
            (kttm.tm_hour * 3600.0 + kttm.tm_min * 60.0 + kttm.tm_sec + ktms / 1e3)
            -
            (uttm.tm_hour * 3600.0 + uttm.tm_min * 60.0 + uttm.tm_sec +  utv.tv_usec/ 1e6)
                    << "s."
                    << std::endl << std::endl;
            }


            sleep(1);
        }
        while(atomic_read(&stop) != 0);

        delete cmdFifo;
        std::cout << "Good bye." << std::endl;
    }
    catch(const rtToolsFifoCmdErr_t& ex)
    {
        delete cmdFifo;
        std::cout <<
            "sendRecvCmd GETTIME has failed (ex=" << ex << ")" << std::endl;
        return -1;
    }

    return 0;
}
