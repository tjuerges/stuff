# $Id$

TEMPLATE = app
TARGET = getTeHandlerTime
LANGUAGE = C++
CONFIG = console rtti stl warn_on
QMAKE_CXXFLAGS_RELEASE += -g $$system(cflags.sh QMAKE_RELEASE)
QMAKE_CXXFLAGS_DEBUG += -g $$system(cflags.sh QMAKE_DEBUG)
INCLUDEPATH += /alma/ACS-6.0/ACSSW/../rtai/include \
    ${INTROOT}/include
LIBPATH += /alma/ACS-6.0/ACSSW/../rtai/lib \
    ${INTROOT}/lib
LIBS += -lrtToolsFifoCmd
SOURCES += getTeHandlerTime.cpp

