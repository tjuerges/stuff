/**
 * $Id$
 */

#include <rtai_sem.h>

static SEM sem;

static void countSem(void)
{
	static int count = 0;

	count = rt_sem_count(&sem);
	printk("Semaphore value = %d.\n", count);
}

static int __init myInit(void)
{

	rt_sem_init(&sem, 1);
	countSem();
	rt_sem_wait(&sem);
	countSem();
	rt_sem_signal(&sem);
	countSem();	
	rt_sem_delete(&sem);

	return -1;
}

static void __exit myExit(void)
{
}

module_init(myInit);
module_exit(myExit);
