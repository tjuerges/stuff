/*******************************************************************************
*
* "@(#) $Id$"
*
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
*/


static char *rcsId="@(#) $Id$";
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

/*
 * System stuff
 */
#include <linux/moduleparam.h>
#include <linux/delay.h>
#include <asm/atomic.h>
/* ... as you need them ... */

/*
 * RTAI stuff
 */
#include <rtai_sched.h>
#include <rtai_registry.h>
#include <rtai_sem.h>
/* ... as you need them ... */

/*
 * ACS stuff
 */
#include <rtTools.h>
#include <rtlog.h>


/*
 * used for logging
 */
#define modName    "kernelModuleTemplate"

/*
 * Local stuff
 */
/*#include "kernelModuleTemplatePrivate.h"*/

MODULE_AUTHOR("Thomas Juerges <tjuerges@nrao.edu>");
MODULE_DESCRIPTION(modName ": This is the module description.");
MODULE_LICENSE("GPL");

/*
 * module parameters
 */
static unsigned int initTimeout = 1000;         /* msec */
static unsigned int cleanUpTimeout = 1000;      /* msec */
module_param(initTimeout, uint, S_IRUGO);
module_param(cleanUpTimeout, uint, S_IRUGO);

/*
 * my local data
 */
static SEM access_sem; /* API must be reentrant, protect with this semaphore */

/*
 * API
 */
static void kernelModuleTemplateFun1(void)
{
}
static void kernelModuleTemplateFunN(void)
{
}

/*
 * exported API
 */
EXPORT_SYMBOL (kernelModuleTemplateFun1);
/* ... */
EXPORT_SYMBOL (kernelModuleTemplateFunN);

/*
 * intialization task
 */
void initTaskFunction(long flag)
{
	int status = 0;
	atomic_t *flag_p = (atomic_t *)flag;
	rtlogRecord_t logRecord;

	RTLOG(modName, RTLOG_NORMAL, "Initialization task started...");

	/*
	 * do the real work here, if it fails do not set status and go to TheEnd.
	 */

	rt_sleep(nano2count(100000000LL));

	status = 1;

TheEnd:
	RTLOG(modName, RTLOG_NORMAL, "Initialization task exit.");

	atomic_set(flag_p, status);

	rt_task_delete(rt_whoami());
}

/*
 * clean up task
 */
static void cleanUpTaskFunction(long flag)
{
	int status = 0;
	atomic_t *flag_p = (atomic_t *)flag;
	rtlogRecord_t logRecord;

	RTLOG(modName, RTLOG_NORMAL, "Clean up task started...");

	/*
	 * do the real work here, if it fails do not set status and go to TheEnd.
	 */
	rt_sleep(nano2count(100000000LL));

	status = 1;

TheEnd:
	RTLOG(modName, RTLOG_NORMAL, "Clean up task exit.");

	atomic_set(flag_p, status);

	rt_task_delete(rt_whoami());
}

static int firstInitSection(void)
{
	return RT_TOOLS_MODULE_INIT_SUCCESS;
}
static int nthInitSection(void)
{
	return RT_TOOLS_MODULE_INIT_SUCCESS;
}

static int firstCleanUpSection(void)
{
	return RT_TOOLS_MODULE_EXIT_SUCCESS;
}
static int nthCleanUpSection(void)
{
	return RT_TOOLS_MODULE_EXIT_SUCCESS;
}

static int kernelModuleTemplate_main(int stage)
{
	int status;
	int temporaryStatus = RT_TOOLS_MODULE_CLEANUP_ERROR;
	int cleanUpStatus = RT_TOOLS_MODULE_EXIT_SUCCESS;
	RT_TASK initTask, cleanUpTask;
	atomic_t initFlag = ATOMIC_INIT(0);
	atomic_t cleanUpFlag = ATOMIC_INIT(0);
	rtlogRecord_t logRecord;

	if ( stage == RT_TOOLS_MODULE_STAGE_INIT )
	{
		status = RT_TOOLS_MODULE_INIT_ERROR;

		/*
		 * log RCS ID (cvs version)
		 */
		RTLOG(modName, RTLOG_NORMAL, "%s", rcsId);

		RTLOG(modName, RTLOG_NORMAL, "initializing module...");

		RTLOG(modName, RTLOG_NORMAL, "initTimeout    = %dms", initTimeout);
		RTLOG(modName, RTLOG_NORMAL, "cleanUpTimeout = %dms", cleanUpTimeout);

		goto Initialization;
	}
	else
	{
		status = RT_TOOLS_MODULE_EXIT_SUCCESS;

		RTLOG(modName, RTLOG_NORMAL, "Cleaning up module...");

		goto FullCleanUp;
	}

Initialization:
	/**
	 * ATTENTION!!!!!
	 * Whenever firstInitSection() fails, take care of jumping to the correct
	 * clean-up level. It must not necessarily be that LevelNCleanUp is the
	 * clean-up level to jump to after nthInitSection() failed, but (here in
	 * this template) Level1CleanUp.
	 *
	 * The reason for it could be:
	 * - The first init level screws up things totally. Then a jump to the first
	 *   clean-up level would let the module try to clean up the mess, which is
	 *   not possible anymore. A stuck computer could be the result.
	 *
	 * The right way to do it:
	 * The Nth init level fails at a certain point. Then
	 * - The Nth init level cleans up everything which was successfully
	 *   initialised in this level.
	 * - The Nth init level returns to this place with
	 *   RT_TOOLS_MODULE_INIT_ERROR.
	 * - Here, the code jumps to the higher (one level higher, counting up from
	 *   level N to level 1 as the highest) clean-up level. This would be in
	 *   this template 
	 */
	if ( (status = firstInitSection()) != RT_TOOLS_MODULE_INIT_SUCCESS )
	{
		goto Exit;
	}

	/* ... more initialization steps ... */

	if ( (status = nthInitSection()) != RT_TOOLS_MODULE_INIT_SUCCESS )
	{
		goto Level1CleanUp;
	}

	/*
	 * initialization of resources is finished. Now spawn the rtai
	 * task that takes care of doing the asynchronous part of the
	 * initialization.
	 */
	if ( rt_task_init(&initTask,
			  initTaskFunction,
			  (long)&initFlag,
			  RT_TOOLS_INIT_TASK_STACK,
			  RT_TOOLS_INIT_TASK_PRIO,
			  0, 0)
	 != 0 )
	{
		RTLOG(modName, RTLOG_ERROR, "Failed to spawn initialization task!");

		status = RT_TOOLS_MODULE_NO_INIT_TASK_ERROR;

		goto LevelNCleanUp;
	}
	else
	{
		RTLOG(modName, RTLOG_NORMAL,
			"Created initialization task, address = 0x%p.", &initTask);
	}

	/*
	 * resume the initialization task
	 */
	if(rt_task_resume(&initTask) != 0)
	{
		rt_task_delete(&initTask);
		status = RT_TOOLS_MODULE_NO_INIT_TASK_ERROR;

		RTLOG(modName, RTLOG_ERROR, "Cannot resume initalisation task!");

		goto LevelNCleanUp;
	}

	/*
	 * wait for the initialization task to be ready
	 */
	if ( rtToolsWaitOnFlag(&initFlag, initTimeout) != 0 )
	{
		/*
		 * okay, the init task was not ready on time, now forcibly remove
		 * it for the rtai's scheduler
		 */
		rt_task_suspend(&initTask);
		rt_task_delete(&initTask);

		RTLOG(modName, RTLOG_ERROR,
			  "Initialization task did not report back on time!");

		status  = RT_TOOLS_MODULE_INIT_TIMEOUT;

		goto FullCleanUp;
	}
	else
	{
		/*
		 * Waited successfully for the init task to run through.
		 */
		status = RT_TOOLS_MODULE_INIT_SUCCESS;
	}

	goto Exit;

FullCleanUp:
	/*
	 * asynchronous clean up phase to be handled by this task
	 */
	if ( rt_task_init(&cleanUpTask,
			cleanUpTaskFunction,
			(long)&cleanUpFlag,
			RT_TOOLS_CLEANUP_TASK_STACK,
			RT_TOOLS_CLEANUP_TASK_PRIO,
			0, 0)
	!= 0)
	{
		RTLOG(modName, RTLOG_ERROR, "Failed to spawn clean up task!");

		/*
		 * Set the status to this error only, if the stage is not the init stage.
		 * Whenever it happens, that something went wrong with the init task,
		 * we are here, too. Then an error in the clean up is not of inerest,
		 * because the init failed. This should then be reported.
		 */
		cleanUpStatus = RT_TOOLS_MODULE_CLEANUP_ERROR;

		goto LevelNCleanUp;
	}
	else
	{
		RTLOG(modName, RTLOG_NORMAL,
			"Created clean up task, address = 0x%p.", &cleanUpTask);
	}

	 /*
	  * resume the clean up task
	  */
	if(rt_task_resume(&cleanUpTask) != 0)
	{
		rt_task_delete(&cleanUpTask);
		cleanUpStatus = RT_TOOLS_MODULE_CLEANUP_ERROR;

		RTLOG(modName, RTLOG_ERROR, "Cannot resume clean up task!");

		goto LevelNCleanUp;
	}

	 /*
	  * wait for the initialization task to be ready
	  */
	if ( rtToolsWaitOnFlag(&cleanUpFlag, cleanUpTimeout) != 0 )
	{
		/*
		 * okay, the init task was not ready on time, now forcibly remove
		 * it for the rtai's scheduler
		 */
		rt_task_suspend(&cleanUpTask);
		rt_task_delete(&cleanUpTask);

		RTLOG(modName, RTLOG_ERROR, "Clean up task did not report back on time!");

		if(stage != RT_TOOLS_MODULE_STAGE_INIT)
		{
			cleanUpStatus = RT_TOOLS_MODULE_CLEANUP_ERROR;
		}
	}
	else
	{
		if(stage != RT_TOOLS_MODULE_STAGE_INIT)
		{
			cleanUpStatus = RT_TOOLS_MODULE_EXIT_SUCCESS;
		}
	 }

LevelNCleanUp:
	/*
	 * Do synchronous clean up here. Set cleanUpStatus only if
	 * (stage == RT_TOOLS_MODULE_STAGE_EXIT).
	 *
	 * For example:
	 */
	temporaryStatus = nthCleanUpSection();
	if( temporaryStatus != RT_TOOLS_MODULE_EXIT_SUCCESS)
	{
		RTLOG(modName, RTLOG_ERROR, "Failed to run the nth clean up function! "
			"Continuing anyway with the clean up.");

		if(cleanUpStatus == RT_TOOLS_MODULE_EXIT_SUCCESS)
		{
			cleanUpStatus = temporaryStatus;
		}
	}

Level1CleanUp:
	temporaryStatus = firstCleanUpSection();
	if( temporaryStatus != RT_TOOLS_MODULE_EXIT_SUCCESS)
	{
		RTLOG(modName, RTLOG_ERROR, "Failed to run the 1st clean up function! "
			"Continuing anyway with the clean up.");

		if(cleanUpStatus == RT_TOOLS_MODULE_EXIT_SUCCESS)
		{
			cleanUpStatus = temporaryStatus;
		}
	}

Exit:
	if(stage != RT_TOOLS_MODULE_STAGE_INIT)
	{
		status = cleanUpStatus;
	}

	return status;
}

static int __init kernelModuleTemplate_init(void)
{
	int status;
	rtlogRecord_t logRecord;

	if ( (status = kernelModuleTemplate_main(RT_TOOLS_MODULE_STAGE_INIT))
	 == RT_TOOLS_MODULE_INIT_SUCCESS )
	{
		RTLOG(modName, RTLOG_NORMAL, "Module initialized successfully.");
	}
	else
	{
		RTLOG(modName, RTLOG_ERROR, "Failed to initialize module!");
	}

	return status;
}

static void __exit kernelModuleTemplate_exit(void)
{
	rtlogRecord_t logRecord;

	if ( kernelModuleTemplate_main(RT_TOOLS_MODULE_STAGE_EXIT)
	 == RT_TOOLS_MODULE_EXIT_SUCCESS )
	{
		RTLOG(modName, RTLOG_NORMAL, "Module cleaned up successfully.");
	}
	else
	{
		RTLOG(modName, RTLOG_ERROR, "Failed to clean up module!");
	}
}

module_init(kernelModuleTemplate_init);
module_exit(kernelModuleTemplate_exit);
