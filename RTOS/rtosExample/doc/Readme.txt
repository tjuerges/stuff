$Id$

ACS Workshop 2007
===============

I have collected here some notes which might be of interest for the RTOS kernel
module developer.


- Linux Device Drivers, 3rd. ed., Jonathan Corbet, Alessandro Rubini, and Greg
Kroah-Hartman.
Available online: http://lwn.net/images/pdf/LDD3/ldd3_pdf.tar.bz2


- Old RTOS web page:
http://www.aero.polimi.it/~rtai/


- New (official) RTOS web page:
http://www.rtai.org/


- RTOS API (old but much more complete than the current edition): A Hard Real
Time support for LINUX.
Available online: http://www.aero.polimi.it/~rtai/documentation/reference/rtai_man.pdf


- RTOS documents (collection, history, RTLinux, etc.):
https://www.rtai.org/index.php?module=documents&JAS_DocumentManager_op=list


- RTOS API for RTOS 3.3, Kernel documentation (2.6.15.2), ACE/TAO API (ACS 6.0.1):
http://www.aoc.nrao.edu/~tjuerges/



Additional comments about the example
=====================================

- The RTOS scheduler can run in either "one shot" or "periodic" mode. The one
shot mode is in most cases preferable. Though one might think that the periodic
mode might serve periodic tasks better, the one shot mode gives the developer
more control. In periodic mode all created tasks (even in different kernel
modules) run with the same frequency. In one shot mode the developer takes care
of the cyclic execution of the tasks himself.

Example: see src/rtosExample.c.
	Here the one shot mode is used. The created task is executed forever until
	(on module removal) an atomic flag is set so that the task leaves the
	execution loop. Please see the source code for additional comments.

- The built module cannot be loaded into the kernel by an ordinary - mortal
;-)- user. Therefore, the NO-LGPL version of ACS comes with two binaries which
are / have to be installed with root as group owner and the sticky bit set for
the group. The two executables are
	+ loadLkmModule
	+ unloadLkmModule
Both take a file name as paratmeter. The file contains a list of kernel modules
to be loaded. Additional module parameter can be added. The lkm file must either
specify kernel modules in $ACSROOT/rtai or $INTROOT/rtai.
Whenever a kernel module shall be tested, it is probably a good idea to use the
RTAI_MODULES_L target and do not install it. Caution: in order for the
loadLkmModule to find the not-installed kernel module, export MODPATH=1!
Here is an example session:
[tjuerges@delphinus ~]$ cd workspace/rtosExample/src
[tjuerges@delphinus src]$ export MODPATH=1
[tjuerges@delphinus src]$ loadLkmModule ../config/rtosExample.lkm 
[tjuerges@delphinus src]$ loadLkmModule ../config/rtosExample.lkm 
LKM configuration file: ../config/rtosExample.lkm
Using /alma/ACS-6.0/rtai/modules/rtai_hal.ko
Using /alma/ACS-6.0/rtai/modules/rtai_up.ko
Using /alma/ACS-6.0/rtai/modules/rtai_fifos.ko
Using /alma/ACS-6.0/rtai/modules/rtai_sem.ko
Using /alma/ACS-6.0/rtai/modules/rtai_mbx.ko
Using ../rtai/rtosExample.ko
Loaded: 6 of 6 kernel modules
[tjuerges@delphinus src]$ unloadLkmModule ../config/rtosExample.lkm 
LKM configuration file: ../config/rtosExample.lkm
Unloaded: 6 of 6 kernel modules
[tjuerges@delphinus src]$

If you want to be able to load the kernel module without the loadLkmModule
executable, su to root. Then insmod the kernel module.


- What does the example do?
It prints every sleepTime numberOfLogs (both module parameters, see
rtosExample.lkm) logging messages with counting number, a time stamp and the
host name.

If you have any trouble with the example or the template, please do not hesitate
to send me an email: tjuerges@nrao.edu

Have fun!
