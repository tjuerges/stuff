#ifndef RTOS_DEFINES_H_
#define RTOS_DEFINES_H_
/*
 * $Id$
 */

#ifdef __KERNEL__
/*
 * Since no ALMA-stuff is available, redefine the logging statements.
 */
#define RTLOG_ERROR(moduleName, format, args...)\
    {\
        rt_printk(KERN_ERR "%s%s(%d) - " format "\n", \
            moduleName, __FILE__, __LINE__, ##args);\
    }
#define RTLOG_WARNING(moduleName, format, args...)\
    {\
        rt_printk(KERN_WARNING "%s%s(%d) - " format "\n", \
            moduleName, __FILE__, __LINE__, ##args);\
    }
#define RTLOG_INFO(moduleName, format, args...)\
    {\
        rt_printk(KERN_INFO "%s%s(%d) - " format "\n", \
            moduleName, __FILE__, __LINE__, ##args);\
    }
#define RTLOG_DEBUG(moduleName, format, args...)\
    {\
        rt_printk(KERN_DEBUG "%s%s(%d) - " format "\n", \
            moduleName, __FILE__, __LINE__, ##args);\
    }

/*
 * module init/exit task attributes
 */
#define RT_TOOLS_INIT_TASK_STACK        32768
#define RT_TOOLS_INIT_TASK_PRIO         10
#define RT_TOOLS_CLEANUP_TASK_STACK     32768
#define RT_TOOLS_CLEANUP_TASK_PRIO      10

/*
 * RTOS kernel module stages. This value decides, what the main
 * function does.
 */
#define RT_TOOLS_MODULE_STAGE_INIT 1
#define RT_TOOLS_MODULE_STAGE_EXIT 2
#define RT_TOOLS_MODULE_STAGE_CLEANUP 3

/*
 * RTOS kernel module error codes.
 */
#define RT_TOOLS_MODULE_INIT_SUCCESS 0
#define RT_TOOLS_MODULE_EXIT_SUCCESS 0
/*
 * Define an error base which does not conflict with the Linux and
 * kernel errnos.
 */
#define RT_TOOLS_MODULE_ERROR_BASE 256
/*
 * Standard errors.
 */
#define RT_TOOLS_MODULE_INIT_ERROR -(RT_TOOLS_MODULE_ERROR_BASE + 1)
#define RT_TOOLS_MODULE_INIT_TIMEOUT -(RT_TOOLS_MODULE_ERROR_BASE + 2)
#define RT_TOOLS_MODULE_CLEANUP_ERROR -(RT_TOOLS_MODULE_ERROR_BASE + 3)
#define RT_TOOLS_MODULE_NO_INIT_TASK_ERROR -(RT_TOOLS_MODULE_ERROR_BASE + 4)
#define RT_TOOLS_MODULE_INIT_EXIT_ERROR -(RT_TOOLS_MODULE_ERROR_BASE + 5)
#define RT_TOOLS_MODULE_PARAM_ERROR -(RT_TOOLS_MODULE_ERROR_BASE + 6)

#endif

#endif /*RTOS_DEFINES_H_*/
