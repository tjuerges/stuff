/**
 * $Id$
 */
#include <qstring.h>
#include <iostream>

int main(int,char**)
{
	QString a("Rotation=1.23456");
	std::cout<<a.section('=',0,0).latin1()<<std::endl;
	std::cout<<a.section('=',1,1).latin1()<<std::endl;
	std::cout<<a.section('=',-1,-1).latin1()<<std::endl;
	return 0;
}
