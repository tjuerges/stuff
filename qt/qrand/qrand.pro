# $Id$

TEMPLATE = app
TARGET = qrand

LANGUAGE = C++

CONFIG += qt rtti stl warn_on release

QMAKE_CXXFLAGS_RELEASE += -g $$system(cflags.sh QMAKE_RELEASE) \
	-DCOMPILATION_DATE=\\\"`date --iso-8601=seconds`\\\" \
    -DVERSION=\\\"`./compilation_run`\\\"

SOURCES += qrand.cpp

