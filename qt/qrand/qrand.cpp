/**
 * $Id$
 */

#include <iostream>
#include <QtCore>

int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    std::cout << "Compiled on "
        << COMPILATION_DATE
        << ", version "
        << VERSION
        << "."
        << std::endl;

	std::cout << qrand() << std::endl;

    return 0;
};
