#ifndef broadcast_h
#define broadcast_h


#include <qwidget.h>
#include <string>

class QProcess;
class QString;

class Broadcast: public QWidget
{
    Q_OBJECT

    public:
    Broadcast(const std::string& host);
    virtual ~Broadcast(void);

    signals:
    void readyReadStdout(void);
    void readyReadStderr(void);

    public slots:
    virtual void readStdout(void);
    virtual void readStderr(void);

    private:
    Broadcast();
    QProcess* p;
};
#endif
