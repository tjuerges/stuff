/*
 * broadcast.cpp
 */
#include "broadcast.h"
#include <iostream>
#include <qapplication.h>
#include <qprocess.h>
#include <QStringList>

Broadcast::Broadcast(const std::string& host):
    p(0)
{
    QStringList s;
    s << "ping" << "-v" << "-b" << host;

    std::cout << "Creating new process: "
        << s.join(" ")
        << std::endl;

    p = new QProcess(s);
    if(p != 0)
    {
        connect(p, SIGNAL(readyReadStdout(void)), this, SLOT(readStdout(void)));
        connect(p, SIGNAL(readyReadStderr(void)), this, SLOT(readStderr(void)));

        p->start();
    }
}

Broadcast::~Broadcast(void)
{
    if(p != 0)
    {
        p->tryTerminate();
    }
}

void Broadcast::readStderr(void)
{
    QByteArray input(p->readStderr());
    if(input.size() > 0)
    {
        std::cout << "From process coming in via stderr:"
            << std::endl
            << input
            << std::endl;
    }
}

void Broadcast::readStdout(void)
{
    QByteArray input(p->readStdout());
    if(input.size() > 0)
    {
        std::cout << "From process coming in via stdout:"
            << std::endl
            << input
            << std::endl;
    }
}

int main(int argc, char* argv[])
{
    QApplication a(argc,argv);

    std::string host("localhost");
    if(argc == 2)
    {
        host = argv[1];
    }

    Broadcast myBroadcast(host);

    a.exec();
    return 0;
}
