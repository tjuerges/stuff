/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

void qp::init()
{
	ds9=0;
}

void qp::destroy()
{
	if(ds9)
	{
		ds9->tryTerminate();
		delete ds9;
		ds9=0;
	}
}

void qp::newSlot()
{
	if(ds9)
	{
		ds9->tryTerminate();
		delete ds9;
	}
	QStringList s;
	s<<lineEdit->text();
	s<<lineEdit2->text();
	ds9=new QProcess(s,this,QString("hptCCDGUI_imagedisplay_process"));
	connect(ds9,SIGNAL(readyReadStdout()),this,SLOT(readline()));
	connect(ds9,SIGNAL(readyReadStderr()),this,SLOT(readline()));
	if(!ds9->start())
	{
		std::cout<<"Cannot start DS9 process!"<<std::endl;
	}
}

void qp::readline()
{
	if(ds9->canReadLineStderr())
	{
		std::cout<<"DS9 stderr output="<<ds9->readLineStderr().latin1()<<std::endl;
	}
	else if(ds9->canReadLineStdout())
	{
		std::cout<<"DS9 stdout output="<<ds9->readLineStdout().latin1()<<std::endl;
	}
}
