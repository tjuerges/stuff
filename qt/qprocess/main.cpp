#include <qapplication.h>
#include "qp.h"

int main( int argc, char ** argv )
{
    QApplication a( argc, argv );
    qp w;
    w.show();
    a.connect( &a, SIGNAL( lastWindowClosed() ), &a, SLOT( quit() ) );
    return a.exec();
}
