#
# $Id$
#

TEMPLATE = app
TARGET = serialisation

LANGUAGE = C++

CONFIG += qt rtti stl exceptions debug warn_on
QT -= gui

LIBS = -Wl,--as-needed $${LIBS}

SOURCES += serialisation.cpp
