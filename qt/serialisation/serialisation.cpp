/**
 * $Id$
 */


#include <iostream>
#include <QDataStream>
#include <QFile>


typedef struct sth
{
    unsigned short a;
    unsigned short b;
    unsigned short c;
    unsigned short d;
    unsigned short e;
    unsigned short f;
    unsigned short i;
    unsigned int g;
    unsigned int h;

    sth():
        a(0),
        b(1),
        c(2),
        d(4),
        e(5),
        f(6),
        i(7),
        g(8),
        h(9)
    {
    }
};


class foo
{
    public:
    foo():
        joe(1),
        joey(2 * joe)
    {
        std::cout << "Yo!\n";
    };

    ~foo()
    {
        std::cout << "Bye!\n";
    };

    int calc(int a, double b)
    {
        return a * b;
    };


    private:
    unsigned short joe;
    const int joey;
    
};


template< typename T >
void writeStuff(const std::string& name, T& stuff)
{
    QFile file(name.c_str());
    if(file.open(QIODevice::WriteOnly))
    {
        QDataStream stream(&file);

        stream.setByteOrder(QDataStream::BigEndian);
        stream.setFloatingPointPrecision(QDataStream::DoublePrecision);

        stream.writeRawData(reinterpret_cast< char* >(&stuff), sizeof(stuff));

        file.close();
    }
    else
    {
        std::cout << "Error writing out \"" << name << "\"!\n";
    }
}


int main(int _a __attribute__((unused)), char* _b[] __attribute__((unused)))
{
    sth s;
    std::cout << "Size = " << sizeof(s) << "\n";
    writeStuff("test.struct", s);

    foo c;
    std::cout << "Size = " << sizeof(c) << "\n";
    std::cout << "foo.calc = " << c.calc(17, 3.14) << "\n";
    writeStuff("test.class", c);

    return 0;
}
