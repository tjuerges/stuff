# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/thomas/Privat/soft/QNeuralNet/main.cpp" "/home/thomas/Privat/soft/QNeuralNet/CMakeFiles/qneuralnet.dir/main.cpp.o"
  "/home/thomas/Privat/soft/QNeuralNet/mainwindow.cpp" "/home/thomas/Privat/soft/QNeuralNet/CMakeFiles/qneuralnet.dir/mainwindow.cpp.o"
  "/home/thomas/Privat/soft/QNeuralNet/moc_mainwindow.cxx" "/home/thomas/Privat/soft/QNeuralNet/CMakeFiles/qneuralnet.dir/moc_mainwindow.cxx.o"
  "/home/thomas/Privat/soft/QNeuralNet/moc_network.cxx" "/home/thomas/Privat/soft/QNeuralNet/CMakeFiles/qneuralnet.dir/moc_network.cxx.o"
  "/home/thomas/Privat/soft/QNeuralNet/moc_race-client.cxx" "/home/thomas/Privat/soft/QNeuralNet/CMakeFiles/qneuralnet.dir/moc_race-client.cxx.o"
  "/home/thomas/Privat/soft/QNeuralNet/network.cpp" "/home/thomas/Privat/soft/QNeuralNet/CMakeFiles/qneuralnet.dir/network.cpp.o"
  "/home/thomas/Privat/soft/QNeuralNet/neuron.cpp" "/home/thomas/Privat/soft/QNeuralNet/CMakeFiles/qneuralnet.dir/neuron.cpp.o"
  "/home/thomas/Privat/soft/QNeuralNet/race-client.cpp" "/home/thomas/Privat/soft/QNeuralNet/CMakeFiles/qneuralnet.dir/race-client.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "QT_GUI_LIB"
  "QT_CORE_LIB"
  "QT_NO_DEBUG"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/QtGui"
  "/usr/include/QtCore"
  "."
  "/usr/include/QtNetwork"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
