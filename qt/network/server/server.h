#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef server_h
#define server_h
//
// * $Id$
//


#include <QtNetwork>


class Server: public QTcpServer
{
    Q_OBJECT

    public:
    Server(const bool, QObject* parent = 0);
    virtual ~Server();


    private slots:
    void incomingConnection();
    void socket_connectionTerminated();
    void socket_hostFound();
    void socket_connected();
    void socket_deleteSocket();
    void socket_error(QAbstractSocket::SocketError);
    void socket_incomingData();

    private:
    const bool sendResponse;
    QTcpSocket* tcpSocket;
};
#endif
