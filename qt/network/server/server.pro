# $Id$

TEMPLATE = app
TARGET = server

LANGUAGE = C++

QT += network
QT -= gui

CONFIG += thread rtti stl warn_on debug

HEADERS += server.h

SOURCES += server.cpp
