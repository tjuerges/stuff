//
// $Id$
//


#include "server.h"
#include <iostream>
#include <sstream>
#include <QtNetwork>
#include <QCoreApplication>
#include <QList>



Server::Server(const bool _sendResponse, QObject* parent):
    QTcpServer(parent),
    sendResponse(_sendResponse),
    tcpSocket(0)
{
    connect(this, SIGNAL(newConnection()), SLOT(incomingConnection()));
}

Server::~Server()
{
    delete tcpSocket;
    tcpSocket = 0;
}

void Server::incomingConnection()
{
    std::cout << "New connection!\n";

    tcpSocket = nextPendingConnection();

    connect(tcpSocket, SIGNAL(readyRead()), SLOT(socket_incomingData()));
    connect(tcpSocket, SIGNAL(hostFound()), SLOT(socket_hostFound()));
    connect(tcpSocket, SIGNAL(connectionClosed()),
        SLOT(socket_connectionTerminated()));
    connect(tcpSocket, SIGNAL(connected()), SLOT(socket_connected()));
    connect(tcpSocket, SIGNAL(delayedCloseFinished()),
        SLOT(socket_deleteSocket()));
    connect(tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)),
        SLOT(socket_error(QAbstractSocket::SocketError)));
}

void Server::socket_connectionTerminated()
{
    std::cout << "Connection terminated.\n";
}

void Server::socket_hostFound()
{
    std::cout << "Host found.\n";
}

void Server::socket_connected()
{
    std::cout << "Socket connected.\n";
}

void Server::socket_deleteSocket()
{
    std::cout << "Delete socket.\n";
}

void Server::socket_error(QAbstractSocket::SocketError socketError)
 {
     if(socketError == QTcpSocket::RemoteHostClosedError)
     {
         return;
     }

     std::cout << "The following error occurred: "
         << tcpSocket->errorString().toStdString();

     tcpSocket->close();
}

void Server::socket_incomingData()
{
    std::cout << "Incoming data."
        << "\n"
        << "Available bytes = "
        << tcpSocket->bytesAvailable()
        << "\n";

    QString message;
    while(tcpSocket->bytesAvailable() != 0)
    {
        message += tcpSocket->readAll();
    }

    std::cout << "Received message = "
        << message.toStdString() << "\n";

    if(sendResponse == true)
    {
        tcpSocket->write(message.toLatin1(), message.length());
        tcpSocket->flush();
        std::cout << "Sent "
            << message.length()
            << " bytes back."
            << "\n\n";
    }
}


int main(int argc, char* argv[])
{
    QCoreApplication a(argc, argv);
    QString hostname("localhost");
    quint16 port(7777);
    bool sendResponse(true);

    switch(argc)
    {
        case 4:
        {
            std::istringstream input(argv[3]);
            input >> std::boolalpha >> sendResponse;
            std::cout << "Sending data back to client: "
                << (sendResponse == true ? "yes" : "no")
                << "\n";
        }

        case 3:
        {
            bool ok;

            port = QString(argv[2]).toInt(&ok);

            if(ok == false)
            {
                port = QString(argv[1]).toInt(&ok);
                if(ok == false)
                {
                    std::cout << "Parameters are wrong.\n";
                    return -1;
                }
            }

            std::cout << "Port: "
                << port
                << "\n";
        }

        case 2:
        {
            hostname = argv[1];
            std::cout << "Hostname: "
                << hostname.toStdString()
                << "\n";
        }

        case 1:
        default:
            break;
    }

    QHostInfo host(QHostInfo::fromName(hostname));
    QList< QHostAddress > hostList(host.addresses());

    if(hostList.empty() == true)
    {
        std::cout << "Could not resolve hostname.\n";
        return -1;
    }

    Server s(sendResponse);
    if(s.listen(hostList[0], port) == false)
    {
        std::cout << "Unable to start the server: "
            << s.errorString().toStdString()
            << "\n";
        return -1;
    }

    return a.exec();
};
