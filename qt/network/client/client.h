#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef Client_h
#define Client_h
//
// $Id$
//


#include <QThread>
#include <QtNetwork>


class Client: public QThread
{
    Q_OBJECT

    public:
    Client(const QHostAddress&, const quint16);
    virtual ~Client();
    virtual void run();


    private slots:
    void socket_host_found();
    void socket_connected();
    void socket_connection_terminated();
    void socket_error(QAbstractSocket::SocketError);
    void socket_incoming_data();
    void socket_delete_socket();

    private:
    Client();
    void connectToHost();

    const QHostAddress host;
    const quint16 port;
    QTcpSocket* tcpSocket;
};
#endif
