//
// $Id$
//


#include "client.h"
#include <iostream>
#include <QtNetwork>
#include <QCoreApplication>
#include <QList>


void Client::run()
{
    const double max(1.0);
    const QString starter("Challenge: ");
    double rndNumber(0.0);
    QString message, rndNumberStr;

    connectToHost();

    do
    {
        //if(tcpSocket->state() == QAbstractSocket::ConnectedState)
        {
            message = starter;

            rndNumber = 1.0 + (max * ::rand() / (RAND_MAX + 1.0));
            rndNumberStr.sprintf("%g", rndNumber);
            message += rndNumberStr;

            tcpSocket->write(message.toLatin1(), message.length());
            tcpSocket->waitForBytesWritten();
        }

        this->msleep(1000);
    }
    while(true);
}

Client::Client(const QHostAddress& _host, const quint16 _port):
    host(_host),
    port(_port),
    tcpSocket(0)
{
}

Client::~Client()
{
    delete tcpSocket;
    tcpSocket = 0;
}

void Client::connectToHost()
{
    tcpSocket = new QTcpSocket;

    connect(tcpSocket ,SIGNAL(hostFound()), SLOT(socket_host_found()));
    connect(tcpSocket ,SIGNAL(connected()), SLOT(socket_connected()));
    connect(tcpSocket ,SIGNAL(readyRead()), SLOT(socket_incoming_data()));
    connect(tcpSocket ,SIGNAL(connectionClosed()),
        SLOT(socket_connection_terminated()));
    connect(tcpSocket, SIGNAL(delayedCloseFinished()),
        SLOT(socket_delete_socket()));
    connect(tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)),
        SLOT(socket_error(QAbstractSocket::SocketError)));

    tcpSocket->connectToHost(host, port);
}

void Client::socket_host_found()
{
    std::cout << "Socket: host found.\n";
}

void  Client::socket_connected()
{
    std::cout << "Socket: connection established.\n";
}

void  Client::socket_connection_terminated()
{
    std::cout << "Socket: connection terminated.\n";
}

void Client::socket_error(QAbstractSocket::SocketError socketError)
 {
     if (socketError == QTcpSocket::RemoteHostClosedError)
     {
         return;
     }

     std::cout << "Socket: the following error occurred: "
         << tcpSocket->errorString().toStdString();

     tcpSocket->close();
}

void  Client::socket_incoming_data()
{
    std::cout << "Socket: incoming data.\n";

    QString line;

    while(tcpSocket->bytesAvailable() != 0);
    {
        line = tcpSocket->readAll();
        std::cout << "Message received = "
            << line.toStdString()
            << "\n";
    }

}

void  Client::socket_delete_socket()
{
    std::cout << "Socket: socket deleted.\n";
}


int main(int argc, char* argv[])
{
    QCoreApplication a(argc, argv);
    QString hostname("192.168.222.10");
    quint16 port(8000);

    if(argc == 3)
    {
        bool ok;

        hostname = argv[1];
        port = QString(argv[2]).toInt(&ok);

        if(ok == false)
        {
            port = QString(argv[1]).toInt(&ok);
            hostname = argv[2];
            if(ok == false)
            {
                return -1;
            }
        }
    }
    else if(argc == 2)
    {
        hostname = argv[1];
    }

    QHostInfo host(QHostInfo::fromName(hostname));
    QList< QHostAddress > hostList(host.addresses());
    if(hostList.empty() == true)
    {
        std::cout << "Could not resolve hostname.\n";
        return -1;
    }

    std::cout << host.hostName().toStdString()
        << ":"
        << port
        << " [host (string)] [port (unsigned int)]"
        << "\n";

    Client c(hostList[0], port);
    c.start();

    return a.exec();
};
