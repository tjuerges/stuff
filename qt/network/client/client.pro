#
# $Id$
#


TEMPLATE = app
TARGET = client

LANGUAGE = C++

QT += network
QT -= gui

CONFIG += thread rtti stl warn_on release

HEADERS += client.h

SOURCES += client.cpp

