all: qtmake.mk
	$(MAKE) -f qtmake.mk

qtmake.mk: qt.pro
	qmake -r -o qtmake.mk qt.pro

clean: qtmake.mk
	$(MAKE) -f qtmake.mk clean

install: qtmake.mk
	$(MAKE) -f qtmake.mk install
