/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include <qapplication.h>
#include <qpoint.h>
#include <qsize.h>
#include <iostream>

QPoint myPosition;
QSize mySize;

void Form1::init()
{
 myPosition = pos();
 mySize = size();
 std::cout << "init" << std::endl;
}

void Form1::destroy()
{
 std::cout << "destroy" << std::endl;
}

void Form1::showMinimized(void)
{
 std::cout << "Jau" << std::endl;
 //  this->setPos(myPosition);
 //  this->setSize(mySize);
}

void Form1::getInput(const QString &text)
{
 bool ok(false);
 double value(text.toDouble(&ok));
 if(ok == true)
 {
  lCDNumber1->display(value);
 }
}

void Form1::getInput()
{
 bool ok(false);
 double value(lineEdit1->text().toDouble(&ok));
 if(ok == true)
 {
  lCDNumber1->display(value);
 }
}
