/**
 * $Id$
 */
#include <qapplication.h>
#include <qsqldatabase.h>
#include <qsqlerror.h>

int main(int argc,char ** argv)
{
 QApplication a(argc,argv);

 qDebug(QSqlDatabase::drivers().join("\n"));

 if(QSqlDatabase::isDriverAvailable("QMYSQL3"))
 {
	 QSqlDatabase* db=QSqlDatabase::addDatabase("QMYSQL3");
	 db->setDatabaseName("mercatortransp"); // or an existing MySQL database
	 db->setUserName("mercatortransp"); // or a valid MySQL user
	 db->setPassword("seppi"); // a valid password for user.
	 // Comment if no password. But this is NOT recommanded.
	 db->setHostName("localhost"); // or whatever host hosting MySQL server
	 if(!db->open())
	 {
		 QSqlError err=db->lastError();
		 qDebug(QString("Error Opening Database connection:\n%1\n%2").arg(err.driverText()).arg(err.databaseText()));
		}
		else
		{
			qDebug("MySQL is ok.");
		}
	}
	else
	{
		qDebug("MySQL Driver is not available. Please review your install");
	}
	return 0;
}
