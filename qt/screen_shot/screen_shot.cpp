/**
 * $Id$
 */

#include <QWidget>
#include <QDesktopWidget>
#include <QPainter>
#include <QPixmap>
#include <QApplication>
#include <QString>
#include <QStringList>
#include <QProcess>


class Screenshot: public QWidget
{
	public:
		Screenshot(QWidget* parent = 0);

	protected:
		void paintEvent(QPaintEvent*);

	private:
		const QString TMP;
		const QStringList env;
		QPixmap p;
};

Screenshot::Screenshot(QWidget* parent):
	QWidget(parent),
	TMP("TMP="),
	env(QProcess::systemEnvironment())
{
	QDesktopWidget desktop;
	p = QPixmap::grabWindow(desktop.screen()->winId());

	QString path(".");
	
	QStringList paths(env.filter(TMP));
	if(paths.isEmpty() == false)
	{
		path = paths[0].replace(TMP, "");
	}

	path += "/screenshot.png";

	resize(p.size());
	p.save(path, "PNG");
}

void Screenshot::paintEvent(QPaintEvent*)
{
	QPainter painter(this);
	painter.drawPixmap(0, 0, p);
}

int main(int argc, char* argv[])
{
	QApplication app(argc, argv);
	Screenshot s;
	s.show();

	return app.exec();
}
