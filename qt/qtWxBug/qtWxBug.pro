#
# $Id$
#

PREFIX = /usr/local

LANGUAGE = C++

CONFIG += qt rtti stl warn_on

QT += network xml
TEMPLATE = app

#DEFINES += NO_SYSTEM_TRAY_SUPPORT
DEFINES += COMPILATION_DATE="\"`date --iso-8601=seconds`\""
DEFINES += VERSION="\"`./compilation_run`\""

TARGET = qtWxBug
target.path = $$PREFIX/bin

RESOURCES = qtWxBug.qrc

INCLUDEPATH += src
INSTALLS += target

win32 {
    debug {
          CONFIG += console
    }

    RC_FILE = icons/qtWxBug.rc
    LIBS += -lws2_32
}

unix {
    !mac {
        MOC_DIR = generated/.moc
        OBJECTS_DIR = generated/.obj
        RCC_DIR = generated/res
        UI_DIR = generated/ui
    }

    mac {
          RC_FILE = icons/qtWxBug.icns
    }
}

QMAKE_CXXFLAGS_RELEASE += -g $$system(cflags.sh QMAKE_RELEASE)
QMAKE_CXXFLAGS_DEBUG += -g $$system(cflags.sh QMAKE_DEBUG)

FORMS += ui/Preferences.ui \
    ui/Main.ui
FORMS += ui/Response.ui
FORMS += ui/About.ui

HEADERS += src/Preferences.h
HEADERS += src/WxBugNetClient.h
HEADERS += src/Stations.h
HEADERS += src/Weather.h
HEADERS += src/Forecast.h
HEADERS += src/TrayIcon.h

SOURCES += src/qtWxBug.cpp
SOURCES += src/WxBugNetClient.cpp
SOURCES += src/Preferences.cpp
SOURCES += src/Stations.cpp
SOURCES += src/Weather.cpp
SOURCES += src/Forecast.cpp
SOURCES += src/TrayIcon.cpp
