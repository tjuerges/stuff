#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

#ifndef WEATHER_H_
#define WEATHER_H_
/**
 * $Id$
 */


#include <WxBugNetClient.h>
#include <QString>
#include <QDateTime>


class Weather: public WxBugNetClient
{
    Q_OBJECT

    public slots:
    virtual void requestData();


    public:
    Weather(QWidget* parent, const QString& _accessCode);
    virtual ~Weather();


    private:
    Weather();
    Weather(const Weather&);
    Weather& operator=(const Weather&);

    virtual void parse(const QString& response);

    QString accessCode;

    const QString version;
    const QString fore;

    QDateTime timestamp;
    float temperature;
};
#endif
