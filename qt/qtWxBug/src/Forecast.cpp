/**
 * $Id$
 */


#include "Forecast.h"


Forecast::Forecast(QWidget* parent):
    WxBugNetClient(parent,
        "/forecastISAPI/ForecastISAPI.cgi?GetForecast60&Magic=10992&RegNum=0")
{
}

Forecast::~Forecast()
{
}

void Forecast::requestData()
{
    const QString urlString(Exec
        + ZipCodeString
        + ZipCode
        + StationIDString
        + StationID
        + "&Version=7"
        + UnitsString
        + Units
        + RandomNumberString
        + randomNumber()
        + "&lv=0");

    makeRequest(urlString);
}

void Forecast::parse(const QString& response)
{
    /**
     Forecast response:
     31|03:31 PM MDT May 11, 2007|Tonight|--|48院18|Partly cloudy in the|evening...|Saturday|85院48院5|Mostly sunny in the|morning then...|Sunday|85院48院143|Mostly sunny in the|morning...then...|1178878140|1178927880|3600|
     */
    if(response.isEmpty() == true)
    {
        output->append("Received an empty string for parsing. Ignoring it.");
    }
    else
    {
        output->append("Forecast response:");
        output->append(response);
    }
}
