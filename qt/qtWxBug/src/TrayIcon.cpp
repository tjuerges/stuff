/**
 * $Id$
 */


#include "TrayIcon.h"
#include "Preferences.h"
#include "ui_About.h"
#include "Stations.h"
#include "Weather.h"
#include "Forecast.h"

#include <QMenu>
#include <QAction>
#include <QIcon>
#include <QtNetwork>
#include <iostream>
#include <QtCore>


TrayIcon::TrayIcon(QIcon myIcon):
    QSystemTrayIcon(myIcon),
    menu(new QMenu),
    preferences(new Preferences(0)),
    stations(new Stations(this, preferences->AccessCode->text())),
    weather(new Weather(this, preferences->AccessCode->text())),
    forecast(new Forecast(this, preferences->AccessCode->text()))
{
    prefs = menu->addAction(QString("Preferences"), this, SLOT(execPrefs()));
    about = menu->addAction(QString("About"), this, SLOT(execAbout()));
    menu->addSeparator();
    leave = menu->addAction(QString("Quit"), this, SLOT(execLeave()));

    setupSignals();

    QObject::connect(preferences->Metric, SIGNAL(toggled(bool)), weather,
        SLOT(setMetric(bool)));

    setContextMenu(menu);
}

TrayIcon::~TrayIcon()
{
}

void TrayIcon::setupSignals()
{
    QObject::connect(preferences->GetStations, SIGNAL(clicked()), stations,
        SLOT(requestData()));
    QObject::connect(preferences->GetWeather, SIGNAL(clicked()), weather,
        SLOT(requestData()));
    QObject::connect(preferences->GetForecast, SIGNAL(clicked()), forecast,
        SLOT(requestData()));

    QObject::connect(preferences->ZipCode, SIGNAL(valueChanged(int)), stations,
        SLOT(zipCodeChanged(int)));
    QObject::connect(preferences->ZipCode, SIGNAL(valueChanged(int)), weather,
        SLOT(zipCodeChanged(int)));
    QObject::connect(preferences->ZipCode, SIGNAL(valueChanged(int)), forecast,
        SLOT(zipCodeChanged(int)));

    QObject::connect(preferences->AccessCode, SIGNAL(textChanged(QString)),
        stations, SLOT(accessCodeChanged(QString)));
    QObject::connect(preferences->AccessCode, SIGNAL(textChanged(QString)),
        weather, SLOT(accessCodeChanged(QString)));
    QObject::connect(preferences->AccessCode, SIGNAL(textChanged(QString)),
        forecast, SLOT(accessCodeChanged(QString)));

    QObject::connect(preferences->ShowResponse, SIGNAL(toggled(bool)),
        stations, SLOT(showResponse(bool)));
    QObject::connect(preferences->ShowResponse, SIGNAL(toggled(bool)), weather,
        SLOT(showResponse(bool)));
    QObject::connect(preferences->ShowResponse, SIGNAL(toggled(bool)),
        forecast, SLOT(showResponse(bool)));
}

void TrayIcon::execPrefs()
{
    preferences->show();
}

void TrayIcon::execAbout()
{
    QPointer< QDialog > aboutDialog(new QDialog);
    Ui::About about;
    about.setupUi(aboutDialog);
    aboutDialog->show();
}

void TrayIcon::execLeave()
{
    hide();
    QApplication::quit();
}
