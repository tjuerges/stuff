/**
 * $Id$
 */


#include "Weather.h"
#include <QTextEdit>
#include <iostream>
#ifdef NO_SYSTEM_TRAY_SUPPORT
#include <cstdlib>
#endif


Weather::Weather(QWidget* parent, const QString& _accessCode):
    WxBugNetClient(parent,
        "/WxDataISAPI/WxDataISAPI.dll?Magic=10991&RegNum=0"),
    version("&Version=7"),
    fore("&Fore=2")
{
    if(_accessCode.isEmpty() == true)
    {
        accessCode = "&magic=21771";
    }
    else
    {
        accessCode = _accessCode;
    }
}

Weather::~Weather()
{
}

void Weather::requestData()
{
    /**
     Magic=10991&
     RegNum=0&
     ZipCode=87801&
     Units=1&
     Version=7&
     Fore=2&
     t=12345
     */

    const QString urlString(Exec
        + zipCodeString
        + zipCode
        + unitsString
        + units
        + version
        + fore
        + randomNumberString
        + randomNumber());

    makeRequest(urlString);
}

void Weather::parse(const QString& response)
{
    if(response.isEmpty() == true)
    {
        output->append("Received an empty string for parsing. Ignoring it.");
        return;
    }

    std::cout << response.toStdString()
        << "\n";
    output->append(response);

    QStringList list(response.split("|"));

    QString timestampHelper(list.value(2) + "T" + list.value(1) + "m");
    timestamp = QDateTime::fromString(timestampHelper, "M/dd/yyyyTh:mm:ssap");
    output->append("Timestamp: " + timestamp.toString("yyyy-MM-ddThh:mm:ss"));

    temperature = list.value(3).toFloat();
    QString temp;
    temp.setNum(temperature);
    output->append("Temperature = " + temp);

    output->append("Wind Direction = " + list.value(4));
    output->append("Wind Speed = " + list.value(5));
    output->append("Gust Wind Direction = " + list.value(6));
    output->append("Gust Wind Speed = " + list.value(7));
    output->append("Today Rainfall = " + list.value(8));
    output->append("Rainfall Rate = " + list.value(9));
    output->append("Barometer = " + list.value(10));
    output->append("Humidity = " + list.value(11));
    output->append("High Temp = " + list.value(12));
    output->append("Low Temp = " + list.value(13));
    output->append("Dew Point = " + list.value(14));
    output->append("Wind Chill = " + list.value(15));
    output->append("Monthly Rain = " + list.value(16));
    output->append("Temperature Rate = " + list.value(17));
    output->append("Humidity Rate = " + list.value(18));
    output->append("Barometer Rate = " + list.value(19));
    output->append("High Humidity = " + list.value(20));
    output->append("Low Humidity = " + list.value(21));
    output->append("High Barometer = " + list.value(22));
    output->append("Low Barometer = " + list.value(23));
    output->append("Max Rain Rate = " + list.value(24));
    output->append("Gust Time = " + list.value(25));
    output->append("Average Wind Direction = " + list.value(26));
    output->append("Average Wind Speed = " + list.value(27));
    output->append("Indoor Temp = " + list.value(28));
    output->append("Auxiliary Temp = " + list.value(29));
    output->append("Light = " + list.value(30));
    output->append("Yearly Rainfall = " + list.value(31));
    output->append("Indoor Temp Rate = " + list.value(32));
    output->append("Aux Temp Rate = " + list.value(33));
    output->append("Light Rate = " + list.value(34));
    output->append("Station Name = " + list.value(35));
    output->append("City and State Name = " + list.value(36));
    output->append("Active Query Frequency (seconds) = " + list.value(37));
    output->append("Inactive/Background Query Frequency (Seconds) = "
        + list.value(38));
}
