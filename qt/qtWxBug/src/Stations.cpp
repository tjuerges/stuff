/**
 * $Id$
 */


#include "Stations.h"
#include <QTextEdit>


Stations::Stations(QWidget* parent, const QString& _accessCode):
    WxBugNetClient(parent, "/wxdataisapi/wxdataisapi.dll?getstations")
{
    if(_accessCode.isEmpty() == true)
    {
        accessCode = "&magic=21771";
    }
    else
    {
        accessCode = _accessCode;
    }
}

Stations::~Stations()
{
}

void Stations::requestData()
{
    /**
     getstations&ZipCode=<ZipCode>&magic=21771
     */
    const QString urlString(Exec
        + zipCodeString
        + zipCode
        + accessCode);

    makeRequest(urlString);
}

void Stations::parse(const QString& response)
{
    /**
     Return values:
     Magic Number (21771)
     NoofStations
     ZipCode
     StationID
     Station Name
     City, State
     Distance from Zip Code center
     Station_Type 0: Internet 1:Modem
     City, State of Given ZipCode
     StationID2
     Etc.
     */
    if(response.isEmpty() == true)
    {
        output->append("Received an empty string for parsing. Ignoring it.");
    }
    else
    {
        output->append(response);
        QStringList list(response.split("|"));
        //const unsigned int number(list.value(1).toUInt());
    }
}
