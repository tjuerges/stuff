#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

#ifndef WXBUGNETCLIENT_H_
#define WXBUGNETCLIENT_H_
/**
 * $Id$
 */


#include <QObject>
#include <QPointer>
#include <QtNetwork>
#include <QString>
#include <ui_Response.h>


class QWidget;
class QDialog;
class QTextEdit;


class WxBugNetClient: public QObject
{
    Q_OBJECT

    public slots:
    virtual void showResponse(bool);
    virtual void accessCodeChanged(QString);
    virtual void zipCodeChanged(int);
    virtual void requestData() = 0;
    virtual void setMetric(bool);


    public:
    WxBugNetClient(QWidget* parent, const QString& exec);
    virtual ~WxBugNetClient();


    private slots:
    virtual void httpRequestFinished(int request, bool error);


    private:
    friend class Stations;
    friend class Weather;
    friend class Forecast;

    virtual void parse(const QString&) = 0;
    void makeRequest(const QString& request);
    const QString randomNumber() const;

    QWidget* myParent;
    const QString Exec;
    const QString RandomNumberString;
    const QString ZipCodeString;
    const QString UnitsString;
    const QString StationIDString;
    QString ZipCode;
    QString Units;
    QString StationID;
    QPointer< QHttp > http;
    QUrl url;
    int requestId;
    QPointer< QDialog > responseDialog;
    QTextEdit* output;
    Ui::ResponseDialog ui;
};
#endif
