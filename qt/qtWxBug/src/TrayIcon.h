#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

#ifndef trayIcon_h
#define trayIcon_h
/**
 * $Id$
 */


#include <QSystemTrayIcon>
#include <QPointer>
#include <QIcon>
#include <QString>


class QMenu;
class QAction;
class Preferences;
class Stations;
class Weather;
class Forecast;


class TrayIcon: public QSystemTrayIcon
{
    Q_OBJECT

    public:
    TrayIcon(QIcon myIcon);
    virtual ~TrayIcon();


    private slots:
    void execPrefs();
    void execAbout();
    void execLeave();


    private:
    void setupSignals();

    QPointer< QMenu > menu;
    QPointer< QIcon > icon;
    QPointer< Preferences > preferences;
    QPointer< Stations > stations;
    QPointer< Weather > weather;
    QPointer< Forecast > forecast;
    QAction* prefs;
    QAction* about;
    QAction* leave;
};
#endif
