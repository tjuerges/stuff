/**
 * $Id$
 */


#include "WxBugNetClient.h"
#include "ui_Response.h"
#include <QDialog>
#include <QTextEdit>


WxBugNetClient::WxBugNetClient(QWidget* parent, const QString& exec):
    myParent(parent),
    Exec(exec),
    RandomNumberString("&t="),
    ZipCodeString("&ZipCode="),
    UnitsString("&Units="),
    StationIDString("&StationID="),
    http(new QHttp(this)),
    requestId(-1),
    responseDialog(new QDialog(myParent))
{
    ui.setupUi(responseDialog);
    output = ui.Output;

    QObject::connect(http, SIGNAL(requestFinished(int, bool)),
        SLOT(httpRequestFinished(int, bool)));
}

WxBugNetClient::~WxBugNetClient()
{
}

void WxBugNetClient::accessCodeChanged(QString accessCode)
{
    url.setHost(QString("%1.isapi.wxbug.net").arg(accessCode));
    http->setHost(url.host());
    output->append(url.host());
    output->append(QString("Access code changed: %1").arg(accessCode));
}

void WxBugNetClient::zipCodeChanged(int newZipCode)
{
    ZipCode.setNum(newZipCode);
    output->append(QString("Access code changed: %1").arg(ZipCode));
}

void WxBugNetClient::makeRequest(const QString& connectionString)
{
    output->append(connectionString);
    requestId = http->get(connectionString);
}

void WxBugNetClient::httpRequestFinished(int id, bool error)
{
    if(error == true)
    {
        output->append(http->errorString());
    }
    else if(id == requestId)
    {
        parse(http->readAll().data());
    }
}

void WxBugNetClient::showResponse(bool isVisible)
{
    responseDialog->setVisible(isVisible);
}

const QString WxBugNetClient::randomNumber() const
{
#ifndef NO_SYSTEM_TRAY_SUPPORT
    return QString("%1").arg(qrand());
#else
    return QString("%1").arg(std::rand());
#endif
}

void WxBugNetClient::setMetric(bool useMetric)
{
    if(useMetric == false)
    {
        Units.setNum(0);
    }
    else
    {
        Units.setNum(1);
    }

    QString metric("Using now ");
    metric.append((useMetric == false) ? "Imperial" : "Metric");
    metric.append(" units.");
    output->append(metric);
}
