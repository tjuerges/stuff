#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

#ifndef Preferences_h
#define Preferences_h
/**
 * $Id$
 */


#include <QDialog>
#include <ui_Preferences.h>


class QWidget;


class Preferences: public QDialog, public Ui::Preferences
{
    Q_OBJECT

    public:
    Preferences(QWidget* parent);
    virtual ~Preferences();


    private:
    Preferences();
    Preferences& operator=(const Preferences&);
    Preferences(const Preferences&);
};
#endif
