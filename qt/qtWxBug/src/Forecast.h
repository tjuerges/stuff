#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

#ifndef FORECAST_H_
#define FORECAST_H_
/**
 * $Id$
 */


#include "WxBugNetClient.h"


class QWidget;


class Forecast: public WxBugNetClient
{
    Q_OBJECT

    public slots:
    virtual void requestData();


    public:
    Forecast(QWidget* parent);
    virtual ~Forecast();


    private:
    virtual void parse(const QString& result);
};
#endif
