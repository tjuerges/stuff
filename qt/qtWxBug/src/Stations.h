#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

#ifndef STATIONS_H_
#define STATIONS_H_
/**
 * $Id$
 */


#include <WxBugNetClient.h>
#include <QString>


class Stations: public WxBugNetClient
{
    Q_OBJECT

    public slots:
    virtual void requestData();


    public:
    Stations(QWidget* parent, const QString& stationMagic);
    virtual ~Stations();


    private:
    Stations();
    Stations(const Stations&);
    Stations& operator=(const Stations&);

    virtual void parse(const QString& result);

    QString accessCode;
};
#endif
