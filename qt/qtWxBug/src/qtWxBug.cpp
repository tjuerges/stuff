/**
 * $Id$
 */


#include <QApplication>
#include <QMessageBox>
#include <QMenu>
#include <QPointer>
#include <ui_About.h>
//#include <qtWxBug.h>


#ifndef NO_SYSTEM_TRAY_SUPPORT
#include <QSystemTrayIcon>
#endif


/*
 Partner ID: 1058146812
 License Key: 939c1f1593fc9559
 */


int main(int argc, char* argv[])
{
    QApplication app(argc, argv);

#ifndef NO_SYSTEM_TRAY_SUPPORT
    QPointer< QSystemTrayIcon > mainUi;
    QPointer< QMenu > menu;
    if(QSystemTrayIcon::isSystemTrayAvailable() == false)
    {
        return QMessageBox::critical(0, "qtWxBug",
            "No system tray is available.\n"
            "This application cannot run without it.");
    }
    else
    {
        menu = new QMenu;
        mainUi = new QSystemTrayIcon(QIcon(":/icons/WeatherBugLogo.png"));
        mainUi->setContextMenu(menu);
#endif

    QPointer< About > mainUi(
        new About(QIcon(":/icons/WeatherBugLogo.png")));
//    QPointer< Ui::Main > mainUi(new Ui::Main(QIcon(":/icons/WeatherBugLogo.png")));
    mainUi->show();

    return app.exec();
}
