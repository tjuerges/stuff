#
# $Id$
#

TEMPLATE = app
TARGET = Example3

LANGUAGE = C++

#DEFINES +=

#CONFIG += qt rtti stl warn_on release
CONFIG += rtti stl warn_on
#CONFIG -= qt

#QMAKE_LFLAGS += -g

QMAKE_CXXFLAGS_RELEASE += -g $$system(cflags.sh QMAKE_RELEASE_CXX) \
	-DCOMPILATION_DATE=\\\"`date --iso-8601=seconds`\\\" \
    -DVERSION=\\\"`./compilation_run`\\\"
QMAKE_CXXFLAGS_DEBUG += -g $$system(cflags.sh QMAKE_DEBUG_CXX) \
	-DCOMPILATION_DATE=\\\"`date --iso-8601=seconds`\\\" \
    -DVERSION=\\\"`./compilation_run`\\\"

QMAKE_CFLAGS_RELEASE += -g $$system(cflags.sh QMAKE_RELEASE_C) \
	-DCOMPILATION_DATE=\\\"`date --iso-8601=seconds`\\\" \
    -DVERSION=\\\"`./compilation_run`\\\"
QMAKE_CFLAGS_DEBUG += -g $$system(cflags.sh QMAKE_DEBUG_C) \
	-DCOMPILATION_DATE=\\\"`date --iso-8601=seconds`\\\" \
    -DVERSION=\\\"`./compilation_run`\\\"

#INCLUDEPATH +=  \
#    /ace \
#    /TAO \
#    /TAO/orbsvcs \
#    /TAO/orbsvcs/orbsvcs \
#    /include \
#    /../rtai/include \
#    /home/thomas/workspace/introot.ICD/include \
#    /home/thomas/workspace/introot.CONTROL/include\
#    /include

#LIBPATH += /ace \
#    /TAO/tao \
#    /lib \
#    /../rtai/lib \
#    /home/thomas/workspace/introot.ICD/lib \
#    /home/thomas/workspace/introot.CONTROL/lib \
#    /lib

#LIBS += -lACE \
#    -lloki \
#    -lacsutil \
#    -lacscommonStubs \
#    -lbaselogging \
#    -llogging

#TEMPLATE = subdirs
#SUBDIRS += foo bar

#FORMS += Example3.ui

HEADERS += Example3.h

SOURCES += Example3.cpp

