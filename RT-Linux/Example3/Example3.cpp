/**
 * $Id$
 */

#include <iostream>
#include <string>
#include <vector>
#include <sstream>

#include "Example3.h"


int main(int argc __attribute__((unused)), char* argv[] __attribute__((unused)))
{
    std::cout << "Compiled on "
        << COMPILATION_DATE
        << ", version "
        << VERSION
        << "."
        << std::endl;

    return 0;
};
