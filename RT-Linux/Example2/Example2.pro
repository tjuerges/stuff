#
# $Id$
#

TEMPLATE = app
TARGET = Example2

LANGUAGE = C

#DEFINES +=

#CONFIG += qt rtti stl warn_on release
CONFIG += debug warn_on
CONFIG -= qt

QMAKE_LINK = gcc

QMAKE_LFLAGS += -g

QMAKE_CFLAGS_RELEASE += $$system(cflags.sh QMAKE_RELEASE_C)
QMAKE_CFLAGS_DEBUG += $$system(cflags.sh QMAKE_DEBUG_C)

LIBPATH += /usr/lib

LIBS += -lrt

SOURCES += \
	Example2.c

