#
# $Id$
#

TEMPLATE = app
TARGET = Example1

LANGUAGE = C++

#DEFINES +=

#CONFIG += qt rtti stl warn_on release
CONFIG += rtti stl warn_on
CONFIG -= qt

QMAKE_LFLAGS += -g

QMAKE_CXXFLAGS_RELEASE += -g $$system(cflags.sh QMAKE_RELEASE_CXX) \
	-DCOMPILATION_DATE=\\\"`date --iso-8601=seconds`\\\" \
    -DVERSION=\\\"`./compilation_run`\\\"
QMAKE_CXXFLAGS_DEBUG += -g $$system(cflags.sh QMAKE_DEBUG_CXX) \
	-DCOMPILATION_DATE=\\\"`date --iso-8601=seconds`\\\" \
    -DVERSION=\\\"`./compilation_run`\\\"

QMAKE_CFLAGS_RELEASE += -g $$system(cflags.sh QMAKE_RELEASE_C) \
	-DCOMPILATION_DATE=\\\"`date --iso-8601=seconds`\\\" \
    -DVERSION=\\\"`./compilation_run`\\\"
QMAKE_CFLAGS_DEBUG += -g $$system(cflags.sh QMAKE_DEBUG_C) \
	-DCOMPILATION_DATE=\\\"`date --iso-8601=seconds`\\\" \
    -DVERSION=\\\"`./compilation_run`\\\"

LIBS += -lrt

HEADERS += Example1.h

SOURCES += Example1.cpp

